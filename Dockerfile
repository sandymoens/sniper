FROM java:8
WORKDIR /
ADD target/sniper.war target/sniper.war
ADD docker_properties sniper_properties
EXPOSE 8080
CMD java -jar target/sniper.war
