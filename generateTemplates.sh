#!/bin/bash
#This requires the presence of node, npm and handlebars 

TEMPLATES="src/main/webapp/templates/templates.js"
PARTIALS="src/main/webapp/templates/partials.js"

if [ -f $TEMPLATES ]
then
	echo "Remove existing template file '$TEMPLATES'"
	rm $TEMPLATES
else
	echo "Template file '$TEMPLATES' does not exist"
fi
	
if [ -f $PARTIALS ]
then
	echo "Remove existing partials file '$PARTIALS'"
	rm $PARTIALS
else
	echo "Partials file '$PARTIALS' does not exist"
fi

echo "Generating templates file"
handlebars src/main/webapp/templates/templates/*.handlebars >> $TEMPLATES

echo "Generating partials file"
handlebars -p src/main/webapp/templates/partials/*.handlebars >> $PARTIALS
