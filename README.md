# README # 

This repository implements SNIPER, an interactive tool for pattern discovery, with Sandy Moens as its lead developer. It is based on the interactive framework called MIME, which has originally been published as a demo paper at KDD'11 (http://www.adrem.ua.ac.be/bibrem/pubs/MIME.pdf).

> "We present a framework for interactive visual pattern mining. Our system enables the user to browse through the data and patterns easily and intuitively, using a toolbox consisting of interestingness measures, mining algorithms and post-processing algorithms to assist in identifying interesting patterns. By mining interactively, we enable the user to combine their subjective interestingness measure and background knowledge with a wide variety of objective measures to easily and quickly mine the most important and interesting patterns. Basically, we enable the user to become an essential part of the mining algorithm. Our demo currently applies to mining interesting itemsets and association rules, and its extension to episodes and decision trees is ongoing."



## Quick start ##

### Compilation ###
The implementation is written in Java and JavaScript. It has some additional dependencies such as [RealKD](https://bitbucket.org/realKD/) for its data model. It uses [Maven](https://maven.apache.org/) for compilation.

In order to compile a non-stable (non-master) build, a RealKD snapshot version is most probably required as a dependency for the project to be built. Please first clone the [RealKD repository](https://bitbucket.org/realKD/realkd/wiki/Home), compile, and install the latest version using maven:

> mvn clean install

To compile the SNIPER project using Maven first clone the code using git and in the resulting directory run the following command from the terminal:

> mvn clean package

If all goes well, this command generates a war file called _target/sniper.war_.

Alternatively one can download a precompiled version of the software [here](https://bitbucket.org/sandymoens/sniper/downloads). Note that the setup below still has to happen for the software to work properly.


### Setup ###

SNIPER requires some additional dependencies in order to run. First of all, it requires at least Java 8 (and up) to be installed. Secondly, the software requires MySQL to be installed and credentials for a user and a database.

We assume that a database called sniper-meta has been created and a user with proper credentials and privileges on the table is available.

1. Copy the files under _src/main/resources_ to a folder called _properties_.
2. Adjust configuration files _persistence.properties_ and _sniper.properties_ according to the file system paths and the database credentials.
3. Export the _properties_ folder as an environment variable with name _SNIPER_CONF_DIR_.
    > export SNIPER_CONF_DIR=properties
    

### Running ###

To run the jar file use the java command as follows:

> java -jar target/sniper.war
