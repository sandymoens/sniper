<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>SNIPER - Schemes</title>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.3.1/dist/jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/chosen/1.8.7/chosen.jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/popper.js/1.14.3/umd/popper.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap/4.2.1/js/bootstrap.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap-material-design/0.5.10/dist/js/material.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery.inview/1.0.0/jquery.inview.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/FileSaver.js/0.0.2/FileSaver.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/handlebars/4.0.12/handlebars.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/smartmenus/1.1.0/dist/jquery.smartmenus.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/templates/templates.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/templates/partials.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/bootstrap-iconpicker.bundle.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/handlebars.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/const.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/messenger.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/utils.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/globalMenuController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/info/schemesController.js"></script>
		
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand" >
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/chosen/1.8.7/chosen.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/bootstrap/4.2.1/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/bootstrap-material-design/0.5.10/dist/css/bootstrap-material-design.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/smartmenus/1.1.0/dist/css/sm-core-css.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/smartmenus/1.1.0/dist/css/sm-clean/sm-clean.css">
		<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap-iconpicker.min.css">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/sniper-ui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/smartmenus.css">
		
		<script type="text/javascript">
			var URL_HANDLE="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/";
			
			var messenger = Messenger();
			messenger.addMessenger(MessageBoxMessenger());
		</script>
	</head>
	
	<body>
	
		<%@ include file="../common/globalMenu.jsp"%>

		<div class="row no-gutters" style="display: inline-flex; width: 100%;">
		
			<div class="mainPage">
			
				<div class="sidebar">
					<div class="scard">
						<div class="ui-widget-content padded">
							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<button class="btn" id="viewSchemes">View schemes</button>
									</div>
								</div>
								<div class="row">
									<div class="col">
										<button class="btn" id="createScheme">Create new scheme</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
				<div class="mainPageContent">
					<div class="scard">
						<%
							if("create".equals(request.getParameter("page"))) {
						%>
							<%@ include file="schemesCreate.html"%>
							<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/momentjs/2.22.2/min/moment-with-locales.min.js"></script>
							<script type="text/javascript" src="${pageContext.request.contextPath}/js/info/schemesCreateController.js"></script>
							<script type="text/javascript">
							
								var schemesCreateController;
								
								$(function() {
									schemesCreateController = new SchemesCreateController(messenger);
									schemesCreateController.init();
								});
								
							</script>
						<%
							} else if ("view".equals(request.getParameter("page"))) {
						%>
							<%@ include file="schemesView.html"%>
							<script type="text/javascript" src="${pageContext.request.contextPath}/js/info/schemesViewController.js"></script>
							<script type="text/javascript">
							
								var schemesViewController;
								
								$(function() {
									schemesViewController = new SchemesViewController(messenger);
									schemesViewController.init();
								});
								
							</script>
						<%
							}
						%>
					</div>
				</div>
				
			</div>

		</div>
		
		<%@ include file="../common/footer.html"%>
			
	</body>
	
	<script type="text/javascript">
	
		var globalMenuController;
		var schemesController;
		
		$(function() {
			globalMenuController = new GlobalMenuController();
			globalMenuController.init();
			
			schemesController = new SchemesController();
			schemesController.init();
			
			$(".chosen-select").chosen();
		});
		
	</script>
	
	<script type="text/javascript">
		// Iconpicker initialization code.
		// Author: Joey De Pauw
		function loaded(selector, callback){
			var obs = new MutationObserver(function(mutations, observer) {
				  // using jQuery to optimize code
				  $.each(mutations, function (i, mutation) {
				    var addedNodes = $(mutation.addedNodes);
				    var filteredEls = addedNodes.find(selector).addBack(selector); // finds either added alone or as tree
				    filteredEls.each(function () { // can use jQuery select to filter addedNodes
				      callback($(this));
				    });
				  });
				});
			obs.observe(document.body, {childList: true, subtree: true});
		}
		
		loaded('[role=iconpicker]', function(el){
			el.iconpicker();
		});		
    </script>
	
</html>