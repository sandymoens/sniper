<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>SNIPER</title>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.3.1/dist/jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/chosen/1.8.7/chosen.jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/popper.js/1.14.3/umd/popper.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap/4.2.1/js/bootstrap.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap-material-design/0.5.10/dist/js/material.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery.inview/1.0.0/jquery.inview.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/FileSaver.js/0.0.2/FileSaver.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/handlebars/4.0.12/handlebars.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/smartmenus/1.1.0/dist/jquery.smartmenus.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/d3js/4.10.2/d3.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jStat/1.7.1/dist/jstat.min.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/templates/templates.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/templates/partials.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/handlebars.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/const.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/messenger.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/utils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/charts.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/graphs.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/circular.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/globalMenuController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/patternUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/event.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/selectionModel.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/globalController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/subMenuController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/sourceDockController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/workDockController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/workDockMenuController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/measuresController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/visualizationsController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/minersController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/patternCollectionPostProcessorsController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/patternPostProcessorsController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/worksheetTabsController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/tabsController.js"></script>
		
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand" >
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand:500" >
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/chosen/1.8.7/chosen.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/bootstrap/4.2.1/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/bootstrap-material-design/0.5.10/dist/css/bootstrap-material-design.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/smartmenus/1.1.0/dist/css/sm-core-css.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/smartmenus/1.1.0/dist/css/sm-clean/sm-clean.css">
		<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/circular.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/d3charts.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/sniper-ui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/smartmenus.css">
		
		<script type="text/javascript">
			var URL_HANDLE="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/";

			var workspaceId = "<%=request.getParameter("workspace")%>";
			var worksheetId = "<%=request.getParameter("worksheet")%>";
		</script>
	</head>
	
	<body class="container-fluid">
	
		<%@ include file="../common/globalMenu.jsp"%>
		
		
		<%
			if("new".equals(request.getParameter("worksheet"))) {
		%>
			<div class="row">
				
				<div class="mainPage container-fluid">
			
					<div class="mainPageContent">
						<div class="scard">
						
							<div class="container-fluid">
							
								<div class="row" id="worksheetsSelectWrapper">
									<select class="chosen-select" id="worksheetsSelect">
									</select>
								</div>
								
								<%@ include file="worksheetCreate.html"%>
								<script type="text/javascript" src="${pageContext.request.contextPath}/js/patterns/worksheetCreateController.js"></script>
								<script type="text/javascript">
								
									var worksheetCreateController = new WorksheetCreateController();
									worksheetCreateController.init(workspaceId);
									
								</script>
								
							</div>
							
						</div>
					</div>
					
				</div>
				
			</div>
			
		<%
			} else {
		%>
			<div class="row">
			
				<div class="mainPage container-fluid">
			
					<div class="mainPageContent">
						<div class="scard">
							<div class="ui-tabs ui-corner-all ui-widget ui-widget-content">
							
								<div class="container-fluid">
							
									<div class="row no-gutters" id="worksheetsSelectWrapper">
										<div class="col-md-10">
											<select class="chosen-select" id="worksheetsSelect" style="height: 90%;">
											</select>
										</div>
										<div class="col-md-1">
											<button class="btn-outline-danger" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="Delete worksheet" id="worksheetDelete"><i class="material-icons">remove</i></button>
										</div>
										<div class="col-md-1">
											<button class="btn-outline-secondary" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="Add worksheet" id="worksheetAdd"><i class="material-icons">add</i></button>
										</div>
									</div>
									
									<%@ include file="subMenu.html"%>
								
									<%@ include file="workDockMenu.html" %>
									<%@ include file="workDock.html" %>
								
								</div>
								
							</div>
						</div>
					</div>
					<div class="sidebar">
						<div class="scard">
							<%@ include file="tabs.html" %>
						</div>
						<div class="scard">
							<%@ include file="sourceDock.html" %>
						</div>
					</div>
					
				</div>

			</div>
					
		<%
			}
		%>
		
		<%@ include file="../common/footer.html"%>
			
		<div class="modal largeInputModal" id="userInputModal">
			<div class="modal-dialog">
				<div class="modal-content panel panel-info container-fluid">
					<div class="mainTitle row">
						<div class="col padded" id="userInputModalHeader"></div>
					</div>
					<div class="panel-body row padded" id="userInputModalBody"></div>
				</div>
			</div>
		</div>
	</body>
	
	<script type="text/javascript">
		var globalController = null;
		var globalMenuController = null;
		var worksheetTabsController = null;
		var messenger = null;
	
		$(function() {
		    $('[data-toggle="popover"]').popover({
		   	 	delay: { 
		    	       show: "1000", 
		    	       hide: "100"
		    	    }
		    });
		    
		    $(".chosen-select.hide-search").chosen({disable_search: true});
		    $(".chosen-select").chosen();
		    
			$(".tabs").tabs();

			messenger = Messenger();
			messenger.addMessenger(MessageBoxMessenger());
			messenger.addMessenger(OutputLogMessenger("#outputLog"));
			
			globalController = new GlobalController();
			globalController.init();
			
			globalMenuController = new GlobalMenuController();
			globalMenuController.init();

			worksheetTabsController = new WorksheetTabsController(globalController);
			worksheetTabsController.init();
		});
		
	</script>
	
	<script type="text/javascript">
		// Circular plot initialization code.
		// Author: Joey De Pauw
		function loaded(selector, callback){
			var obs = new MutationObserver(function(mutations, observer) {
				  // using jQuery to optimize code
				  $.each(mutations, function (i, mutation) {
				    var addedNodes = $(mutation.addedNodes);
				    var filteredEls = addedNodes.find(selector).addBack(selector); // finds either added alone or as tree
				    filteredEls.each(function () { // can use jQuery select to filter addedNodes
				      callback($(this));
				    });
				  });
				});
			obs.observe(document.body, {childList: true, subtree: true});
		}
		
		loaded('.circular-pattern-dissection', function(el){
			var height = el.data("height") != undefined ? el.data("height") : 190;
			circular(d3.select(el[0]), el.data("patterns"), height);
		});		
    </script>

</html>
