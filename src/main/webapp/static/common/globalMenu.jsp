<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<form id="logoutForm" method="POST" action="${pageContext.request.contextPath}/logout">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

<div id="globalMenu" class="row globalMenuBar">
	<div class="col-md-10">
		<ul class="sm sm-clean menu" id="optionMenu">
			<li class="optionSpan btn-outline-secondary" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="Profile" id="profileButton" ><i class="fa fa-user-circle"></i></li>
			<li class="optionSpan btn-outline-secondary" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="Data Tables" id="dataTablesButton"><i class="fa fa-database"></i></li>
			<li class="optionSpan btn-outline-secondary" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="Schemes" id="schemesButton"><i class="fa fa-table"></i></li>
			<li class="optionSpan btn-outline-secondary" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="Workspaces" id="workspacesButton"><i class="fa fa-folder"></i></li>
			<sec:authorize access="hasAnyRole('ADMIN')">
				<li class="divider"></li>
				<li class="optionSpan btn-outline-secondary" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="Users" id="usersButton" ><i class="fa fa-users-cog"></i></li>
			</sec:authorize>
			<li class="divider"></li>
			<li class="optionSpan btn-outline-secondary" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="Logout" id="logoutButton" ><i class="fa fa-sign-out-alt"></i></li>			
		</ul>
	</div>

	<div class="col-md-2">
		<div class="pullRight" style="display: inline-block;">
			<span class="bannerText">SNIPER</span>
		 	<img class="bannerLogo" src="${pageContext.request.contextPath}/resources/img/mime.png">
	 	</div>
 	</div>
	
</div>
