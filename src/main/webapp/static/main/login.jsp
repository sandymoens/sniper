<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>SNIPER</title>
		
		<script type="text/javascript" src="${contextPath}/webjars/jquery/3.3.1/dist/jquery.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/chosen/1.8.7/chosen.jquery.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/popper.js/1.14.3/umd/popper.min.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/bootstrap/4.2.1/js/bootstrap.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/bootstrap-material-design/0.5.10/dist/js/material.min.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/handlebars/4.0.12/handlebars.min.js"></script>
		
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand" >
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/chosen/1.8.7/chosen.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/bootstrap/4.2.1/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/bootstrap-material-design/0.5.10/dist/css/bootstrap-material-design.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.css">
		
		<link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/sniper-ui.css">
	</head>
	
	<body>
		<%@ include file="header.html"%>
		
		${ message != null ? '<div id="messageBox" class="alert alert-success messageBox">'.concat(message).concat('</div>') : "" }
		${ error != null ? '<div id="errorBox" class="alert alert-warning messageBox">'.concat(error).concat('</div>') : "" }
		${ info != null ? '<div id="infoBox" class="alert alert-info messageBox">'.concat(info).concat('</div>') : "" }
		
		<div class="container-fluid">
			<div class="row padded">
				<div class="col-md-4"></div>
				<div class="col-md-4 scard ui-widget-content">
					<form method="POST" modelAttribute="userForm" action="${contextPath}/login" class="form-signin">
						<div class="form-group">
							<div class="row padded">
								<div class="col-md-12">
									<h5 class="text-center">
										Log in to SNIPER
									</h5>
								</div>
								<div class="col-md-12">
									<input name="username" type="text" class="form-control text-center" placeholder="Username"/>
								</div>
								<div class="col-md-12">
									<input name="password" type="password" class="form-control text-center" placeholder="Password"/>
								</div>
								<div class="col-md-12">
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
								</div>
								<div class="col-md-12">
									<button class="btn btn-primary btn-block" type="submit">Log In</button>
									<div class="btn btn-sm btn-secondary btn-block"><a href="${contextPath}/registration">Create an account</a></div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<%@ include file="footer.html"%>
		
		<script type="text/javascript">
			$(".messageBox").fadeOut(5000);
		</script>
	
	</body>
</html>