<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>SNIPER</title>
		
		<script type="text/javascript" src="${contextPath}/webjars/jquery/3.3.1/dist/jquery.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/chosen/1.8.7/chosen.jquery.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/popper.js/1.14.3/umd/popper.min.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/bootstrap/4.2.1/js/bootstrap.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/bootstrap-material-design/0.5.10/dist/js/material.min.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/handlebars/4.0.12/handlebars.min.js"></script>
		
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand" >
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/chosen/1.8.7/chosen.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/bootstrap/4.2.1/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/bootstrap-material-design/0.5.10/dist/css/bootstrap-material-design.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.css">
		
		<link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/sniper-ui.css">
	</head>
	
	<body>
	
		<%@ include file="header.html"%>
	

		<div class="container-fluid">
			<div class="row padded">
				<div class="col-md-4"></div>
				<div class="col-md-4 scard ui-widget-content">	
			        <form:form method="POST" modelAttribute="userForm" class="form-signin">
   						<div class="form-group">
			         		<div class="row padded text-center">
								<div class="col-md-12">
									<h5>
										Create new user account
									</h5>
								</div>
								<div class="col-md-12">
									<spring:bind path="username">
					                    <form:input type="text" path="username" class="form-control text-center" placeholder="Username" autofocus="true"></form:input>
					                    <form:errors class="failMessage" path="username"></form:errors>
						            </spring:bind>
								</div>
								<div class="col-md-12 text-center">
									<spring:bind path="password">
					                    <form:input type="password" path="password" class="form-control text-center" placeholder="Password"></form:input>
					                    <form:errors class="failMessage" path="password"></form:errors>
						            </spring:bind>
								</div>
								<div class="col-md-12 text-center">
									<spring:bind path="passwordConfirm">
					                    <form:input type="password" path="passwordConfirm" class="form-control text-center" placeholder="Confirm your password"></form:input>
					                    <form:errors class="failMessage" path="passwordConfirm"></form:errors>
						            </spring:bind>
								</div>
								<div class="col-md-12">
									<button class="btn btn-primary btn-block" type="submit">Register</button>
									<div class="btn btn-sm btn-secondary btn-block"><a href="${contextPath}/">Log in</a></div>
								</div>
							</div>
						</div>
			        </form:form>
			    </div>
	   		</div>
	    </div>

		<%@ include file="footer.html"%>
		
		<script type="text/javascript">
			$(".messageBox").fadeOut(5000);
		</script>
	
	</body>
</html>