<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>SNIPER - Schemes</title>
		
		<script type="text/javascript" src="${contextPath}/webjars/jquery/3.3.1/dist/jquery.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/chosen/1.8.7/chosen.jquery.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/popper.js/1.14.3/umd/popper.min.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/bootstrap/4.2.1/js/bootstrap.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/bootstrap-material-design/0.5.10/dist/js/material.min.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/jquery.inview/1.0.0/jquery.inview.min.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/FileSaver.js/0.0.2/FileSaver.min.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/handlebars/4.0.12/handlebars.min.js"></script>
		<script type="text/javascript" src="${contextPath}/webjars/smartmenus/1.1.0/dist/jquery.smartmenus.js"></script>
		
		<script type="text/javascript" src="${contextPath}/templates/templates.js"></script>
		<script type="text/javascript" src="${contextPath}/templates/partials.js"></script>
		
		<script type="text/javascript" src="${contextPath}/js/common/handlebars.js"></script>
		<script type="text/javascript" src="${contextPath}/js/common/const.js"></script>
		<script type="text/javascript" src="${contextPath}/js/common/messenger.js"></script>
		<script type="text/javascript" src="${contextPath}/js/common/utils.js"></script>
		
		<script type="text/javascript" src="${contextPath}/js/common/globalMenuController.js"></script>
		<script type="text/javascript" src="${contextPath}/js/admin/userAdminController.js"></script>
		
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand" >
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/chosen/1.8.7/chosen.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/bootstrap/4.2.1/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/bootstrap-material-design/0.5.10/dist/css/bootstrap-material-design.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/smartmenus/1.1.0/dist/css/sm-core-css.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/webjars/smartmenus/1.1.0/dist/css/sm-clean/sm-clean.css">
		<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

		<link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/sniper-ui.css">
		<link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/smartmenus.css">
		
		<script type="text/javascript">
			var URL_HANDLE="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${contextPath}/";
			
			var messenger = Messenger();
			messenger.addMessenger(MessageBoxMessenger());
		</script>
	</head>
	
	<body>
	
		<%@ include file="../common/globalMenu.jsp"%>

		<div class="row no-gutters" style="display: inline-flex; width: 100%;">
		
			<div class="mainPage">
				<div class="scard">
					<div class="ui-widget-content padded">
						<div id="mainContent" class="container-fluid">
						</div>
					</div>
				</div>
			</div>

		</div>
		
		<%@ include file="../common/footer.html"%>
			
	</body>
	
	<script type="text/javascript">
	
		var globalMenuController;
		var userController;
		
		$(function() {
			globalMenuController = new GlobalMenuController();
			globalMenuController.init();
			
			userAdminController = new UserAdminController(messenger);
			userAdminController.init();
			
			$(".chosen-select").chosen();
		});
		
	</script>
	
</html>