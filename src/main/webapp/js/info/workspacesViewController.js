function WorkspacesViewController(messenger) {

	var _this;
	var _messenger = messenger;

	function init() {
		_getLinks();

		_bindEvents();
	}

	function _getLinks() {
		ajaxGet({
			url : "links",
			success : _setResource,
			fail : function() {
				_messenger.error("An error occurred while retrieving links!");
			}
		});
	}

	function _setResource(resource) {
		_this._linksResource = resource;

		_getWorkspaces();
	}

	function _getWorkspaces() {
		var url = buildUrl(getLink(_this._linksResource, KEY_WORKSPACES), [ {
			key : KEY_FIELDS,
			value : KEY_WORKSHEETS
		} ]);

		ajaxGet({
			url : url,
			success : function(resource) {
				_this._workspaces = {};

				for (var i = 0; i < resource.resources.length; i++) {
					var workspaceResource = resource.resources[i];
					_this._workspaces[workspaceResource.workspaceId] = workspaceResource;
				}

				$("#blockContainer").html(
						$(Handlebars.getTemplate("ui.workspaceInfoBlocksList")(
								resource.resources)));
			},
			fail : function() {
				_messenger
						.error("An error occurred while retrieving workspaces");
			}
		});
	}

	function _bindEvents() {
		$("#blockContainer").on({
			click : _loadWorkspace
		}, "#workspaceLoad");

		$("#blockContainer").on({
			click : _deleteWorkspace
		}, "#workspaceDelete");
	}

	function _loadWorkspace() {
		var workspace = _this._workspaces[$(this).closest("[workspaceId]")
				.attr("workspaceId")];

		worksheetId = workspace.worksheets.length == 0 ? "new"
				: workspace.worksheets[0].worksheetId;

		window.location = URL_HANDLE + "patterns?workspace="
				+ workspace.workspaceId + "&worksheet=" + worksheetId;
	}

	function _deleteWorkspace() {
		var workspace = _this._workspaces[$(this).closest("[workspaceId]")
				.attr("workspaceId")];

		if (confirm("Are you sure you wish to delete this workspace? Removing this workspace will remove all worksheets attached to the workspace")) {
			var link = getLink(workspace, "self");

			ajaxDelete({
				url : link,
				success : function() {
					var selector = "#blockContainer .infoBlock[workspaceId='"
							+ workspace.workspaceId + "']";

					$(selector).remove();

					_workspaceDeleteSuccess();
				},
				fail : _workspaceDeleteFail
			});
		}
	}

	function _workspaceDeleteSuccess() {
		_messenger.success("Workspace delete successfully");
		_getWorkspaces();
	}

	function _workspaceDeleteFail() {
		_messenger.error("An error occurred while deleting the workspace!");
		_getWorkspaces();
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
