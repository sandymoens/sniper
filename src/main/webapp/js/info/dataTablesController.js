function DataTablesController() {

	var _this;

	function init() {
		_bindEvents();
	}

	function _bindEvents() {
		$("#uploadDataTable").click(function() {
			window.location = URL_HANDLE + "dataTables?page=upload";
		});

		$("#viewDataTables").click(function() {
			window.location = URL_HANDLE + "dataTables?page=view";
		});
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
