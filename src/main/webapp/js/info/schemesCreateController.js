function SchemesCreateController(messenger) {

	var _this;
	var _messenger = messenger;

	function init() {
		_getLinks();
		
		resize();

		_bindEvents();
	}
	
	function _getLinks() {
		ajaxGet({
			url : "links",
			success : _setResource,
			fail : function() {
				messenger.error("An error occurred while retrieving links!");
			}
		});
	}

	function _setResource(resource) {
		_this._linksResource = resource;

		_getDataTables();
	}

	function _getDataTables() {
		ajaxGet({
			url : getLink(_this._linksResource, KEY_DATATABLES),
			success : _setDataTables,
			fail : function() {
				messenger.error("An error occurred while retrieving data tables");
			}
		});
	}

	function _setDataTables(resource) {
		var options = "<option/>";

		for (var i = 0, end = resource.resources.length; i < end; i++) {
			options += "<option value='"
					+ resource.resources[i].dataTableId + "' href='"
					+ getLink(resource.resources[i], "self") + "'>"
					+ resource.resources[i].caption + '</option>';
		}

		$("#dataTableSelect").html(options);
		$("#dataTableSelect").trigger("chosen:updated");
		
		var id = $.urlParam("id");

		if (id !== null) {
			$("#dataTableSelect").val(id);
			$("#dataTableSelect").trigger("chosen:updated");

			_updateDataTableInfo();
		}
	}
	
	function _bindEvents() {
		$("#dataTableSelect").on({
			change : _updateDataTableInfo
		});

		$("#dataTableTypeSelect").on({
			change : _showOptionsForType
		});

		$("#textTypeOfTransactions").on( {
			change: _showOptionsForTextType
		})

		$(".schemeOption").on({
			change : _printPreviewTable
		});
		
		$("#saveScheme").on({
			click: _saveScheme
		});
		

		$(window).resize(debouncer(resize));
	}

	function _bindSchemesEvents() {
		$("#viewSchemeButton").click(_navigateToViewScheme);
		$("#viewWorkspacesButton").click(_navigateToViewWorkspaces);
	}
	
	function resize() {
		var width = $(window).width();
		width -= $(".sidebar").width();
		width -= 45;

		$("#dataView").width(width);
	}

	function _updateDataTableInfo() {
		var dataTableId = $("#dataTableSelect").val();

		if (dataTableId == null) {
			$("#options").hide();
			return;
		}
		
		$("#options").show();

		var url = $("option[value='" + dataTableId + "']").attr("href");

		ajaxGet({
			url : url,
			success : function(resource) {
				_this._resource = resource;

				_resetView();
				
				var url = buildUrl(getLink(_this._resource, KEY_ROWS), [{
					key : KEY_PAGE,
					value : 0
				}, {
					key : KEY_SIZE,
					value : 10
				}]);

				ajaxGet({
					url : url,
					success : function(resource) {
						_this._rowsResource = resource;

						_printPreviewTable();
					},
					fail : function() {
						messenger.error("An error occurred while fetching data rows");
					}

				});
			},
			fail : function() {
				messenger.error("An error occurred while fetching data table");
			}
		});
	}

	function _showOptionsForType() {
		var type = $("#dataTableTypeSelect").val();

		$("#dataTableOptions").hide();
		$("#transactionsOptions").hide();
		$("#textOptions").hide();

		if (type === "dataTable") {
			$("#dataTableOptions").show();
		} else if (type === "transactions") {
			$("#transactionsOptions").show();
		} else if (type === "text") {
			$("#textOptions").show();
			_showOptionsForTextType();
		}

		_printPreviewTable();
	}
	
	function _showOptionsForTextType() {
		var type = $("#textTypeOfTransactions").val();

		$("#textAsTransactionsOptions").hide();
		$("#textAsBowTfIdfOptions").hide();
		
		if(type === "Transactions") {
			$("#textAsTransactionsOptions").show();
		} else if(type ==="BOWTFIDF") {
			$("#textAsBowTfIdfOptions").show();
		}
	}

	function _resetView() {
		$("#dataTableTypeSelect").val("dataTable").trigger("chosen:updated")
				.change();

		$("#dataTableHasHeaders");
		$("#dataTableDelimiter").val(",");

		$("#sequenceDataTableHasHeaders");
		$("#sequenceDataTableDelimiter").val(",");

		$("#transactionsDelimiter").val(",");

		$("#dataView").html("");
	}

	function _printPreviewTable() {
		if (_this._rowsResource == undefined) {
			return;
		}
		var type = $("#dataTableTypeSelect").val();

		var previewTable = {};
		if (type === "dataTable") {
			previewTable.attributes = _getAttributes($("#dataTableHasHeaders").prop("checked"), $("#dataTableDelimiter").val());
			previewTable.rows = _getRows($("#dataTableDelimiter").val());
			
			if($("#dataTableHasHeaders").prop("checked")) {
				previewTable.rows = previewTable.rows.slice(1);
			}
			
			previewTable.typedAttributes = _inferAttributeTypes(previewTable.attributes, previewTable.rows);
		} else if (type === "transactions") {
			previewTable.rows = _getRows($("#transactionsDelimiter").val());
		} else if (type === "text") {
			previewTable.rows = _getRows(transactionsDelimiter);
		}
		
		$("#dataView").html(
				$(Handlebars.getTemplate("ui.editablePreviewTable")(previewTable)));
		
		$("#dataView .chosen-select").chosen({ width: '100%' });
	}
	
	function _inferAttributeTypes(attributes, rows) {
		var attributeTypes = [];
		
		for(var i = 0; i < attributes.length; i++) {
			attributeTypes.push({
				caption: attributes[i],
				type: _inferType(i, rows)
			});
		}
		
		return attributeTypes;
	}
	
	function _inferType(i, rows) {
		if(_isMetric(i, rows)) {
			return "metric";
		} else if(_isDate($("#dataTableDateTimeFormat").val(), i, rows)) {
			return "date";
		}
		return "categoric";
	}
	
	function _isMetric(i, rows) {
		for(var j = 0; j < rows.length; j++) {
			if(isNaN(rows[j][i])) {
				return false;
			}
		}
		return true;
	}
	
	function _isDate(format, i, rows) {
		for(var j = 0; j < rows.length; j++) {
			if(!moment(rows[j][i], format, true).isValid()) {
				return false;
			}
		}
		return true;
	}
	
	function _getAttributes(hasHeaders, delimiter) {
		var attributes = [];
		
		if(hasHeaders) {
			attributes = _this._rowsResource._embedded.listResources[0].resources[0].split(delimiter);
		} else {
			var length = _this._rowsResource._embedded.listResources[0].resources[0].split(delimiter).length;
			
			for(var i = 0; i < length; i++) {
				attributes.push("Attribute_" + (i+1));
			}
		}
		
		return attributes;
	}

	function _getRows(delimiter) {
		return _this._rowsResource._embedded.listResources[0].resources.map(row => {
			if(delimiter == null) {
				return row;
			}
			return row.split(delimiter);
		});
	}

	function _saveScheme() {
		var scheme = _getScheme();
		
		if(scheme == null) {
			_schemeCreateFail();
			return;
		}

		ajaxPost({
			url : getLink(_this._linksResource, KEY_SCHEMES),
			data: JSON.stringify(scheme),
			contentType : "application/json",
			processData : false,
			success: _schemeCreateSuccess,
			fail: _schemeCreateFail
		});
	}
	
	function _getScheme() {
		checkRequiredOptions(["#dtOption", "#generalOptions"]);
		
		if(_this._resource == undefined) {
			return null;
		}

		var type = $("#dataTableTypeSelect").val();

		if (type === "dataTable") {
			return _getDataTableScheme(); 
		} else if (type === "transactions") {
			return _getTransactionsScheme(); 
		} else if (type === "text") {
			return _getTextScheme(); 
		}

		return null;
	}
	
	function _getDataTableScheme() {
		if(checkRequiredOptions(["#dtOption", "#generalOptions", "#dataTableOptions"])) {
			return null;
		}
		
		var scheme = {
			dataTableId    : _this._resource.dataTableId,
			caption        : $("#schemeCaption").val(),
			description    : $("#schemeDescription").val(),
			schemeType     : $("#dataTableTypeSelect").val(),
			attributes     : _getTypedAttributes(),
			hasHeaders     : $("#dataTableHasHeaders").prop("checked"),
			delimiter      : $("#dataTableDelimiter").val(),
			dateTimeFormat : $("#dataTableDateTimeFormat").val()
		};
		
		if(scheme.caption === "") {
			return null;
		}
		
		return scheme;
	}
	
	function _getTypedAttributes() {
		var attributeInfo = $(".previewTable thead th");
		
		return attributeInfo.map(function() {
			return {
				caption: $(this).find("input[type='text']").val(),
				active: $(this).find("input[type='checkbox']").prop("checked"),
				type: $(this).find("select").val(),
				icon: $(this).find(".iconpicker").find("input[type='hidden']").val()
			}
		}).toArray();
	}
	
	function _getTransactionsScheme() {
		if(checkRequiredOptions(["#dtOption", "#generalOptions", "#transactionsOptions"])) {
			return null;
		}
		
		var scheme = {
			dataTableId : _this._resource.dataTableId,
			caption     : $("#schemeCaption").val(),
			description : $("#schemeDescription").val(),
			schemeType  : $("#dataTableTypeSelect").val(),
			delimiter   : $("#transactionsDelimiter").val()
		};
				
		if(scheme.caption === "") {
			return null;
		}
		
		return scheme;
	}
	
	function _getTextScheme() {
		if(checkRequiredOptions(["#dtOption", "#generalOptions", "#textOptions"])) {
			return null;
		}
	
		if($("#textTypeOfTransactions").val() == "Transactions") {
			var scheme = {
				dataTableId 			: _this._resource.dataTableId,
				caption    			: $("#schemeCaption").val(),
				description 			: $("#schemeDescription").val(),
				schemeType  			: "textAsTransactions",
				recognizeSentences	: $("#textRecognizeSentences").is(':checked'),
				alphabeticalOnly		: $("#textAlphabeticalOnly").is(':checked'),
				filterPOS			: $("#textFilterPOS").is(':checked'),
				stemTokens			: $("#textStemTokens").is(':checked'),
				dFFilterType			: $("#textFilterByDocumentFrequency").val(),
				dFFilterValue		: $("#textDFFilterValue").val()
			};
		} else if($("#textTypeOfTransactions").val() == "BOWTFIDF") {
			var scheme = {
					dataTableId 			: _this._resource.dataTableId,
					caption    			: $("#schemeCaption").val(),
					description 			: $("#schemeDescription").val(),
					schemeType  			: "textAsBowTfIdf",
					recognizeSentences	: $("#textRecognizeSentences").is(':checked'),
					alphabeticalOnly		: $("#textAlphabeticalOnly").is(':checked'),
					filterPOS			: $("#textFilterPOS").is(':checked'),
					stemTokens			: $("#textStemTokens").is(':checked'),
					numberOfBins			: $("#textNumberOfBins").val()
				};
		}
		
		if(scheme.caption === "") {
			return null;
		}
		
		return scheme;
	}
	
	function _schemeCreateSuccess(resource) {
		_this._resource = resource;

		_messenger.success("Scheme created successfully");

		$("#mainContent").append(
				$(Handlebars.getTemplate("ui.schemeCreatedSuccessView")()));

		_bindSchemesEvents();
	}

	function _schemeCreateFail() {
		_messenger.error("An error occurred while creating the scheme!");
	}
	
	function _navigateToViewScheme() {
		window.location = URL_HANDLE + "schemes?page=view&id="
			+ _this._resource.schemeId;
	}
	
	function _navigateToViewWorkspaces() {
		window.location = URL_HANDLE + "workspaces?page=view";
	}

	if (_this == null) {
		return _this = {
			init : init,
			resize : resize
		}
	} else {
		return _this;
	}

}
