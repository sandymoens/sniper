function UserController(messenger) {

	var _this;
	var _messenger = messenger;

	function init() {
		_getLinks();

		_bindEvents();
	}

	function _getLinks() {
		ajaxGet({
			url : "links",
			success : _setResource,
			fail : function() {
				_messenger.error("An error occurred while retrieving links!");
			}
		});
	}

	function _setResource(resource) {
		_this._linksResource = resource;

		_getUserInfo();
	}

	function _getUserInfo() {
		var url = getLink(_this._linksResource, KEY_USER_INFO);

		ajaxGet({
			url : url,
			success : function(resource) {
				$("#userInfo").append(
						$(Handlebars.getTemplate("ui.userInfo")(resource)));
			},
			fail : function() {
				_messenger
						.error("An error occurred while retrieving user info");
			}
		});
	}

	function _bindEvents() {
		$("#userInfo").on({
			click : _resetUserInfo
		}, "#cancelButton");

		$("#userInfo").on({
			click : _saveUserInfo
		}, "#saveButton");
	}

	function _resetUserInfo() {
		$("#password").val("");
		$("#passwordConfirm").val("");
	}

	function _saveUserInfo() {
		var password = $("#password").val();
		var passwordConfirm = $("#passwordConfirm").val();

		if (password != passwordConfirm) {
			_messenger.error("Passwords don't match.");
			return;
		}

		if (password == "") {
			_messenger.info("Nothing to save.");
			return;
		}

		ajaxPatch({
			url : getLink(_this._linksResource, KEY_USER_INFO),
			data : JSON.stringify({
				userType : "userWithPassword",
				password : password,
				passwordConfirm : passwordConfirm
			}),
			contentType : "application/json",
			processData : false,
			success : _userSaveSuccess,
			fail : _userSaveFail
		});
	}

	function _userSaveSuccess() {
		_messenger.success("Updated user info successfully");

		_resetUserInfo();
	}

	function _userSaveFail() {
		_messenger.error("An error occurred while saving user info!");

		_resetUserInfo();
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
