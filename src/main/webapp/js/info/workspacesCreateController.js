function WorkspacesCreateController(messenger) {

	var _this;
	var _messenger = messenger;

	function init() {
		_getLinks();

		_bindEvents();
	}

	function _getLinks() {
		ajaxGet({
			url : "links",
			success : _setResource,
			fail : function() {
				_messenger.error("An error occurred while retrieving links!");
			}
		});
	}

	function _setResource(resource) {
		_this._linksResource = resource;
	}

	function _bindEvents() {
		$("#submit").click(_createNewWorkspace);
	}

	function _bindWorkspacesEvents() {
		$("#viewWorkspacesButton").click(_navigateToViewWorkspaces);
		$("#loadWorkspaceButton").click(_navigateToLoadWorkspace);
	}

	function _createNewWorkspace() {
		if (checkRequiredOptions("#workspaceOptions")) {
			_workspaceCreateFail();
			return;
		}

		ajaxPost({
			url : getLink(_this._linksResource, KEY_WORKSPACES),
			data : _getWorkspace(),
			success : _workspaceCreateSuccess,
			fail : _workspaceCreateFail
		});
	}

	function _getWorkspace() {
		return {
			caption : $("#workspaceCaption").val(),
			description : $("#workspaceDescription").val()
		}
	}

	function _workspaceCreateSuccess(resource) {
		_this._resource = resource;

		_messenger.success("Workspace created successfully");

		$("#mainContent").append(
				$(Handlebars.getTemplate("ui.workspaceCreatedSuccessView")()));

		_bindWorkspacesEvents();
	}

	function _workspaceCreateFail() {
		_messenger.error("An error occurred while creating the workspace!");
	}

	function _navigateToViewWorkspaces() {
		window.location = URL_HANDLE + "workspaces?page=view";
	}

	function _navigateToLoadWorkspace() {
		window.location = URL_HANDLE + "patterns?workspace="
				+ _this._resource.workspaceId + "&worksheet=new";
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
