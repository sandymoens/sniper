function WorkspacesController() {

	var _this;

	function init() {
		_bindEvents();
	}

	function _bindEvents() {
		$("#createWorkspace").click(function() {
			window.location = URL_HANDLE + "workspaces?page=create";
		});

		$("#viewWorkspaces").click(function() {
			window.location = URL_HANDLE + "workspaces?page=view";
		});
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
