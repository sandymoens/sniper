function DataTablesUploadController(messenger) {

	var _this;
	var _messenger = messenger;

	function init() {
		_getLinks();

		_bindEvents();
	}

	function _getLinks() {
		ajaxGet({
			url : "links",
			success : _setResource,
			fail : function() {
				_messenger.error("An error occurred while retrieving links!");
			}
		});
	}

	function _setResource(resource) {
		_this._linksResource = resource;
	}

	function _bindEvents() {
		$("#uploadFileButton").click(_showFileOptions);
		$("#uploadSQLButton").click(_showSQLOptions);
		$("#fileUploadForm").submit(_uploadFromFile);
		$("#sqlUploadForm").submit(_uploadFromSQL);
	}

	function _bindDataTableEvents() {
		$("#viewDataTableButton").click(_navigateToViewDataTable);
		$("#createSchemeButton").click(_navigateToCreateScheme);
	}

	function _showFileOptions() {
		$("#uploadFileLabel").addClass("active");
		$("#uploadSQLLabel").removeClass("active");
		$("#fileOptions").show();
		$("#sqlOptions").hide();
	}

	function _showSQLOptions() {
		$("#uploadFileLabel").removeClass("active");
		$("#uploadSQLLabel").addClass("active");
		$("#fileOptions").hide();
		$("#sqlOptions").show();
	}

	function _uploadFromFile(e) {
		e.preventDefault();

		if (checkRequiredOptions("#fileUploadForm")) {
			_dataTableUploadFail();
			return;
		}

		var url = buildUrl(getLink(_this._linksResource, KEY_DATATABLES), [ {
			key : KEY_TYPE,
			value : KEY_FILE
		} ]);

		ajaxPost({
			url : url,
			success : function(resource) {
				var postData = new FormData();
				postData.append(KEY_FILE, $("#fileToUpload")[0].files[0]);
				postData.append(KEY_CAPTION, $("#fileDataTableCaption").val());
				postData.append(KEY_DESCRIPTION, $("#fileDataTableDescription")
						.val());

				ajaxPost({
					url : getLink(resource, KEY_LINK),
					data : postData,
					contentType : false,
					processData : false,
					success : _dataTableUploadSuccess,
					fail : _dataTableUploadFail
				});
			},
			fail : _dataTableUploadFail
		});
	}

	function _uploadFromSQL(e) {
		e.preventDefault();

		if (checkRequiredOptions("#sqlUploadForm")) {
			_dataTableUploadFail();
			return;
		}

		var url = buildUrl(getLink(_this._linksResource, KEY_DATATABLES), [ {
			key : KEY_TYPE,
			value : KEY_SQL
		} ]);

		ajaxPost({
			url : url,
			success : function(resource) {
				var postData = new FormData();
				postData.append(KEY_CAPTION, $("#sqlDataTableCaption").val());
				postData.append(KEY_DESCRIPTION, $("#sqlDataTableDescription")
						.val());
				postData.append(KEY_HOSTNAME, $("#sqlHostName").val());
				postData.append(KEY_DATABASE, $("#sqlDatabase").val());
				postData.append(KEY_TABLE, $("#sqlTable").val());
				postData.append(KEY_USERNAME, $("#sqlUsername").val());
				postData.append(KEY_PASSWORD, $("#sqlPassword").val());

				ajaxPost({
					url : getLink(resource, KEY_LINK),
					data : postData,
					contentType : false,
					processData : false,
					success : _dataTableUploadSuccess,
					fail : _dataTableUploadFail
				});
			},
			fail : _dataTableUploadFail
		});
	}

	function _dataTableUploadSuccess(resource) {
		_this._resource = resource;

		_messenger.success("Data uploaded successfully");

		$("#mainContent").append(
				$(Handlebars.getTemplate("ui.dataTableUploadSuccessView")()));

		_bindDataTableEvents();
	}

	function _dataTableUploadFail() {
		_messenger.error("An error occurred while uploading the data!");
	}

	function _navigateToViewDataTable() {
		window.location = URL_HANDLE + "dataTables?page=view&id="
				+ _this._resource.dataTableId;
	}

	function _navigateToCreateScheme() {
		window.location = URL_HANDLE + "schemes?page=create&id="
				+ _this._resource.dataTableId;
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
