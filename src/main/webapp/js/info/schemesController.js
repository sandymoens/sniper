function SchemesController() {

	var _this;

	function init() {
		_bindEvents();
	}

	function _bindEvents() {
		$("#createScheme").click(function() {
			window.location = URL_HANDLE + "schemes?page=create";
		});

		$("#viewSchemes").click(function() {
			window.location = URL_HANDLE + "schemes?page=view";
		});
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
