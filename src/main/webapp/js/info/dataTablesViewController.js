function DataTablesViewController(messenger) {

	var _this;
	var _messenger = messenger;

	function init() {
		_getLinks();

		_bindEvents();
	}

	function _getLinks() {
		ajaxGet({
			url : "links",
			success : _setResource,
			fail : _failed
		});
	}

	function _setResource(resource) {
		_this._linksResource = resource;

		_getDataTables();
	}

	function _failed() {
		alert("An error occurred while retrieving links!");
	}

	function _getDataTables() {
		ajaxGet({
			url : getLink(_this._linksResource, KEY_DATATABLES),
			success : _setDataTables,
			fail : function() {
				_messenger
						.error("An error occurred while retrieving data tables");
			}
		});
	}

	function _setDataTables(resource) {
		var options = "<option/>";

		for (var i = 0, end = resource.resources.length; i < end; i++) {
			options += "<option value='" + resource.resources[i].dataTableId
					+ "'>" + resource.resources[i].caption + '</option>';
		}

		$("#dataTableSelect").html(options);
		$("#dataTableSelect").trigger("chosen:updated");

		var id = $.urlParam("id");

		if (id !== null) {
			$("#dataTableSelect").val(id);
			$("#dataTableSelect").trigger("chosen:updated");

			_updateDataTableInfo();
		}
	}

	function _bindEvents() {
		$("#dataTableSelect").on({
			change : _updateDataTableInfo
		});

		$("#dataTableDelete").on({
			click : _deleteDataTable
		});

		$("#createSchemeButton").click(_navigateToCreateScheme);
	}

	function _updateDataTableInfo() {
		var dataTableId = $("#dataTableSelect").val();

		if (dataTableId == null) {
			return;
		}

		ajaxGet({
			url : getLink(_this._linksResource, KEY_DATATABLES) + "/"
					+ dataTableId,
			success : _setDataTable,
			fail : function() {
				_messenger.error("An error occurred while fetching data table");
				$("#dataTableInfo").hide();
			}
		});
	}

	function _setDataTable(resource) {
		_this._resource = resource;

		$("#dataTableInfo").show();
		$("#dataTableCaption").html(resource.caption);
		$("#dataTableDescription").html(resource.description);

	}

	function _deleteDataTable() {
		if (confirm("Are you sure you wish to delete this data table? Removing this table will remove all schemes and worksheets to which the data table is attached")) {
			var link = getLink(_this._resource, KEY_SELF);

			ajaxDelete({
				url : link,
				success : function() {
					_getDataTables();

					$("#dataTableInfo").hide();
					$("#dataTableCaption").html("");
					$("#dataTableDescription").html("");

					_messenger.success("Data table deleted successfully");
				},
				fail : function() {
					_messenger
							.error("An error occurred while deleting the data table");
				}
			});
		}
	}

	function _navigateToCreateScheme() {
		window.location = URL_HANDLE + "schemes?page=create&id="
				+ _this._resource.dataTableId;
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
