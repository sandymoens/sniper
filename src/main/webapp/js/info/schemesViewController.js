function SchemesViewController(messenger) {

	var _this;
	var _messenger = messenger;

	function init() {
		_getLinks();

		resize();

		_bindEvents();
	}

	function _getLinks() {
		ajaxGet({
			url : "links",
			success : _setResource,
			fail : _failed
		});
	}

	function _setResource(resource) {
		_this._linksResource = resource;

		_getSchemes();
	}

	function _failed() {
		alert("An error occurred while retrieving links!");
	}

	function _getSchemes() {
		ajaxGet({
			url : getLink(_this._linksResource, KEY_SCHEMES),
			success : _setSchemes,
			fail : function() {
				_messenger.error("An error occurred while retrieving schemes");
			}
		});
	}

	function _setSchemes(resource) {
		var options = "<option/>";

		for (var i = 0, end = resource.resources.length; i < end; i++) {
			options += "<option value='" + resource.resources[i].schemeId
					+ "'>" + resource.resources[i].caption + '</option>';
		}

		$("#schemeSelect").html(options);
		$("#schemeSelect").trigger("chosen:updated");

		var id = $.urlParam("id");

		if (id !== null) {
			$("#schemeSelect").val(id);
			$("#schemeSelect").trigger("chosen:updated");

			_updateSchemeInfo();
		}
	}

	function _bindEvents() {
		$("#schemeSelect").on({
			change : _updateSchemeInfo
		});

		$("#schemeDelete").on({
			click : _deleteScheme
		});

		$("#viewDataTable").on({
			click : _navigateToViewDataTable
		})

		$(window).resize(debouncer(resize));
	}

	function resize() {
		var width = $(window).width();
		width -= $(".sidebar").width();
		width -= 45;

		$("#dataView").width(width);
	}

	function _updateSchemeInfo() {
		var schemeId = $("#schemeSelect").val();

		if (schemeId == null) {
			return;
		}

		ajaxGet({
			url : getLink(_this._linksResource, KEY_SCHEMES) + "/" + schemeId,
			success : function(resource) {
				_this._resource = resource;
				console.log(_this._resource);

				$("#schemeInfo").show();
				$("#schemeCaption").html(resource.caption);
				$("#schemeDescription").html(resource.description);
				$("#dataTableCaption").html(resource.dataTable.caption);
				$("#dataTableDescription").html(resource.dataTable.description);

				_getFirstLines();
			},
			fail : function() {
				_messenger.error("An error occurred while fetching schemes");
				$("#schemeInfo").hide();
			}
		});
	}

	function _getFirstLines() {
		var url = buildUrl(getLink(_this._resource.dataTable, KEY_ROWS), [ {
			key : KEY_PAGE,
			value : 0
		}, {
			key : KEY_SIZE,
			value : 10
		} ]);

		ajaxGet({
			url : url,
			success : function(resource) {
				_this._rowsResource = resource;

				_printScheme();
			}
		});
	}

	function _printScheme() {
		var previewTable = {};

		if (_this._resource.schemeMetaInfo.schemeType === "dataTable") {
			_setDataTableOptions();
			previewTable.attributes = _this._resource.schemeMetaInfo.attributes;
			previewTable.rows = _this._resource.rows;
		} else if (_this._resource.schemeMetaInfo.schemeType === "transactions") {
			_setTransactionsOptions();
			previewTable.rows = _this._resource.rows;
		} else if (_this._resource.schemeMetaInfo.schemeType === "textAsTransactions") {
			_setTextAsTransactionsOptions();
			previewTable.rows = _this._resource.rows;
		} else if (_this._resource.schemeMetaInfo.schemeType === "textAsBowTfIdf") {
			_setTextAsBowTfIdfOptions();
			previewTable.rows = _this._resource.rows;
		}

		$("#dataView").html(
				$(Handlebars.getTemplate("ui.previewTable")(previewTable)));
	}

	function _setDataTableOptions() {
		$("#dataTableOptions").show();
		$("#transactionsOptions").hide();
		$("#textOptions").hide();
	}

	function _setTransactionsOptions() {
		$("#dataTableOptions").hide();
		$("#transactionsOptions").show();
		$("#textOptions").hide();
	}

	function _setTextAsTransactionsOptions() {
		$("#dataTableOptions").hide();
		$("#transactionsOptions").hide();
		$("#textAsTransactionsOptions").show();
		$("#textAsBowTfIdfOptions").hide();
		$("#textOptions").show();

		$("#textTypeOfTransactions").html("Transactions");
		$("#textRecognizeSentences").html(
				_this._resource.schemeMetaInfo.recognizeSentences ? "True"
						: "False");
		$("#textAlphabeticalOnly").html(
				_this._resource.schemeMetaInfo.alphabeticalOnly ? "True"
						: "False");
		$("#textFilterPOS").html(
				_this._resource.schemeMetaInfo.filterPOS ? "True" : "False");
		$("#textStemTokens").html(
				_this._resource.schemeMetaInfo.stemTokens ? "True" : "False");
		if (_this._resource.schemeMetaInfo.dFFilterType == "NONE") {
			$("#textFilterByDF").html("None");
		} else if (_this._resource.schemeMetaInfo.dFFilterType == "TOPX") {
			$("#textFilterByDF").html("Top X");
		} else if (_this._resource.schemeMetaInfo.dFFilterType == "MINIMUMDF") {
			$("#textFilterByDF").html("Minimum DF");
		}
		$("#textDFFilterValue").html(
				_this._resource.schemeMetaInfo.dFFilterValue);
	}
	function _setTextAsBowTfIdfOptions() {
		$("#dataTableOptions").hide();
		$("#transactionsOptions").hide();
		$("#textAsTransactionsOptions").hide();
		$("#textAsBowTfIdfOptions").show();
		$("#textOptions").show();

		$("#textTypeOfTransactions").html("Bag of words TF*IDF");
		$("#textRecognizeSentences").html(
				_this._resource.schemeMetaInfo.recognizeSentences ? "True"
						: "False");
		$("#textAlphabeticalOnly").html(
				_this._resource.schemeMetaInfo.alphabeticalOnly ? "True"
						: "False");
		$("#textFilterPOS").html(
				_this._resource.schemeMetaInfo.filterPOS ? "True" : "False");
		$("#textStemTokens").html(
				_this._resource.schemeMetaInfo.stemTokens ? "True" : "False");
		$("#textNumberOfBins")
				.html(_this._resource.schemeMetaInfo.numberOfBins);
	}

	function _deleteScheme() {
		if (confirm("Are you sure you wish to delete this scheme? Removing this scheme will remove all worksheets to which the scheme is attached")) {
			var link = getLink(_this._resource, KEY_SELF);

			ajaxDelete({
				url : link,
				success : function() {
					_getSchemes();

					$("#schemeInfo").hide();
					$("#schemeCaption").html("");
					$("#schemeDescription").html("");
					$("#dataTableCaption").html("");
					$("#dataTableDescription").html("");
					$("#dataView").html("");

					_messenger.success("Scheme deleted successfully");
				},
				fail : function() {
					_messenger
							.error("An error occurred while deleting the scheme");
				}
			});
		}
	}

	function _navigateToViewDataTable() {
		window.location = URL_HANDLE + "dataTables?page=view&id="
				+ _this._resource.dataTable.dataTableId;
	}

	if (_this == null) {
		return _this = {
			init : init,
			resize : resize
		}
	} else {
		return _this;
	}

}
