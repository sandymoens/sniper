function UserAdminController(messenger) {

	var _this;
	var _messenger = messenger;

	function init() {
		_getLinks();

		_bindEvents();
	}

	function _getLinks() {
		ajaxGet({
			url : "links",
			success : _setResource,
			fail : function() {
				_messenger.error("An error occurred while retrieving links!");
			}
		});
	}

	function _setResource(resource) {
		_this._linksResource = resource;

		_getRoles().then(_getUserInfos).then(_buildTable);
	}

	function _getRoles() {
		return ajaxGet({
			url : getLink(_this._linksResource, KEY_ROLES),
			success : function(resource) {
				_this.rolesResource = resource;
			},
			fail : function() {
				_messenger.error("An error occurred while retrieving roles");
			}
		});
	}

	function _getUserInfos() {
		return ajaxGet({
			url : getLink(_this._linksResource, KEY_USERS),
			success : function(resource) {
				_this.usersResource = resource;
			},
			fail : function() {
				_messenger
						.error("An error occurred while retrieving user info");
			}
		});
	}
	function _buildTable() {
		var roles =  _this.rolesResource.map(role => role.name);
		var users = _getUsers(roles);
		var tableData = {
			roles : roles,
			users : users
		};
		
		console.log(tableData);
		$("#mainContent").append(
				$(Handlebars.getTemplate("ui.userTable")(tableData)));
		
		$("#userTable tr[data-enabled='true'] input, #userTable tr[data-enabled='true'] button").prop("disabled", "true");
	}
	
	function _getUsers(roles) {
		return _this.usersResource.map((user, ix) => function() {
			console.log(user);
			return {
				userName: user.userName,
				roles: roles.map(role => user.roles.includes(role)),
				enabled: user.enabled,
				ix: ix,
				editing_enabled: user.userName != "root"
			}
		}());
	}

	function _bindEvents() {
		$("#mainContent").on({
			click : _saveUser
		}, ".saveButton");

		$("#mainContent").on({
			click : _deleteUser
		}, ".deleteButton");
	}
	
	function _saveUser() {
		var tr = $(this).closest("tr");
		var roleNames = tr.find("input:not(:last)").map(function() {return $(this).prop("checked");}).toArray();

		ajaxPatch({
			url : _this.usersResource[tr.attr("data-ix")].links[0].href,
			data:  JSON.stringify({
				userType : "userWithAuthorities",
				roles : _getRoleNames(roleNames),
				enabled : tr.find("input:last").prop("checked")
			}),
			contentType : "application/json",
			processData : false,
			success : _userSaveSuccess,
			fail : _userSaveFail
		});
	}
	
	function _getRoleNames(enabled) {
		var roles = [];
		
		for(var i = 0; i < _this.rolesResource.length; i++) {
			if(enabled[i]) {
				roles.push(_this.rolesResource[i].name);
			}
		}
			
		return roles;
	}
	
	
	function _deleteUser() {
		var tr = $(this).closest("tr");
		
		ajaxDelete({
			url : _this.usersResource[tr.attr("data-ix")].links[0].href,
			success : function() {
				tr.remove();
				_messenger.success("Deleted user successfully");
			},
			fail : function() {
				_messenger
						.error("An error occurred while deleting user");
			}
		});
	}

	function _userSaveSuccess() {
		_messenger.success("Updated user info successfully");
	}

	function _userSaveFail() {
		_messenger.error("An error occurred while saving user info!");
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
