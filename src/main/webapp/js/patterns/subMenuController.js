function SubMenuController(globalController, measuresController,
		visualizationsController, workDockController) {

	var FORM_ID = "settingsParametersForm";
	var SAVE_SETTINGS_BTN_CLASS = "saveSettings";

	var _this;

	var _globalController = globalController;
	var _measuresController = measuresController;
	var _visualizationsController = visualizationsController;
	var _workDockController = workDockController;
	var _minersController = new MinersController(workDockController);
	var _patternCollectionPostProcessorsController = new PatternCollectionPostProcessorsController(
			workDockController);

	function init(resource) {
		_bindEvents();

		_this._worksheetResource = resource;

		_minersController.init(resource);
		_patternCollectionPostProcessorsController.init(resource);

		$("#subMenu .sm").smartmenus({
			mainMenuSubOffsetX : -20,
			mainMenuSubOffsetY : 15,
			showTimeout : 0,
			subMenusSubOffsetX : 6,
			subMenusSubOffsetY : -6
		});
	}

	function _bindEvents() {
		$("#worksheetSettingsButton").click(_worksheetSettings);

		$("#userInputModal").on({
			click : _saveSettings
		}, "." + SAVE_SETTINGS_BTN_CLASS);

		$("#savePatternsButton").click(_savePatterns);

		$("#measureOptionButton").click(function() {
			globalController.createMeasureCheckBoxes("#Measurements");
			$("#measureModal").modal();
		});

		$("#measuresDeselectAll").click(function() {
			_measuresController.deselectAllMeasures("#Measurements");
		});

		$("#measuresSelectAll").click(function() {
			_measuresController.selectAllMeasures("#Measurements");
		});

		$("#visualizationOptionButton").click(function() {
			globalController.createVisualizationCheckboxes("#Visualizations");
			$("#visualizationModal").modal();
		});

		$("#visualisationDeselectAll").click(
				function() {
					_visualizationsController
							.deselectAllVisualizations("#Visualizations");
				});

		$("#visualisationSelectAll").click(
				function() {
					_visualizationsController
							.selectAllVisualizations("#Visualizations");
				});

		$("#downloadPatternsButton").click(_downloadPatterns);
	}

	function _worksheetSettings() {
		if (_this._worksheetResource == undefined) {
			messenger.error("Invalid worksheet");
			return;
		}

		ajaxGet({
			url : getLink(_this._worksheetResource, KEY_PROPERTIES),
			success : function(resource) {
				$("#userInputModalHeader").empty().append("Worksheet settings");

				_setSettingParameters(resource);

				$("#userInputModal").modal();
			},
			fail : function() {
				messenger
						.error("An error occurred while getting worksheet properties");
			}
		});
	}

	function _setSettingParameters(resource) {
		$("#userInputModalBody").empty().append(
				$(Handlebars.getTemplate("ui.formWithHelp")({
					formId : FORM_ID,
					inputs : prepareParameters(resource),
					action : "Save settings",
					buttonClass : SAVE_SETTINGS_BTN_CLASS
				})));

		$("#userInputModal .chosen-select").chosen();
	}

	function _saveSettings() {
		var parameters = {};

		$("#" + FORM_ID + " input[name], #" + FORM_ID + " select[name]").each(
				function() {
					parameters[$(this).attr("name")] = $(this).val();
				});

		ajaxPut({
			url : getLink(_this._worksheetResource, KEY_PROPERTIES),
			data : JSON.stringify(parameters),
			contentType : "application/json",
			processData : false,
			success : function() {
				messenger.success("Settings saved successfully");
				_workDockController.redrawPatterns();
			},
			fail : function() {
				messenger
						.error("A problem occurred while updating miner parameters");
			}
		});
	}

	function _savePatterns() {
		if (_this._worksheetResource != undefined) {

			var data = {};
			data[KEY_TYPE] = "saveWorksheet";
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : function() {
					messenger.success("Worksheet saved successfully");
				},
				fail : function() {
					messenger
							.error("An error occurred while saving the worksheet");
				}
			});
		}
	}

	function _downloadPatterns() {
		if (_this._worksheetResource != undefined) {
			ajaxGet({
				url : getLink(_this._worksheetResource, KEY_DOWNLOAD_PATTERNS),
				success : function(data) {
					saveFile("patterns.txt", data);
				},
				fail : function() {
					messenger
							.error("An error occurred while downloading patterns");
				}
			});
		}
	}

	if (_this == null) {
		return _this = {
			init : init,
		}
	} else {
		return _this;
	}

}