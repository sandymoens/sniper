function GlobalController() {

	var _this;

	function init() {
		_this._selectionModel = new SelectionModel();
		_this._measuresController = new MeasuresController();
		_this._visualizationsController = new VisualizationsController();
		_this._workDockController = new WorkDockController(
				_this._selectionModel, _this._measuresController,
				_this._visualizationsController);
		_this._workDockMenuController = new WorkDockMenuController(
				_this._workDockController);
		_this._sourceDockController = new SourceDockController(
				_this._selectionModel);
		_this._subMenuController = new SubMenuController(_this,
				_this._measuresController, _this._visualizationsController,
				_this._workDockController);
		_this._tabsController = new TabsController();

		graphGlobalController = this;

		_bindEvents();

		_getLinks();
	}

	function _getLinks() {
		ajaxGet({
			url : "links",
			success : _setResource,
			fail : _failed
		});
	}

	function _setResource(resource) {
		_this._linksResource = resource;

		_getWorksheet();
	}

	function _failed() {
		alert("An error occurred while retrieving links!");
	}

	function _getWorksheet() {
		if (worksheetId != "new") {
			ajaxGet({
				url : getLink(_this._linksResource, KEY_WORKSHEETS) + "/"
						+ worksheetId,
				success : _setWorksheetResource,
				fail : _worksheetLoadFailed
			});
		}
	}

	function _bindEvents() {
		$(window).resize(debouncer(_resizeDocks));

		$(".modal").on({
			mouseover : _updateHelpBox
		}, "*[parameterhint]");
	}

	function _updateHelpBox() {
		$("#parameterHelpBox").empty().append(
				$(Handlebars.getTemplate("ui.helpBoxMessage")({
					parameterName : $(this).attr("parametername"),
					parameterHelp : $(this).attr("parameterhelp"),
					parameterHint : $(this).attr("parameterhint")
				})));
	}

	function _resizeDocks() {
		_this._sourceDockController.resize();
		_this._workDockController.resize();
	}

	function _setWorksheetResource(worksheetResource) {
		_this._worksheetResource = worksheetResource;

		_this._sourceDockController.init(worksheetResource);
		_this._workDockMenuController.init(worksheetResource);
		_this._workDockController.init(worksheetResource);
		_this._measuresController.init(worksheetResource);
		_this._visualizationsController.init(worksheetResource);
		_this._subMenuController.init(worksheetResource);
		_this._tabsController.init(worksheetResource);

		_resizeDocks();
	}

	function _worksheetLoadFailed(response) {
		messenger.error(response.responseJSON.error + "<br>"
				+ response.responseJSON.message);
	}

	function deleteWorksheet(callback) {
		ajaxDelete({
			url : getLink(_this._worksheetResource, KEY_SELF),
			success : function() {
				messenger.success("Worksheet deleted successfully!");
				callback();
			},
			fail : function() {
				messenger
						.error("A problem occurred while deleting the worksheet!");
			}
		});
	}

	function createMeasureCheckBoxes(id) {
		_this._measuresController.createMeasureCheckBoxes(id);
	}

	function saveMeasureActivity(id) {
		_this._measuresController.saveMeasureActivity(id);
	}

	function createVisualizationCheckboxes(id) {
		_this._visualizationsController.createVisualizationCheckboxes(id);
	}

	function saveVisualizationActivity(id) {
		_this._visualizationsController.saveVisualizationActivity(id);
	}

	function workDockController() {
		return _this._workDockController;
	}

	if (_this == null) {
		return _this = {
			init : init,

			deleteWorksheet : deleteWorksheet,

			createMeasureCheckBoxes : createMeasureCheckBoxes,
			saveMeasureActivity : saveMeasureActivity,
			createVisualizationCheckboxes : createVisualizationCheckboxes,
			saveVisualizationActivity : saveVisualizationActivity,

			workDockController : workDockController
		}
	} else {
		return _this;
	}

}
