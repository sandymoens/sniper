function WorkDockMenuController(workDockController) {

	var _this;

	var _workDockController = workDockController;
	var _patternPostProcessorsController = new PatternPostProcessorsController(
			workDockController);

	function init(worksheetResource) {
		if (worksheetResource == null) {
			messenger
					.error("A problem occurred while initializing the work dock menu!");
			return;
		}

		_this._worksheetResource = worksheetResource;

		_patternPostProcessorsController.init(worksheetResource);

		$("#workDockMenu .sm").smartmenus({
			mainMenuSubOffsetX : -20,
			mainMenuSubOffsetY : 15,
			showTimeout : 0,
			subMenusSubOffsetX : 6,
			subMenusSubOffsetY : -6
		});

		_bindEvents();

		_redrawPattern();
	}

	function _bindEvents() {
		if (_this._eventsBound) {
			return;
		}

		$("#clearSelectedPatternsButton").on({
			click : _workDockController.clearSelectedPatterns
		});

		$("#clearAllPatternsButton").on({
			click : _workDockController.clearAllPatterns
		});

		$("#undoButton").on({
			click : _undo
		});

		$("#redoButton").on({
			click : _redo
		});

		$("#patternPageLinks").on({
			click : _workDockController.setPatternPage
		}, "[href]");

		$("#filterPatternsButton").on({
			click : _toggleFilterView
		});

		$("#copyToClipboardButton").on({
			click : _copyToClipboard
		});

		$("#pasteFromClipboardButton").on({
			click : _pasteFromClipboard
		});

		$("#filterView").on({
			mouseenter : function() {
				if ($(this).html() != "") {
					return;
				}

				if ($(".ui-draggable-dragging .pattern").length > 0) {
				} else if ($(".ui-draggable-dragging").length > 0) {
					$(this).addClass("droppable");
				}
			},
			mouseleave : function() {
				$(this).removeClass("droppable");
			},
		});

		$("#filterView").on({
			click : _buildElementClicked
		}, ".buildElement");

		$("#filterView").on({
			mouseenter : function() {
				if ($(".ui-draggable-dragging .pattern").length > 0) {
				} else if ($(".ui-draggable-dragging").length > 0) {
					$(this).addClass("droppable");
				}
			},
			mouseleave : function() {
				$(this).removeClass("droppable");
			},
		}, ".extensionDescriptor, .elContainer");

		$("#filterView").droppable({
			drop : _dropEventHandler,
			tolerance : "pointer"
		});

		$("#filterTypeIdentifierSelect").on({
			change : _changeFilterTypeIdentifier
		});

		$("#clearFilterViewButton").on({
			click : _clearFilterView
		});

		_this._eventsBound = true;
	}

	function _buildElementClicked(e) {
		if (e.which == 2 || (e.which == 1 && e.altKey)) {
			_removeFromDescriptor($(this));
		}
	}

	function _toggleFilterView() {
		$("#filterViewContainer").toggle();
		$(window).trigger('resize');
	}

	function _toggleFilterPatternsButtonSelected() {
		if ($("#filterView").html() == "") {
			$("#filterPatternsButton").removeClass("selected");
		} else {
			$("#filterPatternsButton").addClass("selected");
		}
	}

	function _getFilterParameters() {
		ajaxGet({
			url : getLink(_this._worksheetResource, KEY_FILTER_PARAMETERS),
			success : function(resource) {
				var select = $("#filterTypeIdentifierSelect");

				select.find(
						"option[value='" + resource["map"]["typeSelectorType"]
								+ "']").prop("selected", true);
				select.trigger('chosen:updated')
			},
			fail : function() {
				messenger
						.error("A problem occurred while getting filter parameters");
			}
		});
	}

	function _changeFilterTypeIdentifier() {
		console
				.log(getLink(_this._worksheetResource, KEY_FILTER_PARAMETERS)
						+ "?typeSelectorType="
						+ $("#filterTypeIdentifierSelect").val());
		ajaxPut({
			url : getLink(_this._worksheetResource, KEY_FILTER_PARAMETERS)
					+ "?typeSelectorType="
					+ $("#filterTypeIdentifierSelect").val(),
			success : function() {
				console.log("REDRAW");
				_redrawPattern();

				_workDockController.redrawPatterns();
			},
			fail : function() {
				messenger
						.error("A problem occurred while setting filter parameters");

				_getFilterParameters();
			}
		});
	}

	function _clearFilterView() {
		var data = {};
		data[KEY_TYPE] = "clearFilterPattern";

		ajaxPost({
			url : getLink(_this._worksheetResource, KEY_ACTIONS),
			data : JSON.stringify(data),
			contentType : "application/json",
			processData : false,
			success : function() {
				_redrawPattern();

				_workDockController.redrawPatterns();
			},
			fail : function() {
				messenger.error("An error occurred");
			}
		});
	}

	function _getDropData(event, dropData) {
		var toElement = document.elementFromPoint(event.clientX, event.clientY);

		var data = {};

		var elContainer = $(toElement).closest(".elContainer");
		var edContainer = $(toElement).closest(".extensionDescriptor");

		var type = dropData.elements[0].type == "item" ? "proposition"
				: dropData.elements[0].type;
		var ids = dropData.elements.map(function() {
			return this.id;
		}).toArray();

		if (edContainer.length == 1) {
			data[KEY_TYPE] = "addToFilterDescriptor";
			data[KEY_DESCRIPTOR_ID] = $(edContainer[0]).attr("descriptorid");
			data[KEY_ELEMENT_TYPE] = type;
			data[KEY_IDS] = ids;
		} else if (elContainer.length == 1) {
			data[KEY_TYPE] = "addToFilterDescriptor";
			data[KEY_DESCRIPTOR_ID] = $(elContainer[0]).attr("descriptorid");
			data[KEY_ELEMENT_TYPE] = type;
			data[KEY_IDS] = ids;
		} else {
			var pattern = $(toElement).closest(".pattern");

			data[KEY_TYPE] = "createNewFilterPattern";
			data[KEY_ELEMENT_TYPE] = type;
			data[KEY_IDS] = ids;
		}

		return data;
	}

	function _dropEventHandler(event, ui) {
		var data = ui.draggable.data("data");

		var dropData;

		if (data.func !== undefined) {
			dropData = data.func();
		}

		if (_this._worksheetResource != undefined) {
			var dropData = _getDropData(event, dropData);

			if (dropData == null) {
				return;
			}

			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(dropData),
				contentType : "application/json",
				processData : false,
				success : function() {
					_redrawPattern();

					if (data.callback != null) {
						data.callback();
					}

					_workDockController.redrawPatterns();
				},
				fail : function() {
					messenger
							.error("An error occurred while dropping an element");
				}
			});
		}
	}

	function _removeFromDescriptor(element) {
		var data = {}
		data[KEY_TYPE] = "removeFromFilterDescriptor";
		data[KEY_DESCRIPTOR_ID] = element.closest(".elContainer[descriptorId]")
				.attr("descriptorId");
		data[KEY_ELEMENT_TYPE] = element.attr("type") == "item" ? "proposition"
				: element.attr("type");
		data[KEY_ID] = element.attr("id");

		if (_this._worksheetResource != undefined) {
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : function() {
					_redrawPattern();

					if (data.callback != null) {
						data.callback();
					}

					_workDockController.redrawPatterns();
				},
				fail : function() {
					messenger.error("fail");
				}
			});
		}
	}

	function _redrawPattern() {
		if (_this._worksheetResource != undefined) {
			ajaxGet({
				url : getLink(_this._worksheetResource, KEY_FILTER_PATTERN),
				success : _drawPattern,
				fail : function() {
					messenger
							.error("A problem occurred while fetching filter pattern");
				}
			});
		}
	}

	function _drawPattern(resource) {
		$("#filterView")
				.html($(Handlebars.getTemplate("ui.pattern")(resource)));

		var descriptor = resource.patternDescriptor;

		if (descriptor !== undefined) {
			if (descriptor.patternType == "Episode") {
				var episode = resource;
				drawEpisode(
						{
							id : episode.patternDescriptor.graph.nodes.id,
							nodes : episode.patternDescriptor.graph.nodes.elements,
							localIds : episode.patternDescriptor.graph.localIds.elements,
							edges : episode.patternDescriptor.graph.edges.elements
						}, this._worksheetResource);
			} else if (descriptor.patternType == "EpisodeRule") {
				drawEpisodeRule(resource, this._worksheetResource);
			}
		}

		_toggleFilterPatternsButtonSelected();
	}

	function _copyToClipboard() {
		const serializablePatterns = $(_workDockController.selectedPatterns())
				.map(function() {
					return createSerializablePattern(this);
				}).toArray();

		if (serializablePatterns.length == 0) {
			messenger.error("Copy failed because no patterns are selected!");
			return;
		}

		copyToClipboard(JSON.stringify(serializablePatterns));

		if (serializablePatterns.length == 1) {
			messenger.success("Copied 1 pattern to clipboard");
		} else {
			messenger.success("Copied " + serializablePatterns.length
					+ " pattern(s) to clipboard");
		}
	}

	function _pasteFromClipboard() {
		readFromClipboard(_pasteFromClipboardCallback);
	}

	function _pasteFromClipboardCallback(value) {
		var data = {}
		data[KEY_TYPE] = "addPattern";
		try {
			data[KEY_PATTERNS] = JSON.parse(value);
		} catch (err) {
			messenger.fail("Invalid data. Could not paste from clipboard.");
			return;
		}

		if (_this._worksheetResource != undefined) {
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : function() {
					if (data.callback != null) {
						data.callback();
					}

					_workDockController.redrawPatterns();

					messenger
							.success("Pasted patterns from clipboard successfully.");
				},
				fail : function() {
					messenger.error("Failed to paste patterns from clipboard.");
				}
			});
		}
	}

	function _undoRedo(value) {
		var data = {};
		data[KEY_TYPE] = "undoRedo";
		data[KEY_VALUE] = value;

		if (_this._worksheetResource != undefined) {
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : _workDockController.redrawPatterns,
				fail : function() {
					messenger.error("fail");
				}
			});
		}
	}

	function _undo() {
		_undoRedo("undo");
	}

	function _redo() {
		_undoRedo("redo");
	}

	if (_this == null) {
		return _this = {
			init : init,
		};
	} else {
		return _this;
	}
}