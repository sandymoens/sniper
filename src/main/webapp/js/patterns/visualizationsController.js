function VisualizationsController() {

	var _this;
	var _observers = [];

	function subscribe(observer) {
		_observers.push(observer);
	}

	function unsubscribe(observer) {
		var index = _observers.indexOf(observer);
		if (index > -1) {
			_observers.splice(index, 1);
		}
	}

	function _notifyObserver(observer) {
		var index = _observers.indexOf(observer);
		if (index > -1) {
			_observers[index].notify(_this);
		}
	}

	function _notifyAll() {
		for (var i = 0; i < _observers.length; i++) {
			_observers[i].notify(_this);
		}
	}

	function init(worksheetResource) {
		if (worksheetResource == null) {
			messenger
					.error("A problem occurred while initializing the visualizations!");
			return;
		}

		_this._worksheetResource = worksheetResource;

		_getVisualizations();
	}

	function _getVisualizations() {
		ajaxGet({
			url : getLink(_this._worksheetResource, KEY_VISUALIZATIONS),
			success : _setVisualizations,
			fail : function() {
				messenger
						.error("A problem occurred while fetching visualisations");
			}
		});
	}

	function _setVisualizations(resource) {
		_this._visualizations = resource;
		_notifyAll();
	}

	function createVisualizationCheckboxes(id) {
		var checkboxes = "";

		var visualizations = $(_this._visualizations).map(function() {
			return {
				name : this.id,
				caption : this.caption,
				enabled : this.isActive
			}
		}).toArray();

		$(id).html($(Handlebars.getTemplate("ui.checkboxes")(visualizations)));
	}

	function selectAllVisualizations(id) {
		$(id).find("input[type='checkbox']").prop('checked', true);
	}

	function deselectAllVisualizations(id) {
		$(id).find("input[type='checkbox']").prop('checked', false);
	}

	function saveVisualizationActivity(id) {
		var visualizations = $(id).find("input[type='checkbox']").map(
				function() {
					return {
						caption : $(this).attr("name"),
						isActive : $(this).is(":checked")
					};
				}).toArray();

		ajaxPut({
			url : getLink(_this._worksheetResource, KEY_VISUALIZATIONS),
			data : JSON.stringify(visualizations),
			contentType : "application/json",
			processData : false,
			success : function(resource) {
				_setVisualizations(resource);
				messenger.success("Visualizations selected successfully");
			},
			fail : function() {
				messenger
						.error("An error occurred while saving visualizations");
			}
		});
	}

	function getActiveVisualizations() {
		return $(_this._visualizations).filter(function() {
			return this.isActive
		}).map(function() {
			return {
				name : this.id,
				caption : this.caption,
			}
		}).toArray();
	}

	if (_this == null) {
		return _this = {
			init : init,
			createVisualizationCheckboxes : createVisualizationCheckboxes,
			selectAllVisualizations : selectAllVisualizations,
			deselectAllVisualizations : deselectAllVisualizations,
			saveVisualizationActivity : saveVisualizationActivity,
			getActiveVisualizations : getActiveVisualizations,
			subscribe : subscribe,
			unsubscribe : unsubscribe
		}
	} else {
		return _this;
	}

}