function WorksheetCreateController() {

	var _this;

	function init(workspaceId) {
		_this._workspaceId = workspaceId;

		resize();

		_getDataTables();

		_bindEvents();
	}

	function _getDataTables() {
		ajaxGet({
			url : "dataTables?fields=schemes",
			success : function(resource) {
				_this._dataTablesResources = resource;

				_this._dataTables = {};

				for (var i = 0; i < resource.resources.length; i++) {
					var dataTableResource = resource.resources[i];
					_this._dataTables[dataTableResource.dataTableId] = dataTableResource;
				}

				var options = "<option/>";

				for (var i = 0, end = resource.resources.length; i < end; i++) {
					options += "<option value='"
							+ resource.resources[i].dataTableId + "'>"
							+ resource.resources[i].caption + '</option>';
				}

				$("#dataTableSelect").html(options);
				$("#dataTableSelect").trigger("chosen:updated");
			},
			fail : function() {
				messenger
						.error("An error occurred while fetching data tables!");
			}
		});
	}

	function _getSchemes() {
		$("#dataView").html("");

		var dataTableResource = _this._dataTables[$("#dataTableSelect").val()];

		if (dataTableResource === undefined) {
			messenger
					.error("An error occurred while fetching schemes for data table!");
			return;
		}

		_this._schemes = {};

		for (var i = 0; i < dataTableResource.schemes.length; i++) {
			var schemeResource = dataTableResource.schemes[i];
			_this._schemes[schemeResource.schemeId] = schemeResource;
		}

		var options = "<option/>";

		for (var i = 0; i < dataTableResource.schemes.length; i++) {
			options += "<option value='"
					+ dataTableResource.schemes[i].schemeId + "'>"
					+ dataTableResource.schemes[i].caption + '</option>';
		}

		$("#schemeSelect").html(options);
		$("#schemeSelect").trigger("chosen:updated");
	}

	function _bindEvents() {
		$("#dataTableSelect").on({
			change : function() {
				$("#worksheetOptions").hide();
				_getSchemes();
			}
		});

		$("#schemeSelect").on({
			change : function() {
				_initOptions();
				_updateSchemeInfo();
			}
		});

		$("#discretization").on({
			change : _changeDiscretizationOptions
		});

		$("#createWorksheetButton").on({
			click : _createWorksheet
		});

		$("#dataTableType").on({
			change : _updateVisibleOptions
		})

		$("#dataView").on({
			change : _showDiscretizationOptions
		}, ".sDiscretization")

		$(".parameterHelp").on({
			mouseenter : _setParameterHelp
		});

		$(window).resize(debouncer(resize));
	}

	function _updateVisibleOptions() {
		var val = $("#dataTableType").val();

		if (val == "DataTable" || val == "Sequence") {
			$("#sequentialDataTableOptions").hide();
		} else if (val == "SequentialDataTable"
				|| val == "DataTableSingleSequence") {
			$("#sequentialDataTableOptions").show();
		}
	}

	function _initOptions() {
		$("#worksheetOptions").show();

		_updateVisibleOptions();
	}

	function resize() {
		var width = $(window).width();
		width -= 45;

		$("#dataView").width(width);
	}

	function _setParameterHelp() {
		$("#parameterHelpBox").html(
				$(Handlebars.getTemplate("ui.helpBoxMessage")({
					parameterName : $(this).attr("parameterName"),
					parameterHelp : $(this).attr("parameterHelp"),
					parameterHint : $(this).attr("parameterHint")
				})));
	}

	function _changeDiscretizationOptions() {
		var val = $(this).val();

		if (val == "EQUIFREQUENT" || val == "EQUIDISTANT") {
			$("#numberOfBinsSpan").show();
			$("#numberOfCutOffPointsSpan").hide();
		} else if (val == "CLUSTERING") {
			$("#numberOfBinsSpan").hide();
			$("#numberOfCutOffPointsSpan").show();
		} else {
			$("#numberOfBinsSpan").hide();
			$("#numberOfCutOffPointsSpan").hide();
		}
	}

	function _showDiscretizationOptions() {
		var val = $(this).val();

		if (val == "EQUIFREQUENT" || val == "EQUIDISTANT") {
			$(this).parent().find(".bins").show();
			$(this).parent().find(".cutOff").hide();
			$(this).parent().find(".intervals").hide();
		} else if (val == "CLUSTERING") {
			$(this).parent().find(".bins").hide();
			$(this).parent().find(".cutOff").show();
			$(this).parent().find(".intervals").hide();
		} else if (val == "FIXED_INTERVALS") {
			$(this).parent().find(".bins").hide();
			$(this).parent().find(".cutOff").hide();
			$(this).parent().find(".intervals").show();
		} else {
			$(this).parent().find(".bins").hide();
			$(this).parent().find(".cutOff").hide();
			$(this).parent().find(".intervals").hide();
		}
	}

	function _updateSchemeInfo() {
		var schemeId = $("#schemeSelect").val();

		if (schemeId == null) {
			messenger.error("An error occurred while fetching scheme info!");
			return;
		}

		var schemeResource = _this._schemes[schemeId];

		ajaxGet({
			url : getLink(schemeResource, "self"),
			success : function(resource) {
				_this._schemeResource = resource;
				_updateOptions();
				_printScheme();
			},
			fail : function() {
				messenger.error("An error occurred while fetching schemes!");
			}
		});
	}

	function _updateOptions() {
		if (_this._schemeResource.schemeMetaInfo.schemeType === "dataTable") {
			$("#dataTableOptionsList").show();

			_updateSequentialDataTableOptions();
		} else {
			$("#dataTableOptionsList").hide();
		}
	}

	function _updateSequentialDataTableOptions() {
		var idAttributes = [];
		var distanceAttributes = [];

		var attributes = _this._schemeResource.schemeMetaInfo.attributes;

		for (var i = 0; i < attributes.length; i++) {
			var attribute = attributes[i];
			var attributeO = {
				caption : attribute.caption,
				value : attribute.caption
			};

			if (attribute.type === "CATEGORIC") {
				idAttributes.push(attributeO);
			} else if (attribute.type === "METRIC" || attribute.type === "DATE") {
				distanceAttributes.push(attributeO);
			}
		}

		$("#idAttribute").html(
				$(Handlebars.getTemplate("ui.selectOptions")(idAttributes)));
		$("#idAttribute").trigger("chosen:updated");

		$("#distanceAttribute").html(
				$(Handlebars.getTemplate("ui.selectOptions")
						(distanceAttributes)));
		$("#distanceAttribute").trigger("chosen:updated");
	}

	function _printScheme() {
		var previewTable = {};

		if (_this._schemeResource.schemeMetaInfo.schemeType === "dataTable") {
			previewTable.attributes = _this._schemeResource.schemeMetaInfo.attributes;
			previewTable.rows = _this._schemeResource.rows;
		} else if (_this._schemeResource.schemeMetaInfo.schemeType === "transactions") {
			previewTable.rows = _this._schemeResource.rows;
		}

		$("#dataView").html(
				$(Handlebars.getTemplate("ui.previewTableWithDiscretization")(
						previewTable)));
	}

	function _createWorksheet() {
		var worksheet = _getWorksheet();

		if (worksheet == null) {
			_worksheetCreateFail();
			return;
		}

		ajaxPost({
			url : "workspaces/" + workspaceId + "/worksheets",
			data : JSON.stringify(worksheet),
			contentType : "application/json",
			processData : false,
			success : _worksheetCreateSuccess,
			fail : _worksheetCreateFail
		});
	}

	function _getWorksheet() {
		var worksheetMetaInfo = _getWorksheetMetaInfo();

		if (worksheetMetaInfo == null) {
			return null;
		}

		var worksheet = {
			caption : $("#worksheetCaption").val(),
			description : $("#worksheetDescription").val(),
			schemeId : $("#schemeSelect").val(),
			worksheetMetaInfo : worksheetMetaInfo
		};

		return worksheet;
	}

	function _getWorksheetMetaInfo() {
		var schemeType = _this._schemeResource.schemeMetaInfo.schemeType;

		if (schemeType === "dataTable") {
			var val = $("#dataTableType").val();
			console.log(val);
			if (val == "DataTable") {
				return _dataTableMetaInfo();
			} else if (val == "SequentialDataTable") {
				return _sequentialDataTableMetaInfo();
			} else if (val == "Sequence") {
				return _sequenceMetaInfo();
			} else if (val == "DataTableSingleSequence") {
				return _dataTableSingleSequenceMetaInfo();
			}

		} else if (schemeType === "text") {
			return {
				type : "text"
			};
		} else if (schemeType === "transactions") {
			return {
				type : "transactions"
			};
		}

		return null;
	}

	function _getGlobalDiscretizationValue() {
		var type = $("#discretization").val();

		if (type == "EQUIFREQUENT" || type == "EQUIDISTANT") {
			return $("#numberOfBins").val();
		} else if (type == "CLUSTERING") {
			return $("#numberOfCutOffPoints").val();
		}

		return "";
	}

	function _getDiscretizationValue(type, selector) {
		if (type == "EQUIFREQUENT" || type == "EQUIDISTANT") {
			return selector.find(".bins input").val();
		} else if (type == "CLUSTERING") {
			return selector.find(".cutOff select").val();
		} else if (type == "FIXED_INTERVALS") {
			return selector.find(".intervals input").val();
		}
		return "";
	}

	function _getMetricDiscretization(discretizationTypeDiv) {
		var type = discretizationTypeDiv.find("select").val();

		if (type == "DEFAULT") {
			return null;
		}

		return {
			"type" : type,
			"value" : _getDiscretizationValue(type, discretizationTypeDiv)
		};
	}

	function _getDateDiscretization(discretizationTypeDiv) {
		var values = discretizationTypeDiv.find("input:checked").map(
				function() {
					return $(this).attr("name");
				}).toArray();

		return {
			"type" : "DATE",
			"value" : values.join(",")
		};
	}

	function _getDiscretizations() {
		var attributes = $(".previewTable thead tr:nth-child(1) th");
		var attributeTypes = $(".previewTable thead tr:nth-child(2) th");
		var discretizationTypes = $(".previewTable thead tr:nth-child(4) th");

		var discretizations = {};

		for (var i = 0; i < discretizationTypes.length; i++) {
			var discretization = null;

			if ($(attributeTypes[i]).text().trim() == "METRIC") {
				discretization = _getMetricDiscretization($(discretizationTypes[i]));
			} else if ($(attributeTypes[i]).text().trim() == "DATE") {
				discretization = _getDateDiscretization($(discretizationTypes[i]));
			}

			if (discretization != null) {
				discretizations[$(attributes[i]).text().trim()] = discretization;
			}
		}

		return discretizations;
	}

	function _dataTableMetaInfo() {
		if (checkRequiredOptions([ "#generalOptions", "#dataTableOptionsList" ])) {
			return null;
		}

		return {
			type : "dataTable",
			discretization : $("#discretization").val(),
			value : _getGlobalDiscretizationValue(),
			discretizations : _getDiscretizations()
		};
	}

	function _sequentialDataTableMetaInfo() {
		if (checkRequiredOptions([ "#generalOptions", "#dataTableOptionsList",
				"#sequentialDataTableOptions" ])) {
			return null;
		}

		return {
			type : "sequentialDataTable",
			discretization : $("#discretization").val(),
			value : _getGlobalDiscretizationValue(),
			discretizations : _getDiscretizations(),
			idAttribute : $("#idAttribute").val(),
			distanceAttribute : $("#distanceAttribute").val(),
			generateSingleSequence : $("#generateSingleSequence")
					.is(":checked")
		};
	}

	function _sequenceMetaInfo() {
		return {
			type : "sequence",
			discretization : $("#discretization").val(),
			value : _getGlobalDiscretizationValue(),
		};
	}

	function _dataTableSingleSequenceMetaInfo() {
		if (checkRequiredOptions([ "#generalOptions", "#dataTableOptionsList",
				"#sequentialDataTableOptions" ])) {
			return null;
		}

		return {
			type : "dataTableSingleSequence",
			discretization : $("#discretization").val(),
			value : _getGlobalDiscretizationValue(),
			discretizations : _getDiscretizations(),
			idAttribute : $("#idAttribute").val(),
			distanceAttribute : $("#distanceAttribute").val()
		};
	}

	function _worksheetCreateSuccess(resource) {
		window.setTimeout(
				function() {
					window.location = URL_HANDLE + "patterns?workspace="
							+ _this._workspaceId + "&worksheet="
							+ resource.worksheetId;
				}, 1000);

		messenger
				.success("Worksheet created succesfully. Worksheet will be loaded");
	}

	function _worksheetCreateFail() {
		messenger.error("An error occurred while creating the worksheet!");
	}

	if (_this == null) {
		return _this = {
			init : init,
			resize : resize
		}
	} else {
		return _this;
	}

}
