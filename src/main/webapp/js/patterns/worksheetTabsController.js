function WorksheetTabsController(globalController) {

	var _this;
	var _globalController = globalController;

	function init() {
		_getWorkspaceInfo();

		_bindEvents();
	}

	function _getWorkspaceInfo() {
		ajaxGet({
			url : "workspaces/" + workspaceId,
			success : function(resource) {
				_this._resource = resource;

				_updateTabs();
			},
			fail : function() {
				messenger
						.error("An error occurred while retrieving workspace information");
			}
		});
	}

	function _bindEvents() {
		$("#worksheetsSelect").on({
			change : _loadWorksheet
		});

		$("#worksheetDelete").on({
			click : _deleteWorksheet
		});

		$("#worksheetAdd").on({
			click : _newWorksheet
		});
	}

	function _loadWorksheet() {
		window.location = URL_HANDLE + "patterns?workspace="
				+ _this._resource.workspaceId + "&worksheet="
				+ $("#worksheetsSelect").val();
	}

	function _loadWorksheet() {
		window.location = URL_HANDLE + "patterns?workspace="
				+ _this._resource.workspaceId + "&worksheet="
				+ $("#worksheetsSelect").val();
	}

	function _deleteWorksheet() {
		_globalController.deleteWorksheet(function() {
			window.location = URL_HANDLE + "patterns?workspace="
					+ _this._resource.workspaceId + "&worksheet=new";
		});
	}

	function _newWorksheet() {
		window.location = URL_HANDLE + "patterns?workspace="
				+ _this._resource.workspaceId + "&worksheet=new";
	}

	function _updateTabs() {
		var options = "<option></option>";

		for (var i = 0; i < _this._resource.worksheets.length; i++) {
			options += "<option value='"
					+ _this._resource.worksheets[i].worksheetId
					+ "'"
					+ (worksheetId === _this._resource.worksheets[i].worksheetId ? " selected"
							: "") + ">" + _this._resource.worksheets[i].caption
					+ "</option>";
		}

		$("#worksheetsSelect").empty().html(options);
		$("#worksheetsSelect").trigger('chosen:updated');
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}
