function SourceDockController(selectionModelObject) {

	var _this;
	
	var _selectionModelObject = selectionModelObject;
	
	var PAGE_SIZE = 100;
	
	function _newCheckElementsTrigger() {
		return "<div class='row'><div style='margin-left: auto; margin-right: auto;'>...</div></div>";
	}

	function init(worksheetResource) {
		if(worksheetResource == null) {
			messenger.error("A problem occurred while initializing the source dock!");
			return;
		}
		
		_this._worksheetResource = worksheetResource;
		
		_this._parameters = {};
		
		var propositionsUrl = getLink(_this._worksheetResource, KEY_PROPOSITIONS);
		
		var sdpt = $("#SourcePropositionsDock");

		if(propositionsUrl != "") {
			_this._parameters.propositions = {
				fetching : false,
				baseUrl : null,
				drawFunction : _drawPropositionsFromData,
				loader : $(_newCheckElementsTrigger()),
				model : [],
				next : null,
				selection : new Set()
			};
		} else {
			sdpt.remove();
			$("#sourceDockMenu li:has(*[href='#propositions'])").remove();
		}
		
		var propositionsUrl2 = getLink(worksheetResource, KEY_PROPOSITIONS2);
		
		var sdpt2 = $("#SourcePropositionsDock2");
		
		if(propositionsUrl2 != "") {
			_this._parameters.propositions2 = {
				fetching : false,
				baseUrl : null,
				drawFunction : _drawPropositionsFromData2,
				loader : $(_newCheckElementsTrigger()),
				model : [],
				next : null,
				selection : new Set()
			};
		} else {
			sdpt2.hide();
			$("#sourceDockMenu *[href='#propositions2']").hide();
		}

		var attributesUrl = buildUrl(getLink(worksheetResource, KEY_ATTRIBUTES),
				[{
					key: KEY_PAGE,
					value: 0
				}, {
					key: KEY_SIZE,
					value: PAGE_SIZE
				}]);
		
		var sdat = $("#SourceAttributesDockTable");
		
		if(attributesUrl != "" && attributesUrl != null) {
			_this._parameters.attributes = {
				time : 0,
				next : attributesUrl,
				loader : $(_newCheckElementsTrigger()),
				drawFunction : _drawAttributesFromData,
				model : [],
				selection : new Set()
			};

			_resetNextAttributesInviewEvent2();
		} else {
			sdat.remove();
			$("#sourceDockMenu li:has(*[href='#attributes'])").remove();
		}
		
		_bindEvents();
		
		$("#sourceDockMenu *[href]").first().click();
		
		ajaxGet({
			url: buildUrl(getLink(worksheetResource, KEY_SORTINGMEASURES)),
			success: _setSortingMeasures,
			fail: function() {
				messenger.error("An error occurred while retrieving measures for sorting.");
			}
		});
	}
	
	function _setSortingMeasures(resource) {		
		var measures = $(resource).map(function() {
			return {
				value: this.id,
				caption: this.caption
			}
		}).toArray();
				
		$("#measureSelect").html($(Handlebars.getTemplate("ui.selectOptions")(measures)));
		$("#measureSelect").trigger("chosen:updated");
		
		$("#measure1Select").html($(Handlebars.getTemplate("ui.selectOptions")(measures)));
		$("#measure1Select").trigger("chosen:updated");
		
		$("#measure2Select").html($(Handlebars.getTemplate("ui.selectOptions")(measures)));
		$("#measure2Select").trigger("chosen:updated");
		
		_setMeasure(resource[0].id);
	}
	
	function _getElementsById(elements, elementsToGet) {
		return $(elementsToGet).map(function() {
			for(var i = 0; i < elements.length; i++) {
				var element = elements[i];
				
				if(element.id == this) {
					return element;
				}
			}
			return null;
		}).toArray();
	}
	
	function _getElementsByName(elements, elementsToGet) {
		return $(elementsToGet).map(function() {
			return {
				name: this,
				id: this
			};
		}).toArray();
	}
	
	function _createDragHelperForPropositions() {
		var elementsToGet = $("#SourcePropositionsDock .buildElement.selected").map(function() {
			return $(this).attr("id");
		}).toArray();
		
		return _createDragHelper("item", _getElementsById(_this._parameters.propositions.model, elementsToGet),
				($("#propositions #addIndividualCheckboxPropositions").is(":checked")));
	}
	
	function _createDragHelperForPropositions2() {
		var elementsToGet = $("#SourcePropositionsDock2 .buildElement.selected").map(function() {
			return $(this).attr("id");
		}).toArray();
		
		return _createDragHelper("item", _getElementsById(_this._parameters.propositions2.model, elementsToGet),
				($("#propositions2 #addIndividualCheckboxPropositions").is(":checked")));
	}
	
	function _createDragHelperForAttributes() {
		var elementsToGet = $("#SourceAttributesDock .buildElement.selected").map(function() {
			return $(this).attr("id");
		}).toArray();

		return _createDragHelper("attribute", _getElementsById(_this._parameters.attributes.model, elementsToGet),
				($("#addIndividualCheckboxAttributes").is(":checked")));
	}
	
	function _createDragHelper(type, elements, singletons) {
		var maxSize = 5;
		
		var elementsToDraw = {
			incomplete : elements.length > maxSize,
			singletons : singletons,
			elements : $(elements).slice(0, maxSize).map(function() {
				return {
					type : type,
					element : this
				}
			}).toArray()
		};
		
		return $("<div class='dragContainer'></div>").append($(Handlebars.getTemplate("ui.buildElementDragHelper")(elementsToDraw))
			.css("pointer-events", "none"));
	}

	function _resetNextPropositionsInviewEvent() {
		$("#SourcePropositionsDock").empty().append(_this._parameters.propositions.loader);
		_this._parameters.propositions.loader.on("inview", _loadNextPropositionsPage);
	}
	
	function _resetNextPropositionsInviewEvent2() {
		$("#SourcePropositionsDock2").empty().append(_this._parameters.propositions2.loader);
		_this._parameters.propositions2.loader.on("inview", _loadNextPropositionsPage2);
	}
	
	function _resetNextAttributesInviewEvent2() {
		$("#SourceAttributesDockTable").empty().append(_this._parameters.attributes.loader);
		_this._parameters.attributes.loader.on("inview", _loadNextAttributesPage);	
	}
	
	function _setMeasure(measure) {
		$("#propositionsLoader").css("visibility", "visible");
		
		ajaxPost({
			url: getLink(_this._worksheetResource, KEY_PROPOSITIONS),
			data: JSON.stringify({
				measure: $("#measureSelect").val(),
				pattern: null,
			}),
			contentType : "application/json;charset=utf-8",
			processData : false,
			success: function(resource, status, response) {
				var url = response.getResponseHeader("Location");
		
				if(url == null || url == "") {
					messenger.error("An error occurred while fetching propositions for selected pattern");
					return;
				}
				
				_this._parameters.propositions.baseUrl = buildUrl(url,
						[{
							key: KEY_PAGE,
							value: 0
						}, {
							key: KEY_SIZE,
							value: PAGE_SIZE
						}]);
				
				_this._parameters.propositions.next = _this._parameters.propositions.baseUrl;
				_this._parameters.propositions.model = [];

				_resetNextPropositionsInviewEvent();

				$("#propositionsLoader").css("visibility", "hidden");
			},
			error: function() {
				messenger.error("An error occurred while fetching propositions for selected pattern");
			}
		});
		
	}
	
	function _createAssociationPart(selection) {
		if(selection.pattern.patternDescriptor.elements == selection.elContainer) {
			return "elements";
		} 
		
		return  null;
	}
	
	function _createAssociationRulePart(selection) {
		if(selection.pattern.patternDescriptor.antecedent == selection.elContainer) {
			return "antecedent";
		} else  if(selection.pattern.patternDescriptor.consequent == selection.elContainer) {
			return "consequent";
		}

		return null;
	}
	
	function _createExceptionalModelPatternPart(selection) {
		if(selection.pattern.patternDescriptor.extension == selection.elContainer) {
			return "extension";
		} else if(selection.pattern.patternDescriptor.targetAttributes == selection.elContainer) {
			return "targetAttributes";
		}
		
		return null;
	}
	
	function _createAttributeListPart(selection) {
		if(selection.pattern.patternDescriptor.elements == selection.elContainer) {
			return  "elements";
		}
		
		return null;
	}
	
	function _createFunctionalPatternPart(selection) {
		if(selection.pattern.patternDescriptor.domain == selection.elContainer) {
			return "domain";
		} else if(selection.pattern.patternDescriptor.coDomain == selection.elContainer) {
			return "coDomain";
		}
		
		return null;
	}
	
	function _createSequencePart(pattern, selection) {
		var ix = selection.pattern.patternDescriptor.insertionLists.indexOf(selection.elContainer);

		if(ix != -1) {
			pattern.timeSets.splice(ix, 0, []);
		} else {
			ix = selection.pattern.patternDescriptor.timeSets.indexOf(selection.elContainer);
		}
		
		if(ix == -1) {
			return null;
			
		}
		
		return ix.toString();
	}
	
	function _createEpisodePart(selection) {
		if(selection.pattern.patternDescriptor.graph == selection.elContainer) {
			return "graph";
		}
		
		return null;
	}
	
	function _createEpisodeRulePart(selection) {
		if(selection.pattern.patternDescriptor.antecedent == selection.elContainer) {
			return "antecedent";
		} else if(selection.pattern.patternDescriptor.consequent == selection.elContainer) {
			return "consequent"
		}
		
		return null;
	}
	
	function _createPartAndUpdatePattern(pattern, selection) {
		if(selection.pattern.patternDescriptor.patternType == "Association") {
			return _createAssociationPart(selection);
		} else if(selection.pattern.patternDescriptor.patternType == "AssociationRule") {
			return _createAssociationRulePart(selection);
		} else if(selection.pattern.patternDescriptor.patternType == "ExceptionalModelPattern") {
			return _createExceptionalModelPatternPart(selection);
		}  else if(selection.pattern.patternDescriptor.patternType == "AttributeList") {
			return _createAttributeListPart(selection);
		}  else if(selection.pattern.patternDescriptor.patternType == "FunctionalPattern") {
			return _createFunctionalPatternPart(selection);
		}  else if(selection.pattern.patternDescriptor.patternType == "Sequence") {
			return _createSequencePart(pattern, selection);
		} else if(selection.pattern.patternDescriptor.patternType == "Episode") {
			return _createEpisodePart(selection);
		} else if(selection.pattern.patternDescriptor.patternType == "EpisodeRule") {
			return _createEpisodeRulePart(selection);
		} 
		
		return null;
	}
	
	function _createPatternWithPart(selection) {
		if(selection == null) {
			return {
				pattern: null,
				part: null
			};
		}
	
		var pattern = createSerializablePattern(selection.pattern);
		var part = _createPartAndUpdatePattern(pattern, selection);
		
		return {
			pattern: pattern,
			part: part
		}
	}
	
	function getPagedAndFilteredUrl(url, selector) {
		return buildUrl(url,
				[{
					key: KEY_PAGE,
					value: 0
				}, {
					key: KEY_SIZE,
					value: PAGE_SIZE
				}, {
					key: "attribute",
					value: $("#" + selector +" [name='attributeInput']").val()
				}, {
					key: "proposition",
					value:  $("#" + selector +" [name='propositionInput']").val()
				}, {
					key: "score",
					value: $("#" + selector +" [name='scoreInput']").val()
				}]);
	}
	
	function _setMeasure1() {
		$("#propositions2Loader").css("visibility", "visible");
		
		ajaxPost({
			url: getLink(_this._worksheetResource, KEY_PROPOSITIONS2),
			data: JSON.stringify({
				measure1: $("#measure1Select").val(),
				measure2: $("#measure2Select").val(),
				pattern: null,
			}),
			contentType : "application/json;charset=utf-8",
			processData : false,
			success: function(resource, status, response) {
				var url = response.getResponseHeader("Location");

		
				if(url == null || url == "") {
					messenger.error("An error occurred while fetching propositions for selected pattern");
					return;
				}					
				
				_this._parameters.propositions2.baseUrl = url;
				_this._parameters.propositions2.model = [];
				_this._parameters.propositions2.next = url;
				
				_resetNextPropositionsInviewEvent2();
				
				$("#propositions2Loader").css("visibility", "visible");
			},
			error: function() {
				messenger.error("An error occurred while fetching propositions for selected pattern");
			}
		});
	}
	
	function _setSelectedDescriptor() {
		var selection = _selectionModelObject.getSelection();
		
		var patternWithPart = _createPatternWithPart(selection);
		
		if(patternWithPart == null) {
			messenger.error("An error occurred while selecting descriptor");
			return;
		}
		
		if($("#sourcePropositionsDockOptions #selectedPatternCheckboxPropositions").is(":checked")) {
			$("#propositionsLoader").css("visibility", "visible");
			
			ajaxPost({
				url: getLink(_this._worksheetResource, KEY_PROPOSITIONS),
				data: JSON.stringify({
					measure: $("#measureSelect").val(),
					pattern: patternWithPart.pattern,
					part: patternWithPart.part
				}),
				contentType : "application/json;charset=utf-8",
				processData : false,
				success: function(resource, status, response) {
					var url = response.getResponseHeader("Location");
			
					if(url == null || url == "") {
						messenger.error("An error occurred while fetching propositions for selected pattern");
						return;
					}
					
					_this._parameters.propositions.baseUrl = url;
					_this._parameters.propositions.model = [];
					_this._parameters.propositions.next = getPagedAndFilteredUrl(url, "sourcePropositionsDockOptions");
					
					_resetNextPropositionsInviewEvent();
	
					$("#propositionsLoader").css("visibility", "hidden");
				},
				error: function() {
					messenger.error("An error occurred while fetching propositions for selected pattern");
				}
			});
		}

		if($("#sourcePropositionsDockOptions2 #selectedPatternCheckboxPropositions").is(":checked")) {
			$("#propositionsLoader2").css("visibility", "visible");
			
			ajaxPost({
				url: getLink(_this._worksheetResource, KEY_PROPOSITIONS2),
				data: JSON.stringify({
					measure1: $("#measure1Select").val(),
					measure2: $("#measure2Select").val(),
					pattern: patternWithPart.pattern,
					part: patternWithPart.part
				}),
				contentType : "application/json;charset=utf-8",
				processData : false,
				success: function(resource, status, response) {
					var url = response.getResponseHeader("Location");
					
					if(url == null || url == "") {
						messenger.error("An error occurred while fetching propositions for selected pattern");
						return;
					}
					
					_this._parameters.propositions2.baseUrl = url;
					_this._parameters.propositions2.next = url;
					
					_resetNextPropositionsInviewEvent2();
					
					$("#propositionsLoader2").css("visibility", "hidden");
				},
				error: function() {
					messenger.error("An error occurred while fetching propositions for selected pattern");
				}
			});
		}
	}

	function _bindEvents() {
		if(_this._eventsBound) {
			return;
		}
		
		_selectionModelObject.selectionChanged.attach(_setSelectedDescriptor);
			
		$("#sourceDockMenu *[href]").on("click", function(){
			resize();
		});

		$("#sourcePropositionsDockOptions").on("change", "[name='attributeInput'], [name='propositionInput'], [name='scoreInput']",
				_bindPropositionInputEvent);
		
		$("#measureSelect").on("change", function() {
			_setMeasure($(this).val());
		});
		
		$("#measure1Select").on("change", function() {
			_setMeasure1();
		});
		
		$("#measure2Select").on("change", function() {
			_setMeasure1();
		});

		$("#SourceAttributesDock").on({
			mousedown: _inspectAttribute
		}, ".inspectAttribute");
		
		$("#SourcePropositionsDock").on("mousedown", ".buildElement",
				_selectProposition);
		
		$("#SourceAttributesDock").on("mousedown", ".buildElement",
				_selectAttribute);
		
		$("#clearPropositionSelection").on("click", _clearPropositionSelection);
		
		$("#clearAttributeSelection").on("click", _clearAttributeSelection);
		
		_makeDockDraggable("#SourcePropositionsDock", _createDragHelperForPropositions, $("#propositions #addIndividualCheckboxPropositions"), _clearPropositionSelection);
		
		_makeDockDraggable("#SourcePropositionsDock2", _createDragHelperForPropositions2, $("#propositions2 #addIndividualCheckboxPropositions"), _clearPropositionSelection2);
		
		_makeDockDraggable("#SourceAttributesDock", _createDragHelperForAttributes, $("#addIndividualCheckboxAttributes"), _clearAttributeSelection);
		
		$("body").on({
			change: function() {
				ajaxGet({
					url: getLink(_this._worksheetResource, KEY_ATTRIBUTES) + "/" + _this._attributeResource.a_id + "/visualizations/metricHistogram?bins=" + $("#userInputModal .metricHistogramSpinner").val(),
					success: function(resource) {
						condensedHistogram("#userInputModal ." + resource.type, resource.counts);
					},
					fail: function() {
						messenger.error("An error occurred while trying to fetch " + visualizationId + " visualization");
					}
				});
			}
		}, ".attributeChart .metricHistogramSpinner");
	
		_this._eventsBound = true;
	}
	
	function _inspectAttribute(e) {
		e.stopPropagation();
		
		var id = $(this).closest("[id]").attr("id");

		ajaxGet({
			url : getLink(_this._worksheetResource, KEY_ATTRIBUTES) + "/" + id,
			success : _showAttributeInfo,
			fail: function() {
				messenger.error("An error occurred while getting attribute information")
			}
		});
	}
	
	function _getSortedAndTrimmedCategories(rawCategories) {
		var categories = $(rawCategories).map(function() {
			var key = Object.keys(this)[0];
			return {
				name : key,
				value : this[key]
			};
		}).toArray();

		categories.sort(function(a, b) {
			var c = b.value - a.value;

			if (c != 0) {
				return c;
			}

			return a.name - b.name;
		});

		if (categories.length > 9) {
			var sum = 0;
			$(categories.slice(9, categories.length)).each(function() {
				sum += this.value;
			});
			categories = categories.slice(0, 9);
			categories.push({
				name : "Others",
				value : sum
			});
		}

		return categories;
	}
	
	function _getBoxPlotData(info) {
		var boxPlotData = {
			key : 0,
			number : 0
		};

		var mapping = {
			"Min value" : "min",
			"Max value" : "max",
			"Mean" : "mean",
			"Median" : "median",
			"Lower quartile" : "q1",
			"Upper quartile" : "q3"
		}

		$(info).each(function() {
			var key = Object.keys(this)[0];

			if (key in mapping) {
				boxPlotData[mapping[key]] = parseFloat(this[key]);
			}
		});

		return boxPlotData;
	}
	
	function _showAttributeInfo(attributeResource) {
		_this._attributeResource = attributeResource;
		
		var userInputModal = $("#userInputModal");
		userInputModal.find("#userInputModalHeader").html("Attribute information: " + attributeResource.name + " [Type: " + attributeResource.type + "]");
		userInputModal.find("#userInputModalBody").html(
				Handlebars.getTemplate("ui.attributeInfo")(attributeResource));

		for(var visualization in attributeResource.visualizations) {
			var visualizationId = attributeResource.visualizations[visualization]; 
			
			ajaxGet({
				url: getLink(_this._worksheetResource, KEY_ATTRIBUTES) + "/" + attributeResource.a_id + "/visualizations/" + visualizationId,
				success: function(resource) {
					if(resource.type == "categories") {
						pie("#userInputModal ." + resource.type, _getSortedAndTrimmedCategories(resource.categories));
					} else if(resource.type == "boxPlot") {
						condensedBoxPlot("#userInputModal ." + resource.type, [resource]);
					} else if(resource.type.endsWith("Histogram")) {
						condensedHistogram("#userInputModal ." + resource.type, resource.counts);
					}
				},
				fail: function() {
					messenger.error("An error occurred while trying to fetch " + visualizationId + " visualization");
				}
			});
		}

		userInputModal.modal();
	}
	
	function _makeDockDraggable(id, helper, addIndividualCheckbox, callback) {
		$(id).draggable({
			cursorAt : {
				top : -10,
				left : -10
			},
			delay : 100,
			helper: helper,
			scroll : false,
			zIndex : 300,
			start: function(e) {
				if($(id + " .buildElement.selected").length == 0) {
					e.preventDefault();
				}
			}
		}).data("data", {
			func: function() {
				return {
					elements: $(id + " .buildElement.selected").map(function() {
						return { id: $(this).attr("id"), type:  $(this).attr("type") };
					}),
					singletons: addIndividualCheckbox.is(":checked")
				};
			},
			callback: callback
		});
	}

	function _selectProposition() {
		var id = $(this).attr("id");

		if (_this._parameters.propositions.selection.has(id)) {
			_this._parameters.propositions.selection.delete(id);
			$(this).removeClass("selected");
		} else {
			_this._parameters.propositions.selection.add(id);
			$(this).addClass("selected");
		}
	}
	
	function _selectProposition2(id) {
		var element = $("#SourcePropositionsDock2 #" + id);
		
		if (_this._parameters.propositions2.selection.has(id)) {
			_this._parameters.propositions2.selection.delete(id);
			element.removeClass("selected");
		} else {
			_this._parameters.propositions2.selection.add(id);
			element.addClass("selected");
		}
	}
	
	function _selectAttribute() {
		var id = $(this).attr("id");
		
		if (_this._parameters.attributes.selection.has(id)) {
			_this._parameters.attributes.selection.delete(id);
			$(this).removeClass("selected");
		} else {
			_this._parameters.attributes.selection.add(id);
			$(this).addClass("selected");
		}
	}
	
	function _clearPropositionSelection() {
		_this._parameters.propositions.selection.clear()
		$("#SourcePropositionsDock .buildElement").removeClass("selected");
	}
	
	function _clearPropositionSelection2() {
		_this._parameters.propositions2.selection.clear()
		$("#SourcePropositionsDock2 .buildElement").removeClass("selected");
	}
	
	function _clearAttributeSelection() {
		_this._parameters.attributes.selection.clear()
		$("#SourceAttributesDock .buildElement").removeClass("selected");
	}
	
	function _bindPropositionInputEvent() {
		_this._parameters.propositions.next = getPagedAndFilteredUrl(_this._parameters.propositions.baseUrl, "sourcePropositionsDockOptions");
		_this._parameters.propositions.model = [];

		_resetNextPropositionsInviewEvent();
	}
	
	function _loadNextPropositionsPage() {
		_loadNextPage(_this._parameters.propositions, "#propositionsLoader");
	}
	
	function _loadNextPropositionsPage2() {
		_loadNextPage(_this._parameters.propositions2, "#propositions2Loader");
	}

	function _loadNextAttributesPage() {
		_loadNextPage(_this._parameters.attributes, "#attributesLoader");
	}
	
	function _loadNextPage(parameters, selector) {
		if(parameters.fetching) {
			return;
		}
		
		$(selector).css("visibility", "visible");

		parameters.fetching = true;
		
		ajaxGet({
			url : parameters.next,
			success : function(resource) {
				parameters.drawFunction(resource);
				setTimeout(function() {
					parameters.fetching = false;

					$(selector).css("visibility", "hidden");
				}, 250);
			}
		});
	}

	function _drawElementsFromData(parameters, resource, template) {
		var timerStart = Date.now();
		var domTime = 0;

		var elements = resource._embedded == undefined ? [] : resource._embedded[Object.keys(resource._embedded)[0]];
		
		Array.prototype.push.apply(parameters.model, elements);

		if(parameters.model.length == 0) {
			parameters.loader.before("Nothing to show!");
		} else {
			parameters.loader.before($(Handlebars.getTemplate(template)({
				elements : elements
			})));
		}

		parameters.next = getLink(resource, "next");

		if (parameters.next == "" || parameters.next == null) {
			parameters.loader.remove();
		}

		console.log("Elements drawing time: " + (Date.now() - timerStart));
		console.log("DomTime: " + domTime);
	}
	
	function _drawPropositionsFromData(resource) {
		_drawElementsFromData(_this._parameters.propositions, resource, "ui.sourcePropositionList");
	}
	
	function _drawPropositionsFromData2(resource) {
		var elements = resource.elements;
		
		_this._parameters.propositions2.model = elements;

		patternExtensionsScatter("#SourcePropositionsDock2", elements, _selectProposition2);
		
		_this._parameters.propositions2.loader.remove();
	}

	function _drawAttributesFromData(resource) {
		_drawElementsFromData(_this._parameters.attributes, resource, "ui.sourceAttributeList");
	}
	
	function resize() {
		var height = $(window).height();
		height -= $("#globalMenu").height();
		height -= $("#tabs").height();
		height -= $("#sourceDockMenu").height();
		height -= $("#sourcePropositionsDockOptions").height();
		height -= $("#footer").height();
		height -= 55;

		$("#SourcePropositionsDock").height(height);

		var height = $(window).height();
		height -= $("#globalMenu").height();
		height -= $("#tabs").height();
		height -= $("#sourceDockMenu").height();
		height -= $("#sourcePropositionsDockOptions2").height();
		height -= $("#footer").height();
		height -= 55;
		
		$("#SourcePropositionsDock2").height(height);
		
		var height = $(window).height();
		height -= $("#globalMenu").height();
		height -= $("#tabs").height();
		height -= $("#sourceDockMenu").height();
		height -= $("#sourceAttributesDockOptions").height();
		height -= $("#footer").height();
		height -= 55;

		$("#SourceAttributesDock").height(height);
	}
	
	if (_this == null) {
		return _this = {
			init : init,
			resize : resize
		}
	} else {
		return _this;
	}

}