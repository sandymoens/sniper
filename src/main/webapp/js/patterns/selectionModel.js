function SelectionModel() {

	var _this;

	function init(selectionChanger) {
		_this._selectedElement = null;

		_this.selectionChanged = new Event(selectionChanger);
	}

	function setSelection(element) {
		_this._selectedElement = element;

		_this.selectionChanged.notify();
	}

	function getSelection() {
		return _this._selectedElement;
	}

	function clearSelection() {
		setSelection(null);
	}

	if (_this == null) {
		return _this = {
			init : init,
			setSelection : setSelection,
			getSelection : getSelection
		}
	} else {
		return _this;
	}

}
