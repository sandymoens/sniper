function PatternPostProcessorsController(workDockController) {

	var FORM_ID = "patternPostProcessorsParametersForm";
	var RUNNING_MODAL_ID = "patternPostProcessorsModal";
	var RUN_ALGORITHM_BTN_CLASS = "runPatternPostProcessor";
	var STOP_RUNNING_BTN_CLASS = "stopPatternPostProcessor";

	var _this;

	var _workDockController = workDockController;

	function init(worksheetResource) {
		if (worksheetResource == null) {
			messenger
					.error("A problem occurred while initializing the pattern post processors!");
			return;
		}

		_this._worksheetResource = worksheetResource;

		_getAlgorithms();

		_bindEvents();
	}

	function _getAlgorithms() {
		ajaxGet({
			url : getLink(_this._worksheetResource, KEY_PATTERN_POST_PROCESSORS),
			success : _setAlgorithmsResource,
			fail : function() {
				messenger.error("A problem occurred while fetching miners");
			}
		});
	}

	function _bindEvents() {
		$("#patternPostProcessorOptionButton").on({
			click : _selectAlgorithm
		}, "[type='patternPostProcessor']");

		$("#userInputModal").on({
			change : _updateParameters
		}, "#" + FORM_ID);

		$("#userInputModal").on({
			click : _runAlgorithm
		}, "." + RUN_ALGORITHM_BTN_CLASS);

		$("body").on({
			click : _stopMiner
		}, "#" + RUNNING_MODAL_ID + " ." + STOP_RUNNING_BTN_CLASS);
	}

	function _destroyRunningModal(callback) {
		$("#" + RUNNING_MODAL_ID).modal("hide").data("bs.modal", null);

		setTimeout(function() {
			$("#" + RUNNING_MODAL_ID).remove();
			$('.modal-backdrop').remove();
			callback();
		}, 500);
	}

	function _stopMiner() {
		ajaxDelete({
			url : getLink(_this._minerResource, KEY_ACTION),
			success : function() {
				messenger.info("Stopping miner");
			},
			fail : function() {
				messenger.error("A problem occurred while stopping the miner");
			}
		});
	}

	function _runAlgorithm() {
		_startActionAndFollowUp({
			url : getLink(_this._minerResource, KEY_ACTION),
			success : function() {
				_workDockController.redrawPatterns();

				_destroyRunningModal(function() {
					messenger.success("Post processor ran successfully");
				});
			},
			fail : function() {
				_destroyRunningModal(function() {
					messenger
							.error("A problem occurred while running the miner");
				});
			}
		});
	}

	function _checkStatusAndFollowUp(data, resource) {
		ajaxGet({
			url : data.url,
			success : function(resource) {
				if (resource === "RUNNING") {
					setTimeout(function() {
						_checkStatusAndFollowUp(data, resource);
					}, 1000);
				} else {
					data.success();
				}
			},
			fail : data.fail
		});
	}

	function _startActionAndFollowUp(data) {
		var modal = $(Handlebars.getTemplate("ui.runningModal")({
			modalId : RUNNING_MODAL_ID,
			type1 : "Pattern based post processor",
			buttonClass : STOP_RUNNING_BTN_CLASS
		}));

		$("body").append(modal);

		modal.modal();

		ajaxPost({
			url : data.url,
			success : function(resource) {
				_checkStatusAndFollowUp(data, resource);
			},
			fail : data.fail
		});
	}

	function _updateParameters() {
		var parameters = {};

		$("#" + FORM_ID + " input[name], #" + FORM_ID + " select[name]").each(
				function() {
					parameters[$(this).attr("name")] = $(this).val();
				});

		ajaxPut({
			url : getLink(_this._minerResource, KEY_ACTION),
			data : JSON.stringify(parameters),
			contentType : "application/json",
			processData : false,
			success : _setAlgorithmParameters,
			fail : function() {
				_setAlgorithmParameters(_this._minerResource);
				messenger
						.error("A problem occurred while updating pattern post processor parameters");
			}
		});
	}

	function _selectAlgorithm() {
		var data = {}
		data[KEY_TYPE] = "patternPostProcessor";
		data[KEY_ID] = $(this).attr("value");
		data[KEY_IXS] = _workDockController.selectedPatternIndices();

		if (data[KEY_IXS].length == 0) {
			alertBox("Please select some patterns before running a pattern based post processor!");
			return;
		}

		var caption = $(this).find("a").text();

		ajaxPost({
			url : getLink(_this._worksheetResource, KEY_ACTIONS),
			data : JSON.stringify(data),
			contentType : "application/json",
			processData : false,
			success : function(resource) {
				_getParameters(caption, resource);
			},
			fail : function() {
				messenger.error("A problem occurred while fetching miners");
			}
		});
	}

	function _setAlgorithmsResource(resource) {
		_this._algorithms = resource;

		var algorithms = $(resource.resources).map(function() {
			return {
				type : "patternPostProcessor",
				caption : this.caption,
				value : this.id
			};
		}).toArray();

		$("#patternPostProcessorOptionButton").append(
				$(Handlebars.getTemplate("ui.optionsList")(algorithms)));

		$("#workDockMenu .sm").smartmenus("refresh");
	}

	function _getParameters(caption, resource) {
		ajaxGet({
			url : getLink(resource, KEY_ACTION_PARAMETERS),
			success : function(resource) {
				$("#userInputModalHeader").empty().append(
						"Set parameters for " + caption);

				_setAlgorithmParameters(resource);

				$("#userInputModal").modal();
			},
			fail : function() {
				messenger.error("A problem occurred while fetching parameters");
			}
		});
	}

	function _setAlgorithmParameters(resource) {
		_this._minerResource = resource;

		$("#userInputModalBody").empty().append(
				$(Handlebars.getTemplate("ui.formWithHelp")({
					formId : FORM_ID,
					inputs : prepareParameters(resource.parameters),
					action : "Run pattern based post processor",
					buttonClass : RUN_ALGORITHM_BTN_CLASS
				})));

		$("#userInputModal .chosen-select").chosen();
	}

	if (_this == null) {
		return _this = {
			init : init,
		}
	} else {
		return _this;
	}

}