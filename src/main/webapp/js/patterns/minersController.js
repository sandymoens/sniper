function MinersController(workDockController) {

	var FORM_ID = "minerParametersForm";
	var MINING_MODAL_ID = "miningModal";
	var RUN_MINER_BTN_CLASS = "runMiner";
	var STOP_MINER_BTN_CLASS = "stopMiner";

	var _this;

	var _workDockController = workDockController;

	function init(worksheetResource) {
		if (worksheetResource == null) {
			messenger
					.error("A problem occurred while initializing the miners!");
			return;
		}

		_this._worksheetResource = worksheetResource;

		_getMiners();

		_bindEvents();
	}

	function _getMiners() {
		ajaxGet({
			url : getLink(_this._worksheetResource, KEY_MINERS),
			success : _setMinersResource,
			fail : function() {
				messenger.error("A problem occurred while fetching miners");
			}
		});
	}

	function _bindEvents() {
		$("#minerOptionButton").on({
			click : _selectMiner
		}, "[type='miner']");

		$("#userInputModal").on({
			change : _updateParameters
		}, "#" + FORM_ID);

		$("#userInputModal").on({
			click : _runMiner
		}, "." + RUN_MINER_BTN_CLASS);

		$("body").on({
			click : _stopMiner
		}, "#" + MINING_MODAL_ID + " ." + STOP_MINER_BTN_CLASS);
	}

	function _destroyModal(callback) {
		$("#" + MINING_MODAL_ID).modal("hide").data("bs.modal", null);

		setTimeout(function() {
			$("#" + MINING_MODAL_ID).remove();
			$('.modal-backdrop').remove();
			callback();
		}, 500);
	}

	function _stopMiner() {
		ajaxDelete({
			url : getLink(_this._minerResource, KEY_ACTION),
			success : function() {
				messenger.info("Stopping miner");
			},
			fail : function() {
				messenger.error("A problem occurred while stopping the miner");
			}
		});
	}

	function _runMiner() {
		_startActionAndFollowUp({
			url : getLink(_this._minerResource, KEY_ACTION),
			success : function(resource) {
				_workDockController.redrawPatterns();

				_destroyModal(function() {
					messenger.success("Miner ran successfully");
				});
			},
			fail : function() {
				_destroyModal(function() {
					messenger
							.error("A problem occurred while running the miner");
				});
			}
		});
	}

	function _checkStatusAndFollowUp(data, resource) {
		ajaxGet({
			url : data.url,
			success : function(resource) {
				if (resource === "RUNNING") {
					setTimeout(function() {
						_checkStatusAndFollowUp(data, resource);
					}, 1000);
				} else {
					data.success();
				}
			},
			fail : data.fail
		});
	}

	function _startActionAndFollowUp(data) {
		var modal = $(Handlebars.getTemplate("ui.runningModal")({
			modalId : MINING_MODAL_ID,
			type1 : "Miner",
			buttonClass : STOP_MINER_BTN_CLASS
		}));

		$("body").append(modal);

		modal.modal();

		ajaxPost({
			url : data.url,
			success : function(resource) {
				_checkStatusAndFollowUp(data, resource);
			},
			fail : data.fail
		});
	}

	function _updateParameters() {
		var parameters = {};

		$("#" + FORM_ID + " input[name], #" + FORM_ID + " select[name]").each(
				function() {
					parameters[$(this).attr("name")] = $(this).val();
				});

		ajaxPut({
			url : getLink(_this._minerResource, KEY_ACTION),
			data : JSON.stringify(parameters),
			contentType : "application/json",
			processData : false,
			success : _setMinerParameters,
			fail : function() {
				_setMinerParameters(_this._minerResource);
				messenger
						.error("A problem occurred while updating miner parameters");
			}
		});
	}

	function _selectMiner() {
		var data = {}
		data[KEY_TYPE] = "miner";
		data[KEY_ID] = $(this).attr("value");
		data[KEY_IXS] = _workDockController.selectedPatternIndices();

		var caption = $(this).find("a").text();

		ajaxPost({
			url : getLink(_this._worksheetResource, KEY_ACTIONS),
			data : JSON.stringify(data),
			contentType : "application/json",
			processData : false,
			success : function(resource) {
				_getParameters(caption, resource);
			},
			fail : function() {
				messenger.error("A problem occurred while fetching miners");
			}
		});
	}

	function _setMinersResource(resource) {
		_this._miners = resource;

		var miners = $(resource.resources).map(function() {
			return {
				type : "miner",
				caption : this.caption,
				value : this.id
			};
		}).toArray();

		$("#minerOptionButton").append(
				$(Handlebars.getTemplate("ui.optionsList")(miners)));

		$("#subMenu .sm").smartmenus("refresh");
	}

	function _getParameters(caption, resource) {
		ajaxGet({
			url : getLink(resource, KEY_ACTION_PARAMETERS),
			success : function(resource) {
				$("#userInputModalHeader").empty().append(
						"Set parameters for " + caption);

				_setMinerParameters(resource);

				$("#userInputModal").modal();
			},
			fail : function() {
				messenger("A problem occurred while fetching parameters");
			}
		});
	}

	function _setMinerParameters(resource) {
		_this._minerResource = resource;

		$("#userInputModalBody").empty().append(
				$(Handlebars.getTemplate("ui.formWithHelp")({
					formId : FORM_ID,
					inputs : prepareParameters(resource.parameters),
					action : "Run miner",
					buttonClass : RUN_MINER_BTN_CLASS
				})));

		$("#userInputModal .chosen-select").chosen();
	}

	if (_this == null) {
		return _this = {
			init : init,
		}
	} else {
		return _this;
	}

}