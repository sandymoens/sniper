function TabsController() {

	var _this;

	function init(resource) {
		_bindEvents();

		_this._worksheetResource = resource;

		_getWorksheetInfo();
	}

	function _bindEvents() {
	}

	function _getWorksheetInfo() {
		ajaxGet({
			url : getLink(_this._worksheetResource, KEY_WORKSHEET_INFO),
			success : _updateWorksheetInfo,
			fail : function() {
				messenger
						.error("An error occurred while fetching worksheet info");
			}
		});
	}

	function _updateWorksheetInfo(resource) {
		var html = $("<div class='table'></div>");

		for (var i = 0; i < resource.resources.length; i++) {
			var key = Object.keys(resource.resources[i]);

			html.append($("<div class='row'></div>").append(
					"<div class='col-md-6'>" + key
							+ "</div><div class='col-md-6'>"
							+ resource.resources[i][key] + "</div>"));
		}

		$("#tabs-1").empty().html(html);
	}

	if (_this == null) {
		return _this = {
			init : init,
		}
	} else {
		return _this;
	}

}