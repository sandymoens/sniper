function WorkDockController(selectionModelObject, measuresController,
		visualizationsController) {

	var MODAL_ID = "inspectionModal";
	var CLOSE_BTN_CLASS = "closeModal";

	var _this;
	var _selectionModel = selectionModelObject;
	var _measuresObservable = measuresController;
	var _visualizationsObservable = visualizationsController;

	var _redrawTimeout = null;

	_selectionModel.init(_this);

	function init(worksheetResource) {
		if (worksheetResource == null) {
			console.log("A problem occurred while initializing the work dock!");
			return;
		}

		_this._worksheetResource = worksheetResource;
		_this._patternsLink = buildUrl(getLink(_this._worksheetResource,
				KEY_PATTERNS), [ {
			key : "page",
			value : 0
		}, {
			key : "size",
			value : 100
		} ]);

		_bindEvents();

		_drawTableHeader();
		_redrawPatterns();
	}

	function _bindEvents() {
		if (_this._eventsBound) {
			return;
		}

		$("#WorkDock").droppable({
			drop : _dropEventHandler,
			tolerance : "pointer"
		});

		$("#WorkDock").on({
			click : _inspectPattern
		}, ".inspectPatternButton");
		
		$("#WorkDock").on({
			click : _copyPatternToClipboard
		}, ".copyToClipboardButton");

		$("#WorkDock").on({
			click : _deletePattern
		}, ".deletePatternButton");

		$("#WorkDockTable").on({
			click : _selectAllPatterns
		}, ".selectAllPatternsButton");

		$("#WorkDockTable").on({
			click : _deselectAllPatterns
		}, ".deselectAllPatternsButton");

		$("#WorkDockTable").on({
			mouseenter : function() {
				if ($(".ui-draggable-dragging .pattern").length > 0) {
				} else if ($(".ui-draggable-dragging").length > 0) {
					$(this).addClass("droppable");
				}
			},
			mouseleave : function() {
				$(this).removeClass("droppable");
			},
		}, ".extensionDescriptor, .elContainer");

		$("#WorkDockTable").on({
			click : function(event) {
				_removeFromDescriptor($(this).closest(".buildElement"));
				event.stopPropagation();
			},
		}, ".deleteButton");

		$("#WorkDockTable").on({
			mouseenter : _makeDraggable,
			mouseleave : _makeUndraggable,
			click : _buildElementClicked
		}, ".buildElement");

		$("#WorkDockTable").on({
			mouseenter : _makePatternDraggable,
		}, ".patternDrag");

		$("#WorkDock").on({
			click : _clearSelection
		});

		$("#WorkDock").on({
			change : _changeModelType
		}, "select[type='modelType']");

		$("#WorkDockTable tbody").on({
			inview : _getVisualizations
		}, ".pattern[vis='nok']");

		$("#WorkDockTable thead").on({
			click : _sortPatterns
		}, "[function='sort'][order][measure]");

		$("#WorkDockTable thead").on({
			click : _inspectPatternsByMeasure
		}, "[function='inspect']");

		$("body").on({
			click : _destroyModal
		}, "#" + MODAL_ID + " ." + CLOSE_BTN_CLASS);

		$("body").on({
			click : _setInspectPatternPage
		}, "#" + MODAL_ID + " .pageLinks *[href]");
		
		$("#WorkDockTable tbody").on({
			dblclick : _enlargeChart
		}, "div[vis]");

		_this._eventsBound = true;
	}
	
	function _enlargeChart() {
		var emptyPattern = _emptyPattern(_this._patterns[$(this).closest(
			".pattern[patternIx]").attr("patternIx")]);
		var pattern = _this._patterns[$(this).closest(".pattern[patternIx]")
			.attr("patternIx")];

		var data = {
				modalId : MODAL_ID,
				buttonClass : CLOSE_BTN_CLASS,
				chartName: $(this).attr("vis"),
				emptyPattern : emptyPattern,
				pattern : pattern
		};
		
		var isImgChart = $(this).find("[src]").length == 1;
		
		if(isImgChart) {
			data["type"] = "file";
			data["data"] = $(this).find("img").attr("src");
		} else {
			data["type"] = "Circular pattern dissection";
			data["data"] = JSON.parse($(this).find("[data-patterns]").attr("data-patterns"));
		}
		
		var largeChartModal = $(Handlebars.getTemplate("ui.largeChartModal")(data));
		
		$("#" + MODAL_ID).remove();
		$("body").append(largeChartModal);
		
		largeChartModal.modal();
	}

	function _showPatternLoader() {
		$("#patternLoader").css("visibility", "visible");
	}

	function _hidePatternLoader() {
		$("#patternLoader").css("visibility", "hidden");
	}

	function _emptyPattern(pattern) {
		if (pattern.patternDescriptor.patternType === "Association") {
			return {
				patternDescriptor : {
					patternType : pattern.patternDescriptor.patternType,
					elements : {
						elements : []
					}
				}
			}
		} else if (pattern.patternDescriptor.patternType === "AssociationRule") {
			return {
				patternDescriptor : {
					patternType : pattern.patternDescriptor.patternType,
					antecedent : {
						elements : []
					},
					consequent : {
						elements : []
					}
				}
			}
		} else if (pattern.patternDescriptor.patternType === "ExceptionalModelPattern") {
			return {
				patternDescriptor : {
					patternType : pattern.patternDescriptor.patternType,
					modelType : pattern.patternDescriptor.modelType,
					targetAttributes : pattern.patternDescriptor.targetAttributes,
					extension : {
						elements : []
					}
				}
			}
		}
	}

	function _sortPatterns() {
		var data = {};
		data[KEY_TYPE] = "sortPatterns";
		data[KEY_MEASURE] = $(this).attr("measure");
		data[KEY_ORDER] = $(this).attr("order");

		if (_this._worksheetResource != undefined) {
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : function() {
					_redrawPatterns();
				},
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function _inspectPatternsByMeasure() {
		var measure = $(this).closest("[data-measure]").data("measure");

		if (_this._worksheetResource != undefined) {
			ajaxGet({
				url : getLink(_this._worksheetResource, KEY_MEASURES) + "/"
						+ measure,
				success : _drawMeasureCharts,
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function _drawMeasureCharts(resource) {
		var inspectionModal = $(Handlebars.getTemplate(
				"ui.measureVisualizationsModal")({
			modalId : MODAL_ID,
			buttonClass : CLOSE_BTN_CLASS,
			info : resource
		}));

		$("#" + MODAL_ID).remove();
		$("body").append(inspectionModal);

		inspectionModal.modal();
		
		for(let visualization of resource.visualizations) {
			if(visualization.type == "boxPlot") {
				condensedBoxPlot("#" + MODAL_ID + " ." + visualization.type, [visualization]);
			} else if(visualization.type == "normalDistribution") {
				normalDistribution("#" + MODAL_ID + " ." + visualization.type, visualization);
			} else if(visualization.type.endsWith("Histogram")) {
				condensedHistogram("#" + MODAL_ID + " ." + visualization.type, visualization.counts);
			}
		}

	}

	function _changeModelType() {
		var data = {};
		data[KEY_TYPE] = "addToDescriptor";
		data[KEY_DESCRIPTOR_ID] = $(this).attr("descriptorid");
		data[KEY_ELEMENT_TYPE] = $(this).attr("type");
		data[KEY_IDS] = [ $(this).val() ];
		data[KEY_INDIVIDUAL] = false;

		if (_this._worksheetResource != undefined) {
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : function() {
					_redrawPatterns();
				},
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function _buildElementClicked(e) {
		if (e.which == 2 || (e.which == 1 && e.altKey)) {
			_removeFromDescriptor($(this));
			return;
		}

		_setSelection(e, this);
	}

	function _removeFromDescriptor(element) {
		var data = {}
		data[KEY_TYPE] = "removeFromDescriptor";
		data[KEY_DESCRIPTOR_ID] = element.closest(".elContainer[descriptorId]")
				.attr("descriptorId");
		data[KEY_ELEMENT_TYPE] = element.attr("type") == "item" ? "proposition"
				: element.attr("type");
		data[KEY_ID] = element.attr("id");

		if (_this._worksheetResource != undefined) {
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : function() {
					_redrawPatterns();

					if (data.callback != null) {
						data.callback();
					}
				},
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function addEdge(descriptorId, nodeFromId, nodeToId) {
		var data = {}
		data[KEY_TYPE] = "addToDescriptor";
		data[KEY_DESCRIPTOR_ID] = descriptorId;
		data[KEY_ELEMENT_TYPE] = "edge";
		data[KEY_IDS] = [ nodeFromId + "_" + nodeToId ];

		if (_this._worksheetResource != undefined) {
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : function() {
					_redrawPatterns();

					if (data.callback != null) {
						data.callback();
					}
				},
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function removeEdge(descriptorId, nodeFromId, nodeToId) {
		var data = {}
		data[KEY_TYPE] = "removeFromDescriptor";
		data[KEY_DESCRIPTOR_ID] = descriptorId;
		data[KEY_ELEMENT_TYPE] = "edge";
		data[KEY_ID] = nodeFromId + "_" + nodeToId;

		if (_this._worksheetResource != undefined) {
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : function() {
					_redrawPatterns();

					if (data.callback != null) {
						data.callback();
					}
				},
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function _clearSelection(e) {
		e.stopPropagation();

		if (_selectionModel.getSelection() == null) {
			return;
		}

		_selectionModel.setSelection(null);

		$(".buildElement, .elContainer").removeClass("selected");
	}

	function _setSelection(e, element) {
		e.stopPropagation();

		var pattern = element.closest(".pattern");
		var elContainer = element.closest(".elContainer");

		var selectionElement = {
			pattern : $(pattern).data("pattern"),
			elContainer : $(elContainer).data("descriptor")
		};

		if (isEquivalent(_selectionModel.getSelection(), selectionElement)) {
			return;
		}

		_selectionModel.setSelection(selectionElement);

		$(".buildElement, .elContainer").removeClass("selected");

		$(element).closest(".elContainer").addClass("selected");
	}

	function setPatternPage() {
		_this._patternsLink = $(this).attr("href");
		_redrawPatterns();
	}

	function _makeUndraggable() {
		_makeElementUndraggable(this);
	}

	function _makeElementUndraggable(element) {
		if ($(".ui-draggable-dragging").length == 0) {
			$(element).draggable("destroy");
		}
	}

	function _createDragHelper(element) {
		if (element.is("g")) {
			var workElements = {
				elements : [ {
					type : "item",
					element : {
						id : element.attr("data-nodeId"),
						attributeName : element.attr("data-attributeName"),
						propositionValue : element
								.attr("data-propositionValue"),
					}
				} ]
			};
			return $("<div class='dragContainer'></div>").append(
					$(
							Handlebars.getTemplate("ui.buildElementDragHelper")
									(workElements)).css("pointer-events",
							"none"));
		}

		return $("<div class='dragContainer padded'></div>").append(
				$(element).clone()).css("pointer-events", "none");
	}

	function _createPatternDragHelper(element) {
		return $("<div class='dragContainer'></div>").append(
				$("<table></table>").append($(element).clone())).css(
				"pointer-events", "none");
	}

	function _makeDraggable() {
		if ($(".ui-draggable-dragging") > 0) {
			return;
		}

		var element = $(this);
		var id = element.attr("id");
		var type = element.attr("type");

		if (element.is("g")) {
			id = element.attr("data-nodeId");
			type = "node";
		}

		element.draggable({
			appendTo : "body",
			cursorAt : {
				top : -10,
				left : -10
			},
			delay : 100,
			helper : function() {
				return _createDragHelper(element)
			},
			scroll : false,
			zIndex : 300,
		}).data("data", {
			func : function() {
				return {
					elements : $([ {
						id : id,
						type : type
					} ])
				};
			},
			callback : function() {
				_makeElementUndraggable(element);
			}
		});
	}

	function _makePatternDraggable() {
		if ($(".ui-draggable-dragging") > 0) {
			return;
		}

		var pattern = $(this).closest(".pattern");
		var ix = pattern.attr("patternix");
		var type = "pattern";

		pattern.draggable({
			cursorAt : {
				top : -10,
				left : -10
			},
			delay : 100,
			helper : function() {
				return _createPatternDragHelper(pattern)
			},
			scroll : false,
			zIndex : 300,
		}).data("data", {
			func : function() {
				return {
					ix : ix,
					type : type
				};
			},
			callback : function() {
				_makeElementUndraggable(pattern);
			}
		});
	}

	function _selectPatternsCheckedState(checked) {
		$(".pattern input[type='checkbox']").prop('checked', checked);
	}

	function _inspectPattern() {
		if (_this._worksheetResource != undefined) {
			ajaxGet({
				url : getLink(_this._worksheetResource, KEY_PATTERNS) + "/"
						+ $(this).closest("[patternIx]").attr("patternIx")
						+ "/rows?page=0&size=100",
				success : _showRows,
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function _destroyModal() {
		$("#" + MODAL_ID).modal("hide").data("bs.modal", null);

		setTimeout(function() {
			$("#" + MODAL_ID).remove();
		}, 500);
	}

	function _showRows(resource) {
		var inspectionModal = $(Handlebars.getTemplate("ui.inspectionModal")({
			modalId : MODAL_ID,
			buttonClass : CLOSE_BTN_CLASS
		}));

		$("#" + MODAL_ID).remove();
		$("body").append(inspectionModal);

		_updateInspectPatternPage(resource);

		inspectionModal.modal();
	}

	function _setInspectPatternPage() {
		if (_this._worksheetResource != undefined) {
			ajaxGet({
				url : $(this).attr("href"),
				success : _updateInspectPatternPage,
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function _updateInspectPatternPage(resource) {
		var content = resource._embedded[Object.keys(resource._embedded)[0]][0].resources;

		$("#" + MODAL_ID + " #rows").empty().append(
				$(Handlebars.getTemplate("ui.inspectionTable")({
					content : content
				})));

		$("#" + MODAL_ID).find("#pageLinks").empty().html(
				getPageLinks(resource));
	}
	
	function _copyPatternToClipboard() {
		copyToClipboard(JSON.stringify([createSerializablePattern(_this._patterns[$(this).closest("[patternIx]").attr("patternIx")])]));
		
		messenger.success("Copied 1 pattern to clipboard");
	}

	function _selectAllPatterns() {
		_selectPatternsCheckedState(true);
	}

	function _deselectAllPatterns() {
		_selectPatternsCheckedState(false);
	}

	function _deletePattern() {
		var data = {};
		data[KEY_TYPE] = "deletePatterns";
		data[KEY_IXS] = [ $(this).closest("[patternIx]").attr("patternIx") ];

		if (_this._worksheetResource != undefined) {
			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				processData : false,
				success : _redrawPatterns,
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function _getDropData(event, dropData) {
		var toElement = document.elementFromPoint(event.clientX, event.clientY);

		var data = {};

		if (dropData.type == "pattern") {
			var pattern = $(toElement).closest(".pattern");

			data[KEY_TYPE] = "movePattern";
			data[KEY_FROM_IX] = parseInt(dropData.ix);
			data[KEY_TO_IX] = pattern.length == 1 ? parseInt($(pattern).attr(
					"patternIx")) : -1;

			if (data[KEY_FROM_IX] == data[KEY_TO_IX]) {
				return null;
			}
		} else {
			var elContainer = $(toElement).closest(".elContainer");
			var edContainer = $(toElement).closest(".extensionDescriptor");

			var type = dropData.elements[0].type == "item" ? "proposition"
					: dropData.elements[0].type;
			var ids = dropData.elements.map(function() {
				return this.id;
			}).toArray();

			var individual = dropData.singletons;

			if (edContainer.length == 1) {
				data[KEY_TYPE] = "addToDescriptor";
				data[KEY_DESCRIPTOR_ID] = $(edContainer[0])
						.attr("descriptorid");
				data[KEY_ELEMENT_TYPE] = type;
				data[KEY_IDS] = ids;
				data[KEY_INDIVIDUAL] = individual;
			} else if (elContainer.length == 1) {
				data[KEY_TYPE] = "addToDescriptor";
				data[KEY_DESCRIPTOR_ID] = $(elContainer[0])
						.attr("descriptorid");
				data[KEY_ELEMENT_TYPE] = type;
				data[KEY_IDS] = ids;
				data[KEY_INDIVIDUAL] = individual;
			} else {
				var pattern = $(toElement).closest(".pattern");

				data[KEY_TYPE] = "createNewPattern";
				data[KEY_ELEMENT_TYPE] = type;
				data[KEY_IDS] = ids;
				data[KEY_INDIVIDUAL] = individual;
				data[KEY_IX] = pattern.length == 1 ? parseInt($(pattern).attr(
						"patternIx")) + 1 : -1;
			}
		}

		return data;
	}

	function _dropEventHandler(event, ui) {
		var data = ui.draggable.data("data");

		var dropData;

		if (data.func !== undefined) {
			dropData = data.func();
		}

		if (_this._worksheetResource != undefined) {
			var dropData = _getDropData(event, dropData);

			if (dropData == null) {
				return;
			}

			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(dropData),
				contentType : "application/json",
				processData : false,
				success : function() {
					_redrawPatterns();

					if (data.callback != null) {
						data.callback();
					}
				},
				fail : function() {
					console.log("fail");
				}
			});
		}
	}

	function selectedPatterns() {
		const indices = $(".pattern:has(input[type='checkbox']:checked)").map(
				function() {
					return parseInt($(this).attr("patternix"));
				}).toArray();
		
		var patterns = [];
		
		for(var index of indices) {
			patterns.push(_this._patterns[index]);
		}
		
		return patterns;
	}
	
	function selectedPatternIndices() {
		return $(".pattern:has(input[type='checkbox']:checked)").map(
				function() {
					return $(this).attr("patternix");
				}).toArray();
	}

	function clearSelectedPatterns() {
		if (_this._worksheetResource != undefined) {
			var ixs = selectedPatternIndices();

			var data = {};
			data[KEY_TYPE] = "deletePatterns";
			data[KEY_IXS] = ixs;

			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				success : function() {
					messenger.success("Deleted selected patterns successfully");
					_redrawPatterns();
				},
				fail : function() {
					messenger
							.error("A problem occurred while deleting selected patterns!");
				}
			});
		}
	}

	function clearAllPatterns() {
		if (_this._worksheetResource != undefined) {
			var data = {};
			data[KEY_TYPE] = "clearPatterns";

			ajaxPost({
				url : getLink(_this._worksheetResource, KEY_ACTIONS),
				data : JSON.stringify(data),
				contentType : "application/json",
				success : function() {
					messenger.success("Deleted all patterns successfully");
					_redrawPatterns();
				},
				fail : function() {
					messenger
							.error("A problem occurred while deleting all patterns!");
				}
			});
		}
	}

	function _redrawPatterns() {
		if (_this._worksheetResource != undefined) {
			_showPatternLoader();

			ajaxGet({
				url : _this._patternsLink,
				success : _drawPatterns,
				fail : function() {
					messenger
							.error("A problem occurred while fetching patterns");
				}
			});
		}
	}

	function _drawTableHeader() {
		var headerInfo = {
			measures : _measuresObservable.getActiveMeasures(),
			visualizations : _visualizationsObservable
					.getActiveVisualizations()
		};

		$("#WorkDockTable thead").html(
				$(Handlebars.getTemplate("ui.workDockHeader")(headerInfo)));
	}

	function _bindData(htmlPattern, pattern) {
		$(htmlPattern).data("pattern", pattern)

		var elContainers = $(htmlPattern).find(".elContainer");

		if (pattern.patternDescriptor.patternType == "Association") {
			$(elContainers[0]).data("descriptor",
					pattern.patternDescriptor.elements);
		} else if (pattern.patternDescriptor.patternType == "AssociationRule") {
			$(elContainers[0]).data("descriptor",
					pattern.patternDescriptor.antecedent);
			$(elContainers[1]).data("descriptor",
					pattern.patternDescriptor.consequent);
		} else if (pattern.patternDescriptor.patternType == "AttributeList") {
			$(elContainers[0]).data("descriptor",
					pattern.patternDescriptor.elements);
		} else if (pattern.patternDescriptor.patternType == "FunctionalPattern") {
			$(elContainers[0]).data("descriptor",
					pattern.patternDescriptor.domain);
			$(elContainers[1]).data("descriptor",
					pattern.patternDescriptor.coDomain);
		} else if (pattern.patternDescriptor.patternType == "ExceptionalModelPattern") {
			$(elContainers[0]).data("descriptor",
					pattern.patternDescriptor.targetAttributes);
			$(elContainers[1]).data("descriptor",
					pattern.patternDescriptor.extension);
		} else if (pattern.patternDescriptor.patternType == "Sequence") {
			$(elContainers[0]).data("descriptor",
					pattern.patternDescriptor.insertionLists[0]);
			for ( var i in pattern.patternDescriptor.timeSets) {
				$(elContainers[1 + 2 * i]).data("descriptor",
						pattern.patternDescriptor.timeSets[i]);
				$(elContainers[2 + 2 * i])
						.data(
								"descriptor",
								pattern.patternDescriptor.insertionLists[parseInt(i) + 1]);
			}
		} else if (pattern.patternDescriptor.patternType == "Episode") {
			$(elContainers[0]).data("descriptor",
					pattern.patternDescriptor.graph);
		} else if (pattern.patternDescriptor.patternType == "EpisodeRule") {
			$(elContainers[0]).data("descriptor",
					pattern.patternDescriptor.antecedent);
			$(elContainers[1]).data("descriptor",
					pattern.patternDescriptor.consequent);
		}
	}

	function _drawPatterns(resource) {
		_this._patterns = "_embedded" in resource.map.patterns ? resource.map.patterns._embedded[Object
				.keys(resource.map.patterns._embedded)[0]]
				: [];

		var page = resource.map.patterns.page;

		var startIx = page.size * page.number;
		_this._patternsLink = getLink(resource.map.patterns, "self");

		var timerStart = Date.now();
		var patternList = [];

		var visualizations = _visualizationsObservable
				.getActiveVisualizations();

		for (var i = 0, iEnd = _this._patterns.length; i < iEnd; i++) {
			var pattern = _this._patterns[i];

			_setSortedMeasurements(pattern);

			var htmlPattern = $(Handlebars.getTemplate("ui.workDockPatternRow")
					({
						index : pattern.ix,
						ix : pattern.ix + 1,
						pattern : pattern,
						visualizations : visualizations
					}));

			_bindData(htmlPattern, pattern);

			patternList.push(htmlPattern);
		}

		console.log("Drawing time: " + (Date.now() - timerStart));
		timerStart = Date.now();
		$("#WorkDockTable tbody").empty().append(patternList);
		console.log("Swapping time: " + (Date.now() - timerStart));

		for (var i = 0, iEnd = _this._patterns.length; i < iEnd; i++) {
			var descriptor = _this._patterns[i].patternDescriptor;

			if (descriptor.patternType == "Episode") {
				var episode = _this._patterns[i];
				drawEpisode(
						{
							id : episode.patternDescriptor.graph.nodes.id,
							nodes : episode.patternDescriptor.graph.nodes.elements,
							localIds : episode.patternDescriptor.graph.localIds.elements,
							edges : episode.patternDescriptor.graph.edges.elements
						}, this._worksheetResource);
			} else if (descriptor.patternType == "EpisodeRule") {
				drawEpisodeRule(_this._patterns[i], this._worksheetResource);
			}
		}

		$("#patternPageLinks").empty().append(
				getPageLinks(resource.map.patterns));

		if (!resource.map.doneComputing) {
			if (_redrawTimeout == null) {
				clearTimeout(_redrawTimeout);
				_redrawTimeout = setTimeout(function() {
					_redrawPatterns();
					_redrawTimeout = null;
				}, 1000);
			}
		} else {
			_hidePatternLoader();
		}
	}

	function _setSortedMeasurements(pattern) {
		pattern.measurements = $(_measuresObservable.getActiveMeasures())
				.map(
						function() {
							return this.caption in pattern.measurementsMap ? pattern.measurementsMap[this.caption]
									: "-";
						}).toArray();
	}

	function _getVisualizations() {
		var pattern = $(this);

		pattern.removeAttr("vis");

		var patternIx = pattern.attr("patternix");

		ajaxGet({
			url : getLink(_this._worksheetResource, KEY_PATTERNS) + "/"
					+ patternIx + "/visualizations",
			success : function(resource) {
				for ( var name in resource.map) {
					pattern.find("[vis='" + name + "']").empty().append(
							Handlebars.getTemplate("ui.workDockChart")(
									resource.map[name]));
				}
			},
			fail : function() {
				$(this).attr("vis", "nok");
				messenger
						.error("An error occurred while retrieving visualizations");
			}
		});
	}

	function resize() {
		var height = $(window).height();
		height -= $("#globalMenu").height();
		height -= $("#worksheetsSelectWrapper").height();
		height -= $("#subMenu").height();
		height -= $("#workDockMenu").height();
		height -= $("#footer").height();
		height -= 28;

		$("#WorkDock").height(height);

		var width = $("body").width();
		width -= $(".sidebar").width();
		width -= 10;

		$("#WorkDock").width(width);
	}

	function notify(observable) {
		_drawTableHeader();
		_redrawPatterns();
	}

	function selectionModel() {
		return _this._selectionModel;
	}
	
	if (_this == null) {
		_this = {
			init : init,
			resize : resize,
			notify : notify,
			clearSelectedPatterns : clearSelectedPatterns,
			clearAllPatterns : clearAllPatterns,
			redrawPatterns : _redrawPatterns,
			selectedPatterns : selectedPatterns,
			selectedPatternIndices : selectedPatternIndices,
			selectionModel : selectionModel,
			setPatternPage : setPatternPage,
			addEdge : addEdge,
			removeEdge : removeEdge
		};

		_measuresObservable.subscribe(_this);
		_visualizationsObservable.subscribe(_this);

		return _this;
	} else {
		return _this;
	}
}