function MeasuresController() {

	var _this;
	var _observers = [];

	function subscribe(observer) {
		_observers.push(observer);
	}

	function unsubscribe(observer) {
		var index = _observers.indexOf(observer);
		if (index > -1) {
			_observers.splice(index, 1);
		}
	}

	function _notifyObserver(observer) {
		var index = _observers.indexOf(observer);
		if (index > -1) {
			_observers[index].notify(_this);
		}
	}

	function _notifyAll() {
		for (var i = 0; i < _observers.length; i++) {
			_observers[i].notify(_this);
		}
	}

	function init(worksheetResource) {
		if (worksheetResource == null) {
			messenger
					.error("A problem occurred while initializing the measures!");
			return;
		}

		_this._worksheetResource = worksheetResource;

		_getMeasures();
	}

	function _getMeasures() {
		ajaxGet({
			url : getLink(_this._worksheetResource, KEY_MEASURES),
			success : _setMeasures,
			fail : function() {
				messenger.error("A problem occurred while fetching measures");
			}
		});
	}

	function _setMeasures(resource) {
		_this._measures = resource;
		_notifyAll();
	}

	function createMeasuresCheckboxes(id) {
		var checkboxes = "";

		var measures = $(_this._measures).map(function() {
			return {
				name : this.id,
				caption : this.caption,
				enabled : this.isActive
			}
		}).toArray();

		$(id).html($(Handlebars.getTemplate("ui.checkboxes")(measures)));
	}

	function selectAllMeasures(id) {
		$(id).find("input[type='checkbox']").prop('checked', true);
	}

	function deselectAllMeasures(id) {
		$(id).find("input[type='checkbox']").prop('checked', false);
	}

	function saveMeasureActivity(id) {
		var measures = $(id).find("input[type='checkbox']").map(function() {
			return {
				caption : $(this).attr("name"),
				isActive : $(this).is(":checked")
			};
		}).toArray();

		ajaxPut({
			url : getLink(_this._worksheetResource, KEY_MEASURES),
			data : JSON.stringify(measures),
			contentType : "application/json",
			processData : false,
			success : function(resource) {
				_setMeasures(resource);
				messenger.success("Measures selected successfully");
			},
			fail : function() {
				messenger.error("An error occurred while saving measures!");
			}
		});
	}

	function getActiveMeasures() {
		return $(_this._measures).filter(function() {
			return this.isActive
		}).map(function() {
			return {
				id : this.id,
				caption : this.caption,
			}
		}).toArray();
	}

	if (_this == null) {
		return _this = {
			init : init,
			createMeasureCheckBoxes : createMeasuresCheckboxes,
			selectAllMeasures : selectAllMeasures,
			deselectAllMeasures : deselectAllMeasures,
			saveMeasureActivity : saveMeasureActivity,
			getActiveMeasures : getActiveMeasures,
			subscribe : subscribe,
			unsubscribe : unsubscribe
		}
	} else {
		return _this;
	}

}