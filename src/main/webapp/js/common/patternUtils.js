function createSerializablePattern(pattern) {
	if (pattern.patternDescriptor.patternType == "Association") {
		return _serAssociation(pattern);
	} else if (pattern.patternDescriptor.patternType == "AssociationRule") {
		return _serAssociationRule(pattern);
	} else if (pattern.patternDescriptor.patternType == "ExceptionalModelPattern") {
		return _serExceptionalModelPattern(pattern);
	} else if (pattern.patternDescriptor.patternType == "AttributeList") {
		return _serAttributeList(pattern);
	} else if (pattern.patternDescriptor.patternType == "FunctionalPattern") {
		return _serFunctionalPattern(pattern);
	} else if (pattern.patternDescriptor.patternType == "Sequence") {
		return _serSequence(pattern);
	} else if (pattern.patternDescriptor.patternType == "Episode") {
		return _serEpisode(pattern);
	} else if (pattern.patternDescriptor.patternType == "EpisodeRule") {
		return _serEpisodeRule(pattern);
	}
}

function _getElementIds(elements) {
	return $(elements).map(function() {
		return this.id;
	}).toArray();
}

function _serAssociation(pattern) {
	return {
		type : "Association",
		elements : _getElementIds(pattern.patternDescriptor.elements.elements)
	};
}

function _serAssociationRule(pattern) {
	return {
		type : "AssociationRule",
		antecedent : _getElementIds(pattern.patternDescriptor.antecedent.elements),
		consequent : _getElementIds(pattern.patternDescriptor.consequent.elements)
	};
}

function _serExceptionalModelPattern(pattern) {
	return {
		type : "ExceptionalModelPattern",
		modelType : pattern.patternDescriptor.modelType.value,
		targetAttributes : _getElementIds(pattern.patternDescriptor.targetAttributes.elements),
		extension : _getElementIds(pattern.patternDescriptor.extension.elements)
	}
}

function _serAttributeList(pattern) {
	return {
		type : "AttributeList",
		elements : _getElementIds(pattern.patternDescriptor.elements.elements)
	}
}

function _serFunctionalPattern(pattern) {
	return {
		type : "FunctionalPattern",
		domain : _getElementIds(pattern.patternDescriptor.domain.elements),
		coDomain : _getElementIds(pattern.patternDescriptor.coDomain.elements)
	}
}

function _serSequence(pattern) {
	var timeSets = [];

	for ( var i in pattern.patternDescriptor.timeSets) {
		var timeSet = pattern.patternDescriptor.timeSets[i];

		var elements = _getElementIds(pattern.patternDescriptor.timeSets[i].elements);

		timeSets.push(elements);
	}

	return {
		type : "Sequence",
		timeSets : timeSets,
	}
}

function _getNodes(graphDescriptor) {
	var l = graphDescriptor.localIds.elements;
	var n = graphDescriptor.nodes.elements;

	var nodes = [];

	for (var i = 0; i < n.length; i++) {
		nodes.push([ l[i], n[i].id ]);
	}

	return nodes;
}

function _getEdges(graphDescriptor) {
	var e = graphDescriptor.edges.elements;

	var edges = [];

	for (var i = 0; i < e.length; i++) {
		edges.push([ e[i].start, e[i].end ]);
	}

	return edges;
}

function _serEpisode(pattern) {
	return {
		type : "Episode",
		nodes : _getNodes(pattern.patternDescriptor.graph),
		edges : _getEdges(pattern.patternDescriptor.graph)
	};
}

function _serEpisodeRule(pattern) {
	return {
		type : "EpisodeRule",
		antecedentNodes : _getNodes(pattern.patternDescriptor.antecedent),
		antecedentEdges : _getEdges(pattern.patternDescriptor.antecedent),
		consequentNodes : _getNodes(pattern.patternDescriptor.consequent),
		consequentEdges : _getEdges(pattern.patternDescriptor.consequent)
	};
}
