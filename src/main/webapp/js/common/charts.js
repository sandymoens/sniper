var defaultWidth = 500;

var toolTip;

function _createTooltip() {
	return d3.select("body")
		.append("div")
		.attr("class", "toolTip chartToolTip")
}

function _positionTooltip(tooltip) {
	tooltip
		.style("left", d3.event.pageX - 50 + "px")
		.style("top", d3.event.pageY - 80 + "px")
		.style("display", "inline-block");
}

function _getColors(size) {
	if(size <= 20) {
		return d3.scaleOrdinal(d3.schemeCategory20); 
	} else {
		return d3.scaleLinear()
			.domain([0, size])
			.interpolate(d3.interpolateHcl)
			.range([d3.rgb("#42a7f4"), d3.rgb('#41f489'), d3.rgb("#f4d341")]);
	}
}

function _addTooltip(element, lines) {
	element
		.on("mouseover", function() {
			tooltip = _createTooltip();
		})
		.on("mouseout", function() {
			tooltip.remove();
		})
		.on("mousemove", function(d) {
			tooltip
				.style("left", d3.event.pageX - 50 + "px")
				.style("top", d3.event.pageY - 40 - (40 * lines.length) + "px")
				.style("display", "inline-block");
								
			tooltip.html(lines.join("<br>"));
		});
}

function patternExtensionsScatter(selectorId, newData, selectCallback) {
	var width = "200";
	var height = "200";
		
	var svg = d3.select(selectorId).append("svg")
		.attr("height", "100%")
		.attr("width", "100%");

	var minMax = [[Number.MAX_VALUE, Number.MIN_VALUE], [Number.MAX_VALUE, Number.MIN_VALUE]];
	for(var d of newData) {
		minMax[0][0] = isNaN(d.values[0]) ? minMax[0][0] : Math.min(minMax[0][0], d.values[0]); 
		minMax[0][1] = isNaN(d.values[0]) ? minMax[0][1] : Math.max(minMax[0][1], d.values[0]);

		minMax[1][0] = isNaN(d.values[1]) ? minMax[1][0] : Math.min(minMax[1][0], d.values[1]); 
		minMax[1][1] = isNaN(d.values[1]) ? minMax[1][1] : Math.max(minMax[1][1], d.values[1]); 
	}
		
	var xScale = d3.scaleLinear().range([0, width - 30]).domain([minMax[0][0], minMax[0][1]]),
		xAxis = d3.axisBottom(xScale);
	
	var yScale = d3.scaleLinear().range([0, height - 30]).domain([minMax[1][1], minMax[1][0]]),
		yAxis = d3.axisLeft(yScale);
	
	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(30," + height + ")")
		.call(xAxis)
	  .append("text")
		.attr("class", "label")
		.attr("x", width)
		.attr("y", -6)
		.style("text-anchor", "end")
		.text("Calories");
	
	// y-axis
	svg.append("g")
		.attr("class", "y axis")
		.attr("transform", "translate(30, 30)")
		.call(yAxis)
	  .append("text")
		.attr("class", "label")
		.attr("transform", "rotate(-90)")
		.attr("y", 6)
		.attr("dy", ".71em")
		.style("text-anchor", "end")
		.text("Protein (g)");
	
	svg.selectAll(".dot")
			.data(newData)
		.enter().append("circle")
			.attr("id", function(d) { return d.id; })
			.attr("class", "dot buildElement")
			.attr("type", "item")
			.attr("r", 3.5)
			.attr("cx", function(d) { return 30 + xScale(isNaN(d.values[0]) ? 0 : d.values[0]); })
			.attr("cy", function(d) { return 30 + yScale(isNaN(d.values[1]) ? 0 : d.values[1]); })
			.each(function(d) {
				_addTooltip(d3.select(this), [
					"<b>Attribute:</b> " + d.attributeName,
					"<b>Proposition:</b> " + d.propositionValue,	
					"<b>X:</b> " + d.formattedValues[0],	
					"<b>Y:</b> " + d.formattedValues[1],	
				]);
			})
			.on("click", function(d) { selectCallback(d.id); });
}

function pie(selectorId, newData) {
	var data = newData;
	var selector = selectorId;

	var selectorWidth = d3.select(selector)
		.node()
		.getBoundingClientRect()
		.width;
	
	var width = selectorWidth == 0 ? defaultWidth : Math.min(selectorWidth, defaultWidth);
	var height = width;
	var radius = (Math.min(width, height) / 2) - 20;

	var color = _getColors(data.length);	

	var pie = d3.pie()
		.value(function(d) { return d.value; })
		.sort(null);

	var arc = d3.arc()
		.innerRadius(30)	
		.outerRadius(radius - 10);

	var svg = d3.select(selector).html("")
		.append("svg")
		.attr("width", width)
		.attr("height", height)
		.append("g")
		.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

	var path = svg.selectAll("path")
		.data(pie(data))
		.enter()
		.append("path")
		.attr("fill", function(d, i) {
				return color(i);
			})
		.attr("d", arc)
		.on(	"mouseover", function(d, i, j) {
			d3.select(this)
				.transition()
				.attr("transform","scale(1.1)");
			
			tooltip = _createTooltip();
		})
		.on("mouseout", function(d, i, j) {
			d3.select(this)
				.transition()
				.attr("transform","scale(1)");
			
			tooltip.remove();
		})
		.on("mousemove", function(d) {
			var lines = ["<b>Key:</b> " + d.data.name, "<b>Share:</b> " + (100. * d.data.value).toFixed(2) + "%"];
			
			tooltip
				.style("left", d3.event.pageX - 50 + "px")
				.style("top", d3.event.pageY - 40 - (40 * lines.length) + "px")
				.style("display", "inline-block");
								
			tooltip.html(lines.join("<br>"));
		});
}

function condensedHistogram(selectorId, newData) {
	var data = newData;
	var selector = selectorId;
	
	var tooltip;
	
	var color = _getColors(1);	
	
	var selectorWidth = d3.select(selector)
		.node()
		.getBoundingClientRect()
		.width;
	
	var margin = {
		top : 10,
		right : 40,
		bottom : 60,
		left : 50
	};
	
	var w = selectorWidth == 0 ? defaultWidth : Math.min(selectorWidth, defaultWidth);
	var h = 2. * w / 3.
	var width = w - margin.left - margin.right;
	var height = h - margin.top - margin.bottom;
	
	var formatCount = d3.format(",.0f");

	var svg = d3.select(selector).html("").append("svg")
		.attr("width", w)
		.attr("height", h);
	
	var g = svg.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	var dom = $(newData).map(function() {
		return this.label;
	}).toArray();
	
	var x = d3.scaleBand()
		.domain(dom)
		.rangeRound([0, width])
		.padding(0.1);
	
	bins = data;
	
	var y = d3.scaleLinear()
		.domain([ 0, d3.max(bins, function(d) {
			return d.count;
		}) + 1])
		.range([ height, 0 ]);
	
	var bar = g.selectAll(".bar")
		.data(bins)
		.enter()
		.append("g")
		.attr("class", "bar")
		.attr("transform", function(d) {
			return "translate(" + x(d.label) + "," + y(d.count) + ")";
		});
	
	bar.append("rect")
		.attr("x", 1)
		.attr("width", function(d, i) {
			return (width - 10) / dom.length;
		})
		.attr("height", function(d) {
			return height - y(d.count);
		})
		.attr("fill", color(0))
		.style("stroke", "white")
		.style("stroke-width", "1px")
		.each(function(d) {
			_addTooltip(d3.select(this), ["<b>Count:</b> " + d.count,
				d.label]);
		});
	
	g.append("g")
		.attr("class", "axis axis--x")
		.attr("transform", "translate(0," + height + ")")
		.call(d3.axisBottom(x))
		.selectAll("text")
		.attr("y", 0)
		.attr("x", 9)
		.attr("dy", ".35em")
		.attr("transform", "rotate(45)")
		.style("text-anchor", "start");
	
	g.append("g")
		.attr("class", "axis axis--y")
		.call(d3.axisLeft(y));
}

function condensedBoxPlot(selectorId, newData){
	var data = newData;
	var selector = selectorId;

	var color = _getColors(2);
	
	var selectorWidth = d3.select(selector)
		.node()
		.getBoundingClientRect()
		.width;
	
	var w = selectorWidth == 0 ? defaultWidth : Math.min(selectorWidth, defaultWidth);
	var h = 2. * w / 3.;

	var margin = {
		top: 20,
		bottom: 20,
		left: 20,
		right: 30 
	};

	var svg = d3.select(selectorId).html("").append("svg")
		.attr("height", h)
		.attr("width", w);

	var xScale = d3.scaleLinear()
		.domain((data[0].max - data[0].min < 10) ?
				[data[0].min - (data[0].max - data[0].min) * 0.1, data[0].max + (data[0].max - data[0].min) * 0.1 ] :
				[data[0].min - 10, data[0].max + 10])
		.range([margin.left, w - margin.right]);

	var yScale = d3.scaleLinear()
		.domain([-1, data.length])	
		.range([h - margin.bottom - margin.top, margin.top]);

	var xAxis = d3.axisBottom(xScale)
		.ticks(5)
		.tickSizeInner(-(h - margin.top))
		.tickSizeOuter(0);

	var g = svg.append("g")
		.attr("transform", "translate(0," + (h - margin.top) + ")")
		.attr("id", "xAxisG")
		.call(xAxis);

	var strokeWidth = "2px";
	
	svg.selectAll("g.box")
		.data(data)
		.enter()
		.append("g")
		.attr("class", "box")
		.attr("transform", function(d, i){
			return "translate(" + xScale(d.median) + "," + yScale(i) + ")"
		})
		.each(function(d,i){
			var minHandle = d3.select(this)
				.append("line")
				.attr("class", "min")
				.attr("x1", xScale(d.min) - xScale(d.median))
				.attr("x2", xScale(d.min) - xScale(d.median))
				.attr("y1", -20)
				.attr("y2", 20)
				.style("stroke", "black")
				.style("stroke-width", strokeWidth);
			
			var minLine = d3.select(this)
				.append("line")
				.attr("class", "range")
				.attr("x1", 0)
				.attr("x2", xScale(d.min) - xScale(d.median))
				.attr("y1", 0)
				.attr("y2", 0)
				.style("stroke", "black")
				.style("stroke-width", strokeWidth);
			
			var maxLine = d3.select(this)
				.append("line")
				.attr("class", "range")
				.attr("x1", xScale(d.max) - xScale(d.median))
				.attr("x2", 0)
				.attr("y1", 0)
				.attr("y2", 0)
				.style("stroke", "black")
				.style("stroke-width", strokeWidth);
	
			var maxHandle = d3.select(this)
				.append("line")
				.attr("class", "max")
				.attr("x1", xScale(d.max) - xScale(d.median))
				.attr("x2", xScale(d.max) - xScale(d.median))
				.attr("y1", -20)
				.attr("y2", 20)
				.style("stroke", "black")
				.style("stroke-width", strokeWidth);
	
			 var left = d3.select(this)
				 .append("rect")
				 .attr("class", "range")
				 .attr("x", xScale(d.q1) - xScale(d.median))
				 .attr("y", -20)
				 .attr("height", 40)
				 .attr("width", xScale(d.median) - xScale(d.q1))
				 .style("fill", color(0))
				 .style("stroke", "black")
				 .style("stroke-width", strokeWidth);
					
			 var right = d3.select(this)
				 .append("rect")
				 .attr("class", "range")
				 .attr("x", 0)
				 .attr("y", -20)
				 .attr("height", 40)
				 .attr("width", xScale(d.q3) - xScale(d.median))
				 .style("fill", color(0))
				 .style("stroke", "black")
				 .style("stroke-width", strokeWidth);
	
			var median = d3.select(this)
				.append("line")
				.attr("x1", 0)
				.attr("x2", 0)
				.attr("y1", -20)
				.attr("y2", 20)
				.style("stroke", "white")
				.style("stroke-width", "3px");

			var mean = d3.select(this)
				.append("circle")
				.attr("class", "range")
				.attr("r", 10)
				.attr("cx", xScale(d.mean) - xScale(d.median))
				.attr("cy", 0)
				.style("fill", color(1))
				.style("stroke", "black")
				.style("stroke-width", "1px");
			
			_addTooltip(minLine, 
					["<b>Min:</b> " + d.min.toFixed(2), "<b>Q1:</b> " + d.q1.toFixed(2),
						"[ " + d.min.toFixed(2) + " ; " + d.q1.toFixed(2) + " )"]);
			_addTooltip(minHandle,  
					["<b>Min:</b> " + d.min.toFixed(2), "<b>Q1:</b> " + d.q1.toFixed(2),
						"[ " + d.min.toFixed(2) + " ; " + d.q1.toFixed(2) + " )"]);
			
			_addTooltip(maxLine, 
					["<b>Q3:</b> " + d.q3.toFixed(2), "<b>Max:</b> " + d.max.toFixed(2),
						"[ " + d.q3.toFixed(2) + " ; " + d.max.toFixed(2) + " ]"]);
			_addTooltip(maxHandle, 
					["<b>Q3:</b> " + d.q3.toFixed(2), "<b>Max:</b> " + d.max.toFixed(2),
						"[ " + d.q3.toFixed(2) + " ; " + d.max.toFixed(2) + " ]"]);
			
			_addTooltip(left, 
					["<b>Q1:</b> " + d.q1.toFixed(2), "<b>Median:</b> " + d.median.toFixed(2),
						"[ " + d.q1.toFixed(2) + " ; " + d.median.toFixed(2) + " )"]);
			
			_addTooltip(right, 
					["<b>Median:</b> " + d.median.toFixed(2), "<b>Q3:</b> " + d.q3.toFixed(2),
						"( " + d.median.toFixed(2) + " ; " + d.q3.toFixed(2) + " )"]);
			
			_addTooltip(median, ["<b>Median:</b> " + d.median.toFixed(2)]);
			
			_addTooltip(mean, ["<b>Mean:</b> " + d.mean.toFixed(2)]);
		});
}

function normalDistribution(selectorId, newData) {
	var data = newData;
	
	var selectorWidth = d3.select(selectorId)
		.node()
		.getBoundingClientRect()
		.width;
	
	var margin = {
		top : 10,
		right : 40,
		bottom : 60,
		left : 50
	};
	
	var w = selectorWidth == 0 ? defaultWidth : Math.min(selectorWidth, defaultWidth);
	var h = 2. * w / 3.
	var width = w - margin.left - margin.right;
	var height = h - margin.top - margin.bottom;
	
	var svg = d3.select(selectorId).html("").append("svg")
		.attr("height", h)
		.attr("width", w);

	var array1 = _random_normal_Dist(data.mean, data.stdev);

	var x = d3.scaleLinear()
    	.rangeRound([0, width]);

	var min_d = d3.min(array1, function (d) { return d.q; });
	var max_d = d3.max(array1, function (d) { return d.q; });
	var max_p = d3.max(array1, function (d) { return d.p; });
	
	x.domain([min_d, max_d]).nice;

	var y = d3.scaleLinear()
		.domain([0, max_p])
		.range([ height, 0 ]);

	var g = svg.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	g.append("g")
		.attr("class", "axis axis--x")
		.attr("transform", "translate(0," + height + ")")
		.call(d3.axisBottom(x))
		.selectAll("text")
		.attr("y", 0)
		.attr("x", 9)
		.attr("dy", ".35em")
		.attr("transform", "rotate(45)")
		.style("text-anchor", "start");

	var line = d3.line()
      .x(function (d) { return x(d.q); })
      .y(function (d) { return y(d.p); });

	var norm = svg.append("path")
      .datum(array1)
      .attr("class", "line")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .attr("d", line)
      .style("fill", "#1f77b4");
  
	_addTooltip(norm, 
			["<b>Mean:</b> " + data.mean.toFixed(2), "<b>Stdev:</b> " + data.stdev.toFixed(2)])
}


function _random_normal_Dist(mean, stdev) {
	var sampleSize = 100;
    var data = [];
    var diff = 8 * stdev / sampleSize;
    
    var q = mean - 4 * stdev;
    
    for(var i = 0; i < sampleSize; i++) {
        p = jStat.normal.pdf(q, mean, stdev);
        arr = {
            "q": q,
            "p": p
        }
        data.push(arr);
        q += diff;
    };

    return data;
}
