var HttpStatus = {
	SUCCESS : "200",
	BAD_REQUEST : "400",
}

function ajaxCall(parameters) {
	var failed = false;

	if (parameters.url === undefined) {
		messenger.error("Url parameter can not be empty");
		failed = true;
	}

	if (parameters.type === undefined) {
		messenger.error("Type parameter can not be empty");
		failed = true;
	}

	if (failed) {
		return;
	}

	parameters.url = parameters.url.startsWith("http") ? parameters.url
			: URL_HANDLE + "api/v1/" + parameters.url;

	var object = {
		url : parameters.url,
		type : parameters.type,
		context : document.body
	};

	if (parameters.data !== undefined) {
		object.data = parameters.data;
	}
	if (parameters.contentType !== undefined) {
		object.contentType = parameters.contentType;
	}
	if (parameters.processData !== undefined) {
		object.processData = parameters.processData;
	}

	var call = $.ajax(object);

	if (parameters.success !== undefined) {
		call.done(parameters.success);
	}

	if (parameters.fail !== undefined) {
		call.fail(parameters.fail);
	} else {
		call.fail(function(response) {
			messenger.error(response.responseText);
		});
	}
	
	return call;
}

function ajaxGet(parameters) {
	parameters.type = "GET";
	return ajaxCall(parameters);
}

function ajaxPut(parameters) {
	parameters.type = "PUT";
	return ajaxCall(parameters);
}

function ajaxPost(parameters) {
	parameters.type = "POST";
	return ajaxCall(parameters);
}

function ajaxPatch(parameters) {
	parameters.type = "PATCH";
	return ajaxCall(parameters);
}

function ajaxDelete(parameters) {
	parameters.type = "DELETE";
	return ajaxCall(parameters);
}

function debouncer(func, timeout) {
	var timeoutID, timeout = timeout || 200;
	return function() {
		var scope = this, args = arguments;
		clearTimeout(timeoutID);
		timeoutID = setTimeout(function() {
			func.apply(scope, Array.prototype.slice.call(args));
		}, timeout);
	}
}

function getLink(resource, linkToGet) {
	if (resource._links === undefined) {
		return "";
	}

	if (resource._links[linkToGet] === undefined) {
		return "";
	}

	return resource._links[linkToGet].href;

	for (link in resource._links) {
		console.log(link);
		console.log(resource.links[link].rel);
		console.log(resource.links[link].href);
		console.log();

		if (resource.links[link].rel === linkToGet) {
			return resource.links[link].href;
		}
	}

	return "";
}

function findGetParameter(parameterName) {
	var result = null, tmp = [];
	location.search.substr(1).split("&").forEach(function(item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName)
			result = decodeURIComponent(tmp[1]);
	});
	return result;
}

function buildUrl(url, params) {
	if (url == "" || url == null) {
		return "";
	}

	var concatParams = $(params).map(function() {
		return this.key + "=" + this.value;
	}).toArray();
	return url + "?" + concatParams.join("&");
}

function _prepareSimpleParameter(parameter) {
	return {
		type : parameter.type,
		name : parameter.name,
		value : parameter.value,
		description : parameter.description,
		solutionHint : parameter.solutionHint
	}
}

function _prepareRangeEnumerableValues(parameter) {
	return $(parameter.range).map(function() {
		return {
			caption : this,
			selected : this.valueOf() === String(parameter.value)
		};
	}).toArray();
}

function _prepareRangeEnumerableParameter(parameter) {
	return {
		type : parameter.type,
		name : parameter.name,
		values : _prepareRangeEnumerableValues(parameter),
		description : parameter.description,
		solutionHint : parameter.solutionHint
	};
}

function _prepareSubcollectionValues(parameter) {
	return $(parameter.range).map(function() {
		return {
			caption : this,
			selected : parameter.values.indexOf(this.valueOf()) != -1
		};
	}).toArray();
}

function _prepareSubcollectionParameter(parameter) {
	return {
		type : parameter.type,
		name : parameter.name,
		values : _prepareSubcollectionValues(parameter),
		description : parameter.description,
		solutionHint : parameter.solutionHint
	};
}

function prepareParameters(parameters) {
	return $(parameters).filter(function() {
		return !this.hidden && this.name !== "Propositions";
	}).map(
			function() {
				if (this.type === "Text" || this.type === "Integer"
						|| this.type === "Double") {
					return _prepareSimpleParameter(this);
				} else if (this.type === "RangeEnumerable") {
					return _prepareRangeEnumerableParameter(this);
				} else if (this.type === "Subcollection") {
					return _prepareSubcollectionParameter(this);
				}
			}).toArray();
}

function getPageLinks(resource) {
	return $(Handlebars.getTemplate("ui.pageLinks")(
			{
				pageNumber : resource.page.number + 1,
				totalPages : resource.page.totalPages == 0 ? 1
						: resource.page.totalPages,
				firstPage : getLink(resource, "first"),
				lastPage : getLink(resource, "last"),
				prevPage : getLink(resource, "prev"),
				nextPage : getLink(resource, "next")
			}));
}

function alertBox(message) {
	var alertModal = $(Handlebars.getTemplate("ui.alertBox")(message));
	alertModal.modal();

	alertModal.on({
		click : function() {
			alertModal.modal("hide").data("bs.modal", null);

			setTimeout(function() {
				alertModal.remove();
			}, 500);
		}
	}, ".closeButton");
}

function saveFile(fileName, data) {
	var blob = new Blob([ data ], {
		type : "text/plain;charset=utf-8"
	});
	saveAs(blob, fileName);
}

function checkRequiredOptions(selectors) {
	if(typeof(selectors) === "string" || selectors instanceof String) {
		selectors = [selectors];
	}
	
	var reqMissing = false;

	for(selector of selectors) {
		$(selector + " *[data-required]").each(
				function() {
					if ($(this).val() == "") {
						reqMissing = true;
						$(this).closest(".data-row").find(".data-label").addClass(
								"required")
					} else {
						$(this).closest(".data-row").find(".data-label")
								.removeClass("required")
					}
				});
	}
	
	return reqMissing;
}

function isEquivalent(a, b) {
	if (a == null || b == null) {
		return false;
	}

	// Create arrays of property names
	var aProps = Object.getOwnPropertyNames(a);
	var bProps = Object.getOwnPropertyNames(b);

	// If number of properties is different,
	// objects are not equivalent
	if (aProps.length != bProps.length) {
		return false;
	}

	for (var i = 0; i < aProps.length; i++) {
		var propName = aProps[i];

		// If values of same property are not equal,
		// objects are not equivalent
		if (a[propName] !== b[propName]) {
			return false;
		}
	}

	// If we made it this far, objects
	// are considered equivalent
	return true;
}

$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);

	return results == null ? null : results[1];
}

function copyToClipboard(str) {
	const el = document.createElement('textarea');
	el.value = str;
	el.setAttribute('readonly', '');
	el.style.position = 'absolute';
	el.style.left = '-9999px';
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	document.body.removeChild(el);
}

function readFromClipboard(callback) {
	navigator.clipboard.readText()
	  .then(text => { callback(text); })
	  .catch(err => { messenger.error('Failed to read clipboard contents: ', err); });
}
