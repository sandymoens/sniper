var defaultWidth = 300;

var graphGlobalController = null;

var selection = null;

function _bindEvents() {
	$("#WorkDock").on({
		click : _clearNodeSelection
	});
}

function _clearNodeSelection() {
	d3.selectAll("rect").classed("selected", false);
	selection = null;
}

function _handleNodeClickEvent(descriptorId, nodeId, node) {
	d3.selectAll("rect").classed("selected", false);
	
	if (selection == null || (selection != null && (selection.descriptorId != descriptorId || selection.nodeId != nodeId))) {
		selection = {
			descriptorId : descriptorId,
			nodeId : nodeId
		};
		d3.select(node).classed("selected", true);
	} else {
		selection = null;
	}
}

function _handleEdgeClickEvent(descriptorId, edge) {
	graphGlobalController.workDockController().removeEdge(
			descriptorId, edge.start, edge.end);
}

function _handleAddEdgeClickEvent(descriptorId, nodeId, node) {
	if (graphGlobalController == null) {
		messenger.error("Invalid worksheet resource");
		return;
	}

	if (selection == null) {
		messenger.error("No start node selected");
		return;
	}
	
	if (selection.descriptorId != descriptorId) {
		messenger.error("Start node should be in same episode as the end node");
		return;
	}

	if (selection.selectorId == descriptorId && selection.nodeId == nodeId) {
		messenger.error("Start node should not be the same as end node");
		return;
	}
	
	graphGlobalController.workDockController().addEdge(
			selection.descriptorId,
			selection.nodeId, nodeId);
}

function _handleDeleteEdgeClickEvent(descriptorId, edge) {
	graphGlobalController.workDockController().removeEdge(
			descriptorId, edge.start, edge.end);
}

function _computePositions(localIds, edges) {
	var startToEndEdgeMap = {};
	
	for(var edge of edges) {
		if(!(edge.start in startToEndEdgeMap)) {
			startToEndEdgeMap[edge.start] = [];
		}
		
		startToEndEdgeMap[edge.start].push(edge.end);
	}
	
	var positions = [];

	var nAll = new Set(localIds);
	var eAll = new Set();

	for(var edge of edges) {
		eAll.add(edge.end);
	}

	var i = 0;
	
	do {
		var diff = new Set([...nAll].filter(x => !eAll.has(x)));
		
		positions[positions.length] = diff;
		
		nAll = eAll;
		eAll = new Set();
		
		for (var n of nAll) {
			var edges = n in startToEndEdgeMap ? startToEndEdgeMap[n] : [];
			for (edge of edges) {
				eAll.add(edge);
			}
		}
		
		i++;
		
		if(i == 100) {
			return "PROBLEM DETECTED";
		}
	} while (nAll.size != 0);

	var pos = {};
	
	for(var ix in positions) {
		var p = positions[ix];
		var i = 0;
		for(var n of p) {
			pos[n] = [parseInt(ix), i++];
		}
	}

	return pos;
}

function drawEpisode(newData, resource) {
	worksheetResource = resource;
	
	var map = {};

	for(var i in newData.nodes) {
		map[newData.localIds[i]] = newData.nodes[i];
	}
	
	var data = newData;

	var canvas = d3.select("[descriptorid='" + newData.id + "']");
	var svg = canvas.append("svg").attr("pointer-events", "none");	

	svg.append("defs").append("marker")
		.attr("id", "arrow")
		.attr("markerUnits", "strokeWidth")
		.attr("markerWidth", "12")
		.attr("markerHeight", "12")
		.attr("viewBox", "0 0 12 12")
		.attr("refX", "6")
		.attr("refY", "6")
		.attr("orient", "auto")
		.append("path")
		.attr("d", "M2,2 L10,6 L2,10 L2,2")
		.style("fill", "black")
		;
		
	var positions = _computePositions(newData.localIds, newData.edges);
	
	var scale = 0.2;
	
	if(positions == "PROBLEM DETECTED") {
		canvas.attr("width", 1000 * scale)
		.attr("height", 500 * scale).scaleCanvas({
			scale : scale
		});
		
		canvas.drawText({
			layer : true,
			text : "PROBLEM DRAWING EPISODE",
			fontSize : 60,
			fillStyle : "black",
			strokeWidth : 4,
			x : 500,
			y : 250,
		});
		
		return;
	}

	var maxH = 0, maxV = 0;
	
	for(key in positions) {
		var pos = positions[key];
		maxH = Math.max(maxH, pos[0]);
		maxV = Math.max(maxV, pos[1]);
	}
	
	maxH += 1;
	maxV += 1;

	
	var largestLength = 0;
	
	for(var node of newData.nodes) {
		largestLength = Math.max(largestLength, node.attributeName.length);
		largestLength = Math.max(largestLength, node.propositionValue.length);
	}
	
	var nodeWidth = 10 * largestLength;
	var nodeHeight = 50;
	var nodeWidthOffset = nodeWidth / 2;
	var nodeHeightOffset = nodeHeight / 2;

	var svgWidth = (20 + nodeWidth * maxH + (65 * (maxH - 1)));
	var svgHeight = (5 + (15 + nodeHeight) * maxV);
	
	svg.attr("width", svgWidth).attr("height", svgHeight);
	
	svg.append("rect")
		.attr("x", 1)
		.attr("y", 1)
		.attr("width", svgWidth - 2)
		.attr("height", svgHeight - 2)
		.attr("stroke","black")  
        .attr("stroke-width", 1)
        .attr("fill", "transparent");

	var texts = svg.selectAll("texts")
		.data(Object.keys(positions))
		.enter()
		.append("text")
		.attr("y", function (d) { return _episodeMapY(positions[d]) - 15; })
// .style("text-anchor", "middle")
		.style("dominant-baseline", "central");
	
	texts.append('svg:tspan')
		  .attr('x', function (d) { return 5 + _episodeMapX(positions[d], nodeWidth); })
		  .attr('dy', 5)
		  .text(function(d) { return map[d].attributeName; });
	var tspan = texts.append('svg:tspan')
		  .attr('x', function (d) { return 5 + _episodeMapX(positions[d], nodeWidth); })
		  .attr('dy', 20)
		  .text(function(d) { return map[d].propositionValue; });
	
	var edges = svg.selectAll("edges")
		.data(newData.edges)
		.enter()
		.append("g");
	
	edges
		.append("path")
		 .attr("d", function(d){
			 var xStart = _episodeMapX(positions[d.start], nodeWidth);
			 var yStart = _episodeMapY(positions[d.start]);
			 var xEnd = _episodeMapX(positions[d.end], nodeWidth);
			 var yEnd = _episodeMapY(positions[d.end]);
			 
			 return "M" + (xStart + nodeWidth + 10) + "," + yStart
				 + " C" + (xStart + nodeWidth + 35) + "," + yStart
				 + " " + (xEnd - 35) + "," + yEnd
				 + " " + (xEnd - 10) + "," + yEnd;
		 })
        .attr("stroke","black")  
        .attr("stroke-width", 1)
        .attr("fill", "transparent")
        .attr("marker-end","url(#arrow)")
		.attr("pointer-events", "all")
		.on("mouseover", function() {
			 d3.select(this).transition().duration(50).attr("stroke-width", 3);
		})
		.on("mouseout", function() {
			 d3.select(this).transition().duration(50).attr("stroke-width", 1);
		});
	
	edges.append("circle")
		.attr("cx", function (d) { return _episodeMapX(positions[d.end], nodeWidth) - 30; })
		.attr("cy", function (d) { return (_episodeMapY(positions[d.start]) + _episodeMapY(positions[d.end])) / 2; })
		.attr("r", 10)
		.attr("pointer-events", "all")
		.attr("class", "deleteEdgeButton showOnParentHover")
		.style("stroke", "black")
		.style("fill", "white")
        .on("click", function(edge) {
    		_handleDeleteEdgeClickEvent(newData.id, edge);
	});

	edges.append("text")
		.attr("x", function (d) { return _episodeMapX(positions[d.end], nodeWidth) - 30; })
		.attr("y", function (d) { return (_episodeMapY(positions[d.start]) + _episodeMapY(positions[d.end])) / 2; })
		.style("text-anchor", "middle")
		.style("dominant-baseline", "central")
		.attr("class", "showOnParentHover")
		.text("x");
	
	var nodes = svg.selectAll("nodes")
		.data(Object.keys(positions))
		.enter()
		.append("g")
		.attr("id", function(d) { return d; })
		.attr("data-nodeId", function(d) { return map[d].id; })
		.attr("data-attributeName", function(d) { return map[d].attributeName; })
		.attr("data-propositionValue", function(d) { return map[d].propositionValue; })
		.attr("type", "event")
		.attr("class", "buildElement");
	
	nodes
		.append("rect")
		.attr("x", function (d) { return _episodeMapX(positions[d], nodeWidth); })
		.attr("y", function (d) { return _episodeMapY(positions[d]) - nodeHeightOffset; })
		.attr("width", nodeWidth)
		.attr("height", 50)
		.attr("pointer-events", "all")
// .attr("class", "buildElement")
		.style("stroke", "black")
		.style("fill", "transparent")
		.on("click", function(nodeId) {
			_handleNodeClickEvent(newData.id, nodeId, this);
		});
	
	nodes.append("circle")
		.attr("cx", function (d) { return _episodeMapX(positions[d], nodeWidth) + nodeWidth; })
		.attr("cy", function (d) { return _episodeMapY(positions[d]) - nodeHeightOffset; })
		.attr("r", 10)
		.attr("pointer-events", "all")
		.style("stroke", "black")
		.style("fill", "white")
		.attr("class", "deleteButton showOnParentHover");
	
	nodes.append("text")
		.attr("x", function (d) { return _episodeMapX(positions[d], nodeWidth) + nodeWidth; })
		.attr("y", function (d) { return _episodeMapY(positions[d]) - nodeHeightOffset; })
		.style("text-anchor", "middle")
		.style("dominant-baseline", "central")
		.attr("class", "showOnParentHover")
		.text("x");
	
	nodes.append("circle")
		.attr("cx", function (d) { return _episodeMapX(positions[d], nodeWidth); })
		.attr("cy", function (d) { return _episodeMapY(positions[d]); })
		.attr("r", 10)
		.attr("pointer-events", "all")
		.style("stroke", "black")
		.style("fill", "white")
		.attr("class", "showOnParentSelected")
		.on("click", function(nodeId) {
			_handleAddEdgeClickEvent(newData.id, nodeId, this);
			d3.event.stopPropagation();
		});
	
	nodes.append("text")
		.attr("x", function (d) { return _episodeMapX(positions[d], nodeWidth); })
		.attr("y", function (d) { return _episodeMapY(positions[d]); })
		.style("text-anchor", "middle")
		.style("dominant-baseline", "central")
		.attr("class", "showOnParentSelected")
		.text("+");
}

function drawEpisodeRule(newData, resource) {
	worksheetResource = resource;
	
	var data = newData;
	
	drawEpisode( {
		id : data.patternDescriptor.antecedent.nodes.id,
		nodes : data.patternDescriptor.antecedent.nodes.elements,
		localIds : data.patternDescriptor.antecedent.localIds.elements,
		edges : data.patternDescriptor.antecedent.edges.elements
	}, resource);
	drawEpisode( {
		id : data.patternDescriptor.consequent.nodes.id,
		nodes : data.patternDescriptor.consequent.nodes.elements,
		localIds : data.patternDescriptor.consequent.localIds.elements,
		edges : data.patternDescriptor.consequent.edges.elements
	}, resource);

}

function _episodeMapX(pos, nodeWidth) {
	return 10 + (nodeWidth + 65) * pos[0];
}

function _episodeMapY(pos) {
	return 35 + 65 * pos[1];
}

$(function() {
	_bindEvents();
});