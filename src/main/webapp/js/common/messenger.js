function Messenger() {

	var _this;

	var _messengers = [];

	function addMessenger(messenger) {
		_messengers.push(messenger);
	}
	
	function error(message) {
		for(var messenger of _messengers) {
			messenger.error(message);	
		}
	}
	
	function info(message) {
		for(var messenger of _messengers) {
			messenger.info(message);	
		}
	}
	
	function success(message) {
		for(var messenger of _messengers) {
			messenger.success(message);	
		}
	}

	if (_this == null) {
		return _this = {
			addMessenger : addMessenger,
			error : error,
			info : info,
			success : success
		}
	} else {
		return _this;
	}
}

function MessageBoxMessenger() {

	var _this;

	function _showMessageBox(messageBox) {
		$("body").append(messageBox);

		messageBox.delay(1000).fadeOut(3000, function() {
			$(this).remove();
		});
	}

	function error(message) {
		_showMessageBox($("<div class='alert alert-warning messageBox'>"
				+ message + "</div>"));
	}
	
	function info(message) {
		_showMessageBox($("<div class='alert alert-info messageBox'>"
				+ message + "</div>"));
	}

	function success(message) {
		_showMessageBox($("<div class='alert alert-success messageBox'>"
				+ message + "</div>"));
	}

	if (_this == null) {
		return _this = {
			error : error,
			info : info,
			success : success
		}
	} else {
		return _this;
	}

}

function OutputLogMessenger(selector) {
	
	var _this;
	var _outputLog = $(selector);
	
	function _log(message) {
		if(_outputLog.length == 0) {
			return;
		}
		
		for(o of _outputLog) {
			_outputLog.append(new Date().toString() + ": " + message + "<br><br>");
			var height = _outputLog.get(0).scrollHeight;
			_outputLog.animate({
				scrollTop : height
			}, 500);
		}
	}
	
	function error(message) {
		_log("[ERROR]: " + message);
	}
	
	function info(message) {
		_log("[INFO]: " + message);
	}
	
	function success(message) {
		_log("[SUCCESS]: " + message);
	}
	
	if (_this == null) {
		return _this = {
				error : error,
				info : info,
				success : success
		}
	} else {
		return _this;
	}
	
}
