function GlobalMenuController() {

	var _this;

	function init() {
		$("#globalMenu .sm").smartmenus({
			mainMenuSubOffsetX : -20,
			mainMenuSubOffsetY : 15,
			showTimeout : 0,
			subMenusSubOffsetX : 6,
			subMenusSubOffsetY : -6
		});

		_bindEvents();
	}

	function _bindEvents() {
		$("#profileButton").on({
			click : function() {
				window.location = URL_HANDLE + "user";
			}
		});

		$("#usersButton").on({
			click : function() {
				window.location = URL_HANDLE + "admin/users";
			}
		});

		$("#dataTablesButton").on({
			click : function() {
				window.location = URL_HANDLE + "dataTables?page=view";
			}
		});

		$("#schemesButton").on({
			click : function() {
				window.location = URL_HANDLE + "schemes?page=view";
			}
		});

		$("#workspacesButton").on({
			click : function() {
				window.location = URL_HANDLE + "workspaces?page=view";
			}
		});

		$("#logoutButton").on({
			click : function() {
				$("#logoutForm").submit();
			}
		});
	}

	if (_this == null) {
		return _this = {
			init : init
		}
	} else {
		return _this;
	}

}