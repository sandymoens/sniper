$(function() {
	Handlebars.getTemplate = function(name) {
		if (Handlebars.templates === undefined
				|| Handlebars.templates[name] === undefined) {
			$.ajax({
				url : "templates/" + name + ".handlebars",
				success : function(data) {
					if (Handlebars.templates === undefined) {
						Handlebars.templates = {};
					}
					Handlebars.templates[name] = Handlebars.compile(data);
				},
				async : false
			});
		}
		return Handlebars.templates[name];
	};

	Handlebars.registerHelper("equal", function(v1, v2, options) {
		if (arguments.length < 3)
			throw new Error("Handlebars Helper equal needs 2 parameters");
		if (v1 === v2) {
			return options.fn(this);
		}
		return options.inverse(this);
	});

	Handlebars.registerHelper("empty", function(v1, options) {
		if (arguments.length < 2)
			throw new Error("Handlebars Helper empty needs 1 parameters");
		if (v1.length === 0) {
			return options.fn(this);
		}
		return options.inverse(this);
	});

	Handlebars.registerHelper('breakLines', function(text) {
		text = Handlebars.Utils.escapeExpression(text);
		text = text.replace(/(\r\n|\n|\r)/gm, '<br>');
		return new Handlebars.SafeString(text);
	});

	Handlebars.registerHelper("capitalizeFirst", function(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	});

	Handlebars.registerHelper('json', function(context) {
		return JSON.stringify(context);
	});

});
