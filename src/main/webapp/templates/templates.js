(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['graphics.emptyProposition'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"buildElement emptyElement\"></div>\n";
},"useData":true});
templates['ui.alertBox'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" data-backdrop=\"static\" data-keyboard=\"false\">\n	<div class=\"modal-dialog\">\n		<div class=\"modal-content\">\n			<div style=\"height: auto !important;\" class=\"modal-header menuBar shadow-z-2\">\n				<div class=\"modal-title\">Alert</div>\n			</div>\n			<div class=\"modal-body\">\n				"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "\n			</div>\n			<div class=\"modal-footer\">\n				<button class=\"btn btn-info btn-raised btn-xs closeButton\">Close</button>\n			</div>\n		</div>\n	</div>\n</div>\n";
},"useData":true});
templates['ui.algorithmMiningModal'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return " <div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" data-backdrop=\"static\" data-keyboard=\"false\">\n	<div class=\"modal-dialog\">\n		<div class=\"modal-content\">\n			<div style=\"height: auto !important;\" class=\"modal-header menuBar shadow-z-2\">\n				<div class=\"modal-title\">"
    + alias4(((helper = (helper = helpers.type1 || (depth0 != null ? depth0.type1 : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type1","hash":{},"data":data}) : helper)))
    + " is running, please bare with us</div>\n			</div>\n			<div class=\"modal-body\">\n				<div class=\"progress\">\n					<div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:100%\"></div>\n				</div>\n			</div>\n			<div class=\"modal-footer\">\n				<button type=\"button\" class=\"btn btn-default stopButton\">Stop "
    + alias4(((helper = (helper = helpers.type2 || (depth0 != null ? depth0.type2 : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type2","hash":{},"data":data}) : helper)))
    + "</button>\n			</div>\n		</div>\n	</div>\n</div>\n  ";
},"useData":true});
templates['ui.attributeInfo'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=container.escapeExpression;

  return "						<div class=\"row\">\n							<div class=\"col-md-6\">\n								"
    + alias1(((helper = (helper = helpers.key || (data && data.key)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"key","hash":{},"data":data}) : helper)))
    + "\n							</div>\n							<div class=\"col-md-6\">\n								"
    + alias1(container.lambda(depth0, depth0))
    + "\n							</div>\n						</div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "	<div class=\"col-xl-12\">\n		<div class=\"padded\">\n			<div class=\"attributeChart boxed myshadow\">\n				<div class=\"container-fluid\">\n					<div class=\"row boxTitle\">\n						<div class=\"col\">\n							<span class=\"title\">\n							"
    + alias3((helpers.capitalizeFirst || (depth0 && depth0.capitalizeFirst) || alias2).call(alias1,depth0,{"name":"capitalizeFirst","hash":{},"data":data}))
    + "\n							</span>\n						</div>\n					</div>\n"
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,depth0,"metricHistogram",{"name":"equal","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "					<div class=\"row\">\n						<div class=\""
    + alias3(container.lambda(depth0, depth0))
    + "\">\n						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n	</div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "						<div class=\"row\">\n							<div class=\"col-sm-6\">\n								Number of bins:\n							</div>\n							<div class=\"col-sm-6\">\n								<input class=\"metricHistogramSpinner form-control\" type=\"number\" value=\"5\" step=\"1\" min=\"2\">\n							</div>\n						</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"col-md-12\">\n	<div class=\"padded\">\n		<div class=\"boxed myshadow\">\n			<div class=\"container-fluid\">\n				<div class=\"row boxTitle\">\n					<div class=\"col\">\n						<span class=\"title\">\n							Meta info\n						</span>\n					</div>\n				</div>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.info : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\n		</div>\n	</div>\n</div>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.visualizations : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['ui.buildElementDragHelper'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), buffer = 
  "	<div>\n";
  stack1 = ((helper = (helper = helpers.elements || (depth0 != null ? depth0.elements : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"elements","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data}),(typeof helper === "function" ? helper.call(alias1,options) : helper));
  if (!helpers.elements) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.incomplete : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "			<div class=\"elContainer\" style=\"max-height: 50px;\">\n"
    + ((stack1 = container.invokePartial(partials["graphics.workElement"],depth0,{"name":"graphics.workElement","data":data,"indent":"\t\t\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "			</div>\n			<br>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "			<div style=\"font-weight: bold; font-size: 20px;\">...</div>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), buffer = 
  "	<div class=\"elContainer\">\n";
  stack1 = ((helper = (helper = helpers.elements || (depth0 != null ? depth0.elements : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"elements","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data}),(typeof helper === "function" ? helper.call(alias1,options) : helper));
  if (!helpers.elements) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.incomplete : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["graphics.workElement"],depth0,{"name":"graphics.workElement","data":data,"indent":"\t\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.singletons : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"usePartial":true,"useData":true});
templates['ui.checkboxes'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<label>\n		<input type=\"checkbox\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.enabled : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n		&nbsp;"
    + alias4(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"caption","hash":{},"data":data}) : helper)))
    + "\n	</label>\n	<br>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "checked";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['ui.dataTableUploadSuccessView'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"row\">\n	<div class=\"col-md-3\">\n	</div>\n	<div class=\"col-md-9\">\n		<div class=\"row\">\n			<div class=\"col-md-6\">\n				<button id=\"viewDataTableButton\" class=\"btn-outline-secondary\">View data table</button>\n			</div>\n			<div class=\"col-md-6\">\n				<button id=\"createSchemeButton\" class=\"btn-outline-secondary\">Create scheme</button>\n			</div>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['ui.div'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\""
    + alias4(((helper = (helper = helpers.classes || (depth0 != null ? depth0.classes : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"classes","hash":{},"data":data}) : helper)))
    + "\"></div>\n";
},"useData":true});
templates['ui.editablePreviewTable'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "				<th>\n					<input type=\"checkbox\" checked>\n					<br>\n					<input type=\"text\" value=\""
    + container.escapeExpression(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"caption","hash":{},"data":data}) : helper)))
    + "\">\n					<br>\n					<select class=\"chosen-select\">\n						<option value=\"CATEGORIC\" "
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"categoric",{"name":"equal","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">categoric</option>\n						<option value=\"METRIC\" "
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"metric",{"name":"equal","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">metric</option>\n						<option value=\"DATE\" "
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"date",{"name":"equal","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">date</option>\n					</select>\n					<br>\n					<button class=\"btn btn-secondary\" role=\"iconpicker\"></button>\n                </th>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "selected";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "			<tr>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</tr>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "					<td>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</td>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<table class=\"previewTable\">\n	<thead>\n		<tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.typedAttributes : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</tr>\n	</thead>\n	<tbody>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</tbody>\n</table>\n";
},"useData":true});
templates['ui.formWithHelp'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "			<div class=\"row\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.hidden : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " parameterName=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" parameterHelp=\""
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "\" parameterHint=\""
    + alias4(((helper = (helper = helpers.solutionHint || (depth0 != null ? depth0.solutionHint : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"solutionHint","hash":{},"data":data}) : helper)))
    + "\">\n				<div class=\"col-sm-5 subTitle\">\n					"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\n				</div>\n				<div class=\"col-sm-7\">\n"
    + ((stack1 = container.invokePartial(partials["ui.input"],depth0,{"name":"ui.input","data":data,"indent":"\t\t\t\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "				</div>\n			</div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "style=\"display: none;\"";
},"4":function(container,depth0,helpers,partials,data) {
    return "disabled";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"container-fluid\">\n	<form class=\"parameterForm\" id=\""
    + alias4(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"formId","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.inputs : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		<div class=\"row\">\n			<div class=\"col-sm-5\"></div>\n			<div class=\"col-sm-7\">\n				<button class=\"btn-outline-secondary "
    + alias4(((helper = (helper = helpers.buttonClass || (depth0 != null ? depth0.buttonClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonClass","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.disabled : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " data-dismiss=\"modal\">"
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "</button>\n			</div>\n		</div>\n	</form>\n	<div class=\"row\">\n		<div class=\"container-fluid\">\n			<div class=\"boxed padded\" id=\"parameterHelpBox\"></div>\n		</div>\n	</div>\n</div>\n";
},"usePartial":true,"useData":true});
templates['ui.helpBoxMessage'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "";
},"3":function(container,depth0,helpers,partials,data) {
    return "		"
    + container.escapeExpression((helpers.breakLines || (depth0 && depth0.breakLines) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.parameterHelp : depth0),{"name":"breakLines","hash":{},"data":data}))
    + "\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "		<br>\n		<br>\n		<span class=\"subTitle\">Hint:</span>\n		"
    + container.escapeExpression((helpers.breakLines || (depth0 && depth0.breakLines) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.parameterHint : depth0),{"name":"breakLines","hash":{},"data":data}))
    + "\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "<div>\n	<span class=\"title\">"
    + container.escapeExpression(((helper = (helper = helpers.parameterName || (depth0 != null ? depth0.parameterName : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"parameterName","hash":{},"data":data}) : helper)))
    + "</span>\n	<br>\n	<br>\n"
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.parameterHelp : depth0),"",{"name":"equal","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.parameterHint : depth0),"",{"name":"equal","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
templates['ui.imgModal'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"modalImgDiv\"><img src=\""
    + container.escapeExpression(((helper = (helper = helpers.src || (depth0 != null ? depth0.src : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"src","hash":{},"data":data}) : helper)))
    + "\"></div>\n";
},"useData":true});
templates['ui.infoDock'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "				<li class=\"optionSpan "
    + ((stack1 = helpers.unless.call(alias1,(data && data.first),{"name":"unless","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "Option\" data-div-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "Div\">"
    + alias4(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"caption","hash":{},"data":data}) : helper)))
    + "</li>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "selected";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "		<div class=\"tabDiv\" "
    + ((stack1 = helpers["if"].call(alias1,(data && data.first),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + " id=\""
    + container.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "Div\"></div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "";
},"7":function(container,depth0,helpers,partials,data) {
    return "style=\"display: none;\"";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=helpers.blockHelperMissing, buffer = 
  "<div class=\"menu menuBar subMenuBar\">\n	<div class=\"tableCell centeredText\">\n		<ul id=\"infoDockTabs\">\n";
  stack1 = ((helper = (helper = helpers.tabs || (depth0 != null ? depth0.tabs : depth0)) != null ? helper : alias2),(options={"name":"tabs","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.tabs) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "		</ul>\n	</div>\n</div>\n<div class=\"fixedHeight170Scroll width100Pct\">\n";
  stack1 = ((helper = (helper = helpers.tabs || (depth0 != null ? depth0.tabs : depth0)) != null ? helper : alias2),(options={"name":"tabs","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.tabs) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>\n";
},"useData":true});
templates['ui.inspectionModal'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"modal fade centerModal\" id=\""
    + alias4(((helper = (helper = helpers.modalId || (depth0 != null ? depth0.modalId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"modalId","hash":{},"data":data}) : helper)))
    + "\" tabindex=\"-1\" role=\"dialog\">\n	<div class=\"modal-dialog\" style=\"width: 1000px; max-width: 100%;\">\n		<!-- Modal content -->\n		<div class=\"modal-content\">\n			<div style=\"height: auto !important;\" class=\"modal-header menuBar shadow-z-2\">\n				<div class=\"modal-title\">Inspect pattern</div>\n				<button type=\"button\" class=\"close pullRight\" data-dismiss=\"modal\" style=\"width: 10px !important;\">&times;</button>\n			</div>\n			<div class=\"container-fluid\">\n				<div class=\"modal-body\">\n					<div id=\"rows\" style=\"height: 500px; width: 100%; overflow: scroll;\"></div>\n				</div>\n				<div class=\"modal-footer\">\n					<div class=\"container-fluid\">\n						<div class=\"row\">\n							<div id=\"pageLinks\" style=\"width: 100%;\"></div>\n						</div>\n						<div class=\"row\">\n							<button type=\"button\" class=\"btn-outline-secondary "
    + alias4(((helper = (helper = helpers.buttonClass || (depth0 != null ? depth0.buttonClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonClass","hash":{},"data":data}) : helper)))
    + "\">Close</button>\n						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n		<!-- End modal content -->  \n	</div>\n</div>\n";
},"useData":true});
templates['ui.inspectionTable'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "		<div class=\"row tableRow\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "				<div class=\"tableCell\">\n					"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "\n				</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, buffer = 
  "<div class=\"table padded\">\n";
  stack1 = ((helper = (helper = helpers.content || (depth0 != null ? depth0.content : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"content","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!helpers.content) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>\n";
},"useData":true});
templates['ui.largeChartModal'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"file",{"name":"equal","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"Circular pattern dissection",{"name":"equal","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var helper;

  return "								<img class=\"largeChart\" src=\""
    + container.escapeExpression(((helper = (helper = helpers.data || (depth0 != null ? depth0.data : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"data","hash":{},"data":data}) : helper)))
    + "\"></img>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "								<div class=\"circular-pattern-dissection\" data-patterns=\""
    + container.escapeExpression((helpers.json || (depth0 && depth0.json) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.data : depth0),{"name":"json","hash":{},"data":data}))
    + "\" data-height=\"480\">\n								</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"modal fade centerModal\" id=\""
    + alias4(((helper = (helper = helpers.modalId || (depth0 != null ? depth0.modalId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"modalId","hash":{},"data":data}) : helper)))
    + "\" tabindex=\"-1\" role=\"dialog\">\n	<div class=\"modal-dialog\" style=\"width: 1000px; max-width: 100%;\">\n		<!-- Modal content -->\n		<div class=\"modal-content\">\n			<div style=\"height: auto !important;\" class=\"modal-header menuBar shadow-z-2\">\n				<div class=\"modal-title\">"
    + alias4(((helper = (helper = helpers.chartName || (depth0 != null ? depth0.chartName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"chartName","hash":{},"data":data}) : helper)))
    + "</div>\n				<button type=\"button\" class=\"close pullRight\" data-dismiss=\"modal\" style=\"width: 10px !important;\">&times;</button>\n			</div>\n			<div class=\"container-fluid\">\n				<div class=\"row\">\n					<div class=\"col flexDisplay\">\n						<div class=\"legendItem population\"></div>\n"
    + ((stack1 = container.invokePartial(partials["graphics.pattern"],(depth0 != null ? depth0.emptyPattern : depth0),{"name":"graphics.pattern","data":data,"indent":"\t\t\t\t\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "					</div>\n				</div>\n				<div class=\"row\">\n					<div class=\"col flexDisplay\">\n						<div class=\"legendItem subgroup\"></div>\n"
    + ((stack1 = container.invokePartial(partials["graphics.pattern"],(depth0 != null ? depth0.pattern : depth0),{"name":"graphics.pattern","data":data,"indent":"\t\t\t\t\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "					</div>\n				</div>\n				<div class=\"row\">\n					<div class=\"col flexDisplay\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "					</div>\n				</div>\n			</div>\n		</div>\n		<!-- End modal content -->  \n	</div>\n</div>\n";
},"usePartial":true,"useData":true});
templates['ui.measureVisualizationsModal'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "							<div class=\"col-md-6\">\n								<div class=\"padded\">\n									<div class=\"boxed myshadow\">\n										<div class=\"container-fluid\">\n											<div class=\"row boxTitle\">\n												<div class=\"col\">\n													<span class=\"title\">\n													"
    + alias3((helpers.capitalizeFirst || (depth0 && depth0.capitalizeFirst) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"capitalizeFirst","hash":{},"data":data}))
    + "\n													</span>\n												</div>\n											</div>\n											<div class=\"row padded\">\n												<div class=\""
    + alias3(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">\n												</div>\n											</div>\n										</div>\n									</div>\n								</div>\n							</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.escapeExpression;

  return "<div class=\"modal fade centerModal\" id=\""
    + alias2(((helper = (helper = helpers.modalId || (depth0 != null ? depth0.modalId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"modalId","hash":{},"data":data}) : helper)))
    + "\" tabindex=\"-1\" role=\"dialog\">\n	<div class=\"modal-dialog\" style=\"width: 1000px; max-width: 100%;\">\n		<!-- Modal content -->\n		<div class=\"modal-content\">\n			<div style=\"height: auto !important;\" class=\"modal-header menuBar shadow-z-2\">\n				<div class=\"modal-title\">Pattern information for "
    + alias2(container.lambda(((stack1 = (depth0 != null ? depth0.info : depth0)) != null ? stack1.id : stack1), depth0))
    + "</div>\n				<button type=\"button\" class=\"close pullRight\" data-dismiss=\"modal\" style=\"width: 10px !important;\">&times;</button>\n			</div>\n			<div class=\"container-fluid\">\n				<div class=\"modal-body\">\n					<div class=\"container-fluid\" style=\"height: 600px; width: 100%; overflow: scroll;\">\n						<div class=\"row\">\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0.info : depth0)) != null ? stack1.visualizations : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n		<!-- End modal content -->  \n	</div>\n</div>\n";
},"useData":true});
templates['ui.optionsList'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "		<li type=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper)))
    + "\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\"><a>"
    + alias4(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"caption","hash":{},"data":data}) : helper)))
    + "</a></li>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<ul>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</ul>\n";
},"useData":true});
templates['ui.pageLinks'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "";
},"3":function(container,depth0,helpers,partials,data) {
    var helper;

  return "href=\""
    + container.escapeExpression(((helper = (helper = helpers.firstPage || (depth0 != null ? depth0.firstPage : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"firstPage","hash":{},"data":data}) : helper)))
    + "\"";
},"5":function(container,depth0,helpers,partials,data) {
    var helper;

  return "href=\""
    + container.escapeExpression(((helper = (helper = helpers.prevPage || (depth0 != null ? depth0.prevPage : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"prevPage","hash":{},"data":data}) : helper)))
    + "\"";
},"7":function(container,depth0,helpers,partials,data) {
    var helper;

  return "href=\""
    + container.escapeExpression(((helper = (helper = helpers.nextPage || (depth0 != null ? depth0.nextPage : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"nextPage","hash":{},"data":data}) : helper)))
    + "\"";
},"9":function(container,depth0,helpers,partials,data) {
    var helper;

  return "href=\""
    + container.escapeExpression(((helper = (helper = helpers.lastPage || (depth0 != null ? depth0.lastPage : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"lastPage","hash":{},"data":data}) : helper)))
    + "\"";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pageLinks centeredText\">\n	<i class=\"fa fa-angle-double-left menuIcon\" "
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.firstPage : depth0),"",{"name":"equal","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "></i>\n	<i class=\"fa fa-angle-left menuIcon\" "
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.prevPage : depth0),"",{"name":"equal","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "></i>\n	"
    + alias4(((helper = (helper = helpers.pageNumber || (depth0 != null ? depth0.pageNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"pageNumber","hash":{},"data":data}) : helper)))
    + " / "
    + alias4(((helper = (helper = helpers.totalPages || (depth0 != null ? depth0.totalPages : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"totalPages","hash":{},"data":data}) : helper)))
    + "\n	<i class=\"fa fa-angle-right menuIcon\" "
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.nextPage : depth0),"",{"name":"equal","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "></i>\n	<i class=\"fa fa-angle-double-right menuIcon\" "
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.lastPage : depth0),"",{"name":"equal","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(9, data, 0),"data":data})) != null ? stack1 : "")
    + "></i>\n</div>\n";
},"useData":true});
templates['ui.pattern'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["graphics.association"],(depth0 != null ? depth0.patternDescriptor : depth0),{"name":"graphics.association","data":data,"indent":"\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["graphics.associationRule"],(depth0 != null ? depth0.patternDescriptor : depth0),{"name":"graphics.associationRule","data":data,"indent":"\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["graphics.attributeList"],(depth0 != null ? depth0.patternDescriptor : depth0),{"name":"graphics.attributeList","data":data,"indent":"\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["graphics.exceptionalModelPattern"],(depth0 != null ? depth0.patternDescriptor : depth0),{"name":"graphics.exceptionalModelPattern","data":data,"indent":"\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["graphics.sequence"],(depth0 != null ? depth0.patternDescriptor : depth0),{"name":"graphics.sequence","data":data,"indent":"\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"11":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["graphics.episode"],(depth0 != null ? depth0.patternDescriptor : depth0),{"name":"graphics.episode","data":data,"indent":"\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"13":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["graphics.episodeRule"],(depth0 != null ? depth0.patternDescriptor : depth0),{"name":"graphics.episodeRule","data":data,"indent":"\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"15":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["graphics.functionalPattern"],(depth0 != null ? depth0.patternDescriptor : depth0),{"name":"graphics.functionalPattern","data":data,"indent":"\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.patternDescriptor : depth0)) != null ? stack1.patternType : stack1),"Association",{"name":"equal","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.patternDescriptor : depth0)) != null ? stack1.patternType : stack1),"AssociationRule",{"name":"equal","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.patternDescriptor : depth0)) != null ? stack1.patternType : stack1),"AttributeList",{"name":"equal","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.patternDescriptor : depth0)) != null ? stack1.patternType : stack1),"ExceptionalModelPattern",{"name":"equal","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.patternDescriptor : depth0)) != null ? stack1.patternType : stack1),"Sequence",{"name":"equal","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.patternDescriptor : depth0)) != null ? stack1.patternType : stack1),"Episode",{"name":"equal","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.patternDescriptor : depth0)) != null ? stack1.patternType : stack1),"EpisodeRule",{"name":"equal","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.patternDescriptor : depth0)) != null ? stack1.patternType : stack1),"FunctionalPattern",{"name":"equal","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"usePartial":true,"useData":true});
templates['ui.previewTable'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.active : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "					<th>\n						"
    + alias2(alias1((depth0 != null ? depth0.caption : depth0), depth0))
    + "\n						<br>\n						"
    + alias2(alias1((depth0 != null ? depth0.type : depth0), depth0))
    + "\n						<br>\n"
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.icon : depth0),"empty",{"name":"equal","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "					</th>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "							<i>No Icon</i>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "							<i class=\""
    + container.escapeExpression(container.lambda((depth0 != null ? depth0.icon : depth0), depth0))
    + "\"></i>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "			<tr>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</tr>\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "					<td>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</td>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<table class=\"previewTable\">\n	<thead>\n		<tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.attributes : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</tr>\n	</thead>\n	<tbody>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</tbody>\n</table>\n";
},"useData":true});
templates['ui.previewTableWithDiscretization'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.active : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    return "					<th>\n						"
    + container.escapeExpression(container.lambda((depth0 != null ? depth0.caption : depth0), depth0))
    + "\n					</th>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.active : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    return "					<th>\n						"
    + container.escapeExpression(container.lambda((depth0 != null ? depth0.type : depth0), depth0))
    + "\n					</th>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.active : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"8":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "					<th>\n"
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.icon : depth0),"empty",{"name":"equal","hash":{},"fn":container.program(9, data, 0),"inverse":container.program(11, data, 0),"data":data})) != null ? stack1 : "")
    + "					</th>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "							<i>No Icon</i>\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "							<i class=\""
    + container.escapeExpression(container.lambda((depth0 != null ? depth0.icon : depth0), depth0))
    + "\"></i>\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.equal || (depth0 && depth0.equal) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.type : depth0),"METRIC",{"name":"equal","hash":{},"fn":container.program(14, data, 0),"inverse":container.program(16, data, 0),"data":data})) != null ? stack1 : "");
},"14":function(container,depth0,helpers,partials,data) {
    return "					<th>\n						<select class=\"chosen-select sDiscretization\">\n							<option value=\"DEFAULT\">Default</option>\n							<option value=\"EQUIDISTANT\">Equi-distant</option>\n							<option value=\"EQUIFREQUENT\">Equi-frequent</option>\n							<option value=\"CLUSTERING\">Clustering based</option>\n							<option value=\"FIXED_INTERVALS\">Fixed intervals</option>\n						</select>\n						<br>\n						<div class=\"bins\" style=\"display: none;\">\n							Bins:\n							<br>\n							<input type=\"number\" value=\"5\" step=\"1\" min=\"2\" autocomplete=\"off\">\n						</div>\n						<div class=\"cutOff\" style=\"display: none;\">\n							Cut-off points:\n							<br>\n							<select class=\"chosen-select\">\n								<option value=\"2\">2</option>\n								<option value=\"4\">4</option>\n								<option value=\"6\">6</option>\n								<option value=\"8\">8</option>\n								<option value=\"10\">10</option>\n								<option value=\"12\">12</option>\n								<option value=\"14\">14</option>\n								<option value=\"16\">16</option>\n								<option value=\"18\">18</option>\n								<option value=\"20\">20</option>\n							</select>\n						</div>\n						<div class=\"intervals\" style=\"display: none;\">\n							Comma-separated intervals:\n							<br>\n							<input type=\"text\" autocomplete=\"off\">\n						</div>\n					</th>\n";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.equal || (depth0 && depth0.equal) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.type : depth0),"DATE",{"name":"equal","hash":{},"fn":container.program(17, data, 0),"inverse":container.program(19, data, 0),"data":data})) != null ? stack1 : "");
},"17":function(container,depth0,helpers,partials,data) {
    return "						<th>\n							<label><input type=\"checkbox\" name=\"YEAR_MONTH_DATE_HOUR\" checked=\"checked\">Year, Month, Date, Hour</label>\n							<br>\n							<label><input type=\"checkbox\" name=\"MONTH\">Month</label>\n							<br>\n							<label><input type=\"checkbox\" name=\"DAY_OF_WEEK\">Day of the week</label>\n							<br>\n							<label><input type=\"checkbox\" name=\"DAY_OF_MONTH\">Day of the month</label>\n							<br>\n							<label><input type=\"checkbox\" name=\"DAY_OF_YEAR\">Day of the year</label>\n							<br>\n							<label><input type=\"checkbox\" name=\"IS_WEEKDAY\">Is weekday?</label>\n						</th>\n";
},"19":function(container,depth0,helpers,partials,data) {
    return "						<th></th>\n";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "			<tr>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(22, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</tr>\n";
},"22":function(container,depth0,helpers,partials,data) {
    return "					<td>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</td>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<table class=\"previewTable\">\n	<thead>\n		<tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.attributes : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</tr>\n		<tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.attributes : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</tr>\n		<tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.attributes : depth0),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</tr>\n		<tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.attributes : depth0),{"name":"each","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</tr>\n	</thead>\n	<tbody>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</tbody>\n</table>\n";
},"useData":true});
templates['ui.runningModal'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"modal fade\" id=\""
    + alias4(((helper = (helper = helpers.modalId || (depth0 != null ? depth0.modalId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"modalId","hash":{},"data":data}) : helper)))
    + "\" tabindex=\"-1\" role=\"dialog\" data-backdrop=\"static\" data-keyboard=\"false\">\n	<div class=\"modal-dialog\">\n		<div class=\"modal-content\">\n			<div style=\"height: auto !important;\" class=\"modal-header menuBar shadow-z-2\">\n				<div class=\"modal-title\">"
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + " is running, please bear with us</div>\n			</div>\n			<div class=\"modal-body\">\n				<div class=\"progress\">\n					<div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:100%\"></div>\n				</div>\n			</div>\n			<div class=\"modal-footer\">\n				<button type=\"button\" class=\"btn btn-default "
    + alias4(((helper = (helper = helpers.buttonClass || (depth0 != null ? depth0.buttonClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonClass","hash":{},"data":data}) : helper)))
    + "\">Stop running process</button>\n			</div>\n		</div>\n	</div>\n</div>\n";
},"useData":true});
templates['ui.schemeCreatedSuccessView'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"row\">\n	<div class=\"col-md-3\">\n	</div>\n	<div class=\"col-md-9\">\n		<div class=\"row\">\n			<div class=\"col-md-6\">\n				<button id=\"viewSchemeButton\" class=\"btn-outline-secondary\">View scheme</button>\n			</div>\n			<div class=\"col-md-6\">\n				<button id=\"viewWorkspacesButton\" class=\"btn-outline-secondary\">View workspaces</button>\n			</div>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['ui.selectOptions'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<option value=\""
    + alias4(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper)))
    + "\"><a>"
    + alias4(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"caption","hash":{},"data":data}) : helper)))
    + "</a></option>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['ui.sourceAttributeList'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"row buildElement\" type=\"attribute\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n		<div class=\"col-md-1 showOnParentHover\">\n			<i class=\"fa fa-arrows-alt\"></i>\n		</div>\n		<div class=\"col-md-10\">\n			<div class=\"subTitle\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div>\n			<div>"
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "</div>\n		</div>\n		<div class=\"col-md-1 inspectAttribute showOnParentHover\">\n			<i class=\"material-icons\">search</i>\n		</div>\n	</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, buffer = "";

  stack1 = ((helper = (helper = helpers.elements || (depth0 != null ? depth0.elements : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"elements","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!helpers.elements) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
templates['ui.sourceAttributesTable'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<table class=\"tablesorter subContent sourceDockTable\">\n	<thead>\n		<tr>\n			<th data-sorter=\"false\" data-placeholder=\"e.g., attribute 1\">Attribute</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr class=\"height100Pct\"></tr>\n		<tr id=\"sourceDockNextLoader\" style=\"height: 100%;\"></tr>\n	</tbody>\n</table>\n";
},"useData":true});
templates['ui.sourceDockMeasures'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "		<option qualityMeasureId=\""
    + alias4(((helper = (helper = helpers.qualityMeasureId || (depth0 != null ? depth0.qualityMeasureId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"qualityMeasureId","hash":{},"data":data}) : helper)))
    + "\">\n			"
    + alias4(((helper = (helper = helpers.simpleName || (depth0 != null ? depth0.simpleName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"simpleName","hash":{},"data":data}) : helper)))
    + "\n		</option>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<select id=\"sourceDockMeasureSelector\" style=\"width: calc(100% - 3px);\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</select>\n";
},"useData":true});
templates['ui.sourceDockTabs'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "				<li class=\"optionSpan "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" type=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"caption","hash":{},"data":data}) : helper)))
    + "</li>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "selected";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"menu menuBar subMenuBar\">\n	<div class=\"fixedWidth350 tableCell centeredText\">\n		<ul id=\"buildElementTypeOptions\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			<li class=\"optionSpan pullRight\">\n				<div class=\"loader\" id=\"sourceDockLoader\"></div>\n			</li>\n		</ul>\n	</div>\n</div>\n";
},"useData":true});
templates['ui.sourcePropositionList'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"row buildElement\" type=\"item\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"background: linear-gradient(90deg, rgb(255,168,168) "
    + alias4(((helper = (helper = helpers.pct || (depth0 != null ? depth0.pct : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"pct","hash":{},"data":data}) : helper)))
    + "%, white 0%);\">\n		<div class=\"col-md-1 showOnParentHover\">\n			<i class=\"fa fa-arrows-alt\"></i>\n		</div>\n		<div class=\"col-md-4\">"
    + alias4(((helper = (helper = helpers.attributeName || (depth0 != null ? depth0.attributeName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"attributeName","hash":{},"data":data}) : helper)))
    + "<i class=\""
    + alias4(((helper = (helper = helpers.attributeIcon || (depth0 != null ? depth0.attributeIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"attributeIcon","hash":{},"data":data}) : helper)))
    + "\"></i></div>\n		<div class=\"col-md-4\">"
    + alias4(((helper = (helper = helpers.propositionValue || (depth0 != null ? depth0.propositionValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"propositionValue","hash":{},"data":data}) : helper)))
    + "</div>\n		<div class=\"col-md-3\">"
    + alias4(((helper = (helper = helpers.formattedScore || (depth0 != null ? depth0.formattedScore : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"formattedScore","hash":{},"data":data}) : helper)))
    + "</div>\n	</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, buffer = "";

  stack1 = ((helper = (helper = helpers.elements || (depth0 != null ? depth0.elements : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"elements","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!helpers.elements) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
templates['ui.sourcePropositionsTable'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<table class=\"tablesorter subContent sourceDockTable\">\n	<thead>\n		<tr>\n			<th data-sorter=\"false\" data-placeholder=\"e.g., attribute 1\">Attribute</th>\n			<th data-sorter=\"false\" data-placeholder=\"e.g., value\">Value</th>\n			<th data-sorter=\"false\" data-placeholder=\"e.g., < 100 and > 10\">Score</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr class=\"height100Pct\"></tr>\n		<tr id=\"sourceDockNextLoader\" style=\"height: 100%;\"></tr>\n	</tbody>\n</table>\n";
},"useData":true});
templates['ui.table'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"table\">\n</div>";
},"useData":true});
templates['ui.twoColRows'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"row no-gutters\">\n		<div class=\"col-md-6 subTitle\">"
    + alias4(((helper = (helper = helpers.first || (depth0 != null ? depth0.first : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"first","hash":{},"data":data}) : helper)))
    + "</div>\n		<div class=\"col-md-6\">"
    + alias4(((helper = (helper = helpers.second || (depth0 != null ? depth0.second : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"second","hash":{},"data":data}) : helper)))
    + "</div>\n	</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['ui.userInfo'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"container-fluid\">\n	<div class=\"row\">\n		<div class=\"col-md-3 data-label\">Username</div>\n		<div class=\"col-md-9\">"
    + container.escapeExpression(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"userName","hash":{},"data":data}) : helper)))
    + "</div>\n	</div>\n	<div class=\"row\">\n		<div class=\"col-md-3 data-label\">Password</div>\n		<div class=\"col-md-9\"><input class=\"form-control floating-label\" type=\"password\" id=\"password\" placeholder=\"Type new password\" /></div>\n	</div>\n	<div class=\"row\">\n		<div class=\"col-md-3 data-label\">Password confirm</div>\n		<div class=\"col-md-9\"><input class=\"form-control floating-label\" type=\"password\" id=\"passwordConfirm\" placeholder=\"Retype new password\" /></div>\n	</div>\n	<div class=\"row\">\n		<div class=\"col-md-3\"></div>\n		<div class=\"col-md-3\"><button class=\"btn-outline-secondary\" id=\"cancelButton\">Cancel</button></div>\n		<div class=\"col-md-3\"><button class=\"btn-outline-primary\" id=\"saveButton\">Save</button></div>\n	</div>\n</div>";
},"useData":true});
templates['ui.userTable'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "				<th>\n					"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "\n				</th>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "			<tr data-ix=\""
    + alias4(((helper = (helper = helpers.ix || (depth0 != null ? depth0.ix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ix","hash":{},"data":data}) : helper)))
    + "\" data-enabled=\""
    + alias4(((helper = (helper = helpers.editing_enabled || (depth0 != null ? depth0.editing_enabled : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"editing_enabled","hash":{},"data":data}) : helper)))
    + "\">\n				<td>"
    + alias4(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"userName","hash":{},"data":data}) : helper)))
    + "</td>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.roles : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "				<td><input type=\"checkbox\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.enabled : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "></td>\n				<td><button class=\"btn-outline-secondary saveButton\"><i class=\"fa fa-save\"></i></button></td>\n				<td><button class=\"btn-outline-danger deleteButton\"><i class=\"fa fa-trash\"></i></button></td>\n			</tr>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "					<td><input type=\"checkbox\" "
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "></td>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "checked";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<table class=\"userTable\">\n	<thead>\n		<tr>\n			<th>Username</th>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.roles : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			<th>Enabled</th>\n			<th></th>\n			<th></th>\n		</tr>\n	</thead>\n	<tbody>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.users : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</tbody>\n</table>\n";
},"useData":true});
templates['ui.workDock'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<table class=\"subContent height100Pct\">\n	<tr>\n		<td>\n			<div id=\"workMenuWrapper\"></div>\n		</td>\n	</tr>\n	<tr>\n		<td>\n			<div id=\"workPatternsWrapper\"></div>\n		</td>\n	</tr>\n</table>\n";
},"useData":true});
templates['ui.workDockChart'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"file",{"name":"equal","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"Circular pattern dissection",{"name":"equal","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["ui.chartImage"],depth0,{"name":"ui.chartImage","data":data,"indent":"\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials["ui.chartCircularPatternDissection"],depth0,{"name":"ui.chartCircularPatternDissection","data":data,"indent":"\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    return "	<span style=\"color: lightgray\">N/A</span>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.type : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"usePartial":true,"useData":true});
templates['ui.workDockHeader'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "		<th data-measure=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n			"
    + alias4(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"caption","hash":{},"data":data}) : helper)))
    + "\n			<span class=\"pointerStyleCursor\" function=\"sort\" order=\"DESCENDING\" measure=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"><i class=\"fa fa-sort-amount-up\"></i></span>\n			<span class=\"pointerStyleCursor\" function=\"sort\" order=\"ASCENDING\" measure=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"><i class=\"fa fa-sort-amount-down\"></i></span>\n			<span class=\"pointerStyleCursor\" function=\"inspect\"><i class=\"fa fa-chart-line\"></i></span>\n		</th>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var helper;

  return "		<th>\n			"
    + container.escapeExpression(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"caption","hash":{},"data":data}) : helper)))
    + "\n		</th>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<tr>\n	<th></th>\n	<th>\n		<span class=\"menuIcon pointerStyleCursor selectAllPatternsButton\"><i class=\"far fa-check-square\"></i></span>\n		<span style=\"font-size: 20px;\">/</span>\n		<span class=\"menuIcon pointerStyleCursor deselectAllPatternsButton\"><i class=\"far fa-square\"></i></span>\n	</th>\n	<th>\n	</th>\n	<th>\n		Pattern\n	</th>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.measures : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.visualizations : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	<th>\n	</th>\n</tr>\n";
},"useData":true});
templates['ui.workDockMenu'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"menuBar subMenuBar\">\n	<div class=\"row no-gutters\">\n		<div class=\"col-md-8\">\n			<ul id=\"workDockMenu\" class=\"sm sm-simple\">\n				<li id=\"patternDrivenPPMenu\" class=\"hoveringMenuItem hide-sub-arrow\">\n					<a>\n						<span>\n							<i class=\"material-icons menuIcon\">menu</i>\n						</span>\n					</a>\n				</li>\n				<li id=\"undo\" class=\"hoveringMenuItem\" data-placement=\"bottom\" data-trigger=\"hover\" data-toggle=\"popover\" data-content=\"Undo\">\n					<a>\n						<span>\n							<i class=\"material-icons menuIcon\">undo</i>\n						</span>\n					</a>\n				</li>\n				<li id=\"redo\" class=\"hoveringMenuItem\" data-placement=\"bottom\" data-trigger=\"hover\" data-toggle=\"popover\" data-content=\"Redo\">\n					<a>\n						<span>\n							<i class=\"material-icons menuIcon\">redo</i>\n						</span>\n					</a>\n				</li>\n				<li id=\"deletePatterns\" class=\"hoveringMenuItem\" data-placement=\"bottom\" data-trigger=\"hover\" data-toggle=\"popover\" data-content=\"Delete selected patterns\">\n					<a>\n						<span>\n							<i class=\"material-icons menuIcon\">delete</i>\n						</span>\n					</a>\n				</li>\n				<li class=\"hoveringMenuItem\" data-placement=\"bottom\" data-trigger=\"hover\" data-toggle=\"popover\" data-content=\"Add source dock elements as singleton?\">\n					<a>\n						<label>\n							<input id=\"addIndividualCheckbox\" type=\"checkbox\" checked=\"checked\">\n							Add as singleton?\n						</label>\n					</a>\n				</li>\n				<li id=\"downloadPatterns\" class=\"hoveringMenuItem\" data-placement=\"bottom\" data-trigger=\"hover\" data-toggle=\"popover\" data-content=\"Download\">\n					<a>\n						<span class=\"menuIcon\">\n							<i class=\"material-icons\">file_download</i>\n						</span>\n					</a>\n				</li>\n			</ul>\n		</div>\n		<div class=\"col-md-4\">\n			<ul>\n				<li class=\"optionSpan pullRight\">\n					<div class=\"loader\"></div>\n				</li>\n				<li id=\"refreshPatterns\" class=\"optionSpan hoveringMenuItem pullRight\">\n					<i class=\"material-icons menuIcon\">refresh</i>\n				</li>\n				<li id=\"workDockPageLinks\" class=\"optionSpan pullRight\">\n				</li>\n			</ul>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['ui.workDockPatternRow'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "		<td>\n			<span "
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,"NaN",{"name":"equal","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</span>\n		</td>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "style=\"color: lightgray;\"";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "		<td "
    + ((stack1 = (helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,(depth0 != null ? depth0.name : depth0),"CircularPatternDissectionVisualization",{"name":"equal","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n			<div vis=\""
    + container.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\"></div>\n		</td>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "style=\"height:200px;\"";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<tr class=\"pattern width100Pct\" vis=\"nok\" patternIx=\""
    + alias4(((helper = (helper = helpers.index || (depth0 != null ? depth0.index : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" id=\""
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.pattern : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">\n	<td>\n		<div class=\"showOnParentHover\">\n			<i class=\"fa fa-grip-lines patternDrag\"></i>\n		</div>\n	</td>\n	<td>\n		<input type=\"checkbox\">\n	</td>\n	<td>\n		"
    + alias4(((helper = (helper = helpers.ix || (depth0 != null ? depth0.ix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ix","hash":{},"data":data}) : helper)))
    + "\n	</td>\n	<td>\n"
    + ((stack1 = container.invokePartial(partials["graphics.pattern"],(depth0 != null ? depth0.pattern : depth0),{"name":"graphics.pattern","data":data,"indent":"\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "	</td>\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0.pattern : depth0)) != null ? stack1.measurements : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.visualizations : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	<td>\n		<div class=\"patternOptions showOnAncestorHover\">\n			<div><button class=\"btn btn-fab btn-fab-mini btnInfo inspectPatternButton\"><i class=\"fa fa-search\"></i></button></div>\n			<!-- <div><button class=\"btn btn-fab btn-fab-mini btnSuccess conditionDataTableButton\"><i class=\"material-icons\">flip_to_front</i></button></div> -->\n			<div><button class=\"btn btn-fab btn-fab-mini btnSuccess copyToClipboardButton\"><i class=\"fa fa-clipboard\"></i></button></div>\n			<!-- <div><button class=\"btn btn-fab btn-fab-mini btnSuccess copyToNewWorksheetButton\"><i class=\"fa fa-folder-plus\"></i></button></div> -->\n			<div><button class=\"btn btn-fab btn-fab-mini btnWarning deletePatternButton\"><i class=\"fa fa-trash\"></i></button></div>\n		</div>\n	</td>\n</tr>";
},"usePartial":true,"useData":true});
templates['ui.workDockTable'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"droppable-workdock\" id=\"WorkDock\">\n	<div id=\"workDockWrapper\" class=\"width100Pct height100Pct\"  style=\"overflow-y: scroll;\">\n		<table id=\"workDockTable\" class=\"workDockTable\">\n			<thead class=\"head\">\n				<tr>\n					<th></th>\n					<th></th>\n					<th></th>\n				</tr>\n			</thead>\n			<tbody class=\"body\">\n			</tbody>\n		</table>\n	</div>\n</div>\n";
},"useData":true});
templates['ui.workspaceCreatedSuccessView'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"row\">\n	<div class=\"col-md-3\">\n	</div>\n	<div class=\"col-md-9\">\n		<div class=\"row\">\n			<div class=\"col-md-6\">\n				<button id=\"viewWorkspacesButton\" class=\"btn-outline-secondary\">View workspaces</button>\n			</div>\n			<div class=\"col-md-6\">\n				<button id=\"loadWorkspaceButton\" class=\"btn-outline-secondary\">Load workspace</button>\n			</div>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['ui.workspaceInfoBlocksList'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"col-md-6 padded\" workspaceId=\""
    + alias4(((helper = (helper = helpers.workspaceId || (depth0 != null ? depth0.workspaceId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"workspaceId","hash":{},"data":data}) : helper)))
    + "\">\n		<div class=\"container-fluid boxed shadowhover\">\n			<div class=\"row\">\n				<div class=\"col-sm-4\">Name:</div>\n				<div class=\"col-sm-8\">"
    + alias4(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"caption","hash":{},"data":data}) : helper)))
    + "</div>\n			</div>\n			<div class=\"row\">\n				<div class=\"col-sm-4\">Description:</div>\n				<div class=\"col-sm-8\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</div>\n			</div>\n			<div class=\"row\">\n				<div class=\"col-sm-4\">Worksheet Count:</div>\n				<div class=\"col-sm-8\">"
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.worksheets : depth0)) != null ? stack1.length : stack1), depth0))
    + "</div>\n			</div>\n			<div class=\"row\">\n				<div class=\"col-sm-4\"></div>\n				<div class=\"col-xl-4 padded\"><button class=\"btn-outline-secondary\" id=\"workspaceLoad\">Load</button></div>\n				<div class=\"col-xl-4 padded\"><button class=\"btn-outline-danger\" id=\"workspaceDelete\"><i class=\"fa fa-trash\"></i></button></div>\n			</div>\n		</div>\n	</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
})();
