CREATE TABLE IF NOT EXISTS DataTable (
	dataTableId VARCHAR(36) NOT NULL,
	caption VARCHAR(50),
	description VARCHAR(255),
	PRIMARY KEY (dataTableId)
);

CREATE TABLE IF NOT EXISTS Scheme (
	schemeId VARCHAR(36) NOT NULL,
	caption VARCHAR(50),
	description VARCHAR(255),
	dataTableId VARCHAR(36) NOT NULL,
	PRIMARY KEY (schemeId),
	FOREIGN KEY (dataTableId) REFERENCES DataTable(dataTableId) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Workspace (
	workspaceId VARCHAR(36) NOT NULL,
	caption VARCHAR(50),
	description VARCHAR(50),
	PRIMARY KEY (workspaceId)
);

CREATE TABLE IF NOT EXISTS Worksheet (
	worksheetId VARCHAR(36) NOT NULL,
	caption VARCHAR(50),
	description VARCHAR(255),
	schemeId VARCHAR(36) NOT NULL,
	workspaceId VARCHAR (36) NOT NULL,
	PRIMARY KEY (worksheetId),
	FOREIGN KEY (schemeId) REFERENCES Scheme(schemeId) ON DELETE CASCADE,
	FOREIGN KEY (workspaceId) REFERENCES Workspace(workspaceId)
);

CREATE TABLE IF NOT EXISTS users (
	user_id INT(10) NOT NULL AUTO_INCREMENT,
	username VARCHAR(36) NOT NULL UNIQUE,
	password BINARY(60) NOT NULL,
	enabled BOOLEAN DEFAULT false,
	PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS roles (
	role_id INT(10) NOT NULL AUTO_INCREMENT,
	role_name VARCHAR(36) NOT NULL UNIQUE,
	PRIMARY KEY (role_id)
);

CREATE TABLE IF NOT EXISTS user_roles (
	user_id INT(10) NOT NULL,
	role_id INT(10) NOT NULL,
	FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
	FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

CREATE TABLE IF NOT EXISTS user_data_tables (
	user_id INT(10) NOT NULL,
	data_table_id VARCHAR(36) NOT NULL,
	FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
	FOREIGN KEY (data_table_id) REFERENCES DataTable(dataTableId) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS user_schemes (
	user_id INT(10) NOT NULL,
	scheme_id VARCHAR(36) NOT NULL,
	FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
	FOREIGN KEY (scheme_id) REFERENCES Scheme(schemeId) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS user_workspaces (
	user_id INT(10) NOT NULL,
	workspace_id VARCHAR(36) NOT NULL,
	FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
	FOREIGN KEY (workspace_id) REFERENCES Workspace(workspaceId) ON DELETE CASCADE
);

set @x := (select count(*) from information_schema.statistics where table_name = 'user_roles' and index_name = 'unique_index' and table_schema = database());
set @sql := if( @x > 0, 'select ''Index exists.''', 'ALTER TABLE user_roles ADD UNIQUE unique_index(user_id, role_id)');
PREPARE stmt FROM @sql;
EXECUTE stmt;
