INSERT IGNORE INTO roles(role_id, role_name) VALUES
	(1, "ROLE_ADMIN"),
	(2, "ROLE_USER")
;

INSERT IGNORE INTO users(user_id, username, password, enabled) VALUES
	(1, "root", "$2a$10$lX9FbgtDubvgb2FMWrlDUO.3inX938y/ZY/hXN.HOK058RihStbK2", true)
;

INSERT IGNORE INTO user_roles(user_id, role_id) VALUES
	(1, 1),
	(1, 2)
;
