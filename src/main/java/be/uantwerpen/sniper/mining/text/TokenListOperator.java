/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.mining.text;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.Math.min;
import static java.util.stream.Collectors.toSet;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.mutable.MutableInt;

import com.google.common.primitives.Ints;

import be.uantwerpen.sniper.config.Settings;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.DFFilterType;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.stemmer.snowball.SnowballStemmer;
import opennlp.tools.stemmer.snowball.SnowballStemmer.ALGORITHM;
import opennlp.tools.util.InvalidFormatException;

/**
 *
 *
 * @author Sandy
 * @since
 * @version
 */
public interface TokenListOperator {

	public List<List<String>> perform(List<List<String>> tokenList);

	public static class TokenListStemmer implements TokenListOperator {

		@Override
		public List<List<String>> perform(List<List<String>> tokenLists) {
			SnowballStemmer stemmer = new SnowballStemmer(ALGORITHM.ENGLISH);

			List<List<String>> sTokenList = tokenLists.stream().map(tokenList -> tokenList.stream()
					.map(s -> stemmer.stem(s.toLowerCase()).toString()).collect(Collectors.toList()))
					.collect(Collectors.toList());
			return sTokenList.stream()
					.map(tokenList -> tokenList.stream().filter(token -> !token.isEmpty()).collect(Collectors.toList()))
					.collect(Collectors.toList());
		}

	}

	public static class POSFilter implements TokenListOperator {

		public static Set<String> posToKeep = newHashSet("FW", "JJ", "JJR", "JJS", "NN", "NNS", "NNP", "PDT", "POS",
				"RB", "RBR", "RBS", "UH"
		/* "MD", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ" */);

		private POSTaggerME tagger;

		public POSFilter() {
			TextLanguage lang = TextLanguage.EN;
			try {
				tagger = new POSTaggerME(new POSModel(new File(Settings.Instance.getProperty(Settings.DATA_DIR)
						+ "/corpora/" + lang.toString().toLowerCase() + "-pos-maxent.bin")));
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public List<List<String>> perform(List<List<String>> tokenLists) {
			if (tagger == null) {
				return tokenLists;
			}

			List<List<String>> fTokenLists = newArrayList();
			for (List<String> tokenList : tokenLists) {
				List<String> fTokenList = newArrayList();
				String[] pos = tagger.tag(tokenList.toArray(new String[] {}));
				for (int i = 0, iEnd = tokenList.size(); i < iEnd; i++) {
					if (posToKeep.contains(pos[i])) {
						fTokenList.add(tokenList.get(i));
					}
				}
				fTokenLists.add(fTokenList);
			}
			return fTokenLists;
		}

	}

	public static class EmptyTokenListFilter implements TokenListOperator {

		@Override
		public List<List<String>> perform(List<List<String>> tokenLists) {
			return tokenLists.stream().filter(tokenList -> !tokenList.isEmpty()).map(tokenList -> tokenList)
					.collect(Collectors.toList());
		}

	}

	public static class DFFilter implements TokenListOperator {

		private DFFilterType dFFilterType = DFFilterType.TOPX;
		private int value = 100;

		public DFFilter() {
		}

		public DFFilter(DFFilterType dFFilterType, int value) {
			this();

			this.dFFilterType = dFFilterType;
			this.value = value;
		}

		private List<Entry<String, MutableInt>> getDFs(List<List<String>> tokenLists) {
			Map<String, MutableInt> dfMap = newHashMap();
			for (List<String> tokenList : tokenLists) {
				for (String token : newHashSet(tokenList)) {
					MutableInt df = dfMap.get(token);
					if (df == null) {
						df = new MutableInt();
						dfMap.put(token, df);
					}
					df.add(1);
				}
			}
			return newArrayList(dfMap.entrySet());
		}

		private List<Entry<String, MutableInt>> getTokensSortedByDF(List<List<String>> tokenLists) {
			List<Entry<String, MutableInt>> dfs = getDFs(tokenLists);
			Collections.sort(dfs, new Comparator<Entry<String, MutableInt>>() {
				@Override
				public int compare(Entry<String, MutableInt> o1, Entry<String, MutableInt> o2) {
					return Ints.compare(o1.getValue().intValue(), o2.getValue().intValue());
				}
			});
			Collections.reverse(dfs);
			return dfs;
		}

		@Override
		public List<List<String>> perform(List<List<String>> tokenLists) {
			List<Entry<String, MutableInt>> dfs = getTokensSortedByDF(tokenLists);

			Set<String> tokens;

			switch (dFFilterType) {
			case MINIMUMDF:
				tokens = dfs.stream().filter(entry -> entry.getValue().intValue() >= value).map(entry -> entry.getKey())
						.collect(toSet());
				break;
			case TOPX:
				tokens = dfs.subList(0, min(dfs.size(), value)).stream().map(entry -> entry.getKey()).collect(toSet());
				break;
			default:
				tokens = dfs.stream().map(entry -> entry.getKey()).collect(toSet());
				break;
			}

			return tokenLists.stream().map(tokenList -> tokenList.stream().filter(token -> tokens.contains(token))
					.map(token -> token).collect(Collectors.toList())).collect(Collectors.toList());
		}

	}
}
