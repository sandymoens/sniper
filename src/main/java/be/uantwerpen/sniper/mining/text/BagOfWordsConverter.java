/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.mining.text;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.Math.log;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.mutable.MutableDouble;

/**
 *
 *
 * @author Sandy
 * @since
 * @version
 */
public interface BagOfWordsConverter {

	public List<String> getTokens();

	public List<List<Double>> getValues();

	public static class TFIDF implements BagOfWordsConverter {

		private List<String> tokens;
		private List<List<Double>> tfidfs;

		public TFIDF(List<List<String>> tokenLists) {
			List<Entry<String, MutableDouble>> idfs = getIDFs(tokenLists);
			tokens = idfs.stream().map(idf -> idf.getKey()).collect(Collectors.toList());
			tfidfs = computeTFIDF(tokenLists, idfs);
		}

		private List<Entry<String, MutableDouble>> getIDFs(List<List<String>> tokenLists) {
			Map<String, MutableDouble> dfMap = newHashMap();
			for (List<String> tokenList : tokenLists) {
				for (String token : newHashSet(tokenList)) {
					MutableDouble df = dfMap.get(token);
					if (df == null) {
						df = new MutableDouble();
						dfMap.put(token, df);
					}
					df.add(1);
				}
			}
			Set<Entry<String, MutableDouble>> idfs = dfMap.entrySet();
			Iterator<Entry<String, MutableDouble>> iterator = idfs.iterator();
			while (iterator.hasNext()) {
				if (iterator.next().getValue().doubleValue() <= 1) {
					iterator.remove();
				}
			}
			for (Entry<String, MutableDouble> entry : idfs) {
				entry.getValue().setValue(log((1.0 * tokenLists.size() / entry.getValue().doubleValue()) + 1));
			}
			return newArrayList(dfMap.entrySet());
		}

		private List<List<Double>> computeTFIDF(List<List<String>> tokenLists,
				List<Entry<String, MutableDouble>> idfs) {
			List<List<Double>> tfidfs = newArrayList();
			Map<String, Integer> index = getIndexMap(idfs);

			for (List<String> tokenList : tokenLists) {
				double[] tfs = new double[index.size()];
				for (String token : tokenList) {
					if (!index.containsKey(token)) {
						continue;
					}
					tfs[index.get(token)]++;
				}
				List<Double> values = newArrayListWithCapacity(tfs.length);
				for (int i = 0; i < tfs.length; i++) {
					values.add((tfs[i] / tokenLists.size()) * idfs.get(i).getValue().doubleValue());
				}
				tfidfs.add(values);
			}

			return tfidfs;
		}

		private Map<String, Integer> getIndexMap(List<Entry<String, MutableDouble>> idfs) {
			Map<String, Integer> index = newHashMap();
			int i = 0;
			for (Entry<String, MutableDouble> entry : idfs) {
				index.put(entry.getKey(), i++);
			}
			return index;
		}

		@Override
		public List<String> getTokens() {
			return tokens;
		}

		@Override
		public List<List<Double>> getValues() {
			return tfidfs;
		}

	}
}
