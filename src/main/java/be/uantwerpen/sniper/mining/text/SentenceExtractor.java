/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.mining.text;

import static com.google.common.collect.Lists.newArrayList;

import java.io.File;
import java.io.IOException;
import java.util.List;

import be.uantwerpen.sniper.config.Settings;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.InvalidFormatException;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.0.2
 * @version 0.0.2
 */
public interface SentenceExtractor {

	public List<String> getSentences(String text);

	public static class OpenNLPSentenceExtractor implements SentenceExtractor {

		public static SentenceExtractor newExtractorByLanguange(TextLanguage lang)
				throws InvalidFormatException, IOException {
			return new OpenNLPSentenceExtractor(
					new SentenceModel(new File(Settings.Instance.getProperty(Settings.DATA_DIR) + "/corpora/"
							+ lang.toString().toLowerCase() + "-sent.bin")));
		}

		private SentenceDetectorME sentenceDetector;

		public OpenNLPSentenceExtractor(SentenceModel model) {
			this.sentenceDetector = new SentenceDetectorME(model);
		}

		@Override
		public List<String> getSentences(String text) {
			return newArrayList(this.sentenceDetector.sentDetect(text));
		}

	}
}
