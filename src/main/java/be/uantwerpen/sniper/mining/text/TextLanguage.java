/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.mining.text;

import static com.google.common.collect.Lists.newArrayList;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import be.uantwerpen.sniper.mining.text.TokenListOperator.DFFilter;
import be.uantwerpen.sniper.mining.text.TokenListOperator.EmptyTokenListFilter;
import be.uantwerpen.sniper.mining.text.TokenListOperator.TokenListStemmer;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.0.2
 * @version 0.0.2
 */
public enum TextLanguage {
	EN, NL;

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader("data/upload/kddmime/kddmime.txt"));
		StringBuilder builder = new StringBuilder();
		reader.lines().forEach(s -> {
			builder.append(s);
			builder.append(" ");
		});
		reader.close();

		SentenceExtractor e = SentenceExtractor.OpenNLPSentenceExtractor.newExtractorByLanguange(TextLanguage.EN);
		List<String> sentences = e.getSentences(builder.toString());

		sentences = new SentenceOperator.AlphabetOnlyKeeper().operate(sentences);
		List<List<String>> tokens = sentences.stream().map(s -> newArrayList(s.split(" ")))
				.collect(Collectors.toList());

		TokenListStemmer stemmer = new TokenListStemmer();
		tokens = stemmer.perform(tokens);

		DFFilter filter = new DFFilter();
		tokens = filter.perform(tokens);

		tokens = new EmptyTokenListFilter().perform(tokens);
	}
}
