/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.action.ActionBuilders.actionBuilder;
import static be.uantwerpen.sniper.controller.rest.Utils.getActiveWorksheet;
import static de.unibonn.realkd.common.base.Identifier.identifier;

import java.security.Principal;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.action.Action;
import be.uantwerpen.sniper.action.ParameterizedAction;
import be.uantwerpen.sniper.action.State;
import be.uantwerpen.sniper.action.StatefulAction;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.rest.resource.ActionUploadResourceV1;
import be.uantwerpen.sniper.rest.resource.CommandActionUploadResourcesV1;
import be.uantwerpen.sniper.rest.resource.WorksheetUploadResourcesV1;
import be.uantwerpen.sniper.rest.resource.builder.ActionResourceBuildersV1;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.patterns.NamedPatternCollection;

/**
 * Controller that deals with all access to individual descriptors using REST
 * calls.
 * 
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@RestController
public class ActionController {

	@Autowired
	private WorksheetController worksheetController;

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/actions", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> actions(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/actions", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> createAction(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@RequestBody ActionUploadResourceV1 actionUploadResource, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		if (CommandActionUploadResourcesV1.UndoRedoCommandUploadAction.class
				.isAssignableFrom(actionUploadResource.getClass())) {

			Action action = actionBuilder().build(activeWorksheet, identifier("tmp"), actionUploadResource);

			action.execute();

			return ResponseEntity.ok().build();
		}

		if (WorksheetUploadResourcesV1.SaveWorksheetUploadActionResource.class
				.isAssignableFrom(actionUploadResource.getClass())) {

			activeWorksheet.realKDWorkspace().overwrite(activeWorksheet.measureModel());
			activeWorksheet.realKDWorkspace().overwrite(activeWorksheet.visualizationModel());
			activeWorksheet.realKDWorkspace().overwrite(activeWorksheet.propertiesModel());

			try {
				activeWorksheet.realKDWorkspace()
						.overwrite(new NamedPatternCollection(Identifier.id("PatternCollection"), "PatternCollection",
								"Collection of patterns", activeWorksheet.brickModel().patterns()));
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
			}

			return ResponseEntity.ok().build();
		}

		if (activeWorksheet.actionManager().isRunningAction()) {
			return ResponseEntity.badRequest().build();
		}

		Identifier identifier = activeWorksheet.actionManager().newIdentifier();

		Action action = actionBuilder().build(activeWorksheet, identifier, actionUploadResource);

		if (ParameterizedAction.class.isAssignableFrom(action.getClass())) {
			activeWorksheet.actionManager().addAction(action);

			return ResponseEntity.status(HttpStatus.CREATED)
					.body(ActionResourceBuildersV1.actionResourceBuilder(worksheet).build(action));
		}

		activeWorksheet.actionManager().addAction(action);

		activeWorksheet.actionManager().runAction(action.identifier());

		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/actions/{actionId}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> getAction(@PathVariable("worksheetId") String worksheetId,
			@PathVariable("actionId") String actionId, Principal principal, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Optional<Action> action = activeWorksheet.actionManager().getAction(identifier(actionId));

		if (action.isPresent()) {
			if (StatefulAction.class.isAssignableFrom(action.get().getClass())) {
				return ResponseEntity.ok(((StatefulAction) action.get()).state());
			}

			return ResponseEntity.ok(State.FINISHED);
		}

		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/actions/{actionId}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> updateAction(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@PathVariable("actionId") String actionId, @RequestBody Map<String, Object> parameters,
			HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Optional<Action> action = activeWorksheet.actionManager().getAction(identifier(actionId));

		if (action.isPresent() && ParameterizedAction.class.isAssignableFrom(action.get().getClass())
				&& ((ParameterizedAction) action.get()).parameters(parameters)) {
			return ResponseEntity.ok(ActionResourceBuildersV1.parameterizedActionResourceBuilder(worksheet)
					.build((ParameterizedAction) action.get()));
		}

		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/actions/{actionId}", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> performAction(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@PathVariable("actionId") String actionId, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Optional<Action> action = activeWorksheet.actionManager().getAction(identifier(actionId));

		if (!action.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		activeWorksheet.actionManager().runAction(action.get().identifier());

		return ResponseEntity.ok(ActionResourceBuildersV1.actionResourceBuilder(worksheet).build(action.get()));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/actions/{actionId}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> cancelAction(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@PathVariable("actionId") String actionId, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Optional<Action> action = activeWorksheet.actionManager().getAction(identifier(actionId));

		if (!action.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		activeWorksheet.actionManager().cancelAction(action.get().identifier());

		return ResponseEntity.ok(ActionResourceBuildersV1.actionResourceBuilder(worksheet).build(action.get()));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/actions/{actionId}/parameters", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> getActionParameters(@PathVariable("worksheetId") String worksheetId,
			@PathVariable("actionId") String actionId, Principal principal, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Optional<Action> action = activeWorksheet.actionManager().getAction(identifier(actionId));

		if (!action.isPresent() || !ParameterizedAction.class.isAssignableFrom(action.get().getClass())) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(ActionResourceBuildersV1.parameterizedActionResourceBuilder(worksheet)
				.build((ParameterizedAction) action.get()));
	}

}
