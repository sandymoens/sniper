/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.controller.rest.Utils.getActiveWorksheet;
import static java.util.stream.Collectors.toList;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Maps;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.rest.resource.VisualizationResourcesV1;
import be.uantwerpen.sniper.rest.resource.builder.VisualizationResourceBuildersV1;
import be.uantwerpen.sniper.visualization.Visualization;
import be.uantwerpen.sniper.visualization.pattern.PatternVisualizations;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.0.1
 * @version 0.2.0
 */
@RestController
public class VisualizationController {

	@Autowired
	private WorksheetController worksheetController;

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/visualizations", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> visualizations(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		return ResponseEntity.ok(VisualizationResourceBuildersV1.visualizationsWithStateResourceBuilder()
				.build(activeWorksheet.visualizationModel()));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/visualizations", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> setVisualizations(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@RequestBody List<VisualizationResourcesV1.VisualizationWithStateUploadResource> visualizations,
			HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Map<String, Visualization<Pattern<?>, ?>> mapping = Maps.newHashMap();
//		Arrays.stream(RegisteredPatternVisualization.values())
//				.forEach(v -> mapping.put(v.getClass().getSimpleName(), v));
		PatternVisualizations.PATTERN_VISUALIZATIONS.stream().forEach(v -> mapping.put(v.caption(), v));

		List<Visualization<Pattern<?>, ?>> enabled = visualizations.stream().filter(m -> m.isActive)
				.map(v -> mapping.get(v.caption)).collect(toList());

		activeWorksheet.visualizationModel().enable(enabled);

		// activeWorksheet.get().realKDWorkspace().overwrite(visualizationModel);

		return ResponseEntity.ok(VisualizationResourceBuildersV1.visualizationsWithStateResourceBuilder()
				.build(activeWorksheet.visualizationModel()));
	}

}
