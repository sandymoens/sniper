/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.controller.rest.Utils.getActiveWorksheet;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.algorithm.Miners;
import be.uantwerpen.sniper.algorithm.PatternCollectionPostProcessors;
import be.uantwerpen.sniper.algorithm.PatternPostProcessors;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.rest.resource.AlgorithmsResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1;

/**
 * Controller that deals with mining algorithms such as the retrieval of a
 * applicable miners, the parameters, etc.
 * 
 * @author Sandy Moens
 * @since 0.0.2
 * @version 0.2.0
 */
@RestController
public class AlgorithmController {

	@Autowired
	private WorksheetController worksheetController;

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/miners", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> miners(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		List<AlgorithmsResourcesV1.SimpleAlgorithmResource> resources = Miners
				.minersForType(activeWorksheet.propositionalContext()).stream().sorted()
				.map(m -> new AlgorithmsResourcesV1.SimpleAlgorithmResource(m, m, "", "")).collect(Collectors.toList());

		return ResponseEntity.ok(new ResourcesV1.ListResource<>(resources));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/patternCollectionPostProcessors", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> patternCollectionPostProcessors(@PathVariable("worksheetId") String worksheetId,
			Principal principal, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		getActiveWorksheet(worksheet);

		List<AlgorithmsResourcesV1.SimpleAlgorithmResource> resources = PatternCollectionPostProcessors.postProcessors()
				.stream().sorted().map(p -> new AlgorithmsResourcesV1.SimpleAlgorithmResource(p, p, "", ""))
				.collect(Collectors.toList());

		return ResponseEntity.ok(new ResourcesV1.ListResource<>(resources));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/patternPostProcessors", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> patternPostProcessors(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		getActiveWorksheet(worksheet);

		List<AlgorithmsResourcesV1.SimpleAlgorithmResource> resources = PatternPostProcessors.postProcessors().stream()
				.sorted().map(p -> new AlgorithmsResourcesV1.SimpleAlgorithmResource(p, p, "", ""))
				.collect(Collectors.toList());

		return ResponseEntity.ok(new ResourcesV1.ListResource<>(resources));
	}

}
