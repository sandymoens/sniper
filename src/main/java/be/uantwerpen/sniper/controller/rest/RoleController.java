/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import be.uantwerpen.sniper.dao.entity.Role;
import be.uantwerpen.sniper.dao.service.RoleService;
import be.uantwerpen.sniper.rest.resource.builder.RoleResourceBuildersV1;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@Controller
public class RoleController {

	@Autowired
	private RoleService roleService;

	@GetMapping("/api/v1/admin/roles")
	public ResponseEntity<?> roles(HttpServletRequest request) {
		List<Role> roles = this.roleService.all();

		return ResponseEntity.ok(RoleResourceBuildersV1.simpleRoleResourceBuilder().build(roles));
	}

}