/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.service.UserService;
import be.uantwerpen.sniper.rest.resource.builder.LinksResourceBuildersV1;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@RestController
public class LinksController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/api/v1/links", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> links(Principal principal, HttpServletRequest request) {
		Optional<User> user = this.userService.getByUsernameInjected(principal.getName());

		if (!user.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(LinksResourceBuildersV1.linksResourceBuilder().build(user.get()));
	}

}
