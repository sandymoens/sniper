/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.common.Utils.method;
import static be.uantwerpen.sniper.controller.rest.Utils.getActiveWorksheet;
import static be.uantwerpen.sniper.dao.DataAccessHandlers.worksheetAccessHandler;
import static be.uantwerpen.sniper.rest.resource.builder.PagedResourceBuilders.genericPagedResourceBuilder;
import static be.uantwerpen.sniper.utils.MeasureRegister.measureRegister;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static de.unibonn.creedo.webapp.utils.ParameterToJsonConverter.convertRecursively;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.common.DiscretizationType;
import be.uantwerpen.sniper.controller.rest.errorhandling.WorksheetNotFoundException;
import be.uantwerpen.sniper.core.PatternExtension;
import be.uantwerpen.sniper.core.PatternExtensionsKeyMultiMeasures;
import be.uantwerpen.sniper.core.PatternExtensionsKeySingleMeasure;
import be.uantwerpen.sniper.core.PatternExtensionsKeys;
import be.uantwerpen.sniper.dao.DataAccessHandlers;
import be.uantwerpen.sniper.dao.Discretization;
import be.uantwerpen.sniper.dao.Discretizations;
import be.uantwerpen.sniper.dao.SaveDataParameter;
import be.uantwerpen.sniper.dao.SaveDataParameters.SaveWorksheetMetaInfoParameter;
import be.uantwerpen.sniper.dao.WorksheetMetaInfo;
import be.uantwerpen.sniper.dao.WorksheetMetaInfos;
import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.dao.entity.Workspace;
import be.uantwerpen.sniper.dao.service.SchemeService;
import be.uantwerpen.sniper.dao.service.UserService;
import be.uantwerpen.sniper.dao.service.WorksheetService;
import be.uantwerpen.sniper.dao.service.WorkspaceService;
import be.uantwerpen.sniper.pattern.PatternSerialForm;
import be.uantwerpen.sniper.rest.resource.AttributeResourcesV1.AttributeResource;
import be.uantwerpen.sniper.rest.resource.AttributeResourcesV1.SimpleAttributeResource;
import be.uantwerpen.sniper.rest.resource.CondensedVisualizationResourceV1;
import be.uantwerpen.sniper.rest.resource.PatternExtensionResourcesV1.PatternExtensionResource;
import be.uantwerpen.sniper.rest.resource.PatternExtensionResourcesV1.PropositionPatternExtensionResource;
import be.uantwerpen.sniper.rest.resource.QueryUploadResourcesV1;
import be.uantwerpen.sniper.rest.resource.QueryUploadResourcesV1.SortedPropositionsQuery;
import be.uantwerpen.sniper.rest.resource.WorksheetMetaInfoUploadResourceV1;
import be.uantwerpen.sniper.rest.resource.WorksheetMetaInfoUploadResourcesV1;
import be.uantwerpen.sniper.rest.resource.WorksheetUploadResourceV1;
import be.uantwerpen.sniper.rest.resource.builder.AttributeResourceBuildersV1;
import be.uantwerpen.sniper.rest.resource.builder.AttributeVisualizationResourceBuildersV1;
import be.uantwerpen.sniper.rest.resource.builder.PagedResourceBuilder;
import be.uantwerpen.sniper.rest.resource.builder.PatternExtensionResourceBuildersV1;
import be.uantwerpen.sniper.rest.resource.builder.ResourceBuilder;
import be.uantwerpen.sniper.rest.resource.builder.WorksheetResourceBuildersV1;
import de.unibonn.creedo.common.parameters.AbstractTransferableParameterState;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptors;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;

/**
 * Controller that deals with all data table accessing methods through REST
 * calls.
 * 
 * @author Sandy Moens
 * @since 0.1.0
 * @version 0.2.0
 */
@RestController
public class WorksheetController {

	@Autowired
	private SchemeService schemeService;

	@Autowired
	private WorksheetService worksheetService;

	@Autowired
	private WorkspaceService workspaceService;

	@Autowired
	private UserService userService;

	@Autowired
	private PagedResourcesAssembler<SimpleAttributeResource> attributePageAssembler;

	@Autowired
	private PagedResourcesAssembler<PatternExtensionResource> patternExtensionResourcePageAssembler;

	private PagedResourceBuilder<SimpleAttributeResource> pagedAttributeResourceBuilder;

	private PagedResourceBuilder<PatternExtensionResource> pagedPatternExtensionResourceBuilder;

	private PagedResourceBuilder<PatternExtensionResource> pagedPatternExtensionResourceBuilder() {
		return this.pagedPatternExtensionResourceBuilder != null ? this.pagedPatternExtensionResourceBuilder
				: (this.pagedPatternExtensionResourceBuilder = genericPagedResourceBuilder(
						this.patternExtensionResourcePageAssembler));
	}

	private PagedResourceBuilder<SimpleAttributeResource> pagedAttributeResourceBuilder() {
		return this.pagedAttributeResourceBuilder != null ? this.pagedAttributeResourceBuilder
				: (this.pagedAttributeResourceBuilder = genericPagedResourceBuilder(this.attributePageAssembler));
	}

	@RequestMapping(value = "/api/v1/worksheets", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> worksheets(HttpServletRequest request) {
		// stub method for HATEOAS
		return ResponseEntity.ok().build();
	}

	public Worksheet getWorksheetIfExistsAndAuthorised(String worksheetId, Principal principal) {
		Optional<Worksheet> worksheet = this.worksheetService.getInjected(worksheetId);

		if (!worksheet.isPresent()) {
			throw new WorksheetNotFoundException("Worksheet with id '" + worksheetId + "' does not exist.");
		}

		User user = Utils.getUserOrThrowError(this.userService, principal);

		Utils.isAuthorisedForEntity(user, Sets.newHashSet(user.getWorkspaces()), worksheet.get().getWorkspace().getId(),
				"workspace");

		return worksheet.get();
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> worksheet(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		return ResponseEntity.ok(WorksheetResourceBuildersV1
				.activeWorksheetResourceBuilder(activeWorksheet.propositionalContext()).build(worksheet));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<?> deleteWorksheet(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		if (!this.worksheetService.remove(worksheetId) || !worksheetAccessHandler().remove(worksheet)) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/api/v1/workspaces/{workspaceId}/worksheets", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> newWorksheet(@PathVariable("workspaceId") String workspaceId, Principal principal,
			@RequestBody WorksheetUploadResourceV1 worksheetUploadResource, HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		Utils.isAuthorisedForEntity(user, Sets.newHashSet(user.getWorkspaces()), workspaceId, "workspace");

		if (worksheetUploadResource.caption().isEmpty() || worksheetUploadResource.schemeId().isEmpty()) {
			return ResponseEntity.badRequest().build();
		}

		Optional<Workspace> workspace = this.workspaceService.get(workspaceId);

		if (!workspace.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Optional<Scheme> scheme = this.schemeService.get(worksheetUploadResource.schemeId());

		if (!scheme.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Worksheet worksheet = this.worksheetService.newObject();

		worksheet.setCaption(worksheetUploadResource.caption());
		worksheet.setDescription(worksheetUploadResource.description());
		worksheet.setWorkspace(workspace.get());
		worksheet.setScheme(scheme.get());

		if (!this.worksheetService.add(worksheet)) {
			this.worksheetService.remove(worksheet.getId());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		Optional<SaveDataParameter<Worksheet>> parameter = getSaveWorksheetMetaInfoParameter(worksheet,
				worksheetUploadResource.worksheetMetaInfo());

		if (!parameter.isPresent()
				|| !DataAccessHandlers.worksheetMetaInfoAccessHandler().save(parameter.get(), worksheet)) {
			this.worksheetService.remove(worksheet.getId());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return ResponseEntity.ok(WorksheetResourceBuildersV1.worksheetResourceBuilder().build(worksheet));
	}

	private Optional<WorksheetMetaInfo> getWorksheetMetaInfo(
			WorksheetMetaInfoUploadResourceV1 worksheetMetaInfoResource) {
		if (WorksheetMetaInfoUploadResourcesV1.DataTableWorksheetMetaInfoUploadResource.class
				.isAssignableFrom(worksheetMetaInfoResource.getClass())) {
			WorksheetMetaInfoUploadResourcesV1.DataTableWorksheetMetaInfoUploadResource r = (WorksheetMetaInfoUploadResourcesV1.DataTableWorksheetMetaInfoUploadResource) worksheetMetaInfoResource;

			return Optional.of(new WorksheetMetaInfos.DataTableWorksheetMetaInfo(
					new Discretizations.TypeBasedDiscretization(r.discretization, r.value),
					specificDiscretizations(r.discretizations)));
		} else if (WorksheetMetaInfoUploadResourcesV1.SequentialDataTableWorksheetMetaInfoUploadResource.class
				.isAssignableFrom(worksheetMetaInfoResource.getClass())) {
			WorksheetMetaInfoUploadResourcesV1.SequentialDataTableWorksheetMetaInfoUploadResource r = (WorksheetMetaInfoUploadResourcesV1.SequentialDataTableWorksheetMetaInfoUploadResource) worksheetMetaInfoResource;

			return Optional.of(new WorksheetMetaInfos.SequentialDataTableWorksheetMetaInfo(
					new Discretizations.TypeBasedDiscretization(r.discretization, r.value),
					specificDiscretizations(r.discretizations), r.idAttribute, r.distanceAttribute,
					r.generateSingleSequence));
		} else if (WorksheetMetaInfoUploadResourcesV1.SequenceWorksheetMetaInfoUploadResource.class
				.isAssignableFrom(worksheetMetaInfoResource.getClass())) {
			WorksheetMetaInfoUploadResourcesV1.SequenceWorksheetMetaInfoUploadResource r = (WorksheetMetaInfoUploadResourcesV1.SequenceWorksheetMetaInfoUploadResource) worksheetMetaInfoResource;

			return Optional.of(new WorksheetMetaInfos.SequenceWorksheetMetaInfo(
					new Discretizations.TypeBasedDiscretization(r.discretization, r.value)));
		} else if (WorksheetMetaInfoUploadResourcesV1.TransactionsWorksheetMetaInfoUploadResource.class
				.isAssignableFrom(worksheetMetaInfoResource.getClass())) {
			return Optional.of(new WorksheetMetaInfos.TransactionsWorksheetMetaInfo());
		}

		return Optional.empty();
	}

	private Map<String, Discretization> specificDiscretizations(Map<String, Map<String, String>> discretizations) {
		Map<String, Discretization> theDiscretizations = newHashMap();

		for (Entry<String, Map<String, String>> entry : discretizations.entrySet()) {
			Optional<Discretization> discretization = getDiscretization(entry.getValue().get("type"),
					entry.getValue().get("value"));

			if (discretization.isPresent()) {
				theDiscretizations.put(entry.getKey(), discretization.get());
			}
		}

		return theDiscretizations;
	}

	private Optional<Discretization> getDiscretization(String type, String value) {
		if (type.equals(DiscretizationType.CLUSTERING.toString())) {
			return Optional.of(new Discretizations.TypeBasedDiscretization(DiscretizationType.CLUSTERING,
					Integer.parseInt(value)));
		} else if (type.equals(DiscretizationType.EQUIDISTANT.toString())) {
			return Optional.of(new Discretizations.TypeBasedDiscretization(DiscretizationType.EQUIDISTANT,
					Integer.parseInt(value)));
		} else if (type.equals(DiscretizationType.EQUIFREQUENT.toString())) {
			return Optional.of(new Discretizations.TypeBasedDiscretization(DiscretizationType.EQUIFREQUENT,
					Integer.parseInt(value)));
		} else if (type.equals(DiscretizationType.FIXED_INTERVALS.toString())) {
			return Optional.of(new Discretizations.FixedDiscretization(getIntervals(value)));
		} else if (type.equals("DATE")) {
			return Optional.of(new Discretizations.DateBasedDiscretization(newArrayList(value.split(","))));
		}

		return Optional.empty();
	}

	private List<Double> getIntervals(String value) {
		return Arrays.stream(value.split(",")).map(v -> Double.parseDouble(v)).collect(Collectors.toList());
	}

	private Optional<SaveDataParameter<Worksheet>> getSaveWorksheetMetaInfoParameter(Worksheet worksheet,
			WorksheetMetaInfoUploadResourceV1 worksheetMetaInfoResource) {
		Optional<WorksheetMetaInfo> worksheetMetaInfo = getWorksheetMetaInfo(worksheetMetaInfoResource);

		if (!worksheetMetaInfo.isPresent()) {
			return Optional.empty();
		}

		return Optional.of(new SaveWorksheetMetaInfoParameter(worksheet, worksheetMetaInfo.get()));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/info", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> worksheetInfo(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		return ResponseEntity
				.ok(WorksheetResourceBuildersV1.worksheetInfoResourceBuilder(activeWorksheet).build(worksheet));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/properties", method = RequestMethod.GET, produces = "application/json")

	@ResponseBody
	public ResponseEntity<?> properties(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		List<AbstractTransferableParameterState> parameters = convertRecursively(activeWorksheet.properties());

		return ResponseEntity.ok(parameters);
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/properties", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> properties(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request, @RequestBody Map<String, String> properties) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		if (activeWorksheet.propertiesModel().update(properties)) {
			return ResponseEntity.ok().build();
		}

		List<AbstractTransferableParameterState> parameters = convertRecursively(activeWorksheet.properties());

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(parameters);
	}

	private static Pair<Optional<PatternDescriptor>, String> getPatternDescriptorAndPart(
			ActiveWorksheet activeWorksheet, PatternSerialForm<Pattern<?>> patternDescriptorSerialForm, String part) {
		PropositionalContext context = activeWorksheet.propositionalContext();

		if (patternDescriptorSerialForm == null) {
			if (SingleSequencePropositionalContext.class.isAssignableFrom(context.getClass())) {

				SingleSequencePropositionalContext sPropositionalContext = (SingleSequencePropositionalContext) context;

				Optional<Parameter<?>> property = activeWorksheet.propertiesModel().property("Window size");

				double windowSize = property.isPresent() ? (Double) property.get().current() : 10;

				return Pair.of(Optional.of(EpisodeDescriptors.create(sPropositionalContext, windowSize,
						GraphDescriptors.create(ImmutableList.of(), ImmutableList.of()))), "graph");
			} else {
				return Pair.of(Optional.of(LogicalDescriptors.create(context.population(), ImmutableList.of())),
						"elements");
			}
		} else {
			try {
				Optional<Pattern<?>> pattern = patternDescriptorSerialForm.build(activeWorksheet);

				if (pattern.isPresent()) {
					return Pair.of(Optional.of(pattern.get().descriptor()), part);
				}
			} catch (ValidationException e) {
				e.printStackTrace();
			}
		}

		return Pair.of(Optional.empty(), "");
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/propositions", params = {}, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> sortedPropositions(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@RequestBody SortedPropositionsQuery query, HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		if (query.measure == null || (query.patternSerialForm != null && query.part == null)) {
			return ResponseEntity.badRequest().build();
		}

		Pair<Optional<PatternDescriptor>, String> patternDescriptorAndPart = getPatternDescriptorAndPart(
				activeWorksheet, query.patternSerialForm, query.part);

		if (!patternDescriptorAndPart.getKey().isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		PatternExtensionsKeySingleMeasure key = PatternExtensionsKeys.newPatternExtensionsKeySingleMeasure(
				patternDescriptorAndPart.getKey().get(), patternDescriptorAndPart.getValue(),
				measureRegister().getMeasure(query.measure), query.patternSerialForm == null);

		List<PatternExtension> ppe = newArrayList(
				activeWorksheet.patternExtensionComputerForPropositions().computeExtensions(activeWorksheet, key));

		List<PatternExtensionResource> ppeResources = PatternExtensionResourceBuildersV1
				.patternExtensionResourceBuilder(activeWorksheet,
						measureRegister().measureValueType(measureRegister().getMeasure(query.measure)))
				.build(ppe);

		String id = activeWorksheet.cache(ppeResources);

		return ResponseEntity.created(linkTo(
				method(WorksheetController.class, "sortedPropositions", String.class, String.class, Principal.class,
						Pageable.class, String.class, String.class, String.class, HttpServletRequest.class).get(),
				worksheetId, id, null, null, null, null, null, null).toUri()).build();
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/propositions/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> sortedPropositions(@PathVariable("worksheetId") String worksheetId,
			@PathVariable("id") String id, Principal principal, Pageable pageable,
			@RequestParam(value = "attribute", required = false) String attributeName,
			@RequestParam(value = "proposition", required = false) String propositionValue,
			@RequestParam(value = "score", required = false) String score, HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Optional<Object> value = activeWorksheet.fromCache(id);

		if (!value.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		@SuppressWarnings("unchecked")
		List<PatternExtensionResource> resources = (List<PatternExtensionResource>) value.get();

		resources = resources.stream().filter(r -> {
			PropositionPatternExtensionResource pr = (PropositionPatternExtensionResource) r;

			if (attributeName != null && !pr.attributeName.contains(attributeName)) {
				return false;
			}
			if (propositionValue != null && !pr.propositionValue.contains(propositionValue)) {
				return false;
			}
			if (score != null) {
				if (score.startsWith("=")) {
					return pr.score == Double.valueOf(score.substring(1));
				} else if (score.startsWith("<=")) {
					return pr.score <= Double.valueOf(score.substring(2));
				} else if (score.startsWith("<")) {
					return pr.score < Double.valueOf(score.substring(1));
				} else if (score.startsWith(">=")) {
					return pr.score >= Double.valueOf(score.substring(2));
				} else if (score.startsWith(">")) {
					return pr.score > Double.valueOf(score.substring(1));
				} else if (!score.isEmpty()) {
					return pr.score == Double.valueOf(score);
				}
			}

			return true;
		}).collect(Collectors.toList());

		return ResponseEntity.ok(pagedPatternExtensionResourceBuilder().build(pageable, resources));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/propositions2", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> propositionsMultiMeasures(@PathVariable("worksheetId") String worksheetId,
			Principal principal, @RequestBody QueryUploadResourcesV1.PropositionsQuery query,
			HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Pair<Optional<PatternDescriptor>, String> patternDescriptorAndPart = getPatternDescriptorAndPart(
				activeWorksheet, query.patternSerialForm, query.part);

		if (!patternDescriptorAndPart.getKey().isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		PatternExtensionsKeyMultiMeasures key = PatternExtensionsKeys.newPatternExtensionsKeyMultiMeasures(
				patternDescriptorAndPart.getKey().get(), patternDescriptorAndPart.getValue(), ImmutableList
						.of(measureRegister().getMeasure(query.measure1), measureRegister().getMeasure(query.measure2)),
				query.patternSerialForm == null);

		List<PatternExtension> ppe = newArrayList(
				activeWorksheet.patternExtensionComputerForPropositions().computeExtensions(activeWorksheet, key));

		List<PatternExtensionResource> ppeResources = PatternExtensionResourceBuildersV1
				.patternExtensionResourceBuilderMultiMeasures(activeWorksheet.propositionalContext(),
						ImmutableList.of(
								measureRegister().measureValueType(measureRegister().getMeasure(query.measure1)),
								measureRegister().measureValueType(measureRegister().getMeasure(query.measure2))))
				.build(ppe);

		String id = activeWorksheet.cache(ppeResources);

		return ResponseEntity.created(
				linkTo(method(WorksheetController.class, "propositionsMultiMeasures", String.class, String.class,
						Principal.class, HttpServletRequest.class).get(), worksheetId, id, null, null).toUri())
				.build();
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/propositions2/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> propositionsMultiMeasures(@PathVariable("worksheetId") String worksheetId,
			@PathVariable("id") String id, Principal principal, HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Optional<Object> value = activeWorksheet.fromCache(id);

		if (!value.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		@SuppressWarnings("unchecked")
		List<PatternExtensionResource> resources = (List<PatternExtensionResource>) value.get();

		return ResponseEntity.ok(ImmutableMap.of("elements", resources));
	}

	/**
	 * Gets the list of attributes of the data table that is currently loaded
	 * into the session workspace.
	 * 
	 * @param request the request that holds the session state
	 * @return a sorted list of attributes found in the data
	 */
	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/attributes", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> attributes(@PathVariable("worksheetId") String worksheetId, Principal principal,
			Pageable pageable, HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		PropositionalContext propositionalContext = activeWorksheet.propositionalContext();

		List<SimpleAttributeResource> attributeResources;

		if (propositionalContext == null
				|| !TableBasedPropositionalContext.class.isAssignableFrom(propositionalContext.getClass())) {
			attributeResources = newArrayList();
		}

		DataTable datatable = ((TableBasedPropositionalContext) propositionalContext).getDatatable();

		ArrayList<Attribute<?>> attributes = newArrayList(datatable.attributes());

		attributeResources = AttributeResourceBuildersV1.simpleAttributeResourceBuilder().build(attributes);

		return ResponseEntity.ok(pagedAttributeResourceBuilder().build(pageable, attributeResources));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/attributes/{attributeId}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<AttributeResource> getAttributeInfo(@PathVariable("worksheetId") String worksheetId,
			Principal principal, @PathVariable("attributeId") String attributeId, HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		TableBasedPropositionalContext propositionalContext = (TableBasedPropositionalContext) activeWorksheet
				.propositionalContext();

		Optional<? extends Attribute<?>> attribute = propositionalContext.getDatatable()
				.attribute(Identifier.id(attributeId));

		if (!attribute.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		return ResponseEntity.ok(AttributeResourceBuildersV1.attributeResourceBuilder().build(attribute.get()));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/attributes/{attributeId}/visualizations/{visualizationId}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<CondensedVisualizationResourceV1> visualization(
			@PathVariable("worksheetId") String worksheetId, Principal principal,
			@PathVariable("attributeId") String attributeId, @PathVariable("visualizationId") String visualizationId,
			HttpServletRequest request) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		TableBasedPropositionalContext propositionalContext = (TableBasedPropositionalContext) activeWorksheet
				.propositionalContext();

		Optional<? extends Attribute<?>> attribute = propositionalContext.getDatatable()
				.attribute(Identifier.id(attributeId));

		if (!attribute.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		Optional<ResourceBuilder<Attribute<?>, CondensedVisualizationResourceV1>> attributeVisualizationResourceBuilder = AttributeVisualizationResourceBuildersV1
				.attributeVisualizationResourceBuilder(visualizationId, request.getParameterMap());

		if (!attributeVisualizationResourceBuilder.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		CondensedVisualizationResourceV1 resource = attributeVisualizationResourceBuilder.get().build(attribute.get());

		if (resource == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return ResponseEntity.ok(resource);
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/transactions", method = RequestMethod.GET, produces = "application/json")
	public void getDataTableAsTransactions(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request, HttpServletResponse response) {
		Worksheet worksheet = getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		PropositionalContext propositionalContext = activeWorksheet.propositionalContext();

		try {
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < propositionalContext.population().size(); i++) {
				Set<Integer> truthSet = propositionalContext.truthSet(i);
				truthSet.forEach(t -> builder.append(propositionalContext.propositions().get(t) + ";"));
				builder.setLength(builder.length() - 1);
				builder.append("\n");
			}

			// get your file as InputStream
			InputStream is = new ByteArrayInputStream(builder.toString().getBytes());
			// copy it to response's OutputStream
			org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}
	}

}
