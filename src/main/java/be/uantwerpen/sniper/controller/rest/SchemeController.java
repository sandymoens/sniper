/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.dao.DataAccessHandlers.schemeDataTableAccessHandler;
import static be.uantwerpen.sniper.dao.DataAccessHandlers.schemeMetaInfoAccessHandler;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Sets;

import be.uantwerpen.sniper.dao.DataAccessHandlers;
import be.uantwerpen.sniper.dao.SaveDataParameter;
import be.uantwerpen.sniper.dao.SaveDataParameters.SaveDataSchemeDataTableParameter;
import be.uantwerpen.sniper.dao.SaveDataParameters.SaveDataSchemeMetaInfoParameter;
import be.uantwerpen.sniper.dao.SchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.DataTableSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.SchemeAttribute;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TextAsBowTfIdfSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TextAsTransactionsSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TransactionsSchemeMetaInfo;
import be.uantwerpen.sniper.dao.entity.DataTable;
import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.dao.service.DataTableService;
import be.uantwerpen.sniper.dao.service.SchemeService;
import be.uantwerpen.sniper.dao.service.UserService;
import be.uantwerpen.sniper.rest.resource.SchemeUploadResourceV1;
import be.uantwerpen.sniper.rest.resource.SchemeUploadResourcesV1;
import be.uantwerpen.sniper.rest.resource.builder.SchemeResourceBuildersV1;
import be.uantwerpen.sniper.rest.resource.builder.SchemeResourceBuildersV1.SchemeWithMetaInfo;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@RestController
public class SchemeController {

	@Autowired
	private DataTableService dataTableService;

	@Autowired
	private SchemeService schemeService;

	@Autowired
	private UserService userService;

	@Autowired
	private WorksheetController worksheetController;

	@RequestMapping(value = "/api/v1/schemes", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> schemes(Principal principal, HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		return ResponseEntity
				.ok(SchemeResourceBuildersV1.schemesResourceBuilder().build(newArrayList(user.getSchemes())));
	}

	@RequestMapping(value = "/api/v1/schemes/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> scheme(@PathVariable("id") String id, Principal principal, HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		Utils.isAuthorisedForEntity(user, Sets.newHashSet(user.getSchemes()), id, "scheme");

		Optional<Scheme> scheme = this.schemeService.get(id);

		if (!scheme.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Optional<SchemeMetaInfo> schemeMetaInfo = DataAccessHandlers.schemeMetaInfoAccessHandler().load(scheme.get());

		if (!schemeMetaInfo.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Optional<List<List<String>>> rows = DataAccessHandlers.schemeDataTableAccessHandler().rows(scheme.get(),
				schemeMetaInfo.get().schemeType(), 10, 0);

		if (!rows.isPresent()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		SchemeWithMetaInfo schemeWithMetaInfo = new SchemeWithMetaInfo(scheme.get(), schemeMetaInfo.get(), rows.get());

		return ResponseEntity.ok(SchemeResourceBuildersV1.expandedSchemeResourceBuilder().build(schemeWithMetaInfo));
	}

	@RequestMapping(value = "/api/v1/schemes", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> newScheme(Principal principal, @RequestBody SchemeUploadResourceV1 schemeResource,
			HttpServletRequest request) {
		if (schemeResource.caption().isEmpty()) {
			return ResponseEntity.badRequest().build();
		}

		User user = Utils.getUserOrThrowError(this.userService, principal);

		Optional<DataTable> dataTable = this.dataTableService.get(schemeResource.dataTableId());

		if (!dataTable.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Scheme scheme = this.schemeService.newObject();

		scheme.setCaption(schemeResource.caption());
		scheme.setDescription(schemeResource.description());
		scheme.setDataTable(dataTable.get());

		if (!this.schemeService.add(scheme)) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		Optional<SaveDataParameter<Scheme>> parameter1 = getSaveDataSchemeDataTableParameter(scheme, schemeResource);
		Optional<SaveDataParameter<Scheme>> parameter2 = getSaveDataSchemeMetaInfoParameter(scheme, schemeResource);

		if (!parameter1.isPresent() || !parameter2.isPresent()
				|| !schemeDataTableAccessHandler().save(parameter1.get(), scheme)
				|| !schemeMetaInfoAccessHandler().save(parameter2.get(), scheme)) {
			this.schemeService.remove(scheme.getId());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		user.addScheme(scheme);

		if (!this.userService.updateUser(user).isPresent()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return scheme(scheme.getId(), principal, null);
	}

	private Optional<SchemeMetaInfo> getSchemeMetaInfo(SchemeUploadResourceV1 schemeResource) {
		if (SchemeUploadResourcesV1.DataTableSchemeUploadResource.class.isAssignableFrom(schemeResource.getClass())) {
			SchemeUploadResourcesV1.DataTableSchemeUploadResource r = (SchemeUploadResourcesV1.DataTableSchemeUploadResource) schemeResource;

			List<SchemeMetaInfos.SchemeAttribute> attributes = r.attributes().stream()
					.map(a -> new SchemeAttribute(a.caption.replaceAll(" ", "_"), a.type, a.active, a.icon))
					.collect(toList());

			return Optional.of(new DataTableSchemeMetaInfo(r.caption(), r.description(), r.delimiter(), r.hasHeaders(),
					r.dateTimeFormat(), attributes));
		} else if (SchemeUploadResourcesV1.TransactionsSchemeUploadResource.class
				.isAssignableFrom(schemeResource.getClass())) {
			SchemeUploadResourcesV1.TransactionsSchemeUploadResource r = (SchemeUploadResourcesV1.TransactionsSchemeUploadResource) schemeResource;

			return Optional.of(new TransactionsSchemeMetaInfo(r.caption(), r.description(), r.delimiter()));
		} else if (SchemeUploadResourcesV1.TextAsTransactionsSchemeUploadResource.class
				.isAssignableFrom(schemeResource.getClass())) {
			SchemeUploadResourcesV1.TextAsTransactionsSchemeUploadResource r = (SchemeUploadResourcesV1.TextAsTransactionsSchemeUploadResource) schemeResource;

			return Optional
					.of(new TextAsTransactionsSchemeMetaInfo(r.caption(), r.description(), r.recognizeSentences(),
							r.alphabeticalOnly(), r.filterPOS(), r.stemTokens(), r.dFFilterType(), r.dFFilterValue()));
		} else if (SchemeUploadResourcesV1.TextAsBowTfIdfSchemeUploadResource.class
				.isAssignableFrom(schemeResource.getClass())) {
			SchemeUploadResourcesV1.TextAsBowTfIdfSchemeUploadResource r = (SchemeUploadResourcesV1.TextAsBowTfIdfSchemeUploadResource) schemeResource;

			return Optional.of(new TextAsBowTfIdfSchemeMetaInfo(r.caption(), r.description(), r.recognizeSentences(),
					r.alphabeticalOnly(), r.filterPOS(), r.stemTokens(), r.numberOfBins()));
		}

		return Optional.empty();
	}

	private Optional<SaveDataParameter<Scheme>> getSaveDataSchemeDataTableParameter(Scheme scheme,
			SchemeUploadResourceV1 schemeResource) {
		Optional<SchemeMetaInfo> schemeMetaInfo = getSchemeMetaInfo(schemeResource);

		if (!schemeMetaInfo.isPresent()) {
			return Optional.empty();
		}

		return Optional.of(new SaveDataSchemeDataTableParameter(scheme.getDataTable(), scheme, schemeMetaInfo.get()));
	}

	private Optional<SaveDataParameter<Scheme>> getSaveDataSchemeMetaInfoParameter(Scheme scheme,
			SchemeUploadResourceV1 schemeResource) {
		Optional<SchemeMetaInfo> schemeMetaInfo = getSchemeMetaInfo(schemeResource);

		if (!schemeMetaInfo.isPresent()) {
			return Optional.empty();
		}

		return Optional.of(new SaveDataSchemeMetaInfoParameter(scheme.getDataTable(), scheme, schemeMetaInfo.get()));
	}

	@RequestMapping(value = "/api/v1/schemes/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> deleteScheme(@PathVariable("id") String id, Principal principal,
			HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		Utils.isAuthorisedForEntity(user, Sets.newHashSet(user.getSchemes()), id, "scheme");

		Optional<Scheme> scheme = this.schemeService.getInjected(id);

		if (!scheme.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		boolean errors = false;

		for (Worksheet worksheet : scheme.get().getWorksheets()) {
			errors |= !this.worksheetController.deleteWorksheet(worksheet.getId(), principal, request).getStatusCode()
					.equals(HttpStatus.OK);
		}

		errors |= !this.schemeService.remove(id);
		errors |= !schemeDataTableAccessHandler().remove(scheme.get());
		errors |= !schemeMetaInfoAccessHandler().remove(scheme.get());

		if (errors) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return ResponseEntity.ok().build();
	}

}
