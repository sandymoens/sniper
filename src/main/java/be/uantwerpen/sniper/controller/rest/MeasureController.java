/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.controller.rest.Utils.getActiveWorksheet;
import static be.uantwerpen.sniper.utils.MeasureRegister.measureRegister;
import static java.util.Comparator.naturalOrder;
import static java.util.stream.Collectors.toList;

import java.security.Principal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.rest.resource.MeasureResourcesV1;
import be.uantwerpen.sniper.rest.resource.builder.CondensedVisualizationResourceBuildersV1;
import be.uantwerpen.sniper.rest.resource.builder.MeasureResourceBuildersV1;
import be.uantwerpen.sniper.utils.MeasureRegister;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 * Controller that deals with all access to the list of activated measures
 * through REST calls.
 * 
 * @author Sandy Moens
 * @since 0.0.1
 * @version 0.2.0
 */
@RestController
public class MeasureController {

	@Autowired
	private WorksheetController worksheetController;

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/measures", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> measures(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		return ResponseEntity
				.ok(MeasureResourceBuildersV1.measuresWithStateResourceBuilder().build(activeWorksheet.measureModel()));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/measures", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> setMeasures(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@RequestBody List<MeasureResourcesV1.MeasureWithStateUploadResource> measures, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		List<Measure> measuresList = measures.stream().filter(m -> m.isActive)
				.map(m -> measureRegister().getMeasure(m.caption)).collect(toList());

		activeWorksheet.measureModel().enable(measuresList);

		return ResponseEntity
				.ok(MeasureResourceBuildersV1.measuresWithStateResourceBuilder().build(activeWorksheet.measureModel()));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/sortingMeasures", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> sortingMeasures(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		return ResponseEntity.ok(MeasureResourceBuildersV1.measuresResourceBuilder()
				.build(activeWorksheet.measureModel().availableMeasures()));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/measures/{measure}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> measure(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@PathVariable("measure") String measureName, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Measure measure = MeasureRegister.measureRegister().getMeasure(measureName);
		Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> procedures = MeasureRegister
				.measureRegister().getMeasurementProcedures(measure);

		List<Double> values = activeWorksheet.patternController().brickModel().patterns().stream().map(p -> {
			if (!p.hasMeasure(measure)) {
				PatternDescriptor descriptor = p.descriptor();

				for (MeasurementProcedure<? extends Measure, ? super PatternDescriptor> procedure : procedures) {
					if (procedure.isApplicable(descriptor)) {
						return procedure.perform(descriptor).value();
					}
				}

				return Double.NaN;
			}

			return p.measurement(measure).get().value();
		}).filter(v -> !Double.isNaN(v)).collect(Collectors.toList());

		values.sort(naturalOrder());

		return ResponseEntity.ok(new MeasureResourcesV1.MeasureVisualizationResource(measure.identifier().toString(),
				ImmutableList.of(
						CondensedVisualizationResourceBuildersV1.newBoxPlotVisualizationResourceBuilder().build(values),
						CondensedVisualizationResourceBuildersV1.newMetricHistogramVisualizationResourceBuilder()
								.build(values),
						CondensedVisualizationResourceBuildersV1.newNormalDistributionVisualizationResourceBuilder()
								.build(values))));
	}

}
