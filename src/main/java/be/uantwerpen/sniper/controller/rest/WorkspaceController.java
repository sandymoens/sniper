/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static com.google.common.collect.Lists.newArrayList;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Sets;

import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.dao.entity.Workspace;
import be.uantwerpen.sniper.dao.service.UserService;
import be.uantwerpen.sniper.dao.service.WorkspaceService;
import be.uantwerpen.sniper.rest.resource.builder.WorkspaceResourceBuildersV1;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@RestController
public class WorkspaceController {

	@Autowired
	private WorkspaceService workspaceService;

	@Autowired
	private UserService userService;

	@Autowired
	private WorksheetController worksheetController;

	@RequestMapping(value = "/api/v1/workspaces", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> workspaces(Principal principal, HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		return ResponseEntity
				.ok(WorkspaceResourceBuildersV1.workspacesResourceBuilder().build(newArrayList(user.getWorkspaces())));
	}

	@RequestMapping(value = "/api/v1/workspaces", params = {
			"fields" }, method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> workspaces(Principal principal, @RequestParam("fields") List<String> fields,
			HttpServletRequest request) {
		if (!fields.contains("worksheets")) {
			return workspaces(principal, request);
		}

		User user = Utils.getUserOrThrowError(this.userService, principal);

		List<String> workspaceIds = user.getWorkspaces().stream().map(w -> w.getId()).collect(Collectors.toList());

		return ResponseEntity.ok(WorkspaceResourceBuildersV1.expandedWorkspacesResourceBuilder()
				.build(this.workspaceService.getInjected(workspaceIds)));
	}

	@RequestMapping(value = "/api/v1/workspaces/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> workspace(@PathVariable("id") String id, Principal principal, HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		Utils.isAuthorisedForEntity(user, Sets.newHashSet(user.getWorkspaces()), id, "workspace");

		Optional<Workspace> workspace = this.workspaceService.getInjected(id);

		if (!workspace.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(WorkspaceResourceBuildersV1.expandedWorkspaceResourceBuilder().build(workspace.get()));
	}

	@RequestMapping(value = "/api/v1/workspaces", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> newWorkspace(Principal principal, Workspace workspace, HttpServletRequest request) {
		if (workspace.caption().isEmpty()) {
			return ResponseEntity.badRequest().build();
		}

		User user = Utils.getUserOrThrowError(this.userService, principal);

		Workspace newWorkspace = this.workspaceService.newObject();

		newWorkspace.setCaption(workspace.caption());
		newWorkspace.setDescription(workspace.description());

		if (!this.workspaceService.add(newWorkspace)) {
			this.workspaceService.remove(newWorkspace.getId());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		user.addWorkspace(newWorkspace);

		if (!this.userService.updateUser(user).isPresent()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return ResponseEntity.ok(WorkspaceResourceBuildersV1.workspaceResourceBuilder().build(newWorkspace));
	}

	@RequestMapping(value = "/api/v1/workspaces/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> deleteWorkspace(@PathVariable("id") String id, Principal principal,
			HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		Utils.isAuthorisedForEntity(user, Sets.newHashSet(user.getWorkspaces()), id, "workspace");

		Optional<Workspace> workspace = this.workspaceService.getInjected(id);

		if (!workspace.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		boolean errors = false;

		for (Worksheet worksheet : workspace.get().getWorksheets()) {
			errors |= !this.worksheetController.deleteWorksheet(worksheet.getId(), principal, request).getStatusCode()
					.equals(HttpStatus.OK);
		}

		errors |= !this.workspaceService.remove(id);

		if (errors) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return ResponseEntity.ok().build();
	}

}
