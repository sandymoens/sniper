/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import be.uantwerpen.sniper.common.UpdateUserValidator;
import be.uantwerpen.sniper.common.UserValidator;
import be.uantwerpen.sniper.dao.entity.Role;
import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.service.RoleService;
import be.uantwerpen.sniper.dao.service.UserService;
import be.uantwerpen.sniper.rest.resource.UserUploadResourceV1;
import be.uantwerpen.sniper.rest.resource.UserUploadResourcesV1;
import be.uantwerpen.sniper.rest.resource.builder.UserResourceBuildersV1;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UpdateUserValidator updateUserValidator;

	@Autowired
	private UserValidator userValidator;

	@GetMapping("/registration")
	public String registration(Model model) {
		model.addAttribute("userForm", new User());

		return "main/registration";
	}

	@PostMapping(value = "/registration")
	public String registration(Model model, @ModelAttribute("userForm") User user, BindingResult bindingResult) {
		this.userValidator.validate(user, bindingResult);

		if (bindingResult.hasErrors()) {
			return "main/registration";
		}

		user.setPassword(this.passwordEncoder.encode(user.getPassword()));
		user.setPasswordConfirm(this.passwordEncoder.encode(user.getPasswordConfirm()));

		Optional<User> newUser = this.userService.addNewUser(user);

		if (!newUser.isPresent()) {
			bindingResult.addError(
					new ObjectError("Database", "A problem occurred while creating the user in the database."));
			return "main/registration";
		}

		model.addAttribute("info",
				"User is created but is currently inactive. Please contact the administrator to enable this account.");

		return "redirect:login";
	}

	// Spring Security see this :
	@GetMapping("/login")
	public String login(Model model, String error, String info, String logout) {
		if (error != null) {
			model.addAttribute("error", "Your username and password is invalid.");
		}

		if (info != null) {
			model.addAttribute("info", info);
		}

		if (logout != null) {
			model.addAttribute("message", "You have been logged out successfully.");
		}

		return "main/login";
	}

	@GetMapping("/api/v1/admin/users")
	public ResponseEntity<?> users(HttpServletRequest request) {
		List<User> users = this.userService.allInjected();

		return ResponseEntity.ok(UserResourceBuildersV1.fullUserResourceBuilder().build(users));
	}

	@GetMapping("/api/v1/users/{userId}")
	public ResponseEntity<?> user(@PathVariable Integer userId, HttpServletRequest request) {
		Optional<User> user = this.userService.get(userId);

		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(UserResourceBuildersV1.simpleUserResourceBuilder().build(user.get()));
	}

	@PatchMapping(value = "/api/v1/users/{userId}")
	public ResponseEntity<?> updateUser(@PathVariable Integer userId,
			@RequestBody UserUploadResourceV1 userUploadResource, BindingResult bindingResult,
			HttpServletRequest request) {
		if (UserUploadResourcesV1.UserWithPassword.class.isAssignableFrom(userUploadResource.getClass())) {
			return updateUserPassword(userId, (UserUploadResourcesV1.UserWithPassword) userUploadResource,
					bindingResult, request);
		} else if (UserUploadResourcesV1.UserWithAuthorities.class.isAssignableFrom(userUploadResource.getClass())) {
			return updateUserAuthorities(userId, (UserUploadResourcesV1.UserWithAuthorities) userUploadResource,
					bindingResult, request);
		}

		return ResponseEntity.badRequest().build();
	}

	private ResponseEntity<?> updateUserPassword(@PathVariable Integer userId,
			@RequestBody UserUploadResourcesV1.UserWithPassword userWithPassword, BindingResult bindingResult,
			HttpServletRequest request) {
		Optional<User> user = this.userService.getInjected(userId);

		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		User newUser = user.get();
		newUser.setPassword(userWithPassword.password);
		newUser.setPasswordConfirm(userWithPassword.passwordConfirm);

		this.updateUserValidator.validate(newUser, bindingResult);

		newUser.setPassword(this.passwordEncoder.encode(userWithPassword.password));
		newUser.setPasswordConfirm(this.passwordEncoder.encode(userWithPassword.passwordConfirm));

		if (bindingResult.hasErrors()) {
			return ResponseEntity.badRequest().build();
		}

		this.userService.updateUser(newUser);

		return ResponseEntity.ok(UserResourceBuildersV1.simpleUserResourceBuilder().build(user.get()));
	}

	private ResponseEntity<?> updateUserAuthorities(@PathVariable Integer userId,
			@RequestBody UserUploadResourcesV1.UserWithAuthorities userWithAuthorities, BindingResult bindingResult,
			HttpServletRequest request) {
		Optional<User> user = this.userService.get(userId);

		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Set<Role> roles = userWithAuthorities.roles.stream().map(role -> this.roleService.findRoleByName(role))
				.filter(role -> role.isPresent()).map(role -> role.get()).collect(toSet());

		User newUser = user.get();
		newUser.setRoles(roles);
		newUser.setEnabled(userWithAuthorities.enabled);

		this.userService.updateUser(newUser);

		return ResponseEntity.ok(UserResourceBuildersV1.fullUserResourceBuilder().build(user.get()));
	}

	@DeleteMapping(value = "/api/v1/users/{userId}")
	public ResponseEntity<?> deleteUser(@PathVariable Integer userId, HttpServletRequest request) {
		if (this.userService.remove(userId)) {
			return ResponseEntity.ok().build();
		}

		return ResponseEntity.badRequest().build();
	}

}