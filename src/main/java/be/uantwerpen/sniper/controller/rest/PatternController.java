/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.algorithm.utils.PatternTypeIdentifiers.newPatternTypeIdentifier;
import static be.uantwerpen.sniper.controller.rest.Utils.getActiveWorksheet;
import static be.uantwerpen.sniper.rest.resource.builder.PagedResourceBuilders.genericPagedResourceBuilder;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newTreeSet;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.algorithm.utils.PatternTypeIdentifier;
import be.uantwerpen.sniper.common.TypeSelectorType;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.model.MeasureModel;
import be.uantwerpen.sniper.model.bricks.Brick;
import be.uantwerpen.sniper.rest.resource.PatternResourcesV1.IndexedPatternWithMeasurementsResource;
import be.uantwerpen.sniper.rest.resource.ResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1.ListResource;
import be.uantwerpen.sniper.rest.resource.builder.PagedResourceBuilder;
import be.uantwerpen.sniper.rest.resource.builder.PatternResourceBuildersV1;
import be.uantwerpen.sniper.rest.resource.builder.ResourceBuilder;
import be.uantwerpen.sniper.rest.resource.builder.ResourceBuildersV1;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.sequences.SequenceEvent;
import de.unibonn.realkd.data.sequences.TableBasedSingleSequencePropositionalContext;
import de.unibonn.realkd.data.sequences.Window;
import de.unibonn.realkd.patterns.LocalPatternDescriptor;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.episodes.WindowBasedLocalPatternDescriptor;

/**
 * Controller that deals with all access to the current list of patterns through
 * REST calls.
 * 
 * @author Sandy Moens
 * @since 0.0.1
 * @version 0.2.0
 */
@RestController
public class PatternController {

	@Autowired
	private WorksheetController worksheetController;

	@Autowired
	private PagedResourcesAssembler<IndexedPatternWithMeasurementsResource> patternPageAssembler;

	private PagedResourceBuilder<IndexedPatternWithMeasurementsResource> pagedPatternsResourceBuilder;

	private PagedResourceBuilder<IndexedPatternWithMeasurementsResource> pagedPatternsResourceBuilder() {
		return this.pagedPatternsResourceBuilder != null ? this.pagedPatternsResourceBuilder
				: (this.pagedPatternsResourceBuilder = genericPagedResourceBuilder(this.patternPageAssembler));
	}

	@Autowired
	private PagedResourcesAssembler<ListResource<List<String>>> listResourceAssembler;

	/**
	 * Gets the current list of patterns in the session.
	 * 
	 * @param request the request that holds the session state
	 * @return a list of meta information about all patterns in the session
	 */
	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/patterns", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> patterns(@PathVariable("worksheetId") String worksheetId, Principal principal,
			Pageable pageable, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		MeasureModel measureModel = activeWorksheet.measureModel();

		boolean isDone = activeWorksheet.patternController().measureComputerDaemon().isDone();

		List<IndexedPatternWithMeasurementsResource> patternResources = filteredPatternResources(
				PatternResourceBuildersV1.indexedPatternWithMeasurementsResourceBuilder(activeWorksheet, measureModel),
				activeWorksheet.filterBrickModel().typeSelectorType(), activeWorksheet.filterBrickModel().pattern(),
				activeWorksheet.brickModel().bricks());

		return ResponseEntity
				.ok(ResourceBuildersV1.stringObjectMapResourceBuilder().build(ImmutableMap.of("doneComputing", isDone,
						"patterns", pagedPatternsResourceBuilder().build(pageable, patternResources))));
	}

	private List<IndexedPatternWithMeasurementsResource> filteredPatternResources(
			ResourceBuilder<Brick<Pattern<?>, ?>, IndexedPatternWithMeasurementsResource> simplePatternResourceBuilder,
			TypeSelectorType typeSelectorType, Optional<Pattern<?>> pattern, List<Brick<Pattern<?>, ?>> bricks) {
		if (!pattern.isPresent()) {
			List<IndexedPatternWithMeasurementsResource> patternResources = simplePatternResourceBuilder.build(bricks);

			int ix = 0;

			for (IndexedPatternWithMeasurementsResource patternResource : patternResources) {
				patternResource.ix = ix++;
			}

			return patternResources;
		}

		PatternTypeIdentifier patternTypeIdentifier = newPatternTypeIdentifier();

		List<IndexedPatternWithMeasurementsResource> patternResources = Lists.newArrayList();

		int ix = 0;

		for (Brick<Pattern<?>, ?> brick : bricks) {
			if (patternTypeIdentifier.identify(typeSelectorType, pattern.get(), brick.content())) {
				IndexedPatternWithMeasurementsResource patternResource = simplePatternResourceBuilder.build(brick);
				patternResource.ix = ix;
				patternResources.add(patternResource);
			}

			ix++;
		}

		return patternResources;
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/patterns/{patternIx}/visualizations", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> getVisualisation(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@PathVariable("patternIx") int patternIx, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Pattern<?> pattern = activeWorksheet.brickModel().pattern(patternIx);

		Map<String, Object> visualizations = activeWorksheet.patternVisualizer().getVisualizations(pattern);

		return ResponseEntity.ok(ResourceBuildersV1.stringObjectMapResourceBuilder().build(visualizations));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/patterns/{patternIx}/rows", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> getRows(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@PathVariable("patternIx") int patternIx, Pageable pageable, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Pattern<?> pattern = activeWorksheet.brickModel().patterns().get(patternIx);

		if (activeWorksheet.propositionalContext() instanceof TableBasedSingleSequencePropositionalContext) {
			TableBasedSingleSequencePropositionalContext context = (TableBasedSingleSequencePropositionalContext) activeWorksheet
					.propositionalContext();

			if (!(pattern.descriptor() instanceof WindowBasedLocalPatternDescriptor)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}

			WindowBasedLocalPatternDescriptor descriptor = (WindowBasedLocalPatternDescriptor) pattern.descriptor();

			List<List<String>> rows = Lists.newArrayList();

			double windowSize = ((Parameter<Double>) activeWorksheet.property("Window size").get()).current();

			List<Window> extendedWindows = descriptor.windows().stream().map(w -> {
				double diff = w.end() - w.start();
				diff = windowSize - diff;
				return Window.window(w.start() - diff, w.end() + diff);
			}).collect(Collectors.toList());

			Iterator<SequenceEvent<?>> iterator = context.events().iterator();
			SequenceEvent<?> event = iterator.next();

			for (Window window : extendedWindows) {
				rows.add(ImmutableList.of(String.format("[%.5f, %.5f]", window.start(), window.end()), "", ""));
				do {
					if (window.start() <= event.doubleValue() && event.doubleValue() <= window.end()) {
						rows.add(ImmutableList.of("", String.valueOf(event.value()), event.proposition().toString()));
					} else if (event.doubleValue() > window.end()) {
						break;
					}
					event = iterator.next();
				} while (iterator.hasNext());
				rows.add(ImmutableList.of("", "", ""));
			}

			return ResponseEntity.ok(genericPagedResourceBuilder(this.listResourceAssembler).buildPaged(pageable,
					newArrayList(new ResourcesV1.ListResource<>(rows.subList((int) pageable.getOffset(),
							(int) Math.min(pageable.getOffset() + pageable.getPageSize(), rows.size())))),
					rows.size()));
		}
		PropositionalContext context = activeWorksheet.propositionalContext();

		List<List<String>> rows = Lists.newArrayList();

		if (!LocalPatternDescriptor.class.isAssignableFrom(pattern.descriptor().getClass())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

		for (int objectId : ((LocalPatternDescriptor) pattern.descriptor()).supportSet()) {
			rows.add(newTreeSet(context.truthSet(objectId)).stream().map(o -> context.proposition(o).name())
					.collect(Collectors.toList()));
		}

		return ResponseEntity
				.ok(genericPagedResourceBuilder(this.listResourceAssembler).buildPaged(pageable,
						newArrayList(new ResourcesV1.ListResource<>(rows.subList((int) pageable.getOffset(),
								(int) Math.min(pageable.getOffset() + pageable.getPageSize(), rows.size())))),
						rows.size()));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/patterns/download", method = RequestMethod.GET, produces = "application/text")
	@ResponseBody
	public ResponseEntity<?> downloadPatterns(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request, HttpServletResponse response) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		MeasureModel measureModel = activeWorksheet.measureModel();
		final Collection<Measure> measures = measureModel.enabled();

		try {
			StringBuilder builder = new StringBuilder();

			builder.append("Pattern");

			for (Measure measure : measures) {
				builder.append(";");
				builder.append(measure.caption());
			}
			builder.append("\n");

			activeWorksheet.brickModel().patterns().stream().forEach(pattern -> {
				builder.append("\"");
				builder.append(pattern.descriptor().toString());
				builder.append("\"");

				for (Measure measure : measures) {
					builder.append(";");
					builder.append(pattern.hasMeasure(measure)
							? String.format("%.5f", pattern.value(measure)).replace('.', ',')
							: "N/A");
				}
				builder.append("\n");
			});

			// get your file as InputStream
			InputStream is = new ByteArrayInputStream(builder.toString().getBytes());
			// copy it to response's OutputStream
			org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok().build();
	}

}
