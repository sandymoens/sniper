/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.controller.rest.Utils.getActiveWorksheet;

import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.common.TypeSelectorType;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.model.MeasureModel;
import be.uantwerpen.sniper.model.bricks.Brick;
import be.uantwerpen.sniper.rest.resource.PatternResourcesV1.IndexedPatternWithMeasurementsResource;
import be.uantwerpen.sniper.rest.resource.ResourcesV1;
import be.uantwerpen.sniper.rest.resource.builder.PatternResourceBuildersV1;
import de.unibonn.realkd.patterns.Pattern;

/**
 * 
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@RestController
public class FilterPatternController {

	@Autowired
	private WorksheetController worksheetController;

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/patternFilters/current/pattern", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> filterPattern(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		Optional<Brick<Pattern<?>, ?>> brick = activeWorksheet.filterBrickModel().brick();

		if (!brick.isPresent()) {
			return ResponseEntity.ok().build();
		}

		MeasureModel measureModel = activeWorksheet.measureModel();

		IndexedPatternWithMeasurementsResource patternResource = PatternResourceBuildersV1
				.indexedPatternWithMeasurementsResourceBuilder(activeWorksheet, measureModel).build(brick.get());

		return ResponseEntity.ok(patternResource);
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/patternFilters/current/parameters/current", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> parameters(@PathVariable("worksheetId") String worksheetId, Principal principal,
			HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		return ResponseEntity.ok(new ResourcesV1.MapResource<>(
				ImmutableMap.of("typeSelectorType", activeWorksheet.filterBrickModel().typeSelectorType())));
	}

	@RequestMapping(value = "/api/v1/worksheets/{worksheetId}/patternFilters/current/parameters/current", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> parameters(@PathVariable("worksheetId") String worksheetId, Principal principal,
			@RequestParam("typeSelectorType") TypeSelectorType typeSelectorType, HttpServletRequest request) {
		Worksheet worksheet = this.worksheetController.getWorksheetIfExistsAndAuthorised(worksheetId, principal);

		ActiveWorksheet activeWorksheet = getActiveWorksheet(worksheet);

		activeWorksheet.filterBrickModel().typeSelectorType(typeSelectorType);

		return ResponseEntity.ok().build();
	}

}
