/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.ActiveWorksheetControllers.activeWorksheetController;

import java.security.Principal;
import java.util.Optional;
import java.util.Set;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.controller.rest.errorhandling.InternalServerError;
import be.uantwerpen.sniper.controller.rest.errorhandling.UserNotFoundException;
import be.uantwerpen.sniper.controller.rest.errorhandling.UserUnauthorisedException;
import be.uantwerpen.sniper.dao.entity.RepositoryEntity;
import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.dao.service.UserService;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Utils {

	public static User getUserOrThrowError(UserService userService, Principal principal) throws UserNotFoundException {
		Optional<User> user = userService.getByUsernameInjected(principal.getName());

		if (!user.isPresent()) {
			throw new UserNotFoundException("User with name '" + principal.getName() + "' does not exist.");
		}

		return user.get();
	}

	public static void isAuthorisedForEntity(User user, Set<RepositoryEntity<String>> entities, String id,
			String entityType) throws UserUnauthorisedException {
		for (RepositoryEntity<String> entity : entities) {
			if (entity.getId().equals(id)) {
				return;
			}
		}

		throw new UserUnauthorisedException("User with username '" + user.getUsername() + "' is unauthorised to view "
				+ entityType + " with id '" + id + "'");
	}

	public static ActiveWorksheet getActiveWorksheet(Worksheet worksheet) {
		Optional<ActiveWorksheet> activeWorksheet = activeWorksheetController().activeWorksheet(worksheet);

		if (!activeWorksheet.isPresent()) {
			throw new InternalServerError("A problem occurred while loading the worksheet.");
		}

		return activeWorksheet.get();
	}

}
