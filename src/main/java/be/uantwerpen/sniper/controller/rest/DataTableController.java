/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.controller.rest;

import static be.uantwerpen.sniper.common.Utils.method;
import static be.uantwerpen.sniper.dao.DataAccessHandlers.dataTableAccessHandler;
import static be.uantwerpen.sniper.rest.resource.builder.PagedResourceBuilders.genericPagedResourceBuilder;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.lang.reflect.Method;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Sets;

import be.uantwerpen.sniper.dao.SaveDataParameters.SaveDataFromFileParameter;
import be.uantwerpen.sniper.dao.SaveDataParameters.SaveDataFromSQLConnectionParameter;
import be.uantwerpen.sniper.dao.entity.DataTable;
import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.service.DataTableService;
import be.uantwerpen.sniper.dao.service.UserService;
import be.uantwerpen.sniper.rest.resource.ResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1.ListResource;
import be.uantwerpen.sniper.rest.resource.builder.DataTableResourceBuildersV1;

/**
 * Controller that deals with data repository related operations such as storing
 * of new data or deleting data.
 * 
 * @author Sandy Moens
 * @since 0.1.0
 * @version 0.2.0
 */
@RestController
public class DataTableController {

	@Autowired
	private DataTableService dataTableService;

	@Autowired
	private UserService userService;

	@Autowired
	private SchemeController schemeController;

	@Autowired
	private PagedResourcesAssembler<ListResource<String>> listResourceAssembler;

	private Set<String> pendingIds = newHashSet();

	/**
	 * Gets a list of available data in the repository
	 * 
	 * @return A list of data identifiers in the repository
	 */
	@RequestMapping(value = "/api/v1/dataTables", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> dataTables(Principal principal, HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		return ResponseEntity
				.ok(DataTableResourceBuildersV1.dataTablesResourceBuilder().build(newArrayList(user.getDataTables())));
	}

	@RequestMapping(value = "/api/v1/dataTables", params = {
			"fields" }, method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> dataTables(Principal principal, @RequestParam("fields") Set<String> fields,
			HttpServletRequest request) {
		if (!fields.contains("schemes")) {
			return dataTables(principal, request);
		}

		User user = Utils.getUserOrThrowError(this.userService, principal);

		List<String> dataTableIds = user.getDataTables().stream().map(d -> d.getId()).collect(Collectors.toList());

		return ResponseEntity.ok(DataTableResourceBuildersV1.expandedDataTablesResourceBuilder()
				.build(this.dataTableService.getInjected(dataTableIds, user.getId())));
	}

	@RequestMapping(value = "/api/v1/dataTables/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> dataTable(@PathVariable("id") String id, Principal principal, HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		Utils.isAuthorisedForEntity(user, Sets.newHashSet(user.getDataTables()), id, "data table");

		Optional<DataTable> dataTable = this.dataTableService.get(id);

		if (!dataTable.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		return ResponseEntity.ok(DataTableResourceBuildersV1.dataTableResourceBuilder().build(dataTable.get()));
	}

	@RequestMapping(value = "/api/v1/dataTables/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> deleteDataTable(@PathVariable("id") String id, Principal principal,
			HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		Utils.isAuthorisedForEntity(user, Sets.newHashSet(user.getDataTables()), id, "data table");

		Optional<DataTable> dataTable = this.dataTableService.getInjected(id);

		if (!dataTable.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		boolean errors = false;

		for (Scheme scheme : dataTable.get().getSchemes()) {
			errors |= !this.schemeController.deleteScheme(scheme.getId(), principal, request).getStatusCode()
					.equals(HttpStatus.OK);
		}

		errors |= !this.dataTableService.remove(id);
		errors |= !dataTableAccessHandler().remove(dataTable.get());

		if (errors) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return ResponseEntity.ok().build();
	}

	/**
	 * Requests the upload of a data residing either in a file or in a database.
	 * This method checks the repository for the name. If it exists, an error is
	 * returned. Otherwise the uri to which the data can be posted is returned.
	 * 
	 * @param name the identifier of the data
	 * @param type the type of data, which is either a file or an sql database.
	 * @return a success response with the link to which the file can be posted
	 * or an error response with the error message.
	 */
	@RequestMapping(value = "/api/v1/dataTables", params = "type", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> requestUpload(Principal principal, @RequestParam("type") String type) {
		DataTable dataTable = this.dataTableService.newObject();

		List<Link> links = newArrayList();

		if (type.equals("file")) {
			Optional<Method> method = method(DataTableController.class, "uploadDataFromFile", String.class,
					Principal.class, MultipartFile.class, String.class, String.class);

			if (!method.isPresent()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}

			links.add(linkTo(method.get(), dataTable.getId(), null, null, null, null).withRel("link"));
		} else if (type.equals("sql")) {
			Optional<Method> method = method(DataTableController.class, "uploadDataFromSQLConnection", String.class,
					Principal.class, String.class, String.class, String.class, String.class, String.class, String.class,
					String.class);

			if (!method.isPresent()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}

			links.add(
					linkTo(method.get(), dataTable.getId(), null, null, null, null, null, null, null).withRel("link"));
		}

		this.pendingIds.add(dataTable.getId());

		return ResponseEntity.ok(new ResourcesV1.SimpleResource(links));
	}

	@RequestMapping(value = "/api/v1/dataTables/{id}/file", params = { "caption",
			"description" }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> uploadDataFromFile(@PathVariable("id") String id, Principal principal,
			@RequestParam("file") MultipartFile file, @RequestParam("caption") String caption,
			@RequestParam("description") String description) {

		if (id.isEmpty() || caption.isEmpty() || !this.pendingIds.contains(id)) {
			return ResponseEntity.badRequest().build();
		}

		if (this.dataTableService.contains(id)) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		User user = Utils.getUserOrThrowError(this.userService, principal);

		DataTable dataTable = this.dataTableService.newObject();

		dataTable.setId(id);
		dataTable.setCaption(caption);
		dataTable.setDescription(description);

		try {
			if (!this.dataTableService.add(dataTable)) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}

			if (!dataTableAccessHandler().save(new SaveDataFromFileParameter(dataTable, file), dataTable)) {
				this.dataTableService.remove(id);

				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
		} catch (Exception e) {
			this.dataTableService.remove(id);

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		this.pendingIds.remove(id);

		user.addDataTable(dataTable);

		if (!this.userService.updateUser(user).isPresent()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return dataTable(id, principal, null);
	}

	@RequestMapping(value = "/api/v1/dataTables/{id}/sql", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> uploadDataFromSQLConnection(@PathVariable("id") String id, Principal principal,
			@RequestParam("caption") String caption, @RequestParam("description") String description,
			@RequestParam("hostName") String hostName, @RequestParam("database") String database,
			@RequestParam("table") String table, @RequestParam("userName") String userName,
			@RequestParam("password") String password) {

		if (id.isEmpty() || caption.isEmpty() || hostName.isEmpty() || database.isEmpty() || table.isEmpty()
				|| userName.isEmpty() || password.isEmpty() || !this.pendingIds.contains(id)) {
			return ResponseEntity.badRequest().build();
		}

		if (this.dataTableService.contains(id)) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		User user = Utils.getUserOrThrowError(this.userService, principal);

		DataTable dataTable = this.dataTableService.newObject();

		dataTable.setId(id);
		dataTable.setCaption(caption);
		dataTable.setDescription(description);

		try {
			if (!this.dataTableService.add(dataTable)) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}

			if (!dataTableAccessHandler().save(
					new SaveDataFromSQLConnectionParameter(dataTable, hostName, database, table, userName, password),
					dataTable)) {
				this.dataTableService.remove(id);

				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
		} catch (Exception e) {
			this.dataTableService.remove(id);

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		this.pendingIds.remove(id);

		user.addDataTable(dataTable);

		if (!this.userService.updateUser(user).isPresent()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return dataTable(id, principal, null);
	}

	@RequestMapping(value = "/api/v1/dataTables/{id}/rows", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> rows(@PathVariable("id") String id, Principal principal, Pageable pageable,
			HttpServletRequest request) {
		User user = Utils.getUserOrThrowError(this.userService, principal);

		Utils.isAuthorisedForEntity(user, Sets.newHashSet(user.getDataTables()), id, "data table");

		Optional<DataTable> dataTable = this.dataTableService.get(id);

		if (!dataTable.isPresent()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		Optional<List<String>> rows = dataTableAccessHandler().rows(dataTable.get(), pageable.getPageSize(),
				pageable.getPageNumber());

		if (!rows.isPresent()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		Optional<Integer> rowCount = dataTableAccessHandler().rowCount(dataTable.get());

		if (!rowCount.isPresent()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return ResponseEntity.ok(genericPagedResourceBuilder(this.listResourceAssembler).buildPaged(pageable,
				newArrayList(new ResourcesV1.ListResource<>(rows.get())), rowCount.get()));
	}

}
