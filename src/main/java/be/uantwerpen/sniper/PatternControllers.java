/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper;

import static be.uantwerpen.sniper.core.MeasureComputerDaemons.newMeasureComputerDaemon;

import java.util.Observable;
import java.util.Observer;
import java.util.Optional;

import com.google.common.collect.Lists;

import be.uantwerpen.sniper.core.MeasureComputerDaemon;
import be.uantwerpen.sniper.model.BrickModel;
import be.uantwerpen.sniper.model.BrickModels;
import be.uantwerpen.sniper.model.MeasureModels.ObservableMeasureModel;
import be.uantwerpen.sniper.model.WorksheetPropertiesModel;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.episodes.Episode;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRules;
import de.unibonn.realkd.patterns.episodes.Episodes;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternControllers {

	public static PatternController newPatternController(Workspace realKDWorkspace, ObservableMeasureModel measureModel,
			WorksheetPropertiesModel worksheetPropertiesModel) {
		return new DefaultPatternController(realKDWorkspace, measureModel, worksheetPropertiesModel);
	}

	private static class DefaultPatternController implements PatternController, Observer {

		private BrickModel brickModel;

		private ObservableMeasureModel measureModel;

		private WorksheetPropertiesModel worksheetPropertiesModel;

		private MeasureComputerDaemon measureComputerDaemon;

		private Thread measureComputerDaemonThread;

		private DefaultPatternController(Workspace realKDWorkspace, ObservableMeasureModel measureModel,
				WorksheetPropertiesModel worksheetPropertiesModel) {
			this.brickModel = BrickModels.newEmptyBrickModel(realKDWorkspace);
			this.measureModel = measureModel;
			this.worksheetPropertiesModel = worksheetPropertiesModel;

			this.measureComputerDaemon = newMeasureComputerDaemon(this.brickModel, this.measureModel);

			this.measureComputerDaemonThread = new Thread(this.measureComputerDaemon);
			this.measureComputerDaemonThread.start();

			this.worksheetPropertiesModel.addObserver(this);
		}

		@Override
		public BrickModel brickModel() {
			return this.brickModel;
		}

		@Override
		public MeasureComputerDaemon measureComputerDaemon() {
			return this.measureComputerDaemon;
		};

		@Override
		public void update(Observable o, Object arg) {
			if (o.equals(this.worksheetPropertiesModel)) {
				updatePatternsWithProperties();
			}
		}

		private void updatePatternsWithProperties() {
			this.measureComputerDaemon.pause();

			int i = 0;

			Optional<Parameter<?>> parameter = this.worksheetPropertiesModel.property("Window size");
			double windowSize = (Double) parameter.get().current();

			for (Pattern<?> pattern : this.brickModel.patterns()) {
				if (Episode.class.isAssignableFrom(pattern.getClass())) {
					EpisodeDescriptor descriptor = ((Episode) pattern).descriptor();
					if (windowSize != descriptor.windowSize()) {
						Episode newPattern = Episodes.create(EpisodeDescriptors
								.create(descriptor.propositionalContext(), windowSize, descriptor.graph()),
								Lists.newArrayList(), Lists.newArrayList());

						this.brickModel.remove(i);
						this.brickModel.add(i, newPattern);
					}
				} else if (EpisodeRule.class.isAssignableFrom(pattern.getClass())) {
					EpisodeRuleDescriptor descriptor = ((EpisodeRule) pattern).descriptor();
					if (windowSize != descriptor.windowSize()) {
						EpisodeRule newPattern = EpisodeRules.create(
								EpisodeRuleDescriptors.create(descriptor.propositionalContext(), windowSize,
										descriptor.antecedent(), descriptor.consequent()),
								Lists.newArrayList(), Lists.newArrayList());

						this.brickModel.remove(i);
						this.brickModel.add(i, newPattern);
					}
				}

				i++;
			}

			this.measureComputerDaemon.restart();
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternControllers() {
		throw new AssertionError();
	}

}
