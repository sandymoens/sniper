/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.patterns.attributes;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class AttributeLists {

	public static AttributeList attributeList(Population population, AttributeListDescriptor descriptor,
			List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> measurementProcedures) {
		List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> procedures = measurementProcedures
				.stream().filter(p -> p.isApplicable(descriptor)).collect(Collectors.toList());

		List<Measurement> measurements = procedures.stream().map(p -> p.perform(descriptor))
				.collect(Collectors.toList());

		return new DefaultAttributeList(population, descriptor, measurements);
	}

	private static class DefaultAttributeList extends DefaultPattern<AttributeListDescriptor> implements AttributeList {

		public DefaultAttributeList(Population population, AttributeListDescriptor descriptor,
				List<Measurement> measurements) {
			super(population, descriptor, measurements);
		}

		@Override
		public AttributeList add(Measurement measurement) {
			if (this.hasMeasure(measurement.measure())) {
				return this;
			}

			List<Measurement> newMeasurements = Lists.newArrayList(measurements());
			newMeasurements.add(measurement);

			return new DefaultAttributeList(this.population(), this.descriptor(),
					ImmutableList.copyOf(newMeasurements));
		}

	}

	// Suppress default constructor for non-instantiability
	private AttributeLists() {
		throw new AssertionError();
	}

}
