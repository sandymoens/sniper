/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.patterns.attributes;

import java.util.List;

import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class AttributeListDescriptors {

	public static AttributeListDescriptor attributeListDescriptor(DataTable dataTable, List<Attribute<?>> attributes) {
		return new DefaultAttributeListDescriptor(dataTable, attributes);
	}

	private static class DefaultAttributeListDescriptor implements AttributeListDescriptor {

		private DataTable dataTable;
		private List<Attribute<?>> attributes;

		public DefaultAttributeListDescriptor(DataTable dataTable, List<Attribute<?>> attributes) {
			this.dataTable = dataTable;
			this.attributes = attributes;
		}

		@Override
		public SerialForm<? extends PatternDescriptor> serialForm() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public DataTable table() {
			return this.dataTable;
		}

		@Override
		public List<Attribute<?>> getReferencedAttributes() {
			return this.attributes;
		}

		@Override
		public List<Attribute<?>> attributes() {
			return this.attributes;
		}

	}

	// Suppress default constructor for non-instantiability
	private AttributeListDescriptors() {
		throw new AssertionError();
	}

}
