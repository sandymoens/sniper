/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import static be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptors.attributeListDescriptor;
import static be.uantwerpen.sniper.patterns.attributes.AttributeLists.attributeList;
import static com.google.common.collect.Lists.newArrayList;
import static de.unibonn.realkd.common.base.Identifier.id;
import static java.lang.Integer.parseInt;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.model.FilterBrickModel;
import be.uantwerpen.sniper.patterns.attributes.AttributeList;
import be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptor;
import be.uantwerpen.sniper.rest.resource.FilterPatternActionUploadResourcesV1;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContext;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.association.Associations;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptors;
import de.unibonn.realkd.patterns.episodes.Episodes;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;
import de.unibonn.realkd.patterns.graphs.Nodes;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.sequence.DefaultSequenceDescriptor;
import de.unibonn.realkd.patterns.sequence.Sequences;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class FilterPatternActions {

	@ActionBuilderHandler(resourceClass = FilterPatternActionUploadResourcesV1.ClearFilterPatternUploadActionResource.class)
	public static Action newClearFilterPatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			FilterPatternActionUploadResourcesV1.ClearFilterPatternUploadActionResource resource) {
		return new ClearFilterPatternAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = FilterPatternActionUploadResourcesV1.CreateNewFilterPatternUploadActionResource.class)
	public static Action newCreateFilterPatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			FilterPatternActionUploadResourcesV1.CreateNewFilterPatternUploadActionResource resource) {
		return new CreateFilterPatternAction(identifier, activeWorksheet, resource);
	}

	private static class ClearFilterPatternAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private FilterPatternActionUploadResourcesV1.ClearFilterPatternUploadActionResource resource;

		public ClearFilterPatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				FilterPatternActionUploadResourcesV1.ClearFilterPatternUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			PropositionalContext context = this.activeWorksheet.propositionalContext();

			if (context == null) {
				return Optional.empty();
			}

			this.activeWorksheet.filterBrickModel().clear();

			return Optional.of("OK");
		}

	}

	private static class CreateFilterPatternAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private FilterPatternActionUploadResourcesV1.CreateNewFilterPatternUploadActionResource resource;

		public CreateFilterPatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				FilterPatternActionUploadResourcesV1.CreateNewFilterPatternUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			PropositionalContext context = this.activeWorksheet.propositionalContext();

			if (context == null) {
				return Optional.empty();
			}

			if (this.resource.elementType.equals("proposition")) {
				List<Integer> intIds = this.resource.ids.stream().map(id -> parseInt(id)).collect(Collectors.toList());

				if (SequentialPropositionalContext.class
						.isAssignableFrom(this.activeWorksheet.propositionalContext().getClass())) {
					return newSequencePatternWithPropositions(this.activeWorksheet, intIds);
				} else if (SingleSequencePropositionalContext.class
						.isAssignableFrom(this.activeWorksheet.propositionalContext().getClass())) {
					return newEpisodePatternWithPropositions(this.activeWorksheet, intIds);
				} else {
					return newPatternsWithPropositions(this.activeWorksheet, intIds);
				}
			} else if (this.resource.elementType.equals("attribute")) {
				return newPatternsWithAttributes(this.activeWorksheet, this.resource.ids);
			}

			return Optional.of("OK");
		}

		private Optional<Object> newSequencePatternWithPropositions(ActiveWorksheet sessionWorkspace,
				List<Integer> intIds) {
			PropositionalContext context = sessionWorkspace.propositionalContext();
			if (context == null) {
				return Optional.empty();
			}

			SequentialPropositionalContext sContext = (SequentialPropositionalContext) context;
			FilterBrickModel filterBrickModel = sessionWorkspace.filterBrickModel();

			try {
				// TODO this should be handled in the addToDescriptor
				intIds.remove(new Integer(-1));

				List<Proposition> propositions = intIds.stream().map(id -> context.proposition(id))
						.collect(Collectors.toList());

				filterBrickModel.set(
						Sequences.create(DefaultSequenceDescriptor.create(sContext, ImmutableList.of(propositions)),
								ImmutableList.of()));
			} catch (IllegalArgumentException e) {
				return Optional.empty();
			}

			return Optional.of("OK");
		}

		private Optional<Object> newEpisodePatternWithPropositions(ActiveWorksheet activeWorksheet,
				List<Integer> intIds) {
			PropositionalContext context = activeWorksheet.propositionalContext();
			if (context == null) {
				return Optional.empty();
			}

			SingleSequencePropositionalContext sContext = (SingleSequencePropositionalContext) context;
			FilterBrickModel filterBrickModel = activeWorksheet.filterBrickModel();

			double windowSize = (Double) activeWorksheet.property("Window size").get().current();

			try {
				// TODO this should be handled in the addToDescriptor
				intIds.remove(new Integer(-1));

				List<Proposition> propositions = intIds.stream().map(id -> context.proposition(id))
						.collect(Collectors.toList());

				List<Node> nodes = Lists.newArrayList();

				int i = 0;

				for (Proposition proposition : propositions) {
					nodes.add(Nodes.create(i++, proposition));
				}

				GraphDescriptor graphDescriptor = GraphDescriptors.create(nodes, ImmutableList.of());
				EpisodeDescriptor episodeDescriptor = EpisodeDescriptors.create(sContext, windowSize, graphDescriptor);

				filterBrickModel.set(Episodes.create(episodeDescriptor, ImmutableList.of(), ImmutableList.of()));
			} catch (IllegalArgumentException e) {
				return Optional.empty();
			}

			return Optional.of("OK");
		}

		private Optional<Object> newPatternsWithPropositions(ActiveWorksheet sessionWorkspace, List<Integer> intIds) {
			PropositionalContext context = sessionWorkspace.propositionalContext();
			if (context == null) {
				return Optional.empty();
			}

			FilterBrickModel filterBrickModel = sessionWorkspace.filterBrickModel();

			try {
				intIds.remove(new Integer(-1));

				List<Proposition> propositions = intIds.stream().map(id -> context.proposition(id))
						.collect(Collectors.toList());

				filterBrickModel.set(Associations.association(
						LogicalDescriptors.create(context.population(), propositions), ImmutableList.of()));
			} catch (IllegalArgumentException e) {
				return Optional.empty();
			}

			return Optional.of("OK");
		}

		private Optional<Object> newPatternsWithAttributes(ActiveWorksheet sessionWorkspace, List<String> ids) {
			PropositionalContext context = sessionWorkspace.propositionalContext();
			if (context == null || !TableBasedPropositionalContext.class.isAssignableFrom(context.getClass())
					|| ids.isEmpty()) {
				return Optional.empty();
			}

			if (!(context instanceof TableBasedPropositionalContext)) {
				throw new IllegalArgumentException(
						"Expected TableBasedPropositionalLogic but got " + context.getClass().getSimpleName());
			}

			DataTable dataTable = ((TableBasedPropositionalContext) context).getDatatable();

			FilterBrickModel filterBrickModel = sessionWorkspace.filterBrickModel();

			try {
				// LogicalDescriptor descriptor =
				// LogicalDescriptors.create(context.population(),
				// newArrayList());
				// List<Attribute<?>> targets = ids.stream().map(id ->
				// dataTable.attribute(identifier(id)).get())
				// .collect(toList());
				//
				// ExceptionalModelPattern pattern = emmPattern(
				// subgroup(descriptor, dataTable, targets,
				// ContingencyTableModelFactory.INSTANCE),
				// TOTAL_VARIATION_DISTANCE, ImmutableList.of());
				//
				// filterBrickModel.set(pattern);

				List<Attribute<?>> attributes = ids.stream().map(id -> dataTable.attribute(id(id)).get())
						.collect(toList());

				AttributeListDescriptor descriptor = attributeListDescriptor(dataTable, attributes);

				AttributeList pattern = attributeList(dataTable.population(), descriptor, newArrayList());

				filterBrickModel.set(pattern);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				return Optional.empty();
			} catch (UnsupportedOperationException e) {
				e.printStackTrace();
				return Optional.empty();
			}

			return Optional.of("OK");
		}

	}

	// Suppress default constructor for non-instantiability
	private FilterPatternActions() {
		throw new AssertionError();
	}

}
