/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import java.util.Optional;

import de.unibonn.realkd.common.base.Identifier;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class StatefulActions {

	public static StatefulAction wrappedStatefulAction(Action action) {
		return new WrappedStatefulAction(action);
	}

	private static class WrappedStatefulAction implements StatefulAction {

		private Action action;
		private State state;

		private WrappedStatefulAction(Action action) {
			this.action = action;
			this.state = State.CREATED;
		}

		@Override
		public Optional<Object> execute() {
			this.state = State.RUNNING;

			try {
				Optional<Object> result = this.action.execute();

				this.state = State.FINISHED;

				return result;
			} catch (Exception e) {
				this.state = State.FAILED;

				e.printStackTrace();
			}

			return Optional.empty();
		}

		@Override
		public Identifier identifier() {
			return this.action.identifier();
		}

		@Override
		public State state() {
			return this.state;
		}

	}

	// Suppress default constructor for non-instantiability
	private StatefulActions() {
		throw new AssertionError();
	}

}
