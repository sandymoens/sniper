/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import java.util.Map;
import java.util.Optional;

import de.unibonn.realkd.common.base.Identifier;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public interface ActionManager {

	public Identifier newIdentifier();

	public void addAction(Action action);

	public Optional<Action> getAction(Identifier identifier);

	public boolean updateAction(Identifier identifier, Map<String, Object> parameters);

	public boolean runAction(Identifier identifier);

	public boolean cancelAction(Identifier identifier);

	public boolean deleteAction(Identifier identifier);

	public boolean isRunningAction();

}
