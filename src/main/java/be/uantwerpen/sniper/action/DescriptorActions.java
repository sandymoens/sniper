/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.model.BrickModel;
import be.uantwerpen.sniper.model.bricks.Brick;
import be.uantwerpen.sniper.rest.resource.DescriptorActionUploadResourcesV1;
import be.uantwerpen.sniper.utils.ModelFactoryRegister;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.models.ModelFactory;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class DescriptorActions {

	@ActionBuilderHandler(resourceClass = DescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource.class)
	public static Action newAddToDescriptorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			DescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource resource) {
		return new AddToDescriptorAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = DescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource.class)
	public static Action newRemoveFromDescriptorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			DescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource resource) {
		return new RemoveFromDescriptorAction(identifier, activeWorksheet, resource);
	}

	public static interface Event {
	}

	public static class PropositionBasedNodeEvent implements Event {

		public Proposition proposition;

		public PropositionBasedNodeEvent(Proposition proposition) {
			this.proposition = proposition;
		}

	}

	public static class IdBasedNodeEvent implements Event {

		public int id;

		public IdBasedNodeEvent(int id) {
			this.id = id;
		}

	}

	public static class EdgeEvent implements Event {

		public int start;
		public int end;

		public EdgeEvent(int start, int end) {
			this.start = start;
			this.end = end;
		}

	}

	public static Object getElement(PropositionalContext context, String id, String type) {
		if (type.equals("attribute")) {
			return ((TableBasedPropositionalContext) context).getDatatable().attribute(Identifier.id(id)).get();
		} else if (type.equals("proposition")) {
			if (SingleSequencePropositionalContext.class.isAssignableFrom(context.getClass())) {
				return new PropositionBasedNodeEvent(
						id.equals("-1") ? new EmptyProposition(context) : context.proposition(Integer.valueOf(id)));
			}
			return id.equals("-1") ? new EmptyProposition(context) : context.proposition(Integer.valueOf(id));
		} else if (type.equals("modelType")) {
			for (ModelFactory<?> modelFactory : ModelFactoryRegister.MODEL_FACTORIES) {
				if (modelFactory.getClass().getSimpleName().equals(id)) {
					return modelFactory;
				}
			}
		} else if (type.equals("event")) {
			return new IdBasedNodeEvent(Integer.valueOf(id));
		} else if (type.equals("node")) {
			return new PropositionBasedNodeEvent(context.proposition(Integer.valueOf(id)));
		} else if (type.equals("eventName")) {
//			return new Event(-1, id);
		} else if (type.equals("edge")) {
			String split[] = id.split("_");
			return new EdgeEvent(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
		}
		return null;
	}

	private static class AddToDescriptorAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private DescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource resource;

		public AddToDescriptorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				DescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			BrickModel brickModel = this.activeWorksheet.brickModel();
			PropositionalContext context = this.activeWorksheet.propositionalContext();

			List<Object> elements = this.resource.ids.stream()
					.map(id -> getElement(context, id, this.resource.elementType)).collect(toList());

			List<Brick<Pattern<?>, ?>> bricks;

			try {
				if (this.resource.individual) {
					bricks = brickModel.addToBrickAsSingletons(this.resource.descriptorId, elements);
				} else {
					bricks = brickModel.addToBrickAsCollection(this.resource.descriptorId, elements);
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				return Optional.empty();
			}

			if (bricks == null) {
				return Optional.empty();
			}

			return Optional.of("OK");
		}

	}

	private static class RemoveFromDescriptorAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private DescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource resource;

		public RemoveFromDescriptorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				DescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			BrickModel brickModel = this.activeWorksheet.brickModel();
			PropositionalContext context = this.activeWorksheet.propositionalContext();

			try {
				brickModel.removeFromBrick(this.resource.descriptorId,
						getElement(context, this.resource.id, this.resource.elementType));
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				return Optional.empty();
			} catch (Exception e) {
				e.printStackTrace();
				return Optional.empty();
			}

			return Optional.of("OK");
		}

	}

	// Suppress default constructor for non-instantiability
	private DescriptorActions() {
		throw new AssertionError();
	}

}
