/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import java.util.Optional;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.rest.resource.CommandActionUploadResourcesV1;
import be.uantwerpen.sniper.rest.resource.CommandActionUploadResourcesV1.UndoRedoCommandUploadAction;
import de.unibonn.realkd.common.base.Identifier;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class CommandActions {

	@ActionBuilderHandler(resourceClass = CommandActionUploadResourcesV1.UndoRedoCommandUploadAction.class)
	public static Action undoRedoAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			CommandActionUploadResourcesV1.UndoRedoCommandUploadAction resource) {
		return new UndoRedoAction(identifier, activeWorksheet, resource);
	}

	private static class UndoRedoAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private UndoRedoCommandUploadAction resource;

		public UndoRedoAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				CommandActionUploadResourcesV1.UndoRedoCommandUploadAction resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			if ("redo".equals(this.resource.value)) {
				if (!this.activeWorksheet.undoStack().canRedo()) {
					return Optional.empty();
				}

				this.activeWorksheet.undoStack().redo();

				return Optional.of("Ok");
			} else if ("undo".equals(this.resource.value)) {
				if (!this.activeWorksheet.undoStack().canUndo()) {
					return Optional.empty();
				}

				this.activeWorksheet.undoStack().undo();

				return Optional.of("Ok");
			}

			return Optional.empty();
		}

	}

	// Suppress default constructor for non-instantiability
	private CommandActions() {
		throw new AssertionError();
	}

}
