/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static de.unibonn.creedo.webapp.utils.ParameterToJsonConverter.convertRecursively;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.algorithm.Miner;
import be.uantwerpen.sniper.algorithm.Miners;
import be.uantwerpen.sniper.algorithm.PatternAwareMiner;
import be.uantwerpen.sniper.algorithm.PatternCollectionPostProcessor;
import be.uantwerpen.sniper.algorithm.PatternCollectionPostProcessors;
import be.uantwerpen.sniper.algorithm.PatternPostProcessor;
import be.uantwerpen.sniper.algorithm.PatternPostProcessors;
import be.uantwerpen.sniper.rest.resource.AlgorithmUploadResourcesV1;
import de.unibonn.creedo.common.parameters.AbstractTransferableParameterState;
import de.unibonn.realkd.algorithms.StoppableMiningAlgorithm;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class AlgorithmActions {

	@ActionBuilderHandler(resourceClass = AlgorithmUploadResourcesV1.MinerUploadAction.class)
	public static Action minerAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			AlgorithmUploadResourcesV1.MinerUploadAction resource) {
		return new MinerAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = AlgorithmUploadResourcesV1.PatternCollectionPostProcessorUploadAction.class)
	public static Action patternCollectionPostProcessorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			AlgorithmUploadResourcesV1.PatternCollectionPostProcessorUploadAction resource) {
		return new PatternCollectionPostProcessorAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = AlgorithmUploadResourcesV1.PatternPostProcessorUploadAction.class)
	public static Action patternPostProcessorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			AlgorithmUploadResourcesV1.PatternPostProcessorUploadAction resource) {
		return new PatternPostProcessorAction(identifier, activeWorksheet, resource);
	}

	private static class MinerAction implements InterruptableAction, ParameterizedAction, StatefulAction {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private AlgorithmUploadResourcesV1.MinerUploadAction resource;

		private State state;

		private Map<String, Object> parameters;

		private StoppableMiningAlgorithm algorithm;

		public MinerAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				AlgorithmUploadResourcesV1.MinerUploadAction resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
			this.parameters = ImmutableMap.of();
			this.state = State.CREATED;
		}

		private static Optional<StoppableMiningAlgorithm> createAlgorithm(ActiveWorksheet activeWorksheet,
				String algorithmString, List<Integer> ixs, Map<String, Object> parameters) {
			Optional<Miner> miner = Miners.newMiner(activeWorksheet.propositionalContext(), algorithmString);

			if (!miner.isPresent()) {
				return Optional.empty();
			}

			try {
				StoppableMiningAlgorithm algorithm;

				if (PatternAwareMiner.class.isAssignableFrom(miner.get().getClass())) {
					algorithm = ((PatternAwareMiner) miner.get()).newMiner(activeWorksheet.realKDWorkspace(),
							activeWorksheet.brickModel().patterns(), ixs);
				} else {
					algorithm = miner.get().newMiner(activeWorksheet.realKDWorkspace());
				}

				for (Entry<String, Object> entry : parameters.entrySet()) {
					if (entry.getValue() != null) {
						algorithm.findParameterByName(entry.getKey()).setByString(entry.getValue().toString());
					}
				}

				// if (algorithm.isStateValid()) {
				return Optional.of(algorithm);
				// }
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}

			return Optional.empty();
		}

		@Override
		public boolean parameters(Map<String, Object> parameters) {
			Optional<StoppableMiningAlgorithm> algorithm = createAlgorithm(this.activeWorksheet, this.resource.id,
					this.resource.ixs, parameters);

			if (algorithm.isPresent()) {
				Map<String, Object> newParameters = newHashMap();

				for (Parameter<?> parameter : algorithm.get().getAllParameters()) {
					newParameters.put(parameter.getName(), parameter.current());
				}

				this.parameters = newParameters;

				return true;
			}

			return false;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			this.state = State.RUNNING;

			Optional<StoppableMiningAlgorithm> algorithm = createAlgorithm(this.activeWorksheet, this.resource.id,
					this.resource.ixs, this.parameters);

			if (!algorithm.isPresent()) {
				this.state = State.FAILED;
				return Optional.empty();
			}

			this.algorithm = algorithm.get();

			try {
				Collection<? extends Pattern<?>> patterns = this.algorithm.call();

				if (!patterns.isEmpty()) {
					this.activeWorksheet.brickModel().addAll(this.activeWorksheet.brickModel().size(),
							ImmutableList.copyOf(patterns));
				}
			} catch (ValidationException e) {
				e.printStackTrace();

				this.state = State.FAILED;
				return Optional.empty();
			}

			this.state = State.FINISHED;
			return Optional.of("Ok");
		}

		@Override
		public void cancel() {
			this.algorithm.requestStop();
			this.state = State.CANCELLED;
		}

		@Override
		public State state() {
			return this.state;
		}

		@Override
		public List<AbstractTransferableParameterState> parameters() {
			Optional<StoppableMiningAlgorithm> algorithm = createAlgorithm(this.activeWorksheet, this.resource.id,
					this.resource.ixs, this.parameters);

			if (algorithm.isPresent()) {
				return convertRecursively(algorithm.get().getAllParameters());
			}

			return newArrayList();
		}

	}

	private static class PatternCollectionPostProcessorAction
			implements InterruptableAction, ParameterizedAction, StatefulAction {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private AlgorithmUploadResourcesV1.PatternCollectionPostProcessorUploadAction resource;

		private State state;

		private Map<String, Object> parameters;

		private StoppableMiningAlgorithm algorithm;

		public PatternCollectionPostProcessorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				AlgorithmUploadResourcesV1.PatternCollectionPostProcessorUploadAction resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
			this.parameters = ImmutableMap.of();
			this.state = State.CREATED;
		}

		private static Optional<StoppableMiningAlgorithm> createAlgorithm(ActiveWorksheet activeWorksheet,
				String algorithmString, Map<String, Object> parameters) {
			Optional<PatternCollectionPostProcessor> patternCollectionPostProcessor = PatternCollectionPostProcessors
					.newPostProcessor(algorithmString);

			if (!patternCollectionPostProcessor.isPresent()) {
				return Optional.empty();
			}

			try {
				StoppableMiningAlgorithm algorithm = patternCollectionPostProcessor.get()
						.newPostProcessor(activeWorksheet, activeWorksheet.brickModel().patterns());

				for (Entry<String, Object> entry : parameters.entrySet()) {
					if (entry.getValue() != null) {
						algorithm.findParameterByName(entry.getKey()).setByString(entry.getValue().toString());
					}
				}

				// if (algorithm.isStateValid()) {
				return Optional.of(algorithm);
				// }
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}

			return Optional.empty();
		}

		@Override
		public boolean parameters(Map<String, Object> parameters) {
			Optional<StoppableMiningAlgorithm> algorithm = createAlgorithm(this.activeWorksheet, this.resource.id,
					parameters);

			if (algorithm.isPresent()) {
				Map<String, Object> newParameters = newHashMap();

				for (Parameter<?> parameter : algorithm.get().getAllParameters()) {
					newParameters.put(parameter.getName(), parameter.current());
				}

				this.parameters = newParameters;

				return true;
			}

			return false;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			this.state = State.RUNNING;

			Optional<StoppableMiningAlgorithm> algorithm = createAlgorithm(this.activeWorksheet, this.resource.id,
					this.parameters);

			if (!algorithm.isPresent()) {
				this.state = State.FAILED;
				return Optional.empty();
			}

			this.algorithm = algorithm.get();

			try {
				Collection<? extends Pattern<?>> patterns = this.algorithm.call();

				this.activeWorksheet.brickModel().clear();
				this.activeWorksheet.brickModel().addAll(0, ImmutableList.copyOf(patterns));
			} catch (ValidationException e) {
				e.printStackTrace();

				this.state = State.FAILED;
				return Optional.empty();
			}

			this.state = State.FINISHED;
			return Optional.of("Ok");
		}

		@Override
		public void cancel() {
			this.algorithm.requestStop();
			this.state = State.CANCELLED;
		}

		@Override
		public State state() {
			return this.state;
		}

		@Override
		public List<AbstractTransferableParameterState> parameters() {
			Optional<StoppableMiningAlgorithm> algorithm = createAlgorithm(this.activeWorksheet, this.resource.id,
					this.parameters);

			if (algorithm.isPresent()) {
				return convertRecursively(algorithm.get().getAllParameters());
			}

			return newArrayList();
		}

	}

	private static class PatternPostProcessorAction
			implements InterruptableAction, ParameterizedAction, StatefulAction {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private AlgorithmUploadResourcesV1.PatternPostProcessorUploadAction resource;

		private State state;

		private Map<String, Object> parameters;

		private StoppableMiningAlgorithm algorithm;

		public PatternPostProcessorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				AlgorithmUploadResourcesV1.PatternPostProcessorUploadAction resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
			this.parameters = ImmutableMap.of();
			this.state = State.CREATED;
		}

		private static Optional<StoppableMiningAlgorithm> createAlgorithm(ActiveWorksheet activeWorksheet,
				String algorithmString, List<Integer> ixs, Map<String, Object> parameters) {
			Optional<PatternPostProcessor> patternPostProcessor = PatternPostProcessors
					.newPostProcessor(algorithmString);

			if (!patternPostProcessor.isPresent()) {
				return Optional.empty();
			}

			try {
				StoppableMiningAlgorithm algorithm = patternPostProcessor.get().newPostProcessor(
						activeWorksheet.realKDWorkspace(), activeWorksheet.brickModel().patterns(), ixs);

				for (Entry<String, Object> entry : parameters.entrySet()) {
					if (entry.getValue() != null) {
						algorithm.findParameterByName(entry.getKey()).setByString(entry.getValue().toString());
					}
				}

				// if (algorithm.isStateValid()) {
				return Optional.of(algorithm);
				// }
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}

			return Optional.empty();
		}

		@Override
		public boolean parameters(Map<String, Object> parameters) {
			Optional<StoppableMiningAlgorithm> algorithm = createAlgorithm(this.activeWorksheet, this.resource.id,
					this.resource.ixs, parameters);

			if (algorithm.isPresent()) {
				Map<String, Object> newParameters = newHashMap();

				for (Parameter<?> parameter : algorithm.get().getAllParameters()) {
					newParameters.put(parameter.getName(), parameter.current());
				}

				this.parameters = newParameters;

				return true;
			}

			return false;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			this.state = State.RUNNING;

			Optional<StoppableMiningAlgorithm> algorithm = createAlgorithm(this.activeWorksheet, this.resource.id,
					this.resource.ixs, this.parameters);

			if (!algorithm.isPresent()) {
				this.state = State.FAILED;
				return Optional.empty();
			}

			this.algorithm = algorithm.get();

			try {
				Collection<? extends Pattern<?>> patterns = this.algorithm.call();

				this.activeWorksheet.brickModel().clear();
				this.activeWorksheet.brickModel().addAll(0, ImmutableList.copyOf(patterns));
			} catch (ValidationException e) {
				e.printStackTrace();

				this.state = State.FAILED;
				return Optional.empty();
			}

			this.state = State.FINISHED;
			return Optional.of("Ok");
		}

		@Override
		public void cancel() {
			this.algorithm.requestStop();
			this.state = State.CANCELLED;
		}

		@Override
		public State state() {
			return this.state;
		}

		@Override
		public List<AbstractTransferableParameterState> parameters() {
			Optional<StoppableMiningAlgorithm> algorithm = createAlgorithm(this.activeWorksheet, this.resource.id,
					this.resource.ixs, this.parameters);

			if (algorithm.isPresent()) {
				return convertRecursively(algorithm.get().getAllParameters());
			}

			return newArrayList();
		}

	}

	// Suppress default constructor for non-instantiability
	private AlgorithmActions() {
		throw new AssertionError();
	}

}
