/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import static be.uantwerpen.sniper.command.ActionBasedUndoCommands.actionBasedUndoCommand;
import static com.google.common.collect.Maps.newHashMap;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.common.Utils;
import be.uantwerpen.sniper.model.bricks.Brick;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class ActionManagers {

	public static ActionManager newActionManager(ActiveWorksheet activeWorksheet) {
		return new DefaultActionManager(activeWorksheet);
	}

	private static class DefaultActionManager implements ActionManager {

		private ActiveWorksheet activeWorksheet;

		private Map<Identifier, Action> actions;

		private boolean isRunningAction;

		private DefaultActionManager(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
			this.actions = newHashMap();
		}

		@Override
		public Identifier newIdentifier() {
			Identifier identifier = Identifier.identifier(UUID.randomUUID().toString());

			while (this.actions.containsKey(identifier)) {
				identifier = Identifier.identifier(UUID.randomUUID().toString());
			}

			return identifier;
		}

		@Override
		public void addAction(Action action) {
			this.actions.put(action.identifier(), action);
		}

		@Override
		public Optional<Action> getAction(Identifier identifier) {
			Action action = this.actions.get(identifier);

			if (action == null) {
				return Optional.empty();
			}

			return Optional.of(action);
		}

		@Override
		public boolean updateAction(Identifier identifier, Map<String, Object> parameters) {
			Action action = this.actions.get(identifier);

			if (ParameterizedAction.class.isAssignableFrom(action.getClass())) {
				return ((ParameterizedAction) action).parameters(parameters);
			}

			return false;
		}

		@Override
		public boolean runAction(Identifier identifier) {
			if (this.isRunningAction) {
				return false;
			}

			Action action = this.actions.get(identifier);

			if (action == null) {
				return false;
			}

			if (StatefulAction.class.isAssignableFrom(action.getClass())) {
				Utils.threadPool.execute(new Runnable() {

					@Override
					public void run() {
						List<Brick<Pattern<?>, ?>> previousBricks = DefaultActionManager.this.activeWorksheet
								.brickModel().bricks();

						DefaultActionManager.this.isRunningAction = true;

						try {
							if (action.execute().isPresent()) {
								List<Brick<Pattern<?>, ?>> newBricks = DefaultActionManager.this.activeWorksheet
										.brickModel().bricks();

								DefaultActionManager.this.activeWorksheet.undoStack().push(actionBasedUndoCommand(
										DefaultActionManager.this.activeWorksheet, action, previousBricks, newBricks));
							}
						} catch (OutOfMemoryError e) {
							e.printStackTrace();
							System.gc();
						}

						DefaultActionManager.this.isRunningAction = false;
					}

				});
			} else {
				List<Brick<Pattern<?>, ?>> previousBricks = this.activeWorksheet.brickModel().bricks();

				this.isRunningAction = true;

				try {
					if (action.execute().isPresent()) {
						List<Brick<Pattern<?>, ?>> newBricks = this.activeWorksheet.brickModel().bricks();

						this.activeWorksheet.undoStack()
								.push(actionBasedUndoCommand(this.activeWorksheet, action, previousBricks, newBricks));
					}
				} catch (OutOfMemoryError e) {
					e.printStackTrace();
					System.gc();
				}

				this.isRunningAction = false;
			}

			return true;
		}

		@Override
		public boolean cancelAction(Identifier identifier) {
			Action action = this.actions.get(identifier);

			if (action == null) {
				return false;
			}

			if (InterruptableAction.class.isAssignableFrom(action.getClass())) {
				((InterruptableAction) action).cancel();

				return true;
			}

			return false;
		}

		@Override
		public boolean deleteAction(Identifier identifier) {
			Action action = this.actions.get(identifier);

			if (action == null) {
				return false;
			}

			this.actions.remove(identifier);

			return true;

		}

		@Override
		public boolean isRunningAction() {
			return this.isRunningAction;
		}

	}

}
