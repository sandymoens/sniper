/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import static be.uantwerpen.sniper.action.DescriptorActions.getElement;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.model.FilterBrickModel;
import be.uantwerpen.sniper.rest.resource.FilterDescriptorActionUploadResourcesV1;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.data.propositions.PropositionalContext;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class FilterDescriptorActions {

	@ActionBuilderHandler(resourceClass = FilterDescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource.class)
	public static Action newAddToFilterDescriptorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			FilterDescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource resource) {
		return new AddToDescriptorAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = FilterDescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource.class)
	public static Action newRemoveFromFilterDescriptorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			FilterDescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource resource) {
		return new RemoveFromDescriptorAction(identifier, activeWorksheet, resource);
	}

	private static class AddToDescriptorAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private FilterDescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource resource;

		public AddToDescriptorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				FilterDescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			FilterBrickModel filterBrickModel = this.activeWorksheet.filterBrickModel();
			PropositionalContext context = this.activeWorksheet.propositionalContext();

			List<Object> elements = this.resource.ids.stream()
					.map(id -> getElement(context, id, this.resource.elementType)).collect(toList());

			if (!filterBrickModel.addToBrick(this.resource.descriptorId, elements)) {
				return Optional.empty();
			}

			return Optional.of("OK");
		}

	}

	private static class RemoveFromDescriptorAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private FilterDescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource resource;

		public RemoveFromDescriptorAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				FilterDescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			FilterBrickModel filterBrickModel = this.activeWorksheet.filterBrickModel();
			PropositionalContext context = this.activeWorksheet.propositionalContext();

			if (!filterBrickModel.removeFromBrick(this.resource.descriptorId,
					getElement(context, this.resource.id, this.resource.elementType))) {
				return Optional.empty();
			}

			return Optional.of("OK");
		}

	}

	// Suppress default constructor for non-instantiability
	private FilterDescriptorActions() {
		throw new AssertionError();
	}

}
