/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import static be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptors.attributeListDescriptor;
import static be.uantwerpen.sniper.patterns.attributes.AttributeLists.attributeList;
import static be.uantwerpen.sniper.utils.MeasureRegister.measureRegister;
import static com.google.common.collect.Lists.newArrayList;
import static de.unibonn.realkd.common.base.Identifier.id;
import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.SortOrder;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.model.BrickModel;
import be.uantwerpen.sniper.patterns.attributes.AttributeList;
import be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptor;
import be.uantwerpen.sniper.rest.resource.PatternActionUploadResourcesV1;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContext;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Associations;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptors;
import de.unibonn.realkd.patterns.episodes.Episodes;
import de.unibonn.realkd.patterns.graphs.Edge;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;
import de.unibonn.realkd.patterns.graphs.Nodes;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.sequence.DefaultSequenceDescriptor;
import de.unibonn.realkd.patterns.sequence.Sequences;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternActions {

	@ActionBuilderHandler(resourceClass = PatternActionUploadResourcesV1.ClearPatternsUploadActionResource.class)
	public static Action newClearPatternsAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			PatternActionUploadResourcesV1.ClearPatternsUploadActionResource resource) {
		return new ClearPatternsAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = PatternActionUploadResourcesV1.DeletePatternsUploadActionResource.class)
	public static Action newDeletePatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			PatternActionUploadResourcesV1.DeletePatternsUploadActionResource resource) {
		return new DeletePatternsAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = PatternActionUploadResourcesV1.CreateNewPatternUploadActionResource.class)
	public static Action newCreatePatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			PatternActionUploadResourcesV1.CreateNewPatternUploadActionResource resource) {
		return new CreatePatternAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = PatternActionUploadResourcesV1.SortPatternsUploadActionResource.class)
	public static Action newSortPatternsAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			PatternActionUploadResourcesV1.SortPatternsUploadActionResource resource) {
		return new SortPatternsAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = PatternActionUploadResourcesV1.MovePatternUploadActionResource.class)
	public static Action newMovePatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			PatternActionUploadResourcesV1.MovePatternUploadActionResource resource) {
		return new MovePatternAction(identifier, activeWorksheet, resource);
	}

	@ActionBuilderHandler(resourceClass = PatternActionUploadResourcesV1.AddPatternUploadActionResource.class)
	public static Action addPatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
			PatternActionUploadResourcesV1.AddPatternUploadActionResource resource) {
		return new AddPatternAction(identifier, activeWorksheet, resource);
	}

	private static class ClearPatternsAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		public ClearPatternsAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				PatternActionUploadResourcesV1.ClearPatternsUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			this.activeWorksheet.brickModel().clear();

			// this.activeWorksheet.realKDWorkspace().overwrite(new
			// NamedPatternCollection("PatternCollection",
			// "PatternCollection", "Collection of patterns",
			// this.activeWorksheet.brickModel().patterns()));

			return Optional.of("OK");
		}

	}

	private static class DeletePatternsAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private PatternActionUploadResourcesV1.DeletePatternsUploadActionResource resource;

		public DeletePatternsAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				PatternActionUploadResourcesV1.DeletePatternsUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			try {
				List<Integer> ixs = newArrayList(this.resource.ixs);

				Collections.sort(ixs);
				Collections.reverse(ixs);

				ixs.forEach(ix -> this.activeWorksheet.brickModel().remove(ix));
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
				return Optional.empty();
			}

			// this.activeWorksheet.realKDWorkspace().overwrite(new
			// NamedPatternCollection("PatternCollection",
			// "PatternCollection", "Collection of patterns",
			// this.activeWorksheet.brickModel().patterns()));

			return Optional.of("OK");
		}

	}

	private static class CreatePatternAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private PatternActionUploadResourcesV1.CreateNewPatternUploadActionResource resource;

		public CreatePatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				PatternActionUploadResourcesV1.CreateNewPatternUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			PropositionalContext context = this.activeWorksheet.propositionalContext();

			if (context == null) {
				return Optional.empty();
			}

			List<List<String>> patternsElementIds = extractPatternsElementIds(this.resource.ids,
					this.resource.individual);

			if (this.resource.elementType.equals("proposition")) {
				if (SequentialPropositionalContext.class
						.isAssignableFrom(this.activeWorksheet.propositionalContext().getClass())) {
					return newSequencePattern(this.activeWorksheet, toInts(patternsElementIds),
							this.resource.ix != -1 ? this.resource.ix : this.activeWorksheet.brickModel().size());
				} else if (SingleSequencePropositionalContext.class
						.isAssignableFrom(this.activeWorksheet.propositionalContext().getClass())) {
					return newEpisodePattern(this.activeWorksheet, toInts(patternsElementIds),
							this.resource.ix != -1 ? this.resource.ix : this.activeWorksheet.brickModel().size());
				} else {
					return newAssociation(this.activeWorksheet, toInts(patternsElementIds),
							this.resource.ix != -1 ? this.resource.ix : this.activeWorksheet.brickModel().size());
				}
			} else if (this.resource.elementType.equals("attribute")) {
				return newExceptionalModel(this.activeWorksheet, patternsElementIds,
						this.resource.ix != -1 ? this.resource.ix : this.activeWorksheet.brickModel().size());
			} else if (this.resource.elementType.equals("node")) {
				return newEpisodePattern(this.activeWorksheet, toInts(patternsElementIds),
						this.resource.ix != -1 ? this.resource.ix : this.activeWorksheet.brickModel().size());
			}

			return Optional.of("OK");
		}

		private List<List<String>> extractPatternsElementIds(List<String> ids, boolean individual) {
			if (individual) {
				return ids.stream().map(id -> ImmutableList.of(id)).collect(toList());
			}
			return ImmutableList.of(ids);
		}

		private List<List<Integer>> toInts(List<List<String>> strings) {
			return strings.stream().map(list -> list.stream().map(s -> Integer.parseInt(s)).collect(toList()))
					.collect(toList());
		}

		private Optional<Object> newAssociation(ActiveWorksheet sessionWorkspace,
				List<List<Integer>> patternsElementIds, int ix) {
			PropositionalContext context = sessionWorkspace.propositionalContext();
			if (context == null) {
				return Optional.empty();
			}

			int fails = 0;

			for (List<Integer> ids : patternsElementIds) {
				try {
					List<Proposition> elements = ids.stream().filter(id -> id != -1).map(id -> context.proposition(id))
							.collect(toList());

					sessionWorkspace.brickModel().add(ix++, Associations.association(
							LogicalDescriptors.create(context.population(), elements), ImmutableList.of()));
				} catch (Exception e) {
					e.printStackTrace();
					fails++;
				}
			}

			if (fails == patternsElementIds.size()) {
				return Optional.empty();
			}

			// sessionWorkspace.realKDWorkspace().overwrite(new
			// NamedPatternCollection("PatternCollection",
			// "PatternCollection", "Collection of patterns",
			// sessionWorkspace.brickModel().patterns()));

			return Optional.of("OK");
		}

		private Optional<Object> newExceptionalModel(ActiveWorksheet sessionWorkspace,
				List<List<String>> patternsElementIds, int ix) {
			PropositionalContext context = sessionWorkspace.propositionalContext();
			if (context == null || !TableBasedPropositionalContext.class.isAssignableFrom(context.getClass())
					|| patternsElementIds.isEmpty()) {
				return Optional.empty();
			}

			if (!(context instanceof TableBasedPropositionalContext)) {
				throw new IllegalArgumentException(
						"Expected TableBasedPropositionalLogic but got " + context.getClass().getSimpleName());
			}

			DataTable dataTable = ((TableBasedPropositionalContext) context).getDatatable();

			int fails = 0;

			for (List<String> ids : patternsElementIds) {
				try {
					List<Attribute<?>> attributes = ids.stream().map(id -> dataTable.attribute(id(id)).get())
							.collect(toList());

					AttributeListDescriptor descriptor = attributeListDescriptor(dataTable, attributes);

					AttributeList pattern = attributeList(dataTable.population(), descriptor, newArrayList());

					sessionWorkspace.brickModel().add(ix++, pattern);
				} catch (Exception e) {
					e.printStackTrace();
					fails++;
				}
			}

			if (fails == patternsElementIds.size()) {
				return Optional.empty();
			}

			// sessionWorkspace.realKDWorkspace().overwrite(new
			// NamedPatternCollection("PatternCollection",
			// "PatternCollection", "Collection of patterns",
			// sessionWorkspace.brickModel().patterns()));

			return Optional.of("OK");
		}

		public Optional<Object> newSequencePattern(ActiveWorksheet sessionWorkspace,
				List<List<Integer>> patternsElementIds, int ix) {
			PropositionalContext context = sessionWorkspace.propositionalContext();
			if (context == null) {
				return Optional.empty();
			}

			SequentialPropositionalContext sContext = (SequentialPropositionalContext) context;

			int fails = 0;

			for (List<Integer> ids : patternsElementIds) {
				try {
					List<Proposition> elements = ids.stream().filter(id -> id != -1).map(id -> context.proposition(id))
							.collect(toList());

					sessionWorkspace.brickModel().add(ix++,
							Sequences.create(DefaultSequenceDescriptor.create(sContext, ImmutableList.of(elements)),
									ImmutableList.of()));
				} catch (Exception e) {
					e.printStackTrace();
					fails++;
				}
			}

			if (fails == patternsElementIds.size()) {
				return Optional.empty();
			}

			// sessionWorkspace.realKDWorkspace().overwrite(new
			// NamedPatternCollection("PatternCollection",
			// "PatternCollection", "Collection of patterns",
			// sessionWorkspace.brickModel().patterns()));

			return Optional.of("OK");
		}

		public Optional<Object> newEpisodePattern(ActiveWorksheet sessionWorkspace,
				List<List<Integer>> patternsElementIds, int ix) {
			SingleSequencePropositionalContext context = (SingleSequencePropositionalContext) sessionWorkspace
					.propositionalContext();
			if (context == null) {
				return Optional.empty();
			}
			BrickModel brickModel = sessionWorkspace.brickModel();

			int fails = 0;

			Optional<Parameter<?>> property = sessionWorkspace.propertiesModel().property("Window size");

			double windowSize = property.isPresent() ? (Double) property.get().current() : 10;

			for (List<Integer> ids : patternsElementIds) {
				try {

					List<Node> nodesFrom = IntStream.range(0, ids.size())
							.mapToObj(i -> Nodes.create(i, context.proposition(ids.get(i)))).collect(toList());
					List<Edge> edgesFrom = newArrayList();

					EpisodeDescriptor descriptor = EpisodeDescriptors.create(context, windowSize,
							GraphDescriptors.create(nodesFrom, edgesFrom));

					brickModel.add(ix++, Episodes.create(descriptor, ImmutableList.of(), ImmutableList.of()));
				} catch (Exception e) {
					e.printStackTrace();
					fails++;
				}
			}

			if (fails == patternsElementIds.size()) {
				return Optional.empty();
			}

			// sessionWorkspace.realKDWorkspace().overwrite(new
			// NamedPatternCollection("PatternCollection",
			// "PatternCollection", "Collection of patterns",
			// sessionWorkspace.brickModel().patterns()));

			return Optional.of("OK");
		}

	}

	private static class SortPatternsAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private PatternActionUploadResourcesV1.SortPatternsUploadActionResource resource;

		public SortPatternsAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				PatternActionUploadResourcesV1.SortPatternsUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			if (this.resource.sortOrder.equals(SortOrder.UNSORTED)) {
				return Optional.empty();
			}

			Measure measure = measureRegister().getMeasure(this.resource.measure);

			List<Pair<Optional<Measurement>, Pattern<?>>> patterns = newArrayList();

			for (Pattern<?> pattern : this.activeWorksheet.brickModel().patterns()) {
				patterns.add(Pair.of(pattern.measurement(measure), pattern));
			}

			Collections.sort(patterns, new Comparator<Pair<Optional<Measurement>, Pattern<?>>>() {

				@Override
				public int compare(Pair<Optional<Measurement>, Pattern<?>> o1,
						Pair<Optional<Measurement>, Pattern<?>> o2) {
					if (o1.getKey().isPresent() && o2.getKey().isPresent() && !(Double.isNaN(o1.getKey().get().value()))
							&& !(Double.isNaN(o2.getKey().get().value()))) {
						int compare = Double.compare(o1.getKey().get().value(), o2.getKey().get().value());

						if (compare != 0) {
							return compare;
						}
					} else if (o1.getKey().isPresent() && !(Double.isNaN(o1.getKey().get().value()))) {
						return 1;
					} else if (o2.getKey().isPresent() && !(Double.isNaN(o2.getKey().get().value()))) {
						return -1;
					}

					return o1.getValue().toString().compareTo(o2.getValue().toString());
				}

			});

			if (this.resource.sortOrder.equals(SortOrder.DESCENDING)) {
				Collections.reverse(patterns);
			}

			this.activeWorksheet.brickModel().clear();
			this.activeWorksheet.brickModel().addAll(0, patterns.stream().map(p -> p.getValue()).collect(toList()));

			// this.activeWorksheet.realKDWorkspace().overwrite(new
			// NamedPatternCollection("PatternCollection",
			// "PatternCollection", "Collection of patterns",

			return Optional.of("OK");
		}
	}

	private static class MovePatternAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private PatternActionUploadResourcesV1.MovePatternUploadActionResource resource;

		public MovePatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				PatternActionUploadResourcesV1.MovePatternUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			try {
				this.activeWorksheet.brickModel().move(this.resource.fromIx, this.resource.toIx);
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
				return Optional.empty();
			}

			// this.activeWorksheet.realKDWorkspace().overwrite(new
			// NamedPatternCollection("PatternCollection",
			// "PatternCollection", "Collection of patterns",
			// this.activeWorksheet.brickModel().patterns()));

			return Optional.of("OK");
		}

	}

	private static class AddPatternAction implements Action {

		private Identifier identifier;

		private ActiveWorksheet activeWorksheet;

		private PatternActionUploadResourcesV1.AddPatternUploadActionResource resource;

		public AddPatternAction(Identifier identifier, ActiveWorksheet activeWorksheet,
				PatternActionUploadResourcesV1.AddPatternUploadActionResource resource) {
			this.identifier = identifier;
			this.activeWorksheet = activeWorksheet;
			this.resource = resource;
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public Optional<Object> execute() {
			try {
				List<Optional<? extends Object>> patterns = this.resource.patternSerialForms.stream().map(p -> {
					if (p == null) {
						return Optional.empty();
					}

					try {
						return p.build(this.activeWorksheet);
					} catch (ValidationException e) {
						e.printStackTrace();
					}
					return Optional.empty();
				}).collect(Collectors.toList());

				if (!patterns.stream().allMatch(p -> p.isPresent())) {
					return Optional.empty();
				}

				this.activeWorksheet.brickModel().addAll(this.activeWorksheet.brickModel().size(),
						patterns.stream().map(p -> p.get()).collect(Collectors.toList()));
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
				return Optional.empty();
			}

			return Optional.of("OK");
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternActions() {
		throw new AssertionError();
	}

}
