/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.action;

import static com.google.common.collect.Lists.newArrayList;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.rest.resource.ActionUploadResourceV1;
import de.unibonn.realkd.common.base.Identifier;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class ActionBuilders {

	private static List<Pair<Class<? extends ActionUploadResourceV1>, Method>> handlers;

	public static void register(Class<? extends ActionUploadResourceV1> clazz, Method method) {
		handlers.add(Pair.of(clazz, method));
	}

	static {
		handlers = newArrayList();

		Reflections reflections = new Reflections(
				new ConfigurationBuilder().addUrls(ClasspathHelper.forPackage("be.uantwerpen.sniper.action"))
						.setScanners(new MethodAnnotationsScanner()));

		reflections.getMethodsAnnotatedWith(ActionBuilderHandler.class).stream()
				.forEach(m -> register(m.getAnnotation(ActionBuilderHandler.class).resourceClass(), m));
	}

	private static ActionBuilder defaultActionBuilder;

	public static ActionBuilder actionBuilder() {
		return defaultActionBuilder != null ? defaultActionBuilder
				: (defaultActionBuilder = new DefaultActionBuilder());
	}

	private static class DefaultActionBuilder implements ActionBuilder {

		@Override
		public Action build(ActiveWorksheet activeWorksheet, Identifier identifier, ActionUploadResourceV1 resource) {
			Class<? extends ActionUploadResourceV1> clazz = resource.getClass();

			for (Pair<Class<? extends ActionUploadResourceV1>, Method> handler : handlers) {
				if (handler.getKey().isAssignableFrom(clazz)) {
					try {
						return (Action) handler.getValue().invoke(null, identifier, activeWorksheet, resource);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}

			return null;
		}

	}

	// Suppress default constructor for non-instantiability
	private ActionBuilders() {
		throw new AssertionError();
	}

}
