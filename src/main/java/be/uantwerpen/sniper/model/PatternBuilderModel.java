package be.uantwerpen.sniper.model;
/// *
// * GNU AFFERO GENERAL PUBLIC LICENSE
// *
// * Version 3, 19 November 2007
// *
// * Copyright (C) 2015 University of Antwerp
// *
// * This program is free software: you can redistribute it and/or modify
// * it under the terms of the GNU Affero General Public License as
// * published by the Free Software Foundation, either version 3 of the
// * License, or (at your option) any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// * GNU Affero General Public License for more details.
// *
// * You should have received a copy of the GNU Affero General Public License
// * along with this program. If not, see <http://www.gnu.org/licenses/>.
// */
// package be.uantwerpen.mime.model;
//
// import static be.uantwerpen.mime.model.DescriptorNode.newDescriptorNode;
// import static com.google.common.collect.Lists.newArrayList;
// import static com.google.common.collect.Maps.newHashMap;
// import static com.google.common.collect.Sets.newHashSet;
// import static
/// de.unibonn.realkd.patterns.util.DefaultPropositionList.defaultPropositionListBuilder;
//
// import java.util.Collection;
// import java.util.List;
// import java.util.Map;
// import java.util.Set;
//
// import com.google.common.collect.ImmutableList;
//
// import be.uantwerpen.mime.MIMESessionWorkspace;
// import de.unibonn.realkd.patterns.Pattern;
// import de.unibonn.realkd.patterns.PatternBuilder;
// import de.unibonn.realkd.patterns.PatternDescriptor;
// import de.unibonn.realkd.patterns.PatternDescriptorBuilder;
// import de.unibonn.realkd.patterns.association.AssociationBuilder;
// import de.unibonn.realkd.patterns.emm.ExceptionalModelPatternBuilder;
// import de.unibonn.realkd.patterns.logical.LogicalDescriptorBuilder;
// import de.unibonn.realkd.patterns.rules.AssociationRuleBuilder;
// import de.unibonn.realkd.patterns.rules.RuleDescriptorBuilder;
// import de.unibonn.realkd.patterns.sequence.SequenceBuilder;
// import de.unibonn.realkd.patterns.sequence.SequenceDescriptorBuilder;
// import de.unibonn.realkd.patterns.subgroups.SubgroupBuilder;
// import de.unibonn.realkd.patterns.util.DefaultPropositionList;
// import de.unibonn.realkd.patterns.util.PropositionListBuilder;
//
/// **
// * This class implements a view on a mutable list of patterns.
// *
// * @author Sandy Moens
// * @since 0.0.2
// * @version 0.0.2
// */
// public class PatternBuilderModel {
//
// public static class PatternToDecriptorNode {
// private PatternBuilder patternBuilder;
// private DescriptorNode descriptorNode;
//
// public PatternToDecriptorNode(PatternBuilder patternBuilder, DescriptorNode
/// descriptorNode) {
// this.patternBuilder = patternBuilder;
// this.descriptorNode = descriptorNode;
// }
//
// public PatternBuilder getPatternBuilder() {
// return this.patternBuilder;
// }
//
// public DescriptorNode getDescriptorNode() {
// return this.descriptorNode;
// }
// }
//
// public static class PatternAndDescriptorBuilders {
// public PatternBuilder patternBuilder;
// public PatternDescriptorBuilder patternDescriptorBuilder;
//
// public PatternAndDescriptorBuilders(PatternBuilder patternBuilder,
// PatternDescriptorBuilder patternDescriptorBuilder) {
// this.patternBuilder = patternBuilder;
// this.patternDescriptorBuilder = patternDescriptorBuilder;
// }
// }
//
// public static PatternBuilderModel newEmptyModel() {
// return new PatternBuilderModel();
// }
//
// private List<PatternToDecriptorNode> patternBuilders;
//
// private Map<Integer, DescriptorNode> idToDescriptorNode;
//
// private PatternBuilderModel() {
// this.patternBuilders = newArrayList();
// this.idToDescriptorNode = newHashMap();
// }
//
// /**
// * Removes all patterns from the model.
// */
// public void clear() {
// this.patternBuilders.clear();
// this.idToDescriptorNode.clear();
// }
//
// private DescriptorNode newNodeAndRegister(PatternBuilder patternBuilder,
/// PatternDescriptorBuilder localBuilder,
// PatternDescriptor descriptor) {
// DescriptorNode node = newDescriptorNode(patternBuilder, localBuilder);
// node.setDescriptor(descriptor);
// this.idToDescriptorNode.put(node.getId(), node);
// return node;
// }
//
// private DescriptorNode newNodeAndRegister(PatternBuilder patternBuilder,
/// PatternDescriptorBuilder localBuilder,
// List<DescriptorNode> children, PatternDescriptor descriptor) {
// DescriptorNode node =
/// DescriptorNode.newDescriptorNodeWithChildren(patternBuilder, localBuilder,
/// children);
// node.setDescriptor(descriptor);
// this.idToDescriptorNode.put(node.getId(), node);
// return node;
// }
//
// /**
// * Removes the pattern at the given index.
// *
// * @param ix
// * the index of the pattern to remove
// */
// public void removePattern(int ix) {
// this.patternBuilders.remove(ix);
// }
//
// private static IllegalArgumentException
/// getIllegalArgumentException(DescriptorNode node, String type) {
// return new IllegalArgumentException("Can not add an object of type '" + type
/// + "' to an object of type '"
// + node.getLocalBuilder().getClass().getSimpleName().replace("Implementation",
/// "") + "'!");
// }
//
// private static IllegalArgumentException
/// getIllegalArgumentException(PatternDescriptorBuilder builder, String type) {
// return new IllegalArgumentException("Can not add an object of type '" + type
/// + "' to an object of type '"
// + builder.getClass().getSimpleName().replace("Implementation", "") + "'!");
// }
//
// /**
// * Add an object of a given type and identified with a specific id to an
// * existing descriptor with given id.
// *
// * @param descriptorId
// * the id of the descriptor to which an object is added
// *
// * @param type
// * the type of object that is added
// *
// * @param ids
// * the ids of the objects to be added
// */
// public void addToDescriptor(int descriptorId, String type,
/// Collection<Integer> ids) {
// ids.remove(new Integer(-1));
//
// DescriptorNode node = this.idToDescriptorNode.get(descriptorId);
// try {
// addToDescriptorAndTest(node.getPatternBuilder(), node.getLocalBuilder(),
/// type, ids);
// replace(getIndexOfDescriptor(descriptorId), node.getPatternBuilder());
// } catch (IllegalArgumentException e) {
// node.setDescriptor(null);
// throw e;
// }
// node.setDescriptor(null);
// }
//
// public void addToDescriptorAndTest(PatternBuilder patternBuilder,
/// PatternDescriptorBuilder descriptorBuilder,
// String type, Collection<Integer> ids) throws IllegalArgumentException {
// List<Integer> elementIndexList = null;
// boolean allowDuplicates = false;
// if
/// (LogicalDescriptorBuilder.class.isAssignableFrom(descriptorBuilder.getClass()))
/// {
// if (!type.equals("proposition")) {
// throw getIllegalArgumentException(descriptorBuilder, type);
// }
// elementIndexList = ((LogicalDescriptorBuilder)
/// descriptorBuilder).getElementIndexList();
// } else if
/// (SubgroupBuilder.class.isAssignableFrom(descriptorBuilder.getClass())) {
// if (!type.equals("attribute")) {
// throw getIllegalArgumentException(descriptorBuilder, type);
// }
// elementIndexList = ((SubgroupBuilder)
/// descriptorBuilder).getAttributeIndices();
// } else if
/// (SequenceDescriptorBuilder.class.isAssignableFrom(descriptorBuilder.getClass()))
/// {
// if (!type.equals("proposition")) {
// throw getIllegalArgumentException(descriptorBuilder, type);
// }
// SequenceDescriptorBuilder builder = ((SequenceDescriptorBuilder)
/// descriptorBuilder);
// if (builder.orderedSetsBuilders().get(builder.orderedSetsBuilders().size() -
/// 1).elementList().isEmpty()) {
// elementIndexList =
/// builder.orderedSetsBuilders().get(builder.orderedSetsBuilders().size() - 1)
// .elementList();
// } else {
// PropositionListBuilder pBuilder = defaultPropositionListBuilder();
// builder.orderedSetsBuilders().add(pBuilder);
// elementIndexList = pBuilder.elementList();
// }
// allowDuplicates = true;
// } else if
/// (PropositionListBuilder.class.isAssignableFrom(descriptorBuilder.getClass()))
/// {
// if (!type.equals("proposition")) {
// throw getIllegalArgumentException(descriptorBuilder, type);
// }
// elementIndexList = ((PropositionListBuilder)
/// descriptorBuilder).elementList();
// allowDuplicates = true;
// }
//
// if (allowDuplicates) {
// // attempt addition
// elementIndexList.addAll(ids);
// try {
// if (SequenceBuilder.class.isAssignableFrom(patternBuilder.getClass())) {
// SequenceBuilder builder = (SequenceBuilder) patternBuilder;
// List<PropositionListBuilder> orderedSetsBuilders =
/// builder.getDescriptorBuilder()
// .orderedSetsBuilders();
// if (!orderedSetsBuilders.get(orderedSetsBuilders.size() -
/// 1).elementList().isEmpty()) {
// orderedSetsBuilders.add(DefaultPropositionList.defaultPropositionListBuilder());
// }
// }
// patternBuilder.build(MIMESessionWorkspace.WORKSPACE);
//
// } catch (IllegalArgumentException e) {
// elementIndexList.removeAll(ids);
// throw e;
// }
// return;
// }
// Set<Integer> alreadyPresent = newHashSet(ids);
// alreadyPresent.retainAll(elementIndexList);
//
// Set<Integer> toAdd = newHashSet(ids);
// toAdd.removeAll(elementIndexList);
//
// // attempt addition
// elementIndexList.addAll(toAdd);
// try {
// patternBuilder.build(MIMESessionWorkspace.WORKSPACE);
// } catch (IllegalArgumentException e) {
// elementIndexList.removeAll(toAdd);
// throw e;
// }
//
// if (!alreadyPresent.isEmpty()) {
// if (toAdd.isEmpty()) {
// throw new IllegalArgumentException(
// "Objects with ids '" + alreadyPresent + "' already present! Nothing done.");
// } else {
// throw new IllegalArgumentException(
// "Objects with ids '" + alreadyPresent + "' already present! Only added
/// remaining ones.");
// }
// }
// }
//
// /**
// * Removes an object from the descriptor with given id. This procedure
// * checks if the value is present, if so it removes the value.
// *
// * @param descriptorId
// * the id of the descriptor from which to remove the proposition
// * @param id
// * the id of the object to remove
// */
// public void removeFromDescriptor(int descriptorId, int id) {
// DescriptorNode node = this.idToDescriptorNode.get(descriptorId);
// List<Integer> elementIndexList = null;
// if
/// (LogicalDescriptorBuilder.class.isAssignableFrom(node.getLocalBuilder().getClass()))
/// {
// elementIndexList = ((LogicalDescriptorBuilder)
/// node.getLocalBuilder()).getElementIndexList();
// } else if
/// (SubgroupBuilder.class.isAssignableFrom(node.getLocalBuilder().getClass()))
/// {
// elementIndexList = ((SubgroupBuilder)
/// node.getLocalBuilder()).getAttributeIndices();
// } else if
/// (PropositionListBuilder.class.isAssignableFrom(node.getLocalBuilder().getClass()))
/// {
// elementIndexList = ((PropositionListBuilder)
/// node.getLocalBuilder()).elementList();
// }
//
// // attempt deletion
// if (!elementIndexList.remove(new Integer(id))) {
// throw new IllegalArgumentException("Object with id '" + id + "' was not
/// present!");
// }
// try {
// node.getPatternBuilder().build(MIMESessionWorkspace.WORKSPACE);
// node.setDescriptor(node.getLocalBuilder().build(MIMESessionWorkspace.WORKSPACE));
// } catch (IllegalArgumentException e) {
// // if builder fails, then we have to fall back
// elementIndexList.add(id);
// throw e;
// }
// }
//
// public List<PatternToDecriptorNode> getPatternBuilders() {
// return ImmutableList.copyOf(patternBuilders);
// }
//
// /**
// * Obtains the pattern at the given index.
// *
// * @param ix
// * the index of the pattern to retrieve
// * @return the pattern
// */
// public Pattern getPattern(int ix) {
// return
/// patternBuilders.get(ix).patternBuilder.build(MIMESessionWorkspace.WORKSPACE);
// }
//
// public PatternBuilder get(int ix) {
// return this.patternBuilders.get(ix).patternBuilder;
// }
//
// /**
// * Gets a pair of builders corresponding to the given descriptor id.
// *
// * @param descriptorId
// * id of the descriptor for which to obtain the builders
// * @return a pair of builder. The PatternBuilder contains a builder for the
// * global pattern. The PatternDescriptorBuilder contains a sub
// * pattern descriptor object that is part of the PatternBuilder and
// * that corresponds to the given descriptorId
// */
// public PatternAndDescriptorBuilders getNewBuildersForDescriptorId(int
/// descriptorId) {
// DescriptorNode node = idToDescriptorNode.get(descriptorId);
// PatternBuilder originalBuilder = node.getPatternBuilder();
// PatternBuilder builder =
/// originalBuilder.build(MIMESessionWorkspace.WORKSPACE).toBuilder();
//
// PatternDescriptorBuilder originalLocalBuilder = node.getLocalBuilder();
//
// return new PatternAndDescriptorBuilders(builder,
/// recursiveFind(originalBuilder.getDescriptorBuilder(),
// originalLocalBuilder, builder.getDescriptorBuilder()));
// }
//
// private PatternDescriptorBuilder recursiveFind(PatternDescriptorBuilder
/// source,
// PatternDescriptorBuilder originalLocalBuilder, PatternDescriptorBuilder dest)
/// {
// if (source == originalLocalBuilder) {
// return dest;
// }
// if (RuleDescriptorBuilder.class.isAssignableFrom(source.getClass())) {
// RuleDescriptorBuilder sourceBuilder = (RuleDescriptorBuilder) source;
// RuleDescriptorBuilder destBuilder = (RuleDescriptorBuilder) dest;
// PatternDescriptorBuilder b = null;
// b = recursiveFind(sourceBuilder.getAntecendentBuilder(),
/// originalLocalBuilder,
// destBuilder.getAntecendentBuilder());
// if (b != null) {
// return b;
// }
// b = recursiveFind(sourceBuilder.getConsequentBuilder(), originalLocalBuilder,
// destBuilder.getConsequentBuilder());
// if (b != null) {
// return b;
// }
// } else if (SubgroupBuilder.class.isAssignableFrom(source.getClass())) {
// SubgroupBuilder sourceBuilder = (SubgroupBuilder) source;
// SubgroupBuilder destBuilder = (SubgroupBuilder) dest;
// return recursiveFind(sourceBuilder.getExtensionBuilder(),
/// originalLocalBuilder,
// destBuilder.getExtensionBuilder());
// } else if
/// (SequenceDescriptorBuilder.class.isAssignableFrom(source.getClass())) {
// SequenceDescriptorBuilder sourceBuilder = (SequenceDescriptorBuilder) source;
// SequenceDescriptorBuilder destBuilder = (SequenceDescriptorBuilder) dest;
// PatternDescriptorBuilder b = null;
// for (int i = 0, iEnd = sourceBuilder.orderedSetsBuilders().size(); i < iEnd;
/// i++) {
// b = recursiveFind(sourceBuilder.orderedSetsBuilders().get(i),
/// originalLocalBuilder,
// destBuilder.orderedSetsBuilders().get(i));
// if (b != null) {
// return b;
// }
// }
// }
// return null;
// }
//
// public int insert(int ix, PatternBuilder builder) {
// PatternToDecriptorNode node = registerAndReturnListElement(builder);
// this.patternBuilders.add(ix, node);
// return node.descriptorNode.getId();
// }
//
// private int replace(int ix, PatternBuilder builder) {
// removePattern(ix);
// return insert(ix, builder);
// }
//
// /**
// * @param descriptorId
// * @return
// */
// public int getIndexOfDescriptor(int descriptorId) {
// DescriptorNode descriptorNode = idToDescriptorNode.get(descriptorId);
// PatternBuilder patternBuilder = descriptorNode.getPatternBuilder();
// int i = 0;
// for (PatternToDecriptorNode entry : patternBuilders) {
// if (entry.patternBuilder == patternBuilder) {
// return i;
// }
// i++;
// }
// return i;
// }
//
// /**
// * @param builder
// * @return
// */
// public int addPattern(PatternBuilder builder) {
// PatternToDecriptorNode node = registerAndReturnListElement(builder);
// this.patternBuilders.add(node);
// return node.descriptorNode.getId();
//
// }
//
// private PatternToDecriptorNode register(AssociationBuilder builder) {
// DescriptorNode node = newNodeAndRegister(builder,
/// builder.getDescriptorBuilder(), null);
//
// return new PatternToDecriptorNode(builder, node);
// }
//
// private PatternToDecriptorNode register(AssociationRuleBuilder builder) {
// List<DescriptorNode> children = newArrayList();
// children.add(newNodeAndRegister(builder,
/// builder.getDescriptorBuilder().getAntecendentBuilder(), null));
// children.add(newNodeAndRegister(builder,
/// builder.getDescriptorBuilder().getConsequentBuilder(), null));
//
// DescriptorNode node = newNodeAndRegister(builder,
/// builder.getDescriptorBuilder(), children, null);
//
// return new PatternToDecriptorNode(builder, node);
// }
//
// private PatternToDecriptorNode register(ExceptionalModelPatternBuilder
/// builder) {
// List<DescriptorNode> children = newArrayList();
// children.add(newNodeAndRegister(builder,
/// builder.getDescriptorBuilder().getExtensionBuilder(), null));
//
// DescriptorNode node = newNodeAndRegister(builder,
/// builder.getDescriptorBuilder(), children, null);
//
// return new PatternToDecriptorNode(builder, node);
// }
//
// private PatternToDecriptorNode register(SequenceBuilder builder) {
// List<DescriptorNode> children = newArrayList();
// builder.getDescriptorBuilder().orderedSetsBuilders().stream()
// .forEach(b -> children.add(newNodeAndRegister(builder, b, null)));
//
// DescriptorNode node = newNodeAndRegister(builder,
/// builder.getDescriptorBuilder(), children, null);
//
// return new PatternToDecriptorNode(builder, node);
// }
//
// /**
// * @param builder
// */
// private PatternToDecriptorNode registerAndReturnListElement(PatternBuilder
/// builder) {
// if (AssociationBuilder.class.isAssignableFrom(builder.getClass())) {
// return register((AssociationBuilder) builder);
// } else if (AssociationRuleBuilder.class.isAssignableFrom(builder.getClass()))
/// {
// return register((AssociationRuleBuilder) builder);
// } else if
/// (ExceptionalModelPatternBuilder.class.isAssignableFrom(builder.getClass()))
/// {
// return register((ExceptionalModelPatternBuilder) builder);
// } else if (SequenceBuilder.class.isAssignableFrom(builder.getClass())) {
// return register((SequenceBuilder) builder);
// }
// return null;
// }
//
// }
