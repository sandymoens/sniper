/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model;

import java.util.Collection;
import java.util.List;

import be.uantwerpen.sniper.model.bricks.Brick;
import de.unibonn.realkd.patterns.Pattern;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.1.0
 * @version 0.1.0
 */
public interface BrickModel {

	public void add(Object object);

	public Brick<Pattern<?>, ?> add(int ix, Object object);

	public List<Brick<Pattern<?>, ?>> addAll(int ix, Collection<Object> objects);

	public List<Brick<Pattern<?>, ?>> addToBrickAsCollection(String id, List<? extends Object> extensions);

	public List<Brick<Pattern<?>, ?>> addToBrickAsSingletons(String id, List<? extends Object> extensions);

	public List<Brick<Pattern<?>, ?>> replaceInBrick(String id, List<? extends Object> extensions);

	public void remove(int ix);

	public void removeById(Collection<Integer> idsToRemove);

	public Brick<Pattern<?>, ?> removeFromBrick(String id, Object extension);

	public Pattern<?> get(int ix);

	public Brick<?, ?> getBrickByIndex(int ix);

	public Brick<?, ?> getBrickById(String descriptorId);

	public Brick<?, ?> getTopBrick(String descriptorId);

	public boolean bricks(List<Brick<Pattern<?>, ?>> bricks);

	public List<Brick<Pattern<?>, ?>> bricks();

	public Pattern<?> pattern(int ix);

	public List<Pattern<?>> patterns();

	public int size();

	public void clear();

	public void sortBricks(List<Integer> sortOrder);

	public void move(int fromIx, int toIx);

}