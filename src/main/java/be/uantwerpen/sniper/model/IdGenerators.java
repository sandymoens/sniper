/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model;

import java.util.UUID;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class IdGenerators {

	public static IdGenerator newSingleIdGenerator() {
		return new SingleIdGenerator();
	}

	public static IdGenerator newSimpleIdGenerator() {
		return new SimpleIdGenerator();
	}

	public static IdGenerator newBrickModelBasedIdGenerator(BrickModel brickModel) {
		return new BrickModelBasedIdGenerator(brickModel);
	}

	private static class SingleIdGenerator implements IdGenerator {

		@Override
		public String newId() {
			return "id";
		}

	}

	private static class SimpleIdGenerator implements IdGenerator {

		private int i = 0;

		@Override
		public String newId() {
			return String.format("%d", this.i++);
		}

	}

	private static class BrickModelBasedIdGenerator implements IdGenerator {

		private BrickModel brickModel;

		public BrickModelBasedIdGenerator(BrickModel brickModel) {
			this.brickModel = brickModel;
		}

		@Override
		public String newId() {
			String id = UUID.randomUUID().toString();
			while (this.brickModel.getBrickById(id) != null) {
			}
			return id;
		}

	}

	// Suppress default constructor for non-instantiability
	private IdGenerators() {
		throw new AssertionError();
	}

}
