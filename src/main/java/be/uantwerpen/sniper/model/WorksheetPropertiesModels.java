/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model;

import static be.uantwerpen.sniper.common.Parameters.windowSizeParameter;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.unibonn.realkd.common.KdonTypeName;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.HasSerialForm;
import de.unibonn.realkd.common.workspace.IdentifiableSerialForm;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class WorksheetPropertiesModels {

	public static WorksheetPropertiesModel newWorksheetPropertiesModel(Class<? extends PropositionalContext> clazz) {
		List<Parameter<?>> properties = newArrayList();

		if (SingleSequencePropositionalContext.class.isAssignableFrom(clazz)) {
			properties.add(windowSizeParameter());
		}

		return new DefaultWorksheetPropertiesModel(Identifier.id("WorksheetProperties"), properties);
	}

	private static class DefaultWorksheetPropertiesModel extends WorksheetPropertiesModel
			implements HasSerialForm<DefaultWorksheetPropertiesModel> {

		@KdonTypeName("worksheetPropertiesModel")
		private static class DefaultWorksheetPropertiesModelSerialForm
				implements IdentifiableSerialForm<DefaultWorksheetPropertiesModel> {

			@JsonProperty("identifier")
			private Identifier identifier;

			@JsonProperty("properties")
			private Map<Identifier, Object> properties;

			public DefaultWorksheetPropertiesModelSerialForm(@JsonProperty("identifier") Identifier identifier,
					@JsonProperty("properties") Map<Identifier, Object> properties) {
				this.identifier = identifier;
				this.properties = properties;
			}

			@Override
			public Identifier identifier() {
				return this.identifier;
			}

			@Override
			public DefaultWorksheetPropertiesModel build(Workspace workspace) {
				List<Parameter<?>> parameters = newArrayList();

				for (Entry<Identifier, Object> entry : this.properties.entrySet()) {
					if (entry.getKey().equals(Identifier.id("WindowSize"))) {
						Parameter<Double> parameter = windowSizeParameter();
						parameter.set((Double) entry.getValue());
						parameters.add(parameter);
					}
				}

				return new DefaultWorksheetPropertiesModel(this.identifier, parameters);
			}

		}

		private Identifier identifier;
		private HashMap<String, Parameter<?>> properties;

		private DefaultWorksheetPropertiesModel(Identifier identifier, Collection<Parameter<?>> properties) {
			this.identifier = identifier;
			this.properties = Maps.newHashMap();

			properties.forEach(p -> this.properties.put(p.getName(), p));
		}

		@Override
		public String caption() {
			return "Worksheet properties model";
		}

		@Override
		public String description() {
			return "Model containing all worksheet properties";
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public List<Parameter<?>> properties() {
			return Lists.newArrayList(this.properties.values());
		}

		@Override
		public Optional<Parameter<?>> property(String key) {
			return Optional.ofNullable(this.properties.get(key));
		}

		@Override
		public boolean update(Map<String, String> properties) {
			boolean ok = true;

			for (Entry<String, String> entry : properties.entrySet()) {
				Parameter<?> parameter = this.properties.get(entry.getKey());

				if (parameter == null) {
					continue;
				}

				try {
					parameter.setByString(entry.getValue());
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					ok = false;
				}
			}

			this.setChanged();
			this.notifyObservers();

			return ok;
		}

		@Override
		public SerialForm<? extends DefaultWorksheetPropertiesModel> serialForm() {
			Map<Identifier, Object> valuesMap = newHashMap(this.properties.values().stream().map(p -> (Parameter<?>) p)
					.collect(Collectors.toMap(p -> p.id(), p -> p.current())));
			return new DefaultWorksheetPropertiesModelSerialForm(this.identifier, valuesMap);
		}

	}

	// Suppress default constructor for non-instantiability
	private WorksheetPropertiesModels() {
		throw new AssertionError();
	}

}
