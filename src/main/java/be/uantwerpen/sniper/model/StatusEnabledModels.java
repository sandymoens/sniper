/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import be.uantwerpen.sniper.visualization.Visualization;
import be.uantwerpen.sniper.visualization.pattern.PatternVisualizations;
import de.unibonn.realkd.common.KdonTypeName;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.workspace.Entity;
import de.unibonn.realkd.common.workspace.HasSerialForm;
import de.unibonn.realkd.common.workspace.IdentifiableSerialForm;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class StatusEnabledModels {

	public static StatusEnabledModel<Visualization<Pattern<?>, ?>> newVisualizationModel() {
		return new StatusEnabledVisualizationModelWithSerialForm();
	}

	protected static abstract class StatusEnabledModel<T> extends be.uantwerpen.sniper.model.StatusEnabledModel<T> {

		private Identifier identifier;
		private String caption;
		private String description;

		private Set<T> enabled;

		protected StatusEnabledModel(Identifier identifier, String caption, String description) {
			this.identifier = identifier;
			this.caption = caption;
			this.description = description;

			this.enabled = newHashSet();
		}

		@Override
		public Identifier identifier() {
			return this.identifier;
		}

		@Override
		public String caption() {
			return this.caption;
		}

		@Override
		public String description() {
			return this.description;
		}

		@Override
		public boolean status(T key) {
			return this.enabled.contains(key);
		}

		@Override
		public void enable(Collection<T> keys) {
			this.enabled.clear();
			this.enabled.addAll(keys);

			this.setChanged();
			this.notifyObservers();
		}

		@Override
		public Collection<T> enabled() {
			return ImmutableList.copyOf(this.enabled);
		}

	}

	private static class StatusEnabledVisualizationModelWithSerialForm
			extends StatusEnabledModel<Visualization<Pattern<?>, ?>>
			implements Entity, HasSerialForm<StatusEnabledVisualizationModelWithSerialForm> {

		@KdonTypeName("visualizationModel")
		private static class StatusEnabledVisualizationModelSerialForm
				implements IdentifiableSerialForm<StatusEnabledVisualizationModelWithSerialForm> {

			@JsonProperty("identifier")
			private Identifier identifier;

			@JsonProperty("visualizations")
			private Collection<String> visualizations;

			public StatusEnabledVisualizationModelSerialForm(@JsonProperty("identifier") Identifier identifier,
					@JsonProperty("visualizations") Collection<String> visualizations) {
				this.identifier = identifier;
				this.visualizations = ImmutableSet.copyOf(visualizations);
			}

			@Override
			public Identifier identifier() {
				return this.identifier;
			}

			@Override
			public StatusEnabledVisualizationModelWithSerialForm build(Workspace workspace) {
				StatusEnabledVisualizationModelWithSerialForm model = new StatusEnabledVisualizationModelWithSerialForm();

				List<Visualization<Pattern<?>, ?>> visualizations = PatternVisualizations.PATTERN_VISUALIZATIONS
						.stream().filter(p -> this.visualizations.contains(p.caption())).collect(toList());

				model.enable(visualizations);

				return model;
			}

		}

		public StatusEnabledVisualizationModelWithSerialForm() {
			super(Identifier.id("VisualizationModel"), "VisualizationModel",
					"Model for the enabling/disabling of visualizations");
		}

		@Override
		public SerialForm<? extends StatusEnabledVisualizationModelWithSerialForm> serialForm() {
			List<String> visualizations = this.enabled().stream().map(v -> v.getClass().getSimpleName())
					.collect(toList());

			return new StatusEnabledVisualizationModelSerialForm(identifier(), visualizations);
		}

	}

	// Suppress default constructor for non-instantiability
	private StatusEnabledModels() {
		throw new AssertionError();
	}

}
