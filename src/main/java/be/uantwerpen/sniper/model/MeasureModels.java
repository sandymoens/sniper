/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model;

import static be.uantwerpen.sniper.utils.MeasureRegister.measureRegister;

import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import be.uantwerpen.sniper.utils.MeasureRegister;
import de.unibonn.realkd.common.KdonTypeName;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.workspace.HasSerialForm;
import de.unibonn.realkd.common.workspace.IdentifiableSerialForm;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;

public class MeasureModels {

	public static ObservableMeasureModel newObservableMeasureModel(WorksheetPropertiesModel worksheetPropertiesModel,
			List<Measure> availableMeasures) {
		return new DefaultObservableMeasureModel(worksheetPropertiesModel, availableMeasures);
	}

	public static abstract class ObservableMeasureModel extends Observable implements MeasureModel {
	}

	public static class DefaultObservableMeasureModel extends ObservableMeasureModel
			implements Observer, HasSerialForm<DefaultObservableMeasureModel> {

		@KdonTypeName("measureModel")
		public static class StatusEnabledMeasureModelSerialForm
				implements IdentifiableSerialForm<DefaultObservableMeasureModel> {

			@JsonProperty("identifier")
			private Identifier identifier;

			@JsonProperty("measures")
			private Collection<Measure> measures;

			@JsonProperty("worksheetPropertiesModelIdentifier")
			private Identifier worksheetPropertiesModelIdentifier;

			private final ImmutableList<Identifier> dependencies;

			public StatusEnabledMeasureModelSerialForm(@JsonProperty("identifier") Identifier identifier,
					@JsonProperty("measures") Collection<Measure> measures,
					@JsonProperty("worksheetPropertiesModelIdentifier") Identifier worksheetPropertiesModelIdentifier) {
				this.identifier = identifier;
				this.measures = measures;
				this.worksheetPropertiesModelIdentifier = worksheetPropertiesModelIdentifier;
				this.dependencies = ImmutableList.of(worksheetPropertiesModelIdentifier);
			}

			@Override
			public Identifier identifier() {
				return this.identifier;
			}

			@Override
			public DefaultObservableMeasureModel build(Workspace workspace) {
				WorksheetPropertiesModel worksheetPropertiesModel = workspace
						.get(this.worksheetPropertiesModelIdentifier, WorksheetPropertiesModel.class).get();

				List<Measure> availableMeasures = ImmutableList
						.copyOf(measureRegister().getMeasures(workspace.propositionalContexts().get(0).getClass()));

				DefaultObservableMeasureModel model = new DefaultObservableMeasureModel(worksheetPropertiesModel,
						availableMeasures);

				model.enable(this.measures);

				return model;
			}

			@Override
			public Collection<Identifier> dependencyIds() {
				return this.dependencies;
			}

		}

		private List<Measure> availableMeasures;

		private Set<Measure> enabled;

		private List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> measures;

		private WorksheetPropertiesModel worksheetPropertiesModel;

		public DefaultObservableMeasureModel(WorksheetPropertiesModel worksheetPropertiesModel,
				List<Measure> availableMeasures) {
			this.availableMeasures = ImmutableList.copyOf(availableMeasures);
			this.enabled = ImmutableSet.of();
			this.measures = ImmutableList.of();
			this.worksheetPropertiesModel = worksheetPropertiesModel;
			this.worksheetPropertiesModel.addObserver(this);
		}

		@Override
		public Identifier identifier() {
			return Identifier.id("MeasureModel");
		}

		@Override
		public String caption() {
			return "MeasureModel";
		}

		@Override
		public String description() {
			return "Model for the enabling/disabling of measures";
		}

		@Override
		public boolean status(Measure key) {
			return this.enabled.contains(key);
		}

		@Override
		public List<Measure> availableMeasures() {
			return this.availableMeasures;
		}

		@Override
		public void enable(Collection<Measure> keys) {
			this.enabled = ImmutableSet.copyOf(keys);

			List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> measures = Lists.newArrayList();
			for (Measure measure : this.enabled) {
				measures.addAll(MeasureRegister.measureRegister().getMeasurementProcedures(measure));
			}
			this.measures = ImmutableList.copyOf(measures);

			this.setChanged();
			this.notifyObservers();
		}

		@Override
		public Collection<Measure> enabled() {
			return this.enabled;
		}

		@Override
		public SerialForm<? extends DefaultObservableMeasureModel> serialForm() {
			return new StatusEnabledMeasureModelSerialForm(identifier(),
					this.procedures().stream().map(p -> p.getMeasure()).collect(Collectors.toList()),
					this.worksheetPropertiesModel.identifier());
		}

		@Override
		public List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> procedures() {
			return this.measures;
		}

		@Override
		public void update(Observable o, Object arg) {
		}

	}

	// Suppress default constructor for non-instantiability
	private MeasureModels() {
		throw new AssertionError();
	}

}
