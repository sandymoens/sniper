/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model;

import static be.uantwerpen.sniper.model.bricks.PatternRootBrickFactories.patternRootBrickFactory;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import be.uantwerpen.sniper.model.bricks.Brick;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.Pattern;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.1.0
 * @version 0.1.0
 */
public class BrickModels {

	public static BrickModel newEmptyBrickModel(Workspace workspace) {
		return new DefaultBrickModel(workspace);
	}

	private static class DefaultBrickModel implements BrickModel {

		private IdGenerator idGenerator;

		private Workspace workspace;

		private List<Brick<Pattern<?>, ?>> bricks;
		private BiMap<String, Brick<?, ?>> brickMap;
		private Map<String, List<String>> topBrickToSubBrick;
		private Map<String, String> subBrickToTopBrick;

		private DefaultBrickModel(Workspace workspace) {
			this.workspace = workspace;

			this.bricks = newArrayList();
			this.brickMap = HashBiMap.create();
			this.topBrickToSubBrick = newHashMap();
			this.subBrickToTopBrick = newHashMap();

			this.idGenerator = IdGenerators.newBrickModelBasedIdGenerator(this);
		}

		private void registerBrick(Brick<?, ?> brick) {
			String topBrickId;
			List<String> subBrickIds = Lists.newArrayList();

			this.brickMap.put(topBrickId = brick.id(), brick);
			for (Brick<?, ?> subBrick : brick.allParts()) {
				String subBrickId = subBrick.id();
				this.brickMap.put(subBrickId, subBrick);
				subBrickIds.add(subBrickId);
			}

			this.topBrickToSubBrick.put(topBrickId, subBrickIds);
			subBrickIds.forEach(id -> this.subBrickToTopBrick.put(id, topBrickId));
		}

		private void unregisterBrick(String topBrickId) {
			List<String> subBrickIds = this.topBrickToSubBrick.get(topBrickId);

			this.brickMap.remove(topBrickId);
			subBrickIds.forEach(id -> this.brickMap.remove(id));

			this.topBrickToSubBrick.remove(topBrickId);
			subBrickIds.forEach(id -> this.subBrickToTopBrick.remove(id));
		}

		private boolean replacePatternAtIndex(int ix, Optional<?> replacement) {
			if (!replacement.isPresent()) {
				return false;
			}

			Optional<Brick<Pattern<?>, ?>> newBrick = patternRootBrickFactory().createBrick(replacement.get(),
					this.workspace, this.idGenerator);

			if (!newBrick.isPresent()) {
				return false;
			}

			try {
				remove(ix);
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
				return false;
			}

			this.registerBrick(newBrick.get());
			this.bricks.add(ix, newBrick.get());

			return true;
		}

		private int getTopBrickIndex(String id) {
			Brick<?, ?> topBrick = this.brickMap.get(this.subBrickToTopBrick.get(id));

			if (topBrick == null) {
				throw new NoSuchElementException();
			}

			int ix = 0;
			for (int ixEnd = this.bricks.size(); ix < ixEnd; ix++) {
				if (topBrick == this.bricks.get(ix)) {
					break;
				}
			}
			return ix;
		}

		@Override
		public void add(Object object) {
			this.add(this.bricks.size(), object);
		}

		@Override
		public Brick<Pattern<?>, ?> add(int ix, Object object) {
			Optional<Brick<Pattern<?>, ?>> topBrick = patternRootBrickFactory().createBrick(object, this.workspace,
					this.idGenerator);

			if (!topBrick.isPresent()) {
				return null;
			}

			// Append the top level brick
			this.bricks.add(ix, topBrick.get());

			registerBrick(topBrick.get());

			return topBrick.get();
		}

		@Override
		public List<Brick<Pattern<?>, ?>> addAll(int ix, Collection<Object> objects) {
			List<Brick<Pattern<?>, ?>> bricks = newArrayList();
			int i = 0;
			for (Object object : objects) {
				bricks.add(this.add(ix + i, object));
				i++;
			}
			return bricks;
		}

		@Override
		public List<Brick<Pattern<?>, ?>> addToBrickAsCollection(String id, List<? extends Object> extensions) {
			int ix = getTopBrickIndex(id);
			try {
				Brick<?, ?> brick = this.brickMap.get(id);
				if (brick == null || !replacePatternAtIndex(ix, brick.addAll(extensions))) {
					return null;
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				return null;
			}
			return ImmutableList.of(this.bricks.get(ix));
		}

		@Override
		public List<Brick<Pattern<?>, ?>> addToBrickAsSingletons(String id, List<? extends Object> extensions) {
			List<Optional<?>> newBricks = newArrayList();
			for (int i = 1; i < extensions.size(); i++) {
				Brick<?, ?> brick = this.brickMap.get(id);

				if (brick == null) {
					return null;
				}

				try {
					Optional<?> result = brick.add(extensions.get(i));
					if (!result.isPresent()) {
						// newBricks.add(brick.copyOfRootElement());
					} else {
						newBricks.add(result);
					}
				} catch (RuntimeException e) {
					e.printStackTrace();
				}
			}

			int ix = getTopBrickIndex(id);

			Collections.reverse(newBricks);
			newBricks.stream().forEach(result -> this.add(ix + 1, result.get()));

			List<Brick<Pattern<?>, ?>> newBrick = addToBrickAsCollection(id, newArrayList(extensions.get(0)));

			if (!newBricks.isEmpty()) {
				return this.bricks.subList(ix, ix + newBricks.size() + 1);
			} else if (newBrick != null) {
				return this.bricks.subList(ix, ix + 1);
			}
			return null;
		}

		@Override
		public List<Brick<Pattern<?>, ?>> replaceInBrick(String id, List<? extends Object> extensions) {
			int ix = getTopBrickIndex(id);
			Brick<?, ?> brick = this.brickMap.get(id);
			if (brick == null || !replacePatternAtIndex(ix, brick.replace(extensions))) {
				return null;
			}
			return ImmutableList.of(this.bricks.get(ix));
		}

		@Override
		public void remove(int ix) {
			unregisterBrick(this.bricks.get(ix).id());
			this.bricks.remove(ix);
		}

		@Override
		public void removeById(Collection<Integer> idsToRemove) {
			Set<Integer> toRemove = newHashSet(idsToRemove);
			for (int i = this.bricks.size() - 1; i >= 0; i--) {
				if (toRemove.remove(this.brickMap.inverse().get(this.bricks.get(i)))) {
					remove(i);
				}
			}
		}

		@Override
		public Brick<Pattern<?>, ?> removeFromBrick(String id, Object extension) {
			Brick<?, ?> brick = this.brickMap.get(id);

			if (brick == null) {
				return null;
			}

			Optional<?> remove = brick.remove(extension);

			if (!remove.isPresent()) {
				return null;
			}

			int ix = getTopBrickIndex(id);
			replacePatternAtIndex(ix, remove);
			return this.bricks.get(ix);
		}

		@Override
		public void move(int fromIx, int toIx) {
			this.bricks.add(fromIx < toIx ? toIx + 1 : toIx, this.bricks.get(fromIx));
			this.bricks.remove(fromIx < toIx ? fromIx : fromIx + 1);
		}

		@Override
		public Pattern<?> get(int ix) {
			return this.bricks.get(ix).content();
		}

		@Override
		public Brick<?, ?> getBrickByIndex(int ix) {
			return this.bricks.get(ix);
		}

		@Override
		public Brick<?, ?> getBrickById(String descriptorId) {
			return this.brickMap.get(descriptorId);
		}

		@Override
		public Brick<?, ?> getTopBrick(String descriptorId) {
			String topBrickId = this.subBrickToTopBrick.get(descriptorId);
			return this.brickMap.get(topBrickId != null ? this.subBrickToTopBrick.get(descriptorId) : descriptorId);
		}

		@Override
		public boolean bricks(List<Brick<Pattern<?>, ?>> bricks) {
			this.clear();

			bricks.stream().forEach(b -> {
				this.bricks.add(b);
				this.registerBrick(b);
			});

			return true;
		}

		@Override
		public List<Brick<Pattern<?>, ?>> bricks() {
			return ImmutableList.copyOf(this.bricks);
		}

		@Override
		public Pattern<?> pattern(int ix) {
			return this.bricks.get(ix).content();
		}

		@Override
		public List<Pattern<?>> patterns() {
			return this.bricks.stream().map(b -> b.content()).collect(Collectors.toList());
		}

		@Override
		public int size() {
			return this.bricks.size();
		}

		@Override
		public void clear() {
			this.bricks.clear();
			this.brickMap.clear();
			this.topBrickToSubBrick.clear();
			this.subBrickToTopBrick.clear();
		}

		@Override
		public void sortBricks(List<Integer> sortOrder) {
			List<Brick<Pattern<?>, ?>> sortedBricks = newArrayList();
			sortOrder.stream().forEach(i -> sortedBricks.add(this.bricks.get(i)));
			this.bricks = sortedBricks;
		}

	}

	// Suppress default constructor for non-instantiability
	private BrickModels() {
		throw new AssertionError();
	}

}
