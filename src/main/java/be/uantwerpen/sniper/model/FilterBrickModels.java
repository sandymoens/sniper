/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model;

import static be.uantwerpen.sniper.model.IdGenerators.newSimpleIdGenerator;
import static be.uantwerpen.sniper.model.bricks.PatternRootBrickFactories.patternRootBrickFactory;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import be.uantwerpen.sniper.common.TypeSelectorType;
import be.uantwerpen.sniper.model.bricks.Brick;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.Pattern;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class FilterBrickModels {

	public static FilterBrickModel newFilterBrickModel(Workspace workspace) {
		return new DefaultFilterBrickModel(workspace);
	}

	private static class DefaultFilterBrickModel implements FilterBrickModel {

		private Workspace workspace;

		private Brick<Pattern<?>, ?> brick;
		private BiMap<String, Brick<?, ?>> brickMap;

		private TypeSelectorType typeSelectorType;

		private DefaultFilterBrickModel(Workspace workspace) {
			this.workspace = workspace;

			this.brick = null;
			this.brickMap = HashBiMap.create();

			this.typeSelectorType = TypeSelectorType.SUPER_PATTERNS;
		}

		@Override
		public void typeSelectorType(TypeSelectorType typeSelectorType) {
			this.typeSelectorType = typeSelectorType;
		}

		@Override
		public TypeSelectorType typeSelectorType() {
			return this.typeSelectorType;
		}

		private void registerBrick() {
			this.brickMap.put(this.brick.id(), this.brick);

			for (Brick<?, ?> subBrick : this.brick.allParts()) {
				this.brickMap.put(subBrick.id(), subBrick);
			}
		}

		private void unregisterBrick() {
			this.brickMap.remove(this.brick.id());

			for (Brick<?, ?> subBrick : this.brick.allParts()) {
				this.brickMap.put(subBrick.id(), subBrick);
			}
		}

		@Override
		public boolean set(Object object) {
			clear();

			Optional<Brick<Pattern<?>, ?>> topBrick = patternRootBrickFactory().createBrick(object, this.workspace,
					newSimpleIdGenerator());

			if (!topBrick.isPresent()) {
				return false;
			}

			this.brick = topBrick.get();

			registerBrick();

			return true;
		}

		@Override
		public boolean addToBrick(String id, List<? extends Object> extensions) {
			try {
				Optional<?> replacement = this.brickMap.get(id).addAll(extensions);

				if (!replacement.isPresent()) {
					return false;
				}

				Optional<Brick<Pattern<?>, ?>> newBrick = patternRootBrickFactory().createBrick(replacement.get(),
						this.workspace, newSimpleIdGenerator());

				if (!newBrick.isPresent()) {
					return false;
				}

				unregisterBrick();
				this.brick = newBrick.get();
				registerBrick();

				return true;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			return false;
		}

		@Override
		public boolean removeFromBrick(String id, Object extension) {
			Optional<?> replacement = this.brickMap.get(id).remove(extension);

			if (!replacement.isPresent()) {
				return false;
			}

			Optional<Brick<Pattern<?>, ?>> newBrick = patternRootBrickFactory().createBrick(replacement.get(),
					this.workspace, newSimpleIdGenerator());

			if (!newBrick.isPresent()) {
				return false;
			}

			unregisterBrick();
			this.brick = newBrick.get();
			registerBrick();

			return true;
		}

		@Override
		public Optional<Brick<Pattern<?>, ?>> brick() {
			return Optional.ofNullable(this.brick);
		}

		@Override
		public Optional<Pattern<?>> pattern() {
			if (this.brick != null) {
				return Optional.of(this.brick.content());
			}

			return Optional.empty();
		}

		@Override
		public void clear() {
			if (this.brick == null) {
				return;
			}

			unregisterBrick();
			this.brick = null;
		}

	}

	// Suppress default constructor for non-instantiability
	private FilterBrickModels() {
		throw new AssertionError();
	}

}
