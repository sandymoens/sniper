/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model;

import java.util.Collection;
import java.util.Observable;

import de.unibonn.realkd.common.workspace.Entity;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public abstract class StatusEnabledModel<T> extends Observable implements Entity {

	public abstract boolean status(T key);

	public abstract void enable(Collection<T> keys);

	public abstract Collection<T> enabled();

}
