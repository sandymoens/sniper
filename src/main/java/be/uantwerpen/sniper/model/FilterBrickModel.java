/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model;

import java.util.List;
import java.util.Optional;

import be.uantwerpen.sniper.common.TypeSelectorType;
import be.uantwerpen.sniper.model.bricks.Brick;
import de.unibonn.realkd.patterns.Pattern;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public interface FilterBrickModel {

	public void typeSelectorType(TypeSelectorType typeSelectorType);

	public TypeSelectorType typeSelectorType();

	public boolean set(Object object);

	public boolean addToBrick(String id, List<? extends Object> extensions);

	public boolean removeFromBrick(String id, Object extension);

	public Optional<Brick<Pattern<?>, ?>> brick();

	public Optional<Pattern<?>> pattern();

	public void clear();

}
