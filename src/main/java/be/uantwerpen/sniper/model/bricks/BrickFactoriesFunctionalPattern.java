/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.collectionBrick;
import static be.uantwerpen.sniper.model.bricks.Bricks.compositeBrick;
import static be.uantwerpen.sniper.model.bricks.PatternDescriptorBrickFactories.patternDescriptorBrickFactory;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.model.IdGenerator;
import be.uantwerpen.sniper.model.StatusEnabledModel;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.emm.TotalVariationDistance;
import de.unibonn.realkd.patterns.functional.BinaryAttributeSetRelation;
import de.unibonn.realkd.patterns.functional.FunctionalPattern;
import de.unibonn.realkd.patterns.functional.FunctionalPatterns;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.models.table.ContingencyTableModelFactory;
import de.unibonn.realkd.patterns.rules.AssociationRules;
import de.unibonn.realkd.patterns.rules.DefaultRuleDescriptor;
import de.unibonn.realkd.patterns.rules.RuleDescriptor;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroups;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BrickFactoriesFunctionalPattern {

	@PatternBrickFactoryAnn(patternClass = FunctionalPattern.class)
	public static PatternBrickFactory functionalPatternBrickFactory() {
		return new FunctionalPatternBrickFactory();
	}

	@PatternDescriptorBrickFactoryAnn(patternDescriptorClass = BinaryAttributeSetRelation.class)
	public static PatternDescriptorBrickFactory binaryAttributeSetRelationDescriptorBrickFactory() {
		return new BinaryAttributeSetRelationDescriptorBrickFactory();
	}

	@UtilityBricksFactoryAnn(patternClass = FunctionalPattern.class)
	public static UtilityBricksFactory functionalPatternUtilityBricksFactory() {
		return new FunctionalPatternUtilityBricksFactory();
	}

	private static class FunctionalPatternBrickFactory implements PatternBrickFactory {

		@Override
		public Optional<Brick<PatternDescriptor, Pattern<?>>> createBrick(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			if (!FunctionalPattern.class.isAssignableFrom(pattern.getClass())) {
				return Optional.empty();
			}

			Function<PatternDescriptor, Pattern<?>> toRoot = l -> FunctionalPatterns
					.functionalPattern((BinaryAttributeSetRelation) l);

			return Optional.of(patternDescriptorBrickFactory(((FunctionalPattern) pattern).descriptor()).get()
					.createBrick(((FunctionalPattern) pattern).descriptor(), toRoot, workspace, idGenerator).get());
		}

	}

	private static class AttributeSetBrickFactory {

		public <R> Optional<Brick<Set<Attribute<?>>, R>> createBrick(Set<Attribute<?>> attributes,
				Function<Set<Attribute<?>>, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			BiFunction<Set<Attribute<?>>, Attribute<?>, Optional<Set<Attribute<?>>>> adder = (descr, p) -> {
				Set<Attribute<?>> newAttributes = Sets.newHashSet(descr);
				newAttributes.add(p);
				return Optional.of(newAttributes);
			};
			BiFunction<Set<Attribute<?>>, Attribute<?>, Optional<Set<Attribute<?>>>> remover = (descr, p) -> {
				Set<Attribute<?>> newAttributes = Sets.newHashSet(descr);
				newAttributes.remove(p);
				return Optional.of(newAttributes);
			};

			return Optional.of(collectionBrick(attributes, adder, remover, toRoot, Attribute.class, idGenerator));
		}

	}

	private static class BinaryAttributeSetRelationDescriptorBrickFactory implements PatternDescriptorBrickFactory {

		@Override
		public <R> Optional<Brick<PatternDescriptor, R>> createBrick(PatternDescriptor patternDescriptor,
				Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			if (!BinaryAttributeSetRelation.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}

			BinaryAttributeSetRelation binaryAttributeSetRelation = (BinaryAttributeSetRelation) patternDescriptor;

			AttributeSetBrickFactory attributeSetBrickFactory = new AttributeSetBrickFactory();

			Function<Set<Attribute<?>>, BinaryAttributeSetRelation> fd = l -> FunctionalPatterns
					.binaryAttributeSetRelation(binaryAttributeSetRelation.table(), l,
							binaryAttributeSetRelation.coDomain());

			Optional<Brick<Set<Attribute<?>>, R>> dBrick = attributeSetBrickFactory
					.createBrick(binaryAttributeSetRelation.domain(), fd.andThen(toRoot), workspace, idGenerator);

			Function<Set<Attribute<?>>, BinaryAttributeSetRelation> fcd = l -> FunctionalPatterns
					.binaryAttributeSetRelation(binaryAttributeSetRelation.table(), binaryAttributeSetRelation.domain(),
							l);

			Optional<Brick<Set<Attribute<?>>, R>> cdBrick = attributeSetBrickFactory
					.createBrick(binaryAttributeSetRelation.coDomain(), fcd.andThen(toRoot), workspace, idGenerator);

			if (!dBrick.isPresent() || !cdBrick.isPresent()) {
				return Optional.empty();
			}

			return Optional
					.of(compositeBrick(patternDescriptor, ImmutableList.of(dBrick.get(), cdBrick.get()), idGenerator));
		}

	}

	private static class FunctionalPatternUtilityBricksFactory implements UtilityBricksFactory {

		private static interface ObjectListDescriptor<T> {

			public <R> Optional<ObjectListDescriptor<?>> getSpecialization(R object);

			public List<T> objects();

			public Class<? super T> getObjectClass();

		}

		private static class EmptyVoidListDescriptor implements ObjectListDescriptor<Void> {

			public EmptyVoidListDescriptor() {
			}

			@Override
			@SuppressWarnings("unchecked")
			public <R> Optional<ObjectListDescriptor<?>> getSpecialization(R object) {
				if (object instanceof Integer) {
					if (!object.equals(-1)) {
						return Optional.empty();
					}

					return Optional.of(new TypedObjectListDescriptor<>(newArrayList(), Proposition.class));
				}

				Class<? super R> typedClass = (Class<? super R>) object.getClass();

				if (EmptyProposition.class.isAssignableFrom(typedClass)) {
					return Optional
							.of(new TypedObjectListDescriptor<>(newArrayList(), (Class<? super R>) Proposition.class));
				}

				return Optional.of(new TypedObjectListDescriptor<>(newArrayList(object), typedClass));
			}

			@Override
			public List<Void> objects() {
				return ImmutableList.of();
			}

			@Override
			public Class<? super Void> getObjectClass() {
				return Void.class;
			}

		}

		public static class TypedObjectListDescriptor<T> implements ObjectListDescriptor<T> {

			private Class<? super T> theClass;
			private List<T> objects;

			public TypedObjectListDescriptor(List<T> objects, Class<? super T> theClass) {
				this.theClass = theClass;
				this.objects = objects;
			}

			@Override
			public <R> Optional<ObjectListDescriptor<?>> getSpecialization(R object) {
				if (!this.theClass.isInstance(object)) {
					return Optional.empty();
				}

				List<R> newObjects = newArrayList();

				newObjects.addAll(this.objects.stream().map(o -> {
					@SuppressWarnings("unchecked")
					R r = (R) o;
					return r;
				}).collect(Collectors.toList()));
				newObjects.add(object);

				@SuppressWarnings("unchecked")
				Class<? super R> typedClass = (Class<? super R>) object.getClass();

				return Optional.of(new TypedObjectListDescriptor<>(newObjects, typedClass));
			}

			@Override
			public List<T> objects() {
				return this.objects;
			}

			@Override
			public Class<? super T> getObjectClass() {
				return this.theClass;
			}

		}

		private static Pattern<?> morphToAssociationRule(Workspace workspace, List<?> objects,
				LogicalDescriptor logicalDescriptor, PropositionalContext context,
				StatusEnabledModel<Measure> measureModel, boolean reverse) {

			LogicalDescriptor newDescriptor = LogicalDescriptors.create(context.population(),
					objects.stream().map(o -> (Proposition) o).collect(toList()));

			RuleDescriptor ruledescriptor = reverse
					? DefaultRuleDescriptor.create(context.population(), logicalDescriptor, newDescriptor)
					: DefaultRuleDescriptor.create(context.population(), newDescriptor, logicalDescriptor);

			return AssociationRules.create(ruledescriptor, newArrayList());
		}

		private static Pattern<?> morpthToEmmPattern(Workspace workspace, List<?> objects,
				LogicalDescriptor logicalDescriptor, PropositionalContext logic,
				StatusEnabledModel<Measure> measureModel) {

			List<Attribute<?>> targets = objects.stream().map(o -> (Attribute<?>) o).collect(toList());

			Subgroup<?> descriptor = Subgroups.subgroup(logicalDescriptor,
					((TableBasedPropositionalContext) logic).getDatatable(), targets,
					ContingencyTableModelFactory.INSTANCE);

			return ExceptionalModelMining.emmPattern(descriptor, TotalVariationDistance.TOTAL_VARIATION_DISTANCE,
					newArrayList());
		}

		public static Brick<ObjectListDescriptor<?>, Pattern<?>> associationMorphingBrick(Workspace workspace,
				Association association, boolean reverse, StatusEnabledModel<Measure> measureModel,
				IdGenerator idGenerator, PropositionalContext propositionalLogic) {

			final LogicalDescriptor logicalDescriptor = association.descriptor();

			Function<ObjectListDescriptor<?>, Pattern<?>> fl = l -> {

				if (Proposition.class.isAssignableFrom(l.getObjectClass())) {
					return morphToAssociationRule(workspace, l.objects(), logicalDescriptor, propositionalLogic,
							measureModel, reverse);
				} else if (Attribute.class.isAssignableFrom(l.getObjectClass())) {
					return morpthToEmmPattern(workspace, l.objects(), logicalDescriptor, propositionalLogic,
							measureModel);
				}

				return association;
			};

			BiFunction<ObjectListDescriptor<?>, ?, Optional<ObjectListDescriptor<?>>> adder = (descr, o) -> descr
					.getSpecialization(o);

			return collectionBrick(new EmptyVoidListDescriptor(), adder, null, fl, Object.class, idGenerator);
		}

		@Override
		public List<Brick<?, Pattern<?>>> createBricks(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {
			return ImmutableList.of();
		}

	}

	// Suppress default constructor for non-instantiability
	private BrickFactoriesFunctionalPattern() {
		throw new AssertionError();
	}

}
