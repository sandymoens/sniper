/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static com.google.common.collect.ImmutableList.copyOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import be.uantwerpen.sniper.model.IdGenerator;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 *
 *
 * @author Mario Boley
 * @author Sandy Moens
 * @since 0.1.1
 * @version 0.3.0
 */
public class Bricks {

	public static interface PatternBrick<R> extends Brick<Pattern<?>, R> {

		public Brick<? extends PatternDescriptor, R> patternDescriptorBrick();

		public List<Brick<?, R>> utilityBricks();

	}

	/**
	 * Interface that specifies a reverse operation on the constituting brick.
	 */
	public static interface ReversingBrick<T> extends Brick<Object, T> {
	}

	public static PatternBrick<Pattern<?>> patternCompositeBrick(Pattern<?> content,
			Brick<? extends PatternDescriptor, Pattern<?>> patternDescriptorBrick,
			List<Brick<?, Pattern<?>>> utilityBricks, IdGenerator idGenerator) {
		return new DefaultPatternCompositeBrick(idGenerator.newId(), content, patternDescriptorBrick, utilityBricks);
	}

	public static <T, R> Brick<T, R> compositeBrick(T content, List<Brick<?, R>> parts, IdGenerator idGenerator) {
		return new DefaultCompositeBrick<>(idGenerator.newId(), content, parts);
	}

	public static <G, R> Brick<ImmutableList<G>, R> setBrick(Collection<? extends G> content,
			Function<? super ImmutableList<G>, R> toRoot, Class<? super G> elementType, IdGenerator idGenerator) {
		BiFunction<ImmutableList<G>, G, Optional<ImmutableList<G>>> adder = (c, b) -> {
			if (c.contains(b)) {
				return Optional.empty();
			}
			List<G> augmented = new ArrayList<>(c);
			augmented.add(b);
			return Optional.of(copyOf(augmented));
		};
		BiFunction<ImmutableList<G>, G, Optional<ImmutableList<G>>> remover = (c, b) -> {
			if (c.contains(b)) {
				List<G> augmented = new ArrayList<>(c);
				augmented.remove(b);
				return Optional.of(copyOf(augmented));
			}
			return Optional.empty();
		};
		ImmutableList<G> copyOf = copyOf(content);
		return collectionBrick(copyOf, adder, remover, toRoot, elementType, idGenerator);
	}

	public static <G, R> Brick<ImmutableList<G>, R> listBrick(Collection<? extends G> content,
			Function<? super ImmutableList<G>, R> toRoot, Class<? super G> elementType, IdGenerator idGenerator) {

		BiFunction<ImmutableList<G>, G, Optional<ImmutableList<G>>> adder = (c, b) -> {
			List<G> augmented = new ArrayList<>(c);
			augmented.add(b);
			return Optional.of(copyOf(augmented));
		};

		BiFunction<ImmutableList<G>, G, Optional<ImmutableList<G>>> remover = (c, b) -> {
			if (c.contains(b)) {
				List<G> augmented = new ArrayList<>(c);
				augmented.remove(b);
				return Optional.of(copyOf(augmented));
			}
			return Optional.empty();
		};

		BiFunction<ImmutableList<G>, ImmutableList<G>, Optional<ImmutableList<G>>> replacer = (c, b) -> {
			return Optional.of(copyOf(b));
		};

		ImmutableList<G> copyOf = copyOf(content);
		return collectionBrick(copyOf, adder, remover, replacer, toRoot, elementType, idGenerator);
	}

	public static <T, R, G> Brick<T, R> collectionBrick(T content, BiFunction<T, G, Optional<T>> adder,
			BiFunction<T, G, Optional<T>> remover, BiFunction<T, T, Optional<T>> replacer,
			Function<? super T, R> toRoot, Class<? super G> elementType, IdGenerator idGenerator) {
		return new DefaultCollectionBrick<>(idGenerator.newId(), content, adder, remover, replacer, toRoot,
				elementType);
	}

	public static <T, R, G> Brick<T, R> collectionBrick(T content, BiFunction<T, G, Optional<T>> adder,
			BiFunction<T, G, Optional<T>> remover, Function<? super T, R> toRoot, Class<? super G> elementType,
			IdGenerator idGenerator) {
		return new DefaultCollectionBrick<>(idGenerator.newId(), content, adder, remover, toRoot, elementType);
	}

	public static <T> ReversingBrick<T> reversingBrick(String id, Function<Object, T> toRoot) {
		return new DefaultReversingBrick<>(id, toRoot);
	}

	public static <T, R> SimpleBrick<T, R> simpleBrick(String caption, T content, Function<Object, R> toRoot,
			IdGenerator idGenerator) {
		return new SimpleBrick<>(idGenerator.newId(), caption, content, toRoot);
	}

	private static class DefaultCollectionBrick<T, R, G> implements Brick<T, R> {

		private String id;

		private final T content;

		private final BiFunction<T, G, Optional<T>> adder;

		private final BiFunction<T, G, Optional<T>> remover;

		private final BiFunction<T, T, Optional<T>> replacer;

		private final Function<? super T, R> toRoot;

		private final Class<? super G> elementType;

		public DefaultCollectionBrick(String id, T content, BiFunction<T, G, Optional<T>> adder,
				BiFunction<T, G, Optional<T>> remover, Function<? super T, R> toRoot, Class<? super G> elementType) {
			this(id, content, adder, remover, null, toRoot, elementType);
		}

		public DefaultCollectionBrick(String id, T content, BiFunction<T, G, Optional<T>> adder,
				BiFunction<T, G, Optional<T>> remover, BiFunction<T, T, Optional<T>> replacer,
				Function<? super T, R> toRoot, Class<? super G> elementType) {
			this.id = id;
			this.adder = adder;
			this.remover = remover;
			this.replacer = replacer;
			this.content = content;
			this.toRoot = toRoot;
			this.elementType = elementType;
		}

		@Override
		public String id() {
			return this.id;
		}

		private Optional<G> correctlyTypedElement(Object element) {
			try {
				@SuppressWarnings("unchecked") // safe due to catch
				G castedElement = (G) this.elementType.cast(element);
				return Optional.of(castedElement);
			} catch (ClassCastException exc) {
				return Optional.empty();
			}
		}

		private Optional<T> correctlyTypedList(List<? extends Object> element) {
			try {
				@SuppressWarnings("unchecked") // safe due to catch
				T castedList = (T) ImmutableList.copyOf(element);
				return Optional.of(castedList);
			} catch (ClassCastException exc) {
				return Optional.empty();
			}
		}

		@Override
		public Optional<R> copyOfRootElement() {
			return Optional.of(this.toRoot.apply(this.content));
		}

		@Override
		public Optional<R> add(Object element) {
			Optional<G> castElement = correctlyTypedElement(element);
			Optional<T> newContent = castElement.flatMap(g -> this.adder.apply(this.content, g));
			return newContent.map(c -> this.toRoot.apply(c));
		}

		@Override
		public Optional<R> addAll(List<? extends Object> elements) {
			Optional<T> newContent = null;
			for (Object element : elements) {
				Optional<G> castElement = correctlyTypedElement(element);
				T theContent = newContent == null ? this.content : newContent.get();
				Optional<T> tmp = castElement.flatMap(g -> this.adder.apply(theContent, g));
				if (tmp.isPresent()) {
					newContent = tmp;
				}
			}
			if (newContent == null) {
				return Optional.empty();
			}
			return newContent.map(c -> this.toRoot.apply(c));
		}

		@Override
		public Optional<R> remove(Object element) {
			Optional<G> castElement = correctlyTypedElement(element);
			Optional<T> newContent = castElement.flatMap(g -> this.remover.apply(this.content, g));
			return newContent.map(c -> this.toRoot.apply(c));
		}

		@Override
		public Optional<R> replace(List<? extends Object> elements) {
			Optional<T> castElements = correctlyTypedList(elements);
			Optional<T> newContent = this.replacer.apply(this.content, castElements.get());
			return newContent.map(c -> this.toRoot.apply(c));
		}

		@Override
		public T content() {
			return this.content;
		}

		@Override
		public String toString() {
			return "add/remove(" + this.content.toString() + ")";
		}

	}

	private static class DefaultPatternCompositeBrick implements PatternBrick<Pattern<?>> {

		private String id;

		private final Pattern<?> content;

		private final List<Brick<?, Pattern<?>>> parts;

		private Brick<? extends PatternDescriptor, Pattern<?>> patternDescriptorBrick;

		private List<Brick<?, Pattern<?>>> utilityBricks;

		public DefaultPatternCompositeBrick(String id, Pattern<?> content,
				Brick<? extends PatternDescriptor, Pattern<?>> patternDescriptorBrick,
				List<Brick<?, Pattern<?>>> utilityBricks) {
			this.id = id;
			this.content = content;
			this.patternDescriptorBrick = patternDescriptorBrick;
			this.utilityBricks = utilityBricks;

			List<Brick<?, Pattern<?>>> parts = Lists.newArrayList();
			parts.add(patternDescriptorBrick);
			parts.addAll(utilityBricks);

			this.parts = ImmutableList.copyOf(parts);
		}

		@Override
		public String id() {
			return this.id;
		}

		@Override
		public Pattern<?> content() {
			return this.content;
		}

		@Override
		public List<Brick<?, Pattern<?>>> parts() {
			return this.parts;
		}

		@Override
		public String toString() {
			return this.parts.toString();
		}

		@Override
		public Brick<? extends PatternDescriptor, Pattern<?>> patternDescriptorBrick() {
			return this.patternDescriptorBrick;
		}

		@Override
		public List<Brick<?, Pattern<?>>> utilityBricks() {
			return this.utilityBricks;
		}

	}

	private static class DefaultCompositeBrick<T, R> implements Brick<T, R> {

		private String id;

		private final T content;

		private final List<Brick<?, R>> parts;

		public DefaultCompositeBrick(String id, T content, List<Brick<?, R>> parts) {
			this.id = id;
			this.content = content;
			this.parts = parts;
		}

		@Override
		public String id() {
			return this.id;
		}

		@Override
		public T content() {
			return this.content;
		}

		@Override
		public List<Brick<?, R>> parts() {
			return this.parts;
		}

		@Override
		public String toString() {
			return this.parts.toString();
		}

	}

	private static class DefaultReversingBrick<T> implements ReversingBrick<T> {

		private String id;
		private final Function<Object, T> toRoot;

		public DefaultReversingBrick(String id, Function<Object, T> toRoot) {
			this.id = id;
			this.toRoot = toRoot;
		}

		@Override
		public String id() {
			return this.id;
		}

		@Override
		public Optional<T> copyOfRootElement() {
			return Optional.of(this.toRoot.apply(null));
		}

		@Override
		public Optional<T> add(Object element) {
			return Optional.of(this.toRoot.apply(null));
		}

		@Override
		public Optional<T> addAll(List<? extends Object> elements) {
			return Optional.of(this.toRoot.apply(null));
		}

		@Override
		public Optional<T> remove(Object element) {
			return Optional.of(this.toRoot.apply(null));
		}

		@Override
		public T content() {
			return null;
		}

		@Override
		public String toString() {
			return "reverse";
		}

	}

	private static class SimpleBrick<T, R> implements Brick<T, R> {

		private final String id;
		private final String caption;
		private final T content;
		private final Function<Object, R> toRoot;

		public SimpleBrick(String id, String caption, T content, Function<Object, R> toRoot) {
			this.id = id;
			this.caption = caption;
			this.content = content;
			this.toRoot = toRoot;
		}

		@Override
		public String id() {
			return this.id;
		}

		@Override
		public Optional<R> copyOfRootElement() {
			return Optional.of(this.toRoot.apply(null));
		}

		@Override
		public Optional<R> add(Object element) {
			return Optional.of(this.toRoot.apply(element));
		}

		@Override
		public Optional<R> addAll(List<? extends Object> elements) {
			if (elements.size() == 1) {
				return add(elements.get(0));
			}
			return Optional.empty();
		}

		@Override
		public Optional<R> remove(Object element) {
			return Optional.empty();
		}

		@Override
		public T content() {
			return this.content;
		}

		@Override
		public String toString() {
			return this.caption;
		}

	}

	// Suppress default constructor for non-instantiability
	private Bricks() {
		throw new AssertionError();
	}
}
