/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.patternCompositeBrick;
import static be.uantwerpen.sniper.model.bricks.PatternBrickFactories.patternBrickFactory;
import static be.uantwerpen.sniper.model.bricks.UtilityBricksFactories.utilityBricksFactory;

import java.util.List;
import java.util.Optional;

import be.uantwerpen.sniper.model.IdGenerator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternRootBrickFactories {

	private static PatternRootBrickFactory patternRootBrickFactory;

	public static PatternRootBrickFactory patternRootBrickFactory() {
		return patternRootBrickFactory != null ? patternRootBrickFactory
				: (patternRootBrickFactory = new DefaultPatternRootBrickFactory());
	}

	private static class DefaultPatternRootBrickFactory implements PatternRootBrickFactory {

		@Override
		public Optional<Brick<Pattern<?>, ?>> createBrick(Object object, Workspace workspace, IdGenerator idGenerator) {

			Pattern<?> pattern = (Pattern<?>) object;

			Optional<PatternBrickFactory> patternBrickFactory = patternBrickFactory(pattern);
			Optional<UtilityBricksFactory> utilityBricksFactory = utilityBricksFactory(pattern);

			if (!patternBrickFactory.isPresent() || !utilityBricksFactory.isPresent()) {
				return Optional.empty();
			}

			Optional<Brick<PatternDescriptor, Pattern<?>>> patternDescriptorBrick = patternBrickFactory.get()
					.createBrick(pattern, workspace, idGenerator);

			if (!patternDescriptorBrick.isPresent()) {
				return Optional.empty();
			}

			List<Brick<?, Pattern<?>>> utilityBricks = utilityBricksFactory.get().createBricks(pattern, workspace,
					idGenerator);

			return Optional
					.of(patternCompositeBrick(pattern, patternDescriptorBrick.get(), utilityBricks, idGenerator));
		}

	}

}
