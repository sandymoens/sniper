/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.compositeBrick;
import static be.uantwerpen.sniper.model.bricks.PatternDescriptorBrickFactories.patternDescriptorBrickFactory;
import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import be.uantwerpen.sniper.model.IdGenerator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRules;
import de.unibonn.realkd.patterns.graphs.Edge;
import de.unibonn.realkd.patterns.graphs.Edges;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;
import de.unibonn.realkd.patterns.graphs.Nodes;

/**
 *
 * @author Ali Doku
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BrickFactoriesEpisodeRule {

	@PatternBrickFactoryAnn(patternClass = EpisodeRule.class)
	public static PatternBrickFactory episodeRuleBrickFactory() {
		return new EpisodeRuleBrickFactory();
	}

	@PatternDescriptorBrickFactoryAnn(patternDescriptorClass = EpisodeRuleDescriptor.class)
	public static PatternDescriptorBrickFactory episodeRuleDescriptorBrickFactory() {
		return new EpisodeRuleDescriptorBrickFactory();
	}

	@UtilityBricksFactoryAnn(patternClass = EpisodeRule.class)
	public static UtilityBricksFactory episodeRuleUtilityBricksFactory() {
		return new EpisodeRuleUtilityBricksFactory();
	}

	private static class EpisodeRuleBrickFactory implements PatternBrickFactory {

		@Override
		public Optional<Brick<PatternDescriptor, Pattern<?>>> createBrick(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			if (!EpisodeRule.class.isAssignableFrom(pattern.getClass())) {
				return Optional.empty();
			}

			Function<PatternDescriptor, Pattern<?>> toRoot = l -> EpisodeRules.create((EpisodeRuleDescriptor) l,
					newArrayList(), ImmutableList.of());

			return Optional.of(patternDescriptorBrickFactory(((EpisodeRule) pattern).descriptor()).get()
					.createBrick(((EpisodeRule) pattern).descriptor(), toRoot, workspace, idGenerator).get());
		}

	}

	// private static class GraphDescriptorBrickFactory implements
	// PatternDescriptorBrickFactory {
	//
	// @Override
	// public <R> Optional<Brick<PatternDescriptor, R>>
	// createBrick(PatternDescriptor patternDescriptor,
	// Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator
	// idGenerator) {
	// // to be implemented
	// return null;
	// }
	//
	// }

	private static class EpisodeRuleDescriptorBrickFactory implements PatternDescriptorBrickFactory {

		@Override
		public <R> Optional<Brick<PatternDescriptor, R>> createBrick(PatternDescriptor patternDescriptor,
				Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			if (!EpisodeRuleDescriptor.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}
			EpisodeRuleDescriptor ruleDescriptor = (EpisodeRuleDescriptor) patternDescriptor;

			Optional<PatternDescriptorBrickFactory> patternDescriptorBrickFactory = PatternDescriptorBrickFactories
					.patternDescriptorBrickFactory(ruleDescriptor.antecedent());

			if (!patternDescriptorBrickFactory.isPresent()) {
				return Optional.empty();
			}

			Function<PatternDescriptor, EpisodeRuleDescriptor> fa = l -> {
				Map<Integer, Integer> mapping = Maps.newHashMap();

				List<Node> nodes = Lists.newArrayList(((GraphDescriptor) l).nodes());

				int newId = ruleDescriptor.consequent().nodes().stream().mapToInt(n -> n.id()).max().getAsInt() + 1;

				for (Node node : ruleDescriptor.consequent().nodes()) {
					boolean add = true;
					boolean idInUse = false;

					for (Node nnode : nodes) {
						if (nnode.id() == node.id()) {
							idInUse = true;
							if (nnode.name().equals(node.name())) {
								add = false;
								break;
							}
						}
					}

					if (add) {
						if (idInUse) {
							mapping.put(node.id(), newId);
							nodes.add(Nodes.create(newId, node.proposition()));
						} else {
							nodes.add(node);
						}
					}
				}

				Set<Edge> edges = Sets.newHashSet(((GraphDescriptor) l).edges());

				for (Edge edge : ruleDescriptor.consequent().edges()) {
					Integer newStart = mapping.get(edge.start());
					Integer newEnd = mapping.get(edge.end());

					Edge newEdge = Edges.create(newStart != null ? newStart : edge.start(),
							newEnd != null ? newEnd : edge.end());

					edges.add(newEdge);
				}

				GraphDescriptor graph = GraphDescriptors.create(ImmutableList.copyOf(nodes),
						ImmutableList.copyOf(edges));

				return EpisodeRuleDescriptors.create(ruleDescriptor.propositionalContext(), ruleDescriptor.windowSize(),
						(GraphDescriptor) l, graph);
			};

			Optional<Brick<PatternDescriptor, R>> aBrick = patternDescriptorBrickFactory.get()
					.createBrick(ruleDescriptor.antecedent(), fa.andThen(toRoot), workspace, idGenerator);

			Function<PatternDescriptor, EpisodeRuleDescriptor> fc = l -> {
				Set<Node> nodes = Sets.newHashSet(ruleDescriptor.antecedent().nodes());
				nodes.retainAll(((GraphDescriptor) l).nodes());

				Set<Edge> edges = Sets.newHashSet(ruleDescriptor.antecedent().edges());
				edges.retainAll(((GraphDescriptor) l).edges());

				GraphDescriptor graph = GraphDescriptors.create(ImmutableList.copyOf(nodes),
						ImmutableList.copyOf(edges));

				return EpisodeRuleDescriptors.create(ruleDescriptor.propositionalContext(), ruleDescriptor.windowSize(),
						graph, (GraphDescriptor) l);
			};

			Optional<Brick<PatternDescriptor, R>> cBrick = patternDescriptorBrickFactory.get()
					.createBrick(ruleDescriptor.consequent(), fc.andThen(toRoot), workspace, idGenerator);

			// EpisodeRuleDescriptor ruleDescriptor = (EpisodeRuleDescriptor)
			// patternDescriptor;
			// Optional<PatternDescriptorBrickFactory>
			// patternDescriptorBrickFactory =
			// PatternDescriptorBrickFactories
			// .patternDescriptorBrickFactory(ruleDescriptor.getAntecedent());

			if (!aBrick.isPresent() || !cBrick.isPresent()) {
				return Optional.empty();
			}

			return Optional
					.of(compositeBrick(patternDescriptor, ImmutableList.of(aBrick.get(), cBrick.get()), idGenerator));
		}

	}

	private static class EpisodeRuleUtilityBricksFactory implements UtilityBricksFactory {

		@Override
		public List<Brick<?, Pattern<?>>> createBricks(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {
			return ImmutableList.of();
		}

	}

	// Suppress default constructor for non-instantiability
	private BrickFactoriesEpisodeRule() {
		throw new AssertionError();
	}

}
