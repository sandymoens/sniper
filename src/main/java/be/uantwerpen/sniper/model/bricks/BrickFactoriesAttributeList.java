/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.collectionBrick;
import static be.uantwerpen.sniper.model.bricks.PatternDescriptorBrickFactories.patternDescriptorBrickFactory;
import static be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptors.attributeListDescriptor;
import static be.uantwerpen.sniper.patterns.attributes.AttributeLists.attributeList;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static de.unibonn.realkd.patterns.functional.FunctionalPatterns.binaryAttributeSetRelation;
import static de.unibonn.realkd.patterns.functional.FunctionalPatterns.functionalPattern;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.model.IdGenerator;
import be.uantwerpen.sniper.patterns.attributes.AttributeList;
import be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptor;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.emm.TotalVariationDistance;
import de.unibonn.realkd.patterns.functional.BinaryAttributeSetRelation;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.models.table.ContingencyTableModelFactory;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroups;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BrickFactoriesAttributeList {

	@PatternBrickFactoryAnn(patternClass = AttributeList.class)
	public static PatternBrickFactory attributeListBrickFactory() {
		return new AttributeListBrickFactory();
	}

	@PatternDescriptorBrickFactoryAnn(patternDescriptorClass = AttributeListDescriptor.class)
	public static PatternDescriptorBrickFactory attributeListDescriptorBrickFactory() {
		return new AttributeListDescriptorBrickFactory();
	}

	@UtilityBricksFactoryAnn(patternClass = AttributeList.class)
	public static UtilityBricksFactory attributeListUtilityBricksFactory() {
		return new AttributeListUtilityBricksFactory();
	}

	private static class AttributeListBrickFactory implements PatternBrickFactory {

		@Override
		public Optional<Brick<PatternDescriptor, Pattern<?>>> createBrick(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			if (!AttributeList.class.isAssignableFrom(pattern.getClass())) {
				return Optional.empty();
			}

			Function<PatternDescriptor, Pattern<?>> toRoot = l -> attributeList(((AttributeList) pattern).population(),
					(AttributeListDescriptor) l, newArrayList());

			return Optional.of(patternDescriptorBrickFactory(((AttributeList) pattern).descriptor()).get()
					.createBrick(((AttributeList) pattern).descriptor(), toRoot, workspace, idGenerator).get());
		}

	}

	private static class AttributeListDescriptorBrickFactory implements PatternDescriptorBrickFactory {

		@Override
		public <R> Optional<Brick<PatternDescriptor, R>> createBrick(PatternDescriptor patternDescriptor,
				Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			if (!AttributeListDescriptor.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}

			AttributeListDescriptor attributeListDescriptor = (AttributeListDescriptor) patternDescriptor;

			BiFunction<PatternDescriptor, Attribute<?>, Optional<PatternDescriptor>> adder = (descr, a) -> {
				if (attributeListDescriptor.attributes().contains(a)) {
					return Optional.empty();
				}

				List<Attribute<?>> newAttributes = newArrayList(attributeListDescriptor.attributes());
				newAttributes.add(a);

				return Optional.of(attributeListDescriptor(attributeListDescriptor.table(), newAttributes));
			};

			BiFunction<PatternDescriptor, Attribute<?>, Optional<PatternDescriptor>> remover = (descr, a) -> {
				if (!attributeListDescriptor.attributes().contains(a)) {
					return Optional.empty();
				}

				List<Attribute<?>> newAttributes = newArrayList(attributeListDescriptor.attributes());
				newAttributes.remove(a);

				return Optional.of(attributeListDescriptor(attributeListDescriptor.table(), newAttributes));
			};

			return Optional
					.of(collectionBrick(patternDescriptor, adder, remover, toRoot, Attribute.class, idGenerator));
		}

	}

	private static class AttributeListUtilityBricksFactory implements UtilityBricksFactory {

		private static interface ObjectListDescriptor<T> {

			public <R> Optional<ObjectListDescriptor<?>> getSpecialization(R object);

			public List<T> objects();

			public Class<? super T> getObjectClass();

		}

		private static class EmptyVoidListDescriptor implements ObjectListDescriptor<Void> {

			public EmptyVoidListDescriptor() {
			}

			@Override
			@SuppressWarnings("unchecked")
			public <R> Optional<ObjectListDescriptor<?>> getSpecialization(R object) {
				if (object instanceof Integer) {
					if (!object.equals(-1)) {
						return Optional.empty();
					}

					return Optional.of(new TypedObjectListDescriptor<>(newArrayList(), Proposition.class));
				}

				Class<? super R> typedClass = (Class<? super R>) object.getClass();

				if (EmptyProposition.class.isAssignableFrom(typedClass)) {
					return Optional
							.of(new TypedObjectListDescriptor<>(newArrayList(), (Class<? super R>) Proposition.class));
				}

				return Optional.of(new TypedObjectListDescriptor<>(newArrayList(object), typedClass));
			}

			@Override
			public List<Void> objects() {
				return ImmutableList.of();
			}

			@Override
			public Class<? super Void> getObjectClass() {
				return Void.class;
			}

		}

		public static class TypedObjectListDescriptor<T> implements ObjectListDescriptor<T> {

			private Class<? super T> theClass;
			private List<T> objects;

			public TypedObjectListDescriptor(List<T> objects, Class<? super T> theClass) {
				this.theClass = theClass;
				this.objects = objects;
			}

			@Override
			public <R> Optional<ObjectListDescriptor<?>> getSpecialization(R object) {
				if (!this.theClass.isInstance(object)) {
					return Optional.empty();
				}

				List<R> newObjects = newArrayList();

				newObjects.addAll(this.objects.stream().map(o -> {
					@SuppressWarnings("unchecked")
					R r = (R) o;
					return r;
				}).collect(Collectors.toList()));
				newObjects.add(object);

				@SuppressWarnings("unchecked")
				Class<? super R> typedClass = (Class<? super R>) object.getClass();

				return Optional.of(new TypedObjectListDescriptor<>(newObjects, typedClass));
			}

			@Override
			public List<T> objects() {
				return this.objects;
			}

			@Override
			public Class<? super T> getObjectClass() {
				return this.theClass;
			}

		}

		private static Pattern<?> morpthToEmmPattern(Workspace workspace, List<?> objects,
				AttributeListDescriptor attributeListDescriptor, PropositionalContext context) {

			LogicalDescriptor logicalDescriptor = LogicalDescriptors.create(context.population(),
					objects.stream().map(o -> (Proposition) o).collect(toList()));

			Subgroup<?> descriptor = Subgroups.subgroup(logicalDescriptor, attributeListDescriptor.table(),
					attributeListDescriptor.attributes(), ContingencyTableModelFactory.INSTANCE);

			return ExceptionalModelMining.emmPattern(descriptor, TotalVariationDistance.TOTAL_VARIATION_DISTANCE,
					newArrayList());
		}

		private static Pattern<?> morphToFunctionalPattern(Workspace workspace, List<?> objects,
				AttributeListDescriptor attributeListDescriptor, PropositionalContext propositionalLogic,
				boolean reverse) {

			Set<Attribute<?>> set1 = newHashSet(attributeListDescriptor.attributes());
			Set<Attribute<?>> set2 = objects.stream().map(o -> (Attribute<?>) o).collect(Collectors.toSet());

			BinaryAttributeSetRelation binaryAttributeSetRelation = reverse
					? binaryAttributeSetRelation(attributeListDescriptor.table(), set1, set2)
					: binaryAttributeSetRelation(attributeListDescriptor.table(), set2, set1);

			return functionalPattern(binaryAttributeSetRelation);
		}

		public static Brick<ObjectListDescriptor<?>, Pattern<?>> attributeListMorphingBrick(Workspace workspace,
				AttributeList attributeList, boolean reverse, IdGenerator idGenerator,
				PropositionalContext propositionalLogic) {

			final AttributeListDescriptor attributeListDescriptor = attributeList.descriptor();

			Function<ObjectListDescriptor<?>, Pattern<?>> fl = l -> {

				if (Proposition.class.isAssignableFrom(l.getObjectClass())) {
					return morpthToEmmPattern(workspace, l.objects(), attributeListDescriptor, propositionalLogic);
				} else if (Attribute.class.isAssignableFrom(l.getObjectClass())) {
					return morphToFunctionalPattern(workspace, l.objects(), attributeListDescriptor, propositionalLogic,
							reverse);
				}

				return attributeList;
			};

			BiFunction<ObjectListDescriptor<?>, ?, Optional<ObjectListDescriptor<?>>> adder = (descr, o) -> descr
					.getSpecialization(o);

			return collectionBrick(new EmptyVoidListDescriptor(), adder, null, fl, Object.class, idGenerator);
		}

		@Override
		public List<Brick<?, Pattern<?>>> createBricks(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			PropositionalContext propositionalLogic = workspace.propositionalContexts().get(0);

			Brick<ObjectListDescriptor<?>, Pattern<?>> lBrick = attributeListMorphingBrick(workspace,
					(AttributeList) pattern, false, idGenerator, propositionalLogic);

			Brick<ObjectListDescriptor<?>, Pattern<?>> rBrick = attributeListMorphingBrick(workspace,
					(AttributeList) pattern, true, idGenerator, propositionalLogic);

			return ImmutableList.of(lBrick, rBrick);
		}

	}

	// Suppress default constructor for non-instantiability
	private BrickFactoriesAttributeList() {
		throw new AssertionError();
	}

}
