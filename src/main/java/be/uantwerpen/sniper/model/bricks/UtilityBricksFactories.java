/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static com.google.common.collect.Maps.newHashMap;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class UtilityBricksFactories {

	private static Map<Class<? extends Pattern<?>>, UtilityBricksFactory> factories;

	public static void register(Class<? extends Pattern<?>> clazz, UtilityBricksFactory factory) {
		factories.put(clazz, factory);
	}

	static {
		factories = newHashMap();

		Reflections reflections = new Reflections(
				new ConfigurationBuilder().addUrls(ClasspathHelper.forPackage("be.uantwerpen.sniper.model.bricks"))
						.setScanners(new MethodAnnotationsScanner()));

		reflections.getMethodsAnnotatedWith(UtilityBricksFactoryAnn.class).stream().forEach(m -> {
			try {
				register(m.getAnnotation(UtilityBricksFactoryAnn.class).patternClass(),
						(UtilityBricksFactory) m.invoke(null));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}

	public static Optional<UtilityBricksFactory> utilityBricksFactory(Pattern<?> pattern) {
		for (Entry<Class<? extends Pattern<?>>, UtilityBricksFactory> entry : factories.entrySet()) {
			if (entry.getKey().isAssignableFrom(pattern.getClass())) {
				return Optional.of(entry.getValue());
			}
		}

		return Optional.empty();
	}

}
