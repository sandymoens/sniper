/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.collectionBrick;
import static be.uantwerpen.sniper.model.bricks.PatternDescriptorBrickFactories.patternDescriptorBrickFactory;
import static com.google.common.collect.Lists.newArrayList;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.model.IdGenerator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContext;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.sequence.DefaultSequenceDescriptor;
import de.unibonn.realkd.patterns.sequence.Sequence;
import de.unibonn.realkd.patterns.sequence.SequenceDescriptor;
import de.unibonn.realkd.patterns.sequence.Sequences;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BrickFactoriesSequence {

	@PatternBrickFactoryAnn(patternClass = Sequence.class)
	public static PatternBrickFactory sequenceBrickFactory() {
		return new SequenceBrickFactory();
	}

	@PatternDescriptorBrickFactoryAnn(patternDescriptorClass = SequenceDescriptor.class)
	public static PatternDescriptorBrickFactory sequenceDescriptorBrickFactory() {
		return new SequenceDescriptorBrickFactory();
	}

	@UtilityBricksFactoryAnn(patternClass = Sequence.class)
	public static UtilityBricksFactory sequenceUtilityBricksFactory() {
		return new SequenceUtilityBricksFactory();
	}

	private static OrderedPropositionListBrickFactory orderedPropositionListBrickFactory = new OrderedPropositionListBrickFactory();

	private static class SequenceBrickFactory implements PatternBrickFactory {

		@Override
		public Optional<Brick<PatternDescriptor, Pattern<?>>> createBrick(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			if (!Sequence.class.isAssignableFrom(pattern.getClass())) {
				return Optional.empty();
			}

			Function<PatternDescriptor, Pattern<?>> toRoot = l -> Sequences.create((SequenceDescriptor) l,
					newArrayList());

			return Optional.of(patternDescriptorBrickFactory(((Sequence) pattern).descriptor()).get()
					.createBrick(((Sequence) pattern).descriptor(), toRoot, workspace, idGenerator).get());
		}

	}

	private static class OrderedPropositionListBrickFactory {

		public <R> Optional<Brick<List<Proposition>, R>> createBrick(List<Proposition> propositions,
				Function<List<Proposition>, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			BiFunction<List<Proposition>, Proposition, Optional<List<Proposition>>> adder = (descr, p) -> {
				if (EmptyProposition.class.isAssignableFrom(p.getClass())) {
					return Optional.empty();
				}

				List<Proposition> newPropositions = newArrayList(descr);
				newPropositions.add(p);
				return Optional.of(newPropositions);
			};
			BiFunction<List<Proposition>, Proposition, Optional<List<Proposition>>> remover = (descr, p) -> {
				List<Proposition> newPropositions = newArrayList(descr);
				newPropositions.remove(p);
				return Optional.of(newPropositions);
			};

			return Optional.of(collectionBrick(propositions, adder, remover, toRoot, Proposition.class, idGenerator));
		}

	}

	private static class SequenceDescriptorBrickFactory implements PatternDescriptorBrickFactory {

		@Override
		public <R> Optional<Brick<PatternDescriptor, R>> createBrick(PatternDescriptor patternDescriptor,
				Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			if (!SequenceDescriptor.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}

			SequenceDescriptor sequenceDescriptor = (SequenceDescriptor) patternDescriptor;

			List<List<Proposition>> orderedSets = newArrayList(sequenceDescriptor.orderedSets());
			// if (!orderedSets.get(orderedSets.size() - 1).isEmpty()) {
			// orderedSets.add(ImmutableList.of());
			// }

			List<Brick<List<Proposition>, R>> listBricks = newArrayList();

			for (int i = 0; i < orderedSets.size(); i++) {
				int index = i;

				Function<List<Proposition>, SequenceDescriptor> fl = l -> DefaultSequenceDescriptor.create(
						sequenceDescriptor.sequentialPropositionalLogic(), newSequenceOrderedSetWithReplacedIndex(
								workspace, sequenceDescriptor.sequentialPropositionalLogic(), orderedSets, index, l));

				listBricks.add(orderedPropositionListBrickFactory
						.createBrick(orderedSets.get(i), fl.andThen(toRoot), workspace, idGenerator).get());
			}

			return Optional.of(Bricks.compositeBrick(patternDescriptor, ImmutableList.copyOf(listBricks), idGenerator));
		}

		private List<List<Proposition>> newSequenceOrderedSetWithReplacedIndex(Workspace workspace,
				SequentialPropositionalContext sequentialPropositionalLogic, List<List<Proposition>> orderedSets, int i,
				List<Proposition> replacementOrderedSet) {

			List<List<Proposition>> newOrderedSets = newArrayList();

			for (int j = 0; j < orderedSets.size(); j++) {
				if (i != j) {
					newOrderedSets.add(orderedSets.get(j));
				} else {
					newOrderedSets.add(replacementOrderedSet);
				}
			}

			for (int j = newOrderedSets.size() - 1; j >= 0; j--) {
				if (newOrderedSets.get(j).isEmpty()) {
					newOrderedSets.remove(j);
				}
			}

			return ImmutableList.copyOf(newOrderedSets);
		}

	}

	private static class SequenceUtilityBricksFactory implements UtilityBricksFactory {

		@Override
		public List<Brick<?, Pattern<?>>> createBricks(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			Sequence sequence = (Sequence) pattern;

			Function<SequenceDescriptor, Pattern<?>> fl = l -> {
				return Sequences.create(l, newArrayList());
			};

			List<Brick<?, Pattern<?>>> bricks = newArrayList();

			bricks.addAll(insertionBricks(sequence, fl, workspace, idGenerator));

			return bricks;
		}

		private Collection<? extends Brick<?, Pattern<?>>> insertionBricks(Sequence sequence,
				Function<SequenceDescriptor, Pattern<?>> fRoot, Workspace workspace, IdGenerator idGenerator) {

			SequenceDescriptor sequenceDescriptor = sequence.descriptor();

			List<Brick<?, Pattern<?>>> bricks = newArrayList();

			for (int i = 0; i <= sequence.descriptor().orderedSets().size(); i++) {
				int index = i;

				Function<List<Proposition>, SequenceDescriptor> fl = l -> DefaultSequenceDescriptor.create(
						sequenceDescriptor.sequentialPropositionalLogic(),
						newSequenceOrderedSetWithInsertedList(workspace,
								sequenceDescriptor.sequentialPropositionalLogic(), sequenceDescriptor.orderedSets(),
								index, l));

				bricks.add(orderedPropositionListBrickFactory
						.createBrick(newArrayList(), fl.andThen(fRoot), workspace, idGenerator).get());
			}

			return bricks;
		}

		private List<List<Proposition>> newSequenceOrderedSetWithInsertedList(Workspace workspace,
				SequentialPropositionalContext sequentialPropositionalLogic, List<List<Proposition>> orderedSets, int i,
				List<Proposition> insertionOrderedSet) {

			List<List<Proposition>> newOrderedSets = newArrayList(orderedSets);
			newOrderedSets.add(i, insertionOrderedSet);

			for (int j = newOrderedSets.size() - 1; j >= 0; j--) {
				if (newOrderedSets.get(j).isEmpty()) {
					newOrderedSets.remove(j);
				}
			}

			return ImmutableList.copyOf(newOrderedSets);
		}

	}

	// Suppress default constructor for non-instantiability
	private BrickFactoriesSequence() {
		throw new AssertionError();
	}

}
