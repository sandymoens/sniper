/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.simpleBrick;
import static be.uantwerpen.sniper.model.bricks.PatternDescriptorBrickFactories.patternDescriptorBrickFactory;
import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.model.IdGenerator;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.ModelDeviationMeasure;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroups;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BrickFactoriesExceptionalModelPattern {

	@PatternBrickFactoryAnn(patternClass = ExceptionalModelPattern.class)
	public static PatternBrickFactory exceptionalModelPatternBrickFactory() {
		return new ExceptionalModelPatternBrickFactory();
	}

	@PatternDescriptorBrickFactoryAnn(patternDescriptorClass = Subgroup.class)
	public static PatternDescriptorBrickFactory subgroupDescriptorBrickFactory() {
		return new SubgroupDescriptorBrickFactory();
	}

	@UtilityBricksFactoryAnn(patternClass = ExceptionalModelPattern.class)
	public static UtilityBricksFactory exceptionalModelPatternUtilityBricksFactory() {
		return new ExceptionalModelPatternUtilityBricksFactory();
	}

	private static class ExceptionalModelPatternBrickFactory implements PatternBrickFactory {

		@Override
		public Optional<Brick<PatternDescriptor, Pattern<?>>> createBrick(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			Measure m = ((ExceptionalModelPattern) pattern).getDeviationMeasure();
			MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> procedure = null;

			for (MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> p : ExceptionalModelMining
					.modelDeviationMeasures()) {
				if (p.getMeasure().equals(m)) {
					procedure = p;
					break;
				}
			}

			MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> theProcedure = procedure;

			Function<PatternDescriptor, Pattern<?>> toRoot = l -> {
				return ExceptionalModelMining.emmPattern((Subgroup<?>) l, theProcedure.getMeasure(), newArrayList());
			};

			PatternDescriptorBrickFactory patternDescriptorBrickFactory = PatternDescriptorBrickFactories
					.patternDescriptorBrickFactory(((ExceptionalModelPattern) pattern).descriptor()).get();

			patternDescriptorBrickFactory.createBrick(((ExceptionalModelPattern) pattern).descriptor(), toRoot,
					workspace, idGenerator);

			return Optional.of(patternDescriptorBrickFactory(((ExceptionalModelPattern) pattern).descriptor()).get()
					.createBrick(((ExceptionalModelPattern) pattern).descriptor(), toRoot, workspace, idGenerator)
					.get());
		}

	}

	private static class SubgroupDescriptorBrickFactory implements PatternDescriptorBrickFactory {

		@Override
		public <R> Optional<Brick<PatternDescriptor, R>> createBrick(PatternDescriptor patternDescriptor,
				Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			if (!Subgroup.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}

			Subgroup<?> subgroup = (Subgroup<?>) patternDescriptor;

			Function<PatternDescriptor, Subgroup<?>> extDescrToSubgroup = l -> Subgroups.subgroup((LogicalDescriptor) l,
					subgroup.table(), subgroup.targetAttributes(), subgroup.fittingAlgorithm());

			Optional<PatternDescriptorBrickFactory> patternDescriptorBrickFactory = patternDescriptorBrickFactory(
					subgroup.extensionDescriptor());

			if (!patternDescriptorBrickFactory.isPresent()) {
				return Optional.empty();
			}

			Optional<Brick<PatternDescriptor, R>> extDescrBrick = patternDescriptorBrickFactory.get().createBrick(
					subgroup.extensionDescriptor(), extDescrToSubgroup.andThen(toRoot), workspace, idGenerator);

			if (!extDescrBrick.isPresent()) {
				return Optional.empty();
			}

			Function<ImmutableList<Attribute<?>>, Subgroup<?>> targetAttrToSubgroup = l -> {
				if (subgroup.fittingAlgorithm().isApplicable(l)) {
					return Subgroups.subgroup(subgroup.extensionDescriptor(), subgroup.table(), l,
							subgroup.fittingAlgorithm());
				}

				throw new IllegalArgumentException("Illegal target attributes for fitting algorithm");
			};

			Brick<ImmutableList<Attribute<?>>, R> targetAttrBrick = Bricks.setBrick(subgroup.targetAttributes(),
					targetAttrToSubgroup.andThen(toRoot), Attribute.class, idGenerator);

			return Optional.of(Bricks.compositeBrick(subgroup, ImmutableList.of(extDescrBrick.get(), targetAttrBrick),
					idGenerator));
		}

	}

	private static class ExceptionalModelPatternUtilityBricksFactory implements UtilityBricksFactory {

		private Brick<?, Pattern<?>> modelTypeMorphBrick(Pattern<?> pattern, IdGenerator idGenerator,
				ExceptionalModelPattern emp) {
			Function<Object, Pattern<?>> toRoot = o -> {
				ModelFactory<?> modelFactory = (ModelFactory<?>) o;

				Subgroup<?> subgroup = Subgroups.subgroup(emp.descriptor().extensionDescriptor(),
						emp.descriptor().table(), emp.descriptor().targetAttributes(), modelFactory);

				MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> procedure = null;

				for (MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> p : ExceptionalModelMining
						.modelDeviationMeasures()) {
					if (p.isApplicable(subgroup)) {
						procedure = p;
						break;
					}
				}

				return ExceptionalModelMining.emmPattern(subgroup, procedure.getMeasure(), newArrayList());
			};

			return simpleBrick("Model Type", emp.descriptor().fittingAlgorithm(), toRoot, idGenerator);
		}

		@Override
		public List<Brick<?, Pattern<?>>> createBricks(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			ExceptionalModelPattern emp = (ExceptionalModelPattern) pattern;

			Brick<?, Pattern<?>> typeBrick = modelTypeMorphBrick(pattern, idGenerator, emp);

			return ImmutableList.of(typeBrick);
		}

	}

	// Suppress default constructor for non-instantiability
	private BrickFactoriesExceptionalModelPattern() {
		throw new AssertionError();
	}

}
