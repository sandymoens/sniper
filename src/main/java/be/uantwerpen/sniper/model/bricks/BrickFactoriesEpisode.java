/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.collectionBrick;
import static be.uantwerpen.sniper.model.bricks.Bricks.compositeBrick;
import static be.uantwerpen.sniper.model.bricks.PatternDescriptorBrickFactories.patternDescriptorBrickFactory;
import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import be.uantwerpen.sniper.action.DescriptorActions.PropositionBasedNodeEvent;
import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.model.IdGenerator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.episodes.Episode;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRules;
import de.unibonn.realkd.patterns.episodes.Episodes;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;
import de.unibonn.realkd.patterns.graphs.Nodes;

/**
 *
 * @author Ali Doku
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BrickFactoriesEpisode {

	@PatternBrickFactoryAnn(patternClass = Episode.class)
	public static PatternBrickFactory episodeBrickFactory() {
		return new EpisodeBrickFactory();
	}

	@PatternDescriptorBrickFactoryAnn(patternDescriptorClass = EpisodeDescriptor.class)
	public static PatternDescriptorBrickFactory episodeDescriptorBrickFactory() {
		return new EpisodeDescriptorBrickFactory();
	}

	@UtilityBricksFactoryAnn(patternClass = Episode.class)
	public static UtilityBricksFactory episodeRuleUtilityBricksFactory() {
		return new EpisodeUtilityBricksFactory();
	}

	private static class EpisodeBrickFactory implements PatternBrickFactory {

		@Override
		public Optional<Brick<PatternDescriptor, Pattern<?>>> createBrick(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			if (!Episode.class.isAssignableFrom(pattern.getClass())) {
				return Optional.empty();
			}

			Function<PatternDescriptor, Pattern<?>> toRoot = l -> Episodes.create((EpisodeDescriptor) l, newArrayList(),
					ImmutableList.of());

			return Optional.of(patternDescriptorBrickFactory(((Episode) pattern).descriptor()).get()
					.createBrick(((Episode) pattern).descriptor(), toRoot, workspace, idGenerator).get());
		}

	}

	private static class EpisodeDescriptorBrickFactory implements PatternDescriptorBrickFactory {

		@Override
		public <R> Optional<Brick<PatternDescriptor, R>> createBrick(PatternDescriptor patternDescriptor,
				Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			if (!EpisodeDescriptor.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}

			EpisodeDescriptor episodeDescriptor = (EpisodeDescriptor) patternDescriptor;

			Optional<PatternDescriptorBrickFactory> patternDescriptorBrickFactory = PatternDescriptorBrickFactories
					.patternDescriptorBrickFactory(episodeDescriptor.graph());

			if (!patternDescriptorBrickFactory.isPresent()) {
				return Optional.empty();
			}

			Function<PatternDescriptor, EpisodeDescriptor> f = l -> EpisodeDescriptors.create(
					episodeDescriptor.propositionalContext(), episodeDescriptor.windowSize(), (GraphDescriptor) l);

			Optional<Brick<PatternDescriptor, R>> brick = patternDescriptorBrickFactory.get()
					.createBrick(episodeDescriptor.graph(), f.andThen(toRoot), workspace, idGenerator);

			if (!brick.isPresent()) {
				return Optional.empty();
			}

			return Optional.of(compositeBrick(patternDescriptor, ImmutableList.of(brick.get()), idGenerator));
		}

	}

	private static class EpisodeUtilityBricksFactory implements UtilityBricksFactory {

		private static interface ObjectListDescriptor<T> {

			public <R> Optional<ObjectListDescriptor<?>> getSpecialization(R object);

			public List<T> objects();

			public Class<? super T> getObjectClass();

		}

		private static class EmptyVoidListDescriptor implements ObjectListDescriptor<Void> {

			public EmptyVoidListDescriptor() {
			}

			@Override
			@SuppressWarnings("unchecked")
			public <R> Optional<ObjectListDescriptor<?>> getSpecialization(R object) {
				if (object instanceof Integer) {
					if (!object.equals(-1)) {
						return Optional.empty();
					}

					return Optional.of(new TypedObjectListDescriptor<>(newArrayList(), Proposition.class));
				}

				Class<? super R> typedClass = (Class<? super R>) object.getClass();

				if (EmptyProposition.class.isAssignableFrom(typedClass)) {
					return Optional
							.of(new TypedObjectListDescriptor<>(newArrayList(), (Class<? super R>) Proposition.class));
				}

				return Optional.of(new TypedObjectListDescriptor<>(newArrayList(object), typedClass));
			}

			@Override
			public List<Void> objects() {
				return ImmutableList.of();
			}

			@Override
			public Class<? super Void> getObjectClass() {
				return Void.class;
			}

		}

		public static class TypedObjectListDescriptor<T> implements ObjectListDescriptor<T> {

			private Class<? super T> theClass;
			private List<T> objects;

			public TypedObjectListDescriptor(List<T> objects, Class<? super T> theClass) {
				this.theClass = theClass;
				this.objects = objects;
			}

			@Override
			public <R> Optional<ObjectListDescriptor<?>> getSpecialization(R object) {
				if (!this.theClass.isInstance(object)) {
					return Optional.empty();
				}

				List<R> newObjects = newArrayList();

				newObjects.addAll(this.objects.stream().map(o -> {
					@SuppressWarnings("unchecked")
					R r = (R) o;
					return r;
				}).collect(Collectors.toList()));
				newObjects.add(object);

				@SuppressWarnings("unchecked")
				Class<? super R> typedClass = (Class<? super R>) object.getClass();

				return Optional.of(new TypedObjectListDescriptor<>(newObjects, typedClass));
			}

			@Override
			public List<T> objects() {
				return this.objects;
			}

			@Override
			public Class<? super T> getObjectClass() {
				return this.theClass;
			}

		}

		private static Pattern<?> morpthToEpisodeRulePattern(Workspace workspace, List<?> objects,
				EpisodeDescriptor episodeDescriptor, boolean reverse) {

			int maxId = episodeDescriptor.graph().nodes().stream().mapToInt(n -> n.id()).max().getAsInt();

			List<Node> nodeDescriptors = IntStream.range(0, objects.size())
					.filter(i -> !EmptyProposition.class
							.isAssignableFrom((((PropositionBasedNodeEvent) objects.get(i)).proposition).getClass()))
					.mapToObj(
							i -> Nodes.create(maxId + i + 1, ((PropositionBasedNodeEvent) objects.get(i)).proposition))
					.collect(Collectors.toList());

			GraphDescriptor antecedent;
			GraphDescriptor consequent;

			if (reverse) {
				antecedent = GraphDescriptors.create(episodeDescriptor.graph().nodes(),
						episodeDescriptor.graph().edges());

				List<Node> nodes = Lists.newArrayList(episodeDescriptor.graph().nodes());
				nodes.addAll(nodeDescriptors);

				consequent = GraphDescriptors.create(nodes, episodeDescriptor.graph().edges());
			} else {
				antecedent = GraphDescriptors.create(nodeDescriptors, newArrayList());

				List<Node> nodes = Lists.newArrayList(episodeDescriptor.graph().nodes());
				nodes.addAll(nodeDescriptors);

				consequent = GraphDescriptors.create(nodes, episodeDescriptor.graph().edges());
			}

			EpisodeRuleDescriptor ruleDescriptor = EpisodeRuleDescriptors.create(
					episodeDescriptor.propositionalContext(), episodeDescriptor.windowSize(), antecedent, consequent);

			return EpisodeRules.create(ruleDescriptor, newArrayList(), newArrayList());
		}

		public static Brick<ObjectListDescriptor<?>, Pattern<?>> episodeRuleMorphingBrick(Workspace workspace,
				Episode episode, boolean reverse, IdGenerator idGenerator) {

			final EpisodeDescriptor episodeDescriptor = episode.descriptor();

			Function<ObjectListDescriptor<?>, Pattern<?>> fl = l -> {

				if (PropositionBasedNodeEvent.class.isAssignableFrom(l.getObjectClass())) {
					return morpthToEpisodeRulePattern(workspace, l.objects(), episodeDescriptor, reverse);
				}

				return episode;
			};

			BiFunction<ObjectListDescriptor<?>, ?, Optional<ObjectListDescriptor<?>>> adder = (descr, o) -> descr
					.getSpecialization(o);

			return collectionBrick(new EmptyVoidListDescriptor(), adder, null, fl, Object.class, idGenerator);
		}

		@Override
		public List<Brick<?, Pattern<?>>> createBricks(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			Brick<ObjectListDescriptor<?>, Pattern<?>> lBrick = episodeRuleMorphingBrick(workspace, (Episode) pattern,
					false, idGenerator);

			Brick<ObjectListDescriptor<?>, Pattern<?>> rBrick = episodeRuleMorphingBrick(workspace, (Episode) pattern,
					true, idGenerator);

			return ImmutableList.of(lBrick, rBrick);
		}

	}

	// Suppress default constructor for non-instantiability
	private BrickFactoriesEpisode() {
		throw new AssertionError();
	}

}
