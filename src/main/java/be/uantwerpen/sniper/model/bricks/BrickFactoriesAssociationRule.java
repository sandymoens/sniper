/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.compositeBrick;
import static be.uantwerpen.sniper.model.bricks.PatternDescriptorBrickFactories.patternDescriptorBrickFactory;
import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.model.IdGenerator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.rules.AssociationRule;
import de.unibonn.realkd.patterns.rules.AssociationRules;
import de.unibonn.realkd.patterns.rules.DefaultRuleDescriptor;
import de.unibonn.realkd.patterns.rules.RuleDescriptor;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BrickFactoriesAssociationRule {

	@PatternBrickFactoryAnn(patternClass = AssociationRule.class)
	public static PatternBrickFactory associationRuleBrickFactory() {
		return new AssociationRuleBrickFactory();
	}

	@PatternDescriptorBrickFactoryAnn(patternDescriptorClass = RuleDescriptor.class)
	public static PatternDescriptorBrickFactory ruleDescriptorBrickFactory() {
		return new RuleDescriptorBrickFactory();
	}

	@UtilityBricksFactoryAnn(patternClass = AssociationRule.class)
	public static UtilityBricksFactory associationRuleUtilityBricksFactory() {
		return new AssociationRuleUtilityBricksFactory();
	}

	private static class AssociationRuleBrickFactory implements PatternBrickFactory {

		@Override
		public Optional<Brick<PatternDescriptor, Pattern<?>>> createBrick(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {

			if (!AssociationRule.class.isAssignableFrom(pattern.getClass())) {
				return Optional.empty();
			}

			Function<PatternDescriptor, Pattern<?>> toRoot = l -> AssociationRules.create((RuleDescriptor) l,
					newArrayList());

			return Optional.of(patternDescriptorBrickFactory(((AssociationRule) pattern).descriptor()).get()
					.createBrick(((AssociationRule) pattern).descriptor(), toRoot, workspace, idGenerator).get());
		}

	}

	private static class RuleDescriptorBrickFactory implements PatternDescriptorBrickFactory {

		@Override
		public <R> Optional<Brick<PatternDescriptor, R>> createBrick(PatternDescriptor patternDescriptor,
				Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			if (!RuleDescriptor.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}

			RuleDescriptor ruleDescriptor = (RuleDescriptor) patternDescriptor;

			Optional<PatternDescriptorBrickFactory> patternDescriptorBrickFactory = PatternDescriptorBrickFactories
					.patternDescriptorBrickFactory(ruleDescriptor.getAntecedent());

			if (!patternDescriptorBrickFactory.isPresent()) {
				return Optional.empty();
			}

			Function<PatternDescriptor, RuleDescriptor> fa = l -> DefaultRuleDescriptor
					.create(ruleDescriptor.population(), (LogicalDescriptor) l, ruleDescriptor.getConsequent());

			Optional<Brick<PatternDescriptor, R>> aBrick = patternDescriptorBrickFactory.get()
					.createBrick(ruleDescriptor.getAntecedent(), fa.andThen(toRoot), workspace, idGenerator);

			Function<PatternDescriptor, RuleDescriptor> fc = l -> DefaultRuleDescriptor
					.create(ruleDescriptor.population(), ruleDescriptor.getAntecedent(), (LogicalDescriptor) l);

			Optional<Brick<PatternDescriptor, R>> cBrick = patternDescriptorBrickFactory.get()
					.createBrick(ruleDescriptor.getConsequent(), fc.andThen(toRoot), workspace, idGenerator);

			if (!aBrick.isPresent() || !cBrick.isPresent()) {
				return Optional.empty();
			}

			return Optional
					.of(compositeBrick(patternDescriptor, ImmutableList.of(aBrick.get(), cBrick.get()), idGenerator));
		}

	}

	private static class AssociationRuleUtilityBricksFactory implements UtilityBricksFactory {

		@Override
		public List<Brick<?, Pattern<?>>> createBricks(Pattern<?> pattern, Workspace workspace,
				IdGenerator idGenerator) {
			return ImmutableList.of();
		}

	}

	// Suppress default constructor for non-instantiability
	private BrickFactoriesAssociationRule() {
		throw new AssertionError();
	}

}
