/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.collectionBrick;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import be.uantwerpen.sniper.action.DescriptorActions.EdgeEvent;
import be.uantwerpen.sniper.action.DescriptorActions.Event;
import be.uantwerpen.sniper.action.DescriptorActions.IdBasedNodeEvent;
import be.uantwerpen.sniper.action.DescriptorActions.PropositionBasedNodeEvent;
import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.model.IdGenerator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.graphs.Edges;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;

/**
 *
 *
 * @author ali
 * @since
 * @version
 */
public class BrickFactoriesGraph {

	@PatternDescriptorBrickFactoryAnn(patternDescriptorClass = GraphDescriptor.class)
	public static PatternDescriptorBrickFactory GraphDescriptorBrickFactory() {
		return new GraphDescriptorBrickFactory();
	}

	private static class GraphDescriptorBrickFactory implements PatternDescriptorBrickFactory {

		@Override
		public <R> Optional<Brick<PatternDescriptor, R>> createBrick(PatternDescriptor patternDescriptor,
				Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			if (!GraphDescriptor.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}

			BiFunction<PatternDescriptor, Event, Optional<PatternDescriptor>> adder = (descr, p) -> {
				if (EdgeEvent.class.isAssignableFrom(p.getClass())) {
					EdgeEvent ee = (EdgeEvent) p;
					return Optional.of(((GraphDescriptor) descr).specialization(Edges.create(ee.start, ee.end)));
				} else if (PropositionBasedNodeEvent.class.isAssignableFrom(p.getClass())) {
					if (EmptyProposition.class
							.isAssignableFrom((((PropositionBasedNodeEvent) p).proposition).getClass())) {
						return Optional.empty();
					}
					return Optional
							.of(((GraphDescriptor) descr).specialization(((PropositionBasedNodeEvent) p).proposition));
				}

				return Optional.empty();
			};

			BiFunction<PatternDescriptor, Event, Optional<PatternDescriptor>> remover = (descr, p) -> {
				if (EdgeEvent.class.isAssignableFrom(p.getClass())) {
					EdgeEvent ee = (EdgeEvent) p;
					return Optional.of(((GraphDescriptor) descr).generalization(Edges.create(ee.start, ee.end)));
				} else if (IdBasedNodeEvent.class.isAssignableFrom(p.getClass())) {
					return Optional.of(((GraphDescriptor) descr).generalization(((IdBasedNodeEvent) p).id));
				}
				return Optional.empty();
			};

			return Optional.of(collectionBrick(patternDescriptor, adder, remover, toRoot, Event.class, idGenerator));

		}

	}

	// Suppress default constructor for non-instantiability
	private void BrickFactoriesLogical() {
		throw new AssertionError();
	}

}
