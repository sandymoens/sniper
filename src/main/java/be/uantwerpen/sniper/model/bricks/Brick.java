/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Wrapper that allows polymorphic copy-mutation of wrapped immutable objects.
 *
 * @param<T> the
 *               type of object that is wrapped by brick
 * @param<R> the
 *               type of the root element that sits at top of brick structure
 * 
 * @author Mario Boley
 * @author Sandy Moens
 * @since 0.1.0
 * @version 0.3.0
 */
public interface Brick<T, R> {

	public String id();

	/**
	 * 
	 * @return object wrapped by this brick
	 */
	public T content();

	/**
	 * Computes new root element that is exactly the same as the current brick.
	 * 
	 * @return optional with new root element or empty
	 **/
	public default Optional<R> copyOfRootElement() {
		return Optional.empty();
	}

	/**
	 * Computes a new root element based on adding an element to this brick; if this
	 * operation is possible. Returns an empty optional otherwise.
	 * 
	 * @param element
	 *            the element that should be added to this brick
	 * @return optional with new root element or empty
	 */
	public default Optional<R> add(Object element) {
		return Optional.empty();
	}

	/**
	 * Computes a new root element based on adding a list of elements to this brick;
	 * if this operation is possible. Returns an empty optional otherwise.
	 * 
	 * @param elements
	 *            the list of elements that should be added to this brick
	 * @return optional with new root element or empty
	 */
	public default Optional<R> addAll(List<? extends Object> elements) {
		return Optional.empty();
	}

	/**
	 * Computes a new root element based on removing an element to this brick; if
	 * this operation is possible. Returns an empty optional otherwise.
	 * 
	 * @param element
	 *            the element that should be removed to this brick
	 * @return optional with new root element or empty
	 */
	public default Optional<R> remove(Object element) {
		return Optional.empty();
	}

	/**
	 * Computes a new root element by replacing all elements in this brick; if this
	 * operation is possible. Returns an empty optional otherwise.
	 * 
	 * @param elements
	 *            the list element that should be replace the current elements in
	 *            this brick
	 * @return optional with new root element or empty
	 */
	public default Optional<R> replace(List<? extends Object> elements) {
		return Optional.empty();
	}

	/**
	 * @return all child bricks that this brick is composed of
	 */
	public default List<Brick<?, R>> parts() {
		return ImmutableList.of();
	}

	/**
	 * 
	 * @return all child bricks as well as recursively children of children
	 */
	public default List<Brick<?, R>> allParts() {
		List<Brick<?, R>> result = Lists.newArrayList(parts());
		parts().forEach(p -> {
			result.addAll(p.allParts());
		});
		return result;
	}

}
