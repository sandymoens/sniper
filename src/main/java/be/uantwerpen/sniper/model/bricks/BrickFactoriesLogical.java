/*

			Function<Set<Attribute<?>>, BinaryAttributeSetRelation> fa = l -> FunctionalPatterns
					.binrayAttributeSetRelation(binaryAttributeSetRelation.table(), l,
							binaryAttributeSetRelation.coDomain()); * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.model.bricks;

import static be.uantwerpen.sniper.model.bricks.Bricks.collectionBrick;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.model.IdGenerator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BrickFactoriesLogical {

	@PatternDescriptorBrickFactoryAnn(patternDescriptorClass = LogicalDescriptor.class)
	public static PatternDescriptorBrickFactory logicalDescriptorBrickFactory() {
		return new LogicalDescriptorBrickFactory();
	}

	private static class LogicalDescriptorBrickFactory implements PatternDescriptorBrickFactory {

		@Override
		public <R> Optional<Brick<PatternDescriptor, R>> createBrick(PatternDescriptor patternDescriptor,
				Function<PatternDescriptor, R> toRoot, Workspace workspace, IdGenerator idGenerator) {

			if (!LogicalDescriptor.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}

			BiFunction<PatternDescriptor, Proposition, Optional<PatternDescriptor>> adder = (descr, p) -> {
				if (((LogicalDescriptor) descr).elements().contains(p) || p instanceof EmptyProposition) {
					return Optional.empty();
				}
				return Optional.of(((LogicalDescriptor) descr).specialization(p));
			};

			BiFunction<PatternDescriptor, Proposition, Optional<PatternDescriptor>> remover = (descr, p) -> Optional
					.of(((LogicalDescriptor) descr).generalization(p));

			return Optional
					.of(collectionBrick(patternDescriptor, adder, remover, toRoot, Proposition.class, idGenerator));
		}

	}

	// Suppress default constructor for non-instantiability
	private BrickFactoriesLogical() {
		throw new AssertionError();
	}

}
