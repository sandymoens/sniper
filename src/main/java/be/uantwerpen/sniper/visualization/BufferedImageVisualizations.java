/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.visualization;

import java.awt.image.BufferedImage;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BufferedImageVisualizations {

	public static <X> BufferedImageVisualization<X> realKDBufferedImageVisualization(String caption,
			de.unibonn.realkd.visualization.Visualization<X> visualization) {
		return new RealKDPatternBufferedImageVisualization<>(caption, visualization);
	}

	public static class RealKDPatternBufferedImageVisualization<X> implements BufferedImageVisualization<X> {

		private String caption;

		private de.unibonn.realkd.visualization.Visualization<X> visualization;

		public RealKDPatternBufferedImageVisualization(String caption,
				de.unibonn.realkd.visualization.Visualization<X> visualization) {
			this.caption = caption;
			this.visualization = visualization;
		}

		@Override
		public boolean isApplicable(X object) {
			return this.visualization.isApplicable(object);
		}

		@Override
		public String caption() {
			return this.caption;
		}

		@Override
		public BufferedImage getVisualizationData(X object) {
			return this.visualization.getBufferedImage(object, 1200, 480);
		}

	}

	// Suppress default constructor for non-instantiability
	private BufferedImageVisualizations() {
		throw new AssertionError();
	}

}
