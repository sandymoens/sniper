/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.visualization.pattern;

import static be.uantwerpen.sniper.visualization.BufferedImageVisualizations.realKDBufferedImageVisualization;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import be.uantwerpen.sniper.visualization.Visualization;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.visualization.pattern.ParallelCoordinatesReferencedAttributes;
import de.unibonn.realkd.visualization.pattern.RegisteredPatternVisualization;

/**
 * 
 * @author Joey de Pauw
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternVisualizations {

	public static List<Visualization<Pattern<?>, ?>> PATTERN_VISUALIZATIONS;

	static {
		List<Visualization<Pattern<?>, ?>> visualizations = Lists.newArrayList();

		for (RegisteredPatternVisualization visualization : RegisteredPatternVisualization.values()) {
			String caption = visualization.name();
			caption = caption.substring(0, 1) + caption.substring(1).toLowerCase().replace("_", " ");
			visualizations.add(realKDBufferedImageVisualization(caption, visualization));
		}

		visualizations.add(realKDBufferedImageVisualization("Parallel coordinates referenced attributes",
				new ParallelCoordinatesReferencedAttributes()));

		visualizations.add(new CircularPatternDissectionVisualization());

		PATTERN_VISUALIZATIONS = ImmutableList.copyOf(visualizations);
	}

}
