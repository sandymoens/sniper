/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.visualization.pattern;

import static de.unibonn.realkd.patterns.logical.LogicalDescriptors.create;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import be.uantwerpen.sniper.visualization.JsVisualization;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.Frequency;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 *
 * @author Joey de Pauw
 * @since 0.2.0
 * @version 0.2.0
 */
public class CircularPatternDissectionVisualization implements JsVisualization<Pattern<?>> {

	@Override
	public boolean isApplicable(Pattern<?> pattern) {
		if (Association.class.isAssignableFrom(pattern.getClass())) {
			return true;
		}
		return false;
	}

	@Override
	public String caption() {
		return "Circular pattern dissection";
	}

	/**
	 * Calculate the data required for this visualization. This is an example:
	 * 
	 * <pre>
	 * {
	        "items": [
	            {"id": 1, "label": "AAAAA"},
	            {"id": 2, "label": "B"},
	            {"id": 3, "label": "C"}
	        ],
	        "itemsets": [
	            {"items": [1], "support": 0.9},
	            {"items": [2], "support": 0.7},
	            {"items": [3], "support": 0.6},
	            {"items": [1, 2], "support": 0.6},
	            {"items": [2, 3], "support": 0.2},
	            {"items": [1, 3], "support": 0.5},
	            {"items": [1, 2, 3], "support": 0.15}
	        ]
	    }
	 * </pre>
	 */
	@Override
	public Map<String, Object> getVisualizationData(Pattern<?> pattern) {
		Association itemset = (Association) pattern;

		List<Map<String, Object>> items = new ArrayList<>();
		for (Proposition item : itemset.descriptor().elements()) {
			// make mutable map
			Map<String, Object> itemData = Maps.newHashMap(ImmutableMap.of("id", item.name(), "label", item.name()));
			items.add(itemData);
		}

		List<Map<String, Object>> itemsets = new ArrayList<>();
		for (LogicalDescriptor subset : powerset(itemset.descriptor())) {
			if (subset.isEmpty())
				continue;
			double frequency = Frequency.FREQUENCY.perform(subset).value();
			List<String> itemNames = subset.getElementsAsStringList();
			itemsets.add(ImmutableMap.of("items", itemNames, "support", frequency));
		}

		Map<String, Object> data = ImmutableMap.of("items", items, "itemsets", itemsets);

		return data;
	}

	private static Collection<LogicalDescriptor> powerset(LogicalDescriptor originalSet) {
		Collection<LogicalDescriptor> sets = new HashSet<>();
		if (originalSet.isEmpty()) {
			sets.add(create(originalSet.population(), Lists.newArrayList()));
			return sets;
		}
		List<Proposition> list = new ArrayList<>(originalSet.elements());
		Proposition head = list.get(0);
		LogicalDescriptor rest = create(originalSet.population(), list.subList(1, list.size()));
		for (LogicalDescriptor set : powerset(rest)) {
			LogicalDescriptor newSet = set.specialization(head);
			sets.add(newSet);
			sets.add(set);
		}
		return sets;
	}

}
