/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.visualization;

/**
 * 
 * @author Joey de Pauw
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public interface Visualization<X, T> {

	/**
	 * Tests if a the object is applicable for the target visualization
	 * 
	 * @param object the object that should be visualized
	 * @return true if the object can be visualized, otherwise false
	 */
	public boolean isApplicable(X object);

	/**
	 * Get the type of visualization.
	 * 
	 * @return The type of the visualization.
	 */
	public String caption();

	/**
	 * Get the data of the visualization. This can be any object, but should be
	 * refined by the instance.
	 * 
	 * @param object the object that should be visualized
	 * @return The data for the visualization
	 */
	public T getVisualizationData(X object);

}
