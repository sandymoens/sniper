/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.core;

import static be.uantwerpen.sniper.core.PropositionPatternExtensionComputers.newSinglePropositionMultiMeasuresPatternExtensionComputer;
import static be.uantwerpen.sniper.core.PropositionPatternExtensionComputers.newSinglePropositionPatternExtensionComputer;
import static java.lang.Double.isNaN;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Doubles;

import be.uantwerpen.sniper.ActiveWorksheet;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternExtensionComputers {

	public static PatternExtensionComputer<PatternExtensionsKey> newCachingPatternExtensionComputer() {
		return new CachingPatternExtensionComputer();
	}

	public static PatternExtensionComputer<PatternExtensionsKeySingleMeasure> newSortingPatternExtensionComputer(
			PatternExtensionComputer<PatternExtensionsKeySingleMeasure> patternExtensionComputer) {
		return new SortingPatternExtensionComputer(patternExtensionComputer);
	}

	private static class CachingPatternExtensionComputer implements PatternExtensionComputer<PatternExtensionsKey> {

		private Cache<Object, List<? extends PatternExtension>> cache = CacheBuilder.newBuilder()
				.expireAfterAccess(5, TimeUnit.MINUTES)
				.build(new CacheLoader<Object, List<? extends PatternExtension>>() {

					@Override
					public List<PatternExtension> load(Object key) throws Exception {
						return null;
					}

				});

		private CachingPatternExtensionComputer() {
		}

		@Override
		public String name() {
			return "Cached";
		}

		@Override
		public List<? extends PatternExtension> computeExtensions(ActiveWorksheet activeWorksheet,
				PatternExtensionsKey key) {
			if (!PatternExtensionsKeySingleMeasure.class.isAssignableFrom(key.getClass())
					&& !PatternExtensionsKeyMultiMeasures.class.isAssignableFrom(key.getClass())) {
				return ImmutableList.of();
			}

			try {
				return this.cache.get(key, new Callable<List<? extends PatternExtension>>() {

					@Override
					public List<? extends PatternExtension> call() throws Exception {
						if (PatternExtensionsKeySingleMeasure.class.isAssignableFrom(key.getClass())) {
							return newSortingPatternExtensionComputer(newSinglePropositionPatternExtensionComputer())
									.computeExtensions(activeWorksheet, (PatternExtensionsKeySingleMeasure) key);
						} else if (PatternExtensionsKeyMultiMeasures.class.isAssignableFrom(key.getClass())) {
							return newSinglePropositionMultiMeasuresPatternExtensionComputer()
									.computeExtensions(activeWorksheet, (PatternExtensionsKeyMultiMeasures) key);
						}
						return ImmutableList.of();
					}

				});
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

			return ImmutableList.of();
		}

	}

	private static class SortingPatternExtensionComputer
			implements PatternExtensionComputer<PatternExtensionsKeySingleMeasure> {

		private final static class EmptyThenRankThenExtensionStringComparator implements Comparator<PatternExtension> {

			@Override
			public int compare(PatternExtension o1, PatternExtension o2) {
				if (EmptyPatternExtension.class.isAssignableFrom(o1.getClass())) {
					return 1;
				} else if (EmptyPatternExtension.class.isAssignableFrom(o2.getClass())) {
					return -1;
				}

				double r1 = o1.values().get(0);
				double r2 = o2.values().get(0);
				if (r1 == r2 || (isNaN(r1) && isNaN(r2))) {
					return o1.extensionString().compareTo(o2.extensionString());
				}
				if (isNaN(r1)) {
					return -1;
				}
				if (isNaN(r2)) {
					return 1;
				}
				return Doubles.compare(r1, r2);
			}

		}

		private static EmptyThenRankThenExtensionStringComparator comparator = new EmptyThenRankThenExtensionStringComparator();

		private PatternExtensionComputer<PatternExtensionsKeySingleMeasure> patternExtensionComputer;

		private SortingPatternExtensionComputer(
				PatternExtensionComputer<PatternExtensionsKeySingleMeasure> patternExtensionComputer) {
			this.patternExtensionComputer = patternExtensionComputer;
		}

		@Override
		public String name() {
			return "Sorting" + this.patternExtensionComputer.name();
		}

		@Override
		public List<? extends PatternExtension> computeExtensions(ActiveWorksheet activeWorksheet,
				PatternExtensionsKeySingleMeasure patternExtensionsKey) {
			List<? extends PatternExtension> extensions = this.patternExtensionComputer
					.computeExtensions(activeWorksheet, patternExtensionsKey);

			Collections.sort(extensions, comparator);
			Collections.reverse(extensions);

			return extensions;
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternExtensionComputers() {
		throw new AssertionError();
	}

}
