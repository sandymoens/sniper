/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.core;

import java.util.List;
import java.util.Objects;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternExtensionsKeys {

	public static PatternExtensionsKeySingleMeasure newPatternExtensionsKeySingleMeasure(PatternDescriptor patternDescriptor,
			String part, Measure measure, boolean addEmpty) {
		return new DefaultPatternExtensionsKeySingleMeasure(patternDescriptor, part, measure, addEmpty);
	}

	public static PatternExtensionsKeyMultiMeasures newPatternExtensionsKeyMultiMeasures(
			PatternDescriptor patternDescriptor, String part, List<Measure> measures, boolean addEmpty) {
		return new DefaultPatternExtensionsKeyMultiMeasures(patternDescriptor, part, measures, addEmpty);
	}

	private static class DefaultPatternExtensionsKeySingleMeasure implements PatternExtensionsKeySingleMeasure {

		private final PatternDescriptor patternDescriptor;

		private final String part;

		private final Measure measure;

		private final boolean addEmpty;

		private DefaultPatternExtensionsKeySingleMeasure(PatternDescriptor patternDescriptor, String part, Measure measure,
				boolean addEmpty) {
			this.patternDescriptor = patternDescriptor;
			this.part = part;
			this.measure = measure;
			this.addEmpty = addEmpty;
		}

		@Override
		public PatternDescriptor patternDescriptor() {
			return this.patternDescriptor;
		}

		@Override
		public String part() {
			return this.part;
		}

		@Override
		public boolean addEmpty() {
			return this.addEmpty;
		}

		@Override
		public Measure measure() {
			return this.measure;
		}

		@Override
		public int hashCode() {
			return Objects.hash(this.patternDescriptor, this.part, this.addEmpty, this.measure);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DefaultPatternExtensionsKeySingleMeasure other = (DefaultPatternExtensionsKeySingleMeasure) obj;

			return Objects.equals(this.patternDescriptor, other.patternDescriptor)
					&& Objects.equals(this.part, other.part) && Objects.equals(this.addEmpty, other.addEmpty)
					&& Objects.equals(this.measure, other.measure);
		}

	}

	private static class DefaultPatternExtensionsKeyMultiMeasures implements PatternExtensionsKeyMultiMeasures {

		private final PatternDescriptor patternDescriptor;

		private final String part;

		private final List<Measure> measures;

		private final boolean addEmpty;

		private DefaultPatternExtensionsKeyMultiMeasures(PatternDescriptor patternDescriptor, String part,
				List<Measure> measures, boolean addEmpty) {
			this.patternDescriptor = patternDescriptor;
			this.part = part;
			this.measures = measures;
			this.addEmpty = addEmpty;
		}

		@Override
		public PatternDescriptor patternDescriptor() {
			return this.patternDescriptor;
		}

		@Override
		public String part() {
			return this.part;
		}

		@Override
		public List<Measure> measures() {
			return this.measures;
		}

		@Override
		public boolean addEmpty() {
			return this.addEmpty;
		}

		@Override
		public int hashCode() {
			return Objects.hash(this.patternDescriptor, this.part, this.measures, this.addEmpty);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DefaultPatternExtensionsKeyMultiMeasures other = (DefaultPatternExtensionsKeyMultiMeasures) obj;
			return Objects.equals(this.patternDescriptor, other.patternDescriptor)
					&& Objects.equals(this.part, other.part) && Objects.equals(this.addEmpty, other.addEmpty)
					&& Objects.equals(this.measures, other.measures);
		}
	}

	// Suppress default constructor for non-instantiability
	private PatternExtensionsKeys() {
		throw new AssertionError();
	}

}
