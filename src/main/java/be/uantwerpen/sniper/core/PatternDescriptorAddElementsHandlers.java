/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.core;

import static com.google.common.collect.Sets.newHashSet;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptors;
import de.unibonn.realkd.patterns.graphs.Edge;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;
import de.unibonn.realkd.patterns.graphs.Nodes;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.rules.DefaultRuleDescriptor;
import de.unibonn.realkd.patterns.rules.RuleDescriptor;
import de.unibonn.realkd.patterns.sequence.DefaultSequenceDescriptor;
import de.unibonn.realkd.patterns.sequence.SequenceDescriptor;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroups;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternDescriptorAddElementsHandlers {

	@PatternDescriptorAddElementsHandlerAnn(patternDescriptorClass = LogicalDescriptor.class)
	public static PatternDescriptorAddElementsHandler logicalDescriptorAddElements() {
		return new LogicalDescriptorAddElements();
	}

	@PatternDescriptorAddElementsHandlerAnn(patternDescriptorClass = RuleDescriptor.class)
	public static PatternDescriptorAddElementsHandler ruleDescriptorAddElements() {
		return new RuleDescriptorAddElements();
	}

	@PatternDescriptorAddElementsHandlerAnn(patternDescriptorClass = Subgroup.class)
	public static PatternDescriptorAddElementsHandler subgroupAddElements() {
		return new SubgroupAddElements();
	}

	@PatternDescriptorAddElementsHandlerAnn(patternDescriptorClass = SequenceDescriptor.class)
	public static PatternDescriptorAddElementsHandler sequenceDescriptorAddElements() {
		return new SequenceDescriptorAddElements();
	}

	@PatternDescriptorAddElementsHandlerAnn(patternDescriptorClass = EpisodeDescriptor.class)
	public static PatternDescriptorAddElementsHandler episodeDescriptorAddElements() {
		return new EpisodeDescriptorAddElements();
	}

	@PatternDescriptorAddElementsHandlerAnn(patternDescriptorClass = EpisodeRuleDescriptor.class)
	public static PatternDescriptorAddElementsHandler episodeRuleDescriptorAddElements() {
		return new EpisodeRuleDescriptorAddElements();
	}

	private static class LogicalDescriptorAddElements implements PatternDescriptorAddElementsHandler {

		@Override
		public Optional<PatternDescriptor> add(PatternDescriptor patternDescriptor, List<Object> elements,
				String part) {
			if (!LogicalDescriptor.class.isAssignableFrom(patternDescriptor.getClass()) || !part.equals("elements")) {
				return Optional.empty();
			}

			LogicalDescriptor logicalDescriptor = (LogicalDescriptor) patternDescriptor;

			Set<Proposition> propositions = newHashSet(logicalDescriptor.elements());

			boolean added = false;

			for (Object element : elements) {
				if (Proposition.class.isAssignableFrom(element.getClass())) {
					if (propositions.contains(element)) {
						continue;
					}
					if (!EmptyProposition.class.isAssignableFrom(element.getClass())) {
						propositions.add((Proposition) element);
					}
					added = true;
				}
			}

			if (!added) {
				return Optional.empty();
			}

			return Optional.of(LogicalDescriptors.create(logicalDescriptor.population(), propositions));
		}

	}

	private static class RuleDescriptorAddElements implements PatternDescriptorAddElementsHandler {

		@Override
		public Optional<PatternDescriptor> add(PatternDescriptor patternDescriptor, List<Object> elements,
				String part) {
			if (!RuleDescriptor.class.isAssignableFrom(patternDescriptor.getClass())
					|| !(part.equals("antecedent") || part.equals("consequent"))) {
				return Optional.empty();
			}

			RuleDescriptor ruleDescriptor = (RuleDescriptor) patternDescriptor;

			Set<Proposition> aElements = newHashSet(ruleDescriptor.getAntecedent().elements());

			Set<Proposition> cElements = newHashSet(ruleDescriptor.getConsequent().elements());

			if (containsAll(aElements, elements) || containsAll(cElements, elements)) {
				return Optional.empty();
			}

			Set<Proposition> containerToAddTo = part.equals("antecedent") ? aElements : cElements;

			boolean added = false;

			for (Object element : elements) {
				if (Proposition.class.isAssignableFrom(element.getClass())) {
					if (containerToAddTo.contains(element)) {
						continue;
					}
					if (!EmptyProposition.class.isAssignableFrom(element.getClass())) {
						containerToAddTo.add((Proposition) element);
					}
					added = true;
				}
			}

			if (!added) {
				return Optional.empty();
			}

			return Optional.of(DefaultRuleDescriptor.create(ruleDescriptor.population(),
					LogicalDescriptors.create(ruleDescriptor.population(), aElements),
					LogicalDescriptors.create(ruleDescriptor.population(), cElements)));
		}

		private boolean containsAll(Set<Proposition> aElements, List<Object> elements) {
			for (Object element : elements) {
				if (Proposition.class.isAssignableFrom(element.getClass())) {
					if (EmptyProposition.class.isAssignableFrom(element.getClass())) {
						continue;
					}
					if (!aElements.contains(element)) {
						return false;
					}
				}
			}

			return true;
		}

	}

	private static class SubgroupAddElements implements PatternDescriptorAddElementsHandler {

		@Override
		public Optional<PatternDescriptor> add(PatternDescriptor patternDescriptor, List<Object> elements,
				String part) {
			if (!Subgroup.class.isAssignableFrom(patternDescriptor.getClass()) || !part.equals("extension")) {
				return Optional.empty();
			}

			Subgroup<?> subgroup = (Subgroup<?>) patternDescriptor;

			Set<Proposition> propositions = newHashSet(subgroup.extensionDescriptor().elements());

			boolean added = false;

			for (Object element : elements) {
				if (Proposition.class.isAssignableFrom(element.getClass())) {
					if (propositions.contains(element)) {
						continue;
					}
					if (!EmptyProposition.class.isAssignableFrom(element.getClass())) {
						propositions.add((Proposition) element);
					}
					added = true;
				}
			}

			if (!added) {
				return Optional.empty();
			}

			return Optional.of(Subgroups.subgroup(LogicalDescriptors.create(subgroup.population(), propositions),
					subgroup.getTargetTable(), subgroup.targetAttributes(), subgroup.fittingAlgorithm()));
		}

	}

	private static class SequenceDescriptorAddElements implements PatternDescriptorAddElementsHandler {

		@Override
		public Optional<PatternDescriptor> add(PatternDescriptor patternDescriptor, List<Object> elements,
				String part) {
			int index = -1;

			try {
				index = Integer.parseInt(part);
			} catch (NumberFormatException e) {
				e.printStackTrace();
				return Optional.empty();
			}
			if (!SequenceDescriptor.class.isAssignableFrom(patternDescriptor.getClass())) {
				return Optional.empty();
			}

			SequenceDescriptor sequenceDescriptor = (SequenceDescriptor) patternDescriptor;

			if (sequenceDescriptor.orderedSets().size() < index + 1) {
				return Optional.empty();
			}

			List<List<Proposition>> timeSets = sequenceDescriptor.orderedSets().stream().map(t -> Lists.newArrayList(t))
					.collect(Collectors.toList());

			List<Proposition> containerToAddTo = timeSets.get(index);

			boolean added = true;

			for (Object element : elements) {
				if (Proposition.class.isAssignableFrom(element.getClass())) {
					if (!EmptyProposition.class.isAssignableFrom(element.getClass())) {
						containerToAddTo.add((Proposition) element);
					}
					added = true;
				}
			}

			if (!added) {
				return Optional.empty();
			}

			return Optional
					.of(DefaultSequenceDescriptor.create(sequenceDescriptor.sequentialPropositionalLogic(), timeSets));
		}

	}

	private static class EpisodeDescriptorAddElements implements PatternDescriptorAddElementsHandler {

		@Override
		public Optional<PatternDescriptor> add(PatternDescriptor patternDescriptor, List<Object> elements,
				String part) {
			if (!EpisodeDescriptor.class.isAssignableFrom(patternDescriptor.getClass()) || !part.equals("graph")) {
				return Optional.empty();
			}

			EpisodeDescriptor episodeDescriptor = (EpisodeDescriptor) patternDescriptor;

			List<Node> nodes = Lists.newArrayList(episodeDescriptor.graph().nodes());
			List<Edge> edges = Lists.newArrayList(episodeDescriptor.graph().edges());

			int highestId = nodes.stream().map(n -> n.id()).max(Comparator.naturalOrder()).orElse(-1);

			boolean added = false;

			for (Object element : elements) {
				if (Proposition.class.isAssignableFrom(element.getClass())) {
					if (!EmptyProposition.class.isAssignableFrom(element.getClass())) {
						nodes.add(Nodes.create(++highestId, (Proposition) element));
					}
					added = true;
				}
			}

			if (!added) {
				return Optional.empty();
			}

			return Optional.of(EpisodeDescriptors.create(episodeDescriptor.propositionalContext(),
					episodeDescriptor.windowSize(), GraphDescriptors.create(nodes, edges)));
		}

	}

	private static class EpisodeRuleDescriptorAddElements implements PatternDescriptorAddElementsHandler {

		@Override
		public Optional<PatternDescriptor> add(PatternDescriptor patternDescriptor, List<Object> elements,
				String part) {
			if (!EpisodeRuleDescriptor.class.isAssignableFrom(patternDescriptor.getClass())
					|| !(part.equals("antecedent") || part.equals("consequent"))) {
				return Optional.empty();
			}

			EpisodeRuleDescriptor episodeRuleDescriptor = (EpisodeRuleDescriptor) patternDescriptor;

			List<Node> antecedentNodes = Lists.newArrayList(episodeRuleDescriptor.antecedent().nodes());
			List<Edge> antecedentEdges = Lists.newArrayList(episodeRuleDescriptor.antecedent().edges());
			List<Node> consequentNodes = Lists.newArrayList(episodeRuleDescriptor.consequent().nodes());
			List<Edge> consequentEdges = Lists.newArrayList(episodeRuleDescriptor.consequent().edges());

			List<Node> nodes = part.equals("antecedent") ? antecedentNodes : consequentNodes;

			int highestId = nodes.stream().map(n -> n.id()).max(Comparator.naturalOrder()).orElse(-1);

			boolean added = false;

			for (Object element : elements) {
				if (Proposition.class.isAssignableFrom(element.getClass())) {
					if (!EmptyProposition.class.isAssignableFrom(element.getClass())) {
						nodes.add(Nodes.create(++highestId, (Proposition) element));
					}
					added = true;
				}
			}

			if (!added) {
				return Optional.empty();
			}

			if (part.equals("antecedent")) {
				return Optional.of(EpisodeRuleDescriptors.create(episodeRuleDescriptor.propositionalContext(),
						episodeRuleDescriptor.windowSize(), GraphDescriptors.create(nodes, antecedentEdges),
						GraphDescriptors.create(consequentNodes, consequentEdges)));
			}

			return Optional.of(EpisodeRuleDescriptors.create(episodeRuleDescriptor.propositionalContext(),
					episodeRuleDescriptor.windowSize(), GraphDescriptors.create(antecedentNodes, antecedentEdges),
					GraphDescriptors.create(nodes, consequentEdges)));
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternDescriptorAddElementsHandlers() {
		throw new AssertionError();
	}

}
