/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.core;

import java.util.List;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;

/**
 * 
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternExtensions {

	public static PropositionPatternExtension newEmptyPropositionPatternExtension(PropositionalContext context,
			double score) {
		return new EmptyPropositionPatternExtension(new EmptyProposition(context), ImmutableList.of(score));
	}

	public static PropositionPatternExtension newPropositionPatternExtension(Proposition proposition, double score) {
		return new DefaultPropositionPatternExtension(proposition, ImmutableList.of(score));
	}

	public static PropositionPatternExtension newEmptyPropositionPatternExtension(PropositionalContext context,
			List<Double> values) {
		return new EmptyPropositionPatternExtension(new EmptyProposition(context), values);
	}

	public static PropositionPatternExtension newPropositionPatternExtension(Proposition proposition,
			List<Double> values) {
		return new DefaultPropositionPatternExtension(proposition, values);
	}

	public static class EmptyPropositionPatternExtension implements EmptyPatternExtension, PropositionPatternExtension {

		private Proposition proposition;
		private List<Double> values;

		private EmptyPropositionPatternExtension(Proposition proposition, List<Double> values) {
			this.proposition = proposition;
			this.values = values;
		}

		@Override
		public Proposition proposition() {
			return this.proposition;
		}

		@Override
		public List<Double> values() {
			return this.values;
		}

		@Override
		public String extensionString() {
			return this.proposition.name();
		}

	}

	public static class DefaultPropositionPatternExtension implements PropositionPatternExtension {

		private Proposition proposition;
		private List<Double> values;

		private DefaultPropositionPatternExtension(Proposition proposition, List<Double> values) {
			this.proposition = proposition;
			this.values = values;
		}

		@Override
		public Proposition proposition() {
			return this.proposition;
		}

		@Override
		public List<Double> values() {
			return this.values;
		}

		@Override
		public String extensionString() {
			return this.proposition.toString();
		}

	}

}
