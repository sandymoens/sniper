/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.core;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.stream.Collectors;

import be.uantwerpen.sniper.model.BrickModel;
import be.uantwerpen.sniper.model.MeasureModels.ObservableMeasureModel;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class MeasureComputerDaemons {

	public static MeasureComputerDaemon newMeasureComputerDaemon(BrickModel brickModel,
			ObservableMeasureModel measureModel) {
		return new DefaultMeasureComputerDaemon(brickModel, measureModel);
	}

	private static class DefaultMeasureComputerDaemon implements Observer, MeasureComputerDaemon {

		private BrickModel brickModel;

		private ObservableMeasureModel measureModel;

		private Boolean stop;

		private boolean isDone;

		private boolean isPaused;

		private Boolean shouldRestart;

		private Object sleep;

		private DefaultMeasureComputerDaemon(BrickModel brickModel, ObservableMeasureModel measureModel) {
			this.brickModel = brickModel;
			this.measureModel = measureModel;
			this.sleep = new Object();

			this.measureModel.addObserver(this);
		}

		@Override
		public void run() {
			this.stop = false;
			this.shouldRestart = true;

			do {
				this.isDone = false;

				List<Pattern<?>> patterns = newArrayList();

				if (this.shouldRestart) {
					this.shouldRestart = false;

					patterns = this.brickModel.patterns();
				}

				int end = patterns.size();

				for (int i = 0; i < end; i++) {
					if (this.isPaused || this.stop) {
						break;
					}

					Pattern<?> pattern = patterns.get(i);

					List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> procedures = this.measureModel
							.procedures().stream().filter(m -> m.isApplicable(pattern.descriptor()))
							.filter(m -> !pattern.hasMeasure(m.getMeasure())).collect(Collectors.toList());

					if (!procedures.isEmpty()) {
						Pattern<?> newPattern = pattern;

						for (MeasurementProcedure<? extends Measure, ? super PatternDescriptor> p : procedures) {
							newPattern = newPattern.add(p.perform(pattern.descriptor()));
						}

						if (newPattern != null) {
							this.brickModel.remove(i);
							this.brickModel.add(i, newPattern);
						}
					}
				}

				if (this.stop) {
					break;
				}

				if (this.shouldRestart) {
					continue;
				}

				if (!this.isPaused) {
					this.isDone = true;
				}

				try {
					do {
						synchronized (this.sleep) {
							this.sleep.wait();
						}
						if (this.stop) {
							break;
						}
					} while (this.isPaused);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			} while (!this.stop);
		}

		@Override
		public void stop() {
			this.stop = true;
		}

		@Override
		public synchronized void restart() {
			this.isPaused = false;
			this.shouldRestart = true;

			synchronized (this.sleep) {
				this.sleep.notify();
			}
		}

		@Override
		public void pause() {
			this.isPaused = true;
		}

		@Override
		public boolean isDone() {
			return this.isDone;
		}

		@Override
		public void update(Observable o, Object arg) {
			if (o.equals(this.measureModel)) {
				restart();
			}
		}

	}

	// Suppress default constructor for non-instantiability
	private MeasureComputerDaemons() {
		throw new AssertionError();
	}

}
