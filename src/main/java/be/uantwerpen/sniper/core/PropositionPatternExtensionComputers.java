/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.core;

import static be.uantwerpen.sniper.core.PatternExtensions.newEmptyPropositionPatternExtension;
import static be.uantwerpen.sniper.core.PatternExtensions.newPropositionPatternExtension;
import static be.uantwerpen.sniper.pattern.PatternDescriptorScorers.newMeasureBasedPatternDescriptorScorer;
import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.action.DescriptorActions.PropositionBasedNodeEvent;
import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.pattern.PatternDescriptorScorer;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PropositionPatternExtensionComputers {

	public static PatternExtensionComputer<PatternExtensionsKeySingleMeasure> newSinglePropositionPatternExtensionComputer() {
		return new SinglePropositionPatternExtensionComputer();
	}

	public static PatternExtensionComputer<PatternExtensionsKeyMultiMeasures> newSinglePropositionMultiMeasuresPatternExtensionComputer() {
		return new SinglePropositionMultiMeasuresPatternExtensionComputer();
	}

	private static class SinglePropositionPatternExtensionComputer
			implements PatternExtensionComputer<PatternExtensionsKeySingleMeasure> {

		@Override
		public String name() {
			return "SinglePropositionPatternExtensionComputer";
		}

		@Override
		public List<PropositionPatternExtension> computeExtensions(ActiveWorksheet activeWorksheet,
				PatternExtensionsKeySingleMeasure patternExtensionsKey) {
			PropositionalContext context = activeWorksheet.propositionalContext();

			if (context == null) {
				return newArrayList();
			}

			PatternDescriptorScorer patternScorer = newMeasureBasedPatternDescriptorScorer(
					patternExtensionsKey.measure());

			List<Proposition> propositions = newArrayList(context.propositions());

			if (patternExtensionsKey.addEmpty()) {
				propositions.add(0, new EmptyProposition(context));
			}

			PatternDescriptorAddElementsHandler adder = PatternDescriptorAddElements.newPatternDescriptorAddElements();

			PatternDescriptor patternDescriptor = patternExtensionsKey.patternDescriptor();
			String part = patternExtensionsKey.part();

			return propositions.stream().map(p -> {
				Optional<PatternDescriptor> newDescriptor = adder.add(patternDescriptor, ImmutableList.of(p), part);

				if (!newDescriptor.isPresent()) {
					return null;
				}

				return EmptyProposition.class.isAssignableFrom(p.getClass())
						? newEmptyPropositionPatternExtension(context, patternScorer.score(newDescriptor.get()))
						: newPropositionPatternExtension(p, patternScorer.score(newDescriptor.get()));
			}).filter(p -> p != null).collect(Collectors.toList());
		}

		private static Object getExtensionForPatternClass(Proposition proposition, Class<?> clazz) {
			if (EpisodeRule.class.isAssignableFrom(clazz)) {
				return new PropositionBasedNodeEvent(proposition);
			}

			return proposition;
		}

	}

	private static class SinglePropositionMultiMeasuresPatternExtensionComputer
			implements PatternExtensionComputer<PatternExtensionsKeyMultiMeasures> {

		@Override
		public String name() {
			return "SinglePropositionMultiMeasuresPatternExtensionComputer";
		}

		@Override
		public List<PropositionPatternExtension> computeExtensions(ActiveWorksheet activeWorksheet,
				PatternExtensionsKeyMultiMeasures patternExtensionsKey) {
			PropositionalContext context = activeWorksheet.propositionalContext();

			if (context == null) {
				return newArrayList();
			}

			List<PatternDescriptorScorer> scorers = patternExtensionsKey.measures().stream()
					.map(m -> newMeasureBasedPatternDescriptorScorer(m)).collect(Collectors.toList());

			List<Proposition> propositions = newArrayList(context.propositions());

			if (patternExtensionsKey.addEmpty()) {
				propositions.add(new EmptyProposition(context));
			}

			PatternDescriptorAddElementsHandler adder = PatternDescriptorAddElements.newPatternDescriptorAddElements();

			PatternDescriptor patternDescriptor = patternExtensionsKey.patternDescriptor();
			String part = patternExtensionsKey.part();

			return propositions.stream().map(p -> {
				Optional<PatternDescriptor> newDescriptor = adder.add(patternDescriptor, ImmutableList.of(p), part);

				if (!newDescriptor.isPresent()) {
					return null;
				}

				PatternDescriptor d = newDescriptor.get();

				List<Double> scores = scorers.stream().map(s -> s.score(d)).collect(Collectors.toList());

				return EmptyProposition.class.isAssignableFrom(p.getClass())
						? newEmptyPropositionPatternExtension(context, scores)
						: newPropositionPatternExtension(p, scores);
			}).filter(p -> p != null).collect(Collectors.toList());
		}

	}

	// Suppress default constructor for non-instantiability
	private PropositionPatternExtensionComputers() {
		throw new AssertionError();
	}

}
