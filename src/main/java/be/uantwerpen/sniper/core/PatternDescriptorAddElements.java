/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.core;

import static com.google.common.collect.Lists.newArrayList;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternDescriptorAddElements {

	private static List<Pair<Class<? extends PatternDescriptor>, PatternDescriptorAddElementsHandler>> handlers;

	public static void register(Class<? extends PatternDescriptor> clazz, PatternDescriptorAddElementsHandler handler) {
		handlers.add(Pair.of(clazz, handler));
	}

	static {
		handlers = newArrayList();

		Reflections reflections = new Reflections(
				new ConfigurationBuilder().addUrls(ClasspathHelper.forPackage("be.uantwerpen.sniper.core"))
						.setScanners(new MethodAnnotationsScanner()));

		reflections.getMethodsAnnotatedWith(PatternDescriptorAddElementsHandlerAnn.class).stream().forEach(m -> {
			try {
				register(m.getAnnotation(PatternDescriptorAddElementsHandlerAnn.class).patternDescriptorClass(),
						(PatternDescriptorAddElementsHandler) m.invoke(null));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}

	private static Optional<PatternDescriptorAddElementsHandler> handler(Class<? extends PatternDescriptor> clazz) {
		for (Pair<Class<? extends PatternDescriptor>, PatternDescriptorAddElementsHandler> handler : handlers) {
			if (handler.getKey().isAssignableFrom(clazz)) {
				return Optional.of(handler.getValue());
			}
		}

		return Optional.empty();
	}

	public static PatternDescriptorAddElementsHandler newPatternDescriptorAddElements() {
		return new GenericPatternDescriptorAddElementsHandler();
	}

	private static class GenericPatternDescriptorAddElementsHandler implements PatternDescriptorAddElementsHandler {

		@Override
		public Optional<PatternDescriptor> add(PatternDescriptor patternDescriptor, List<Object> elements,
				String part) {
			Optional<PatternDescriptorAddElementsHandler> handler = handler(patternDescriptor.getClass());

			if (!handler.isPresent()) {
				return Optional.empty();
			}

			return handler.get().add(patternDescriptor, elements, part);
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternDescriptorAddElements() {
		throw new AssertionError();
	}

}
