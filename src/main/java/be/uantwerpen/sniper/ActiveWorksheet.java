/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper;

import java.util.List;
import java.util.Optional;

import be.uantwerpen.sniper.action.ActionManager;
import be.uantwerpen.sniper.command.ActionBasedUndoCommand;
import be.uantwerpen.sniper.command.UndoStack;
import be.uantwerpen.sniper.core.PatternExtensionComputer;
import be.uantwerpen.sniper.core.PatternExtensionsKey;
import be.uantwerpen.sniper.dao.SchemeMetaInfo;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.model.BrickModel;
import be.uantwerpen.sniper.model.FilterBrickModel;
import be.uantwerpen.sniper.model.MeasureModel;
import be.uantwerpen.sniper.model.StatusEnabledModel;
import be.uantwerpen.sniper.model.WorksheetPropertiesModel;
import be.uantwerpen.sniper.pattern.PatternVisualizer;
import be.uantwerpen.sniper.visualization.Visualization;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public interface ActiveWorksheet {

	public List<Parameter<?>> properties();

	public Optional<Parameter<?>> property(String key);

	public WorksheetPropertiesModel propertiesModel();

	public PropositionalContext propositionalContext();

	public PatternController patternController();

	public BrickModel brickModel();

	public FilterBrickModel filterBrickModel();

	public MeasureModel measureModel();

	public StatusEnabledModel<Visualization<Pattern<?>, ?>> visualizationModel();

	public Worksheet worksheet();

	public SchemeMetaInfo schemeMetaInfo();

	public Workspace realKDWorkspace();

	public ActionManager actionManager();

	public UndoStack<ActionBasedUndoCommand> undoStack();

	public PatternVisualizer patternVisualizer();

	public PatternExtensionComputer<PatternExtensionsKey> patternExtensionComputerForPropositions();

	public String cache(Object value);

	public Optional<Object> fromCache(String key);

}
