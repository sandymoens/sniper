/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 *
 *
 * @author sandy
 * @since
 * @version
 */
@Configuration
//@EnableAuthorizationServer
public class OAuth2AuthorizationServerConfigJwt extends AuthorizationServerConfigurerAdapter {

	private static String REALM = "MY_OAUTH_REALM";

//	@Autowired
//	private UserApprovalHandler userApprovalHandler;

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}

	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		final JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey("123");
		// final KeyStoreKeyFactory keyStoreKeyFactory = new
		// KeyStoreKeyFactory(new ClassPathResource("mytest.jks"),
		// "mypass".toCharArray());
		// converter.setKeyPair(keyStoreKeyFactory.getKeyPair("mytest"));
		return converter;
	}

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

//	@Override
//	public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
//		oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
//	}

	@Override
	public void configure(final ClientDetailsServiceConfigurer clients) throws Exception { // @formatter:off
//		clients.inMemory().withClient("sampleClientId").authorizedGrantTypes("implicit")
//				.scopes("read", "write", "foo", "bar").autoApprove(false).accessTokenValiditySeconds(3600)
//				.redirectUris("http://localhost:8083/", "http://localhost:8086/").and()
//				.withClient("fooClientIdPassword").secret(passwordEncoder().encode("secret"))
//				.authorizedGrantTypes("password", "authorization_code", "refresh_token", "client_credentials")
//				.scopes("foo", "read", "write").accessTokenValiditySeconds(3600) // 1
//																					// hour
//				.refreshTokenValiditySeconds(2592000) // 30 days
//				.redirectUris("http://www.example.com", "http://localhost:8089/",
//						"http://localhost:8080/login/oauth2/code/custom",
//						"http://localhost:8080/ui-thymeleaf/login/oauth2/code/custom",
//						"http://localhost:8080/authorize/oauth2/code/bael",
//						"http://localhost:8080/login/oauth2/code/bael")
//				.and().withClient("barClientIdPassword").secret(passwordEncoder().encode("secret"))
//				.authorizedGrantTypes("password", "authorization_code", "refresh_token").scopes("bar", "read", "write")
//				.accessTokenValiditySeconds(3600) // 1 hour
//				.refreshTokenValiditySeconds(2592000) // 30 days
//				.and().withClient("testImplicitClientId").authorizedGrantTypes("implicit")
//				.scopes("read", "write", "foo", "bar").autoApprove(true).redirectUris("http://www.example.com");

		clients.inMemory().withClient("my-trusted-client")
				.authorizedGrantTypes("password", "authorization_code", "refresh_token", "implicit")
				.authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT").scopes("read", "write", "trust").secret("secret")
				.accessTokenValiditySeconds(120).// Access token is only valid
													// for 2 minutes.
				refreshTokenValiditySeconds(600);// Refresh token is only valid
													// for 10 minutes.
	} // @formatter:on

//	@Bean
//	@Primary
//	public DefaultTokenServices tokenServices() {
//		final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
//		defaultTokenServices.setTokenStore(tokenStore());
//		defaultTokenServices.setSupportRefreshToken(true);
//		return defaultTokenServices;
//	}

//	@Override
//	public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
//		final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
//		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));
//		endpoints.tokenStore(tokenStore()).tokenEnhancer(tokenEnhancerChain)
//				.authenticationManager(this.authenticationManager);
//	}

//	@Bean
//	public TokenEnhancer tokenEnhancer() {
//		return new CustomTokenEnhancer();
//	}

//	@Override
//	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
//		endpoints.tokenStore(tokenStore()).userApprovalHandler(this.userApprovalHandler)
//				.authenticationManager(this.authenticationManager);
//	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.realm(REALM + "/client");
	}

}