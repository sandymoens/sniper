/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.config;

import java.util.Properties;

/**
 * Global settings for this project.
 * 
 * @author Sandy Moens
 * @since 0.0.1
 * @version 0.2.1
 */
public class Settings {

	public static String DATA_DIR = "data_dir";
	public static String PLUGIN_DIR = "plugin_dir";
	public static String IMAGE_DIR = "img_dir";

	public static Settings Instance;

	public static void initialize(Properties properties) {
		if (Instance != null) {
			System.err.println("Warning! Settings already initialized! New settings are discarded.");
		}

		Instance = new Settings(properties);
	}

	private Properties properties;

	private Settings(Properties properties) {
		this.properties = properties;
	}

	public String getProperty(String property) {
		return this.properties.getProperty(property, "");
	}

}
