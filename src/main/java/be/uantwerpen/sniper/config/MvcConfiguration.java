/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.config;

import static be.uantwerpen.sniper.config.Settings.DATA_DIR;
import static be.uantwerpen.sniper.config.Settings.IMAGE_DIR;
import static be.uantwerpen.sniper.config.Settings.PLUGIN_DIR;

import java.nio.file.Paths;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.servlet.MultipartConfigElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import be.uantwerpen.sniper.utils.OSUtils;

/**
 * Configuration of spring beans and resources.
 * 
 * @author Sandy Moens
 * @since 0.0.1
 * @version 0.0.2
 */
@Configuration
@EnableWebMvc
@EnableSpringDataWebSupport
@PropertySource("classpath:application.properties")
@PropertySource(value = "file:${SNIPER_CONF_DIR}/application.properties", ignoreResourceNotFound = true)
@PropertySource("classpath:sniper.properties")
@PropertySource(value = "file:${SNIPER_CONF_DIR}/sniper.properties", ignoreResourceNotFound = true)
@ComponentScan(basePackages = "be.uantwerpen.sniper")
public class MvcConfiguration implements WebMvcConfigurer {

	private String dataDir() {
		return String.format("%s/data", this.env.getProperty("sniper.repo.dir", Paths.get("repo").toString()));
	}

	private String pluginDir() {
		String pluginDir = this.env.getProperty("sniper.plugin.dir", Paths.get("plugin").toString());

		switch (OSUtils.decideOperatingSystem()) {
		case LINUX:
			return String.format("%s/linux", pluginDir);
		case MACOSX:
			return String.format("%s/osx", pluginDir);
		case WINDOWS:
			return String.format("%s/windows", pluginDir);
		}

		return pluginDir;
	}

	private String imgDir() {
		return this.env.getProperty("sniper.img.dir",
				Paths.get(System.getProperty("java.io.tmpdir"), "img").toString());
	}

	@PostConstruct
	public void initializeSettings() {
		Properties properties = new Properties();

		properties.put(DATA_DIR, dataDir());
		properties.put(PLUGIN_DIR, pluginDir());
		properties.put(IMAGE_DIR, imgDir());

		Settings.initialize(properties);
	}

	@Autowired
	private Environment env;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		if (!registry.hasMappingForPattern("/js/**")) {
			registry.addResourceHandler("/js/**").addResourceLocations("/js/");
		}
		if (!registry.hasMappingForPattern("/lib/**")) {
			registry.addResourceHandler("/lib/**").addResourceLocations("/lib/");
		}
		if (!registry.hasMappingForPattern("/resources/**")) {
			registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		}
		if (!registry.hasMappingForPattern("/static/**")) {
			registry.addResourceHandler("/static/**").addResourceLocations("/static/");
		}
		if (!registry.hasMappingForPattern("/templates/**")) {
			registry.addResourceHandler("/templates/**").addResourceLocations("/templates/");
		}
		if (!registry.hasMappingForPattern("/img/**")) {
			registry.addResourceHandler("/img/**").addResourceLocations(String.format("file:%s/", imgDir()));
		}
		if (!registry.hasMappingForPattern("/webjars/**")) {
			registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
		}
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("1200800KB");
		factory.setMaxRequestSize("1200800KB");
		return factory.createMultipartConfig();
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		return new CommonsMultipartResolver();
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
//		registry.addViewController("/").setViewName("main/login");
//		registry.addViewController("/login").setViewName("main/login");

		registry.addViewController("/").setViewName("/workspaces");

		registry.addViewController("/user").setViewName("info/user");
		registry.addViewController("/dataTables").setViewName("info/dataTables");
		registry.addViewController("/schemes").setViewName("info/schemes");
		registry.addViewController("/workspaces").setViewName("info/workspaces");

		registry.addViewController("/patterns").setViewName("patterns/sniper");

		registry.addViewController("/admin/users").setViewName("admin/userAdmin");
	}

	@Bean
	public ViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver bean = new InternalResourceViewResolver();
		bean.setViewClass(JstlView.class);
		bean.setPrefix(this.env.getProperty("spring.mvc.view.prefix"));
		bean.setSuffix(this.env.getProperty("spring.mvc.view.suffix"));
		return bean;
	}

}
