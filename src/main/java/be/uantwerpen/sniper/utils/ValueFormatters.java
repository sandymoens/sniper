/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.utils;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.1.0
 * @version 0.1.0
 */
public class ValueFormatters {

	private static ValueFormatter defaultValueFormatter;

	public static ValueFormatter valueFormatter() {
		return defaultValueFormatter != null ? defaultValueFormatter : new DefaultValueFormatter();
	}

	private static class DefaultValueFormatter implements ValueFormatter {

		@Override
		public String format(ValueType valueType, double value) {
			if (Double.isNaN(value)) {
				return "NaN";
			}
			switch (valueType) {
			case FLOAT:
				return String.format("%.4f", value);
			case INTEGER:
				return String.format("%.0f", value);
			case PERCENTAGE:
				return String.format("%.2f%%", value * 100);
			case STRING:
				return String.format("%.4f", value);
			}
			return String.format("%.4f", value);
		}

	}

	// Suppress default constructor for non-instantiability
	private ValueFormatters() {
		throw new AssertionError();
	}

}
