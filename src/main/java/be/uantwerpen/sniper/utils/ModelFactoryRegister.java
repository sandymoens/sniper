/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.utils;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.models.gaussian.GaussianModelFactory;
import de.unibonn.realkd.patterns.models.mean.MetricEmpiricalDistributionFactory;
import de.unibonn.realkd.patterns.models.regression.LeastSquareRegressionModelFactory;
import de.unibonn.realkd.patterns.models.regression.TheilSenLinearRegressionModelFactory;
import de.unibonn.realkd.patterns.models.table.ContingencyTableModelFactory;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class ModelFactoryRegister {

	public final static List<ModelFactory<?>> MODEL_FACTORIES = newArrayList(

			ContingencyTableModelFactory.INSTANCE,

			GaussianModelFactory.INSTANCE,

			LeastSquareRegressionModelFactory.INSTANCE,

			MetricEmpiricalDistributionFactory.INSTANCE,

			TheilSenLinearRegressionModelFactory.INSTANCE

	);

	// Suppress default constructor for non-instantiability
	private ModelFactoryRegister() {
		throw new AssertionError();
	}

}
