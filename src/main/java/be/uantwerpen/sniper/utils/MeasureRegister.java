/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.utils;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.data.propositions.DefaultPropositionalContext;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContext;
import de.unibonn.realkd.data.sequences.TableBasedSingleSequencePropositionalContext;
import de.unibonn.realkd.patterns.Frequency;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.Support;
import de.unibonn.realkd.patterns.emm.AngularDistanceOfSlopes;
import de.unibonn.realkd.patterns.emm.HellingerDistance;
import de.unibonn.realkd.patterns.emm.KolmogorovSmirnovStatistic;
import de.unibonn.realkd.patterns.emm.ManhattenMeanDistance;
import de.unibonn.realkd.patterns.emm.NormalizedAbsoluteMeanShift;
import de.unibonn.realkd.patterns.emm.NormalizedAbsoluteMedianShift;
import de.unibonn.realkd.patterns.emm.NormalizedNegativeMeanShift;
import de.unibonn.realkd.patterns.emm.NormalizedNegativeMedianShift;
import de.unibonn.realkd.patterns.emm.NormalizedPositiveMeanShift;
import de.unibonn.realkd.patterns.emm.NormalizedPositiveMedianShift;
import de.unibonn.realkd.patterns.emm.PositiveProbabilityShift;
import de.unibonn.realkd.patterns.emm.TotalVariationDistance;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleConfidence;
import de.unibonn.realkd.patterns.episodes.EpisodeSupport;
import de.unibonn.realkd.patterns.functional.FractionOfInformation;
import de.unibonn.realkd.patterns.functional.ReliableFractionOfInformation;
import de.unibonn.realkd.patterns.logical.Area;
import de.unibonn.realkd.patterns.logical.ItemsetLeverage;
import de.unibonn.realkd.patterns.logical.Lift;
import de.unibonn.realkd.patterns.rules.Confidence;
import de.unibonn.realkd.patterns.rules.ConfidenceLift;
import de.unibonn.realkd.patterns.rules.MinimalAllConfidenceLift;
import de.unibonn.realkd.patterns.rules.MinimalConfidenceLift;
import de.unibonn.realkd.patterns.rules.OddsRatio;
import de.unibonn.realkd.patterns.rules.PhiCoefficient;
import de.unibonn.realkd.patterns.rules.RuleLift;
import de.unibonn.realkd.patterns.rules.WeightedRelativeAccuracy;
import de.unibonn.realkd.patterns.sequence.Cohesion;
import de.unibonn.realkd.patterns.sequence.Interestingness;
import de.unibonn.realkd.patterns.sequence.SequenceSupport;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.1.0
 * @version 0.2.0
 */
public class MeasureRegister {

	private static MeasureRegister INSTANCE = null;

	private static ArrayList<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> localPatternDescriptorMeasures() {
		return newArrayList(

				Support.SUPPORT,

				Frequency.FREQUENCY

		);
	}

	private static ArrayList<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> logicalDescriptorMeasures() {
		return newArrayList(

				Area.AREA,

				Lift.LIFT,

				ItemsetLeverage.ITEMSET_LEVERAGE

		);
	}

	private static ArrayList<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> ruleDescriptorMeasures() {
		return newArrayList(

				Confidence.CONFIDENCE,

				RuleLift.RULE_LIFT,

				ConfidenceLift.CONFIDENCE_LIFT,

				MinimalConfidenceLift.MINIMAL_CONFIDENCE_LIFT,

				MinimalAllConfidenceLift.MINIMAL_ALL_CONFIDENCE_LIFT,

				OddsRatio.ODDS_RATIO,

				PhiCoefficient.PHI_COEFFICIENT,

				WeightedRelativeAccuracy.WRACC

		);
	}

	private static ArrayList<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> subgroupMeasures() {
		return newArrayList(

				TotalVariationDistance.TOTAL_VARIATION_DISTANCE,

				NormalizedAbsoluteMeanShift.NORMALIZED_ABSOLUTE_MEAN_SHIFT,

				NormalizedPositiveMeanShift.NORMALIZED_POSITIVE_MEAN_SHIFT,

				NormalizedNegativeMeanShift.NORMALIZED_NEGATIVE_MEAN_SHIFT,

				NormalizedAbsoluteMedianShift.NORMALIZED_ABSOLUTE_MEDIAN_SHIFT,

				NormalizedPositiveMedianShift.NORMALIZED_POSITIVE_MEDIAN_SHIFT,

				NormalizedNegativeMedianShift.NORMALIZED_NEGATIVE_MEDIAN_SHIFT,

				AngularDistanceOfSlopes.ANGULAR_DISTANCE_OF_SLOPES,

				KolmogorovSmirnovStatistic.KOLMOGOROV_SMIRNOV_STATISTIC,

				ManhattenMeanDistance.MANHATTAN_MEAN_DISTANCE,

				PositiveProbabilityShift.POSITIVE_PROBABILITY_SHIFT,

				HellingerDistance.HELLINGER_DISTANCE

		);
	}

	private static ArrayList<MeasurementProcedure<? extends Measure, PatternDescriptor>> sequencePatternMeasurementProcedures() {
		return newArrayList(

				SequenceSupport.SEQUENCE_SUPPORT,

				Cohesion.COHESION,

				Interestingness.INTERESTINGNESS

		);
	}

	private static ArrayList<MeasurementProcedure<? extends Measure, Object>> functionalPatternMeasurementProcedures() {
		return newArrayList(

				FractionOfInformation.FRACTION_OF_INFORMATION,

				ReliableFractionOfInformation.RELIABLE_FRACTION_OF_INFORMATION

		);
	}

	private static ArrayList<MeasurementProcedure<? extends Measure, PatternDescriptor>> episodeMeasures() {
		return newArrayList(

				EpisodeSupport.EPISODE_SUPPORT

		);
	}

	private static ArrayList<MeasurementProcedure<? extends Measure, PatternDescriptor>> episodeRuleMeasures() {
		return newArrayList(

				EpisodeRuleConfidence.EPISODE_RULE_CONDIFENCE

		);
	}

	private static Set<? extends Measure> integerMeasures = ImmutableSet.of(Support.SUPPORT, Area.AREA,
			SequenceSupport.SEQUENCE_SUPPORT, EpisodeSupport.EPISODE_SUPPORT);

	private static Set<? extends Measure> percentageMeasures = ImmutableSet.of(Frequency.FREQUENCY,
			Confidence.CONFIDENCE, ConfidenceLift.CONFIDENCE_LIFT, MinimalConfidenceLift.MINIMAL_CONFIDENCE_LIFT,
			MinimalAllConfidenceLift.MINIMAL_ALL_CONFIDENCE_LIFT, EpisodeRuleConfidence.EPISODE_RULE_CONDIFENCE);

	static {
		INSTANCE = new MeasureRegister();

		localPatternDescriptorMeasures().stream().forEach(m -> INSTANCE.register(m));
		logicalDescriptorMeasures().stream().forEach(m -> INSTANCE.register(m));
		ruleDescriptorMeasures().stream().forEach(m -> INSTANCE.register(m));
		subgroupMeasures().stream().forEach(m -> INSTANCE.register(m));
		sequencePatternMeasurementProcedures().stream().forEach(m -> INSTANCE.register(m));
		functionalPatternMeasurementProcedures().stream().forEach(m -> INSTANCE.register(m));
		episodeMeasures().stream().forEach(m -> INSTANCE.register(m));
		episodeRuleMeasures().stream().forEach(m -> INSTANCE.register(m));
	}

	public static MeasureRegister measureRegister() {
		return INSTANCE;
	}

	private Map<Measure, Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>>> register;
	private List<Measure> measures;

	private MeasureRegister() {
		this.register = newHashMap();
		this.measures = newArrayList();
	}

	public void register(MeasurementProcedure<? extends Measure, ? super PatternDescriptor> measurementProcedure) {
		Measure measure = measurementProcedure.getMeasure();

		Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> values = this.register.get(measure);

		if (values == null) {
			this.register.put(measure, values = newHashSet());
			this.measures.add(measure);
		}

		values.add(measurementProcedure);
	}

	public Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> getMeasurementProcedures(
			Measure qualityMeasureId) {
		Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> elements = this.register
				.get(qualityMeasureId);

		if (elements == null) {
			return newHashSet();
		}

		Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> values = newHashSet(elements);

		return ImmutableSet.copyOf(values);
	}

	public Collection<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> getMeasurementProcedures() {
		List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> measurementProcedures = newArrayList();

		for (Measure measure : this.measures) {
			measurementProcedures.addAll(this.register.get(measure));
		}

		return measurementProcedures;
	}

	public Measure getMeasure(String measureName) {
		Measure measure = null;
		for (Measure m : this.register.keySet()) {
			if (m.toString().equals(measureName) || m.caption().toString().equals(measureName)) {
				measure = m;
				break;
			}
		}
		if (measure == null) {
			throw new IllegalArgumentException("Invalid measure name (" + measureName + ") specified");
		}
		return measure;
	}

	public ValueType measureValueType(Measure measure) {
		if (integerMeasures.contains(measure)) {
			return ValueType.INTEGER;
		} else if (percentageMeasures.contains(measure)) {
			return ValueType.PERCENTAGE;
		}
		return ValueType.FLOAT;
	}

	public Collection<Measure> getMeasures() {
		return this.measures;
	}

	public List<Measure> getMeasures(Class<? extends PropositionalContext> clazz) {
		List<Measure> measures = Lists.newArrayList();

		if (DefaultPropositionalContext.class.isAssignableFrom(clazz)) {
			measures.addAll(localPatternDescriptorMeasures().stream().map(p -> p.getMeasure()).collect(toList()));
			measures.addAll(logicalDescriptorMeasures().stream().map(p -> p.getMeasure()).collect(toList()));
			measures.addAll(ruleDescriptorMeasures().stream().map(p -> p.getMeasure()).collect(toList()));
		} else if (TableBasedPropositionalContext.class.isAssignableFrom(clazz)) {
			measures.addAll(localPatternDescriptorMeasures().stream().map(p -> p.getMeasure()).collect(toList()));
			measures.addAll(logicalDescriptorMeasures().stream().map(p -> p.getMeasure()).collect(toList()));
			measures.addAll(ruleDescriptorMeasures().stream().map(p -> p.getMeasure()).collect(toList()));
			measures.addAll(subgroupMeasures().stream().map(p -> p.getMeasure()).collect(toList()));
			measures.addAll(
					functionalPatternMeasurementProcedures().stream().map(p -> p.getMeasure()).collect(toList()));
		} else if (SequentialPropositionalContext.class.isAssignableFrom(clazz)) {
			measures.addAll(sequencePatternMeasurementProcedures().stream().map(p -> p.getMeasure()).collect(toList()));
		} else if (TableBasedSingleSequencePropositionalContext.class.isAssignableFrom(clazz)) {
			measures.addAll(episodeMeasures().stream().map(p -> p.getMeasure()).collect(toList()));
			measures.addAll(episodeRuleMeasures().stream().map(p -> p.getMeasure()).collect(toList()));
		}

		return measures;
	}

}
