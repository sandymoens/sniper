/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.utils;

import java.io.File;

/**
 * General utility functions for file operation.
 * 
 * @author Sandy Moens
 * @since 0.0.1
 * @version 0.0.1
 */
public class FileUtils {

	/**
	 * Checks if a directory structure exists. All intermediate directories that
	 * do not yet exist are created.
	 * 
	 * @param dir
	 *            the directory path that should exist or be created.
	 */
	public static void checkDirAndCreate(String dir) {
		if (!new File(dir).exists()) {
			new File(dir).mkdirs();
		}
	}

}
