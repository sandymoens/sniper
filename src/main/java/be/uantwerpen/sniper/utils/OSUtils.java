/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.utils;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class OSUtils {

	// Different types of operating systems
	public static enum OperatingSystem {
		WINDOWS, LINUX, MACOSX
	}

	public final static OperatingSystem operatingSystem = decideOperatingSystem();

	/**
	 * Checks if the current operating system is windows based
	 * 
	 * @return true if the current operating system is windows
	 */
	public static boolean isWindows() {
		if (operatingSystem.equals(OperatingSystem.WINDOWS)) {
			return true;
		} else
			return false;
	}

	/**
	 * Checks if the current operating system is linux based
	 * 
	 * @return true if the current operating system is linux
	 */
	public static boolean isLinux() {
		if (operatingSystem.equals(OperatingSystem.LINUX)) {
			return true;
		} else
			return false;
	}

	/**
	 * Checks if the current operating system is os x based
	 * 
	 * @return true if the current operating system is os x
	 */
	public static boolean isMacOSX() {
		if (operatingSystem.equals(OperatingSystem.MACOSX)) {
			return true;
		} else
			return false;
	}

	/**
	 * Determines the type of the operating system
	 * 
	 * @return the type of operating system
	 */
	public static OperatingSystem decideOperatingSystem() {
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.startsWith("windows"))
			return OperatingSystem.WINDOWS;
		if (osName.startsWith("mac os x"))
			return OperatingSystem.MACOSX;
		return OperatingSystem.LINUX;
	}

}
