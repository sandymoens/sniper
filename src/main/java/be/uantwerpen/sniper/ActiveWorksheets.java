/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper;

import static be.uantwerpen.sniper.action.ActionManagers.newActionManager;
import static be.uantwerpen.sniper.command.UndoStacks.newActionCommandBasedUndoStack;
import static be.uantwerpen.sniper.model.FilterBrickModels.newFilterBrickModel;
import static be.uantwerpen.sniper.model.WorksheetPropertiesModels.newWorksheetPropertiesModel;
import static be.uantwerpen.sniper.pattern.PatternVisualizers.newPatternVisualizer;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import be.uantwerpen.sniper.action.ActionManager;
import be.uantwerpen.sniper.command.ActionBasedUndoCommand;
import be.uantwerpen.sniper.command.UndoStack;
import be.uantwerpen.sniper.core.PatternExtensionComputer;
import be.uantwerpen.sniper.core.PatternExtensionComputers;
import be.uantwerpen.sniper.core.PatternExtensionsKey;
import be.uantwerpen.sniper.dao.DataAccessHandlers;
import be.uantwerpen.sniper.dao.SchemeMetaInfo;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.model.BrickModel;
import be.uantwerpen.sniper.model.FilterBrickModel;
import be.uantwerpen.sniper.model.MeasureModel;
import be.uantwerpen.sniper.model.MeasureModels;
import be.uantwerpen.sniper.model.MeasureModels.ObservableMeasureModel;
import be.uantwerpen.sniper.model.StatusEnabledModel;
import be.uantwerpen.sniper.model.StatusEnabledModels;
import be.uantwerpen.sniper.model.WorksheetPropertiesModel;
import be.uantwerpen.sniper.pattern.PatternVisualizer;
import be.uantwerpen.sniper.utils.MeasureRegister;
import be.uantwerpen.sniper.visualization.Visualization;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.NamedPatternCollection;
import de.unibonn.realkd.patterns.Pattern;

/**
 *
 * @author Sandy Moens
 * @since 0.0.1
 * @version 0.2.0
 */
public class ActiveWorksheets {

	public static ActiveWorksheet activeWorksheet(Worksheet worksheet, Workspace workspace) {
		return new DefaultActiveWorksheet(worksheet, workspace);
	}

	private static class DefaultActiveWorksheet implements ActiveWorksheet {

		private Worksheet worksheet;

		private Workspace realKDWorkspace;

		private WorksheetPropertiesModel worksheetProperties;

		private PropositionalContext propositionalContext;

		private PatternController patternController;

		private FilterBrickModel filterBrickModel;

		private ObservableMeasureModel measureModel;

		private StatusEnabledModel<Visualization<Pattern<?>, ?>> visualizationModel;

		private ActionManager actionManager;

		private UndoStack<ActionBasedUndoCommand> undoStack;

		private PatternVisualizer patternVisualizer;

		private SchemeMetaInfo schemeMetaInfo;

		private PatternExtensionComputer<PatternExtensionsKey> patternExtensionComputer;

		private Cache<String, Object> cache = CacheBuilder.newBuilder().expireAfterAccess(10, TimeUnit.MINUTES).build();

		public DefaultActiveWorksheet(Worksheet worksheet, Workspace workspace) {
			this.worksheet = worksheet;
			this.realKDWorkspace = workspace;

			this.propositionalContext = workspace.propositionalContexts().get(0);

			if (workspace.contains(Identifier.id("WorksheetProperties"))) {
				this.worksheetProperties = (WorksheetPropertiesModel) workspace
						.get(Identifier.id("WorksheetProperties"));
			} else {
				this.worksheetProperties = newWorksheetPropertiesModel(this.propositionalContext.getClass());
				this.realKDWorkspace.add(this.worksheetProperties);
			}

			if (workspace.contains(Identifier.id("MeasureModel"))) {
				this.measureModel = (ObservableMeasureModel) workspace.get(Identifier.id("MeasureModel"));
			} else {
				List<Measure> availableMeasures = MeasureRegister.measureRegister()
						.getMeasures(workspace.propositionalContexts().get(0).getClass());

				this.measureModel = MeasureModels.newObservableMeasureModel(this.worksheetProperties,
						availableMeasures);
				this.realKDWorkspace.add(this.measureModel);
			}

			if (workspace.contains(Identifier.id("VisualizationModel"))) {
				@SuppressWarnings("unchecked")
				StatusEnabledModel<Visualization<Pattern<?>, ?>> visualizationModel = (StatusEnabledModel<Visualization<Pattern<?>, ?>>) workspace
						.get(Identifier.id("VisualizationModel"));
				this.visualizationModel = visualizationModel;
			} else {
				this.visualizationModel = StatusEnabledModels.newVisualizationModel();
				this.realKDWorkspace.add(this.visualizationModel);
			}

			this.patternController = PatternControllers.newPatternController(workspace, this.measureModel,
					this.worksheetProperties);

			if (workspace.contains(Identifier.id("PatternCollection"))) {
				NamedPatternCollection npc = (NamedPatternCollection) workspace.get(Identifier.id("PatternCollection"));
				this.patternController.brickModel().addAll(0,
						npc.patterns().stream().map(p -> (Object) p).collect(toList()));

			} else {
				this.realKDWorkspace.add(new NamedPatternCollection(Identifier.id("PatternCollection"),
						"PatternCollection", "Collection of patterns", this.patternController.brickModel().patterns()));
			}

			this.filterBrickModel = newFilterBrickModel(workspace);

			this.actionManager = newActionManager(this);
			this.undoStack = newActionCommandBasedUndoStack(20);
			this.patternVisualizer = newPatternVisualizer(this);

			this.schemeMetaInfo = DataAccessHandlers.schemeMetaInfoAccessHandler().load(worksheet.getScheme()).get();

			this.patternExtensionComputer = PatternExtensionComputers.newCachingPatternExtensionComputer();
		}

		@Override
		public List<Parameter<?>> properties() {
			return this.worksheetProperties.properties();
		}

		@Override
		public Optional<Parameter<?>> property(String key) {
			return this.worksheetProperties.property(key);
		}

		@Override
		public WorksheetPropertiesModel propertiesModel() {
			return this.worksheetProperties;
		}

		@Override
		public PropositionalContext propositionalContext() {
			return this.propositionalContext;
		}

		@Override
		public PatternController patternController() {
			return this.patternController;
		}

		@Override
		public BrickModel brickModel() {
			return this.patternController.brickModel();
		}

		@Override
		public FilterBrickModel filterBrickModel() {
			return this.filterBrickModel;
		}

		@Override
		public MeasureModel measureModel() {
			return this.measureModel;
		}

		@Override
		public StatusEnabledModel<Visualization<Pattern<?>, ?>> visualizationModel() {
			return this.visualizationModel;
		}

		@Override
		public Worksheet worksheet() {
			return this.worksheet;
		}

		@Override
		public SchemeMetaInfo schemeMetaInfo() {
			return this.schemeMetaInfo;
		}

		@Override
		public Workspace realKDWorkspace() {
			return this.realKDWorkspace;
		}

		@Override
		public ActionManager actionManager() {
			return this.actionManager;
		}

		@Override
		public UndoStack<ActionBasedUndoCommand> undoStack() {
			return this.undoStack;
		}

		@Override
		public PatternVisualizer patternVisualizer() {
			return this.patternVisualizer;
		}

		@Override
		public PatternExtensionComputer<PatternExtensionsKey> patternExtensionComputerForPropositions() {
			return this.patternExtensionComputer;
		}

		@Override
		public String cache(Object value) {
			String key = UUID.randomUUID().toString();

			while (this.cache.getIfPresent(key) != null) {
				key = UUID.randomUUID().toString();
			}

			this.cache.put(key, value);

			return key;
		}

		@Override
		public Optional<Object> fromCache(String key) {
			return Optional.ofNullable(this.cache.getIfPresent(key));
		}

	}

	// Suppress default constructor for non-instantiability
	private ActiveWorksheets() {
		throw new AssertionError();
	}

}
