/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper;

import static be.uantwerpen.sniper.dao.DataAccessHandlers.worksheetPropositionalContextAccessHandler;
import static be.uantwerpen.sniper.dao.PersistentStoragePathGenerators.persistentStoragePathGenerator;

import java.io.File;
import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.common.workspace.Workspaces;
import de.unibonn.realkd.data.propositions.PropositionalContext;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class WorksheetLoadActions {

	private static Logger LOGGER = Logger.getLogger(WorksheetLoadActions.class);

	public static class WorksheetLoadAction implements Callable<ActiveWorksheet> {

		private Worksheet worksheet;

		public WorksheetLoadAction(Worksheet worksheet) {
			this.worksheet = worksheet;
		}

		@Override
		public ActiveWorksheet call() throws Exception {
			LOGGER.info("Loading worksheet with id '" + this.worksheet.getId() + "'.");

			Optional<Path> path = persistentStoragePathGenerator()
					.directory(new PersistentStorageIdentifiers.WorksheetIdentifier(this.worksheet));

			if (!path.isPresent()) {
				return null;
			}

			File file = new File(path.get().toUri());
			file.mkdirs();

			Workspace workspace = Workspaces.workspace(path.get());

			if (workspace.propositionalContexts().isEmpty()) {
				Optional<PropositionalContext> propositionalContext = worksheetPropositionalContextAccessHandler()
						.load(this.worksheet);

				if (!propositionalContext.isPresent()) {
					throw new Exception("Unable to load worksheet with id '" + this.worksheet.getId() + "'.");
				}

				workspace.add(propositionalContext.get());
			}

			return ActiveWorksheets.activeWorksheet(this.worksheet, workspace);
		}

	}
}
