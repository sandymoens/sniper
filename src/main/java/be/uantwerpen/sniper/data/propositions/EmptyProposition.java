/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.data.propositions;

import java.util.Objects;

import de.unibonn.realkd.common.IndexSet;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;

public class EmptyProposition implements Proposition {

	private PropositionalContext context;

	public EmptyProposition(PropositionalContext context) {
		this.context = context;
	}

	@Override
	public SerialForm<? extends Proposition> serialForm() {
		return new SerialForm<Proposition>() {

			@Override
			public Proposition build(Workspace workspace) {
				return new EmptyProposition(workspace.propositionalContexts().get(0));
			}

		};
	}

	@Override
	public boolean holdsFor(int i) {
		return true;
	}

	@Override
	public String name() {
		return "EMPTY";
	}

	@Override
	public IndexSet supportSet() {
		return this.context.population().objectIds();
	}

	@Override
	public int supportCount() {
		return this.context.population().size();
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.context.caption());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmptyProposition other = (EmptyProposition) obj;
		if (this.context == null) {
			if (other.context != null)
				return false;
		} else if (!this.context.equals(other.context))
			return false;
		return true;
	}

}
