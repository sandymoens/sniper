/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.data.sequence;

import static com.google.common.collect.Lists.newLinkedList;
import static com.google.common.collect.Maps.newHashMap;
import static de.unibonn.realkd.common.base.Identifier.id;
import static de.unibonn.realkd.data.sequences.DefaultTableBasedSingleSequencePropositionalContext.singleSequencePropositionalContext;
import static de.unibonn.realkd.data.sequences.SequenceEvents.newSequenceEvent;
import static de.unibonn.realkd.data.sequences.SequenceTransactions.newSequenceTransaction;
import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.unibonn.realkd.common.IndexSets;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContextFromTableBuilder;
import de.unibonn.realkd.data.propositions.PropositionalizationRule;
import de.unibonn.realkd.data.propositions.Propositions;
import de.unibonn.realkd.data.sequences.SequenceEvent;
import de.unibonn.realkd.data.sequences.SequenceEvents;
import de.unibonn.realkd.data.sequences.SequenceTransaction;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 * 
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SequentialDataTableAsSingleSequenceFromTableFactory {

	private static final Logger LOGGER = Logger.getLogger(PropositionalContextFromTableBuilder.class.getName());

	private SubCollectionParameter<PropositionalizationRule, Set<PropositionalizationRule>> attributeFactories;

	private String groupingAttributeName;
	private String distanceAttributeName;

	public SequentialDataTableAsSingleSequenceFromTableFactory() {
		this.attributeFactories = Parameters.subSetParameter(Identifier.identifier("Attribute_mappers"),
				"Attribute mappers",
				"Mappers that are used to create binary propositions for pattern mining from the data table",
				() -> PropositionalContextFromTableBuilder.ALL_MAPPERS,
				() -> PropositionalContextFromTableBuilder.DEFAULT_MAPPERS);

		this.distanceAttributeName = "id";
		this.distanceAttributeName = "date";
	}

	/**
	 * 
	 * @return the externally accessible parameter of the attribute mappers
	 */
	public SubCollectionParameter<PropositionalizationRule, Set<PropositionalizationRule>> attributeMapperParamater() {
		return this.attributeFactories;
	}

	/**
	 * 
	 * @return the current set of attribute to propositions mappers to be used
	 * for creating propositional logics from table
	 */
	public Set<PropositionalizationRule> mappers() {
		return this.attributeFactories.current();
	}

	/**
	 * 
	 * @param mappers the collection of mappers to be used for creating
	 * propositional logics from table
	 */
	public void mappers(Set<PropositionalizationRule> mappers) {
		this.attributeFactories.set(mappers);
	}

	/**
	 * Sets the name of the attribute that is used for grouping sequences
	 * 
	 * @param groupingAttributeName the name of the grouping attribute in the
	 * data table
	 */
	public void groupingAttributeName(String groupingAttributeName) {
		this.groupingAttributeName = groupingAttributeName;
	}

	/**
	 * Sets the name of the attribute that is used for annotating the distance
	 * between events in a sequences
	 * 
	 * @param distanceAttributeName the name of the distance attribute in the
	 * data table
	 */
	public void distanceAttributeName(String distanceAttributeName) {
		this.distanceAttributeName = distanceAttributeName;
	}

	public SingleSequencePropositionalContext build(DataTable dataTable) {
		List<Proposition> propositions = compilePropositions(dataTable);
		List<SequenceTransaction> sequences = compileSequences(dataTable, propositions);

		int gapSize = computeGapSize(sequences);

		List<SequenceEvent<?>> events = compileEvents(sequences, gapSize);

		propositions = recompilePropositions(propositions, events);
		events = recompileSequence(propositions, events);

		return singleSequencePropositionalContext(dataTable, events, propositions);
	}

	private List<Proposition> compilePropositions(DataTable dataTable) {
		LOGGER.fine("Compiling proposition list");

		List<Proposition> propositions = new ArrayList<>();

		for (Attribute<?> attribute : dataTable.attributes()) {
			if (this.groupingAttributeName.equals(attribute.caption())
					|| this.distanceAttributeName.equals(attribute.caption())) {
				continue;
			}
			for (PropositionalizationRule mapper : this.attributeFactories.current()) {
				propositions.addAll(mapper.apply(dataTable, attribute));
			}
		}

//		propositions.forEach(p -> {
//			propositions.subList(0, propositions.indexOf(p)).forEach(q -> {
//				if (p.implies(q)) {
//					LOGGER.fine("'" + p + "' implies already present proposition '" + q + "'");
//				}
//				if (q.implies(p)) {
//					LOGGER.fine("'" + p + "' is implied by already present proposition '" + q + "'");
//				}
//			});
//		});

		LOGGER.info("Done compiling proposition list (" + propositions.size() + " propositions added)");

		return propositions;
	}

	private Map<String, List<Integer>> getGroupToIndices(DataTable dataTable, Attribute<?> groupingAttribute) {
		Map<String, List<Integer>> groupToIndices = newHashMap();

		for (int i = 0; i < dataTable.population().size(); i++) {
			Optional<?> groupingValue = groupingAttribute.getValueOption(i);

			if (!groupingValue.isPresent()) {
				continue;
			}

			String value = groupingValue.get().toString();

			List<Integer> indices = groupToIndices.get(value);

			if (indices == null) {
				groupToIndices.put(value, indices = newLinkedList());
			}

			indices.add(i);
		}

		return groupToIndices;
	}

	private List<List<SequenceEvent<?>>> getSequences(DataTable dataTable, List<Proposition> propositions,
			Attribute<?> distanceAttribute, Collection<List<Integer>> groupToIndices) {

		return groupToIndices.stream().map(indexList -> {
			List<SequenceEvent<?>> sequence = Lists.newArrayList();

			for (int i : indexList) {
				Optional<?> distValue = distanceAttribute.getValueOption(i);

				if (!distValue.isPresent()) {
					continue;
				}
				;

				Comparable<?> object = (Comparable<?>) distValue.get();

				List<Proposition> eventList = IntStream.range(0, propositions.size())
						.filter(j -> propositions.get(j).holdsFor(i)).mapToObj(j -> propositions.get(j))
						.collect(Collectors.toList());

				for (Proposition proposition : eventList) {
					sequence.add(newSequenceEvent(object, proposition));
				}
			}

			return sequence;
		}).collect(Collectors.toList());
	}

	private List<SequenceTransaction> compileSequences(DataTable dataTable, List<Proposition> propositions) {
		LOGGER.fine("Creating sequences");

		Attribute<?> groupingAttribute = dataTable.attribute(id(this.groupingAttributeName)).get();
		Attribute<?> distanceAttribute = dataTable.attribute(id(this.distanceAttributeName)).get();

		Map<String, List<Integer>> groupToIndices = getGroupToIndices(dataTable, groupingAttribute);

		List<List<SequenceEvent<?>>> sequences = getSequences(dataTable, propositions, distanceAttribute,
				groupToIndices.values());

		List<SequenceTransaction> sequenceTransactions = sequences.stream().map(s -> newSequenceTransaction(s))
				.collect(Collectors.toList());

		LOGGER.info("Done creating sequences (" + sequences.size() + " sequences created)");

		return sequenceTransactions;
	}

	private String getName(Proposition proposition) {
		if (AttributeBasedProposition.class.isAssignableFrom(proposition.getClass())) {
			AttributeBasedProposition<?> aProposition = (AttributeBasedProposition<?>) proposition;
			return aProposition.attribute().caption() + "=" + aProposition.constraint().description();
		}
		return proposition.name();
	}

	private int computeGapSize(List<SequenceTransaction> sequences) {
		int gapSize = 0;

		for (SequenceTransaction sequence : sequences) {
			Set<?> uniqueValues = sequence.events().stream().map(e -> e.value()).collect(toSet());

			gapSize = Math.max(gapSize, uniqueValues.size());
		}

		return gapSize;
	}

	private List<SequenceEvent<?>> compileEvents(List<SequenceTransaction> sequences, int gapSize) {
		List<SequenceEvent<?>> events = Lists.newArrayList();

		long beg = System.currentTimeMillis();

		int i = 0;
		double newValue = -1.;
		Object value = null;

		for (SequenceTransaction sequence : sequences) {
			for (SequenceEvent<?> event : sequence.events()) {
				if (!event.value().equals(value)) {
					newValue++;
					value = event.value();
					System.out.println(newValue + " " + value);
				}

				events.add(SequenceEvents.newSequenceEvent(newValue, event.proposition()));

				i += 1;

				if (i % 10000 == 0) {
					System.out.println(newValue + " " + (System.currentTimeMillis() - beg) + "ms");
				}
			}
			newValue += gapSize;

		}

		return events;
	}

	private List<Proposition> recompilePropositions(List<Proposition> propositions, List<SequenceEvent<?>> events) {
		Map<Proposition, List<Integer>> indexSets = Maps.newHashMap();

		for (Proposition proposition : propositions) {
			indexSets.put(proposition, Lists.newLinkedList());
		}

		int i = 0;
		for (SequenceEvent<?> event : events) {
			indexSets.get(event.proposition()).add(i);
			i++;
		}

		List<Proposition> newPropositions = Lists.newArrayList();

		i = 0;
		for (Proposition proposition : propositions) {
			newPropositions.add(
					Propositions.proposition(i++, getName(proposition), IndexSets.copyOf(indexSets.get(proposition))));
		}

		return newPropositions;
	}

	private List<SequenceEvent<?>> recompileSequence(List<Proposition> propositions, List<SequenceEvent<?>> events) {
		Map<String, Proposition> mapping = Maps.newHashMap();

		for (Proposition proposition : propositions) {
			mapping.put(proposition.name(), proposition);
		}

		List<SequenceEvent<?>> newEvents = Lists.newArrayList();

		for (SequenceEvent<?> sequenceEvent : events) {
			newEvents.add(newSequenceEvent(sequenceEvent.value(), mapping.get(getName(sequenceEvent.proposition()))));
		}

		return newEvents;
	}

}
