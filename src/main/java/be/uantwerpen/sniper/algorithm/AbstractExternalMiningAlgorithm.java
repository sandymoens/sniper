/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.patterns.Pattern;
import ua.ac.be.mime.tool.threading.StreamReader;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public abstract class AbstractExternalMiningAlgorithm<T extends Pattern<?>> extends AbstractMiningAlgorithm<T> {

	protected File dbFile;

	protected File outputFile;

	private Process process;

	public AbstractExternalMiningAlgorithm() {
		try {
			this.dbFile = File.createTempFile("input", ".txt");
			this.dbFile.deleteOnExit();

			this.outputFile = File.createTempFile("output" + this.dbFile.getName().substring(5), ".txt");
			this.outputFile.deleteOnExit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected abstract boolean writeDbFile();

	protected abstract Collection<T> readOutputFile();

	protected abstract List<String> command();

	@Override
	protected Collection<T> concreteCall() {
		if (!writeDbFile()) {
			return null;
		}

		List<String> command = command();

		if (command == null) {
			return null;
		}

		ProcessBuilder pb = new ProcessBuilder(command);

		System.out.println(command);

		try {
			this.process = pb.start();

			Thread inputStreamThread = new Thread(new StreamReader(this.process.getInputStream()));
			Thread errorStreamThread = new Thread(new StreamReader(this.process.getErrorStream()));

			inputStreamThread.start();
			errorStreamThread.start();

			this.process.waitFor();

			if (this.outputFile.exists()) {
				return readOutputFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return ImmutableList.of();
	}

	@Override
	protected void onStopRequest() {
		this.process.destroy();
	}

}
