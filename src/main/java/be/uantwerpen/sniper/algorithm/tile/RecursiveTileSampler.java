/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.tile;

import java.util.Collection;
import java.util.stream.Collectors;

import be.uantwerpen.patterns.tile.TilePattern;
import be.uantwerpen.sniper.common.Parameters;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.StoppableMiningAlgorithm;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.association.Associations;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class RecursiveTileSampler extends AbstractMiningAlgorithm<Association> implements StoppableMiningAlgorithm {

	private PropositionalContext context;

	private be.uantwerpen.algorithms.tiles.RecursiveTileSampler sampler;

	private Parameter<Integer> numberOfResultsParameter;

	public RecursiveTileSampler(Workspace workspace) {
		this.context = workspace.propositionalContexts().get(0);

		this.sampler = null;

		this.registerParameter(this.numberOfResultsParameter = Parameters.numberOfResultsParameter());
	}

	@Override
	public String caption() {
		return "Recursive Tile Sampling";
	}

	@Override
	public String description() {
		return "Finds a sample of tiles (implementation by Moens)";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.ASSOCIATION_MINING;
	}

	@Override
	protected Collection<Association> concreteCall() throws ValidationException {
		this.sampler = new be.uantwerpen.algorithms.tiles.RecursiveTileSampler(this.context);

		Collection<TilePattern> tiles = this.sampler.sample(this.numberOfResultsParameter.current());

		this.sampler = null;

		return tiles.stream().map(t -> {
			LogicalDescriptor descriptor = LogicalDescriptors.create(this.context.population(),
					t.descriptor().elements());
			return Associations.association(descriptor);
		}).collect(Collectors.toList());
	}

	@Override
	protected void onStopRequest() {
		if (this.sampler != null) {
			this.sampler.stop();
		}
	}

}
