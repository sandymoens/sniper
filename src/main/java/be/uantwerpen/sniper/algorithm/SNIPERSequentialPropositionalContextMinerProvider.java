/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm;

import java.util.Collection;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.algorithm.sequence.CMClaSP;
import be.uantwerpen.sniper.algorithm.sequence.CMSPADE;
import be.uantwerpen.sniper.algorithm.sequence.TSP;
import de.unibonn.realkd.algorithms.StoppableMiningAlgorithm;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SNIPERSequentialPropositionalContextMinerProvider implements SequentialPropositionalContextMinerProvider {

	@Override
	public Collection<Miner> get() {
		return ImmutableList.copyOf(new Miner[] {

				new Miner() {
					@Override
					public String caption() {
						return "SPADE";
					}

					@Override
					public StoppableMiningAlgorithm newMiner(Workspace workspace) {
						return new CMSPADE(workspace);
					}
				},

				new Miner() {
					@Override
					public String caption() {
						return "ClaSP";
					}
					
					@Override
					public StoppableMiningAlgorithm newMiner(Workspace workspace) {
						return new CMClaSP(workspace);
					}
				},

				new Miner() {
					@Override
					public String caption() {
						return "TSP";
					}

					@Override
					public StoppableMiningAlgorithm newMiner(Workspace workspace) {
						return new TSP(workspace);
					}
				}

		});
	}

}
