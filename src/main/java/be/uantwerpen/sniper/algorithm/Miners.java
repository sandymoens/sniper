/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContext;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Miners {

	private static Comparator<Miner> CAPTION_COMPARATOR = new Comparator<Miner>() {

		@Override
		public int compare(Miner o1, Miner o2) {
			return o1.caption().compareTo(o2.caption());
		}

	};

	private static List<Miner> defaultPropositionalContextMiners;
	private static Map<String, Miner> defaultPropositionalContextMinersMap;

	private static List<Miner> tableBasedPropositionalContextMiners;
	private static Map<String, Miner> tableBasedPropositionalContextMinersMap;

	private static List<Miner> sequentialPropositionalContextMiners;
	private static Map<String, Miner> sequentialPropositionalContextMinersMap;

	private static List<Miner> singleSequencePropositionalContextMiners;
	private static Map<String, Miner> singleSequencePropositionalContextMinersMap;

	static {
		defaultPropositionalContextMiners = loadMiners(DefaultPropositionalContextMinerProvider.class);
		defaultPropositionalContextMinersMap = mapMiners(defaultPropositionalContextMiners);

		tableBasedPropositionalContextMiners = loadMiners(TableBasedPropositionalContextMinerProvider.class);
		tableBasedPropositionalContextMinersMap = mapMiners(tableBasedPropositionalContextMiners);

		sequentialPropositionalContextMiners = loadMiners(SequentialPropositionalContextMinerProvider.class);
		sequentialPropositionalContextMinersMap = mapMiners(sequentialPropositionalContextMiners);

		singleSequencePropositionalContextMiners = loadMiners(SingleSequencePropositionalContextMinerProvider.class);
		singleSequencePropositionalContextMinersMap = mapMiners(singleSequencePropositionalContextMiners);
	}

	private static <T extends MinerProvider> List<Miner> loadMiners(Class<T> providerClass) {
		ServiceLoader<T> loader = ServiceLoader.load(providerClass);
		Iterator<T> iterator = loader.iterator();

		List<Miner> tmp = Lists.newArrayList();

		while (iterator.hasNext()) {
			tmp.addAll(iterator.next().get());
		}

		Collections.sort(tmp, CAPTION_COMPARATOR);

		return ImmutableList.copyOf(tmp);
	}

	private static Map<String, Miner> mapMiners(List<Miner> miners) {
		final Map<String, Miner> minersMap = Maps.newHashMap();

		miners.stream().forEach(m -> minersMap.put(m.caption(), m));

		return ImmutableMap.copyOf(minersMap);
	}

	public static List<String> minersForType(PropositionalContext context) {
		if (SingleSequencePropositionalContext.class.isAssignableFrom(context.getClass())) {
			return singleSequencePropositionalContextMiners.stream().map(m -> m.caption()).collect(Collectors.toList());
		} else if (SequentialPropositionalContext.class.isAssignableFrom(context.getClass())) {
			return sequentialPropositionalContextMiners.stream().map(m -> m.caption()).collect(Collectors.toList());
		} else if (TableBasedPropositionalContext.class.isAssignableFrom(context.getClass())) {
			return tableBasedPropositionalContextMiners.stream().map(m -> m.caption()).collect(Collectors.toList());
		}
		return defaultPropositionalContextMiners.stream().map(m -> m.caption()).collect(Collectors.toList());
	}

	public static Optional<Miner> newMiner(PropositionalContext context, String caption) {
		if (SingleSequencePropositionalContext.class.isAssignableFrom(context.getClass())) {
			return Optional.ofNullable(singleSequencePropositionalContextMinersMap.get(caption));
		} else if (SequentialPropositionalContext.class.isAssignableFrom(context.getClass())) {
			return Optional.ofNullable(sequentialPropositionalContextMinersMap.get(caption));
		} else if (TableBasedPropositionalContext.class.isAssignableFrom(context.getClass())) {
			return Optional.ofNullable(tableBasedPropositionalContextMinersMap.get(caption));
		}
		return Optional.ofNullable(defaultPropositionalContextMinersMap.get(caption));
	}

	// Suppress default constructor for non-instantiability
	private Miners() {
		throw new AssertionError();
	}

}
