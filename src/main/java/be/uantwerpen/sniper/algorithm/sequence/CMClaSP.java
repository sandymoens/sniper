/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.sequence;

import static be.uantwerpen.sniper.algorithm.utils.spmf.SPMFContextWriters.sequentialContextWriter;
import static be.uantwerpen.sniper.algorithm.utils.spmf.SPMFPatternReaders.sequencePatternReader;
import static be.uantwerpen.sniper.common.Parameters.minFrequencyParameter;
import static be.uantwerpen.sniper.config.Settings.PLUGIN_DIR;
import static com.google.common.collect.Lists.newArrayList;

import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

import be.uantwerpen.sniper.algorithm.AbstractExternalMiningAlgorithm;
import be.uantwerpen.sniper.config.Settings;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContext;
import de.unibonn.realkd.patterns.sequence.Sequence;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class CMClaSP extends AbstractExternalMiningAlgorithm<Sequence> {

	private SequentialPropositionalContext context;

	private Parameter<Double> frequencyParameter;

	public CMClaSP(Workspace workspace) {
		this.context = (SequentialPropositionalContext) workspace.propositionalContexts().get(0);

		this.registerParameter(this.frequencyParameter = minFrequencyParameter());
	}

	@Override
	public String caption() {
		return "CM-ClaSP";
	}

	@Override
	public String description() {
		return "Finds all closed frequent sequence patterns (implementation provided by SPMF)";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.SEQUENCE_MINING;
	}

	@Override
	protected boolean writeDbFile() {
		return sequentialContextWriter().writeContext(this.dbFile.getAbsolutePath(), this.context);
	}

	@Override
	protected Collection<Sequence> readOutputFile() {
		return sequencePatternReader().readPatterns(this.outputFile.getAbsolutePath(), this.context);
	}

	@Override
	protected List<String> command() {
		double frequency = this.frequencyParameter.current() * 100;

		return newArrayList("java", "-jar",
				Paths.get(Settings.Instance.getProperty(PLUGIN_DIR), "spmf-2.22.jar").toString(), "run", "CM-ClaSP",
				this.dbFile.getAbsolutePath(), this.outputFile.getAbsolutePath(), String.format("%.5f%%", frequency),
				"false");
	}

}
