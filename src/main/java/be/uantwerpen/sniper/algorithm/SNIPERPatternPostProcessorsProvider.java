/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.algorithm.postprocessor.pattern.EpisodeRulesFromEpisodeGenerators;
import be.uantwerpen.sniper.algorithm.postprocessor.pattern.InversePatternGenerators;
import be.uantwerpen.sniper.algorithm.postprocessor.pattern.PatternsRemovers;
import be.uantwerpen.sniper.algorithm.postprocessor.pattern.SerialEpisodesFromEpisodeGenerators;
import be.uantwerpen.sniper.algorithm.postprocessor.pattern.SimpleExtendedEpisodeRuleGenerator;
import be.uantwerpen.sniper.algorithm.postprocessor.pattern.SubPatternsGenerators;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.Pattern;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SNIPERPatternPostProcessorsProvider implements PatternPostProcessorProvider {

	@Override
	public Collection<PatternPostProcessor> get() {
		return ImmutableList.copyOf(new PatternPostProcessor[] {

				new PatternPostProcessor() {
					@Override
					public String caption() {
						return "Patterns remover";
					}

					@Override
					public PatternPostProcessingAlgorithm newPostProcessor(Workspace workspace,
							List<Pattern<?>> patterns, List<Integer> patternIndices) {
						return PatternsRemovers.newPatternsRemover(patterns, patternIndices);
					};
				},

				new PatternPostProcessor() {
					@Override
					public String caption() {
						return "Sub patterns generator";
					}

					@Override
					public PatternPostProcessingAlgorithm newPostProcessor(Workspace workspace,
							List<Pattern<?>> patterns, List<Integer> patternIndices) {
						return SubPatternsGenerators.newSubPatternsGenerator(patterns, patternIndices);
					};
				},

				new PatternPostProcessor() {
					@Override
					public String caption() {
						return "Inverse pattern generator";
					}

					@Override
					public PatternPostProcessingAlgorithm newPostProcessor(Workspace workspace,
							List<Pattern<?>> patterns, List<Integer> patternIndices) {
						return InversePatternGenerators.newInversePatternsGenerator(patterns, patternIndices);
					};
				},

				new PatternPostProcessor() {
					@Override
					public String caption() {
						return "Episode rules from episode generator";
					}

					@Override
					public PatternPostProcessingAlgorithm newPostProcessor(Workspace workspace,
							List<Pattern<?>> patterns, List<Integer> patternIndices) {
						return EpisodeRulesFromEpisodeGenerators.newEpisodeRulesFromEpisodeGenerator(patterns,
								patternIndices);
					};
				},

				new PatternPostProcessor() {
					@Override
					public String caption() {
						return "Serial episodes from episode generator";
					}

					@Override
					public PatternPostProcessingAlgorithm newPostProcessor(Workspace workspace,
							List<Pattern<?>> patterns, List<Integer> patternIndices) {
						return SerialEpisodesFromEpisodeGenerators.newSerialEpisodesFromEpisodeGenerator(patterns,
								patternIndices);
					};
				},

				new PatternPostProcessor() {
					@Override
					public String caption() {
						return "Simple episode rule generator";
					}

					@Override
					public PatternPostProcessingAlgorithm newPostProcessor(Workspace workspace,
							List<Pattern<?>> patterns, List<Integer> patternIndices) {
						return SimpleExtendedEpisodeRuleGenerator.newSimpleExtendedEpisodeRuleGenerator(workspace,
								patterns, patternIndices);
					};
				}

		});
	}

}
