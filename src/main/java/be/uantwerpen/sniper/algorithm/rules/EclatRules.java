/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.rules;

import static be.uantwerpen.sniper.algorithm.utils.borgelt.BorgeltFIMContextWriters.borgeltContextWriter;
import static be.uantwerpen.sniper.algorithm.utils.borgelt.BorgeltFIMPatternReaders.associationRuleReader;
import static be.uantwerpen.sniper.common.Parameters.maxFrequencyParameter;
import static be.uantwerpen.sniper.common.Parameters.maxSizeParameter;
import static be.uantwerpen.sniper.common.Parameters.minConfidenceParameter;
import static be.uantwerpen.sniper.common.Parameters.minFrequencyParameter;
import static be.uantwerpen.sniper.common.Parameters.minSizeParameter;
import static be.uantwerpen.sniper.common.Parameters.superPatternsOnlyParameter;
import static be.uantwerpen.sniper.config.Settings.PLUGIN_DIR;
import static com.google.common.collect.Lists.newArrayList;

import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import be.uantwerpen.sniper.algorithm.AbstractExternalMiningAlgorithm;
import be.uantwerpen.sniper.config.Settings;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.rules.AssociationRule;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class EclatRules extends AbstractExternalMiningAlgorithm<AssociationRule> {

	private PropositionalContext context;

	private List<? extends Pattern<?>> patterns;

	private Collection<Integer> patternIndices;

	private Parameter<Double> minFrequencyParameter;

	private Parameter<Double> maxFrequencyParameter;

	private Parameter<Double> minConfidenceParameter;

	private Parameter<Integer> minSizeParameter;

	private Parameter<Integer> maxSizeParameter;

	private Parameter<Boolean> superPatternsOnly;

	public EclatRules(Workspace workspace, List<? extends Pattern<?>> patterns, Collection<Integer> patternIndices) {
		this.context = workspace.propositionalContexts().get(0);
		this.patterns = patterns;
		this.patternIndices = patternIndices;

		this.registerParameter(this.minFrequencyParameter = minFrequencyParameter());
		this.registerParameter(this.maxFrequencyParameter = maxFrequencyParameter());
		this.registerParameter(this.minConfidenceParameter = minConfidenceParameter());
		this.registerParameter(this.minSizeParameter = minSizeParameter());
		this.registerParameter(this.maxSizeParameter = maxSizeParameter());

		if (patternIndices.size() > 0) {
			this.registerParameter(this.superPatternsOnly = superPatternsOnlyParameter());
		}
	}

	@Override
	public String caption() {
		return "Eclat Rules";
	}

	@Override
	public String description() {
		return "Finds all associaiton rules given the constraints (implementation by Borgelt)";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.ASSOCIATION_RULE_MINING;
	}

	@Override
	protected boolean writeDbFile() {
		return borgeltContextWriter().writeContext(this.dbFile.getAbsolutePath(), this.context);
	}

	@Override
	protected Collection<AssociationRule> readOutputFile() {
		List<AssociationRule> minedPatterns = associationRuleReader().readPatterns(this.outputFile.getAbsolutePath(),
				this.context);

		if (this.patternIndices.isEmpty() || !this.superPatternsOnly.current()) {
			return minedPatterns;
		}

		List<AssociationRule> rules = this.patternIndices.stream().map(i -> this.patterns.get(i))
				.filter(p -> AssociationRule.class.isAssignableFrom(p.getClass())).map(p -> (AssociationRule) p)
				.collect(Collectors.toList());

		return minedPatterns.stream().filter(p -> {
			for (AssociationRule rule : rules) {
				if (p.descriptor().getAntecedent().elements().containsAll(rule.descriptor().getAntecedent().elements())
						&& p.descriptor().getConsequent().elements()
								.containsAll(rule.descriptor().getConsequent().elements())) {
					return true;
				}
			}
			return false;
		}).collect(Collectors.toList());
	}

	@Override
	protected List<String> command() {
		double minFrequency = this.minFrequencyParameter.current() * 100;
		double maxFrequency = this.maxFrequencyParameter.current() * 100;
		double minConfidence = this.minConfidenceParameter.current() * 100;
		int minSize = this.minSizeParameter.current();
		int maxSize = this.maxSizeParameter.current();

		return newArrayList(Paths.get(Settings.Instance.getProperty(PLUGIN_DIR), "eclat").toString(), "-tr",
				String.format("-s%.5f", minFrequency), String.format("-S%.5f", maxFrequency),
				String.format("-c%.5f", minConfidence), String.format("-m%d", minSize), String.format("-n%d", maxSize),
				this.dbFile.getAbsolutePath(), this.outputFile.getAbsolutePath());
	}

}
