/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternCollectionPostProcessors {

	private static Comparator<PatternCollectionPostProcessor> CAPTION_COMPARATOR = new Comparator<PatternCollectionPostProcessor>() {

		@Override
		public int compare(PatternCollectionPostProcessor o1, PatternCollectionPostProcessor o2) {
			return o1.caption().compareTo(o2.caption());
		}

	};

	private static List<PatternCollectionPostProcessor> postProcessors;
	private static Map<String, PatternCollectionPostProcessor> postProcessorsMap;

	static {
		List<PatternCollectionPostProcessor> postProcessorsList = Lists.newArrayList();

		ServiceLoader<PatternCollectionPostProcessorProvider> loader = ServiceLoader
				.load(PatternCollectionPostProcessorProvider.class);
		Iterator<PatternCollectionPostProcessorProvider> iterator = loader.iterator();

		while (iterator.hasNext()) {
			postProcessorsList.addAll(iterator.next().get());
		}

		Collections.sort(postProcessorsList, CAPTION_COMPARATOR);

		postProcessors = ImmutableList.copyOf(postProcessorsList);
		postProcessorsMap = Maps.newHashMap();
		postProcessors.stream().forEach(p -> postProcessorsMap.put(p.caption(), p));
	}

	public static List<String> postProcessors() {
		return postProcessors.stream().map(p -> p.caption()).collect(Collectors.toList());
	}

	public static Optional<PatternCollectionPostProcessor> newPostProcessor(String caption) {
		return Optional.ofNullable(postProcessorsMap.get(caption));
	}

	// Suppress default constructor for non-instantiability
	private PatternCollectionPostProcessors() {
		throw new AssertionError();
	}

}
