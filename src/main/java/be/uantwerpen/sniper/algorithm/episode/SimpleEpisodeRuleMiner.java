/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.episode;

import static be.uantwerpen.sniper.common.Parameters.maxConfidenceParameter;
import static be.uantwerpen.sniper.common.Parameters.maxSupportParameter;
import static be.uantwerpen.sniper.common.Parameters.minConfidenceParameter;
import static be.uantwerpen.sniper.common.Parameters.minSupportParameter;
import static be.uantwerpen.sniper.common.ParametersEpisodes.includedTypeParameter;
import static be.uantwerpen.sniper.rest.resource.common.Utils.extractAttributeNameFromProposition;
import static be.uantwerpen.sniper.rest.resource.common.Utils.extractPropositionValueFromProposition;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.model.WorksheetPropertiesModel;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.StoppableMiningAlgorithm;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.sequences.TableBasedSingleSequencePropositionalContext;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleConfidence;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRules;
import de.unibonn.realkd.patterns.episodes.EpisodeSupport;
import de.unibonn.realkd.patterns.graphs.Edges;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;
import de.unibonn.realkd.patterns.graphs.Nodes;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SimpleEpisodeRuleMiner extends AbstractMiningAlgorithm<EpisodeRule> implements StoppableMiningAlgorithm {

	private Workspace workspace;

	private TableBasedSingleSequencePropositionalContext context;

	private Parameter<Integer> minSupport;

	private Parameter<Integer> maxSupport;

	private Parameter<Double> minConfidence;

	private Parameter<Double> maxConfidence;

	private RangeEnumerableParameter<String> typeInAntecedentFilterParameter;

	private RangeEnumerableParameter<String> typeInConsequentFilterParameter;

	public SimpleEpisodeRuleMiner(Workspace workspace) {
		this.workspace = workspace;

		this.context = (TableBasedSingleSequencePropositionalContext) workspace.propositionalContexts().get(0);

		this.registerParameter(this.minSupport = minSupportParameter(this.context.events().size()));
		this.registerParameter(this.maxSupport = maxSupportParameter(this.context.events().size()));

		this.registerParameter(this.minConfidence = minConfidenceParameter());
		this.registerParameter(this.maxConfidence = maxConfidenceParameter());

		this.registerParameter(this.typeInAntecedentFilterParameter = includedTypeParameter("Antecedent type",
				"Types to be used in the antecedent", this.context));
		this.registerParameter(this.typeInConsequentFilterParameter = includedTypeParameter("Consequent type",
				"Types to be used in the consequent", this.context));
	}

	@Override
	public String caption() {
		return "Simple Episode Rule Miner";
	}

	@Override
	public String description() {
		return "Finds all simple episode rules with a single element on the left hand side and two elements on the right hand side.";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.EPISODE_MINING;
	}

	@Override
	protected Collection<EpisodeRule> concreteCall() throws ValidationException {
		String antecedentAttribute = this.typeInAntecedentFilterParameter.current();
		String consequentAttribute = this.typeInConsequentFilterParameter.current();

		List<Proposition> antecedentPropositions = newArrayList();
		Map<String, Proposition> consequentPropositions = newHashMap();

		for (Proposition proposition : this.context.propositions()) {
			String attributeName = extractAttributeNameFromProposition(proposition);
			String propositionValue = extractPropositionValueFromProposition(proposition);

			if (attributeName.equals(antecedentAttribute)) {
				antecedentPropositions.add(proposition);
			} else if (attributeName.equals(consequentAttribute)) {
				consequentPropositions.put(propositionValue, proposition);
			}
		}

		double windowSize = (Double) ((WorksheetPropertiesModel) this.workspace
				.get(Identifier.id("WorksheetProperties"))).property("Window size").get().current();

		int minSupp = this.minSupport.current();
		int maxSupp = this.maxSupport.current();

		double minConf = this.minConfidence.current();
		double maxConf = this.maxConfidence.current();

		List<EpisodeRule> patterns = newArrayList();

		for (Proposition proposition : antecedentPropositions) {
			if (super.stopRequested()) {
				break;
			}

			String propositionValue = extractPropositionValueFromProposition(proposition);

			Proposition proposition2 = consequentPropositions.get(propositionValue);

			if (proposition2 == null) {
				continue;
			}

			Node node1 = Nodes.create(1, proposition);
			Node node2 = Nodes.create(2, proposition2);

			GraphDescriptor antecedent = GraphDescriptors.create(ImmutableList.of(node1), ImmutableList.of());

			GraphDescriptor consequent = GraphDescriptors.create(ImmutableList.of(node1, node2),
					ImmutableList.of(Edges.create(1, 2)));

			EpisodeRuleDescriptor episodeRuleDescriptor = EpisodeRuleDescriptors.create(this.context, windowSize,
					antecedent, consequent);

			Measurement support = EpisodeSupport.EPISODE_SUPPORT.perform(episodeRuleDescriptor);

			if (support.value() < minSupp || support.value() > maxSupp) {
				continue;
			}

			Measurement confidence = EpisodeRuleConfidence.EPISODE_RULE_CONDIFENCE.perform(episodeRuleDescriptor);

			if (confidence.value() < minConf || confidence.value() > maxConf) {
				continue;
			}

			patterns.add(EpisodeRules.create(episodeRuleDescriptor, ImmutableList.of(),
					ImmutableList.of(support, confidence)));
		}

		return patterns;
	}

}
