/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.episode;

import static be.uantwerpen.sniper.algorithm.utils.marbles.MarblesContextWriters.marblesContextWriter;
import static be.uantwerpen.sniper.algorithm.utils.marbles.MarblesPatternReaders.marblesEpisodeReader;
import static be.uantwerpen.sniper.common.Parameters.minConfidenceParameter;
import static be.uantwerpen.sniper.common.Parameters.miningThresholdParameter;
import static be.uantwerpen.sniper.common.Parameters.windowSizeParameter;
import static be.uantwerpen.sniper.config.Settings.PLUGIN_DIR;
import static com.google.common.collect.Lists.newArrayList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

import be.uantwerpen.sniper.algorithm.AbstractExternalMiningAlgorithm;
import be.uantwerpen.sniper.algorithm.PatternCollectionPostProcessingAlgorithm;
import be.uantwerpen.sniper.algorithm.postprocessor.patterncollection.EpisodeRuleTypeFilters;
import be.uantwerpen.sniper.common.ParametersEpisodes;
import be.uantwerpen.sniper.config.Settings;
import be.uantwerpen.sniper.model.WorksheetPropertiesModel;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.sequences.TableBasedSingleSequencePropositionalContext;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;

/**
 *
 * @author Ali Doku
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Marbles extends AbstractExternalMiningAlgorithm<EpisodeRule> {

	private Workspace workspace;

	private TableBasedSingleSequencePropositionalContext context;

	private List<? extends Pattern<?>> patterns;

	private Collection<Integer> patternIndices;

	private Parameter<Integer> miningThreshold;
	private Parameter<Double> windowSize;
	private Parameter<Double> minConfidence;

	private Parameter<Double> worksheetWindowSize;

	private SubCollectionParameter<String, List<String>> typeInAntecedentFilterParameter;

	private SubCollectionParameter<String, List<String>> typeInConsequentFilterParameter;

	private SubCollectionParameter<String, List<String>> typeNotInAntecedentFilterParameter;

	private SubCollectionParameter<String, List<String>> typeNotInConsequentFilterParameter;

	public Marbles(Workspace workspace, List<? extends Pattern<?>> patterns, Collection<Integer> patternIndices) {
		this.workspace = workspace;

		this.context = (TableBasedSingleSequencePropositionalContext) workspace.propositionalContexts().get(0);

		this.patterns = patterns;
		this.patternIndices = patternIndices; // ? to be asked whats this

		this.registerParameter(this.miningThreshold = miningThresholdParameter());
		this.registerParameter(this.windowSize = windowSizeParameter());
		this.registerParameter(this.minConfidence = minConfidenceParameter());

		this.registerParameter(this.typeInAntecedentFilterParameter = ParametersEpisodes
				.includedTypesParameter("Antecedent types", "Types to be used in the antecedent", this.context));
		this.registerParameter(this.typeInConsequentFilterParameter = ParametersEpisodes
				.includedTypesParameter("Consequent types", "Types to be used in the consequent", this.context));

		this.registerParameter(this.typeNotInAntecedentFilterParameter = ParametersEpisodes.includedTypesParameter(
				"Non antecedent types", "Types not to be used in the antecedent", this.context));
		this.registerParameter(this.typeNotInConsequentFilterParameter = ParametersEpisodes.includedTypesParameter(
				"Non consequent types", "Types not to be used in the consequent", this.context));

		this.worksheetWindowSize = (Parameter<Double>) ((WorksheetPropertiesModel) workspace
				.get(Identifier.id("WorksheetProperties"))).property("Window size").get();
	}

	@Override
	public String caption() {
		return "Marbles Episode Rules";
	}

	@Override
	public String description() {
		return "Finds all episode rules given the contrains (implementation by Nikolaj Tatti)";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.EPISODE_MINING;
	}

	@Override
	protected boolean writeDbFile() {
		return marblesContextWriter(this.worksheetWindowSize.current()).writeContext(this.dbFile.getAbsolutePath(),
				this.context);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Collection<EpisodeRule> readOutputFile() {
		List<EpisodeRule> patterns = marblesEpisodeReader(this.worksheetWindowSize.current())
				.readPatterns(this.outputFile.getAbsolutePath(), this.context);

		PatternCollectionPostProcessingAlgorithm filter = EpisodeRuleTypeFilters
				.newEpisodeRuleTypeFilter(this.workspace, patterns);

		((SubCollectionParameter<String, List<String>>) filter
				.findParameterByName(this.typeInAntecedentFilterParameter.getName()))
						.set(this.typeInAntecedentFilterParameter.current());
		((SubCollectionParameter<String, List<String>>) filter
				.findParameterByName(this.typeInConsequentFilterParameter.getName()))
						.set(this.typeInConsequentFilterParameter.current());
		((SubCollectionParameter<String, List<String>>) filter
				.findParameterByName(this.typeNotInAntecedentFilterParameter.getName()))
						.set(this.typeNotInAntecedentFilterParameter.current());
		((SubCollectionParameter<String, List<String>>) filter
				.findParameterByName(this.typeNotInConsequentFilterParameter.getName()))
						.set(this.typeNotInConsequentFilterParameter.current());

		try {
			patterns = (List<EpisodeRule>) filter.call();
		} catch (ValidationException e) {
			e.printStackTrace();
			return Lists.newArrayList();
		}

		return patterns;
	}

	@Override
	protected List<String> command() {
		try {
			File file = File.createTempFile("tmp", ".txt");
			file.deleteOnExit();

			// TODO here it will be the command of execution of marbles
			// algorithm
			return newArrayList(Paths.get(Settings.Instance.getProperty(PLUGIN_DIR), "marbles/closepi").toString(),
					String.format("-t%d", this.miningThreshold.current()),
					String.format("-w%d", this.windowSize.current().longValue()),
					String.format("-i%s", this.dbFile.getAbsoluteFile()), String.format("-o%s", file.getAbsolutePath()),
					"-xs", String.format("-c%.2f", this.minConfidence.current()),
					String.format("-a%s", this.outputFile.getAbsolutePath()));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

}
