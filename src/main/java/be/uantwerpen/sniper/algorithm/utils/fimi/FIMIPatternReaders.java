/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.utils.fimi;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.algorithm.utils.PatternReader;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.association.Associations;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.rules.AssociationRule;
import de.unibonn.realkd.patterns.rules.AssociationRules;
import de.unibonn.realkd.patterns.rules.DefaultRuleDescriptor;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class FIMIPatternReaders {

	public static PatternReader<PropositionalContext, Association> associationPatternReader() {
		return new AssociationReader();
	}

	public static PatternReader<PropositionalContext, AssociationRule> associationRuleReader() {
		return new AssociationRuleReader();
	}

	private static class AssociationReader implements PatternReader<PropositionalContext, Association> {

		@Override
		public List<Association> readPatterns(String fileName, PropositionalContext context) {
			Map<String, Proposition> mapping = newHashMap();

			for (int i = 0; i < context.propositions().size(); i++) {
				mapping.put(String.valueOf(i), context.proposition(i));
			}

			try {
				BufferedReader reader = new BufferedReader(new FileReader(fileName));

				List<Association> patterns = newArrayList();

				String line;

				while ((line = reader.readLine()) != null) {
					String[] split = line.split(" \\(")[0].split(" ");

					List<Proposition> propositions = Arrays.stream(split).map(s -> mapping.get(s)).collect(toList());

					patterns.add(
							Associations.association(LogicalDescriptors.create(context.population(), propositions)));
				}

				reader.close();

				return patterns;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}

	}

	private static class AssociationRuleReader implements PatternReader<PropositionalContext, AssociationRule> {

		@Override
		public List<AssociationRule> readPatterns(String fileName, PropositionalContext context) {
			Map<String, Proposition> mapping = newHashMap();

			for (int i = 0; i < context.propositions().size(); i++) {
				mapping.put(String.valueOf(i), context.proposition(i));
			}

			try {
				BufferedReader reader = new BufferedReader(new FileReader(fileName));

				List<AssociationRule> patterns = newArrayList();

				String line;

				while ((line = reader.readLine()) != null) {
					String[] split = line.split(" \\(")[0].split(" -> ");

					List<Proposition> antecedent = Arrays.stream(split[0].split(" ")).map(s -> mapping.get(s))
							.collect(toList());
					List<Proposition> consequent = split.length == 2
							? Arrays.stream(split[1].split(" ")).map(s -> mapping.get(s)).collect(toList())
							: newArrayList();

					patterns.add(AssociationRules.create(DefaultRuleDescriptor.create(context.population(),
							LogicalDescriptors.create(context.population(), antecedent),
							LogicalDescriptors.create(context.population(), consequent)), ImmutableList.of()));
				}

				reader.close();

				return patterns;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}

	}

	// Suppress default constructor for non-instantiability
	private FIMIPatternReaders() {
		throw new AssertionError();
	}

}
