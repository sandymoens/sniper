/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.utils.marbles;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Optional;

import be.uantwerpen.sniper.algorithm.utils.ContextWriter;
import de.unibonn.realkd.data.sequences.SequenceEvent;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;

/**
 *
 * @author Ali Doku
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class MarblesContextWriters {

	public static ContextWriter<SingleSequencePropositionalContext> marblesContextWriter(double windowSize) {
		return new MarblesContextWriter();
	}

	private static class MarblesContextWriter implements ContextWriter<SingleSequencePropositionalContext> {

		@Override
		public boolean writeContext(String fileName, SingleSequencePropositionalContext context) {
			try {
				Writer writer = new BufferedWriter(new FileWriter(fileName));

				for (SequenceEvent<?> event : context.events()) {
					Optional<Integer> index = context.index(event.proposition());

					if (!index.isPresent()) {
						System.err.println("Invalid proposition when writing context for MARBLES. Skipping.");
						continue;
					}

					Double time = (Double) event.value();
					long timestamp = time.longValue();
					writer.write(String.valueOf(timestamp));
					writer.write(" ");
					writer.write(String.valueOf(context.index(event.proposition()).get()));
					writer.write("\n");
				}

				writer.close();

				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}

			return false;
		}

	}

	// Suppress default constructor for non-instantiability
	private MarblesContextWriters() {
		throw new AssertionError();
	}

}
