/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.utils.borgelt;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import be.uantwerpen.sniper.algorithm.utils.ContextWriter;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.propositions.PropositionalContext;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class BorgeltFIMContextWriters {

	public static ContextWriter<PropositionalContext> borgeltContextWriter() {
		return new BorgeltContextWriter();
	}

	private static class BorgeltContextWriter implements ContextWriter<PropositionalContext> {

		@Override
		public boolean writeContext(String fileName, PropositionalContext context) {
			try {
				Writer writer = new BufferedWriter(new FileWriter(fileName));

				Population p = context.population();

				StringBuilder builder = new StringBuilder();

				for (int i : p.objectIds()) {
					builder.setLength(0);

					for (int t : context.truthSet(i)) {
						builder.append(t);
						builder.append(" ");
					}

					builder.setLength(builder.length() - 1);

					writer.write(builder.toString());
					writer.write("\n");
				}

				writer.close();

				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}

			return false;
		}

	}

	// Suppress default constructor for non-instantiability
	private BorgeltFIMContextWriters() {
		throw new AssertionError();
	}

}
