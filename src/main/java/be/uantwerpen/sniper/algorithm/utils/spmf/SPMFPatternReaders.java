/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.utils.spmf;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import be.uantwerpen.sniper.algorithm.utils.PatternReader;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContext;
import de.unibonn.realkd.patterns.sequence.DefaultSequenceDescriptor;
import de.unibonn.realkd.patterns.sequence.Sequence;
import de.unibonn.realkd.patterns.sequence.Sequences;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SPMFPatternReaders {

	public static PatternReader<SequentialPropositionalContext, Sequence> sequencePatternReader() {
		return new SequencePatternReader();
	}

	private static class SequencePatternReader implements PatternReader<SequentialPropositionalContext, Sequence> {

		@Override
		public List<Sequence> readPatterns(String fileName, SequentialPropositionalContext context) {
			Map<String, Proposition> mapping = newHashMap();

			for (int i = 0; i < context.propositions().size(); i++) {
				mapping.put(String.valueOf(i), context.proposition(i));
			}

			try {
				BufferedReader reader = new BufferedReader(new FileReader(fileName));

				List<Sequence> patterns = newArrayList();

				String line;

				while ((line = reader.readLine()) != null) {
					if (!line.contains("#SUP")) {
						System.err.println("Skipping invalid line");
						continue;
					}

					String[] split = line.split("#SUP")[0].split(" -1 ");

					List<List<Proposition>> orderedSets = newArrayList();

					for (String eventString : split) {
						if (eventString.startsWith("#SUP")) {
							break;
						}

						String[] event = eventString.split(" ");

						if (event.length > 0) {
							orderedSets.add(Arrays.stream(event).map(e -> mapping.get(e)).collect(toList()));
						}
					}

					patterns.add(
							Sequences.create(DefaultSequenceDescriptor.create(context, orderedSets), newArrayList()));
				}

				reader.close();

				return patterns;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}

	}

	// Suppress default constructor for non-instantiability
	private SPMFPatternReaders() {
		throw new AssertionError();
	}

}
