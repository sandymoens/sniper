/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.utils.marbles;

import static com.google.common.collect.Lists.newArrayList;
import static de.unibonn.realkd.patterns.episodes.EpisodeRuleConfidence.EPISODE_RULE_CONDIFENCE;
import static de.unibonn.realkd.patterns.episodes.EpisodeSupport.EPISODE_SUPPORT;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.compress.utils.Lists;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.algorithm.utils.PatternReader;
import de.unibonn.realkd.common.measures.Measures;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRules;
import de.unibonn.realkd.patterns.graphs.Edge;
import de.unibonn.realkd.patterns.graphs.Edges;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;
import de.unibonn.realkd.patterns.graphs.Nodes;

/**
 *
 * @author Ali Doku
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class MarblesPatternReaders {

	public static PatternReader<SingleSequencePropositionalContext, EpisodeRule> marblesEpisodeReader(
			double windowSize) {
		return new MarblesEpisodeReader(windowSize);
	}

	private static class MarblesEpisodeReader
			implements PatternReader<SingleSequencePropositionalContext, EpisodeRule> {

		private double windowSize;

		private MarblesEpisodeReader(double windowSize) {
			this.windowSize = windowSize;
		}

		@Override
		public List<EpisodeRule> readPatterns(String fileName, SingleSequencePropositionalContext context) {

			List<EpisodeRule> patterns = newArrayList();

			try {
				BufferedReader reader = new BufferedReader(new FileReader(fileName));
				String line;

				GraphDescriptor from = null;
				GraphDescriptor to = null;

				double support = Double.NaN;
				double confidence = Double.NaN;

				while ((line = reader.readLine()) != null) {

					if (line.isEmpty()) {
						if (from != null & to != null) {

							EpisodeRuleDescriptor ep = EpisodeRuleDescriptors.create(context, this.windowSize, from,
									to);
							patterns.add(EpisodeRules.create(ep, ImmutableList.of(),
									ImmutableList.of(Measures.measurement(EPISODE_SUPPORT, support),
											Measures.measurement(EPISODE_RULE_CONDIFENCE, confidence))));

						}

						from = null;
						to = null;
					}

					if (line.contains("from")) {
						from = buildGraph(reader, "from", context);
					}

					if (line.contains("to")) {
						to = buildGraph(reader, "to", context);
					}

					if (line.contains("m-support")) {
						support = Double.parseDouble(line.split("m-support:")[1]);
					}

					if (line.contains("m-confidence:")) {
						confidence = Double.parseDouble(line.split("m-confidence:")[1]);
					}

				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("finished");
			return patterns;
		}

		private GraphDescriptor buildGraph(BufferedReader reader, String caption,
				SingleSequencePropositionalContext context) {
			List<String> bandwidth = new ArrayList<>();

			List<Node> nodes = Lists.newArrayList();
			List<Edge> edges = Lists.newArrayList();

			String tmp;
			try {
				while ((tmp = reader.readLine()) != null) {
					bandwidth.add(tmp);
					if (bandwidth.size() == 4)
						break;
				}

				String[] size = bandwidth.get(0).split(" ");
				int node_size = Integer.parseInt(size[1]);
				int edge_size = Integer.parseInt(size[2]);

				if (node_size != 0) {
					String[] _nodes = bandwidth.get(1).split(" ");

					List<String> nodeIds = Arrays.asList(_nodes).subList(1, _nodes.length);

					int i = 0;

					for (String nodeId : nodeIds) {
						nodes.add(Nodes.create((i), context.proposition(Integer.parseInt(nodeId))));
						i++;
					}
				}

				if (edge_size != 0) {
					String[] _edges = bandwidth.get(2).split(" ");

					List<String> edgez = newArrayList(Arrays.asList(_edges).subList(1, _edges.length));

					Iterator<String> itEdges = edgez.iterator();

					while (itEdges.hasNext()) {
						if (itEdges.next().equals("->")) {
							itEdges.remove();
						}
					}

					if (node_size == 2 & edge_size == 1) {
						edges.add(Edges.create(Integer.parseInt(edgez.get(0)), Integer.parseInt(edgez.get(1))));
					} else {
						for (int i = 0; i < edge_size; i++) {
							edges.add(Edges.create(Integer.parseInt(edgez.get(i * 2)),
									Integer.parseInt(edgez.get((i * 2) + 1))));
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return GraphDescriptors.create(nodes, edges);
		}

	}

// Suppress default constructor for non-instantiability
	private MarblesPatternReaders() {
		throw new AssertionError();
	}
}
