/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.utils;

import static com.google.common.collect.Lists.newArrayList;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import be.uantwerpen.sniper.common.TypeSelectorType;
import be.uantwerpen.sniper.patterns.attributes.AttributeList;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.functional.FunctionalPattern;
import de.unibonn.realkd.patterns.rules.AssociationRule;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternTypeIdentifiers {

	private static List<Pair<Class<? extends Pattern<?>>, PatternTypeIdentifier>> handlers;

	public static void register(Class<? extends Pattern<?>> clazz, PatternTypeIdentifier handler) {
		handlers.add(Pair.of(clazz, handler));
	}

	static {
		handlers = newArrayList();

		Reflections reflections = new Reflections(
				new ConfigurationBuilder().addUrls(ClasspathHelper.forPackage("be.uantwerpen.sniper.algorithm.utils"))
						.setScanners(new MethodAnnotationsScanner()));

		reflections.getMethodsAnnotatedWith(PatternTypeIdentifierHandler.class).stream().forEach(m -> {
			try {
				register(m.getAnnotation(PatternTypeIdentifierHandler.class).patternClass(),
						(PatternTypeIdentifier) m.invoke(null));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}

	private static Optional<PatternTypeIdentifier> handler(Class<?> clazz) {
		for (Pair<Class<? extends Pattern<?>>, PatternTypeIdentifier> handler : handlers) {
			if (handler.getKey().isAssignableFrom(clazz)) {
				return Optional.of(handler.getValue());
			}
		}

		return Optional.empty();
	}

	public static PatternTypeIdentifier newPatternTypeIdentifier() {
		return new GenericPatternTypeIdentifier();
	}

	private static class GenericPatternTypeIdentifier implements PatternTypeIdentifier {

		@Override
		public boolean identify(TypeSelectorType type, Pattern<?> originalPattern, Pattern<?> patternToIdentify) {
			Optional<PatternTypeIdentifier> handler = handler(originalPattern.getClass());

			if (!handler.isPresent()) {
				return false;
			}

			return handler.get().identify(type, originalPattern, patternToIdentify);
		}

	}

	@PatternTypeIdentifierHandler(patternClass = Association.class)
	public static PatternTypeIdentifier associationPatternTypeIdentifier() {
		return new AssociationPatternTypeIdentifier();
	}

	@PatternTypeIdentifierHandler(patternClass = AssociationRule.class)
	public static PatternTypeIdentifier associationRulePatternTypeIdentifier() {
		return new AssociationRulePatternTypeIdentifier();
	}

	@PatternTypeIdentifierHandler(patternClass = AttributeList.class)
	public static PatternTypeIdentifier attributeListPatternTypeIdentifier() {
		return new AttributeListPatternTypeIdentifier();
	}

	@PatternTypeIdentifierHandler(patternClass = FunctionalPattern.class)
	public static PatternTypeIdentifier functionalPatternPatternTypeIdentifier() {
		return new FunctionalPatternPatternTypeIdentifier();
	}

	@PatternTypeIdentifierHandler(patternClass = ExceptionalModelPattern.class)
	public static PatternTypeIdentifier exceptionalModelPatternTypeIdentifier() {
		return new ExceptionalModelPatternTypeIdentifier();
	}

	private static class AssociationPatternTypeIdentifier implements PatternTypeIdentifier {

		@Override
		public boolean identify(TypeSelectorType type, Pattern<?> originalPattern, Pattern<?> patternToIdentify) {
			if (!Association.class.isAssignableFrom(originalPattern.getClass())
					|| !Association.class.isAssignableFrom(patternToIdentify.getClass())) {
				return false;
			}

			Association originalAssociation = (Association) originalPattern;
			Association associationToIdentify = (Association) patternToIdentify;

			switch (type) {
			case SELECTED:
				return originalAssociation == associationToIdentify;
			case SUB_PATTERNS:
				return originalAssociation != associationToIdentify && originalAssociation.descriptor().elements()
						.containsAll(associationToIdentify.descriptor().elements());
			case SUPER_PATTERNS:
				return originalAssociation != associationToIdentify && associationToIdentify.descriptor().elements()
						.containsAll(originalAssociation.descriptor().elements());
			case NON_SUB_PATTERNS:
				return originalAssociation != associationToIdentify && !originalAssociation.descriptor().elements()
						.containsAll(associationToIdentify.descriptor().elements());
			case NON_SUPER_PATTERNS:
				return originalAssociation != associationToIdentify && !associationToIdentify.descriptor().elements()
						.containsAll(originalAssociation.descriptor().elements());
			default:
				return false;
			}
		}

	}

	private static class AssociationRulePatternTypeIdentifier implements PatternTypeIdentifier {

		@Override
		public boolean identify(TypeSelectorType type, Pattern<?> originalPattern, Pattern<?> patternToIdentify) {
			if (!AssociationRule.class.isAssignableFrom(originalPattern.getClass())
					|| !AssociationRule.class.isAssignableFrom(patternToIdentify.getClass())) {
				return false;
			}

			AssociationRule originalAssociationRule = (AssociationRule) originalPattern;
			AssociationRule associationRuleToIdentify = (AssociationRule) patternToIdentify;

			switch (type) {
			case SELECTED:
				return originalAssociationRule == associationRuleToIdentify;
			case SUB_PATTERNS:
				return originalAssociationRule != associationRuleToIdentify
						&& originalAssociationRule.descriptor().getAntecedent().elements()
								.containsAll(associationRuleToIdentify.descriptor().getAntecedent().elements())
						&& originalAssociationRule.descriptor().getConsequent().elements()
								.containsAll(associationRuleToIdentify.descriptor().getConsequent().elements());
			case SUPER_PATTERNS:
				return originalAssociationRule != associationRuleToIdentify
						&& associationRuleToIdentify.descriptor().getAntecedent().elements()
								.containsAll(originalAssociationRule.descriptor().getAntecedent().elements())
						&& associationRuleToIdentify.descriptor().getConsequent().elements()
								.containsAll(originalAssociationRule.descriptor().getConsequent().elements());
			case NON_SUB_PATTERNS:
				return originalAssociationRule != associationRuleToIdentify
						&& (!originalAssociationRule.descriptor().getAntecedent().elements()
								.containsAll(associationRuleToIdentify.descriptor().getAntecedent().elements())
								|| !originalAssociationRule.descriptor().getConsequent().elements().containsAll(
										associationRuleToIdentify.descriptor().getConsequent().elements()));
			case NON_SUPER_PATTERNS:
				return originalAssociationRule != associationRuleToIdentify
						&& (!associationRuleToIdentify.descriptor().getAntecedent().elements()
								.containsAll(originalAssociationRule.descriptor().getAntecedent().elements())
								|| !associationRuleToIdentify.descriptor().getConsequent().elements()
										.containsAll(originalAssociationRule.descriptor().getConsequent().elements()));
			default:
				return false;
			}
		}

	}

	private static class AttributeListPatternTypeIdentifier implements PatternTypeIdentifier {

		@Override
		public boolean identify(TypeSelectorType type, Pattern<?> originalPattern, Pattern<?> patternToIdentify) {
			if (!AttributeList.class.isAssignableFrom(originalPattern.getClass())
					|| !AttributeList.class.isAssignableFrom(patternToIdentify.getClass())) {
				return false;
			}

			AttributeList originalAttributeList = (AttributeList) originalPattern;
			AttributeList attributeListToIdentify = (AttributeList) patternToIdentify;

			switch (type) {
			case SELECTED:
				return originalAttributeList == attributeListToIdentify;
			case SUB_PATTERNS:
				return originalAttributeList != attributeListToIdentify && originalAttributeList.descriptor()
						.attributes().containsAll(attributeListToIdentify.descriptor().attributes());
			case SUPER_PATTERNS:
				return originalAttributeList != attributeListToIdentify && attributeListToIdentify.descriptor()
						.attributes().containsAll(originalAttributeList.descriptor().attributes());
			case NON_SUB_PATTERNS:
				return originalAttributeList != attributeListToIdentify && !originalAttributeList.descriptor()
						.attributes().containsAll(attributeListToIdentify.descriptor().attributes());
			case NON_SUPER_PATTERNS:
				return originalAttributeList != attributeListToIdentify && !attributeListToIdentify.descriptor()
						.attributes().containsAll(originalAttributeList.descriptor().attributes());
			default:
				return false;
			}
		}

	}

	private static class FunctionalPatternPatternTypeIdentifier implements PatternTypeIdentifier {

		@Override
		public boolean identify(TypeSelectorType type, Pattern<?> originalPattern, Pattern<?> patternToIdentify) {
			if (!FunctionalPattern.class.isAssignableFrom(originalPattern.getClass())
					|| !FunctionalPattern.class.isAssignableFrom(patternToIdentify.getClass())) {
				return false;
			}

			FunctionalPattern originalFunctionalPattern = (FunctionalPattern) originalPattern;
			FunctionalPattern functionalPatternToIdentify = (FunctionalPattern) patternToIdentify;

			switch (type) {
			case SELECTED:
				return originalFunctionalPattern == functionalPatternToIdentify;
			case SUB_PATTERNS:
				return originalFunctionalPattern != functionalPatternToIdentify
						&& originalFunctionalPattern.descriptor().domain()
								.containsAll(functionalPatternToIdentify.descriptor().domain())
						&& originalFunctionalPattern.descriptor().coDomain()
								.containsAll(functionalPatternToIdentify.descriptor().coDomain());
			case SUPER_PATTERNS:
				return originalFunctionalPattern != functionalPatternToIdentify
						&& functionalPatternToIdentify.descriptor().domain()
								.containsAll(originalFunctionalPattern.descriptor().domain())
						&& functionalPatternToIdentify.descriptor().coDomain()
								.containsAll(originalFunctionalPattern.descriptor().coDomain());
			case NON_SUB_PATTERNS:
				return originalFunctionalPattern != functionalPatternToIdentify && (!originalFunctionalPattern
						.descriptor().domain().containsAll(functionalPatternToIdentify.descriptor().domain())
						|| !originalFunctionalPattern.descriptor().coDomain()
								.containsAll(functionalPatternToIdentify.descriptor().coDomain()));
			case NON_SUPER_PATTERNS:
				return originalFunctionalPattern != functionalPatternToIdentify && (!functionalPatternToIdentify
						.descriptor().domain().containsAll(originalFunctionalPattern.descriptor().domain())
						|| !functionalPatternToIdentify.descriptor().coDomain()
								.containsAll(originalFunctionalPattern.descriptor().coDomain()));
			default:
				return false;
			}
		}

	}

	@PatternTypeIdentifierHandler(patternClass = ExceptionalModelPattern.class)
	private static class ExceptionalModelPatternTypeIdentifier implements PatternTypeIdentifier {

		@Override
		public boolean identify(TypeSelectorType type, Pattern<?> originalPattern, Pattern<?> patternToIdentify) {
			if (!ExceptionalModelPattern.class.isAssignableFrom(originalPattern.getClass())
					|| !ExceptionalModelPattern.class.isAssignableFrom(patternToIdentify.getClass())) {
				return false;
			}

			ExceptionalModelPattern originalExceptionalModelPattern = (ExceptionalModelPattern) originalPattern;
			ExceptionalModelPattern exceptionalModelPatternToIdentify = (ExceptionalModelPattern) patternToIdentify;

			switch (type) {
			case SELECTED:
				return originalExceptionalModelPattern == exceptionalModelPatternToIdentify;
			case SUB_PATTERNS:
				return originalExceptionalModelPattern != exceptionalModelPatternToIdentify
						&& originalExceptionalModelPattern.getDeviationMeasure()
								.equals(exceptionalModelPatternToIdentify.getDeviationMeasure())
						&& originalExceptionalModelPattern.descriptor().fittingAlgorithm()
								.equals(exceptionalModelPatternToIdentify.descriptor().fittingAlgorithm())
						&& originalExceptionalModelPattern.descriptor().targetAttributes()
								.containsAll(exceptionalModelPatternToIdentify.descriptor().targetAttributes())
						&& originalExceptionalModelPattern.descriptor().extensionDescriptor().elements().containsAll(
								exceptionalModelPatternToIdentify.descriptor().extensionDescriptor().elements());
			case SUPER_PATTERNS:
				return originalExceptionalModelPattern != exceptionalModelPatternToIdentify
						&& originalExceptionalModelPattern.getDeviationMeasure()
								.equals(exceptionalModelPatternToIdentify.getDeviationMeasure())
						&& originalExceptionalModelPattern.descriptor().fittingAlgorithm()
								.equals(exceptionalModelPatternToIdentify.descriptor().fittingAlgorithm())
						&& exceptionalModelPatternToIdentify.descriptor().targetAttributes()
								.containsAll(originalExceptionalModelPattern.descriptor().targetAttributes())
						&& exceptionalModelPatternToIdentify.descriptor().extensionDescriptor().elements().containsAll(
								originalExceptionalModelPattern.descriptor().extensionDescriptor().elements());
			case NON_SUB_PATTERNS:
				return originalExceptionalModelPattern != exceptionalModelPatternToIdentify
						&& originalExceptionalModelPattern.getDeviationMeasure()
								.equals(exceptionalModelPatternToIdentify.getDeviationMeasure())
						&& originalExceptionalModelPattern.descriptor().fittingAlgorithm()
								.equals(exceptionalModelPatternToIdentify.descriptor().fittingAlgorithm())
						&& (!originalExceptionalModelPattern.descriptor().targetAttributes()
								.containsAll(exceptionalModelPatternToIdentify.descriptor().targetAttributes())
								|| !originalExceptionalModelPattern.descriptor().extensionDescriptor().elements()
										.containsAll(exceptionalModelPatternToIdentify.descriptor()
												.extensionDescriptor().elements()));
			case NON_SUPER_PATTERNS:
				return originalExceptionalModelPattern != exceptionalModelPatternToIdentify
						&& originalExceptionalModelPattern.getDeviationMeasure()
								.equals(exceptionalModelPatternToIdentify.getDeviationMeasure())
						&& originalExceptionalModelPattern.descriptor().fittingAlgorithm()
								.equals(exceptionalModelPatternToIdentify.descriptor().fittingAlgorithm())
						&& (!exceptionalModelPatternToIdentify.descriptor().targetAttributes()
								.containsAll(originalExceptionalModelPattern.descriptor().targetAttributes())
								|| !exceptionalModelPatternToIdentify.descriptor().extensionDescriptor().elements()
										.containsAll(originalExceptionalModelPattern.descriptor().extensionDescriptor()
												.elements()));
			default:
				return false;
			}
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternTypeIdentifiers() {
		throw new AssertionError();
	}

}
