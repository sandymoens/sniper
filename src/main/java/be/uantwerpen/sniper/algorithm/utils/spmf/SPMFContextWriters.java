/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.utils.spmf;

import static com.google.common.collect.Maps.newHashMap;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import be.uantwerpen.sniper.algorithm.utils.ContextWriter;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.sequences.SequenceEvent;
import de.unibonn.realkd.data.sequences.SequenceTransaction;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContext;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SPMFContextWriters {

	public static ContextWriter<SequentialPropositionalContext> sequentialContextWriter() {
		return new SequentialContextWriter();
	}

	private static class SequentialContextWriter implements ContextWriter<SequentialPropositionalContext> {

		@Override
		public boolean writeContext(String fileName, SequentialPropositionalContext context) {
			Map<Proposition, Integer> mapping = newHashMap();

			for (int i = 0; i < context.propositions().size(); i++) {
				mapping.put(context.proposition(i), i);
			}

			try {
				Writer writer = new BufferedWriter(new FileWriter(fileName));

				for (SequenceTransaction transaction : context.sequences()) {
					Object timestamp = null;
					for (SequenceEvent<?> event : transaction.events()) {
						writer.write(String.valueOf(mapping.get(event.proposition())));
						writer.write(" ");
						if (timestamp != null && timestamp != event.value()) {
							writer.write("-1 ");
						}
						timestamp = event.value();
					}
					writer.write("-2\n");
				}

				writer.close();

				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}

			return false;
		}

	}

	// Suppress default constructor for non-instantiability
	private SPMFContextWriters() {
		throw new AssertionError();
	}

}
