/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.patterncollection;

import static be.uantwerpen.sniper.common.Parameters.antecedentRegex;
import static be.uantwerpen.sniper.common.Parameters.consequentRegex;
import static be.uantwerpen.sniper.common.Parameters.logic;

import java.util.Collection;
import java.util.stream.Collectors;

import be.uantwerpen.sniper.algorithm.PatternCollectionPostProcessingAlgorithm;
import be.uantwerpen.sniper.common.LogicOperator;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.Node;

/**
 * 
 * @author Ali Doku
 * @since 0.2.0
 * @version 0.2.0
 */
public class EpisodeRuleRegexFilters {

	public static PatternCollectionPostProcessingAlgorithm newEpisodePatterFilter(
			Collection<? extends Pattern<?>> patterns) {
		return new EpisodeRegexFilter(patterns);
	}

	private static class EpisodeRegexFilter extends AbstractMiningAlgorithm<Pattern<?>>
			implements MeasureBasedPatternFilter {

		private Collection<? extends Pattern<?>> patterns;

		private Parameter<String> antecedentRegex;

		private Parameter<String> consequentRegex;
		private Parameter<LogicOperator> logic;

		private EpisodeRegexFilter(Collection<? extends Pattern<?>> patterns) {
			this.patterns = patterns;

			this.registerParameter(this.antecedentRegex = antecedentRegex());
			this.registerParameter(this.consequentRegex = consequentRegex());
			this.registerParameter(this.logic = logic());
		}

		@Override
		public String caption() {
			return "Episode Rule filter based on Regular Expression";
		}

		@Override
		public String description() {
			return "Filter Episodes based on the Regular Expression ";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.OTHER;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			String antecedentRegex = this.antecedentRegex.current();
			String consequentRegex = this.consequentRegex.current();
			LogicOperator logic = this.logic.current();

			return this.patterns.stream().map(p -> (EpisodeRule) p)
					.filter(p -> EpisodeRule.class.isAssignableFrom(p.getClass())).filter(p -> {
						boolean antecedent = verifyEpisode(logic.name(), p.descriptor().antecedent(), antecedentRegex);
						boolean consequent = verifyEpisode(logic.name(), p.descriptor().consequent(), consequentRegex);
						switch (logic) {
						case and:
							if (antecedent & consequent)
								return true;
							else
								return false;
						case or:
							if (antecedent | consequent)
								return true;
							return false;

						}
						return false;

					}).collect(Collectors.toList());

		}

		private boolean verifyEpisode(String logic, GraphDescriptor episode, String regex) {

			String regexs[] = regex.split(",");
			boolean regexVerification[] = new boolean[episode.nodes().size()];
			boolean satisfyRegex = false;

			for (int i = 0; i < regexs.length; i++) {
				for (int k = 0; k < episode.nodes().size(); k++) {
					// if there is only 1 regular expression
					Node node = episode.nodes().get(k);
					if (node.name().matches(regexs[i].trim())) {
						regexVerification[k] = true;
					}
				}
			}

			for (boolean b : regexVerification)
				if (b)
					satisfyRegex = true;

			return satisfyRegex;

		}

	}

	// Suppress default constructor for non-instantiability
	private EpisodeRuleRegexFilters() {
		throw new AssertionError();
	}

}
