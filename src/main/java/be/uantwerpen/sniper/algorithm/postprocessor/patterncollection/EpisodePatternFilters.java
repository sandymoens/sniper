/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.patterncollection;

import static be.uantwerpen.sniper.common.Parameters.antecedentTypeSelectorGraph;
import static be.uantwerpen.sniper.common.Parameters.consequentTypeSelectorGraph;

import java.util.Collection;
import java.util.stream.Collectors;

import be.uantwerpen.sniper.algorithm.PatternCollectionPostProcessingAlgorithm;
import be.uantwerpen.sniper.common.TypeSelectorEpisodes;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.GraphType;

/**
 * 
 * @author Ali Doku
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class EpisodePatternFilters {

	public static PatternCollectionPostProcessingAlgorithm newEpisodePatterFilter(
			Collection<? extends Pattern<?>> patterns) {
		return new EpisodeRuleTypeFilter(patterns);
	}

	private static class EpisodeRuleTypeFilter extends AbstractMiningAlgorithm<Pattern<?>>
			implements MeasureBasedPatternFilter {

		private Collection<? extends Pattern<?>> patterns;

		private Parameter<TypeSelectorEpisodes> antecedentTypeSelector;

		private Parameter<TypeSelectorEpisodes> consequentTypeSelector;

		private EpisodeRuleTypeFilter(Collection<? extends Pattern<?>> patterns) {
			this.patterns = patterns;

			this.registerParameter(this.antecedentTypeSelector = antecedentTypeSelectorGraph());
			this.registerParameter(this.consequentTypeSelector = consequentTypeSelectorGraph());
		}

		@Override
		public String caption() {
			return "Episode rule type filter";
		}

		@Override
		public String description() {
			return "Filters episode rules based on their graph types";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.OTHER;
		}

		private boolean isType(TypeSelectorEpisodes type, GraphDescriptor graphDescriptor) {
			switch (type) {
			case ANY:
				return true;
			case SERIAL:
				return graphDescriptor.graphType().equals(GraphType.SERIAL);
			case PARALLEL:
				return graphDescriptor.graphType().equals(GraphType.PARALLEL);
			case GENERAL:
				return graphDescriptor.graphType().equals(GraphType.GENERAL);
			default:
			}
			return true;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			TypeSelectorEpisodes antecedentType = this.antecedentTypeSelector.current();
			TypeSelectorEpisodes consequentType = this.consequentTypeSelector.current();

			return this.patterns.stream().filter(p -> {
				if (!(p instanceof EpisodeRule)) {
					return false;
				}

				EpisodeRule r = (EpisodeRule) p;

				return isType(antecedentType, r.descriptor().antecedent())
						&& isType(consequentType, r.descriptor().consequent());
			}).collect(Collectors.toList());
		}

	}

	// Suppress default constructor for non-instantiability
	private EpisodePatternFilters() {
		throw new AssertionError();
	}

}
