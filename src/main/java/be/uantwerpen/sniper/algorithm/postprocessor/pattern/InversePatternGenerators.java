/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.pattern;

import static be.uantwerpen.sniper.common.Parameters.keepOriginalPatternsParameter;
import static com.google.common.collect.Lists.newArrayList;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import com.google.common.collect.Sets;

import be.uantwerpen.sniper.algorithm.PatternPostProcessingAlgorithm;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class InversePatternGenerators {

	private static List<Pair<Class<? extends Pattern<?>>, InversePatternGeneratorHandler>> handlers;

	public static void register(Class<? extends Pattern<?>> clazz, InversePatternGeneratorHandler handler) {
		handlers.add(Pair.of(clazz, handler));
	}

	static {
		handlers = newArrayList();

		Reflections reflections = new Reflections(new ConfigurationBuilder()
				.addUrls(ClasspathHelper.forPackage("be.uantwerpen.sniper.algorithm.postprocessor.pattern"))
				.setScanners(new MethodAnnotationsScanner()));

		reflections.getMethodsAnnotatedWith(InversePatternGeneratorHandlerAnn.class).stream().forEach(m -> {
			try {
				register(m.getAnnotation(InversePatternGeneratorHandlerAnn.class).patternClass(),
						(InversePatternGeneratorHandler) m.invoke(null));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}

	private static Optional<InversePatternGeneratorHandler> handler(Class<? extends Pattern<?>> class1) {
		for (Pair<Class<? extends Pattern<?>>, InversePatternGeneratorHandler> handler : handlers) {
			if (handler.getKey().isAssignableFrom(class1)) {
				return Optional.of(handler.getValue());
			}
		}

		return Optional.empty();
	}

	public static PatternPostProcessingAlgorithm newInversePatternsGenerator(Collection<? extends Pattern<?>> patterns,
			Collection<Integer> patternIndices) {
		return new GenericSubPatternsGenerator(patterns, patternIndices);
	}

	private static class GenericSubPatternsGenerator extends AbstractMiningAlgorithm<Pattern<?>>
			implements SubPatternsGenerator {

		private Collection<? extends Pattern<?>> patterns;

		private Set<Integer> patternIndices;

		private RangeEnumerableParameter<Boolean> keepOriginalPatternsParameter;

		private GenericSubPatternsGenerator(Collection<? extends Pattern<?>> patterns,
				Collection<Integer> patternIndices) {
			this.patterns = patterns;
			this.patternIndices = Sets.newHashSet(patternIndices);

			this.registerParameter(this.keepOriginalPatternsParameter = keepOriginalPatternsParameter());
		}

		@Override
		public String caption() {
			return "Inverse pattern generator";
		}

		@Override
		public String description() {
			return "Generates the inverse patterns for the selected collection of patterns";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.GENERIC;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			Collection<Pattern<?>> patterns = newArrayList();

			int i = 0;

			Iterator<? extends Pattern<?>> iterator = this.patterns.iterator();

			while (iterator.hasNext()) {
				Pattern<?> pattern = iterator.next();

				if (super.stopRequested()) {
					patterns.add(pattern);
					continue;
				}

				if (this.patternIndices.contains(i)) {
					@SuppressWarnings("unchecked")
					Optional<InversePatternGeneratorHandler> handler = handler(
							(Class<? extends Pattern<?>>) pattern.getClass());

					if (handler.isPresent()) {
						if (this.keepOriginalPatternsParameter.current()) {
							patterns.add(pattern);
						}

						Optional<Pattern<?>> inversePattern = handler.get().generate(pattern);

						if (inversePattern.isPresent()) {
							patterns.add(inversePattern.get());
						}
					} else {
						patterns.add(pattern);
					}
				} else {
					patterns.add(pattern);
				}

				i++;

			}

			return patterns;
		}

	}

	// Suppress default constructor for non-instantiability
	private InversePatternGenerators() {
		throw new AssertionError();
	}

}
