/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.patterncollection;

import static be.uantwerpen.sniper.common.Parameters.measureParameter;
import static be.uantwerpen.sniper.common.Parameters.orderTypeParameter;
import static be.uantwerpen.sniper.common.Parameters.valueParameter;
import static java.util.Comparator.naturalOrder;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.Lists;

import be.uantwerpen.sniper.algorithm.PatternCollectionPostProcessingAlgorithm;
import be.uantwerpen.sniper.common.OrderType;
import be.uantwerpen.sniper.common.Parameters;
import be.uantwerpen.sniper.utils.MeasureRegister;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class MeasureBasedPatternFilters {

	public static PatternCollectionPostProcessingAlgorithm newMeasureBasedPatternFilter(Workspace workspace,
			List<Measure> availableMeasures, Collection<? extends Pattern<?>> patterns) {
		return new GenericMeasureBasedPatternFilter(workspace, availableMeasures, patterns);
	}

	public static PatternCollectionPostProcessingAlgorithm newMeasureBasedOutlierPatternFilter(Workspace workspace,
			List<Measure> availableMeasures, Collection<? extends Pattern<?>> patterns) {
		return new GenericMeasureBasedOutlierPatternFilter(workspace, availableMeasures, patterns);
	}

	private static class GenericMeasureBasedPatternFilter extends AbstractMiningAlgorithm<Pattern<?>>
			implements MeasureBasedPatternFilter {

		private Collection<? extends Pattern<?>> patterns;

		private Parameter<Measure> measureParameter;

		private Parameter<Double> valueParameter;

		private Parameter<String> orderTypeParameter;

		private GenericMeasureBasedPatternFilter(Workspace workspace, List<Measure> availableMeasures,
				Collection<? extends Pattern<?>> patterns) {
			this.patterns = patterns;

			this.registerParameter(this.measureParameter = measureParameter(availableMeasures));
			this.registerParameter(this.valueParameter = valueParameter());
			this.registerParameter(this.orderTypeParameter = orderTypeParameter());
		}

		@Override
		public String caption() {
			return "Measure based pattern filter";
		}

		@Override
		public String description() {
			return "Filter all patterns that do not satisfy the given measure constraint";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.GENERIC;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			Measure measure = this.measureParameter.current();

			Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> procedures = MeasureRegister
					.measureRegister().getMeasurementProcedures(measure);

			double minValue = this.valueParameter.current();
			OrderType orderType = OrderType.valueOf(this.orderTypeParameter.current().toUpperCase().replace(" ", "_"));

			return this.patterns.stream().filter(p -> {
				double value = Double.NaN;
				if (!p.hasMeasure(measure)) {
					PatternDescriptor descriptor = p.descriptor();

					for (MeasurementProcedure<? extends Measure, ? super PatternDescriptor> procedure : procedures) {
						if (procedure.isApplicable(descriptor)) {
							value = procedure.perform(descriptor).value();
						}
					}

					if (Double.isNaN(value)) {
						return false;
					}
				} else {
					value = p.measurement(measure).get().value();
				}

				switch (orderType) {
				case LARGER_OR_EQUAL_THAN:
					return value >= minValue;
				case LARGER_THAN:
					return value > minValue;
				case SMALLER_THAN:
					return value < minValue;
				case SMALLER_OR_EQUAL_THAN:
					return value <= minValue;
				default:
					break;
				}

				return false;
			}).collect(Collectors.toList());
		}

	}

	private static class GenericMeasureBasedOutlierPatternFilter extends AbstractMiningAlgorithm<Pattern<?>>
			implements MeasureBasedPatternFilter {

		private Collection<? extends Pattern<?>> patterns;

		private Parameter<Measure> measureParameter;

		private Parameter<Double> zValueParameter;

		private GenericMeasureBasedOutlierPatternFilter(Workspace workspace, List<Measure> availableMeasures,
				Collection<? extends Pattern<?>> patterns) {
			this.patterns = patterns;

			this.registerParameter(this.measureParameter = measureParameter(availableMeasures));
			this.registerParameter(this.zValueParameter = Parameters.zValueParameter());
		}

		@Override
		public String caption() {
			return "Measure based outlier pattern filter";
		}

		@Override
		public String description() {
			return "Filter all patterns that are (no) outliers given the z-value using the median method.";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.GENERIC;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			Measure measure = this.measureParameter.current();

			Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> procedures = MeasureRegister
					.measureRegister().getMeasurementProcedures(measure);

			double zValue = this.zValueParameter.current();

			List<Pair<Double, ? extends Pattern<?>>> pairs = Lists.newArrayList(this.patterns.stream().map(p -> {
				if (!p.hasMeasure(measure)) {
					PatternDescriptor descriptor = p.descriptor();

					for (MeasurementProcedure<? extends Measure, ? super PatternDescriptor> procedure : procedures) {
						if (procedure.isApplicable(descriptor)) {
							return Pair.of(procedure.perform(descriptor).value(), p);
						}
					}

					return Pair.of(Double.NaN, p);
				}

				return Pair.of(p.measurement(measure).get().value(), p);
			}).filter(p -> !Double.isNaN(p.getLeft())).collect(Collectors.toList()));

			List<Double> values = pairs.stream().map(p -> p.getLeft()).collect(Collectors.toList());
			values.sort(naturalOrder());

			double mean = 0;

			for (double value : values) {
				mean += value;
			}

			mean /= pairs.size();

			double stdev = 0;

			for (double value : values) {
				double diff = value - mean;
				stdev += diff * diff;
			}

			stdev = Math.sqrt(stdev / pairs.size());

			double std = stdev;

			double median = values.get((int) (values.size() * 0.5));

			return pairs.stream()
					.filter(p -> !(p.getLeft() >= median - (zValue * std) && p.getLeft() <= median + (zValue * std)))
					.map(p -> p.getRight()).collect(Collectors.toList());
		}

	}

	// Suppress default constructor for non-instantiability
	private MeasureBasedPatternFilters() {
		throw new AssertionError();
	}

}
