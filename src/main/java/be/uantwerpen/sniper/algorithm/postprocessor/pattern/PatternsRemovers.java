/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.pattern;

import static be.uantwerpen.sniper.algorithm.utils.PatternTypeIdentifiers.newPatternTypeIdentifier;
import static be.uantwerpen.sniper.common.Parameters.typeSelectorParameter;
import static com.google.common.collect.Lists.newArrayList;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import be.uantwerpen.sniper.algorithm.PatternPostProcessingAlgorithm;
import be.uantwerpen.sniper.algorithm.utils.PatternTypeIdentifier;
import be.uantwerpen.sniper.common.TypeSelectorType;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternsRemovers {

	public static PatternPostProcessingAlgorithm newPatternsRemover(Collection<? extends Pattern<?>> patterns,
			Collection<Integer> patternIndices) {
		return new GenericPatternsRemover(patterns, patternIndices);
	}

	private static class GenericPatternsRemover extends AbstractMiningAlgorithm<Pattern<?>> implements PatternsRemover {

		private List<? extends Pattern<?>> patterns;

		private Set<Integer> patternIndices;

		private DefaultRangeEnumerableParameter<TypeSelectorType> typeSelectorParameter;

		private GenericPatternsRemover(Collection<? extends Pattern<?>> patterns, Collection<Integer> patternIndices) {
			this.patterns = ImmutableList.copyOf(patterns);
			this.patternIndices = Sets.newHashSet(patternIndices);

			this.registerParameter(this.typeSelectorParameter = typeSelectorParameter());
		}

		@Override
		public String caption() {
			return "Patterns remover";
		}

		@Override
		public String description() {
			return "Removes all patterns that satisfy the given constraints";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.GENERIC;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			int i = 0;

			Iterator<? extends Pattern<?>> iterator = this.patterns.iterator();

			List<Pattern<?>> selectedPatterns = this.patternIndices.stream().map(ix -> this.patterns.get(ix))
					.collect(Collectors.toList());

			List<Integer> indicesToRemove = newArrayList();

			while (iterator.hasNext()) {
				Pattern<?> pattern = iterator.next();

				if (super.stopRequested()) {
					break;
				}

				PatternTypeIdentifier patternTypeIdentifier = newPatternTypeIdentifier();

				for (Pattern<?> selectedPattern : selectedPatterns) {
					if (patternTypeIdentifier.identify(this.typeSelectorParameter.current(), selectedPattern,
							pattern)) {
						indicesToRemove.add(i);
						break;
					}
				}

				i++;
			}

			Collections.reverse(indicesToRemove);

			List<Pattern<?>> patterns = newArrayList(this.patterns);

			for (int indexToRemove : indicesToRemove) {
				patterns.remove(indexToRemove);
			}

			return patterns;
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternsRemovers() {
		throw new AssertionError();
	}

}
