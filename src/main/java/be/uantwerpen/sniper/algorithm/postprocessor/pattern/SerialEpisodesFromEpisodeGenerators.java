/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.pattern;

import static be.uantwerpen.sniper.common.Parameters.keepOriginalPatternsParameter;
import static com.google.common.collect.Lists.newArrayList;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import be.uantwerpen.sniper.algorithm.PatternPostProcessingAlgorithm;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.episodes.Episode;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptors;
import de.unibonn.realkd.patterns.episodes.Episodes;
import de.unibonn.realkd.patterns.graphs.Edge;
import de.unibonn.realkd.patterns.graphs.Edges;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SerialEpisodesFromEpisodeGenerators {

	public static PatternPostProcessingAlgorithm newSerialEpisodesFromEpisodeGenerator(
			Collection<? extends Pattern<?>> patterns, Collection<Integer> patternIndices) {
		return new DefaultSerialEpisodesFromEpisodeGenerator(patterns, patternIndices);
	}

	private static class DefaultSerialEpisodesFromEpisodeGenerator extends AbstractMiningAlgorithm<Pattern<?>>
			implements PatternPostProcessingAlgorithm {

		private Collection<? extends Pattern<?>> patterns;

		private Set<Integer> patternIndices;

		private RangeEnumerableParameter<Boolean> keepOriginalPatternsParameter;

		private DefaultSerialEpisodesFromEpisodeGenerator(Collection<? extends Pattern<?>> patterns,
				Collection<Integer> patternIndices) {
			this.patterns = patterns;
			this.patternIndices = Sets.newHashSet(patternIndices);

			this.registerParameter(this.keepOriginalPatternsParameter = keepOriginalPatternsParameter());
		}

		@Override
		public String caption() {
			return "Serial episodes from episode generator";
		}

		@Override
		public String description() {
			return "Generates all single serial episode from an episodes";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.GENERIC;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			Collection<Pattern<?>> patterns = newArrayList();

			int i = 0;

			Iterator<? extends Pattern<?>> iterator = this.patterns.iterator();

			while (iterator.hasNext()) {
				Pattern<?> pattern = iterator.next();

				if (super.stopRequested()) {
					patterns.add(pattern);
					continue;
				}

				if (this.patternIndices.contains(i)) {
					if (this.keepOriginalPatternsParameter.current()) {
						patterns.add(pattern);
					}

					if (Episode.class.isAssignableFrom(pattern.getClass())) {
						patterns.addAll(generateEpisodeRules((Episode) pattern));
					}
				} else {
					patterns.add(pattern);
				}

				i++;

			}

			return patterns;
		}

		private Collection<Pattern<?>> generateEpisodeRules(Episode episode) {
			Collection<Pattern<?>> patterns = newArrayList();

			GraphDescriptor graph = episode.descriptor().graph();

			List<Integer> nodeIds = getStartingNodes(graph);

			Map<Integer, List<Integer>> startToEndsMap = getEdgesMap(graph.edges());

			Map<Integer, Node> nodeMap = getNodeMap(graph.nodes());

			for (Integer nodeId : nodeIds) {
				List<List<Integer>> patternsEdges = expand(startToEndsMap, Lists.newArrayList(nodeId),
						startToEndsMap.get(nodeId));

				for (List<Integer> patternEdges : patternsEdges) {
					List<Node> nodes = patternEdges.stream().map(i -> nodeMap.get(i)).collect(Collectors.toList());
					List<Edge> edges = Lists.newArrayList();

					for (int i = 1; i < patternEdges.size(); i++) {
						edges.add(Edges.create(patternEdges.get(i - 1), patternEdges.get(i)));
					}

					patterns.add(Episodes.create(
							EpisodeDescriptors.create(episode.descriptor().propositionalContext(),
									episode.descriptor().windowSize(), GraphDescriptors.create(nodes, edges)),
							ImmutableList.of(), ImmutableList.of()));
				}
			}

			return patterns;
		}

		private Map<Integer, Node> getNodeMap(List<Node> nodes) {
			return nodes.stream().collect(Collectors.toMap(n -> n.id(), n -> n));
		}

		private List<List<Integer>> expand(Map<Integer, List<Integer>> edgesMap, List<Integer> currentPatternEdges,
				List<Integer> extensions) {
			if (extensions == null) {
				return ImmutableList.of(currentPatternEdges);
			}

			List<List<Integer>> patternEdges = Lists.newArrayList();

			for (Integer extension : extensions) {
				List<Integer> newPatternEdges = Lists.newArrayList(currentPatternEdges);
				newPatternEdges.add(extension);

				patternEdges.addAll(expand(edgesMap, newPatternEdges, edgesMap.get(extension)));
			}

			return patternEdges;
		}

		private List<Integer> getStartingNodes(GraphDescriptor graph) {
			List<Integer> nodeIds = graph.nodes().stream().map(n -> n.id()).collect(Collectors.toList());

			for (Edge edge : graph.edges()) {
				nodeIds.remove(new Integer(edge.end()));
			}

			return nodeIds;
		}

		private Map<Integer, List<Integer>> getEdgesMap(List<Edge> edges) {
			Map<Integer, List<Integer>> map = Maps.newHashMap();

			for (Edge edge : edges) {
				List<Integer> list = map.get(edge.start());

				if (list == null) {
					map.put(edge.start(), list = newArrayList());
				}

				list.add(edge.end());
			}

			return map;
		}

	}

	// Suppress default constructor for non-instantiability
	private SerialEpisodesFromEpisodeGenerators() {
		throw new AssertionError();
	}

}
