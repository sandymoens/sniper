/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.pattern;

import static com.google.common.collect.Lists.newArrayList;
import static de.unibonn.realkd.patterns.functional.FunctionalPatterns.binaryAttributeSetRelation;
import static de.unibonn.realkd.patterns.functional.FunctionalPatterns.functionalPattern;

import java.util.Optional;

import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.functional.FunctionalDependencyMeasure;
import de.unibonn.realkd.patterns.functional.FunctionalPattern;
import de.unibonn.realkd.patterns.rules.AssociationRule;
import de.unibonn.realkd.patterns.rules.AssociationRules;
import de.unibonn.realkd.patterns.rules.DefaultRuleDescriptor;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class InversePatternGeneratorHandlers {

	@InversePatternGeneratorHandlerAnn(patternClass = AssociationRule.class)
	public static InversePatternGeneratorHandler inverseAssociationRuleGenerator() {
		return new InverseAssociationRuleGenerator();
	}

	@InversePatternGeneratorHandlerAnn(patternClass = FunctionalPattern.class)
	public static InversePatternGeneratorHandler inverseFunctionalPatternGenerator() {
		return new InverseFunctionalPatternGenerator();
	}

	private static class InverseAssociationRuleGenerator implements InversePatternGeneratorHandler {

		@Override
		public Optional<Pattern<?>> generate(Pattern<?> pattern) {
			if (!AssociationRule.class.isAssignableFrom(pattern.getClass())) {
				return Optional.empty();
			}

			AssociationRule associationRule = (AssociationRule) pattern;
			Population population = associationRule.descriptor().population();

			return Optional.of(AssociationRules.create(DefaultRuleDescriptor.create(population,
					associationRule.descriptor().getConsequent(), associationRule.descriptor().getAntecedent()),
					newArrayList()));
		}

	}

	private static class InverseFunctionalPatternGenerator implements InversePatternGeneratorHandler {

		@Override
		public Optional<Pattern<?>> generate(Pattern<?> pattern) {
			if (!FunctionalPattern.class.isAssignableFrom(pattern.getClass())) {
				return Optional.empty();
			}

			FunctionalPattern functionalPattern = (FunctionalPattern) pattern;
			DataTable table = functionalPattern.descriptor().table();

			return Optional.of(functionalPattern(
					binaryAttributeSetRelation(table, functionalPattern.descriptor().coDomain(),
							functionalPattern.descriptor().domain()),
					(FunctionalDependencyMeasure) functionalPattern.functionalityMeasure(), new Measurement[0]));
		}

	}

	// Suppress default constructor for non-instantiability
	private InversePatternGeneratorHandlers() {
		throw new AssertionError();
	}

}
