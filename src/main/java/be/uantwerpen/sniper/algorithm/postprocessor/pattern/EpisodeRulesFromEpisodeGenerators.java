/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.pattern;

import static be.uantwerpen.sniper.common.Parameters.keepOriginalPatternsParameter;
import static com.google.common.collect.Lists.newArrayList;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import be.uantwerpen.sniper.algorithm.PatternPostProcessingAlgorithm;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.episodes.Episode;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRules;
import de.unibonn.realkd.patterns.graphs.Edge;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class EpisodeRulesFromEpisodeGenerators {

	public static PatternPostProcessingAlgorithm newEpisodeRulesFromEpisodeGenerator(
			Collection<? extends Pattern<?>> patterns, Collection<Integer> patternIndices) {
		return new DefaultEpisodeRulesFromEpisodeGenerator(patterns, patternIndices);
	}

	private static class DefaultEpisodeRulesFromEpisodeGenerator extends AbstractMiningAlgorithm<Pattern<?>>
			implements PatternPostProcessingAlgorithm {

		private Collection<? extends Pattern<?>> patterns;

		private Set<Integer> patternIndices;

		private RangeEnumerableParameter<Boolean> keepOriginalPatternsParameter;

		private DefaultEpisodeRulesFromEpisodeGenerator(Collection<? extends Pattern<?>> patterns,
				Collection<Integer> patternIndices) {
			this.patterns = patterns;
			this.patternIndices = Sets.newHashSet(patternIndices);

			this.registerParameter(this.keepOriginalPatternsParameter = keepOriginalPatternsParameter());
		}

		@Override
		public String caption() {
			return "Episode rule from episode generator";
		}

		@Override
		public String description() {
			return "Generates episode rules from episodes";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.GENERIC;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			Collection<Pattern<?>> patterns = newArrayList();

			int i = 0;

			Iterator<? extends Pattern<?>> iterator = this.patterns.iterator();

			while (iterator.hasNext()) {
				Pattern<?> pattern = iterator.next();

				if (super.stopRequested()) {
					patterns.add(pattern);
					continue;
				}

				if (this.patternIndices.contains(i)) {
					if (this.keepOriginalPatternsParameter.current()) {
						patterns.add(pattern);
					}

					if (Episode.class.isAssignableFrom(pattern.getClass())) {
						patterns.addAll(generateEpisodeRules((Episode) pattern));
					}
				} else {
					patterns.add(pattern);
				}

				i++;

			}

			return patterns;
		}

		private Collection<Pattern<?>> generateEpisodeRules(Episode episode) {
			Collection<Pattern<?>> patterns = newArrayList();

			GraphDescriptor graph = episode.descriptor().graph();

			List<Edge> edges = graph.edges();

			List<Integer> nodeIds = graph.nodes().stream().map(n -> n.id()).collect(Collectors.toList());

			for (Edge edge : edges) {
				nodeIds.remove(new Integer(edge.start()));
			}

			for (Integer nodeId : nodeIds) {
				patterns.add(EpisodeRules.create(
						EpisodeRuleDescriptors.create(episode.descriptor().propositionalContext(),
								episode.descriptor().windowSize(), graph.generalization(nodeId), graph),
						ImmutableList.of(), ImmutableList.of()));
			}

			return patterns;
		}

	}

	// Suppress default constructor for non-instantiability
	private EpisodeRulesFromEpisodeGenerators() {
		throw new AssertionError();
	}

}
