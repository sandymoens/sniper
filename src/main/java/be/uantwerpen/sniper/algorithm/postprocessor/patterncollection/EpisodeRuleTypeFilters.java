/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.patterncollection;

import static be.uantwerpen.sniper.rest.resource.common.Utils.extractAttributeNameFromProposition;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import be.uantwerpen.sniper.algorithm.PatternCollectionPostProcessingAlgorithm;
import be.uantwerpen.sniper.common.ParametersEpisodes;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.sequences.TableBasedSingleSequencePropositionalContext;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.graphs.Node;

/**
 * 
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class EpisodeRuleTypeFilters {

	public static PatternCollectionPostProcessingAlgorithm newEpisodeRuleTypeFilter(Workspace workspace,
			Collection<? extends Pattern<?>> patterns) {
		return new DefaultEpisodeRuleTypeFilter(workspace, patterns);
	}

	private static class DefaultEpisodeRuleTypeFilter extends AbstractMiningAlgorithm<Pattern<?>>
			implements MeasureBasedPatternFilter {

		private TableBasedSingleSequencePropositionalContext context;

		private Collection<? extends Pattern<?>> patterns;

		private SubCollectionParameter<String, List<String>> typeInAntecedentFilterParameter;

		private SubCollectionParameter<String, List<String>> typeInConsequentFilterParameter;

		private SubCollectionParameter<String, List<String>> typeNotInAntecedentFilterParameter;

		private SubCollectionParameter<String, List<String>> typeNotInConsequentFilterParameter;

		private DefaultEpisodeRuleTypeFilter(Workspace workspace, Collection<? extends Pattern<?>> patterns) {
			this.context = (TableBasedSingleSequencePropositionalContext) workspace.propositionalContexts().get(0);

			this.patterns = patterns;

			this.registerParameter(this.typeInAntecedentFilterParameter = ParametersEpisodes
					.includedTypesParameter("Antecedent types", "Types to be used in the antecedent", this.context));
			this.registerParameter(this.typeInConsequentFilterParameter = ParametersEpisodes
					.includedTypesParameter("Consequent types", "Types to be used in the consequent", this.context));

			this.registerParameter(this.typeNotInAntecedentFilterParameter = ParametersEpisodes.includedTypesParameter(
					"Non antecedent types", "Types not to be used in the antecedent", this.context));
			this.registerParameter(this.typeNotInConsequentFilterParameter = ParametersEpisodes.includedTypesParameter(
					"Non consequent types", "Types not to be used in the consequent", this.context));
		}

		@Override
		public String caption() {
			return "Episode rule filter based on types";
		}

		@Override
		public String description() {
			return "Episode rule filter based on types";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.OTHER;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			List<String> typesInAntecedent = this.typeInAntecedentFilterParameter.current();
			List<String> typesInConsequent = this.typeInConsequentFilterParameter.current();
			List<String> typesNotInAntecedent = this.typeNotInAntecedentFilterParameter.current();
			List<String> typesNotInConsequent = this.typeNotInConsequentFilterParameter.current();

			return this.patterns.stream().filter(p -> EpisodeRule.class.isAssignableFrom(p.getClass())).filter(p -> {
				EpisodeRule er = (EpisodeRule) p;

				return checkTypeOccurs(er.descriptor().antecedent().nodes(), typesInAntecedent)
						&& checkTypeOccurs(er.descriptor().consequent().nodes(), typesInConsequent)
						&& checkTypeNotOccurs(er.descriptor().antecedent().nodes(), typesNotInAntecedent)
						&& checkTypeNotOccurs(er.descriptor().consequent().nodes(), typesNotInConsequent);

			}).collect(Collectors.toList());
		}

		private boolean checkTypeOccurs(List<Node> nodes, List<String> types) {
			if (types.isEmpty()) {
				return true;
			}

			Set<String> typesInList = nodes.stream().map(n -> extractAttributeNameFromProposition(n.proposition()))
					.collect(Collectors.toSet());

			return typesInList.containsAll(types);
		}

		private boolean checkTypeNotOccurs(List<Node> nodes, List<String> types) {
			if (types.isEmpty()) {
				return true;
			}

			Set<String> typesInList = nodes.stream().map(n -> extractAttributeNameFromProposition(n.proposition()))
					.collect(Collectors.toSet());

			for (String type : types) {
				if (typesInList.contains(type)) {
					return false;
				}
			}

			return true;
		}

	}

	// Suppress default constructor for non-instantiability
	private EpisodeRuleTypeFilters() {
		throw new AssertionError();
	}

}
