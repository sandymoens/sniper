/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.pattern;

import static be.uantwerpen.sniper.common.Parameters.keepOriginalPatternsParameter;
import static be.uantwerpen.sniper.common.ParametersEpisodes.includedTypeParameter;
import static be.uantwerpen.sniper.rest.resource.common.Utils.extractAttributeNameFromProposition;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import be.uantwerpen.sniper.algorithm.PatternPostProcessingAlgorithm;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.sequences.TableBasedSingleSequencePropositionalContext;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptor;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRules;
import de.unibonn.realkd.patterns.episodes.EpisodeSupport;
import de.unibonn.realkd.patterns.graphs.Edge;
import de.unibonn.realkd.patterns.graphs.Edges;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;
import de.unibonn.realkd.patterns.graphs.Nodes;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SimpleExtendedEpisodeRuleGenerator {

	public static PatternPostProcessingAlgorithm newSimpleExtendedEpisodeRuleGenerator(Workspace workspace,
			Collection<? extends Pattern<?>> patterns, Collection<Integer> patternIndices) {
		return new DefaultSimpleExtendedEpisodeRuleGenerator(workspace, patterns, patternIndices);
	}

	private static class DefaultSimpleExtendedEpisodeRuleGenerator extends AbstractMiningAlgorithm<Pattern<?>>
			implements PatternPostProcessingAlgorithm {

		private TableBasedSingleSequencePropositionalContext context;

		private Collection<? extends Pattern<?>> patterns;

		private Set<Integer> patternIndices;

		private RangeEnumerableParameter<Boolean> keepOriginalPatternsParameter;

		private RangeEnumerableParameter<String> typeFilterParameter;

		private DefaultSimpleExtendedEpisodeRuleGenerator(Workspace workspace,
				Collection<? extends Pattern<?>> patterns, Collection<Integer> patternIndices) {
			this.context = (TableBasedSingleSequencePropositionalContext) workspace.propositionalContexts().get(0);

			this.patterns = patterns;
			this.patternIndices = Sets.newHashSet(patternIndices);

			this.registerParameter(this.keepOriginalPatternsParameter = keepOriginalPatternsParameter());

			this.registerParameter(this.typeFilterParameter = includedTypeParameter("Type",
					"Types to be used in the construction of specialised episodes", this.context));
		}

		@Override
		public String caption() {
			return "Simple extended episode rule generator";
		}

		@Override
		public String description() {
			return "Generates rules by adding an additional node to the left hand side of an episode rule";
		}

		@Override
		public AlgorithmCategory getCategory() {
			return AlgorithmCategory.EPISODE_MINING;
		}

		@Override
		protected Collection<Pattern<?>> concreteCall() throws ValidationException {
			String attribute = this.typeFilterParameter.current();

			List<Proposition> propositions = this.context.propositions().stream()
					.filter(p -> extractAttributeNameFromProposition(p).equals(attribute)).collect(toList());

			Collection<Pattern<?>> patterns = newArrayList();

			int i = 0;

			Iterator<? extends Pattern<?>> iterator = this.patterns.iterator();

			while (iterator.hasNext()) {
				Pattern<?> pattern = iterator.next();

				if (super.stopRequested()) {
					patterns.add(pattern);
					continue;
				}

				if (this.patternIndices.contains(i)) {
					if (this.keepOriginalPatternsParameter.current()) {
						patterns.add(pattern);
					}

					if (pattern instanceof EpisodeRule) {
						patterns.addAll(generateEpisodeRules((EpisodeRule) pattern, propositions));
					}
				} else {
					patterns.add(pattern);
				}

				i++;

			}

			return patterns;
		}

		private Collection<Pattern<?>> generateEpisodeRules(EpisodeRule episodeRule, List<Proposition> propositions) {
			Collection<Pattern<?>> patterns = newArrayList();

			for (Proposition proposition : propositions) {
				EpisodeRuleDescriptor descriptor = specialization(episodeRule, proposition);

				Measurement support = EpisodeSupport.EPISODE_SUPPORT.perform(descriptor);

				if (support.value() == 0) {
					continue;
				}

				patterns.add(EpisodeRules.create(descriptor, ImmutableList.of(), ImmutableList.of()));
			}

			return patterns;
		}

		private EpisodeRuleDescriptor specialization(EpisodeRule episodeRule, Proposition proposition) {
			GraphDescriptor antecedentOld = episodeRule.descriptor().antecedent();
			GraphDescriptor consequentOld = episodeRule.descriptor().consequent();

			int newId = consequentOld.nodes().stream().mapToInt(n -> n.id()).max().getAsInt() + 1;

			Node addition = Nodes.create(newId, proposition);

			List<Node> nodesAntecedent = Lists.newArrayList(antecedentOld.nodes());
			nodesAntecedent.add(addition);
			GraphDescriptor antecedentNew = GraphDescriptors.create(nodesAntecedent, antecedentOld.edges());

			List<Node> nodesConsequent = Lists.newArrayList(consequentOld.nodes());
			nodesConsequent.add(addition);
			List<Edge> edgesConsequent = Lists.newArrayList(consequentOld.edges());
			edgesConsequent.add(Edges.create(newId, edgesConsequent.get(0).end()));
			GraphDescriptor consequentNew = GraphDescriptors.create(nodesConsequent, edgesConsequent);

			return EpisodeRuleDescriptors.create(episodeRule.descriptor().propositionalContext(),
					episodeRule.descriptor().windowSize(), antecedentNew, consequentNew);
		}

	}

	// Suppress default constructor for non-instantiability
	private SimpleExtendedEpisodeRuleGenerator() {
		throw new AssertionError();
	}

}
