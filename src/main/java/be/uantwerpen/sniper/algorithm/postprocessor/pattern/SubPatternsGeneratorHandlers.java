/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.postprocessor.pattern;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.powerSet;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import be.uantwerpen.sniper.patterns.attributes.AttributeList;
import be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptors;
import be.uantwerpen.sniper.patterns.attributes.AttributeLists;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.association.Associations;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.functional.FunctionalDependencyMeasure;
import de.unibonn.realkd.patterns.functional.FunctionalPattern;
import de.unibonn.realkd.patterns.functional.FunctionalPatterns;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.rules.AssociationRule;
import de.unibonn.realkd.patterns.rules.AssociationRules;
import de.unibonn.realkd.patterns.rules.DefaultRuleDescriptor;
import de.unibonn.realkd.patterns.subgroups.Subgroups;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SubPatternsGeneratorHandlers {

	@SubPatternsGeneratorHandlerAnn(patternClass = Association.class)
	public static SubPatternsGeneratorHandler associationSubPatternsGenerator() {
		return new AssociationSubPatternsGenerator();
	}

	@SubPatternsGeneratorHandlerAnn(patternClass = AssociationRule.class)
	public static SubPatternsGeneratorHandler associationRuleSubPatternsGenerator() {
		return new AssociationRuleSubPatternsGenerator();
	}

	@SubPatternsGeneratorHandlerAnn(patternClass = AttributeList.class)
	public static SubPatternsGeneratorHandler attributeListSubPatternsGenerator() {
		return new AttributeListSubPatternsGenerator();
	}

	@SubPatternsGeneratorHandlerAnn(patternClass = FunctionalPattern.class)
	public static SubPatternsGeneratorHandler functionalPatternSubPatternsGenerator() {
		return new FunctionalPatternSubPatternsGenerator();
	}

	@SubPatternsGeneratorHandlerAnn(patternClass = ExceptionalModelPattern.class)
	public static SubPatternsGeneratorHandler exceptionalModelPatternSubPatternsGenerator() {
		return new ExceptionalModelPatternSubPatternsGenerator();
	}

	private static <T> List<Pattern<?>> generateSubPatterns(Collection<T> elements,
			Function<? super Set<T>, ? extends Pattern<?>> generator) {
		int size = elements.size();

		List<Pattern<?>> patterns = Set.class.isAssignableFrom(elements.getClass())
				? powerSet((Set<T>) elements).stream().filter(s -> !s.isEmpty() && s.size() != size)
						.sorted((s1, s2) -> Integer.compare(s1.size(), s2.size())).map(generator).collect(toList())
				: powerSet(ImmutableSet.copyOf(elements)).stream().filter(s -> !s.isEmpty() && s.size() != size)
						.sorted((s1, s2) -> Integer.compare(s1.size(), s2.size())).map(generator).collect(toList());

		Collections.reverse(patterns);

		return patterns;
	}

	private static class AssociationSubPatternsGenerator implements SubPatternsGeneratorHandler {

		@Override
		public List<Pattern<?>> generate(Pattern<?> pattern) {
			if (!Association.class.isAssignableFrom(pattern.getClass())) {
				return ImmutableList.of();
			}

			Association association = (Association) pattern;
			Collection<Proposition> elements = association.descriptor().elements();

			return generateSubPatterns(elements, s -> {
				return Associations.association(LogicalDescriptors.create(association.descriptor().population(), s));
			});
		}

	}

	private static class AssociationRuleSubPatternsGenerator implements SubPatternsGeneratorHandler {

		@Override
		public List<Pattern<?>> generate(Pattern<?> pattern) {
			if (!AssociationRule.class.isAssignableFrom(pattern.getClass())) {
				return ImmutableList.of();
			}

			AssociationRule associationRule = (AssociationRule) pattern;
			Collection<Proposition> elements = associationRule.descriptor().getAntecedent().elements();

			return generateSubPatterns(elements, s -> {
				return AssociationRules.create(DefaultRuleDescriptor.create(associationRule.descriptor().population(),
						LogicalDescriptors.create(associationRule.descriptor().getAntecedent().population(), s),
						associationRule.descriptor().getConsequent()), Lists.newArrayList());
			});
		}

	}

	private static class AttributeListSubPatternsGenerator implements SubPatternsGeneratorHandler {

		@Override
		public List<Pattern<?>> generate(Pattern<?> pattern) {
			if (!AttributeList.class.isAssignableFrom(pattern.getClass())) {
				return ImmutableList.of();
			}

			AttributeList attributeList = (AttributeList) pattern;
			List<Attribute<?>> elements = attributeList.descriptor().attributes();

			return generateSubPatterns(elements, s -> {
				return AttributeLists.attributeList(attributeList.population(), AttributeListDescriptors
						.attributeListDescriptor(attributeList.descriptor().table(), ImmutableList.copyOf(s)),
						ImmutableList.of());
			});
		}

	}

	private static class FunctionalPatternSubPatternsGenerator implements SubPatternsGeneratorHandler {

		@Override
		public List<Pattern<?>> generate(Pattern<?> pattern) {
			if (!FunctionalPattern.class.isAssignableFrom(pattern.getClass())) {
				return ImmutableList.of();
			}

			FunctionalPattern functionalPattern = (FunctionalPattern) pattern;
			FunctionalDependencyMeasure measure = (FunctionalDependencyMeasure) functionalPattern
					.functionalityMeasure();
			Set<Attribute<?>> elements = functionalPattern.descriptor().domain();

			return generateSubPatterns(elements, s -> {
				return FunctionalPatterns.functionalPattern(
						FunctionalPatterns.binaryAttributeSetRelation(functionalPattern.descriptor().table(), s,
								functionalPattern.descriptor().coDomain()),
						measure, new Measurement[0]);
			});
		}

	}

	private static class ExceptionalModelPatternSubPatternsGenerator implements SubPatternsGeneratorHandler {

		@Override
		public List<Pattern<?>> generate(Pattern<?> pattern) {
			if (!ExceptionalModelPattern.class.isAssignableFrom(pattern.getClass())) {
				return ImmutableList.of();
			}

			ExceptionalModelPattern exceptionalModalPattern = (ExceptionalModelPattern) pattern;
			Collection<Proposition> elements = exceptionalModalPattern.descriptor().extensionDescriptor().elements();

			return generateSubPatterns(elements, s -> {
				return ExceptionalModelMining.emmPattern(
						Subgroups.subgroup(
								LogicalDescriptors.create(
										exceptionalModalPattern.descriptor().extensionDescriptor().population(), s),
								exceptionalModalPattern.descriptor().getTargetTable(),
								exceptionalModalPattern.descriptor().targetAttributes(),
								exceptionalModalPattern.descriptor().fittingAlgorithm()),
						exceptionalModalPattern.getDeviationMeasure(), newArrayList());
			});
		}

	}

	// Suppress default constructor for non-instantiability
	private SubPatternsGeneratorHandlers() {
		throw new AssertionError();
	}

}