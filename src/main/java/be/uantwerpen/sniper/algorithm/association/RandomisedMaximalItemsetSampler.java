/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.association;

import static be.uantwerpen.sniper.common.Parameters.minSupportParameter;
import static be.uantwerpen.util.WeightingSchemes.adaptiveWeightingScheme;
import static be.uantwerpen.util.WeightingSchemes.additiveWeightingScheme;
import static be.uantwerpen.util.WeightingSchemes.multiplicativeWeightingScheme;
import static be.uantwerpen.util.WeightingSchemes.noWeightScheme;
import static de.unibonn.realkd.common.base.Identifier.id;
import static de.unibonn.realkd.common.parameter.Parameters.rangeEnumerableParameter;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import be.uantwerpen.algorithms.itemsets.RandomMaximalItemsetSampler;
import be.uantwerpen.sniper.common.Parameters;
import be.uantwerpen.util.SetReporter;
import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.StoppableMiningAlgorithm;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.association.Associations;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class RandomisedMaximalItemsetSampler extends AbstractMiningAlgorithm<Association>
		implements StoppableMiningAlgorithm {

	private static final ImmutableList<String> WEIGHTING_SCHEMES = ImmutableList.of("No weighting", "Additive weights",
			"Multiplicative weights", "Adaptive weights");

	private PropositionalContext context;

	private RandomMaximalItemsetSampler sampler;

	private Parameter<Integer> minSupportParameter;

	private Parameter<String> weightingSchemeParameter;

	private Parameter<Integer> numberOfResultsParameter;

	public RandomisedMaximalItemsetSampler(Workspace workspace) {
		this.context = workspace.propositionalContexts().get(0);

		this.sampler = null;

		this.registerParameter(this.minSupportParameter = minSupportParameter(this.context.population().size()));
		this.registerParameter(this.weightingSchemeParameter = rangeEnumerableParameter(id("Weighting_Schemes"),
				"Weighting scheme", "Weighting scheme used for discounting singletons after a new patterns is sampled.",
				String.class, () -> WEIGHTING_SCHEMES));
		this.registerParameter(this.numberOfResultsParameter = Parameters.numberOfResultsParameter());
	}

	@Override
	public String caption() {
		return "Randomised Maximal Itemset Sampling";
	}

	@Override
	public String description() {
		return "Finds a sample of maximal itemsets given the constraints (implementation by Moens)";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.ASSOCIATION_MINING;
	}

	@Override
	protected Collection<Association> concreteCall() throws ValidationException {
		initializeSampler();

		final List<Association> associations = Lists.newArrayList();

		this.sampler.setReporter(new SetReporter() {

			@Override
			public void close() {
			}

			@Override
			public void report(LogicalDescriptor descriptor) {
				associations.add(Associations.association(descriptor));
			}

		});

		this.sampler.run(this.minSupportParameter.current(), this.numberOfResultsParameter.current());

		this.sampler = null;

		return associations;
	}

	private void initializeSampler() {
		this.sampler = new RandomMaximalItemsetSampler(this.context);

		if (this.weightingSchemeParameter.current().equals("No Weighting")) {
			this.sampler.weightingScheme(noWeightScheme());
		} else if (this.weightingSchemeParameter.current().equals("Additive weights")) {
			this.sampler.weightingScheme(additiveWeightingScheme());
		} else if (this.weightingSchemeParameter.current().equals("Multiplicative weights")) {
			this.sampler.weightingScheme(multiplicativeWeightingScheme());
		} else if (this.weightingSchemeParameter.current().equals("Adaptive weights")) {
			this.sampler.weightingScheme(adaptiveWeightingScheme());
		}
	}

	@Override
	protected void onStopRequest() {
		if (this.sampler != null) {
			this.sampler.stop();
		}
	}

}
