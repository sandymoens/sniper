/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm.association;

import static be.uantwerpen.sniper.algorithm.utils.borgelt.BorgeltFIMContextWriters.borgeltContextWriter;
import static be.uantwerpen.sniper.algorithm.utils.borgelt.BorgeltFIMPatternReaders.associationPatternReader;
import static be.uantwerpen.sniper.common.Parameters.maxFrequencyParameter;
import static be.uantwerpen.sniper.common.Parameters.maxSizeParameter;
import static be.uantwerpen.sniper.common.Parameters.minFrequencyParameter;
import static be.uantwerpen.sniper.common.Parameters.minSizeParameter;
import static be.uantwerpen.sniper.common.Parameters.superPatternsOnlyParameter;
import static be.uantwerpen.sniper.config.Settings.PLUGIN_DIR;
import static com.google.common.collect.Lists.newArrayList;
import static de.unibonn.realkd.common.base.Identifier.id;
import static de.unibonn.realkd.common.parameter.Parameters.rangeEnumerableParameter;

import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.algorithm.AbstractExternalMiningAlgorithm;
import be.uantwerpen.sniper.config.Settings;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Eclat extends AbstractExternalMiningAlgorithm<Association> {

	public static enum PatternType {
		FREQUENT, CLOSED, MAXIMAL
	}

	private PropositionalContext context;

	private List<? extends Pattern<?>> patterns;

	private Collection<Integer> patternIndices;

	private Parameter<PatternType> patternTypeParameter;

	private Parameter<Double> minFrequencyParameter;

	private Parameter<Double> maxFrequencyParameter;

	private Parameter<Integer> minSizeParameter;

	private Parameter<Integer> maxSizeParameter;

	private Parameter<Boolean> superPatternsOnly;

	public Eclat(Workspace workspace, List<? extends Pattern<?>> patterns, Collection<Integer> patternIndices) {
		this.context = workspace.propositionalContexts().get(0);
		this.patterns = patterns;
		this.patternIndices = patternIndices;

		this.registerParameter(this.patternTypeParameter = rangeEnumerableParameter(id("Pattern Type"), "Pattern Type",
				"Type of pattern", PatternType.class, () -> ImmutableList.copyOf(PatternType.values())));
		this.registerParameter(this.minFrequencyParameter = minFrequencyParameter());
		this.registerParameter(this.maxFrequencyParameter = maxFrequencyParameter());
		this.registerParameter(this.minSizeParameter = minSizeParameter());
		this.registerParameter(this.maxSizeParameter = maxSizeParameter());

		if (this.patternIndices.size() > 0) {
			this.registerParameter(this.superPatternsOnly = superPatternsOnlyParameter());
		}
	}

	@Override
	public String caption() {
		return "Eclat";
	}

	@Override
	public String description() {
		return "Finds all frequent associations given the constraints (implementation by Borgelt)";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.ASSOCIATION_MINING;
	}

	@Override
	protected boolean writeDbFile() {
		return borgeltContextWriter().writeContext(this.dbFile.getAbsolutePath(), this.context);
	}

	@Override
	protected Collection<Association> readOutputFile() {
		List<Association> minedPatterns = ImmutableList
				.copyOf(associationPatternReader().readPatterns(this.outputFile.getAbsolutePath(), this.context));

		if (this.patternIndices.isEmpty() || !this.superPatternsOnly.current()) {
			return minedPatterns;
		}

		List<Association> associations = this.patternIndices.stream().map(i -> this.patterns.get(i))
				.filter(p -> Association.class.isAssignableFrom(p.getClass())).map(p -> (Association) p)
				.collect(Collectors.toList());

		return minedPatterns.stream().filter(p -> {
			for (Association association : associations) {
				if (p.descriptor().elements().containsAll(association.descriptor().elements())) {
					return true;
				}
			}
			return false;
		}).collect(Collectors.toList());

	}

	@Override
	protected List<String> command() {
		double minFrequency = this.minFrequencyParameter.current() * 100;
		double maxFrequency = this.maxFrequencyParameter.current() * 100;
		int minSize = this.minSizeParameter.current();
		int maxSize = this.maxSizeParameter.current();

		String t;
		switch (this.patternTypeParameter.current()) {
		case CLOSED:
			t = "c";
			break;
		case FREQUENT:
			t = "s";
			break;
		case MAXIMAL:
			t = "m";
			break;
		default:
			t = "s";
			break;
		}

		return newArrayList(Paths.get(Settings.Instance.getProperty(PLUGIN_DIR), "eclat").toString(),
				String.format("-t%s", t), String.format("-s%.5f", minFrequency), String.format("-S%.5f", maxFrequency),
				String.format("-m%d", minSize), String.format("-n%d", maxSize), this.dbFile.getAbsolutePath(),
				this.outputFile.getAbsolutePath());
	}

}
