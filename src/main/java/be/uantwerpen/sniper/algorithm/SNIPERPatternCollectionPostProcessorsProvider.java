/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.algorithm;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.algorithm.postprocessor.patterncollection.EpisodePatternFilters;
import be.uantwerpen.sniper.algorithm.postprocessor.patterncollection.EpisodeRuleRegexFilters;
import be.uantwerpen.sniper.algorithm.postprocessor.patterncollection.EpisodeRuleTypeFilters;
import be.uantwerpen.sniper.algorithm.postprocessor.patterncollection.MeasureBasedPatternFilters;
import de.unibonn.realkd.patterns.Pattern;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SNIPERPatternCollectionPostProcessorsProvider implements PatternCollectionPostProcessorProvider {

	@Override
	public Collection<PatternCollectionPostProcessor> get() {
		return ImmutableList.copyOf(new PatternCollectionPostProcessor[] {

				new PatternCollectionPostProcessor() {
					@Override
					public String caption() {
						return "Measure based pattern filter";
					}

					@Override
					public PatternCollectionPostProcessingAlgorithm newPostProcessor(ActiveWorksheet activeWorksheet,
							List<Pattern<?>> patterns) {
						return MeasureBasedPatternFilters.newMeasureBasedPatternFilter(
								activeWorksheet.realKDWorkspace(), activeWorksheet.measureModel().availableMeasures(),
								patterns);
					};
				},

				new PatternCollectionPostProcessor() {
					@Override
					public String caption() {
						return "Measure based outlier pattern filter";
					}

					@Override
					public PatternCollectionPostProcessingAlgorithm newPostProcessor(ActiveWorksheet activeWorksheet,
							List<Pattern<?>> patterns) {
						return MeasureBasedPatternFilters.newMeasureBasedOutlierPatternFilter(
								activeWorksheet.realKDWorkspace(), activeWorksheet.measureModel().availableMeasures(),
								patterns);
					};
				},

				new PatternCollectionPostProcessor() {
					@Override
					public String caption() {
						return "Episode rule type filter";
					}

					@Override
					public PatternCollectionPostProcessingAlgorithm newPostProcessor(ActiveWorksheet activeWorksheet,
							List<Pattern<?>> patterns) {
						return EpisodePatternFilters.newEpisodePatterFilter(patterns);
					};
				},

				new PatternCollectionPostProcessor() {
					@Override
					public String caption() {
						return "Episode pattern regex filter";
					}

					@Override
					public PatternCollectionPostProcessingAlgorithm newPostProcessor(ActiveWorksheet activeWorksheet,
							List<Pattern<?>> patterns) {
						return EpisodeRuleRegexFilters.newEpisodePatterFilter(patterns);
					};
				},

				new PatternCollectionPostProcessor() {
					@Override
					public String caption() {
						return "Episode rule required type filter";
					}

					@Override
					public PatternCollectionPostProcessingAlgorithm newPostProcessor(ActiveWorksheet activeWorksheet,
							List<Pattern<?>> patterns) {
						return EpisodeRuleTypeFilters.newEpisodeRuleTypeFilter(activeWorksheet.realKDWorkspace(),
								patterns);
					};
				}

		});

	}

}
