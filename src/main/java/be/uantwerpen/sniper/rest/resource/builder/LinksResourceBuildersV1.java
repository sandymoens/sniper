/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.common.Utils.method;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.hateoas.ResourceSupport;

import be.uantwerpen.sniper.controller.rest.DataTableController;
import be.uantwerpen.sniper.controller.rest.RoleController;
import be.uantwerpen.sniper.controller.rest.SchemeController;
import be.uantwerpen.sniper.controller.rest.UserController;
import be.uantwerpen.sniper.controller.rest.WorksheetController;
import be.uantwerpen.sniper.controller.rest.WorkspaceController;
import be.uantwerpen.sniper.dao.entity.Role;
import be.uantwerpen.sniper.dao.entity.User;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class LinksResourceBuildersV1 {

	private static ResourceBuilder<User, ResourceSupport> linksResourceBuilder;

	public static ResourceBuilder<User, ResourceSupport> linksResourceBuilder() {
		return linksResourceBuilder != null ? linksResourceBuilder
				: (linksResourceBuilder = new LinksResourceBuilder());
	}

	private static boolean isAdmin(User user) {
		for (Role role : user.getRoles()) {
			if (role.getName().equals("ROLE_ADMIN")) {
				return true;
			}
		}

		return false;
	}

	private static class LinksResourceBuilder implements ResourceBuilder<User, ResourceSupport> {

		@Override
		public ResourceSupport build(User user) {
			ResourceSupport resource = new ResourceSupport();

			resource.add(linkTo(DataTableController.class,
					method(DataTableController.class, "dataTables", Principal.class, HttpServletRequest.class).get(),
					(Principal) null, (HttpServletRequest) null).withRel("dataTables"));
			resource.add(linkTo(SchemeController.class,
					method(SchemeController.class, "schemes", Principal.class, HttpServletRequest.class).get(),
					(Principal) null, (HttpServletRequest) null).withRel("schemes"));
			resource.add(linkTo(WorksheetController.class,
					method(WorksheetController.class, "worksheets", HttpServletRequest.class).get(),
					(HttpServletRequest) null).withRel("worksheets"));
			resource.add(linkTo(WorkspaceController.class,
					method(WorkspaceController.class, "workspaces", Principal.class, HttpServletRequest.class).get(),
					(Principal) null, (HttpServletRequest) null).withRel("workspaces"));
			resource.add(linkTo(UserController.class,
					method(UserController.class, "user", Integer.class, HttpServletRequest.class).get(), user.getId(),
					(HttpServletRequest) null).withRel("userInfo"));

			if (isAdmin(user)) {
				resource.add(linkTo(UserController.class,
						method(UserController.class, "users", HttpServletRequest.class).get(),
						(HttpServletRequest) null).withRel("users"));
				resource.add(linkTo(RoleController.class,
						method(RoleController.class, "roles", HttpServletRequest.class).get(),
						(HttpServletRequest) null).withRel("roles"));
			}

			return resource;
		}

	}

	// Suppress default constructor for non-instantiability
	private LinksResourceBuildersV1() {
		throw new AssertionError();
	}

}
