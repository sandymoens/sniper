/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import be.uantwerpen.sniper.dao.SchemeMetaInfos.DFFilterType;
import be.uantwerpen.sniper.rest.resource.AttributeResourcesV1.SchemeAttributeResource;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SchemeMetaInfoResourcesV1 {

	public static interface SchemeMetaInfoResource {
	}

	public static class DataTableSchemeMetaInfoResource extends ResourceSupport implements SchemeMetaInfoResource {

		public final String schemeType = "dataTable";

		public final String delimiter;

		public final String dateTimeFormat;

		public final List<SchemeAttributeResource> attributes;

		public DataTableSchemeMetaInfoResource(String delimiter, String dateTimeFormat,
				List<SchemeAttributeResource> attributes) {
			this.delimiter = delimiter;
			this.dateTimeFormat = dateTimeFormat;
			this.attributes = attributes;
		}

	}

	public static class TransactionsSchemeMetaInfoResource extends ResourceSupport implements SchemeMetaInfoResource {

		public final String schemeType = "transactions";

		public final String delimiter;

		public TransactionsSchemeMetaInfoResource(String delimiter) {
			this.delimiter = delimiter;
		}

	}

	public static class TextAsTransactionsSchemeMetaInfoResource extends ResourceSupport
			implements SchemeMetaInfoResource {

		public final String schemeType = "textAsTransactions";

		public final boolean recognizeSentences;

		public final boolean alphabeticalOnly;

		public final boolean filterPOS;

		public final boolean stemTokens;

		public final DFFilterType dFFilterType;

		public final int dFFilterValue;

		public TextAsTransactionsSchemeMetaInfoResource(boolean recognizeSentences, boolean alphabeticalOnly,
				boolean filterPOS, boolean stemTokens, DFFilterType dFFilterType, int dFFilterValue) {
			this.recognizeSentences = recognizeSentences;
			this.alphabeticalOnly = alphabeticalOnly;
			this.filterPOS = filterPOS;
			this.stemTokens = stemTokens;
			this.dFFilterType = dFFilterType;
			this.dFFilterValue = dFFilterValue;
		}

	}

	public static class TextAsBowTfIdfSchemeMetaInfoResource extends ResourceSupport implements SchemeMetaInfoResource {

		public final String schemeType = "textAsBowTfIdf";

		public final boolean recognizeSentences;

		public final boolean alphabeticalOnly;

		public final boolean filterPOS;

		public final boolean stemTokens;

		public final int numberOfBins;

		public TextAsBowTfIdfSchemeMetaInfoResource(boolean recognizeSentences, boolean alphabeticalOnly,
				boolean filterPOS, boolean stemTokens, int numberOfBins) {
			this.recognizeSentences = recognizeSentences;
			this.alphabeticalOnly = alphabeticalOnly;
			this.filterPOS = filterPOS;
			this.stemTokens = stemTokens;
			this.numberOfBins = numberOfBins;
		}

	}

	// Suppress default constructor for non-instantiability
	private SchemeMetaInfoResourcesV1() {
		throw new AssertionError();
	}

}
