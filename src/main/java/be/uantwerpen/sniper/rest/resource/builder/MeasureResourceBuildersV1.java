/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static java.util.stream.Collectors.toList;

import java.util.List;

import be.uantwerpen.sniper.model.MeasureModel;
import be.uantwerpen.sniper.rest.resource.MeasureResourcesV1;
import de.unibonn.realkd.common.measures.Measure;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class MeasureResourceBuildersV1 {

	private static ResourceBuilder<Measure, MeasureResourcesV1.MeasureResource> measuresResourceBuilder;

	private static ResourceBuilder<MeasureModel, List<MeasureResourcesV1.MeasureWithStateResource>> measuresWithStateResourceBuilder;

	public static ResourceBuilder<Measure, MeasureResourcesV1.MeasureResource> measuresResourceBuilder() {
		return measuresResourceBuilder != null ? measuresResourceBuilder
				: (measuresResourceBuilder = new MeasureResourceBuilder());
	}

	public static ResourceBuilder<MeasureModel, List<MeasureResourcesV1.MeasureWithStateResource>> measuresWithStateResourceBuilder() {
		return measuresWithStateResourceBuilder != null ? measuresWithStateResourceBuilder
				: (measuresWithStateResourceBuilder = new MeasuresWithStateResourceBuilder());
	}

	private static class MeasureResourceBuilder
			implements ResourceBuilder<Measure, MeasureResourcesV1.MeasureResource> {

		@Override
		public MeasureResourcesV1.MeasureResource build(Measure measure) {
			return new MeasureResourcesV1.MeasureResource(measure.toString(), measure.caption(), measure.description());
		}

	}

	private static class MeasuresWithStateResourceBuilder
			implements ResourceBuilder<MeasureModel, List<MeasureResourcesV1.MeasureWithStateResource>> {

		@Override
		public List<MeasureResourcesV1.MeasureWithStateResource> build(MeasureModel measureModel) {
			return measureModel.availableMeasures().stream()
					.map(m -> new MeasureResourcesV1.MeasureWithStateResource(m.toString(), m.caption(),
							m.description(), measureModel.status(m)))
					.collect(toList());
		}

	}

	// Suppress default constructor for non-instantiability
	private MeasureResourceBuildersV1() {
		throw new AssertionError();
	}

}
