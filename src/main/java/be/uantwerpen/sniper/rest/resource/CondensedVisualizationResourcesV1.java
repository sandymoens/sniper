/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import be.uantwerpen.sniper.statistics.Count;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class CondensedVisualizationResourcesV1 {

	public static class CategoriesVisualizationResource implements CondensedVisualizationResourceV1 {

		public final String type = "categories";

		public final List<Pair<String, Double>> categories;

		public CategoriesVisualizationResource(List<Pair<String, Double>> categories) {
			this.categories = categories;
		}

	}

	public static class BoxPlotVisualizationResource implements CondensedVisualizationResourceV1 {

		public final String type = "boxPlot";

		public final double min;

		public final double max;

		public final double mean;

		public final double median;

		public final double q1;

		public final double q3;

		public BoxPlotVisualizationResource(double min, double max, double mean, double median, double q1, double q3) {
			this.min = min;
			this.max = max;
			this.mean = mean;
			this.median = median;
			this.q1 = q1;
			this.q3 = q3;
		}

	}

	public static class NormalDistributionVisualizationResource implements CondensedVisualizationResourceV1 {

		public final String type = "normalDistribution";

		public final double mean;

		public final double stdev;

		public NormalDistributionVisualizationResource(double mean, double stdev) {
			this.mean = mean;
			this.stdev = stdev;
		}

	}

	public static class MetricHistogramVisualizationResource implements CondensedVisualizationResourceV1 {

		public final String type = "metricHistogram";

		public final List<Count> counts;

		public MetricHistogramVisualizationResource(List<Count> counts) {
			this.counts = counts;
		}

	}

	public static class DateHistogramVisualizationResource implements CondensedVisualizationResourceV1 {

		public final String type = "dateHistogram";

		public final List<Count> counts;

		public DateHistogramVisualizationResource(List<Count> counts) {
			this.counts = counts;
		}

	}

	// Suppress default constructor for non-instantiability
	private CondensedVisualizationResourcesV1() {
		throw new AssertionError();
	}

}
