/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.utils.MeasureRegister.measureRegister;
import static be.uantwerpen.sniper.utils.ValueFormatters.valueFormatter;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.model.MeasureModel;
import be.uantwerpen.sniper.model.bricks.Brick;
import be.uantwerpen.sniper.model.bricks.Bricks.PatternBrick;
import be.uantwerpen.sniper.patterns.attributes.AttributeList;
import be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptor;
import be.uantwerpen.sniper.rest.resource.DescriptorResourcesV1.IdentifiableListResource;
import be.uantwerpen.sniper.rest.resource.PatternDescriptorResourcesV1.GraphDescriptorResource;
import be.uantwerpen.sniper.rest.resource.PatternResourceV1;
import be.uantwerpen.sniper.rest.resource.PatternResourcesV1;
import be.uantwerpen.sniper.rest.resource.PropositionResourcesV1.SimplePropositionResource;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.episodes.Episode;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.functional.FunctionalPattern;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.rules.AssociationRule;
import de.unibonn.realkd.patterns.rules.RuleDescriptor;
import de.unibonn.realkd.patterns.sequence.Sequence;
import de.unibonn.realkd.patterns.subgroups.Subgroup;

/**
 * @author Sandy Moens
 * @since 0.0.2
 * @version 0.2.0
 */
public class PatternResourceBuildersV1 {

	public static ResourceBuilder<Brick<Pattern<?>, ?>, PatternResourcesV1.IndexedPatternWithMeasurementsResource> indexedPatternWithMeasurementsResourceBuilder(
			ActiveWorksheet activeWorksheet, MeasureModel measureModel) {
		return new IndexedPatternWithMeasurementsResourceBuilder(activeWorksheet, measureModel);
	}

	private static class IndexedPatternWithMeasurementsResourceBuilder implements
			ResourceBuilder<Brick<Pattern<?>, ?>, PatternResourcesV1.IndexedPatternWithMeasurementsResource> {

		private MeasureModel measureModel;

		private AssociationResourceBuilder associationResourceBuilder;
		private AssociationRuleResourceBuilder associationRuleResourceBuilder;
		private AttributeListResourceBuilder attributeListResourceBuilder;
		private ExceptionalModelPatternResourceBuilder exceptionalModelPatternResourceBuilder;
		private SequenceResourceBuilder sequenceResourceBuilder;
		private EpisodeResourceBuilder episodeResourceBuilder;
		private EpisodeRuleResourceBuilder episodeRuleResourceBuilder;
		private FunctionalPatternResourceBuilder functionalPatternResourceBuilder;

		public IndexedPatternWithMeasurementsResourceBuilder(ActiveWorksheet activeWorksheet,
				MeasureModel measureModel) {
			this.measureModel = measureModel;

			this.associationResourceBuilder = new AssociationResourceBuilder(activeWorksheet);
			this.associationRuleResourceBuilder = new AssociationRuleResourceBuilder(activeWorksheet);
			this.attributeListResourceBuilder = new AttributeListResourceBuilder(activeWorksheet);
			this.exceptionalModelPatternResourceBuilder = new ExceptionalModelPatternResourceBuilder(activeWorksheet);
			this.sequenceResourceBuilder = new SequenceResourceBuilder(activeWorksheet);
			this.episodeResourceBuilder = new EpisodeResourceBuilder(activeWorksheet);
			this.episodeRuleResourceBuilder = new EpisodeRuleResourceBuilder(activeWorksheet);
			this.functionalPatternResourceBuilder = new FunctionalPatternResourceBuilder(activeWorksheet);
		}

		@Override
		public PatternResourcesV1.IndexedPatternWithMeasurementsResource build(Brick<Pattern<?>, ?> brick) {
			if (!PatternBrick.class.isAssignableFrom(brick.getClass())) {
				return null;
			}

			Pattern<?> pattern = brick.content();

			ResourceBuilder<PatternBrick<Pattern<?>>, PatternResourceV1> resourceBuilder;

			if (Association.class.isAssignableFrom(pattern.getClass())) {
				resourceBuilder = this.associationResourceBuilder;
			} else if (Episode.class.isAssignableFrom(pattern.getClass())) {
				resourceBuilder = this.episodeResourceBuilder;
			} else if (EpisodeRule.class.isAssignableFrom(pattern.getClass())) {
				resourceBuilder = this.episodeRuleResourceBuilder;
			} else if (AssociationRule.class.isAssignableFrom(pattern.getClass())) {
				resourceBuilder = this.associationRuleResourceBuilder;
			} else if (AttributeList.class.isAssignableFrom(pattern.getClass())) {
				resourceBuilder = this.attributeListResourceBuilder;
			} else if (ExceptionalModelPattern.class.isAssignableFrom(pattern.getClass())) {
				resourceBuilder = this.exceptionalModelPatternResourceBuilder;
			} else if (Sequence.class.isAssignableFrom(pattern.getClass())) {
				resourceBuilder = this.sequenceResourceBuilder;
			} else if (FunctionalPattern.class.isAssignableFrom(pattern.getClass())) {
				resourceBuilder = this.functionalPatternResourceBuilder;
			} else {
				return null;
			}

			@SuppressWarnings("unchecked")
			PatternBrick<Pattern<?>> patternBrick = (PatternBrick<Pattern<?>>) brick;

			return new PatternResourcesV1.IndexedPatternWithMeasurementsResource(resourceBuilder.build(patternBrick),
					getMeasurements(pattern));
		}

		private Map<String, String> getMeasurements(Pattern<?> pattern) {
			Map<String, String> measurements = Maps.newHashMap();

			for (MeasurementProcedure<? extends Measure, ? super PatternDescriptor> measurementProcedure : this.measureModel
					.procedures()) {
				Measure measure = measurementProcedure.getMeasure();

				measurements.put(measure.caption(),
						!pattern.hasMeasure(measure) ? "NaN"
								: valueFormatter().format(measureRegister().measureValueType(measure),
										pattern.measurement(measure).get().value()));
			}

			return measurements;
		}

	}

	private static class AssociationResourceBuilder
			implements ResourceBuilder<PatternBrick<Pattern<?>>, PatternResourceV1> {

		private ActiveWorksheet activeWorksheet;

		public AssociationResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public PatternResourceV1 build(PatternBrick<Pattern<?>> patternBrick) {
			@SuppressWarnings("unchecked")
			Brick<LogicalDescriptor, ?> descriptorBrick = (Brick<LogicalDescriptor, ?>) patternBrick
					.patternDescriptorBrick();

			Brick<?, ?> leftExtensionBrick = patternBrick.utilityBricks().get(0);
			Brick<?, ?> rightExtensionBrick = patternBrick.utilityBricks().get(1);

			return new PatternResourcesV1.AssociationResource(
					DescriptorResourceBuildersV1.logicalDescriptorResourceBuilder(this.activeWorksheet)
							.build(descriptorBrick),
					DescriptorResourceBuildersV1.extensionDescriptorResourceBuilder(this.activeWorksheet)
							.build(leftExtensionBrick),
					DescriptorResourceBuildersV1.extensionDescriptorResourceBuilder(this.activeWorksheet)
							.build(rightExtensionBrick));
		}

	}

	private static class AssociationRuleResourceBuilder
			implements ResourceBuilder<PatternBrick<Pattern<?>>, PatternResourceV1> {

		private ActiveWorksheet activeWorksheet;

		public AssociationRuleResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public PatternResourceV1 build(PatternBrick<Pattern<?>> patternBrick) {
			@SuppressWarnings("unchecked")
			Brick<RuleDescriptor, ?> descriptorBrick = (Brick<RuleDescriptor, ?>) patternBrick.patternDescriptorBrick();
			@SuppressWarnings("unchecked")
			Brick<LogicalDescriptor, ?> antecedentBrick = (Brick<LogicalDescriptor, ?>) descriptorBrick.parts().get(0);
			@SuppressWarnings("unchecked")
			Brick<LogicalDescriptor, ?> consequentBrick = (Brick<LogicalDescriptor, ?>) descriptorBrick.parts().get(1);

			return new PatternResourcesV1.AssociationRuleResource(
					DescriptorResourceBuildersV1.logicalDescriptorResourceBuilder(this.activeWorksheet)
							.build(antecedentBrick),
					DescriptorResourceBuildersV1.logicalDescriptorResourceBuilder(this.activeWorksheet)
							.build(consequentBrick));
		}

	}

	private static class EpisodeResourceBuilder
			implements ResourceBuilder<PatternBrick<Pattern<?>>, PatternResourceV1> {

		private ActiveWorksheet activeWorksheet;

		public EpisodeResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public PatternResourceV1 build(PatternBrick<Pattern<?>> patternBrick) {
			@SuppressWarnings("unchecked")
			Brick<GraphDescriptor, ?> graphBrick = (Brick<GraphDescriptor, ?>) patternBrick.patternDescriptorBrick()
					.parts().get(0);

			ResourceBuilder<Brick<GraphDescriptor, ?>, GraphDescriptorResource> graphDescriptorResourceBuilder = DescriptorResourceBuildersV1
					.graphDescriptorResourceBuilder(this.activeWorksheet);

			Brick<?, ?> leftExtensionBrick = patternBrick.utilityBricks().get(0);
			Brick<?, ?> rightExtensionBrick = patternBrick.utilityBricks().get(1);

			return new PatternResourcesV1.EpisodeResource(graphDescriptorResourceBuilder.build(graphBrick),
					DescriptorResourceBuildersV1.extensionDescriptorResourceBuilder(this.activeWorksheet)
							.build(leftExtensionBrick),
					DescriptorResourceBuildersV1.extensionDescriptorResourceBuilder(this.activeWorksheet)
							.build(rightExtensionBrick));
		}
	}

	private static class EpisodeRuleResourceBuilder
			implements ResourceBuilder<PatternBrick<Pattern<?>>, PatternResourceV1> {

		private ActiveWorksheet activeWorksheet;

		public EpisodeRuleResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public PatternResourceV1 build(PatternBrick<Pattern<?>> object) {
			@SuppressWarnings("unchecked")
			Brick<GraphDescriptor, ?> antecedentBrick = (Brick<GraphDescriptor, ?>) object.patternDescriptorBrick()
					.parts().get(0);
			@SuppressWarnings("unchecked")
			Brick<GraphDescriptor, ?> consequentBrick = (Brick<GraphDescriptor, ?>) object.patternDescriptorBrick()
					.parts().get(1);

			ResourceBuilder<Brick<GraphDescriptor, ?>, GraphDescriptorResource> graphDescriptorResourceBuilder = DescriptorResourceBuildersV1
					.graphDescriptorResourceBuilder(this.activeWorksheet);

			return new PatternResourcesV1.EpisodeRuleResource(graphDescriptorResourceBuilder.build(antecedentBrick),
					graphDescriptorResourceBuilder.build(consequentBrick));
		}
	}

	private static class AttributeListResourceBuilder
			implements ResourceBuilder<PatternBrick<Pattern<?>>, PatternResourceV1> {

		private ActiveWorksheet activeWorksheet;

		public AttributeListResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public PatternResourceV1 build(PatternBrick<Pattern<?>> patternBrick) {
			@SuppressWarnings("unchecked")
			Brick<AttributeListDescriptor, ?> descriptorBrick = (Brick<AttributeListDescriptor, ?>) patternBrick
					.patternDescriptorBrick();

			Brick<?, ?> leftExtensionBrick = patternBrick.utilityBricks().get(0);
			Brick<?, ?> rightExtensionBrick = patternBrick.utilityBricks().get(1);

			return new PatternResourcesV1.AttributeListResource(
					DescriptorResourceBuildersV1.attributeListDescriptorResourceBuilder().build(descriptorBrick),
					DescriptorResourceBuildersV1.extensionDescriptorResourceBuilder(this.activeWorksheet)
							.build(leftExtensionBrick),
					DescriptorResourceBuildersV1.extensionDescriptorResourceBuilder(this.activeWorksheet)
							.build(rightExtensionBrick));
		}

	}

	private static class ExceptionalModelPatternResourceBuilder
			implements ResourceBuilder<PatternBrick<Pattern<?>>, PatternResourceV1> {

		private ActiveWorksheet activeWorksheet;

		public ExceptionalModelPatternResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		@SuppressWarnings("unchecked")
		public PatternResourceV1 build(PatternBrick<Pattern<?>> patternBrick) {
			Brick<Collection<Attribute<?>>, ?> attributesBrick = (Brick<Collection<Attribute<?>>, ?>) patternBrick
					.patternDescriptorBrick().parts().get(1);

			Brick<LogicalDescriptor, ?> descriptorBrick = (Brick<LogicalDescriptor, ?>) patternBrick
					.patternDescriptorBrick().parts().get(0);

			Brick<ModelFactory<?>, Pattern<?>> modelFactoryBrick = (Brick<ModelFactory<?>, Pattern<?>>) patternBrick
					.utilityBricks().get(0);

			return new PatternResourcesV1.ExceptionalModelPatternResource(
					DescriptorResourceBuildersV1.attributeCollectionResourceBuilder().build(attributesBrick),
					DescriptorResourceBuildersV1.logicalDescriptorResourceBuilder(this.activeWorksheet)
							.build(descriptorBrick),
					DescriptorResourceBuildersV1
							.modelFactoryDescriptorResourceBuilder((Subgroup<?>) patternBrick.content().descriptor())
							.build(modelFactoryBrick));
		}

	}

	private static class SequenceResourceBuilder
			implements ResourceBuilder<PatternBrick<Pattern<?>>, PatternResourceV1> {

		private ActiveWorksheet activeWorksheet;

		public SequenceResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		@SuppressWarnings("unchecked")
		public PatternResourceV1 build(PatternBrick<Pattern<?>> patternBrick) {
			List<IdentifiableListResource<SimplePropositionResource>> orderedLists = patternBrick
					.patternDescriptorBrick().parts().stream().map(b -> DescriptorResourceBuildersV1
							.orderedListResourceBuilder(this.activeWorksheet).build((Brick<List<Proposition>, ?>) b))
					.collect(toList());

			List<IdentifiableListResource<SimplePropositionResource>> insertionLists = patternBrick
					.utilityBricks().stream().map(b -> DescriptorResourceBuildersV1
							.orderedListResourceBuilder(this.activeWorksheet).build((Brick<List<Proposition>, ?>) b))
					.collect(toList());

			return new PatternResourcesV1.SequenceResource(orderedLists, insertionLists);
		}

	}

	private static class FunctionalPatternResourceBuilder
			implements ResourceBuilder<PatternBrick<Pattern<?>>, PatternResourceV1> {

		private ActiveWorksheet activeWorksheet;

		public FunctionalPatternResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		@SuppressWarnings("unchecked")
		public PatternResourceV1 build(PatternBrick<Pattern<?>> patternBrick) {
			Brick<Collection<Attribute<?>>, ?> domainBrick = (Brick<Collection<Attribute<?>>, ?>) patternBrick
					.patternDescriptorBrick().parts().get(0);

			Brick<Collection<Attribute<?>>, ?> coDomainBrick = (Brick<Collection<Attribute<?>>, ?>) patternBrick
					.patternDescriptorBrick().parts().get(1);

			return new PatternResourcesV1.FunctionalPatternResource(
					DescriptorResourceBuildersV1.attributeCollectionResourceBuilder().build(domainBrick),
					DescriptorResourceBuildersV1.attributeCollectionResourceBuilder().build(coDomainBrick));
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternResourceBuildersV1() {
		throw new AssertionError();
	}

}
