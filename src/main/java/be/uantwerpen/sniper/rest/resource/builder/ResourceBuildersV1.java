/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import java.util.Map;

import be.uantwerpen.sniper.rest.resource.ResourcesV1;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class ResourceBuildersV1 {

	public static ResourceBuilder<Map<String, String>, ResourcesV1.MapResource<String, String>> stringMapResourceBuilder() {
		return new MapResourceBuilder<>();
	}

	public static ResourceBuilder<Map<String, Object>, ResourcesV1.MapResource<String, Object>> stringObjectMapResourceBuilder() {
		return new MapResourceBuilder<>();
	}

	private static class MapResourceBuilder<T, R> implements ResourceBuilder<Map<T, R>, ResourcesV1.MapResource<T, R>> {

		@Override
		public ResourcesV1.MapResource<T, R> build(Map<T, R> map) {
			return new ResourcesV1.MapResource<>(map);
		}

	}
}
