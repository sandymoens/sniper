/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class UserUploadResourcesV1 {

	public static class UserWithPassword extends ResourceSupport implements UserUploadResourceV1 {

		public String password;

		public String passwordConfirm;

		public UserWithPassword(@JsonProperty("password") String password,
				@JsonProperty("passwordConfirm") String passwordConfirm) {
			this.password = password;
			this.passwordConfirm = passwordConfirm;
		}

	}

	public static class UserWithAuthorities extends ResourceSupport implements UserUploadResourceV1 {

		public List<String> roles;

		public boolean enabled;

		public UserWithAuthorities(@JsonProperty("roles") List<String> roles,
				@JsonProperty("enabled") boolean enabled) {
			this.roles = roles;
			this.enabled = enabled;
		}

	}

}
