/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.common.Utils.method;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Pageable;

import be.uantwerpen.sniper.controller.rest.DataTableController;
import be.uantwerpen.sniper.dao.entity.DataTable;
import be.uantwerpen.sniper.rest.resource.DataTableResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1.ListResource;
import be.uantwerpen.sniper.rest.resource.SchemeResourcesV1.SchemeResource;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class DataTableResourceBuildersV1 {

	private static ResourceBuilder<DataTable, DataTableResourcesV1.DataTableResource> dataTableResourceBuilder;

	private static ResourceBuilder<List<DataTable>, ListResource<DataTableResourcesV1.DataTableResource>> dataTablesResourceBuilder;

	private static ResourceBuilder<DataTable, DataTableResourcesV1.ExpandedDataTableResource> expandedDataTableResourceBuilder;

	private static ResourceBuilder<List<DataTable>, ListResource<DataTableResourcesV1.ExpandedDataTableResource>> expandedDataTablesResourceBuilder;

	public static ResourceBuilder<DataTable, DataTableResourcesV1.DataTableResource> dataTableResourceBuilder() {
		return dataTableResourceBuilder != null ? dataTableResourceBuilder
				: (dataTableResourceBuilder = new DataTableResourceBuilder());
	}

	public static ResourceBuilder<List<DataTable>, ListResource<DataTableResourcesV1.DataTableResource>> dataTablesResourceBuilder() {
		return dataTablesResourceBuilder != null ? dataTablesResourceBuilder
				: (dataTablesResourceBuilder = new DataTablesResourceBuilder());
	}

	public static ResourceBuilder<DataTable, DataTableResourcesV1.ExpandedDataTableResource> expandedDataTableResourceBuilder() {
		return expandedDataTableResourceBuilder != null ? expandedDataTableResourceBuilder
				: (expandedDataTableResourceBuilder = new ExpandedDataTableResourceBuilder());
	}

	public static ResourceBuilder<List<DataTable>, ListResource<DataTableResourcesV1.ExpandedDataTableResource>> expandedDataTablesResourceBuilder() {
		return expandedDataTablesResourceBuilder != null ? expandedDataTablesResourceBuilder
				: (expandedDataTablesResourceBuilder = new ExpandedDataTablesResourceBuilder());
	}

	private static class DataTableResourceBuilder
			implements ResourceBuilder<DataTable, DataTableResourcesV1.DataTableResource> {

		@Override
		public DataTableResourcesV1.DataTableResource build(DataTable dataTable) {
			DataTableResourcesV1.DataTableResource dataTableResource = new DataTableResourcesV1.DataTableResource(
					dataTable.getId(), dataTable.caption(), dataTable.description());

			dataTableResource.add(linkTo(DataTableController.class, method(DataTableController.class, "dataTable",
					String.class, Principal.class, HttpServletRequest.class).get(), dataTable.getId(), null, null)
							.withSelfRel());
			dataTableResource
					.add(linkTo(DataTableController.class,
							method(DataTableController.class, "rows", String.class, Principal.class, Pageable.class,
									HttpServletRequest.class).get(),
							dataTable.getId(), null, null, null).withRel("rows"));

			return dataTableResource;
		}

	}

	private static class DataTablesResourceBuilder
			implements ResourceBuilder<List<DataTable>, ListResource<DataTableResourcesV1.DataTableResource>> {

		@Override
		public ListResource<DataTableResourcesV1.DataTableResource> build(List<DataTable> dataTables) {
			List<DataTableResourcesV1.DataTableResource> dataTableResources = dataTableResourceBuilder()
					.build(dataTables);

			ListResource<DataTableResourcesV1.DataTableResource> listResource = new ResourcesV1.ListResource<>(
					dataTableResources);

			listResource.add(linkTo(
					method(DataTableController.class, "dataTables", Principal.class, HttpServletRequest.class).get(),
					(Principal) null, (HttpServletRequest) null).withSelfRel());

			return listResource;
		}

	}

	private static class ExpandedDataTableResourceBuilder
			implements ResourceBuilder<DataTable, DataTableResourcesV1.ExpandedDataTableResource> {

		@Override
		public DataTableResourcesV1.ExpandedDataTableResource build(DataTable dataTable) {
			List<SchemeResource> schemeResources = SchemeResourceBuildersV1.schemeResourceBuilder()
					.build(dataTable.getSchemes());

			DataTableResourcesV1.ExpandedDataTableResource dataTableResource = new DataTableResourcesV1.ExpandedDataTableResource(
					dataTable.getId(), dataTable.caption(), dataTable.description(), schemeResources);

			dataTableResource.add(linkTo(DataTableController.class, method(DataTableController.class, "dataTable",
					String.class, Principal.class, HttpServletRequest.class).get(), dataTable.getId(), null, null)
							.withSelfRel());
			dataTableResource
					.add(linkTo(DataTableController.class,
							method(DataTableController.class, "rows", String.class, Principal.class, Pageable.class,
									HttpServletRequest.class).get(),
							dataTable.getId(), null, null, null).withRel("rows"));

			return dataTableResource;
		}

	}

	private static class ExpandedDataTablesResourceBuilder
			implements ResourceBuilder<List<DataTable>, ListResource<DataTableResourcesV1.ExpandedDataTableResource>> {

		@Override
		public ListResource<DataTableResourcesV1.ExpandedDataTableResource> build(List<DataTable> dataTables) {
			List<DataTableResourcesV1.ExpandedDataTableResource> dataTableResources = expandedDataTableResourceBuilder()
					.build(dataTables);

			ListResource<DataTableResourcesV1.ExpandedDataTableResource> listResource = new ResourcesV1.ListResource<>(
					dataTableResources);

			listResource.add(linkTo(
					method(DataTableController.class, "dataTables", Principal.class, HttpServletRequest.class).get(),
					(Principal) null, (HttpServletRequest) null).withSelfRel());

			return listResource;
		}

	}

	// Suppress default constructor for non-instantiability
	private DataTableResourceBuildersV1() {
		throw new AssertionError();
	}

}
