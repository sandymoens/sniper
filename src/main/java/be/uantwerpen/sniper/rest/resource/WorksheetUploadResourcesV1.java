/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class WorksheetUploadResourcesV1 {

	public static class DefaultWorksheetUploadResource extends ResourceSupport implements WorksheetUploadResourceV1 {

		public final String schemeId;

		public final String caption;

		public final String description;

		public final WorksheetMetaInfoUploadResourceV1 worksheetMetaInfo;

		public DefaultWorksheetUploadResource(@JsonProperty("schemeId") String schemeId,
				@JsonProperty("caption") String caption, @JsonProperty("description") String description,
				@JsonProperty("worksheetMetaInfo") WorksheetMetaInfoUploadResourceV1 worksheetMetaInfo) {
			this.schemeId = schemeId;
			this.caption = caption;
			this.description = description;
			this.worksheetMetaInfo = worksheetMetaInfo;
		}

		@Override
		public String schemeId() {
			return this.schemeId;
		}

		@Override
		public String caption() {
			return this.caption;
		}

		@Override
		public String description() {
			return this.description;
		}

		@Override
		public WorksheetMetaInfoUploadResourceV1 worksheetMetaInfo() {
			return this.worksheetMetaInfo;
		}

	}

	public static class SaveWorksheetUploadActionResource implements ActionUploadResourceV1 {
	}

	// Suppress default constructor for non-instantiability
	private WorksheetUploadResourcesV1() {
		throw new AssertionError();
	}

}
