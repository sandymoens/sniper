/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;
import java.util.Map;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import com.google.common.collect.ImmutableList;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class ResourcesV1 {

	public static class SimpleIdentifiableResource extends ResourceSupport {

		public String id;

		public SimpleIdentifiableResource(String id) {
			this(id, ImmutableList.of());
		}

		public SimpleIdentifiableResource(String id, Iterable<Link> links) {
			this.id = id;

			this.add(links);
		}

	}

	public static class SimpleResource extends ResourceSupport {

		public SimpleResource(Iterable<Link> links) {
			this.add(links);
		}

	}

	public static class ListResource<T> extends ResourceSupport {

		public final List<T> resources;

		public ListResource(List<T> resources) {
			this.resources = resources;
		}

	}

	public static class MapResource<T, R> extends ResourceSupport {

		public final Map<T, R> map;

		public MapResource(Map<T, R> map) {
			this.map = map;
		}

	}

	// Suppress default constructor for non-instantiability
	private ResourcesV1() {
		throw new AssertionError();
	}
}
