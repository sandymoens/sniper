/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.rest.resource.common.Utils.extractAttributeIconFromActiveWorksheet;
import static be.uantwerpen.sniper.rest.resource.common.Utils.extractAttributeNameFromProposition;
import static be.uantwerpen.sniper.rest.resource.common.Utils.extractPropositionValueFromProposition;
import static be.uantwerpen.sniper.utils.ValueFormatters.valueFormatter;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.core.PatternExtension;
import be.uantwerpen.sniper.core.PropositionPatternExtension;
import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.rest.resource.PatternExtensionResourcesV1.PatternExtensionResource;
import be.uantwerpen.sniper.rest.resource.PatternExtensionResourcesV1.PropositionPatternExtensionResource;
import be.uantwerpen.sniper.rest.resource.PatternExtensionResourcesV1.PropositionPatternExtensionResourceMultiMeasures;
import be.uantwerpen.sniper.utils.ValueType;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternExtensionResourceBuildersV1 {

	public static ResourceBuilder<PatternExtension, PatternExtensionResource> patternExtensionResourceBuilder(
			ActiveWorksheet activeWorksheet, ValueType valueType) {
		return new PatternExtensionResourceBuilder(activeWorksheet, valueType);
	}

	public static ResourceBuilder<PatternExtension, PatternExtensionResource> patternExtensionResourceBuilderMultiMeasures(
			PropositionalContext context, List<ValueType> valueTypes) {
		return new PatternExtensionResourceBuilderMultiMeasures(context, valueTypes);
	}

	private static PropositionsPatternExtensionResourceBuilder propositionsPatternExtensionResourceBuilder(
			ActiveWorksheet activeWorksheet, ValueType valueType) {
		return new PropositionsPatternExtensionResourceBuilder(activeWorksheet, valueType);
	}

	private static ResourceBuilder<PropositionPatternExtension, PropositionPatternExtensionResourceMultiMeasures> propositionsPatternExtensionResourceBuilderMultiMeasures(
			PropositionalContext context, List<ValueType> valueTypes) {
		return new PropositionsPatternExtensionResourceBuilderMultiMeasures(context, valueTypes);
	}

	private static class PatternExtensionResourceBuilder
			implements ResourceBuilder<PatternExtension, PatternExtensionResource> {

		private ActiveWorksheet activeWorksheet;
		private ValueType valueType;

		private PatternExtensionResourceBuilder(ActiveWorksheet activeWorksheet, ValueType valueType) {
			this.activeWorksheet = activeWorksheet;
			this.valueType = valueType;
		}

		@Override
		public PatternExtensionResource build(PatternExtension patternExtension) {
			if (PropositionPatternExtension.class.isAssignableFrom(patternExtension.getClass())) {
				return propositionsPatternExtensionResourceBuilder(this.activeWorksheet, this.valueType)
						.build((PropositionPatternExtension) patternExtension);
			}

			return null;
		}

		@Override
		public List<PatternExtensionResource> build(Collection<PatternExtension> patternExtensions) {
			double min = Double.MAX_VALUE;
			double max = Double.MIN_VALUE;

			for (PatternExtension patternExtension : patternExtensions) {
				double score = patternExtension.values().get(0);
				min = Double.isNaN(score) ? min : min(min, score);
				max = Double.isNaN(score) ? max : max(max, score);
			}

			double diff = max - min;

			double minValue = min;
			double diffValue = diff;

			return patternExtensions.stream().map(patternExtension -> {
				if (PropositionPatternExtension.class.isAssignableFrom(patternExtension.getClass())) {
					return propositionsPatternExtensionResourceBuilder(this.activeWorksheet, this.valueType)
							.build((PropositionPatternExtension) patternExtension, minValue, diffValue);
				}

				return null;
			}).collect(toList());
		}
	}

	private static class PatternExtensionResourceBuilderMultiMeasures
			implements ResourceBuilder<PatternExtension, PatternExtensionResource> {

		private PropositionalContext context;
		private List<ValueType> valueTypes;

		private PatternExtensionResourceBuilderMultiMeasures(PropositionalContext context, List<ValueType> valueTypes) {
			this.context = context;
			this.valueTypes = valueTypes;
		}

		@Override
		public PatternExtensionResource build(PatternExtension patternExtension) {
			if (PropositionPatternExtension.class.isAssignableFrom(patternExtension.getClass())) {
				return propositionsPatternExtensionResourceBuilderMultiMeasures(this.context, this.valueTypes)
						.build((PropositionPatternExtension) patternExtension);
			}

			return null;
		}

		@Override
		public List<PatternExtensionResource> build(Collection<PatternExtension> patternExtensions) {
			return patternExtensions.stream().map(patternExtension -> {
				if (PropositionPatternExtension.class.isAssignableFrom(patternExtension.getClass())) {
					return propositionsPatternExtensionResourceBuilderMultiMeasures(this.context, this.valueTypes)
							.build((PropositionPatternExtension) patternExtension);
				}

				return null;
			}).collect(toList());
		}
	}

	private static class PropositionsPatternExtensionResourceBuilder
			implements ResourceBuilder<PropositionPatternExtension, PropositionPatternExtensionResource> {

		private ActiveWorksheet activeWorksheet;
		private ValueType valueType;

		private PropositionsPatternExtensionResourceBuilder(ActiveWorksheet activeWorksheet, ValueType valueType) {
			this.activeWorksheet = activeWorksheet;
			this.valueType = valueType;
		}

		@Override
		public PropositionPatternExtensionResource build(PropositionPatternExtension propositionPatternExtension) {
			Proposition p = propositionPatternExtension.proposition();

			int id = EmptyProposition.class.isInstance(p) ? -1
					: this.activeWorksheet.propositionalContext().index(p).get();
			String attributeName = extractAttributeNameFromProposition(p);
			String attributeIcon = extractAttributeIconFromActiveWorksheet(attributeName, this.activeWorksheet);
			String propositionValue = extractPropositionValueFromProposition(p);
			double score = propositionPatternExtension.values().get(0);

			return new PropositionPatternExtensionResource(id, attributeName, attributeIcon, propositionValue,
					valueFormatter().format(this.valueType, score), score, 100);
		}

		public PropositionPatternExtensionResource build(PropositionPatternExtension propositionPatternExtension,
				double minValue, double diffValue) {
			Proposition p = propositionPatternExtension.proposition();

			int id = EmptyProposition.class.isInstance(p) ? -1
					: this.activeWorksheet.propositionalContext().index(p).get();
			String attributeName = extractAttributeNameFromProposition(p);
			String attributeIcon = extractAttributeIconFromActiveWorksheet(attributeName, this.activeWorksheet);
			String propositionValue = extractPropositionValueFromProposition(p);
			double score = propositionPatternExtension.values().get(0);

			return new PropositionPatternExtensionResource(id, attributeName, attributeIcon, propositionValue,
					valueFormatter().format(this.valueType, score), score, 100. * (score - minValue) / diffValue);
		}

	}

	private static class PropositionsPatternExtensionResourceBuilderMultiMeasures
			implements ResourceBuilder<PropositionPatternExtension, PropositionPatternExtensionResourceMultiMeasures> {

		private PropositionalContext context;
		private List<ValueType> valueTypes;

		private PropositionsPatternExtensionResourceBuilderMultiMeasures(PropositionalContext context,
				List<ValueType> valueTypes) {
			this.context = context;
			this.valueTypes = valueTypes;
		}

		@Override
		public PropositionPatternExtensionResourceMultiMeasures build(
				PropositionPatternExtension propositionPatternExtension) {
			Proposition p = propositionPatternExtension.proposition();

			int id = EmptyProposition.class.isInstance(p) ? -1 : this.context.index(p).get();
			String attributeName = extractAttributeNameFromProposition(p);
			String propositionValue = extractPropositionValueFromProposition(p);
			List<Double> values = propositionPatternExtension.values();

			List<String> formattedValues = newArrayList();
			for (int i = 0; i < values.size(); i++) {
				formattedValues.add(valueFormatter().format(this.valueTypes.get(i), values.get(i)));
			}

			return new PropositionPatternExtensionResourceMultiMeasures(id, attributeName, propositionValue, values,
					formattedValues);
		}

	}

}
