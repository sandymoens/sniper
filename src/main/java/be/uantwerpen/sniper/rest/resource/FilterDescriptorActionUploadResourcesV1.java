/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class FilterDescriptorActionUploadResourcesV1 {

	public static class AddToDescriptorUploadActionResource implements ActionUploadResourceV1 {

		public String descriptorId;
		public String elementType;
		public List<String> ids;

		public AddToDescriptorUploadActionResource(@JsonProperty("type") String type,
				@JsonProperty("descriptorId") String descriptorId, @JsonProperty("elementType") String elementType,
				@JsonProperty("ids") List<String> ids) {
			this.descriptorId = descriptorId;
			this.elementType = elementType;
			this.ids = ids;
		}

	}

	public static class RemoveFromDescriptorUploadActionResource implements ActionUploadResourceV1 {

		public String descriptorId;
		public String elementType;
		public String id;

		public RemoveFromDescriptorUploadActionResource(@JsonProperty("type") String type,
				@JsonProperty("descriptorId") String descriptorId, @JsonProperty("elementType") String elementType,
				@JsonProperty("id") String id) {
			this.descriptorId = descriptorId;
			this.elementType = elementType;
			this.id = id;
		}

	}

}
