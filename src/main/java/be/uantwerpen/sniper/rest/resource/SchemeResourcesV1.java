/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SchemeResourcesV1 {

	public static class SchemeResource extends ResourceSupport {

		public final String schemeId;

		public final String caption;

		public final String description;

		public SchemeResource(String id, String caption, String description) {
			this.schemeId = id;
			this.caption = caption;
			this.description = description;
		}

	}

	public static class ExpandedSchemeResource extends ResourceSupport {

		public final String schemeId;

		public final String caption;

		public final String description;

		public final DataTableResourcesV1.DataTableResource dataTable;

		public final SchemeMetaInfoResourcesV1.SchemeMetaInfoResource schemeMetaInfo;

		public final List<List<String>> rows;

		public ExpandedSchemeResource(String id, String caption, String description,
				DataTableResourcesV1.DataTableResource dataTable,
				SchemeMetaInfoResourcesV1.SchemeMetaInfoResource schemeMetaInfo, List<List<String>> rows) {
			this.schemeId = id;
			this.caption = caption;
			this.description = description;
			this.dataTable = dataTable;
			this.schemeMetaInfo = schemeMetaInfo;
			this.rows = rows;
		}

	}

	// Suppress default constructor for non-instantiability
	private SchemeResourcesV1() {
		throw new AssertionError();
	}

}
