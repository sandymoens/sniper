/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.common.Utils.method;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import be.uantwerpen.sniper.action.Action;
import be.uantwerpen.sniper.action.ParameterizedAction;
import be.uantwerpen.sniper.controller.rest.ActionController;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.rest.resource.ActionResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class ActionResourceBuildersV1 {

	public static ResourceBuilder<Action, ResourcesV1.SimpleIdentifiableResource> actionResourceBuilder(
			Worksheet worksheet) {
		return new ActionResourceBuilder(worksheet);
	}

	public static ResourceBuilder<ParameterizedAction, ActionResourcesV1.ParameterizedActionResource> parameterizedActionResourceBuilder(
			Worksheet worksheet) {
		return new ParameterizedActionResourceBuilder(worksheet);
	}

	private static class ActionResourceBuilder
			implements ResourceBuilder<Action, ResourcesV1.SimpleIdentifiableResource> {

		private Worksheet worksheet;

		private ActionResourceBuilder(Worksheet worksheet) {
			this.worksheet = worksheet;

		}

		@Override
		public ResourcesV1.SimpleIdentifiableResource build(Action action) {
			ResourcesV1.SimpleIdentifiableResource resource = new ResourcesV1.SimpleIdentifiableResource(
					action.identifier().toString());

			resource.add(linkTo(
					method(ActionController.class, "getAction", String.class, String.class, Principal.class,
							HttpServletRequest.class).get(),
					this.worksheet.getId(), action.identifier().toString(), null, null).withSelfRel());

			if (ParameterizedAction.class.isAssignableFrom(action.getClass())) {
				resource.add(linkTo(
						method(ActionController.class, "getActionParameters", String.class, String.class,
								Principal.class, HttpServletRequest.class).get(),
						this.worksheet.getId(), action.identifier().toString(), null, null).withRel("parameters"));
			}

			return resource;
		}

	}

	private static class ParameterizedActionResourceBuilder
			implements ResourceBuilder<ParameterizedAction, ActionResourcesV1.ParameterizedActionResource> {

		private Worksheet worksheet;

		private ParameterizedActionResourceBuilder(Worksheet worksheet) {
			this.worksheet = worksheet;

		}

		@Override
		public ActionResourcesV1.ParameterizedActionResource build(ParameterizedAction action) {
			ActionResourcesV1.ParameterizedActionResource resource = new ActionResourcesV1.ParameterizedActionResource(
					action.identifier().toString(), action.parameters());

			resource.add(linkTo(
					method(ActionController.class, "getActionParameters", String.class, String.class, Principal.class,
							HttpServletRequest.class).get(),
					this.worksheet.getId(), action.identifier().toString(), null, null).withSelfRel());
			resource.add(linkTo(
					method(ActionController.class, "getAction", String.class, String.class, Principal.class,
							HttpServletRequest.class).get(),
					this.worksheet.getId(), action.identifier().toString(), null, null).withRel("action"));

			return resource;
		}

	}

	// Suppress default constructor for non-instantiability
	private ActionResourceBuildersV1() {
		throw new AssertionError();
	}

}
