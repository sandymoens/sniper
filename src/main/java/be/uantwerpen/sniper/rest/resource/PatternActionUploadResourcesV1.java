/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import javax.swing.SortOrder;

import com.fasterxml.jackson.annotation.JsonProperty;

import be.uantwerpen.sniper.pattern.PatternSerialForm;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternActionUploadResourcesV1 {

	public static class CreateNewPatternUploadActionResource implements ActionUploadResourceV1 {

		public String elementType;
		public List<String> ids;
		public boolean individual;
		public int ix;

		public CreateNewPatternUploadActionResource(@JsonProperty("type") String type,
				@JsonProperty("elementType") String elementType, @JsonProperty("ids") List<String> ids,
				@JsonProperty("individual") boolean individual, @JsonProperty("ix") int ix) {
			this.elementType = elementType;
			this.ids = ids;
			this.individual = individual;
			this.ix = ix;
		}

	}

	public static class AddPatternUploadActionResource implements ActionUploadResourceV1 {

		public List<PatternSerialForm<Pattern<?>>> patternSerialForms;

		public AddPatternUploadActionResource(
				@JsonProperty("patterns") List<PatternSerialForm<Pattern<?>>> patternSerialForms) {
			this.patternSerialForms = patternSerialForms;
		}

	}

	public static class DeletePatternsUploadActionResource implements ActionUploadResourceV1 {

		public List<Integer> ixs;

		public DeletePatternsUploadActionResource(@JsonProperty("type") String type,
				@JsonProperty("ixs") List<Integer> ixs) {
			this.ixs = ixs;
		}

	}

	public static class ClearPatternsUploadActionResource implements ActionUploadResourceV1 {

		public ClearPatternsUploadActionResource(@JsonProperty("type") String type) {
		}

	}

	public static class SortPatternsUploadActionResource implements ActionUploadResourceV1 {

		public String measure;

		public SortOrder sortOrder;

		public SortPatternsUploadActionResource(@JsonProperty("measure") String measure,
				@JsonProperty("order") SortOrder sortOrder) {
			this.measure = measure;
			this.sortOrder = sortOrder;
		}

	}

	public static class MovePatternUploadActionResource implements ActionUploadResourceV1 {

		public int fromIx;

		public int toIx;

		public MovePatternUploadActionResource(@JsonProperty("fromIx") int fromIx, @JsonProperty("toIx") int toIx) {
			this.fromIx = fromIx;
			this.toIx = toIx;
		}

	}

}
