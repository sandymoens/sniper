/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.rest.resource.common.Utils.extractAttributeIconFromActiveWorksheet;
import static be.uantwerpen.sniper.rest.resource.common.Utils.extractAttributeNameFromProposition;
import static be.uantwerpen.sniper.rest.resource.common.Utils.extractPropositionValueFromProposition;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.rest.resource.PropositionResourcesV1.SimplePropositionResource;
import de.unibonn.realkd.data.propositions.Proposition;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PropositionResourceBuildersV1 {

	public static ResourceBuilder<Proposition, SimplePropositionResource> propositionResourceBuilder(
			ActiveWorksheet activeWorksheet) {
		return new SimplePropositionResourceBuilder(activeWorksheet);
	}

	private static class SimplePropositionResourceBuilder
			implements ResourceBuilder<Proposition, SimplePropositionResource> {

		private ActiveWorksheet activeWorksheet;

		public SimplePropositionResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public SimplePropositionResource build(Proposition proposition) {
			Integer index = this.activeWorksheet.propositionalContext().index(proposition).get();
			String caption = extractAttributeNameFromProposition(proposition);
			String icon = extractAttributeIconFromActiveWorksheet(caption, this.activeWorksheet);
			String value = extractPropositionValueFromProposition(proposition);
			return new SimplePropositionResource(index, caption, icon, value);
		}

	}

	// Suppress default constructor for non-instantiability
	private PropositionResourceBuildersV1() {
		throw new AssertionError();
	}

}
