/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;
import java.util.Map;

import be.uantwerpen.sniper.rest.resource.AttributeResourcesV1.SimpleAttributeResource;
import be.uantwerpen.sniper.rest.resource.DescriptorResourcesV1.IdentifiableListResource;
import be.uantwerpen.sniper.rest.resource.DescriptorResourcesV1.OptionDescriptorResource;
import be.uantwerpen.sniper.rest.resource.PatternDescriptorResourcesV1.GraphDescriptorResource;
import be.uantwerpen.sniper.rest.resource.PropositionResourcesV1.SimplePropositionResource;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternResourcesV1 {

	public static class IndexedPatternWithMeasurementsResource {

		public int ix;

		public final PatternResourceV1 patternDescriptor;

		public final Map<String, String> measurementsMap;

		public IndexedPatternWithMeasurementsResource(PatternResourceV1 patternDescriptor,
				Map<String, String> measurements) {
			this(-1, patternDescriptor, measurements);
		}

		public IndexedPatternWithMeasurementsResource(int ix, PatternResourceV1 patternDescriptor,
				Map<String, String> measurements) {
			this.ix = ix;
			this.patternDescriptor = patternDescriptor;
			this.measurementsMap = measurements;
		}

	}

	public static class AssociationResource implements PatternResourceV1 {

		public final String patternType = "Association";

		public final DescriptorResourcesV1.IdentifiableListResource<SimplePropositionResource> elements;

		public final DescriptorResourcesV1.ExtensionDescriptorResource leftExtension;

		public final DescriptorResourcesV1.ExtensionDescriptorResource rightExtension;

		public AssociationResource(DescriptorResourcesV1.IdentifiableListResource<SimplePropositionResource> elements,
				DescriptorResourcesV1.ExtensionDescriptorResource leftExtension,
				DescriptorResourcesV1.ExtensionDescriptorResource rightExtension) {
			this.elements = elements;
			this.leftExtension = leftExtension;
			this.rightExtension = rightExtension;
		}

	}

	public static class AssociationRuleResource implements PatternResourceV1 {

		public final String patternType = "AssociationRule";

		public final DescriptorResourcesV1.IdentifiableListResource<SimplePropositionResource> antecedent;

		public final DescriptorResourcesV1.IdentifiableListResource<SimplePropositionResource> consequent;

		public AssociationRuleResource(
				DescriptorResourcesV1.IdentifiableListResource<SimplePropositionResource> antecedent,
				DescriptorResourcesV1.IdentifiableListResource<SimplePropositionResource> consequent) {
			this.antecedent = antecedent;
			this.consequent = consequent;
		}

	}

	public static class AttributeListResource implements PatternResourceV1 {

		public final String patternType = "AttributeList";

		public final DescriptorResourcesV1.IdentifiableListResource<SimpleAttributeResource> elements;

		public final DescriptorResourcesV1.ExtensionDescriptorResource leftExtension;

		public final DescriptorResourcesV1.ExtensionDescriptorResource rightExtension;

		public AttributeListResource(DescriptorResourcesV1.IdentifiableListResource<SimpleAttributeResource> elements,
				DescriptorResourcesV1.ExtensionDescriptorResource leftExtension,
				DescriptorResourcesV1.ExtensionDescriptorResource rightExtension) {
			this.elements = elements;
			this.leftExtension = leftExtension;
			this.rightExtension = rightExtension;
		}

	}

	public static class ExceptionalModelPatternResource implements PatternResourceV1 {

		public String patternType = "ExceptionalModelPattern";

		public final IdentifiableListResource<SimpleAttributeResource> targetAttributes;

		public final IdentifiableListResource<SimplePropositionResource> extension;

		public final OptionDescriptorResource<String> modelType;

		public ExceptionalModelPatternResource(IdentifiableListResource<SimpleAttributeResource> targetAttributes,
				IdentifiableListResource<SimplePropositionResource> extension,
				OptionDescriptorResource<String> modelType) {
			this.targetAttributes = targetAttributes;
			this.extension = extension;
			this.modelType = modelType;
		}

	}

	public static class SequenceResource implements PatternResourceV1 {

		public String patternType = "Sequence";

		public final List<IdentifiableListResource<SimplePropositionResource>> timeSets;

		public final List<IdentifiableListResource<SimplePropositionResource>> insertionLists;

		public SequenceResource(List<IdentifiableListResource<SimplePropositionResource>> timeSets,
				List<IdentifiableListResource<SimplePropositionResource>> insertionLists) {
			this.timeSets = timeSets;
			this.insertionLists = insertionLists;
		}

	}

	public static class FunctionalPatternResource implements PatternResourceV1 {

		public String patternType = "FunctionalPattern";

		public final IdentifiableListResource<SimpleAttributeResource> domain;

		public final IdentifiableListResource<SimpleAttributeResource> coDomain;

		public FunctionalPatternResource(IdentifiableListResource<SimpleAttributeResource> domain,
				IdentifiableListResource<SimpleAttributeResource> coDomain) {
			this.domain = domain;
			this.coDomain = coDomain;
		}

	}

	public static class EpisodeResource implements PatternResourceV1 {

		public String patternType = "Episode";

		public final GraphDescriptorResource graph;

		public final DescriptorResourcesV1.ExtensionDescriptorResource leftExtension;

		public final DescriptorResourcesV1.ExtensionDescriptorResource rightExtension;

		public EpisodeResource(GraphDescriptorResource graph,
				DescriptorResourcesV1.ExtensionDescriptorResource leftExtension,
				DescriptorResourcesV1.ExtensionDescriptorResource rightExtension) {
			this.graph = graph;
			this.leftExtension = leftExtension;
			this.rightExtension = rightExtension;
		}

	}

	public static class EpisodeRuleResource implements PatternResourceV1 {

		public String patternType = "EpisodeRule";

		public GraphDescriptorResource antecedent;

		public GraphDescriptorResource consequent;

		public EpisodeRuleResource(GraphDescriptorResource antecedent, GraphDescriptorResource consequent) {
			this.antecedent = antecedent;
			this.consequent = consequent;
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternResourcesV1() {
		throw new AssertionError();
	}

}
