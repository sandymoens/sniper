/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import java.util.List;

import be.uantwerpen.sniper.rest.resource.CondensedVisualizationResourcesV1.BoxPlotVisualizationResource;
import be.uantwerpen.sniper.rest.resource.CondensedVisualizationResourcesV1.MetricHistogramVisualizationResource;
import be.uantwerpen.sniper.rest.resource.CondensedVisualizationResourcesV1.NormalDistributionVisualizationResource;
import be.uantwerpen.sniper.statistics.HistogramComputer;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class CondensedVisualizationResourceBuildersV1 {

	public static ResourceBuilder<List<Double>, NormalDistributionVisualizationResource> newNormalDistributionVisualizationResourceBuilder() {
		return new NormalDistributionVisualizationResourceBuilder();
	}

	public static ResourceBuilder<List<Double>, BoxPlotVisualizationResource> newBoxPlotVisualizationResourceBuilder() {
		return new BoxPlotVisualizationResourceBuilder();
	}

	public static ResourceBuilder<List<Double>, MetricHistogramVisualizationResource> newMetricHistogramVisualizationResourceBuilder() {
		return new MetricHistogramVisualizationResourceBuilder();
	}

	private static class NormalDistributionVisualizationResourceBuilder
			implements ResourceBuilder<List<Double>, NormalDistributionVisualizationResource> {

		@Override
		public NormalDistributionVisualizationResource build(List<Double> values) {
			double mean = 0;

			for (double value : values) {
				mean += value;
			}

			mean /= values.size();

			double stdev = 0;

			for (double value : values) {
				double diff = value - mean;
				stdev += diff * diff;
			}

			stdev = Math.sqrt(stdev / values.size());

			return new NormalDistributionVisualizationResource(mean, stdev);
		}

	}

	private static class BoxPlotVisualizationResourceBuilder
			implements ResourceBuilder<List<Double>, BoxPlotVisualizationResource> {

		@Override
		public BoxPlotVisualizationResource build(List<Double> values) {
			double mean = 0;

			for (double value : values) {
				mean += value;
			}

			mean /= values.size();

			return new BoxPlotVisualizationResource(values.get(0), values.get(values.size() - 1), mean,
					values.get((int) (values.size() * 0.5)), values.get((int) (values.size() * 0.25)),
					values.get((int) (values.size() * 0.75)));
		}

	}

	private static class MetricHistogramVisualizationResourceBuilder
			implements ResourceBuilder<List<Double>, MetricHistogramVisualizationResource> {

		@Override
		public MetricHistogramVisualizationResource build(List<Double> values) {
			return new MetricHistogramVisualizationResource(HistogramComputer.computeHistogram(10, values));
		}

	}

}
