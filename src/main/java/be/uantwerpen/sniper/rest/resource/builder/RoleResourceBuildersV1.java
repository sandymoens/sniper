/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import be.uantwerpen.sniper.dao.entity.Role;
import be.uantwerpen.sniper.rest.resource.RoleResourcesV1;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class RoleResourceBuildersV1 {

	private static ResourceBuilder<Role, RoleResourcesV1.SimpleRoleResource> simpleRoleResourceBuilder;

	public static ResourceBuilder<Role, RoleResourcesV1.SimpleRoleResource> simpleRoleResourceBuilder() {
		return simpleRoleResourceBuilder != null ? simpleRoleResourceBuilder
				: (simpleRoleResourceBuilder = new SimpleRoleResourceBuilder());
	}

	private static class SimpleRoleResourceBuilder
			implements ResourceBuilder<Role, RoleResourcesV1.SimpleRoleResource> {

		@Override
		public RoleResourcesV1.SimpleRoleResource build(Role role) {
			return new RoleResourcesV1.SimpleRoleResource(role.getName());
		}

	}

	// Suppress default constructor for non-instantiability
	private RoleResourceBuildersV1() {
		throw new AssertionError();
	}

}
