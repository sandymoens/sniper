/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
		@JsonSubTypes.Type(value = WorksheetMetaInfoUploadResourcesV1.DataTableWorksheetMetaInfoUploadResource.class, name = "dataTable"),
		@JsonSubTypes.Type(value = WorksheetMetaInfoUploadResourcesV1.SequentialDataTableWorksheetMetaInfoUploadResource.class, name = "sequentialDataTable"),
		@JsonSubTypes.Type(value = WorksheetMetaInfoUploadResourcesV1.SequenceWorksheetMetaInfoUploadResource.class, name = "sequence"),
		@JsonSubTypes.Type(value = WorksheetMetaInfoUploadResourcesV1.TransactionsWorksheetMetaInfoUploadResource.class, name = "transactions") })
public interface WorksheetMetaInfoUploadResourceV1 {

	public String type();

}
