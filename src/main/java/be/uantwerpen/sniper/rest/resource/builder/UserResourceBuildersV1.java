/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.common.Utils.method;
import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import be.uantwerpen.sniper.controller.rest.UserController;
import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.rest.resource.UserResourcesV1;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class UserResourceBuildersV1 {

	private static ResourceBuilder<User, UserResourcesV1.SimpleUserResource> simpleUserResourceBuilder;

	private static ResourceBuilder<User, UserResourcesV1.FullUserResource> fullUserResourceBuilder;

	public static ResourceBuilder<User, UserResourcesV1.SimpleUserResource> simpleUserResourceBuilder() {
		return simpleUserResourceBuilder != null ? simpleUserResourceBuilder
				: (simpleUserResourceBuilder = new SimpleUserResourceBuilder());
	}

	public static ResourceBuilder<User, UserResourcesV1.FullUserResource> fullUserResourceBuilder() {
		return fullUserResourceBuilder != null ? fullUserResourceBuilder
				: (fullUserResourceBuilder = new FullUserResourceBuilder());
	}

	private static class SimpleUserResourceBuilder
			implements ResourceBuilder<User, UserResourcesV1.SimpleUserResource> {

		@Override
		public UserResourcesV1.SimpleUserResource build(User user) {
			return new UserResourcesV1.SimpleUserResource(user.getUsername());
		}

	}

	private static class FullUserResourceBuilder implements ResourceBuilder<User, UserResourcesV1.FullUserResource> {

		@Override
		public UserResourcesV1.FullUserResource build(User user) {
			List<String> roles = user.getRoles().stream().map(r -> r.getName()).collect(toList());

			UserResourcesV1.FullUserResource resource = new UserResourcesV1.FullUserResource(user.getUsername(), roles,
					user.getEnabled());

			resource.add(linkTo(method(UserController.class, "user", Integer.class, HttpServletRequest.class).get(),
					user.getId(), null).withSelfRel());

			return resource;
		}

	}

	// Suppress default constructor for non-instantiability
	private UserResourceBuildersV1() {
		throw new AssertionError();
	}

}
