/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternDescriptorResourcesV1 {

	public static class GraphDescriptorResource implements PatternDescriptorResourceV1 {

		public final DescriptorResourcesV1.IdentifiableListResource<PropositionResourcesV1.SimplePropositionResource> nodes;

		public final DescriptorResourcesV1.ListResource<Integer> localIds;

		public final DescriptorResourcesV1.ListResource<EdgeResourcesV1.SimpleEdgeResource> edges;

		public GraphDescriptorResource(
				DescriptorResourcesV1.IdentifiableListResource<PropositionResourcesV1.SimplePropositionResource> nodes,
				DescriptorResourcesV1.ListResource<Integer> localIds,
				DescriptorResourcesV1.ListResource<EdgeResourcesV1.SimpleEdgeResource> edges) {
			this.nodes = nodes;
			this.localIds = localIds;
			this.edges = edges;
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternDescriptorResourcesV1() {
		throw new AssertionError();
	}

}
