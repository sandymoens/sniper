/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PropositionResourcesV1 {

	public static class SimplePropositionResource {

		public final int id;

		public final String attributeName;

		public final String attributeIcon;

		public final String propositionValue;

		public SimplePropositionResource(int id, String attributeName, String attributeIcon, String propositionValue) {
			this.id = id;
			this.attributeName = attributeName;
			this.attributeIcon = attributeIcon;
			this.propositionValue = propositionValue;
		}

	}

	public static class PropositionWithScoreResource {

		public final int id;

		public final String attributeName;

		public final String attributeIcon;

		public final String propositionName;

		public final String formattedScore;

		public final double score;

		public final double pct;

		public PropositionWithScoreResource(int id, String attributeName, String attributeIcon, String propositionName,
				String formattedScore, double score, double pct) {
			this.id = id;
			this.attributeName = attributeName;
			this.attributeIcon = attributeIcon;
			this.propositionName = propositionName;
			this.formattedScore = formattedScore;
			this.score = score;
			this.pct = pct;
		}

	}

	// Suppress default constructor for non-instantiability
	private PropositionResourcesV1() {
		throw new AssertionError();
	}

}
