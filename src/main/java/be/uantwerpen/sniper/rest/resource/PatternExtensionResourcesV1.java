/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternExtensionResourcesV1 {

	public static abstract class PatternExtensionResource {

		public final double score;

		public PatternExtensionResource(double score) {
			this.score = score;
		}

	}

	public static class PropositionPatternExtensionResource extends PatternExtensionResource {

		public final int id;

		public final String attributeName;

		public final String attributeIcon;

		public final String propositionValue;

		public final String formattedScore;

		public final double pct;

		public PropositionPatternExtensionResource(int id, String attributeName, String attributeIcon,
				String propositionValue, String formattedScore, double score, double pct) {
			super(score);

			this.id = id;
			this.attributeName = attributeName;
			this.attributeIcon = attributeIcon;
			this.propositionValue = propositionValue;
			this.formattedScore = formattedScore;
			this.pct = pct;
		}

	}

	public static class PropositionPatternExtensionResourceMultiMeasures extends PatternExtensionResource {

		public final int id;

		public final String attributeName;

		public final String propositionValue;

		public final List<Double> values;

		public final List<String> formattedValues;

		public PropositionPatternExtensionResourceMultiMeasures(int id, String attributeName, String propositionValue,
				List<Double> values, List<String> formattedValues) {
			super(0);

			this.id = id;
			this.attributeName = attributeName;
			this.propositionValue = propositionValue;
			this.values = values;
			this.formattedValues = formattedValues;
		}

	}

}
