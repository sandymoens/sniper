/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class UserResourcesV1 {

	public static class SimpleUserResource extends ResourceSupport {

		public final String userName;

		public SimpleUserResource(String userName) {
			this.userName = userName;
		}

	}

	public static class FullUserResource extends ResourceSupport {

		public final String userName;

		public final List<String> roles;

		public final boolean enabled;

		public FullUserResource(String userName, List<String> roles, boolean enabled) {
			this.userName = userName;
			this.roles = roles;
			this.enabled = enabled;
		}

	}

	// Suppress default constructor for non-instantiability
	private UserResourcesV1() {
		throw new AssertionError();
	}

}
