/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import com.fasterxml.jackson.annotation.JsonProperty;

import be.uantwerpen.sniper.pattern.PatternSerialForm;
import de.unibonn.realkd.patterns.Pattern;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class QueryUploadResourcesV1 {

	public static class PropositionsQuery {

		public String measure1;

		public String measure2;

		public PatternSerialForm<Pattern<?>> patternSerialForm;

		public String part;

		public PropositionsQuery(@JsonProperty("measure1") String measure1, @JsonProperty("measure2") String measure2,
				@JsonProperty("pattern") PatternSerialForm<Pattern<?>> patternSerialForm,
				@JsonProperty("part") String part) {
			this.measure1 = measure1;
			this.measure2 = measure2;
			this.patternSerialForm = patternSerialForm;
			this.part = part;
		}

	}

	public static class SortedPropositionsQuery {

		public String measure;

		public PatternSerialForm<Pattern<?>> patternSerialForm;

		public String part;

		public SortedPropositionsQuery(@JsonProperty("measure") String measure,
				@JsonProperty("pattern") PatternSerialForm<Pattern<?>> patternSerialForm,
				@JsonProperty("part") String part) {
			this.measure = measure;
			this.patternSerialForm = patternSerialForm;
			this.part = part;
		}

	}

}
