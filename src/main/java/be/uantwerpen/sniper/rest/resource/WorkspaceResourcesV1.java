/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class WorkspaceResourcesV1 {

	public static class WorkspaceResource extends ResourceSupport {

		public final String workspaceId;

		public final String caption;

		public final String description;

		public WorkspaceResource(String id, String caption, String description) {
			this.workspaceId = id;
			this.caption = caption;
			this.description = description;
		}

	}

	public static class ExpandedWorkspaceResource extends ResourceSupport {

		public final String workspaceId;

		public final String caption;

		public final String description;

		public final List<WorksheetResourcesV1.WorksheetResource> worksheets;

		public ExpandedWorkspaceResource(String id, String caption, String description,
				List<WorksheetResourcesV1.WorksheetResource> worksheetResources) {
			this.workspaceId = id;
			this.caption = caption;
			this.description = description;
			this.worksheets = worksheetResources;
		}

	}

	// Suppress default constructor for non-instantiability
	private WorkspaceResourcesV1() {
		throw new AssertionError();
	}

}
