/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static java.util.stream.Collectors.toList;

import java.util.List;

import be.uantwerpen.sniper.model.StatusEnabledModel;
import be.uantwerpen.sniper.rest.resource.VisualizationResourcesV1;
import be.uantwerpen.sniper.visualization.Visualization;
import be.uantwerpen.sniper.visualization.pattern.PatternVisualizations;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class VisualizationResourceBuildersV1 {

	private static ResourceBuilder<StatusEnabledModel<Visualization<Pattern<?>, ?>>, List<VisualizationResourcesV1.VisualizationWithStateResource>> visualizationsWithStateResourceBuilder;

	public static ResourceBuilder<StatusEnabledModel<Visualization<Pattern<?>, ?>>, List<VisualizationResourcesV1.VisualizationWithStateResource>> visualizationsWithStateResourceBuilder() {
		return visualizationsWithStateResourceBuilder != null ? visualizationsWithStateResourceBuilder
				: (visualizationsWithStateResourceBuilder = new VisualizationsWithStateResourceBuilder());
	}

	private static class VisualizationsWithStateResourceBuilder implements
			ResourceBuilder<StatusEnabledModel<Visualization<Pattern<?>, ?>>, List<VisualizationResourcesV1.VisualizationWithStateResource>> {

		@Override
		public List<VisualizationResourcesV1.VisualizationWithStateResource> build(
				StatusEnabledModel<Visualization<Pattern<?>, ?>> visualizationModel) {
			return PatternVisualizations.PATTERN_VISUALIZATIONS.stream()
					.map(v -> new VisualizationResourcesV1.VisualizationWithStateResource(v.caption(), v.caption(),
							visualizationModel.status(v)))
					.collect(toList());
		}

	}

	// Suppress default constructor for non-instantiability
	private VisualizationResourceBuildersV1() {
		throw new AssertionError();
	}

}
