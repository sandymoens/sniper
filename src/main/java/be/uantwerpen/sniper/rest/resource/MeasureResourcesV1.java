/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class MeasureResourcesV1 {

	public static class MeasureResource {

		public final String id;

		public final String caption;

		public final String description;

		public MeasureResource(String id, String caption, String description) {
			this.id = id;
			this.caption = caption;
			this.description = description;
		}

	}

	public static class MeasureWithStateResource {

		public final String id;

		public final String caption;

		public final String description;

		public final boolean isActive;

		public MeasureWithStateResource(String id, String caption, String description, boolean isActive) {
			this.id = id;
			this.caption = caption;
			this.description = description;
			this.isActive = isActive;
		}

	}

	public static class MeasureWithStateUploadResource {

		public final String caption;

		public final boolean isActive;

		public MeasureWithStateUploadResource(@JsonProperty("name") String caption,
				@JsonProperty("isActive") boolean isActive) {
			this.caption = caption;
			this.isActive = isActive;
		}

	}

	public static class MeasureVisualizationResource {

		public final String id;

		public final List<CondensedVisualizationResourceV1> visualizations;

		public MeasureVisualizationResource(String id, List<CondensedVisualizationResourceV1> visualizations) {
			this.id = id;
			this.visualizations = visualizations;
		}

	}

	// Suppress default constructor for non-instantiability
	private MeasureResourcesV1() {
		throw new AssertionError();
	}

}
