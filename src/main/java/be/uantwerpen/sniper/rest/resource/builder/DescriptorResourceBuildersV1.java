/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.utils.ModelFactoryRegister.MODEL_FACTORIES;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.model.bricks.Brick;
import be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptor;
import be.uantwerpen.sniper.rest.resource.AttributeResourcesV1;
import be.uantwerpen.sniper.rest.resource.DescriptorResourcesV1;
import be.uantwerpen.sniper.rest.resource.DescriptorResourcesV1.IdentifiableListResource;
import be.uantwerpen.sniper.rest.resource.EdgeResourcesV1;
import be.uantwerpen.sniper.rest.resource.EdgeResourcesV1.SimpleEdgeResource;
import be.uantwerpen.sniper.rest.resource.PatternDescriptorResourcesV1.GraphDescriptorResource;
import be.uantwerpen.sniper.rest.resource.PropositionResourcesV1;
import be.uantwerpen.sniper.rest.resource.PropositionResourcesV1.SimplePropositionResource;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.subgroups.Subgroup;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class DescriptorResourceBuildersV1 {

	private static ResourceBuilder<Brick<Collection<Attribute<?>>, ?>, IdentifiableListResource<AttributeResourcesV1.SimpleAttributeResource>> attributeListResourceBuilder;

	private static ResourceBuilder<Brick<AttributeListDescriptor, ?>, IdentifiableListResource<AttributeResourcesV1.SimpleAttributeResource>> attributeListDescriptorResourceBuilder;

	private static ResourceBuilder<Brick<?, ?>, DescriptorResourcesV1.ExtensionDescriptorResource> extensionDescriptorResourceBuilder;

	public static ResourceBuilder<Brick<LogicalDescriptor, ?>, IdentifiableListResource<PropositionResourcesV1.SimplePropositionResource>> logicalDescriptorResourceBuilder(
			ActiveWorksheet activeWorksheet) {
		return new LogicalDescriptorResourceBuilder(activeWorksheet);
	}

	public static ResourceBuilder<Brick<List<Proposition>, ?>, IdentifiableListResource<PropositionResourcesV1.SimplePropositionResource>> orderedListResourceBuilder(
			ActiveWorksheet activeWorksheet) {
		return new OrderedListResourceBuilder(activeWorksheet);
	}

	public static ResourceBuilder<Brick<Collection<Attribute<?>>, ?>, IdentifiableListResource<AttributeResourcesV1.SimpleAttributeResource>> attributeCollectionResourceBuilder() {
		return attributeListResourceBuilder != null ? attributeListResourceBuilder
				: (attributeListResourceBuilder = new AttributeListResourceBuilder());
	}

	public static ResourceBuilder<Brick<AttributeListDescriptor, ?>, IdentifiableListResource<AttributeResourcesV1.SimpleAttributeResource>> attributeListDescriptorResourceBuilder() {
		return attributeListDescriptorResourceBuilder != null ? attributeListDescriptorResourceBuilder
				: (attributeListDescriptorResourceBuilder = new AttributeListDescriptorResourceBuilder());
	}

	public static ResourceBuilder<Brick<?, ?>, DescriptorResourcesV1.ExtensionDescriptorResource> extensionDescriptorResourceBuilder(
			ActiveWorksheet activeWorksheet) {
		return extensionDescriptorResourceBuilder != null ? extensionDescriptorResourceBuilder
				: (extensionDescriptorResourceBuilder = new ExtensionDescriptorResourceBuilder());
	}

	public static ResourceBuilder<Brick<ModelFactory<?>, ?>, DescriptorResourcesV1.OptionDescriptorResource<String>> modelFactoryDescriptorResourceBuilder(
			Subgroup<?> subgroup) {
		return new ModelFactoryDescriptorResourceBuilder(subgroup);
	}

	public static ResourceBuilder<Brick<GraphDescriptor, ?>, GraphDescriptorResource> graphDescriptorResourceBuilder(
			ActiveWorksheet activeWorksheet) {
		return new GraphDescriptorResourceBuilder(activeWorksheet);
	}

	private static class LogicalDescriptorResourceBuilder implements
			ResourceBuilder<Brick<LogicalDescriptor, ?>, IdentifiableListResource<PropositionResourcesV1.SimplePropositionResource>> {

		private ActiveWorksheet activeWorksheet;

		public LogicalDescriptorResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public IdentifiableListResource<PropositionResourcesV1.SimplePropositionResource> build(
				Brick<LogicalDescriptor, ?> logicalDescriptorBrick) {
			LogicalDescriptor logicalDescriptor = logicalDescriptorBrick.content();

			return new IdentifiableListResource<>(logicalDescriptorBrick.id(), PropositionResourceBuildersV1
					.propositionResourceBuilder(this.activeWorksheet).build(logicalDescriptor.elements()));
		}

	}

	private static class AttributeListResourceBuilder implements
			ResourceBuilder<Brick<Collection<Attribute<?>>, ?>, IdentifiableListResource<AttributeResourcesV1.SimpleAttributeResource>> {

		@Override
		public IdentifiableListResource<AttributeResourcesV1.SimpleAttributeResource> build(
				Brick<Collection<Attribute<?>>, ?> attributeListBrick) {
			Collection<Attribute<?>> attributeList = attributeListBrick.content();

			return new IdentifiableListResource<>(attributeListBrick.id(),
					AttributeResourceBuildersV1.simpleAttributeResourceBuilder().build(attributeList));
		}

	}

	private static class AttributeListDescriptorResourceBuilder implements
			ResourceBuilder<Brick<AttributeListDescriptor, ?>, IdentifiableListResource<AttributeResourcesV1.SimpleAttributeResource>> {

		@Override
		public IdentifiableListResource<AttributeResourcesV1.SimpleAttributeResource> build(
				Brick<AttributeListDescriptor, ?> attributeListDescriptorBrick) {
			List<Attribute<?>> attributeList = attributeListDescriptorBrick.content().attributes();

			return new IdentifiableListResource<>(attributeListDescriptorBrick.id(),
					AttributeResourceBuildersV1.simpleAttributeResourceBuilder().build(attributeList));
		}

	}

	private static class ExtensionDescriptorResourceBuilder
			implements ResourceBuilder<Brick<?, ?>, DescriptorResourcesV1.ExtensionDescriptorResource> {

		@Override
		public DescriptorResourcesV1.ExtensionDescriptorResource build(Brick<?, ?> brick) {
			return new DescriptorResourcesV1.ExtensionDescriptorResource(brick.id());
		}

	}

	private static class ModelFactoryDescriptorResourceBuilder implements
			ResourceBuilder<Brick<ModelFactory<?>, ?>, DescriptorResourcesV1.OptionDescriptorResource<String>> {

		private Subgroup<?> subgroup;

		private ModelFactoryDescriptorResourceBuilder(Subgroup<?> subgroup) {
			this.subgroup = subgroup;
		}

		@Override
		public DescriptorResourcesV1.OptionDescriptorResource<String> build(Brick<ModelFactory<?>, ?> brick) {
			List<String> modelTypeNames = MODEL_FACTORIES.stream()
					.filter(m -> m.isApplicable(this.subgroup.targetAttributes()))
					.map(m -> m.getClass().getSimpleName()).collect(toList());

			return new DescriptorResourcesV1.OptionDescriptorResource<>(brick.id(),
					brick.content().getClass().getSimpleName(), modelTypeNames);
		}

	}

	private static class OrderedListResourceBuilder implements
			ResourceBuilder<Brick<List<Proposition>, ?>, IdentifiableListResource<PropositionResourcesV1.SimplePropositionResource>> {

		private ActiveWorksheet activeWorksheet;

		public OrderedListResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public IdentifiableListResource<PropositionResourcesV1.SimplePropositionResource> build(
				Brick<List<Proposition>, ?> orderedListsBrick) {
			return new IdentifiableListResource<>(orderedListsBrick.id(), PropositionResourceBuildersV1
					.propositionResourceBuilder(this.activeWorksheet).build(orderedListsBrick.content()));
		}

	}

	private static class GraphDescriptorResourceBuilder
			implements ResourceBuilder<Brick<GraphDescriptor, ?>, GraphDescriptorResource> {

		private ActiveWorksheet activeWorksheet;

		public GraphDescriptorResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public GraphDescriptorResource build(Brick<GraphDescriptor, ?> graphDescriptorBrick) {
			ResourceBuilder<Proposition, SimplePropositionResource> propositionResourceBuilder = PropositionResourceBuildersV1
					.propositionResourceBuilder(this.activeWorksheet);

			List<PropositionResourcesV1.SimplePropositionResource> propositionResources = graphDescriptorBrick.content()
					.nodes().stream().map(n -> propositionResourceBuilder.build(n.proposition()))
					.collect(Collectors.toList());

			DescriptorResourcesV1.IdentifiableListResource<PropositionResourcesV1.SimplePropositionResource> nodes = new DescriptorResourcesV1.IdentifiableListResource<>(
					graphDescriptorBrick.id(), propositionResources);

			DescriptorResourcesV1.ListResource<Integer> localIds = DescriptorResourcesV1.newListResource(
					graphDescriptorBrick.content().nodes().stream().map(n -> n.id()).collect(Collectors.toList()));

			DescriptorResourcesV1.ListResource<SimpleEdgeResource> edges = DescriptorResourcesV1
					.newListResource(graphDescriptorBrick.content().edges().stream()
							.map(e -> new EdgeResourcesV1.SimpleEdgeResource(e.start(), e.end()))
							.collect(Collectors.toList()));

			return new GraphDescriptorResource(nodes, localIds, edges);
		}

	}

	// Suppress default constructor for non-instantiability
	private DescriptorResourceBuildersV1() {
		throw new AssertionError();
	}

}
