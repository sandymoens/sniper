/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.statistics.HistogramComputer.computeDateHistogram;
import static be.uantwerpen.sniper.statistics.HistogramComputer.computeHistogramOptionals;
import static com.google.common.collect.Maps.newHashMap;
import static java.lang.Integer.parseInt;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import org.apache.commons.lang3.tuple.Pair;

import be.uantwerpen.sniper.rest.resource.CondensedVisualizationResourceV1;
import be.uantwerpen.sniper.rest.resource.CondensedVisualizationResourcesV1;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricAttribute;
import de.unibonn.realkd.data.table.attribute.DefaultDateAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class AttributeVisualizationResourceBuildersV1 {

	private static Map<String, ResourceBuilder<Attribute<?>, CondensedVisualizationResourceV1>> handlers;

	public static void register(String visualizationId,
			ResourceBuilder<Attribute<?>, CondensedVisualizationResourceV1> handler) {
		handlers.put(visualizationId, handler);
	}

	static {
		handlers = newHashMap();

		register("categories", new CategoricAttributeCategoriesVisualizationResourceBuilder());
		register("boxPlot", new MetricAttributeBoxPlotVisualizationResourceBuilder());
		register("dateHistogram", new DateAttributeHistogramVisualizationResourceBuilder());
	}

	public static Optional<ResourceBuilder<Attribute<?>, CondensedVisualizationResourceV1>> attributeVisualizationResourceBuilder(
			String visualizationId, Map<String, String[]> parameters) {
		if (visualizationId.equals("metricHistogram")) {
			int bins = parameters.containsKey("bins") ? parseInt(parameters.get("bins")[0]) : 5;

			return Optional.of(new MetricAttributeHistogramVisualizationResourceBuilder(bins));
		}

		return Optional.ofNullable(handlers.get(visualizationId));
	}

	private static class CategoricAttributeCategoriesVisualizationResourceBuilder
			implements ResourceBuilder<Attribute<?>, CondensedVisualizationResourceV1> {

		@Override
		public CondensedVisualizationResourceV1 build(Attribute<?> attribute) {
			if (attribute instanceof CategoricAttribute) {
				CategoricAttribute<?> cAttribute = (CategoricAttribute<?>) attribute;

				List<Pair<String, Double>> categories = IntStream.range(0, cAttribute.categories().size())
						.mapToObj(i -> {
							return Pair.of(cAttribute.categories().get(i).toString(),
									cAttribute.categoryFrequencies().get(i));
						}).collect(toList());

				return new CondensedVisualizationResourcesV1.CategoriesVisualizationResource(categories);
			}

			return null;
		}

	}

	private static class MetricAttributeBoxPlotVisualizationResourceBuilder
			implements ResourceBuilder<Attribute<?>, CondensedVisualizationResourceV1> {

		@Override
		public CondensedVisualizationResourceV1 build(Attribute<?> attribute) {
			if (attribute instanceof MetricAttribute) {
				MetricAttribute mAttribute = (MetricAttribute) attribute;

				return new CondensedVisualizationResourcesV1.BoxPlotVisualizationResource(mAttribute.min(),
						mAttribute.max(), mAttribute.mean(), mAttribute.median(), mAttribute.lowerQuartile(),
						mAttribute.upperQuartile());
			}

			return null;
		}

	}

	private static class MetricAttributeHistogramVisualizationResourceBuilder
			implements ResourceBuilder<Attribute<?>, CondensedVisualizationResourceV1> {

		private int bins;

		private MetricAttributeHistogramVisualizationResourceBuilder(int bins) {
			this.bins = bins;
		}

		@Override
		public CondensedVisualizationResourceV1 build(Attribute<?> attribute) {
			if (attribute instanceof MetricAttribute) {
				return new CondensedVisualizationResourcesV1.MetricHistogramVisualizationResource(
						computeHistogramOptionals(this.bins, ((MetricAttribute) attribute).getValues()));
			}

			return null;
		}

	}

	private static class DateAttributeHistogramVisualizationResourceBuilder
			implements ResourceBuilder<Attribute<?>, CondensedVisualizationResourceV1> {

		private DateAttributeHistogramVisualizationResourceBuilder() {
		}

		@Override
		public CondensedVisualizationResourceV1 build(Attribute<?> attribute) {
			if (attribute instanceof DefaultDateAttribute) {
				return new CondensedVisualizationResourcesV1.DateHistogramVisualizationResource(
						computeDateHistogram(((DefaultDateAttribute) attribute).getValues()));
			}

			return null;
		}

	}

	// Suppress default constructor for non-instantiability
	private AttributeVisualizationResourceBuildersV1() {
		throw new AssertionError();
	}

}
