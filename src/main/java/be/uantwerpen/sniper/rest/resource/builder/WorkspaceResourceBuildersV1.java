/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.common.Utils.method;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import be.uantwerpen.sniper.controller.rest.WorkspaceController;
import be.uantwerpen.sniper.dao.entity.Workspace;
import be.uantwerpen.sniper.rest.resource.ResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1.ListResource;
import be.uantwerpen.sniper.rest.resource.WorksheetResourcesV1;
import be.uantwerpen.sniper.rest.resource.WorkspaceResourcesV1;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class WorkspaceResourceBuildersV1 {

	private static ResourceBuilder<Workspace, WorkspaceResourcesV1.WorkspaceResource> workspaceResourceBuilder;

	private static ResourceBuilder<List<Workspace>, ListResource<WorkspaceResourcesV1.WorkspaceResource>> workspacesResourceBuilder;

	private static ResourceBuilder<Workspace, WorkspaceResourcesV1.ExpandedWorkspaceResource> expandedWorkspaceResourceBuilder;

	private static ResourceBuilder<List<Workspace>, ListResource<WorkspaceResourcesV1.ExpandedWorkspaceResource>> expandedWorkspacesResourceBuilder;

	public static ResourceBuilder<Workspace, WorkspaceResourcesV1.WorkspaceResource> workspaceResourceBuilder() {
		return workspaceResourceBuilder != null ? workspaceResourceBuilder
				: (workspaceResourceBuilder = new WorkspaceResourceBuilder());
	}

	public static ResourceBuilder<List<Workspace>, ListResource<WorkspaceResourcesV1.WorkspaceResource>> workspacesResourceBuilder() {
		return workspacesResourceBuilder != null ? workspacesResourceBuilder
				: (workspacesResourceBuilder = new WorkspacesResourceBuilder());
	}

	public static ResourceBuilder<Workspace, WorkspaceResourcesV1.ExpandedWorkspaceResource> expandedWorkspaceResourceBuilder() {
		return expandedWorkspaceResourceBuilder != null ? expandedWorkspaceResourceBuilder
				: (expandedWorkspaceResourceBuilder = new ExpandedWorkspaceResourceBuilder());
	}

	public static ResourceBuilder<List<Workspace>, ListResource<WorkspaceResourcesV1.ExpandedWorkspaceResource>> expandedWorkspacesResourceBuilder() {
		return expandedWorkspacesResourceBuilder != null ? expandedWorkspacesResourceBuilder
				: (expandedWorkspacesResourceBuilder = new ExpandedWorkspacesResourceBuilder());
	}

	private static class WorkspaceResourceBuilder
			implements ResourceBuilder<Workspace, WorkspaceResourcesV1.WorkspaceResource> {

		@Override
		public WorkspaceResourcesV1.WorkspaceResource build(Workspace workspace) {
			WorkspaceResourcesV1.WorkspaceResource workspaceResource = new WorkspaceResourcesV1.WorkspaceResource(
					workspace.getId(), workspace.caption(), workspace.description());

			workspaceResource.add(linkTo(WorkspaceController.class, method(WorkspaceController.class, "workspace",
					String.class, Principal.class, HttpServletRequest.class).get(), workspace.getId(), null, null)
							.withSelfRel());

			return workspaceResource;
		}

	}

	private static class WorkspacesResourceBuilder
			implements ResourceBuilder<List<Workspace>, ListResource<WorkspaceResourcesV1.WorkspaceResource>> {

		@Override
		public ListResource<WorkspaceResourcesV1.WorkspaceResource> build(List<Workspace> workspaces) {
			List<WorkspaceResourcesV1.WorkspaceResource> workspaceResources = workspaceResourceBuilder()
					.build(workspaces);

			ListResource<WorkspaceResourcesV1.WorkspaceResource> listResource = new ResourcesV1.ListResource<>(
					workspaceResources);

			listResource.add(linkTo(method(WorkspaceController.class, "workspaces", HttpServletRequest.class).get(),
					(HttpServletRequest) null).withSelfRel());

			return listResource;
		}

	}

	private static class ExpandedWorkspaceResourceBuilder
			implements ResourceBuilder<Workspace, WorkspaceResourcesV1.ExpandedWorkspaceResource> {

		@Override
		public WorkspaceResourcesV1.ExpandedWorkspaceResource build(Workspace workspace) {
			List<WorksheetResourcesV1.WorksheetResource> worksheetResources = WorksheetResourceBuildersV1
					.worksheetResourceBuilder().build(workspace.getWorksheets());

			WorkspaceResourcesV1.ExpandedWorkspaceResource workspaceResource = new WorkspaceResourcesV1.ExpandedWorkspaceResource(
					workspace.getId(), workspace.caption(), workspace.description(), worksheetResources);

			workspaceResource.add(linkTo(WorkspaceController.class, method(WorkspaceController.class, "workspace",
					String.class, Principal.class, HttpServletRequest.class).get(), workspace.getId(), null, null)
							.withSelfRel());

			return workspaceResource;
		}

	}

	private static class ExpandedWorkspacesResourceBuilder
			implements ResourceBuilder<List<Workspace>, ListResource<WorkspaceResourcesV1.ExpandedWorkspaceResource>> {

		@Override
		public ListResource<WorkspaceResourcesV1.ExpandedWorkspaceResource> build(List<Workspace> workspaces) {
			List<WorkspaceResourcesV1.ExpandedWorkspaceResource> workspaceResources = expandedWorkspaceResourceBuilder()
					.build(workspaces);

			ListResource<WorkspaceResourcesV1.ExpandedWorkspaceResource> listResource = new ResourcesV1.ListResource<>(
					workspaceResources);

			listResource.add(linkTo(
					method(WorkspaceController.class, "workspaces", Principal.class, HttpServletRequest.class).get(),
					(Principal) null, (HttpServletRequest) null).withSelfRel());

			return listResource;
		}

	}

	// Suppress default constructor for non-instantiability
	private WorkspaceResourceBuildersV1() {
		throw new AssertionError();
	}

}
