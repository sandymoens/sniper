/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

import be.uantwerpen.sniper.dao.SchemeMetaInfos.DFFilterType;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SchemeUploadResourcesV1 {

	public static class DataTableSchemeUploadResource extends ResourceSupport implements SchemeUploadResourceV1 {

		private final String dataTableId;

		private final String caption;

		private final String description;

		private final String delimiter;

		private final boolean hasHeaders;

		private final String dateTimeFormat;

		private final List<AttributeResourcesV1.SchemeAttributeResource> attributes;

		public DataTableSchemeUploadResource(@JsonProperty("schemeType") String schemeType,
				@JsonProperty("dataTableId") String dataTableId, @JsonProperty("caption") String caption,
				@JsonProperty("description") String description, @JsonProperty("delimiter") String delimiter,
				@JsonProperty("hasHeaders") boolean hasHeaders, @JsonProperty("dateTimeFormat") String dateTimeFormat,
				@JsonProperty("attributes") List<AttributeResourcesV1.SchemeAttributeResource> attributes) {
			this.dataTableId = dataTableId;
			this.caption = caption;
			this.description = description;
			this.delimiter = delimiter;
			this.hasHeaders = hasHeaders;
			this.dateTimeFormat = dateTimeFormat;
			this.attributes = attributes;
		}

		@Override
		public String dataTableId() {
			return this.dataTableId;
		}

		@Override
		public String caption() {
			return this.caption;
		}

		@Override
		public String description() {
			return this.description;
		}

		public String delimiter() {
			return this.delimiter;
		}

		public boolean hasHeaders() {
			return this.hasHeaders;
		}

		public String dateTimeFormat() {
			return this.dateTimeFormat;
		}

		public List<AttributeResourcesV1.SchemeAttributeResource> attributes() {
			return this.attributes;
		}

	}

	public static class TransactionsSchemeUploadResource extends ResourceSupport implements SchemeUploadResourceV1 {

		private final String dataTableId;

		private final String caption;

		private final String description;

		private final String delimiter;

		public TransactionsSchemeUploadResource(@JsonProperty("schemeType") String schemeType,
				@JsonProperty("dataTableId") String dataTableId, @JsonProperty("caption") String caption,
				@JsonProperty("description") String description, @JsonProperty("delimiter") String delimiter) {
			this.dataTableId = dataTableId;
			this.caption = caption;
			this.description = description;
			this.delimiter = delimiter;
		}

		@Override
		public String dataTableId() {
			return this.dataTableId;
		}

		@Override
		public String caption() {
			return this.caption;
		}

		@Override
		public String description() {
			return this.description;
		}

		public String delimiter() {
			return this.delimiter;
		}

	}

	private static abstract class TextSchemeUploadResource extends ResourceSupport implements SchemeUploadResourceV1 {

		private String dataTableId;

		private String caption;

		private String description;

		private boolean recognizeSentences;

		private boolean alphabeticalOnly;

		private boolean filterPOS;

		private boolean stemTokens;

		public TextSchemeUploadResource(@JsonProperty("schemeType") String schemeType,
				@JsonProperty("dataTableId") String dataTableId, @JsonProperty("caption") String caption,
				@JsonProperty("description") String description,
				@JsonProperty("recognizeSentences") boolean recognizeSentences,
				@JsonProperty("alphabeticalOnly") boolean alphabeticalOnly,
				@JsonProperty("filterPOS") boolean filterPOS, @JsonProperty("stemTokens") boolean stemTokens) {
			this.dataTableId = dataTableId;
			this.caption = caption;
			this.description = description;
			this.recognizeSentences = recognizeSentences;
			this.alphabeticalOnly = alphabeticalOnly;
			this.filterPOS = filterPOS;
			this.stemTokens = stemTokens;
		}

		@Override
		public String dataTableId() {
			return this.dataTableId;
		}

		@Override
		public String caption() {
			return this.caption;
		}

		@Override
		public String description() {
			return this.description;
		}

		public boolean recognizeSentences() {
			return this.recognizeSentences;
		}

		public boolean alphabeticalOnly() {
			return this.alphabeticalOnly;
		}

		public boolean filterPOS() {
			return this.filterPOS;
		}

		public boolean stemTokens() {
			return this.stemTokens;
		}

	}

	public static class TextAsTransactionsSchemeUploadResource extends TextSchemeUploadResource {

		private DFFilterType dFFilterType;

		private int dFFilterValue;

		public TextAsTransactionsSchemeUploadResource(@JsonProperty("schemeType") String schemeType,
				@JsonProperty("dataTableId") String dataTableId, @JsonProperty("caption") String caption,
				@JsonProperty("description") String description,
				@JsonProperty("recognizeSentences") boolean recognizeSentences,
				@JsonProperty("alphabeticalOnly") boolean alphabeticalOnly,
				@JsonProperty("filterPOS") boolean filterPOS, @JsonProperty("stemTokens") boolean stemTokens,
				@JsonProperty("dFFilterType") DFFilterType dFFilterType,
				@JsonProperty("dFFilterValue") int dFFilterValue) {
			super(schemeType, dataTableId, caption, description, recognizeSentences, alphabeticalOnly, filterPOS,
					stemTokens);
			this.dFFilterType = dFFilterType;
			this.dFFilterValue = dFFilterValue;
		}

		public DFFilterType dFFilterType() {
			return this.dFFilterType;
		}

		public int dFFilterValue() {
			return this.dFFilterValue;
		}

	}

	public static class TextAsBowTfIdfSchemeUploadResource extends TextSchemeUploadResource {

		private int numberOfBins;

		public TextAsBowTfIdfSchemeUploadResource(@JsonProperty("schemeType") String schemeType,
				@JsonProperty("dataTableId") String dataTableId, @JsonProperty("caption") String caption,
				@JsonProperty("description") String description,
				@JsonProperty("recognizeSentences") boolean recognizeSentences,
				@JsonProperty("alphabeticalOnly") boolean alphabeticalOnly,
				@JsonProperty("filterPOS") boolean filterPOS, @JsonProperty("stemTokens") boolean stemTokens,
				@JsonProperty("numberOfBins") int numberOfBins) {
			super(schemeType, dataTableId, caption, description, recognizeSentences, alphabeticalOnly, filterPOS,
					stemTokens);
			this.numberOfBins = numberOfBins;
		}

		public int numberOfBins() {
			return this.numberOfBins;
		}
	}

	// Suppress default constructor for non-instantiability
	private SchemeUploadResourcesV1() {
		throw new AssertionError();
	}

}
