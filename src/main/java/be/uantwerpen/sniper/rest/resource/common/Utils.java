/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.common;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.DataTableSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.SchemeAttribute;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.Proposition;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Utils {

	public static String extractAttributeNameFromProposition(Proposition proposition) {
		if (AttributeBasedProposition.class.isAssignableFrom(proposition.getClass())) {
			return ((AttributeBasedProposition<?>) proposition).attribute().caption();
		} else if (proposition.name().contains("=")) {
			return proposition.name().split("=")[0];
		}
		return "Item";

	}

	public static String extractAttributeIconFromActiveWorksheet(String caption, ActiveWorksheet activeWorksheet) {
		if (DataTableSchemeMetaInfo.class.isAssignableFrom(activeWorksheet.schemeMetaInfo().getClass())) {
			DataTableSchemeMetaInfo metaInfo = (DataTableSchemeMetaInfo) activeWorksheet.schemeMetaInfo();
			for (SchemeAttribute attribute : metaInfo.attributes()) {
				if (attribute.caption().equals(caption)) {
					return attribute.icon();
				}
			}
		}
		return "empty";
	}

	public static String extractPropositionValueFromProposition(Proposition proposition) {
		if (AttributeBasedProposition.class.isAssignableFrom(proposition.getClass())) {
			return ((AttributeBasedProposition<?>) proposition).constraint().description();
		} else if (proposition.name().contains("=")) {
			return proposition.name().split("=")[1];
		}
		return proposition.name();
	}

	// Suppress default constructor for non-instantiability
	private Utils() {
		throw new AssertionError();
	}

}
