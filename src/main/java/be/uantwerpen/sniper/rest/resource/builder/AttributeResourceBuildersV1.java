/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.common.AttributeType;
import be.uantwerpen.sniper.rest.resource.AttributeResourcesV1;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricAttribute;
import de.unibonn.realkd.data.table.attribute.DefaultDateAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class AttributeResourceBuildersV1 {

	private static ResourceBuilder<Attribute<?>, AttributeResourcesV1.SimpleAttributeResource> simpleAttributeResourceBuilder;

	private static ResourceBuilder<Attribute<?>, AttributeResourcesV1.AttributeResource> attributeResourceBuilder;

	public static ResourceBuilder<Attribute<?>, AttributeResourcesV1.SimpleAttributeResource> simpleAttributeResourceBuilder() {
		return simpleAttributeResourceBuilder != null ? simpleAttributeResourceBuilder
				: (simpleAttributeResourceBuilder = new SimpleAttributeResourceBuilder());
	}

	public static ResourceBuilder<Attribute<?>, AttributeResourcesV1.AttributeResource> attributeResourceBuilder() {
		return attributeResourceBuilder != null ? attributeResourceBuilder
				: (attributeResourceBuilder = new AttributeResourceBuilder());
	}

	private static class SimpleAttributeResourceBuilder
			implements ResourceBuilder<Attribute<?>, AttributeResourcesV1.SimpleAttributeResource> {

		@Override
		public AttributeResourcesV1.SimpleAttributeResource build(Attribute<?> attribute) {
			return new AttributeResourcesV1.SimpleAttributeResource(attribute.identifier().toString(), attribute);
		}

	}

	private static class AttributeResourceBuilder
			implements ResourceBuilder<Attribute<?>, AttributeResourcesV1.AttributeResource> {

		@Override
		public AttributeResourcesV1.AttributeResource build(Attribute<?> attribute) {
			if (attribute instanceof CategoricAttribute) {
				return new AttributeResourcesV1.AttributeResource(attribute.identifier(), attribute.caption(),
						AttributeType.CATEGORIC, AttributeInfoListBuildersV1.attributeInfoListBuilder(attribute)
								.build(attribute).get(0).getRight(),
						ImmutableList.of("categories"));
			} else if (attribute instanceof MetricAttribute) {
				return new AttributeResourcesV1.AttributeResource(
						attribute.identifier(), attribute.caption(), AttributeType.METRIC, AttributeInfoListBuildersV1
								.attributeInfoListBuilder(attribute).build(attribute).get(0).getRight(),
						ImmutableList.of("metricHistogram", "boxPlot"));
			} else if (attribute instanceof DefaultDateAttribute) {
				return new AttributeResourcesV1.AttributeResource(
						attribute.identifier(), attribute.caption(), AttributeType.DATE, AttributeInfoListBuildersV1
								.attributeInfoListBuilder(attribute).build(attribute).get(0).getRight(),
						ImmutableList.of("dateHistogram"));
			}

			return null;
		}

	}

	// Suppress default constructor for non-instantiability
	private AttributeResourceBuildersV1() {
		throw new AssertionError();
	}

}
