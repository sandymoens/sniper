/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static java.lang.Math.min;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PagedResourceBuilders {

	public static <T> PagedResourceBuilder<T> genericPagedResourceBuilder(PagedResourcesAssembler<T> assembler) {
		return new GenericPagedResourceBuilder<>(assembler);
	}

	private static class GenericPagedResourceBuilder<T> implements PagedResourceBuilder<T> {

		private PagedResourcesAssembler<T> assembler;

		public GenericPagedResourceBuilder(PagedResourcesAssembler<T> assembler) {
			this.assembler = assembler;
		}

		@Override
		public PagedResources<Resource<T>> build(Pageable pageable, List<T> resources) {
			int pageNumber = min(pageable.getPageNumber(), (resources.size() - 1) / pageable.getPageSize());
			int offset = pageNumber * pageable.getPageSize();

			Page<T> page = new PageImpl<>(
					resources.subList(offset, min(resources.size(), offset + pageable.getPageSize())),
					new PageRequest(pageNumber, pageable.getPageSize()), resources.size());

			return this.assembler.toResource(page);
		}

		@Override
		public PagedResources<Resource<T>> buildPaged(Pageable pageable, List<T> resources, int maxSize) {
			Page<T> page = new PageImpl<>(resources, pageable, maxSize);

			return this.assembler.toResource(page);
		}

	}

	// Suppress default constructor for non-instantiability
	private PagedResourceBuilders() {
		throw new AssertionError();
	}

}
