/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.Map;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

import be.uantwerpen.sniper.common.DiscretizationType;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class WorksheetMetaInfoUploadResourcesV1 {

	public static class DataTableWorksheetMetaInfoUploadResource extends ResourceSupport
			implements WorksheetMetaInfoUploadResourceV1 {

		public DiscretizationType discretization;

		public int value;

		public Map<String, Map<String, String>> discretizations;

		public DataTableWorksheetMetaInfoUploadResource(
				@JsonProperty("discretization") DiscretizationType discretization, @JsonProperty("value") int value,
				@JsonProperty("discretizations") Map<String, Map<String, String>> discretizations) {
			this.discretization = discretization;
			this.value = value;
			this.discretizations = discretizations;
		}

		@Override
		public String type() {
			return "dataTable";
		}

	}

	public static class SequentialDataTableWorksheetMetaInfoUploadResource extends ResourceSupport
			implements WorksheetMetaInfoUploadResourceV1 {

		public DiscretizationType discretization;

		public int value;

		public Map<String, Map<String, String>> discretizations;

		public String idAttribute;

		public String distanceAttribute;

		public boolean generateSingleSequence;

		public SequentialDataTableWorksheetMetaInfoUploadResource(
				@JsonProperty("discretization") DiscretizationType discretization, @JsonProperty("value") int value,
				@JsonProperty("discretizations") Map<String, Map<String, String>> discretizations,
				@JsonProperty("idAttribute") String idAttribute,
				@JsonProperty("distanceAttribute") String distanceAttribute,
				@JsonProperty("generateSingleSequence") boolean generateSingleSequence) {
			this.discretization = discretization;
			this.discretizations = discretizations;
			this.value = value;
			this.idAttribute = idAttribute;
			this.distanceAttribute = distanceAttribute;
			this.generateSingleSequence = generateSingleSequence;
		}

		@Override
		public String type() {
			return "sequentialDataTable";
		}

	}

	public static class SequenceWorksheetMetaInfoUploadResource extends ResourceSupport
			implements WorksheetMetaInfoUploadResourceV1 {

		public DiscretizationType discretization;

		public int value;

		public SequenceWorksheetMetaInfoUploadResource(
				@JsonProperty("discretization") DiscretizationType discretization, @JsonProperty("value") int value) {
			this.discretization = discretization;
			this.value = value;
		}

		@Override
		public String type() {
			return "sequence";
		}

	}

	public static class TransactionsWorksheetMetaInfoUploadResource extends ResourceSupport
			implements WorksheetMetaInfoUploadResourceV1 {

		public TransactionsWorksheetMetaInfoUploadResource() {
		}

		@Override
		public String type() {
			return "transactions";
		}

	}

}
