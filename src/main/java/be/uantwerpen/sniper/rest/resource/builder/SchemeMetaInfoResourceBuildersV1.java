/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static java.util.stream.Collectors.toList;

import java.util.List;

import be.uantwerpen.sniper.dao.SchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.DataTableSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TextAsBowTfIdfSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TextAsTransactionsSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TransactionsSchemeMetaInfo;
import be.uantwerpen.sniper.rest.resource.AttributeResourcesV1;
import be.uantwerpen.sniper.rest.resource.AttributeResourcesV1.SchemeAttributeResource;
import be.uantwerpen.sniper.rest.resource.SchemeMetaInfoResourcesV1;
import be.uantwerpen.sniper.rest.resource.SchemeMetaInfoResourcesV1.SchemeMetaInfoResource;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SchemeMetaInfoResourceBuildersV1 {

	private static ResourceBuilder<SchemeMetaInfo, SchemeMetaInfoResourcesV1.SchemeMetaInfoResource> schemeMetaInfoResourceBuilder;

	public static ResourceBuilder<SchemeMetaInfo, SchemeMetaInfoResourcesV1.SchemeMetaInfoResource> schemeMetaInfoResourceBuilder() {
		return schemeMetaInfoResourceBuilder != null ? schemeMetaInfoResourceBuilder
				: (schemeMetaInfoResourceBuilder = new SchemeMetaInfoResourceBuilder());
	}

	private static class SchemeMetaInfoResourceBuilder
			implements ResourceBuilder<SchemeMetaInfo, SchemeMetaInfoResourcesV1.SchemeMetaInfoResource> {

		@Override
		public SchemeMetaInfoResource build(SchemeMetaInfo schemeMetaInfo) {
			if (DataTableSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				return build((DataTableSchemeMetaInfo) schemeMetaInfo);
			} else if (TransactionsSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				return build((TransactionsSchemeMetaInfo) schemeMetaInfo);
			} else if (TextAsTransactionsSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				return build((TextAsTransactionsSchemeMetaInfo) schemeMetaInfo);
			} else if (TextAsBowTfIdfSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				return build((TextAsBowTfIdfSchemeMetaInfo) schemeMetaInfo);
			}

			return null;
		}

		private SchemeMetaInfoResource build(DataTableSchemeMetaInfo schemeMetaInfo) {
			List<SchemeAttributeResource> attributes = schemeMetaInfo.attributes().stream().map(
					a -> new AttributeResourcesV1.SchemeAttributeResource(a.caption(), a.type(), a.active(), a.icon()))
					.collect(toList());

			return new SchemeMetaInfoResourcesV1.DataTableSchemeMetaInfoResource(schemeMetaInfo.delimiter(),
					schemeMetaInfo.dateTimeFormat(), attributes);
		}

		private SchemeMetaInfoResource build(TransactionsSchemeMetaInfo schemeMetaInfo) {
			return new SchemeMetaInfoResourcesV1.TransactionsSchemeMetaInfoResource(schemeMetaInfo.delimiter());
		}

		private SchemeMetaInfoResource build(TextAsTransactionsSchemeMetaInfo schemeMetaInfo) {
			return new SchemeMetaInfoResourcesV1.TextAsTransactionsSchemeMetaInfoResource(
					schemeMetaInfo.recognizeSentences(), schemeMetaInfo.alphabeticalOnly(), schemeMetaInfo.filterPOS(),
					schemeMetaInfo.stemTokens(), schemeMetaInfo.dFFilterType(), schemeMetaInfo.dFFilterValue());
		}

		private SchemeMetaInfoResource build(TextAsBowTfIdfSchemeMetaInfo schemeMetaInfo) {
			return new SchemeMetaInfoResourcesV1.TextAsBowTfIdfSchemeMetaInfoResource(
					schemeMetaInfo.recognizeSentences(), schemeMetaInfo.alphabeticalOnly(), schemeMetaInfo.filterPOS(),
					schemeMetaInfo.stemTokens(), schemeMetaInfo.numberOfBins());
		}

	}

	// Suppress default constructor for non-instantiability
	private SchemeMetaInfoResourceBuildersV1() {
		throw new AssertionError();
	}

}
