/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.reverse;
import static java.util.Collections.sort;
import static java.util.stream.Collectors.toList;

import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricAttribute;
import de.unibonn.realkd.data.table.attribute.DefaultDateAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.3.0
 * @version 0.3.0
 */
public class AttributeInfoListBuildersV1 {

	private static ResourceBuilder<Attribute<?>, List<Pair<String, List<Pair<String, String>>>>> defaultAttributeInfoListBuilder;
	private static ResourceBuilder<Attribute<?>, List<Pair<String, List<Pair<String, String>>>>> dateAttributeInfoListBuilder;
	private static ResourceBuilder<Attribute<?>, List<Pair<String, List<Pair<String, String>>>>> metricAttributeInfoListBuilder;
	private static ResourceBuilder<Attribute<?>, List<Pair<String, List<Pair<String, String>>>>> categoricAttributeInfoListBuilder;

	public static ResourceBuilder<Attribute<?>, List<Pair<String, List<Pair<String, String>>>>> attributeInfoListBuilder(
			Attribute<?> attribute) {
		if (attribute instanceof DefaultDateAttribute) {
			return dateAttributeInfoListBuilder != null ? dateAttributeInfoListBuilder
					: (dateAttributeInfoListBuilder = new DateAttributeInfoListBuilder());
		} else if (attribute instanceof MetricAttribute) {
			return metricAttributeInfoListBuilder != null ? metricAttributeInfoListBuilder
					: (metricAttributeInfoListBuilder = new MetricAttributeInfoListBuilder());
		} else if (attribute instanceof CategoricAttribute) {
			return categoricAttributeInfoListBuilder != null ? categoricAttributeInfoListBuilder
					: (categoricAttributeInfoListBuilder = new CategoricAttributeInfoListBuilder());
		}
		return defaultAttributeInfoListBuilder != null ? defaultAttributeInfoListBuilder
				: (defaultAttributeInfoListBuilder = new DefaultAttributeInfoListBuilder());
	}

	private static class DefaultAttributeInfoListBuilder
			implements ResourceBuilder<Attribute<?>, List<Pair<String, List<Pair<String, String>>>>> {

		@Override
		public List<Pair<String, List<Pair<String, String>>>> build(Attribute<?> attribute) {
			return ImmutableList.of();
		}

	}

	private static class DateAttributeInfoListBuilder
			implements ResourceBuilder<Attribute<?>, List<Pair<String, List<Pair<String, String>>>>> {

		@Override
		public List<Pair<String, List<Pair<String, String>>>> build(Attribute<?> attribute) {
			DefaultDateAttribute dAttribute = (DefaultDateAttribute) attribute;

			List<Pair<String, String>> info = newArrayList();

			Pair<Date, Date> minMax = getMinMaxDate(dAttribute);

			info.add(Pair.of("Earliest date", minMax.getLeft().toString()));
			info.add(Pair.of("Latest date", minMax.getRight().toString()));
			info.add(Pair.of("Non-missing values count", String.format("%d", dAttribute.nonMissingValues().size())));
			info.add(Pair.of("Missing values count", String.format("%d", dAttribute.missingPositions().size())));

			return newArrayList(Pair.of("Meta info", info));
		}

		private Pair<Date, Date> getMinMaxDate(DefaultDateAttribute dAttribute) {
			Iterator<Date> it = dAttribute.nonMissingValues().iterator();

			Date min = it.next();
			Date max = min;

			while (it.hasNext()) {
				Date n = it.next();

				if (n.before(min)) {
					min = n;
				}
				if (n.after(max)) {
					max = n;
				}
			}

			return Pair.of(min, max);
		}

	}

	private static class MetricAttributeInfoListBuilder
			implements ResourceBuilder<Attribute<?>, List<Pair<String, List<Pair<String, String>>>>> {

		@Override
		public List<Pair<String, List<Pair<String, String>>>> build(Attribute<?> attribute) {
			MetricAttribute mAttribute = (MetricAttribute) attribute;

			List<Pair<String, String>> info = newArrayList();

			info.add(Pair.of("Min value", String.format("%.4f", mAttribute.min())));
			info.add(Pair.of("Max value", String.format("%.4f", mAttribute.max())));
			info.add(Pair.of("Mean", String.format("%.4f", mAttribute.mean())));
			info.add(Pair.of("Median", String.format("%.4f", mAttribute.median())));
			info.add(Pair.of("Lower quartile", String.format("%.4f", mAttribute.lowerQuartile())));
			info.add(Pair.of("Upper quartile", String.format("%.4f", mAttribute.upperQuartile())));
			info.add(Pair.of("Non-missing values count", String.format("%d", mAttribute.nonMissingValues().size())));
			info.add(Pair.of("Missing values count", String.format("%d", mAttribute.missingPositions().size())));
			info.add(Pair.of("Non-missing distinct values count",
					String.format("%d", mAttribute.distinctNonMissingValuesInOrder().size())));

			return newArrayList(Pair.of("Meta info", info));
		}

	}

	private static class CategoricAttributeInfoListBuilder
			implements ResourceBuilder<Attribute<?>, List<Pair<String, List<Pair<String, String>>>>> {

		@Override
		public List<Pair<String, List<Pair<String, String>>>> build(Attribute<?> attribute) {
			CategoricAttribute<?> cAttribute = (CategoricAttribute<?>) attribute;

			List<Pair<String, String>> info = newArrayList();

			info.add(Pair.of("Categories count", String.format("%d", cAttribute.categories().size())));
			info.add(Pair.of("Non-missing values count", String.format("%d", cAttribute.numberOfNonMissingValues())));
			info.add(Pair.of("Missing values count", String.format("%d", cAttribute.missingPositions().size())));

			List<Pair<?, Double>> categories = getSortedCategoryFrequencies(cAttribute);
			List<Pair<String, String>> cat = categories.stream().map(pair -> Pair
					.of("Category: " + pair.getKey().toString(), String.format("%.2f%%", pair.getValue() * 100)))
					.collect(toList());

			return newArrayList(Pair.of("Meta info", info), Pair.of("Categories", cat));
		}

		private List<Pair<?, Double>> getSortedCategoryFrequencies(CategoricAttribute<?> attribute) {
			List<Pair<?, Double>> categories = Lists.newArrayList();
			for (int i = 0; i < attribute.categories().size(); i++) {
				categories.add(Pair.of(attribute.categories().get(i), attribute.categoryFrequencies().get(i)));
			}

			sort(categories, new Comparator<Pair<?, Double>>() {

				@Override
				public int compare(Pair<?, Double> o1, Pair<?, Double> o2) {
					int c = o1.getValue().compareTo(o2.getValue());

					if (c != 0) {
						return c;
					}

					return o1.getKey().toString().compareTo(o2.getKey().toString());
				}

			});

			reverse(categories);
			return categories;
		}

	}

	// Suppress default constructor for non-instantiability
	public AttributeInfoListBuildersV1() {
		throw new AssertionError();
	}

}
