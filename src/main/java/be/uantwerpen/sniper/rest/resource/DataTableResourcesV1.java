/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class DataTableResourcesV1 {

	public static class DataTableResource extends ResourceSupport {

		public final String dataTableId;

		public final String caption;

		public final String description;

		public DataTableResource(String id, String caption, String description) {
			this.dataTableId = id;
			this.caption = caption;
			this.description = description;
		}

	}

	public static class ExpandedDataTableResource extends ResourceSupport {

		public final String dataTableId;

		public final String caption;

		public final String description;

		public final List<SchemeResourcesV1.SchemeResource> schemes;

		public ExpandedDataTableResource(String id, String caption, String description,
				List<SchemeResourcesV1.SchemeResource> schemes) {
			this.dataTableId = id;
			this.caption = caption;
			this.description = description;
			this.schemes = schemes;
		}

	}

	// Suppress default constructor for non-instantiability
	private DataTableResourcesV1() {
		throw new AssertionError();
	}

}
