/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.common.Utils.method;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import be.uantwerpen.sniper.controller.rest.SchemeController;
import be.uantwerpen.sniper.dao.SchemeMetaInfo;
import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.rest.resource.DataTableResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1.ListResource;
import be.uantwerpen.sniper.rest.resource.SchemeMetaInfoResourcesV1;
import be.uantwerpen.sniper.rest.resource.SchemeResourcesV1;
import be.uantwerpen.sniper.rest.resource.SchemeResourcesV1.SchemeResource;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SchemeResourceBuildersV1 {

	private static ResourceBuilder<Scheme, SchemeResourcesV1.SchemeResource> schemeResourceBuilder;

	private static ResourceBuilder<List<Scheme>, ListResource<SchemeResourcesV1.SchemeResource>> schemesResourceBuilder;

	private static ResourceBuilder<SchemeWithMetaInfo, SchemeResourcesV1.ExpandedSchemeResource> expandedSchemeResourceBuilder;

	public static ResourceBuilder<Scheme, SchemeResourcesV1.SchemeResource> schemeResourceBuilder() {
		return schemeResourceBuilder != null ? schemeResourceBuilder
				: (schemeResourceBuilder = new SchemeResourceBuilder());
	}

	public static ResourceBuilder<List<Scheme>, ListResource<SchemeResourcesV1.SchemeResource>> schemesResourceBuilder() {
		return schemesResourceBuilder != null ? schemesResourceBuilder
				: (schemesResourceBuilder = new SchemesResourceBuilder());
	}

	public static ResourceBuilder<SchemeWithMetaInfo, SchemeResourcesV1.ExpandedSchemeResource> expandedSchemeResourceBuilder() {
		return expandedSchemeResourceBuilder != null ? expandedSchemeResourceBuilder
				: (expandedSchemeResourceBuilder = new ExpandedSchemeResourceBuilder());
	}

	public static class SchemeWithMetaInfo {

		private Scheme scheme;
		private SchemeMetaInfo schemeMetaInfo;
		private List<List<String>> rows;

		public SchemeWithMetaInfo(Scheme scheme, SchemeMetaInfo schemeMetaInfo, List<List<String>> rows) {
			this.scheme = scheme;
			this.schemeMetaInfo = schemeMetaInfo;
			this.rows = rows;
		}

	}

	private static class SchemeResourceBuilder implements ResourceBuilder<Scheme, SchemeResourcesV1.SchemeResource> {

		@Override
		public SchemeResourcesV1.SchemeResource build(Scheme scheme) {
			SchemeResourcesV1.SchemeResource schemeResource = new SchemeResourcesV1.SchemeResource(scheme.getId(),
					scheme.caption(), scheme.description());

			schemeResource.add(linkTo(SchemeController.class,
					method(SchemeController.class, "scheme", String.class, Principal.class, HttpServletRequest.class)
							.get(),
					scheme.getId(), null, null).withSelfRel());

			return schemeResource;
		}

	}

	private static class SchemesResourceBuilder
			implements ResourceBuilder<List<Scheme>, ListResource<SchemeResourcesV1.SchemeResource>> {

		@Override
		public ListResource<SchemeResource> build(List<Scheme> schemes) {
			List<SchemeResourcesV1.SchemeResource> schemeResources = SchemeResourceBuildersV1.schemeResourceBuilder()
					.build(schemes);

			ListResource<SchemeResourcesV1.SchemeResource> listResource = new ResourcesV1.ListResource<>(
					schemeResources);

			listResource.add(
					linkTo(method(SchemeController.class, "schemes", Principal.class, HttpServletRequest.class).get(),
							(Principal) null, (HttpServletRequest) null).withSelfRel());

			return listResource;
		}

	}

	private static class ExpandedSchemeResourceBuilder
			implements ResourceBuilder<SchemeWithMetaInfo, SchemeResourcesV1.ExpandedSchemeResource> {

		@Override
		public SchemeResourcesV1.ExpandedSchemeResource build(SchemeWithMetaInfo schemeWithMetaInfo) {
			Scheme scheme = schemeWithMetaInfo.scheme;
			SchemeMetaInfo schemeMetaInfo = schemeWithMetaInfo.schemeMetaInfo;

			DataTableResourcesV1.DataTableResource dataTableResource = DataTableResourceBuildersV1
					.dataTableResourceBuilder().build(scheme.getDataTable());

			SchemeMetaInfoResourcesV1.SchemeMetaInfoResource schemeMetaInfoResource = SchemeMetaInfoResourceBuildersV1
					.schemeMetaInfoResourceBuilder().build(schemeMetaInfo);

			SchemeResourcesV1.ExpandedSchemeResource schemeResource = new SchemeResourcesV1.ExpandedSchemeResource(
					scheme.getId(), scheme.caption(), scheme.description(), dataTableResource, schemeMetaInfoResource,
					schemeWithMetaInfo.rows);

			schemeResource.add(linkTo(SchemeController.class,
					method(SchemeController.class, "scheme", String.class, Principal.class, HttpServletRequest.class)
							.get(),
					scheme.getId(), null, null).withSelfRel());

			return schemeResource;
		}

	}

	// Suppress default constructor for non-instantiability
	private SchemeResourceBuildersV1() {
		throw new AssertionError();
	}

}
