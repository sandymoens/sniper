/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class AlgorithmUploadResourcesV1 {

	public static class MinerUploadAction implements ActionUploadResourceV1 {

		public String id;

		public List<Integer> ixs;

		public MinerUploadAction(@JsonProperty("type") String type, @JsonProperty("id") String id,
				@JsonProperty("ixs") List<Integer> ixs) {
			this.id = id;
			this.ixs = ixs;
		}

	}

	public static class PatternCollectionPostProcessorUploadAction implements ActionUploadResourceV1 {

		public String id;

		public PatternCollectionPostProcessorUploadAction(@JsonProperty("type") String type,
				@JsonProperty("id") String id) {
			this.id = id;
		}

	}

	public static class PatternPostProcessorUploadAction implements ActionUploadResourceV1 {

		public String id;

		public List<Integer> ixs;

		public PatternPostProcessorUploadAction(@JsonProperty("type") String type, @JsonProperty("id") String id,
				@JsonProperty("ixs") List<Integer> ixs) {
			this.id = id;
			this.ixs = ixs;
		}

	}

}
