/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource.builder;

import static be.uantwerpen.sniper.common.Utils.method;
import static com.google.common.collect.Lists.newArrayList;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.domain.Pageable;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.controller.rest.ActionController;
import be.uantwerpen.sniper.controller.rest.AlgorithmController;
import be.uantwerpen.sniper.controller.rest.FilterPatternController;
import be.uantwerpen.sniper.controller.rest.MeasureController;
import be.uantwerpen.sniper.controller.rest.PatternController;
import be.uantwerpen.sniper.controller.rest.VisualizationController;
import be.uantwerpen.sniper.controller.rest.WorksheetController;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import be.uantwerpen.sniper.rest.resource.QueryUploadResourcesV1.PropositionsQuery;
import be.uantwerpen.sniper.rest.resource.QueryUploadResourcesV1.SortedPropositionsQuery;
import be.uantwerpen.sniper.rest.resource.ResourcesV1;
import be.uantwerpen.sniper.rest.resource.ResourcesV1.ListResource;
import be.uantwerpen.sniper.rest.resource.WorksheetResourcesV1;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;
import de.unibonn.realkd.data.sequences.TableBasedSequentialPropositionalContext;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class WorksheetResourceBuildersV1 {

	private static ResourceBuilder<Worksheet, WorksheetResourcesV1.WorksheetResource> worksheetResourceBuilder;

	public static ResourceBuilder<Worksheet, WorksheetResourcesV1.WorksheetResource> worksheetResourceBuilder() {
		return worksheetResourceBuilder != null ? worksheetResourceBuilder
				: (worksheetResourceBuilder = new WorksheetResourceBuilder());
	}

	public static ResourceBuilder<Worksheet, ResourcesV1.ListResource<Pair<String, String>>> worksheetInfoResourceBuilder(
			ActiveWorksheet activeWorksheet) {
		return new WorksheetInfoResourceBuilder(activeWorksheet);
	}

	public static ResourceBuilder<Worksheet, WorksheetResourcesV1.WorksheetResource> activeWorksheetResourceBuilder(
			PropositionalContext context) {
		return new ActiveWorksheetResourceBuilder(context);
	}

	private static class WorksheetResourceBuilder
			implements ResourceBuilder<Worksheet, WorksheetResourcesV1.WorksheetResource> {

		@Override
		public WorksheetResourcesV1.WorksheetResource build(Worksheet worksheet) {
			WorksheetResourcesV1.WorksheetResource worksheetResource = new WorksheetResourcesV1.WorksheetResource(
					worksheet.getId(), worksheet.caption(), worksheet.description());

			return worksheetResource;
		}
	}

	private static class WorksheetInfoResourceBuilder
			implements ResourceBuilder<Worksheet, ResourcesV1.ListResource<Pair<String, String>>> {

		private ActiveWorksheet activeWorksheet;

		public WorksheetInfoResourceBuilder(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
		}

		@Override
		public ResourcesV1.ListResource<Pair<String, String>> build(Worksheet worksheet) {
			List<Pair<String, String>> info = newArrayList();

			info.add(Pair.of("Name", worksheet.caption()));
			info.add(Pair.of("Description", worksheet.description()));
			info.add(Pair.of("Scheme name", worksheet.getScheme().caption()));
			info.add(Pair.of("Data table name", worksheet.getScheme().getDataTable().caption()));

			PropositionalContext context = this.activeWorksheet.propositionalContext();

			if (TableBasedPropositionalContext.class.isAssignableFrom(context.getClass())) {
				info.add(Pair.of("Attribute count", String.format("%d",
						((TableBasedPropositionalContext) context).getDatatable().attributes().size())));
			}
			info.add(Pair.of("Proposition count", String.format("%d", context.propositions().size())));

			if (context instanceof SingleSequencePropositionalContext) {
				info.add(Pair.of("Event count",
						String.format("%d", ((SingleSequencePropositionalContext) context).events().size())));
			}

			ListResource<Pair<String, String>> listResource = new ListResource<>(info);

			listResource.add(linkTo(method(WorksheetController.class, "worksheetInfo", String.class, Principal.class,
					HttpServletRequest.class).get(), worksheet.getId(), null, null).withSelfRel());

			return listResource;
		}

	}

	private static class ActiveWorksheetResourceBuilder
			implements ResourceBuilder<Worksheet, WorksheetResourcesV1.WorksheetResource> {

		private PropositionalContext context;

		private ActiveWorksheetResourceBuilder(PropositionalContext context) {
			this.context = context;
		}

		@Override
		public WorksheetResourcesV1.WorksheetResource build(Worksheet worksheet) {
			WorksheetResourcesV1.WorksheetResource worksheetResource = new WorksheetResourcesV1.WorksheetResource("id",
					"caption", "description");

			worksheetResource.add(linkTo(method(WorksheetController.class, "worksheet", String.class, Principal.class,
					HttpServletRequest.class).get(), worksheet.getId(), null, null).withSelfRel());

			worksheetResource.add(linkTo(
					method(WorksheetController.class, "sortedPropositions", String.class, Principal.class,
							SortedPropositionsQuery.class, HttpServletRequest.class).get(),
					worksheet.getId(), null, null, null).withRel("propositions"));
			worksheetResource.add(linkTo(
					method(WorksheetController.class, "propositionsMultiMeasures", String.class, Principal.class,
							PropositionsQuery.class, HttpServletRequest.class).get(),
					worksheet.getId(), null, null, null).withRel("propositions2"));

			if (TableBasedPropositionalContext.class.isAssignableFrom(this.context.getClass())
					&& !TableBasedSequentialPropositionalContext.class.isAssignableFrom(this.context.getClass())) {
				worksheetResource
						.add(linkTo(
								method(WorksheetController.class, "attributes", String.class, Principal.class,
										Pageable.class, HttpServletRequest.class).get(),
								worksheet.getId(), null, null, null).withRel("attributes"));
			}
//			}

			worksheetResource.add(linkTo(method(WorksheetController.class, "properties", String.class, Principal.class,
					HttpServletRequest.class).get(), worksheet.getId(), null, null).withRel("properties"));
			worksheetResource
					.add(linkTo(method(WorksheetController.class, "worksheetInfo", String.class, Principal.class,
							HttpServletRequest.class).get(), worksheet.getId(), null, null).withRel("worksheetInfo"));
			worksheetResource.add(
					linkTo(method(PatternController.class, "patterns", String.class, Principal.class, Pageable.class,
							HttpServletRequest.class).get(), worksheet.getId(), null, null, null).withRel("patterns"));
			worksheetResource.add(linkTo(method(FilterPatternController.class, "filterPattern", String.class,
					Principal.class, HttpServletRequest.class).get(), worksheet.getId(), null, null, null)
							.withRel("filterPattern"));
			worksheetResource.add(linkTo(method(FilterPatternController.class, "parameters", String.class,
					Principal.class, HttpServletRequest.class).get(), worksheet.getId(), null, null)
							.withRel("filterParameters"));
			worksheetResource.add(linkTo(
					method(MeasureController.class, "measures", String.class, Principal.class, HttpServletRequest.class)
							.get(),
					worksheet.getId(), null, null).withRel("measures"));
			worksheetResource
					.add(linkTo(method(MeasureController.class, "sortingMeasures", String.class, Principal.class,
							HttpServletRequest.class).get(), worksheet.getId(), null, null).withRel("sortingMeasures"));
			worksheetResource
					.add(linkTo(method(VisualizationController.class, "visualizations", String.class, Principal.class,
							HttpServletRequest.class).get(), worksheet.getId(), null, null).withRel("visualizations"));
			worksheetResource.add(linkTo(
					method(AlgorithmController.class, "miners", String.class, Principal.class, HttpServletRequest.class)
							.get(),
					worksheet.getId(), null, null).withRel("miners"));
			worksheetResource.add(linkTo(method(AlgorithmController.class, "patternPostProcessors", String.class,
					Principal.class, HttpServletRequest.class).get(), worksheet.getId(), null, null)
							.withRel("patternPostProcessors"));
			worksheetResource.add(linkTo(method(AlgorithmController.class, "patternCollectionPostProcessors",
					String.class, Principal.class, HttpServletRequest.class).get(), worksheet.getId(), null, null)
							.withRel("patternCollectionPostProcessors"));
			worksheetResource.add(linkTo(
					method(ActionController.class, "actions", String.class, Principal.class, HttpServletRequest.class)
							.get(),
					worksheet.getId(), null, null).withRel("actions"));
			worksheetResource.add(linkTo(
					method(PatternController.class, "downloadPatterns", String.class, Principal.class,
							HttpServletRequest.class, HttpServletResponse.class).get(),
					worksheet.getId(), null, null, null).withRel("downloadPatterns"));

			return worksheetResource;
		}

	}

	// Suppress default constructor for non-instantiability
	private WorksheetResourceBuildersV1() {
		throw new AssertionError();
	}

}
