/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

import be.uantwerpen.sniper.common.AttributeType;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 * @author Sandy Moens
 * @since 0.0.1
 * @version 0.2.0
 */
public class AttributeResourcesV1 {

	public static class SimpleAttributeResource {

		public final String id;

		public final String name;

		public final String type;

		public SimpleAttributeResource(String id, Attribute<?> attribute) {
			this.id = id;
			this.name = attribute.caption();
			this.type = attribute.getClass().getSimpleName().replace("Default", "").replace("Attribute", "")
					.toLowerCase();
		}

	}

	public static class AttributeResource extends ResourceSupport {

		public final String id;

		public final String a_id;

		public final String name;

		public final String type;

		public final List<Pair<String, String>> info;

		public final List<String> visualizations;

		public AttributeResource(Identifier identifier, String name, AttributeType type,
				List<Pair<String, String>> info, List<String> visualizations) {
			this.id = identifier.toString();
			this.a_id = this.id;
			this.name = name;
			this.type = type.toString().toLowerCase();
			this.info = info;
			this.visualizations = visualizations;
		}

	}

	public static class SchemeAttributeResource extends ResourceSupport {

		public final String caption;

		public final AttributeType type;

		public final boolean active;

		public final String icon;

		public SchemeAttributeResource(@JsonProperty("caption") String caption,
				@JsonProperty("type") AttributeType type, @JsonProperty("active") boolean active,
				@JsonProperty("icon") String icon) {
			this.caption = caption;
			this.type = type;
			this.active = active;
			this.icon = icon;
		}

		public String caption() {
			return this.caption;
		}

		public be.uantwerpen.sniper.common.AttributeType type() {
			return this.type;
		}

		public boolean active() {
			return this.active;
		}

	}

	// Suppress default constructor for non-instantiability
	private AttributeResourcesV1() {
		throw new AssertionError();
	}

}
