/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.rest.resource;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
		@JsonSubTypes.Type(value = PatternActionUploadResourcesV1.ClearPatternsUploadActionResource.class, name = "clearPatterns"),
		@JsonSubTypes.Type(value = PatternActionUploadResourcesV1.DeletePatternsUploadActionResource.class, name = "deletePatterns"),
		@JsonSubTypes.Type(value = PatternActionUploadResourcesV1.CreateNewPatternUploadActionResource.class, name = "createNewPattern"),
		@JsonSubTypes.Type(value = PatternActionUploadResourcesV1.SortPatternsUploadActionResource.class, name = "sortPatterns"),
		@JsonSubTypes.Type(value = PatternActionUploadResourcesV1.MovePatternUploadActionResource.class, name = "movePattern"),
		@JsonSubTypes.Type(value = PatternActionUploadResourcesV1.AddPatternUploadActionResource.class, name = "addPattern"),
		@JsonSubTypes.Type(value = DescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource.class, name = "addToDescriptor"),
		@JsonSubTypes.Type(value = DescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource.class, name = "removeFromDescriptor"),
		@JsonSubTypes.Type(value = FilterPatternActionUploadResourcesV1.ClearFilterPatternUploadActionResource.class, name = "clearFilterPattern"),
		@JsonSubTypes.Type(value = FilterPatternActionUploadResourcesV1.CreateNewFilterPatternUploadActionResource.class, name = "createNewFilterPattern"),
		@JsonSubTypes.Type(value = FilterDescriptorActionUploadResourcesV1.AddToDescriptorUploadActionResource.class, name = "addToFilterDescriptor"),
		@JsonSubTypes.Type(value = FilterDescriptorActionUploadResourcesV1.RemoveFromDescriptorUploadActionResource.class, name = "removeFromFilterDescriptor"),
		@JsonSubTypes.Type(value = CommandActionUploadResourcesV1.UndoRedoCommandUploadAction.class, name = "undoRedo"),
		@JsonSubTypes.Type(value = AlgorithmUploadResourcesV1.MinerUploadAction.class, name = "miner"),
		@JsonSubTypes.Type(value = AlgorithmUploadResourcesV1.PatternCollectionPostProcessorUploadAction.class, name = "patternCollectionPostProcessor"),
		@JsonSubTypes.Type(value = AlgorithmUploadResourcesV1.PatternPostProcessorUploadAction.class, name = "patternPostProcessor"),
		@JsonSubTypes.Type(value = WorksheetUploadResourcesV1.SaveWorksheetUploadActionResource.class, name = "saveWorksheet") })
public interface ActionUploadResourceV1 {

}
