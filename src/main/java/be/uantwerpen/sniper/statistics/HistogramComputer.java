/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.statistics;

import static java.util.stream.Collectors.toList;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import org.apache.commons.lang3.mutable.MutableInt;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.3.0
 * @version 0.3.0
 */
public class HistogramComputer {

	private static List<Count> computeHistogram(int bins, double min, double max, List<Double> values) {
		double width = (max - min) / bins;

		int[] counts = new int[bins];

		values.stream().forEach(v -> {
			int ix = (int) Math.floor(((v - min) / width));
			counts[Math.min(ix, bins - 1)]++;
		});

		return IntStream.range(0, bins).mapToObj(i -> {
			String label = String.format("[%.2f;%.2f%s", min + (width * i), min + (width * (i + 1)),
					((i == bins - 1)) ? "]" : ")");
			return new Count(label, counts[i]);
		}).collect(toList());
	}

	public static List<Count> computeHistogram(int bins, List<Double> values) {
		double[] minMax = getMinMax(values);

		return computeHistogram(bins, minMax[0], minMax[1], values);
	}

	private static double[] getMinMax(List<Double> values) {
		double[] minMax = new double[] { Double.MAX_VALUE, Double.MIN_VALUE };

		values.stream().forEach(v -> {
			minMax[0] = minMax[0] < v ? minMax[0] : v;
			minMax[1] = minMax[1] > v ? minMax[1] : v;
		});

		return minMax;
	}

	public static List<Count> computeHistogramOptionals(int bins, double min, double max,
			List<Optional<Double>> values) {
		double width = (max - min) / bins;

		int[] counts = new int[bins];

		values.stream().forEach(v -> {
			if (!v.isPresent()) {
				return;
			}

			int ix = (int) Math.floor(((v.get() - min) / width));
			counts[Math.min(ix, bins - 1)]++;
		});

		return IntStream.range(0, bins).mapToObj(i -> {
			String label = String.format("[%.2f;%.2f%s", min + (width * i), min + (width * (i + 1)),
					((i == bins - 1)) ? "]" : ")");
			return new Count(label, counts[i]);
		}).collect(toList());
	}

	public static List<Count> computeHistogramOptionals(int bins, List<Optional<Double>> values) {
		double[] minMax = getMinMaxOptionals(values);

		return computeHistogramOptionals(bins, minMax[0], minMax[1], values);
	}

	private static double[] getMinMaxOptionals(List<Optional<Double>> values) {
		double[] minMax = new double[] { Double.MAX_VALUE, Double.MIN_VALUE };

		values.stream().forEach(v -> {
			if (!v.isPresent()) {
				return;
			}

			double value = v.get();

			minMax[0] = minMax[0] < value ? minMax[0] : value;
			minMax[1] = minMax[1] > value ? minMax[1] : value;
		});

		return minMax;
	}

	public static List<Count> computeDateHistogram(List<Optional<Date>> values) {

		Map<String, MutableInt> counts = Maps.newHashMap();

		for (Optional<Date> value : values) {
			if (!value.isPresent()) {
				continue;
			}

			Calendar c = Calendar.getInstance();
			c.setTime(value.get());

			String year = "" + c.get(Calendar.YEAR);
			MutableInt i = counts.get(year);

			if (i == null) {
				counts.put(year, i = new MutableInt(0));
			}

			i.increment();
		}

		return Sets.newTreeSet(counts.keySet()).stream().map(d -> new Count(d, counts.get(d).intValue()))
				.collect(toList());
	}

}
