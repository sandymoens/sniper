/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.pattern;

import static be.uantwerpen.sniper.rest.resource.common.Utils.extractAttributeIconFromActiveWorksheet;
import static com.google.common.collect.Maps.newHashMap;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.config.Settings;
import be.uantwerpen.sniper.visualization.BufferedImageVisualization;
import be.uantwerpen.sniper.visualization.Visualization;
import be.uantwerpen.sniper.visualization.pattern.CircularPatternDissectionVisualization;
import be.uantwerpen.sniper.visualization.pattern.PatternVisualizations;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Joey de Pauw
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternVisualizers {

	public static PatternVisualizer newPatternVisualizer(ActiveWorksheet activeWorksheet) {
		return new DefaultPatternVisualizer(activeWorksheet);
	}

	private static class DefaultPatternVisualizer implements PatternVisualizer {

		private final static String IMAGE_DIR = Settings.Instance.getProperty(Settings.IMAGE_DIR);

		private final Map<String, Map<Visualization<Pattern<?>, ?>, Map<String, Object>>> visualizationsCache;

		private int i = 0;

		private final String imageDir;

		private ActiveWorksheet activeWorksheet;

		public DefaultPatternVisualizer(ActiveWorksheet activeWorksheet) {
			this.activeWorksheet = activeWorksheet;
			this.visualizationsCache = newHashMap();
			this.i = 0;
			this.imageDir = Paths.get(IMAGE_DIR, activeWorksheet.worksheet().getId()).toString();

			try {
				Files.deleteIfExists(Paths.get(this.imageDir));
			} catch (IOException e) {
				e.printStackTrace();
			}
			new File(this.imageDir).mkdirs();
		}

		private String newFileName(String extension) {
			return this.i++ + "." + extension;
		}

		private Map<String, Object> getVisualizationData(Pattern<?> pattern, Visualization<Pattern<?>, ?> v) {
			String stringRepresentation = pattern.toString();

			Map<Visualization<Pattern<?>, ?>, Map<String, Object>> map = this.visualizationsCache
					.get(stringRepresentation);

			Map<String, Object> ret;

			if (map == null) {
				this.visualizationsCache.put(stringRepresentation, map = newHashMap());
			}

			if ((ret = map.get(v)) != null) {
				return ret;
			}

			ret = newHashMap();

			if (!v.isApplicable(pattern)) {
				return ret;
			}

			if (BufferedImageVisualization.class.isAssignableFrom(v.getClass())) {
				BufferedImageVisualization<Pattern<?>> imageVis = (BufferedImageVisualization<Pattern<?>>) v;
				BufferedImage bi = imageVis.getVisualizationData(pattern);
				ret.put("type", "file");
				if (bi != null) {
					try {
						String file = newFileName("png");
						File file2 = Paths.get(this.imageDir, file).toFile();
						ImageIO.write(bi, "PNG", file2);
						ret.put("data", "img/" + this.activeWorksheet.worksheet().getId() + "/" + file);
					} catch (IOException e) {
						System.err.println(e);
					}
				}
			} else {
				Object data = v.getVisualizationData(pattern);
				ret.put("type", v.caption());
				ret.put("data", data);
				if (v instanceof CircularPatternDissectionVisualization) {
					// Add icon information
					Map<String, List<Map<String, Object>>> json = (Map<String, List<Map<String, Object>>>) data;
					List<Map<String, Object>> items = json.get("items");
					for (Map<String, Object> item : items) {
						String attribute = (String) item.get("label");
						if (attribute.contains("=")) {
							attribute = attribute.split("=")[0];
						}
						String icon = extractAttributeIconFromActiveWorksheet(attribute, this.activeWorksheet);
						item.put("icon", icon);
					}
				}
			}

			map.put(v, ret);

			return ret;
		}

		@Override
		public Map<String, Object> getVisualizations(Pattern<?> pattern) {
			Map<String, Object> visualizations = newHashMap();

			for (Visualization<Pattern<?>, ?> visualization : PatternVisualizations.PATTERN_VISUALIZATIONS) {
				if (!this.activeWorksheet.visualizationModel().status(visualization)) {
					continue;
				}

				visualizations.put(visualization.caption(), getVisualizationData(pattern, visualization));
			}

			return visualizations;
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternVisualizers() {
		throw new AssertionError();
	}

}
