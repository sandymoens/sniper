/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.pattern;

import static be.uantwerpen.sniper.utils.MeasureRegister.measureRegister;

import java.util.Set;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternDescriptorScorers {

	public static PatternDescriptorScorer newMeasureBasedPatternDescriptorScorer(Measure measure) {
		return new MeasureBasedPatternScorer(measureRegister().getMeasurementProcedures(measure));
	}

	private static class MeasureBasedPatternScorer implements PatternDescriptorScorer {

		private static MeasurementProcedure<? extends Measure, ? super PatternDescriptor> getMeasurementProcedure(
				Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> measurementProcedures,
				PatternDescriptor descriptor) {

			for (MeasurementProcedure<? extends Measure, ? super PatternDescriptor> measurementProcedure : measurementProcedures) {
				if (measurementProcedure.isApplicable(descriptor)) {
					return measurementProcedure;
				}
			}

			return null;
		}

		private Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> measurementProcedures;

		private MeasureBasedPatternScorer(
				Set<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> measurementProcedures) {
			this.measurementProcedures = measurementProcedures;
		}

		@Override
		public double score(PatternDescriptor patternDescriptor) {
			MeasurementProcedure<? extends Measure, ? super PatternDescriptor> measurementProcedure = getMeasurementProcedure(
					this.measurementProcedures, patternDescriptor);

			if (measurementProcedure == null) {
				return Double.NaN;
			}

			return measurementProcedure.perform(patternDescriptor).value();
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternDescriptorScorers() {
		throw new AssertionError();
	}

}
