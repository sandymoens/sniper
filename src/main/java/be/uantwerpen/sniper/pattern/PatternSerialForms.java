/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.pattern;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.data.propositions.EmptyProposition;
import be.uantwerpen.sniper.patterns.attributes.AttributeList;
import be.uantwerpen.sniper.patterns.attributes.AttributeListDescriptors;
import be.uantwerpen.sniper.patterns.attributes.AttributeLists;
import be.uantwerpen.sniper.utils.ModelFactoryRegister;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContext;
import de.unibonn.realkd.data.sequences.SingleSequencePropositionalContext;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.association.Associations;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.ModelDeviationMeasure;
import de.unibonn.realkd.patterns.episodes.Episode;
import de.unibonn.realkd.patterns.episodes.EpisodeDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRule;
import de.unibonn.realkd.patterns.episodes.EpisodeRuleDescriptors;
import de.unibonn.realkd.patterns.episodes.EpisodeRules;
import de.unibonn.realkd.patterns.episodes.Episodes;
import de.unibonn.realkd.patterns.functional.FunctionalPattern;
import de.unibonn.realkd.patterns.functional.FunctionalPatterns;
import de.unibonn.realkd.patterns.graphs.Edges;
import de.unibonn.realkd.patterns.graphs.GraphDescriptor;
import de.unibonn.realkd.patterns.graphs.GraphDescriptors;
import de.unibonn.realkd.patterns.graphs.Node;
import de.unibonn.realkd.patterns.graphs.Nodes;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.rules.AssociationRule;
import de.unibonn.realkd.patterns.rules.AssociationRules;
import de.unibonn.realkd.patterns.rules.DefaultRuleDescriptor;
import de.unibonn.realkd.patterns.sequence.DefaultSequenceDescriptor;
import de.unibonn.realkd.patterns.sequence.Sequence;
import de.unibonn.realkd.patterns.sequence.Sequences;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroups;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PatternSerialForms {

	public static class AssociationSerialForm implements PatternSerialForm<Association> {

		public List<Integer> elements;

		public AssociationSerialForm(@JsonProperty("elements") List<Integer> elements) {
			this.elements = elements;
		}

		@Override
		public Optional<Association> build(ActiveWorksheet activeWorksheet) throws ValidationException {
			PropositionalContext context = activeWorksheet.propositionalContext();

			return Optional.of(Associations
					.association(LogicalDescriptors.create(context.population(), this.elements.stream().map(i -> {
						if (i == -1) {
							return new EmptyProposition(context);
						}
						return context.proposition(i);
					}).collect(Collectors.toList())), ImmutableList.of()));
		}

	}

	public static class AssociationRuleSerialForm implements PatternSerialForm<AssociationRule> {

		public List<Integer> antecedent;

		public List<Integer> consequent;

		public AssociationRuleSerialForm(@JsonProperty("antecedent") List<Integer> antecedent,
				@JsonProperty("consequent") List<Integer> consequent) {
			this.antecedent = antecedent;
			this.consequent = consequent;
		}

		@Override
		public Optional<AssociationRule> build(ActiveWorksheet activeWorksheet) throws ValidationException {
			PropositionalContext context = activeWorksheet.propositionalContext();

			LogicalDescriptor antecedent = LogicalDescriptors.create(context.population(),
					this.antecedent.stream().map(i -> {
						if (i == -1) {
							return new EmptyProposition(context);
						}
						return context.proposition(i);
					}).collect(Collectors.toList()));

			LogicalDescriptor consequent = LogicalDescriptors.create(context.population(),
					this.consequent.stream().map(i -> {
						if (i == -1) {
							return new EmptyProposition(context);
						}
						return context.proposition(i);
					}).collect(Collectors.toList()));

			return Optional.of(AssociationRules.create(
					DefaultRuleDescriptor.create(context.population(), antecedent, consequent), ImmutableList.of()));
		}

	}

	public static class AttributeListSerialForm implements PatternSerialForm<AttributeList> {

		public List<String> elements;

		public AttributeListSerialForm(@JsonProperty("elements") List<String> elements) {
			this.elements = elements;
		}

		@Override
		public Optional<AttributeList> build(ActiveWorksheet activeWorksheet) throws ValidationException {
			PropositionalContext context = activeWorksheet.propositionalContext();

			if (!TableBasedPropositionalContext.class.isAssignableFrom(context.getClass())) {
				return Optional.empty();
			}

			DataTable dataTable = ((TableBasedPropositionalContext) context).getDatatable();

			List<Attribute<?>> attributes = this.elements.stream().map(s -> dataTable.attribute(Identifier.id(s)).get())
					.collect(toList());

			return Optional.of(AttributeLists.attributeList(dataTable.population(),
					AttributeListDescriptors.attributeListDescriptor(dataTable, attributes), ImmutableList.of()));
		}

	}

	public static class ExceptionalModelPatternSerialForm implements PatternSerialForm<ExceptionalModelPattern> {

		public List<String> targetAttributes;

		public List<Integer> extension;

		public String modelType;

		public ExceptionalModelPatternSerialForm(@JsonProperty("targetAttributes") List<String> targetAttributes,
				@JsonProperty("extension") List<Integer> extension, @JsonProperty("modelType") String modelType) {
			this.targetAttributes = targetAttributes;
			this.extension = extension;
			this.modelType = modelType;
		}

		@Override
		public Optional<ExceptionalModelPattern> build(ActiveWorksheet activeWorksheet) throws ValidationException {
			PropositionalContext context = activeWorksheet.propositionalContext();

			if (!TableBasedPropositionalContext.class.isAssignableFrom(context.getClass())) {
				return Optional.empty();
			}

			if (this.targetAttributes == null || this.extension == null || this.modelType == null) {
				return Optional.empty();
			}

			ModelFactory<?> modelFactory = null;

			for (ModelFactory<?> mf : ModelFactoryRegister.MODEL_FACTORIES) {
				if (mf.getClass().getSimpleName().equals(this.modelType)) {
					modelFactory = mf;
				}
			}

			if (modelFactory == null) {
				return Optional.empty();
			}

			DataTable dataTable = ((TableBasedPropositionalContext) context).getDatatable();

			List<Attribute<?>> targetAttributes = this.targetAttributes.stream()
					.map(s -> dataTable.attribute(Identifier.id(s)).get()).collect(toList());

			LogicalDescriptor extension = LogicalDescriptors.create(context.population(),
					this.extension.stream().map(i -> {
						if (i == -1) {
							return new EmptyProposition(context);
						}
						return context.proposition(i);
					}).collect(Collectors.toList()));

			Subgroup<?> subgroup = Subgroups.subgroup(extension, dataTable, targetAttributes, modelFactory);

			MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> procedure = null;

			for (MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> p : ExceptionalModelMining
					.modelDeviationMeasures()) {
				if (p.isApplicable(subgroup)) {
					procedure = p;
					break;
				}
			}

			return Optional.of(ExceptionalModelMining.emmPattern(subgroup, procedure.getMeasure(), ImmutableList.of()));
		}

	}

	public static class FunctionalPatternSerialForm implements PatternSerialForm<FunctionalPattern> {

		public List<String> domain;

		public List<String> coDomain;

		public FunctionalPatternSerialForm(@JsonProperty("domain") List<String> domain,
				@JsonProperty("coDomain") List<String> coDomain) {
			this.domain = domain;
			this.coDomain = coDomain;
		}

		@Override
		public Optional<FunctionalPattern> build(ActiveWorksheet activeWorksheet) throws ValidationException {
			PropositionalContext context = activeWorksheet.propositionalContext();

			if (!TableBasedPropositionalContext.class.isAssignableFrom(context.getClass())) {
				return Optional.empty();
			}

			DataTable dataTable = ((TableBasedPropositionalContext) context).getDatatable();

			Set<Attribute<?>> domain = this.domain.stream().map(s -> dataTable.attribute(Identifier.id(s)).get())
					.collect(toSet());

			Set<Attribute<?>> coDomain = this.coDomain.stream().map(s -> dataTable.attribute(Identifier.id(s)).get())
					.collect(toSet());

			return Optional.of(FunctionalPatterns
					.functionalPattern(FunctionalPatterns.binaryAttributeSetRelation(dataTable, domain, coDomain)));
		}

	}

	public static class SequenceSerialForm implements PatternSerialForm<Sequence> {

		public List<List<Integer>> timeSets;

		public SequenceSerialForm(@JsonProperty("timeSets") List<List<Integer>> timeSets) {
			this.timeSets = timeSets;
		}

		@Override
		public Optional<Sequence> build(ActiveWorksheet activeWorksheet) throws ValidationException {
			PropositionalContext context = activeWorksheet.propositionalContext();

			if (!SequentialPropositionalContext.class.isAssignableFrom(context.getClass())) {
				return Optional.empty();
			}

			SequentialPropositionalContext sContext = (SequentialPropositionalContext) context;

			List<List<Proposition>> orderedSetsDescriptors = this.timeSets.stream().map(timeSet -> {
				return timeSet.stream().map(i -> {
					if (i == -1) {
						return new EmptyProposition(context);
					}
					return context.proposition(i);
				}).collect(Collectors.toList());
			}).collect(Collectors.toList());

			return Optional.of(Sequences.create(DefaultSequenceDescriptor.create(sContext, orderedSetsDescriptors),
					ImmutableList.of()));
		}

	}

	public static class EpisodeSerialForm implements PatternSerialForm<Episode> {

		public List<List<Integer>> nodes;

		public List<List<Integer>> edges;

		public EpisodeSerialForm(@JsonProperty("nodes") List<List<Integer>> nodes,
				@JsonProperty("edges") List<List<Integer>> edges) {
			this.nodes = nodes;
			this.edges = edges;
		}

		@Override
		public Optional<Episode> build(ActiveWorksheet activeWorksheet) throws ValidationException {
			PropositionalContext context = activeWorksheet.propositionalContext();

			if (!SingleSequencePropositionalContext.class.isAssignableFrom(context.getClass())) {
				return Optional.empty();
			}

			Optional<Parameter<?>> property = activeWorksheet.propertiesModel().property("Window size");

			double windowSize = property.isPresent() ? (Double) property.get().current() : 10;

			SingleSequencePropositionalContext sContext = (SingleSequencePropositionalContext) context;

			List<Node> nodes = this.nodes.stream().map(l -> Nodes.create(l.get(0), sContext.proposition(l.get(1))))
					.collect(toList());

			GraphDescriptor graph = GraphDescriptors.create(nodes, ImmutableList.of());

			for (List<Integer> edge : this.edges) {
				graph = graph.specialization(Edges.create(edge.get(0), edge.get(1)));
			}

			return Optional.of(Episodes.create(EpisodeDescriptors.create(sContext, windowSize, graph),
					ImmutableList.of(), ImmutableList.of()));
		}

	}

	public static class EpisodeRuleSerialForm implements PatternSerialForm<EpisodeRule> {

		public List<List<Integer>> antecedentNodes;

		public List<List<Integer>> antecedentEdges;

		public List<List<Integer>> consequentNodes;

		public List<List<Integer>> consequentEdges;

		public EpisodeRuleSerialForm(@JsonProperty("antecedentNodes") List<List<Integer>> antecedentNodes,
				@JsonProperty("antecedentEdges") List<List<Integer>> antecedentEdges,
				@JsonProperty("consequentNodes") List<List<Integer>> consequentNodes,
				@JsonProperty("consequentEdges") List<List<Integer>> consequentEdges) {
			this.antecedentNodes = antecedentNodes;
			this.antecedentEdges = antecedentEdges;
			this.consequentNodes = consequentNodes;
			this.consequentEdges = consequentEdges;
		}

		@Override
		public Optional<EpisodeRule> build(ActiveWorksheet activeWorksheet) throws ValidationException {
			PropositionalContext context = activeWorksheet.propositionalContext();

			if (!SingleSequencePropositionalContext.class.isAssignableFrom(context.getClass())) {
				return Optional.empty();
			}

			Optional<Parameter<?>> property = activeWorksheet.propertiesModel().property("Window size");

			double windowSize = property.isPresent() ? (Double) property.get().current() : 10;

			SingleSequencePropositionalContext sContext = (SingleSequencePropositionalContext) context;

			List<Node> antecedentNodes = this.antecedentNodes.stream()
					.map(l -> Nodes.create(l.get(0), sContext.proposition(l.get(1)))).collect(toList());

			GraphDescriptor antecedent = GraphDescriptors.create(antecedentNodes, ImmutableList.of());

			for (List<Integer> edge : this.antecedentEdges) {
				antecedent = antecedent.specialization(Edges.create(edge.get(0), edge.get(1)));
			}

			List<Node> consequentNodes = this.consequentNodes.stream()
					.map(l -> Nodes.create(l.get(0), sContext.proposition(l.get(1)))).collect(toList());

			GraphDescriptor consequent = GraphDescriptors.create(consequentNodes, ImmutableList.of());

			for (List<Integer> edge : this.consequentEdges) {
				consequent = consequent.specialization(Edges.create(edge.get(0), edge.get(1)));
			}

			return Optional
					.of(EpisodeRules.create(EpisodeRuleDescriptors.create(sContext, windowSize, antecedent, consequent),
							ImmutableList.of(), ImmutableList.of()));
		}

	}

	// Suppress default constructor for non-instantiability
	private PatternSerialForms() {
		throw new AssertionError();
	}

}
