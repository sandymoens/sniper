/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.pattern;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import be.uantwerpen.sniper.ActiveWorksheet;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.patterns.Pattern;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = PatternSerialForms.AssociationSerialForm.class, name = "Association"),
		@JsonSubTypes.Type(value = PatternSerialForms.AssociationRuleSerialForm.class, name = "AssociationRule"),
		@JsonSubTypes.Type(value = PatternSerialForms.AttributeListSerialForm.class, name = "AttributeList"),
		@JsonSubTypes.Type(value = PatternSerialForms.ExceptionalModelPatternSerialForm.class, name = "ExceptionalModelPattern"),
		@JsonSubTypes.Type(value = PatternSerialForms.FunctionalPatternSerialForm.class, name = "FunctionalPattern"),
		@JsonSubTypes.Type(value = PatternSerialForms.SequenceSerialForm.class, name = "Sequence"),
		@JsonSubTypes.Type(value = PatternSerialForms.EpisodeSerialForm.class, name = "Episode"),
		@JsonSubTypes.Type(value = PatternSerialForms.EpisodeRuleSerialForm.class, name = "EpisodeRule") })
public interface PatternSerialForm<T extends Pattern<?>> extends RuntimeBuilder<Optional<T>, ActiveWorksheet> {

}
