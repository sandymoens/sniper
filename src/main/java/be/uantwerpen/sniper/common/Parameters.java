/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.common;

import static de.unibonn.realkd.common.base.Identifier.id;
import static de.unibonn.realkd.common.parameter.Parameters.booleanParameter;
import static de.unibonn.realkd.common.parameter.Parameters.doubleParameter;
import static de.unibonn.realkd.common.parameter.Parameters.integerParameter;
import static de.unibonn.realkd.common.parameter.Parameters.rangeEnumerableParameter;
import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;
import static java.util.stream.Collectors.toList;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Parameters {

	public static RangeEnumerableParameter<Boolean> keepOriginalPatternsParameter() {
		return booleanParameter(id("Keep original patterns"), "Keep original patterns",
				"Indicates if the original patterns should remain in the collection of patterns or not.");
	}

	public static DefaultRangeEnumerableParameter<TypeSelectorType> typeSelectorParameter() {
		return rangeEnumerableParameter(id("Type selector"), "Type selector", "Type of patterns to select",
				TypeSelectorType.class,
				() -> ImmutableList.of(TypeSelectorType.SELECTED, TypeSelectorType.SUB_PATTERNS,
						TypeSelectorType.SUPER_PATTERNS, TypeSelectorType.NON_SUB_PATTERNS,
						TypeSelectorType.NON_SUPER_PATTERNS));
	}

	public static Parameter<String> orderTypeParameter() {
		ImmutableList<OrderType> orderTypes = ImmutableList.of(OrderType.LARGER_OR_EQUAL_THAN, OrderType.LARGER_THAN,
				OrderType.SMALLER_THAN, OrderType.SMALLER_OR_EQUAL_THAN);

		List<String> orderTypesAsString = orderTypes.stream().map(o -> o.toString().toLowerCase().replace("_", " "))
				.collect(toList());

		return rangeEnumerableParameter(id("Order type"), "Order type", "Order type", String.class,
				() -> orderTypesAsString);
	}

	public static Parameter<Integer> topKParameter() {
		return integerParameter(id("Top-K"), "Top-K", "Number of patterns to find", 10, d -> d >= 0,
				"Set larger than 0");
	}

	public static Parameter<Integer> minSupportParameter(int maxSupport) {
		return integerParameter(id("Min Support"), "Minimum support", "The minimum amount of occurrences of a pattern",
				10, d -> d > 0 && d <= maxSupport, String.format("Value between 0 (excl) and %d (incl)", maxSupport));
	}

	public static Parameter<Integer> maxSupportParameter(int maxSupport) {
		return integerParameter(id("Max Support"), "Maximum support", "The maximum amount of occurrences of a pattern",
				maxSupport, d -> d > 0 && d <= maxSupport,
				String.format("Value between 0 (excl) and %d (incl)", maxSupport));
	}

	public static Parameter<Double> minFrequencyParameter() {
		return doubleParameter(id("Min Frequency"), "Minimum frequency",
				"The minimum amount of occurrences relative to the size of the data", 0.5, d -> d >= 0 && d <= 1,
				"Set between 0 and 1");
	}

	public static Parameter<Double> maxFrequencyParameter() {
		return doubleParameter(id("Max Frequency"), "Maximum frequency",
				"The maximum amount of occurrences relative to the size of the data", 1, d -> d >= 0 && d <= 1,
				"Set between 0 and 1");
	}

	public static Parameter<Double> minConfidenceParameter() {
		return doubleParameter(id("Min Confidence"), "Minimum confidence",
				"The minimum confidence that the antecedent implies the consequent", 0.8, d -> d >= 0 && d <= 1,
				"Set between 0 and 1");
	}

	public static Parameter<Double> maxConfidenceParameter() {
		return doubleParameter(id("Max Confidence"), "Maximum confidence",
				"The maximum confidence that the antecedent implies the consequent", 1, d -> d >= 0 && d <= 1,
				"Set between 0 and 1");
	}

	public static Parameter<Integer> minSizeParameter() {
		return integerParameter(id("Min Size"), "Minimum Size", "The minimum size of a pattern", 1, d -> d > 0,
				"Set a positive integer");
	}

	public static Parameter<Integer> maxSizeParameter() {
		return integerParameter(id("Max Size"), "Maximum Size", "The maximum size of a pattern", 10, d -> d > 0,
				"Set a positive integer");
	}

	public static Parameter<Boolean> superPatternsOnlyParameter() {
		return booleanParameter(id("Super patterns only"), "Super patterns only",
				"Indicates if only super patterns of the selected patterns should be reported.");
	}

	public static Parameter<Measure> measureParameter(List<Measure> measures) {
		return rangeEnumerableParameter(id("Measure"), "Measure", "The objective quality measure", String.class,
				() -> measures);
	}

	public static Parameter<Double> valueParameter() {
		return doubleParameter(id("Value"), "Value", "A value", 1, d -> true, "Set a real number");
	}

	public static Parameter<Integer> miningThresholdParameter() {
		return integerParameter(id("Value"), "Value", "put a threshold value", 10, d -> true, "set a real integer");
	}

	public static Parameter<Double> windowSizeParameter() {
		return doubleParameter(id("WindowSize"), "Window size", "put a threshold value", 10, d -> d > 0,
				"set a positive value");
	}

	public static Parameter<Integer> numberOfResultsParameter() {
		return integerParameter(id("Number Of Results"), "Number of results", "Number of results (patterns) to find",
				100, d -> d > 0, "Set a positive integer");
	}

	public static DefaultRangeEnumerableParameter<TypeSelectorEpisodes> antecedentTypeSelectorGraph() {
		return rangeEnumerableParameter(id("antecedent_type_selector"), "antecedent type selector",
				"Type of episodes to select", TypeSelectorEpisodes.class,
				() -> ImmutableList.of(TypeSelectorEpisodes.ANY, TypeSelectorEpisodes.SERIAL,
						TypeSelectorEpisodes.PARALLEL, TypeSelectorEpisodes.GENERAL));
	}

	public static DefaultRangeEnumerableParameter<TypeSelectorEpisodes> consequentTypeSelectorGraph() {
		return rangeEnumerableParameter(id("consequent_type_selector"), "consequent type selector",
				"Type of episodes to select", TypeSelectorEpisodes.class,
				() -> ImmutableList.of(TypeSelectorEpisodes.ANY, TypeSelectorEpisodes.SERIAL,
						TypeSelectorEpisodes.PARALLEL, TypeSelectorEpisodes.GENERAL));
	}

	public static Parameter<String> antecedentRegex() {
		return stringParameter(id("antecedentRegex"), "Antecedent Regex Filter",
				"Please the regular expression for left-hand side of the rule", "", d -> true,
				"set valid regular expression");
	}

	public static Parameter<LogicOperator> logic() {
		return rangeEnumerableParameter(id("Logic Operator"), "Logic Operator", "Logic Operator", LogicOperator.class,
				() -> ImmutableList.of(LogicOperator.and, LogicOperator.or));
	}

	public static Parameter<String> consequentRegex() {
		return stringParameter(id("consequentRegex"), "Consequent Regex Filter",
				"Please the regular expression for right-hand side of the rule", "", d -> true,
				"set valid regular expression");
	}

	public static Parameter<Double> zValueParameter() {
		return doubleParameter(id("z value"), "z-value", "The z-value", 2.5, d -> d >= 0, "Set larger than 0");
	}

	// Suppress default constructor for non-instantiability
	private Parameters() {
		throw new AssertionError();
	}

}
