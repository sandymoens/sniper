package be.uantwerpen.sniper.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.service.UserService;

@Component
public class UserValidator implements Validator {

	@Autowired
	private UserService userService;

	@Autowired
	private UpdateUserValidator updateUserValidator;

	@Override
	public boolean supports(Class<?> aClass) {
		return User.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		User user = (User) o;

		if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
			errors.rejectValue("username", "Size.userForm.username",
					"Username should be between 7 and 31 characters long.");
		}

		if (this.userService.findByUsername(user.getUsername()).isPresent()) {
			errors.rejectValue("username", "Duplicate.userForm.username", "Username already exists!");
		}

		this.updateUserValidator.validate(o, errors);
	}

}