/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.common;

import static de.unibonn.realkd.common.base.Identifier.id;
import static de.unibonn.realkd.common.parameter.Parameters.rangeEnumerableParameter;
import static de.unibonn.realkd.common.parameter.Parameters.subListParameter;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.data.sequences.TableBasedSingleSequencePropositionalContext;
import de.unibonn.realkd.data.table.DataTable;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class ParametersEpisodes {

	public static RangeEnumerableParameter<String> includedTypeParameter(String name, String description,
			TableBasedSingleSequencePropositionalContext context) {

		DataTable dataTable = context.datatable();

		List<String> types = dataTable.attributes().stream().map(a -> a.caption()).collect(toList());

		Supplier<List<String>> collectionComputer = () -> types;

		Predicate<List<String>> validator = typeList -> types.containsAll(typeList);

		return rangeEnumerableParameter(id(name), name, description, String.class, collectionComputer);
	}

	public static SubCollectionParameter<String, List<String>> includedTypesParameter(String name, String description,
			TableBasedSingleSequencePropositionalContext context) {

		DataTable dataTable = context.datatable();

		List<String> types = dataTable.attributes().stream().map(a -> a.caption()).collect(toList());

		Supplier<List<String>> collectionComputer = () -> types;

		Predicate<List<String>> validator = typeList -> types.containsAll(typeList);

		return subListParameter(id(name), name, description, collectionComputer, validator);
	}

}
