/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.common;

import java.lang.reflect.Method;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Utils {

	public static final ExecutorService threadPool = Executors.newCachedThreadPool();

	public static Optional<Method> method(Class<?> clazz, String name, Class<?>... parameterTypes) {
		try {
			Method method = clazz.getMethod(name, parameterTypes);

			return Optional.of(method);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}

		return Optional.empty();
	}

}
