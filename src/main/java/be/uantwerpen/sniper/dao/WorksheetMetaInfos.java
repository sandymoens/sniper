/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class WorksheetMetaInfos {

	public static class DataTableWorksheetMetaInfo implements WorksheetMetaInfo {

		@JsonProperty("discretization")
		private Discretization discretization;

		@JsonProperty("discretizations")
		private Map<String, Discretization> discretizations;

		public DataTableWorksheetMetaInfo(@JsonProperty("discretization") Discretization discretization,
				@JsonProperty("discretizations") Map<String, Discretization> discretizations) {
			this.discretization = discretization;
			this.discretizations = discretizations;
		}

		public Discretization discretization() {
			return this.discretization;
		}

		public Map<String, Discretization> discretizations() {
			return this.discretizations;
		}

	}

	public static class SequentialDataTableWorksheetMetaInfo implements WorksheetMetaInfo {

		@JsonProperty("discretization")
		private Discretization discretization;

		@JsonProperty("discretizations")
		private Map<String, Discretization> discretizations;

		@JsonProperty("idAttribute")
		private String idAttribute;

		@JsonProperty("distanceAttribute")
		private String distanceAttribute;

		@JsonProperty("generateSingleSequence")
		private boolean generateSingleSequence;

		public SequentialDataTableWorksheetMetaInfo(@JsonProperty("discretization") Discretization discretization,
				@JsonProperty("discretizations") Map<String, Discretization> discretizations,
				@JsonProperty("idAttribute") String idAttribute,
				@JsonProperty("distanceAttribute") String distanceAttribute,
				@JsonProperty("generateSingleSequence") boolean generateSingleSequence) {
			this.discretization = discretization;
			this.discretizations = discretizations;
			this.idAttribute = idAttribute;
			this.distanceAttribute = distanceAttribute;
			this.generateSingleSequence = generateSingleSequence;
		}

		public Discretization discretization() {
			return this.discretization;
		}

		public Map<String, Discretization> discretizations() {
			return this.discretizations;
		}

		public String idAttribute() {
			return this.idAttribute;
		}

		public String distanceAttribute() {
			return this.distanceAttribute;
		}

		public boolean generateSingleSequence() {
			return this.generateSingleSequence;
		}

	}

	public static class SequenceWorksheetMetaInfo implements WorksheetMetaInfo {

		@JsonProperty("discretization")
		private Discretization discretization;

		public SequenceWorksheetMetaInfo(@JsonProperty("discretization") Discretization discretization) {
			this.discretization = discretization;
		}

		public Discretization discretization() {
			return this.discretization;
		}

	}

	public static class TransactionsWorksheetMetaInfo implements WorksheetMetaInfo {

		public TransactionsWorksheetMetaInfo() {
		}

	}

}
