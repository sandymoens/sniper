/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import static be.uantwerpen.sniper.dao.PersistentStoragePathGenerators.persistentStoragePathGenerator;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newTreeSet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import be.uantwerpen.sniper.dao.Discretizations.DateBasedDiscretization;
import be.uantwerpen.sniper.dao.Discretizations.FixedDiscretization;
import be.uantwerpen.sniper.dao.Discretizations.TypeBasedDiscretization;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.ProcessedDataTableIdentifier;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.DFFilterType;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TextAsBowTfIdfSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TextAsTransactionsSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TextSchemeMetaInfo;
import be.uantwerpen.sniper.dao.WorksheetMetaInfos.DataTableWorksheetMetaInfo;
import be.uantwerpen.sniper.dao.WorksheetMetaInfos.SequentialDataTableWorksheetMetaInfo;
import be.uantwerpen.sniper.data.sequence.SequentialDataTableAsSingleSequenceFromTableFactory;
import be.uantwerpen.sniper.mining.text.BagOfWordsConverter.TFIDF;
import be.uantwerpen.sniper.mining.text.SentenceExtractor.OpenNLPSentenceExtractor;
import be.uantwerpen.sniper.mining.text.SentenceOperator;
import be.uantwerpen.sniper.mining.text.TextLanguage;
import be.uantwerpen.sniper.mining.text.TokenListOperator.DFFilter;
import be.uantwerpen.sniper.mining.text.TokenListOperator.EmptyTokenListFilter;
import be.uantwerpen.sniper.mining.text.TokenListOperator.POSFilter;
import be.uantwerpen.sniper.mining.text.TokenListOperator.TokenListStemmer;
import de.unibonn.realkd.data.propositions.ClusteringAttributeToPropositionsMapper;
import de.unibonn.realkd.data.propositions.DateAttributeToPropositionsMapper;
import de.unibonn.realkd.data.propositions.DefaultAttributeToPropositionMappers;
import de.unibonn.realkd.data.propositions.DefaultNumericAttributeToPropositionMappers;
import de.unibonn.realkd.data.propositions.DefaultPropositionalContext;
import de.unibonn.realkd.data.propositions.LegacyAttributesToPropositionsMapper;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.PropositionalContextFromTableBuilder;
import de.unibonn.realkd.data.propositions.PropositionalLogicFromTableManualBinningFactory;
import de.unibonn.realkd.data.propositions.PropositionalLogicFromTokenListsFactory;
import de.unibonn.realkd.data.propositions.PropositionalLogicFromTransactionFileFactory;
import de.unibonn.realkd.data.propositions.PropositionalizationRule;
import de.unibonn.realkd.data.sequences.SequentialPropositionalContextFromTableFactory;
import de.unibonn.realkd.data.sequences.SingleCategoricSequenceLoader;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTableFromCSVFileBuilder;
import de.unibonn.realkd.data.xarf.XarfImport;
import opennlp.tools.util.InvalidFormatException;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PropositionalContextLoaders {

	private static Map<DataFormatType, PropositionalContextLoader> handlers;

	public static <T extends SaveDataParameter<?>> void register(DataFormatType dataFormatType,
			PropositionalContextLoader handler) {
		handlers.put(dataFormatType, handler);
	}

	static {
		handlers = newHashMap();

		Reflections reflections = new Reflections(
				new ConfigurationBuilder().addUrls(ClasspathHelper.forPackage("be.uantwerpen.sniper.dao"))
						.setScanners(new MethodAnnotationsScanner()));

		reflections.getMethodsAnnotatedWith(PropositionalContextLoaderAnn.class).stream().forEach(m -> {
			try {
				register(m.getAnnotation(PropositionalContextLoaderAnn.class).dataFormatType(),
						(PropositionalContextLoader) m.invoke(null));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}

	public static PropositionalContextLoader propositionalContextLoader(DataFormatType dataFormatType) {
		return handlers.get(dataFormatType);
	}

	@PropositionalContextLoaderAnn(dataFormatType = DataFormatType.DATATABLE)
	public static PropositionalContextLoader dataTablePropositionalContextLoader() {
		return new DataTablePropositionalContextLoader();
	}

	@PropositionalContextLoaderAnn(dataFormatType = DataFormatType.SEQUENCEDATATABLE)
	public static PropositionalContextLoader sequenceDataTablePropositionalContextLoader() {
		return new SequenceDataTablePropositionalContextLoader();
	}

	@PropositionalContextLoaderAnn(dataFormatType = DataFormatType.TEXTFILE)
	public static PropositionalContextLoader textFileContextLoader() {
		return new TextFileContextLoader();
	}

	@PropositionalContextLoaderAnn(dataFormatType = DataFormatType.TRANSACTIONS)
	public static PropositionalContextLoader transactionPropositionalContextLoader() {
		return new TransactionPropositionalContextLoader();
	}

	@PropositionalContextLoaderAnn(dataFormatType = DataFormatType.SEQUENCE)
	public static PropositionalContextLoader SequenceDataTableLoader() {
		return new SequenceDataTableLoader();
	}

	private static List<PropositionalizationRule> dateAttributeToPropositionMappers(
			DateBasedDiscretization discretization) {
		return discretization.values().stream().map(m -> DateAttributeToPropositionsMapper.valueOf(m))
				.collect(Collectors.toList());
	}

	private static PropositionalizationRule numericAttributeToPropositionMapper(
			TypeBasedDiscretization discretization) {
		switch (discretization.discretization()) {
		case CLUSTERING:
			if (discretization.value() == 2) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_2_CUTOFFS;
			} else if (discretization.value() == 4) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_4_CUTOFFS;
			} else if (discretization.value() == 6) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_6_CUTOFFS;
			} else if (discretization.value() == 8) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_8_CUTOFFS;
			} else if (discretization.value() == 10) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_10_CUTOFFS;
			} else if (discretization.value() == 12) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_12_CUTOFFS;
			} else if (discretization.value() == 14) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_14_CUTOFFS;
			} else if (discretization.value() == 16) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_16_CUTOFFS;
			} else if (discretization.value() == 18) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_18_CUTOFFS;
			} else if (discretization.value() == 20) {
				return ClusteringAttributeToPropositionsMapper.CLUSTERING_20_CUTOFFS;
			}
			return ClusteringAttributeToPropositionsMapper.CLUSTERING_4_CUTOFFS;
		case EQUIDISTANT:
			return DefaultNumericAttributeToPropositionMappers.newEquiDistantMapper(discretization.value());
		case EQUIFREQUENT:
			return DefaultNumericAttributeToPropositionMappers.newEquiFrequentMapper(discretization.value());
		default:
			return ClusteringAttributeToPropositionsMapper.CLUSTERING_4_CUTOFFS;
		}

	}

	private static PropositionalizationRule numericAttributeToPropositionMapper(FixedDiscretization discretization) {
		return DefaultNumericAttributeToPropositionMappers.newFixedBinMapper(newTreeSet(discretization.borders()));
	}

	private static Set<PropositionalizationRule> attributeToPropositionMappers(Discretization discretization,
			Map<String, Discretization> discretizations) {
		PropositionalizationRule globalMapper;

		if (TypeBasedDiscretization.class.isAssignableFrom(discretization.getClass())) {
			globalMapper = numericAttributeToPropositionMapper((TypeBasedDiscretization) discretization);
		} else if (FixedDiscretization.class.isAssignableFrom(discretization.getClass())) {
			globalMapper = numericAttributeToPropositionMapper((FixedDiscretization) discretization);
		} else {
			globalMapper = ClusteringAttributeToPropositionsMapper.CLUSTERING_4_CUTOFFS;
		}

		Map<String, List<PropositionalizationRule>> specificMappers = newHashMap();

		for (Entry<String, Discretization> entry : discretizations.entrySet()) {
			List<PropositionalizationRule> mappers = specificMappers.get(entry.getKey());

			if (mappers == null) {
				specificMappers.put(entry.getKey(), mappers = newArrayList());
			}

			if (TypeBasedDiscretization.class.isAssignableFrom(entry.getValue().getClass())) {
				mappers.add(numericAttributeToPropositionMapper((TypeBasedDiscretization) entry.getValue()));
			} else if (FixedDiscretization.class.isAssignableFrom(entry.getValue().getClass())) {
				mappers.add(numericAttributeToPropositionMapper((FixedDiscretization) entry.getValue()));
			} else if (DateBasedDiscretization.class.isAssignableFrom(entry.getValue().getClass())) {
				mappers.addAll(dateAttributeToPropositionMappers((DateBasedDiscretization) entry.getValue()));
			}
		}

		return ImmutableSet.of(DefaultAttributeToPropositionMappers.newSpecificThenGlobalMapper(
				ImmutableList.of(LegacyAttributesToPropositionsMapper.CATEGORIC_EQUALiTY, globalMapper),
				specificMappers));
	}

	private static class DataTablePropositionalContextLoader implements PropositionalContextLoader {

		@Override
		public Optional<PropositionalContext> load(ProcessedDataTableIdentifier identifier,
				SchemeMetaInfo schemeMetaInfo, WorksheetMetaInfo worksheetMetaInfo) {
			Optional<Path> path = persistentStoragePathGenerator().path(identifier);

			if (!path.isPresent()) {
				return Optional.empty();
			}

			PropositionalContextFromTableBuilder f = new PropositionalContextFromTableBuilder();

			if (DataTableWorksheetMetaInfo.class.isAssignableFrom(worksheetMetaInfo.getClass())) {
				DataTableWorksheetMetaInfo info = (DataTableWorksheetMetaInfo) worksheetMetaInfo;

				f.mappers(attributeToPropositionMappers(info.discretization(), info.discretizations()));
			}

			return Optional.of(f.apply(XarfImport.xarfImport(path.get().toString()).get()));
		}

	}

	private static class SequenceDataTablePropositionalContextLoader implements PropositionalContextLoader {

		@Override
		public Optional<PropositionalContext> load(ProcessedDataTableIdentifier identifier,
				SchemeMetaInfo schemeMetaInfo, WorksheetMetaInfo worksheetMetaInfo) {
			Optional<Path> path = persistentStoragePathGenerator().path(identifier);

			if (!path.isPresent()) {
				return Optional.empty();
			}

			if (SequentialDataTableWorksheetMetaInfo.class.isAssignableFrom(worksheetMetaInfo.getClass())) {
				SequentialDataTableWorksheetMetaInfo info = (SequentialDataTableWorksheetMetaInfo) worksheetMetaInfo;

				if (info.generateSingleSequence()) {
					SequentialDataTableAsSingleSequenceFromTableFactory f = new SequentialDataTableAsSingleSequenceFromTableFactory();

					f.mappers(attributeToPropositionMappers(info.discretization(), info.discretizations()));
					f.groupingAttributeName(info.idAttribute());
					f.distanceAttributeName(info.distanceAttribute());

					return Optional.of(f.build(XarfImport.xarfImport(path.get().toString()).get()));
				} else {
					SequentialPropositionalContextFromTableFactory f = new SequentialPropositionalContextFromTableFactory();

					f.mappers(attributeToPropositionMappers(info.discretization(), info.discretizations()));
					f.groupingAttributeName(info.idAttribute());
					f.distanceAttributeName(info.distanceAttribute());

					return Optional.of(f.build(XarfImport.xarfImport(path.get().toString()).get()));
				}
			}

			return Optional.empty();
		}

	}

	private static class SequenceDataTableLoader implements PropositionalContextLoader {

		@Override
		public Optional<PropositionalContext> load(ProcessedDataTableIdentifier identifier,
				SchemeMetaInfo schemeMetaInfo, WorksheetMetaInfo worksheetMetaInfo) {
			Optional<Path> path = persistentStoragePathGenerator().path(identifier);

			if (!path.isPresent()) {
				return Optional.empty();
			}

			SingleCategoricSequenceLoader loader = new SingleCategoricSequenceLoader();

			return Optional.of(loader.build(XarfImport.xarfImport(path.get().toString()).get()));
		}

	}

	private static class TransactionPropositionalContextLoader implements PropositionalContextLoader {

		@Override
		public Optional<PropositionalContext> load(ProcessedDataTableIdentifier identifier,
				SchemeMetaInfo schemeMetaInfo, WorksheetMetaInfo worksheetMetaInfo) {
			Optional<Path> path = persistentStoragePathGenerator().path(identifier);

			if (!path.isPresent()) {
				return Optional.empty();
			}

			DefaultPropositionalContext propositionalContext = PropositionalLogicFromTransactionFileFactory
					.build(path.get().toAbsolutePath().toString(), ",");

			return Optional.of(propositionalContext);
		}

	}

	private static class TextFileContextLoader implements PropositionalContextLoader {

		@Override
		public Optional<PropositionalContext> load(ProcessedDataTableIdentifier identifier,
				SchemeMetaInfo schemeMetaInfo, WorksheetMetaInfo worksheetMetaInfo) {
			if (!TextSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				return Optional.empty();
			}

			Optional<Path> path = persistentStoragePathGenerator().path(identifier);

			if (!path.isPresent()) {
				return Optional.empty();
			}

			String[] files = new String[] { path.get().toAbsolutePath().toString() };

			try {
				if (files.length == 1) {
					return Optional.of(loadSingleFile(identifier, (TextSchemeMetaInfo) schemeMetaInfo, files[0]));
				} else {
					return Optional.of(loadMultiFile(identifier, (TextSchemeMetaInfo) schemeMetaInfo, files));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return Optional.empty();
		}

		private PropositionalContext loadSingleFile(ProcessedDataTableIdentifier identifier,
				TextSchemeMetaInfo schemeMetaInfo, String dataFile)
				throws FileNotFoundException, IOException, InvalidFormatException {

			BufferedReader reader = new BufferedReader(new FileReader(dataFile));
			List<String> sentences = reader.lines().collect(Collectors.toList());
			reader.close();

			if (schemeMetaInfo.recognizeSentences()) {
				StringBuilder builder = new StringBuilder();
				sentences.forEach(s -> {
					builder.append(s);
					builder.append(" ");
				});
				sentences = OpenNLPSentenceExtractor.newExtractorByLanguange(TextLanguage.EN)
						.getSentences(builder.toString());
			}

			if (schemeMetaInfo.alphabeticalOnly()) {
				sentences = new SentenceOperator.AlphabetOnlyKeeper().operate(sentences);
			}

			List<List<String>> tokens = sentences.stream().map(s -> newArrayList(s.split(" ")))
					.collect(Collectors.toList());

			if (schemeMetaInfo.filterPOS()) {
				tokens = new POSFilter().perform(tokens);
			}
			if (schemeMetaInfo.stemTokens()) {
				tokens = new TokenListStemmer().perform(tokens);
			}

			tokens = new EmptyTokenListFilter().perform(tokens);

			if (TextAsTransactionsSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				TextAsTransactionsSchemeMetaInfo cSchemeMetaInfo = (TextAsTransactionsSchemeMetaInfo) schemeMetaInfo;

				// TODO Move up! Add another filter to make set!
				if (!cSchemeMetaInfo.dFFilterType().equals(DFFilterType.NONE)) {
					tokens = new DFFilter(cSchemeMetaInfo.dFFilterType(), cSchemeMetaInfo.dFFilterValue())
							.perform(tokens);
				}

				return PropositionalLogicFromTokenListsFactory.build(identifier.dataTableCaption(), tokens);
			} else if (TextAsBowTfIdfSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				TextAsBowTfIdfSchemeMetaInfo cSchemeMetaInfo = (TextAsBowTfIdfSchemeMetaInfo) schemeMetaInfo;

				List<String> words = newArrayList();
				List<List<Double>> bowVectors = newArrayList();

				TFIDF c = new TFIDF(tokens);
				words = c.getTokens();
				bowVectors = c.getValues();

				File file = new File("tmp.txt");
				BufferedWriter writer = new BufferedWriter(new FileWriter(file));
				StringBuilder builder = new StringBuilder();
				for (List<Double> bowVector : bowVectors) {
					builder.setLength(0);
					for (Double value : bowVector) {
						builder.append(value);
						builder.append(";");
					}
					if (builder.length() != 0) {
						writer.write(builder.substring(0, builder.length() - 1));
						writer.write("\n");
					}
				}
				writer.close();

				File file1 = new File("tmp1.txt");
				writer = new BufferedWriter(new FileWriter(file1));
				for (String word : words) {
					writer.write(word + ";numeric;" + word + "\n");
				}
				writer.close();

				try {
					PropositionalLogicFromTableManualBinningFactory.NUMBER_OF_BINS = cSchemeMetaInfo.numberOfBins();
					PropositionalContext propositionLogic = PropositionalLogicFromTableManualBinningFactory
							.build(new DataTableFromCSVFileBuilder().setDataCSVFilename(file.getAbsolutePath())
									.setAttributeMetadataCSVFilename(file1.getAbsolutePath()).build());
					file.delete();
					file1.delete();
					return propositionLogic;
				} catch (DataFormatException e) {
					e.printStackTrace();
				}
			}
			return PropositionalLogicFromTokenListsFactory.build(identifier.dataTableCaption(), tokens);
		}

		private PropositionalContext loadMultiFile(ProcessedDataTableIdentifier identifier,
				TextSchemeMetaInfo schemeMetaInfo, String[] files)
				throws FileNotFoundException, IOException, InvalidFormatException {
			// List<String> sentences = newArrayList();
			//
			// for (String file : files) {
			// String dataFile = Settings.Instance.getProperty(UPLOAD_DIR) +
			// name + "/" +
			// file;
			// BufferedReader reader = new BufferedReader(new
			// FileReader(dataFile));
			// StringBuilder builder = new StringBuilder();
			// reader.lines().forEach(line -> builder.append(line + " "));
			// reader.close();
			// sentences.add(builder.substring(0, builder.length() - 1));
			// }
			//
			// if (!options.containsKey("useAlphabetical") ||
			// options.get("useAlphabetical").equals("true")) {
			// sentences = new
			// SentenceOperator.AlphabetOnlyKeeper().operate(sentences);
			// }
			//
			// List<List<String>> tokens = sentences.stream().map(s ->
			// newArrayList(s.split(" ")))
			// .collect(Collectors.toList());
			//
			// if (!options.containsKey("usePOSFilter") ||
			// options.get("usePOSFilter").equals("true")) {
			// tokens = new POSFilter().perform(tokens);
			// }
			// if (!options.containsKey("useTokenStemmer") ||
			// options.get("useTokenStemmer").equals("true")) {
			// tokens = new TokenListStemmer().perform(tokens);
			// }
			// if (!options.containsKey("useDFFilter") ||
			// options.get("useDFFilter").equals("true")) {
			// tokens = new DFFilter(options).perform(tokens);
			// }
			//
			// tokens = new EmptyTokenListFilter().perform(tokens);
			//
			// tokens.stream().forEach(tok -> System.out.println(tok));
			//
			// return PropositionalLogicFromTokenListsFactory.build(name,
			// tokens);
			return null;
		}

	}

	// Suppress default constructor for non-instantiability
	private PropositionalContextLoaders() {
		throw new AssertionError();
	}

}
