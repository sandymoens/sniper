/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.dao.entity.RepositoryEntity;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Repositories {

	public static abstract class HibernateRepository<S, T extends RepositoryEntity<S>> implements Repository<S, T> {

		@Autowired
		protected SessionFactory sessionFactory;

		private Class<T> clazz;

		public HibernateRepository(Class<T> clazz) {
			this.clazz = clazz;
		}

		@Override
		public List<T> all() {
			Session session = this.sessionFactory.openSession();

			CriteriaQuery<T> query = session.getCriteriaBuilder().createQuery(this.clazz);

			query.from(this.clazz);

			List<T> results = session.createQuery(query).list();

			session.close();

			return results;
		}

		@Override
		public List<T> allInjected() {
			List<JoinTuple> joinTuples = injectionJoinTuples();

			if (joinTuples.isEmpty()) {
				return all();
			}

			Session session = this.sessionFactory.openSession();

			CriteriaBuilder builder = session.getCriteriaBuilder();

			CriteriaQuery<T> query = builder.createQuery(this.clazz).distinct(true);

			Root<T> root = query.from(this.clazz);

			for (JoinTuple joinTuple : joinTuples) {
				root.fetch(joinTuple.attributeName, joinTuple.joinType);
			}

			List<T> results = session.createQuery(query).list();

			session.close();

			return results;
		}

		@Override
		public Optional<T> get(S id) {
			Session session = this.sessionFactory.openSession();

			CriteriaBuilder builder = session.getCriteriaBuilder();

			CriteriaQuery<T> query = builder.createQuery(this.clazz);

			Root<T> root = query.from(this.clazz);

			query.where(builder.equal(root.get("id"), id));

			List<T> results = session.createQuery(query).list();

			session.close();

			if (!results.isEmpty()) {
				return Optional.of(results.get(0));
			}

			return Optional.empty();
		}

		@Override
		public List<T> get(List<S> ids) {
			if (ids.isEmpty()) {
				return ImmutableList.of();
			}

			Session session = this.sessionFactory.openSession();

			CriteriaBuilder builder = session.getCriteriaBuilder();

			CriteriaQuery<T> query = builder.createQuery(this.clazz);

			Root<T> root = query.from(this.clazz);

			query.where(root.get("id").in(ids));

			List<T> results = session.createQuery(query).list();

			session.close();

			return results;
		}

		protected List<JoinTuple> injectionJoinTuples() {
			return ImmutableList.of();
		}

		@Override
		public Optional<T> getInjected(S id) {
			List<JoinTuple> joinTuples = injectionJoinTuples();

			if (joinTuples.isEmpty()) {
				return get(id);
			}

			Session session = this.sessionFactory.openSession();

			CriteriaBuilder builder = session.getCriteriaBuilder();

			CriteriaQuery<T> query = builder.createQuery(this.clazz).distinct(true);

			Root<T> root = query.from(this.clazz);

			for (JoinTuple joinTuple : joinTuples) {
				root.fetch(joinTuple.attributeName, joinTuple.joinType);
			}

			query.where(builder.equal(root.get("id"), id));

			List<T> results = session.createQuery(query).list();

			session.close();

			if (!results.isEmpty()) {
				return Optional.of(results.get(0));
			}

			return Optional.empty();
		}

		@Override
		public List<T> getInjected(List<S> ids) {
			if (ids.isEmpty()) {
				return ImmutableList.of();
			}

			List<JoinTuple> joinTuples = injectionJoinTuples();

			if (joinTuples.isEmpty()) {
				return get(ids);
			}

			Session session = this.sessionFactory.openSession();

			CriteriaBuilder builder = session.getCriteriaBuilder();

			CriteriaQuery<T> query = builder.createQuery(this.clazz).distinct(true);

			Root<T> root = query.from(this.clazz);

			for (JoinTuple joinTuple : joinTuples) {
				root.fetch(joinTuple.attributeName, joinTuple.joinType);
			}

			query.select(root).where(root.get("id").in(ids));

			List<T> results = session.createQuery(query).list();

			session.close();

			return results;
		}

		@Override
		public boolean remove(S id) {
			Optional<T> object = this.get(id);

			if (!object.isPresent()) {
				return false;
			}

			Session session = this.sessionFactory.openSession();

			Transaction tx = session.beginTransaction();

			session.remove(object.get());
			session.flush();

			tx.commit();

			session.close();

			return true;
		}

		@Override
		public boolean add(T object) {
			if (this.contains(object.getId())) {
				return false;
			}

			Session session = this.sessionFactory.openSession();

			Transaction tx = session.beginTransaction();

			session.save(object);
			session.flush();

			tx.commit();

			session.close();

			return true;
		}

		@Override
		public boolean update(T object) {
			Session session = this.sessionFactory.openSession();

			Transaction tx = session.beginTransaction();

			session.update(object);
			session.flush();

			tx.commit();

			session.close();

			return true;
		}

	}

	// Suppress default constructor for non-instantiability
	private Repositories() {
		throw new AssertionError();
	}

}
