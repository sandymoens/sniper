/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.repository.Repositories.HibernateRepository;

/**
 * 
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class UserRepositories {

	@Repository
	public static class HibernateUserRepository extends HibernateRepository<Integer, User> implements UserRepository {

		public HibernateUserRepository() {
			super(User.class);
		}

		@Override
		protected List<JoinTuple> injectionJoinTuples() {
			return ImmutableList.of(new JoinTuple("roles", JoinType.LEFT), new JoinTuple("dataTables", JoinType.LEFT),
					new JoinTuple("schemes", JoinType.LEFT), new JoinTuple("workspaces", JoinType.LEFT));
		}

		@Override
		public Optional<User> findByUsername(String username) {
			Session session = this.sessionFactory.openSession();

			CriteriaBuilder builder = session.getCriteriaBuilder();

			CriteriaQuery<User> query = builder.createQuery(User.class);

			Root<User> schemeRoot = query.from(User.class);

			query.where(builder.equal(schemeRoot.get("username"), username));

			List<User> users = session.createQuery(query).list();

			session.close();

			if (!users.isEmpty()) {
				return Optional.of(users.get(0));
			}

			return Optional.empty();
		}

		@Override
		public Optional<User> getByUsernameInjected(String username) {
			Session session = this.sessionFactory.openSession();

			CriteriaBuilder builder = session.getCriteriaBuilder();

			CriteriaQuery<User> query = builder.createQuery(User.class);

			Root<User> root = query.from(User.class);

			for (JoinTuple joinTuple : injectionJoinTuples()) {
				root.fetch(joinTuple.attributeName, joinTuple.joinType);
			}

			query.where(builder.equal(root.get("username"), username));

			List<User> users = session.createQuery(query).list();

			session.close();

			if (!users.isEmpty()) {
				return Optional.of(users.get(0));
			}

			return Optional.empty();
		}

	}

	// Suppress default constructor for non-instantiability
	private UserRepositories() {
		throw new AssertionError();
	}

}
