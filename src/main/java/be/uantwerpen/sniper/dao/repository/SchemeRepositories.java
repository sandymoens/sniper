/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.repository;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.springframework.stereotype.Repository;

import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.dao.repository.Repositories.HibernateRepository;

/**
 * 
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SchemeRepositories {

	@Repository
	public static class HibernateSchemeRepository extends HibernateRepository<String, Scheme>
			implements SchemeRepository {

		public HibernateSchemeRepository() {
			super(Scheme.class);
		}

		@Override
		protected List<JoinTuple> injectionJoinTuples() {
			return ImmutableList.of(new JoinTuple("worksheets", JoinType.LEFT));
		}

	}

	// Suppress default constructor for non-instantiability
	private SchemeRepositories() {
		throw new AssertionError();
	}

}
