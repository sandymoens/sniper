/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import be.uantwerpen.sniper.dao.entity.Role;
import be.uantwerpen.sniper.dao.repository.Repositories.HibernateRepository;

/**
 * 
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class RoleRepositories {

	@Repository
	public static class HibernateRoleRepository extends HibernateRepository<Integer, Role> implements RoleRepository {

		public HibernateRoleRepository() {
			super(Role.class);
		}

		@Override
		public Optional<Role> findByName(String name) {
			Session session = this.sessionFactory.openSession();

			CriteriaBuilder builder = session.getCriteriaBuilder();

			CriteriaQuery<Role> query = builder.createQuery(Role.class);

			Root<Role> schemeRoot = query.from(Role.class);

			query.where(builder.equal(schemeRoot.get("name"), name));

			List<Role> roles = session.createQuery(query).list();

			session.close();

			if (!roles.isEmpty()) {
				return Optional.of(roles.get(0));
			}

			return Optional.empty();
		}

	}

	// Suppress default constructor for non-instantiability
	private RoleRepositories() {
		throw new AssertionError();
	}

}
