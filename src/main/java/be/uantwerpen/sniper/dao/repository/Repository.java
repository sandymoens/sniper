/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.repository;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public interface Repository<S, T> {

	public List<T> all();

	public List<T> allInjected();

	public Optional<T> get(S id);

	public List<T> get(List<S> ids);

	public Optional<T> getInjected(S id);

	public List<T> getInjected(List<S> ids);

	public boolean add(T object);

	public boolean update(T object);

	public boolean remove(S id);

	public default boolean remove(List<S> ids) {
		boolean removed = false;

		Iterator<S> it = ids.iterator();

		while (it.hasNext()) {
			removed |= remove(it.next());
		}

		return removed;
	}

	public default boolean contains(S id) {
		return get(id).isPresent();
	}

}
