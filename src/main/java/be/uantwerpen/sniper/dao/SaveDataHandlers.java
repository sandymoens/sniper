/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import static com.google.common.collect.Maps.newHashMap;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SaveDataHandlers {

	private static Map<Class<?>, SaveDataHandler<?>> handlers;

	public static <T extends SaveDataParameter<?>> void register(Class<T> clazz, SaveDataHandler<T> handler) {
		handlers.put(clazz, handler);
	}

	static {
		handlers = newHashMap();

		// register(SaveDataFromFileParameter.class, new
		// SaveDataHandlersUpload.SaveDataFromFileHandler());
		// register(SaveDataFromSQLConnectionParameter.class, new
		// SaveDataHandlersUpload.SaveDataFromSQLConnectionHandler());
		// register(SaveDataSchemeDataTableParameter.class, new
		// SaveDataHandlersScheme.SaveDataSchemeDataTableHandler());
		// register(SaveDataSchemeMetaInfoParameter.class, new
		// SaveDataHandlersScheme.SaveDataSchemeMetaInfoHandler());
		// register(SaveWorksheetMetaInfoParameter.class, new
		// SaveWorksheetMetaInfoHandler());

		Reflections reflections = new Reflections(
				new ConfigurationBuilder().addUrls(ClasspathHelper.forPackage("be.uantwerpen.sniper.dao"))
						.setScanners(new MethodAnnotationsScanner()));

		reflections.getMethodsAnnotatedWith(SaveDataHandlerAnn.class).stream().forEach(m -> {
			try {
				castAndRegister(m.getAnnotation(SaveDataHandlerAnn.class).saveDataParameterClass(), m.invoke(null));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}

	@SuppressWarnings("unchecked")
	private static <T extends SaveDataParameter<?>> void castAndRegister(Class<T> saveDataParameterClass,
			Object object) {
		register(saveDataParameterClass, (SaveDataHandler<T>) object);
	}

	public static SaveDataHandler<SaveDataParameter<?>> saveDataHandler(Class<?> clazz) {
		@SuppressWarnings("unchecked")
		SaveDataHandler<SaveDataParameter<?>> handler = (SaveDataHandler<SaveDataParameter<?>>) handlers.get(clazz);

		return handler;
	}

	// Suppress default constructor for non-instantiability
	private SaveDataHandlers() {
		throw new AssertionError();
	}

}
