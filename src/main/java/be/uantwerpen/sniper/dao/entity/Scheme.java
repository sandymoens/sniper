package be.uantwerpen.sniper.dao.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import de.unibonn.realkd.common.base.Documented;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@Entity
@Table(name = "Scheme")
public class Scheme implements Documented, RepositoryEntity<String> {

	@Id
	@NotNull
	@Column(name = "schemeId")
	private String id;

	@Column(name = "caption")
	private String caption;

	@Column(name = "description")
	private String description;

	@ManyToOne()
	@JoinColumn(name = "dataTableId")
	private DataTable dataTable;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "scheme")
	private Set<Worksheet> worksheets;

	public Scheme() {
		this(null);
	}

	public Scheme(String id) {
		this(id, null, null, null);
	}

	public Scheme(String id, String caption, String description, DataTable dataTable) {
		this.id = id;
		this.caption = caption;
		this.description = description;
		this.dataTable = dataTable;
	}

	@Override
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String caption() {
		return this.caption;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	@Override
	public String description() {
		return this.description;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DataTable getDataTable() {
		return this.dataTable;
	}

	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
	}

	public Set<Worksheet> getWorksheets() {
		return this.worksheets;
	}

	public void setWorksheet(Set<Worksheet> worksheets) {
		this.worksheets = worksheets;
	}

	@Override
	public String toString() {
		return "Scheme: id=" + this.id + ", caption='" + this.caption + "', description='" + this.description
				+ "', dataTableId=" + this.dataTable.getId();
	}

}