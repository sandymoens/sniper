package be.uantwerpen.sniper.dao.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import de.unibonn.realkd.common.base.Documented;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@Entity
@Table(name = "DataTable")
public class DataTable implements Documented, RepositoryEntity<String> {

	@Id
	@NotNull
	@Column(name = "dataTableId")
	private String id;

	@Column(name = "caption")
	private String caption;

	@Column(name = "description")
	private String description;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "dataTable")
	private Set<Scheme> schemes;

	public DataTable() {
		this(null);
	}

	public DataTable(String id) {
		this(id, null, null);
	}

	public DataTable(String id, String caption, String description) {
		this.id = id;
		this.caption = caption;
		this.description = description;
	}

	@Override
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String caption() {
		return this.caption;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	@Override
	public String description() {
		return this.description;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Scheme> getSchemes() {
		return this.schemes;
	}

	public void setSchemes(Set<Scheme> schemes) {
		this.schemes = schemes;
	}

	@Override
	public String toString() {
		return "DataTable: id=" + this.id + ", caption='" + this.caption + "', description='" + this.description + "'";
	}

}