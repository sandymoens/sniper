package be.uantwerpen.sniper.dao.entity;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import de.unibonn.realkd.common.base.Documented;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@Entity
@Table(name = "Workspace")
public class Workspace implements Documented, RepositoryEntity<String> {

	@Id
	@NotNull
	@Column(name = "workspaceId")
	private String id;

	@Column(name = "caption")
	private String caption;

	@Column(name = "description")
	private String description;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "workspace")
	private Collection<Worksheet> worksheets;

	public Workspace() {
		this(null);
	}

	public Workspace(String id) {
		this(id, null, null, null);
	}

	public Workspace(String id, String caption, String description, Collection<Worksheet> worksheets) {
		this.id = id;
		this.caption = caption;
		this.description = description;
		this.worksheets = worksheets;
	}

	@Override
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String caption() {
		return this.caption;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	@Override
	public String description() {
		return this.description;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Collection<Worksheet> getWorksheets() {
		return this.worksheets;
	}

	public void setWorksheets(Collection<Worksheet> worksheets) {
		this.worksheets = worksheets;
	}

	@Override
	public String toString() {
		return "Workspace: id=" + this.id + ", caption='" + this.caption + "', description='" + this.description
				+ "', worksheetIds="
				+ this.worksheets.stream().map(w -> w.getId()).collect(Collectors.toList()).toString();
	}

}