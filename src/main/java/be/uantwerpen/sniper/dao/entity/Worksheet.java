package be.uantwerpen.sniper.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import de.unibonn.realkd.common.base.Documented;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
@Entity
@Table(name = "Worksheet")
public class Worksheet implements Documented, RepositoryEntity<String> {

	@Id
	@NotNull
	@Column(name = "worksheetId")
	private String id;

	@Column(name = "caption")
	private String caption;

	@Column(name = "description")
	private String description;

	@ManyToOne()
	@JoinColumn(name = "schemeId")
	private Scheme scheme;

	@ManyToOne()
	@JoinColumn(name = "workspaceId")
	private Workspace workspace;

	public Worksheet() {
		this(null);
	}

	public Worksheet(String id) {
		this(id, null, null, null);
	}

	public Worksheet(String id, String caption, String description, Scheme scheme) {
		this.id = id;
		this.caption = caption;
		this.description = description;
		this.scheme = scheme;
	}

	@Override
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String caption() {
		return this.caption;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	@Override
	public String description() {
		return this.description;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Scheme getScheme() {
		return this.scheme;
	}

	public void setScheme(Scheme scheme) {
		this.scheme = scheme;
	}

	public Workspace getWorkspace() {
		return this.workspace;
	}

	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}

	@Override
	public String toString() {
		return "Worksheet: id=" + this.id + ", caption='" + this.caption + "', description='" + this.description
				+ "', schemeId=" + this.scheme.getId();
	}

}