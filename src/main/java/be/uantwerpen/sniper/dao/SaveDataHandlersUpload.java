/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import static be.uantwerpen.io.filesupport.DataProviders.newSQLTableRowProvider;
import static be.uantwerpen.sniper.dao.PersistentStorageHandlers.fileBasedPersistentStorageHandler;

import java.io.File;
import java.sql.SQLException;

import org.springframework.web.multipart.MultipartFile;

import be.uantwerpen.io.filesupport.DataProvider;
import be.uantwerpen.io.filesupport.DataProviders;
import be.uantwerpen.io.filesupport.DataTypeHandler;
import be.uantwerpen.io.filesupport.DataTypeHandlers;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.RawDataTableIdentifier;
import be.uantwerpen.sniper.dao.SaveDataParameters.SaveDataFromFileParameter;
import be.uantwerpen.sniper.dao.SaveDataParameters.SaveDataFromSQLConnectionParameter;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SaveDataHandlersUpload {

	@SaveDataHandlerAnn(saveDataParameterClass = SaveDataFromFileParameter.class)
	public static SaveDataHandler<SaveDataFromFileParameter> saveDataFromFileHandler() {
		return new SaveDataFromFileHandler();
	}

	@SaveDataHandlerAnn(saveDataParameterClass = SaveDataFromSQLConnectionParameter.class)
	public static SaveDataHandler<SaveDataFromSQLConnectionParameter> saveDataFromSQLConnectionHandler() {
		return new SaveDataFromSQLConnectionHandler();
	}

	private static class SaveDataFromFileHandler implements SaveDataHandler<SaveDataFromFileParameter> {

		@Override
		public boolean save(SaveDataFromFileParameter parameter) {
			DataProvider dp = null;
			DataTypeHandler<File> dth = null;

			MultipartFile file = parameter.multiPartFile();

			if (file.getOriginalFilename().endsWith(".xlsx")) {
				dp = DataProviders.newExcelFileHandler(file);
				dth = DataTypeHandlers.newDataTableTypeHandler(true);
			} else if (file.getOriginalFilename().endsWith(".dat")) {
				dp = DataProviders.newFlatFileHandlerWithDelimiter(file, " ");
				dth = DataTypeHandlers.newDataTableTypeHandler(true);
			} else if (file.getOriginalFilename().endsWith(".arff")) {
				dp = DataProviders.newFlatFileHandlerWithDelimiter(file, ",");
				dth = DataTypeHandlers.newArffTypeHandler();
			} else {
				dp = DataProviders.newFlatFileHandlerWithDelimiter(file, ";");
				dth = DataTypeHandlers.newDataTableTypeHandler(true);
			}

			if (dp == null || dth == null) {
				return false;
			}

			try {
				dth.handleDataFile(new RawDataTableIdentifier(parameter.dataTable()), dp,
						fileBasedPersistentStorageHandler());

				return true;
			} catch (Exception e) {
				e.printStackTrace();

				return false;
			}

		}

	}

	private static class SaveDataFromSQLConnectionHandler
			implements SaveDataHandler<SaveDataFromSQLConnectionParameter> {

		@Override
		public boolean save(SaveDataFromSQLConnectionParameter parameter) {
			DataProvider dp;
			try {
				dp = newSQLTableRowProvider(parameter.hostName(), parameter.database(), parameter.table(),
						parameter.userName(), parameter.password());
			} catch (ClassNotFoundException | SQLException e1) {
				e1.printStackTrace();

				return false;
			}
			DataTypeHandler<File> dth = DataTypeHandlers.newDataTableTypeHandler(true);

			try {
				dth.handleDataFile(new RawDataTableIdentifier(parameter.dataTable()), dp,
						fileBasedPersistentStorageHandler());

				return true;
			} catch (Exception e) {
				e.printStackTrace();

				return false;
			}
		}

	}

	// Suppress default constructor for non-instantiability
	private SaveDataHandlersUpload() {
		throw new AssertionError();
	}

}
