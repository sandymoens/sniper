/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import be.uantwerpen.sniper.config.Settings;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.ProcessedDataTableIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.RawDataTableIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.SchemeMetaInformationIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.WorksheetIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.WorksheetMetaInfoIdentifier;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PersistentStoragePathGenerators {

	private static PersistentStoragePathGenerator persistentStoragePathGenerator;

	public static PersistentStoragePathGenerator persistentStoragePathGenerator() {
		return persistentStoragePathGenerator != null ? persistentStoragePathGenerator
				: (persistentStoragePathGenerator = new DefaultPersistentStoragePathGenerator());
	}

	private static class DefaultPersistentStoragePathGenerator implements PersistentStoragePathGenerator {

		@Override
		public Optional<Path> directory(PersistentStorageIdentifier identifier) {
			String dataDir = Settings.Instance.getProperty(Settings.DATA_DIR);

			if (RawDataTableIdentifier.class.isAssignableFrom(identifier.getClass())) {
				RawDataTableIdentifier i = (RawDataTableIdentifier) identifier;

				return Optional.of(Paths.get(dataDir, i.dataTableId()));
			} else if (ProcessedDataTableIdentifier.class.isAssignableFrom(identifier.getClass())) {
				ProcessedDataTableIdentifier i = (ProcessedDataTableIdentifier) identifier;

				return Optional.of(Paths.get(dataDir, i.dataTableId()));
			} else if (SchemeMetaInformationIdentifier.class.isAssignableFrom(identifier.getClass())) {
				SchemeMetaInformationIdentifier i = (SchemeMetaInformationIdentifier) identifier;

				return Optional.of(Paths.get(dataDir, i.dataTableId()));
			} else if (WorksheetIdentifier.class.isAssignableFrom(identifier.getClass())) {
				WorksheetIdentifier i = (WorksheetIdentifier) identifier;

				return Optional.of(Paths.get(dataDir, "repo", i.worksheetId()));
			} else if (WorksheetMetaInfoIdentifier.class.isAssignableFrom(identifier.getClass())) {
				WorksheetMetaInfoIdentifier i = (WorksheetMetaInfoIdentifier) identifier;

				return Optional.of(Paths.get(dataDir, "repo", i.worksheetId()));
			}

			return Optional.empty();
		}

		@Override
		public Optional<Path> path(PersistentStorageIdentifier identifier) {
			String dataDir = Settings.Instance.getProperty(Settings.DATA_DIR);

			if (RawDataTableIdentifier.class.isAssignableFrom(identifier.getClass())) {
				RawDataTableIdentifier i = (RawDataTableIdentifier) identifier;

				return Optional.of(Paths.get(dataDir, i.dataTableId(), "d_" + i.dataTableId()));
			} else if (ProcessedDataTableIdentifier.class.isAssignableFrom(identifier.getClass())) {
				ProcessedDataTableIdentifier i = (ProcessedDataTableIdentifier) identifier;

				return Optional.of(Paths.get(dataDir, i.dataTableId(), "d_" + i.schemeId()));
			} else if (SchemeMetaInformationIdentifier.class.isAssignableFrom(identifier.getClass())) {
				SchemeMetaInformationIdentifier i = (SchemeMetaInformationIdentifier) identifier;

				return Optional.of(Paths.get(dataDir, i.dataTableId(), "m_" + i.schemeId()));
			} else if (WorksheetMetaInfoIdentifier.class.isAssignableFrom(identifier.getClass())) {
				WorksheetMetaInfoIdentifier i = (WorksheetMetaInfoIdentifier) identifier;

				return Optional.of(Paths.get(dataDir, "repo", i.worksheetId(), "mInfo.json"));
			}

			return Optional.empty();
		}

	}

}
