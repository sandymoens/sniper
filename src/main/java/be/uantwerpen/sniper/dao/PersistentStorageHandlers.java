/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import static be.uantwerpen.sniper.dao.PersistentStoragePathGenerators.persistentStoragePathGenerator;
import static java.nio.file.Files.createDirectories;
import static org.apache.commons.io.FileUtils.deleteDirectory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidParameterException;
import java.util.Iterator;
import java.util.Optional;

import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.RawDataTableIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.WorksheetIdentifier;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PersistentStorageHandlers {

	private static PersistentStorageHandler<File> fileBasedPersistentStorageHandler;

	public static PersistentStorageHandler<File> fileBasedPersistentStorageHandler() {
		return fileBasedPersistentStorageHandler != null ? fileBasedPersistentStorageHandler
				: (fileBasedPersistentStorageHandler = new FileBasedPersistentStorageHandler());
	}

	private static class FileBasedPersistentStorageHandler implements PersistentStorageHandler<File> {

		@Override
		public boolean store(PersistentStorageIdentifier identifier, Iterator<String> data) {
			Optional<Path> path = persistentStoragePathGenerator().path(identifier);

			if (!path.isPresent()) {
				throw new InvalidParameterException("Invalid identifier type " + identifier.getClass());
			}

			if (Files.exists(path.get())) {
				return false;
			}

			try {
				createDirectories(persistentStoragePathGenerator().directory(identifier).get());

				BufferedWriter writer = new BufferedWriter(new FileWriter(path.get().toFile()));

				while (data.hasNext()) {
					writer.write(data.next());
					writer.write("\n");
				}

				writer.close();

				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}

		@Override
		public Optional<File> get(PersistentStorageIdentifier identifier) {
			Optional<Path> path = persistentStoragePathGenerator().path(identifier);

			if (!path.isPresent()) {
				throw new InvalidParameterException("Invalid identifier type " + identifier.getClass());
			}

			return Optional.of(path.get().toFile());
		}

		@Override
		public boolean exists(PersistentStorageIdentifier identifier) {
			Optional<Path> path = persistentStoragePathGenerator().path(identifier);
			if (!path.isPresent()) {
				throw new InvalidParameterException("Invalid identifier type " + identifier.getClass());
			}
			System.out.println(path.get().toAbsolutePath().toString());

			return Files.exists(path.get());
		}

		@Override
		public boolean delete(PersistentStorageIdentifier identifier) {
			if (RawDataTableIdentifier.class.isAssignableFrom(identifier.getClass())
					|| WorksheetIdentifier.class.isAssignableFrom(identifier.getClass())) {
				Optional<Path> path = persistentStoragePathGenerator().directory(identifier);

				if (!path.isPresent()) {
					return true;
				}

				try {
					deleteDirectory(path.get().toFile());
				} catch (IOException e) {
					e.printStackTrace();
				}

				return !Files.exists(path.get());
			}

			Optional<Path> path = persistentStoragePathGenerator().path(identifier);

			if (!path.isPresent()) {
				return true;
			}

			try {
				Files.delete(path.get());
			} catch (IOException e) {
				e.printStackTrace();
			}

			return !Files.exists(path.get());
		}

	}

	// Suppress default constructor for non-instantiability
	private PersistentStorageHandlers() {
		throw new AssertionError();
	}

}
