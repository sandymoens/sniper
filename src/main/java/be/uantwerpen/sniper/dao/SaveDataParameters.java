/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import org.springframework.web.multipart.MultipartFile;

import be.uantwerpen.sniper.dao.entity.DataTable;
import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.dao.entity.Worksheet;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SaveDataParameters {

	public static class SaveDataFromFileParameter implements SaveDataParameter<DataTable> {

		private DataTable dataTable;

		private MultipartFile multiPartFile;

		public SaveDataFromFileParameter(DataTable dataTable, MultipartFile multiPartFile) {
			this.dataTable = dataTable;
			this.multiPartFile = multiPartFile;
		}

		public DataTable dataTable() {
			return this.dataTable;
		}

		public MultipartFile multiPartFile() {
			return this.multiPartFile;
		}

	}

	public static class SaveDataFromSQLConnectionParameter implements SaveDataParameter<DataTable> {

		private DataTable dataTable;

		private String hostName;
		private String database;
		private String table;
		private String userName;
		private String password;

		public SaveDataFromSQLConnectionParameter(DataTable dataTable, String hostName, String database, String table,
				String userName, String password) {
			this.dataTable = dataTable;
			this.hostName = hostName;
			this.database = database;
			this.table = table;
			this.userName = userName;
			this.password = password;
		}

		public DataTable dataTable() {
			return this.dataTable;
		}

		public String hostName() {
			return this.hostName;
		}

		public String database() {
			return this.database;
		}

		public String table() {
			return this.table;
		}

		public String userName() {
			return this.userName;
		}

		public String password() {
			return this.password;
		}

	}

	public static class SaveDataSchemeDataTableParameter implements SaveDataParameter<Scheme> {

		private DataTable dataTable;
		private Scheme scheme;

		private SchemeMetaInfo schemeMetaInfo;

		public SaveDataSchemeDataTableParameter(DataTable dataTable, Scheme scheme, SchemeMetaInfo schemeMetaInfo) {
			this.dataTable = dataTable;
			this.scheme = scheme;
			this.schemeMetaInfo = schemeMetaInfo;
		}

		public DataTable dataTable() {
			return this.dataTable;
		}

		public Scheme scheme() {
			return this.scheme;
		}

		public SchemeMetaInfo schemeMetaInfo() {
			return this.schemeMetaInfo;
		}

	}

	public static class SaveDataSchemeMetaInfoParameter implements SaveDataParameter<Scheme> {

		private DataTable dataTable;
		private Scheme scheme;

		private SchemeMetaInfo schemeMetaInfo;

		public SaveDataSchemeMetaInfoParameter(DataTable dataTable, Scheme scheme, SchemeMetaInfo schemeMetaInfo) {
			this.dataTable = dataTable;
			this.scheme = scheme;
			this.schemeMetaInfo = schemeMetaInfo;
		}

		public DataTable dataTable() {
			return this.dataTable;
		}

		public Scheme scheme() {
			return this.scheme;
		}

		public SchemeMetaInfo schemeMetaInfo() {
			return this.schemeMetaInfo;
		}

	}

	public static class SaveWorksheetMetaInfoParameter implements SaveDataParameter<Worksheet> {

		private DataTable dataTable;
		private Worksheet worksheet;

		private WorksheetMetaInfo worksheetMetaInfo;

		public SaveWorksheetMetaInfoParameter(Worksheet worksheet, WorksheetMetaInfo worksheetMetaInfo) {
			this.worksheet = worksheet;
			this.worksheetMetaInfo = worksheetMetaInfo;
		}

		public DataTable dataTable() {
			return this.dataTable;
		}

		public Worksheet worksheet() {
			return this.worksheet;
		}

		public WorksheetMetaInfo worksheetMetaInfo() {
			return this.worksheetMetaInfo;
		}

	}

	// Suppress default constructor for non-instantiability
	private SaveDataParameters() {
		throw new AssertionError();
	}

}
