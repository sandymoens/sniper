/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import static be.uantwerpen.sniper.dao.PersistentStorageHandlers.fileBasedPersistentStorageHandler;
import static be.uantwerpen.sniper.dao.PropositionalContextLoaders.propositionalContextLoader;
import static be.uantwerpen.sniper.dao.SaveDataHandlers.saveDataHandler;
import static com.google.common.collect.Lists.newArrayList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.ProcessedDataTableIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.RawDataTableIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.SchemeMetaInformationIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.WorksheetIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.WorksheetMetaInfoIdentifier;
import be.uantwerpen.sniper.dao.entity.DataTable;
import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.dao.entity.Worksheet;
import de.unibonn.realkd.data.propositions.PropositionalContext;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class DataAccessHandlers {

	private static be.uantwerpen.sniper.dao.DataTableAccessHandler<PropositionalContext> dataTableAccessHandler;

	private static be.uantwerpen.sniper.dao.SchemeDataTableAccessHandler<PropositionalContext> schemeDataTableAccessHandler;

	private static be.uantwerpen.sniper.dao.SchemeAccessHandler<SchemeMetaInfo> schemeMetaInfoAccessHandler;

	private static be.uantwerpen.sniper.dao.WorksheetAccessHandler<Void> worksheetAccessHandler;

	private static be.uantwerpen.sniper.dao.WorksheetAccessHandler<PropositionalContext> worksheetPropositionalContextAccessHandler;

	private static be.uantwerpen.sniper.dao.WorksheetAccessHandler<WorksheetMetaInfo> worksheetMetaInfoAccessHandler;

	public static be.uantwerpen.sniper.dao.DataTableAccessHandler<PropositionalContext> dataTableAccessHandler() {
		return dataTableAccessHandler != null ? dataTableAccessHandler
				: (dataTableAccessHandler = new DataTableAccessHandler());
	}

	public static be.uantwerpen.sniper.dao.SchemeDataTableAccessHandler<PropositionalContext> schemeDataTableAccessHandler() {
		return schemeDataTableAccessHandler != null ? schemeDataTableAccessHandler
				: (schemeDataTableAccessHandler = new SchemeDataTableAccessHandler());
	}

	public static be.uantwerpen.sniper.dao.SchemeAccessHandler<SchemeMetaInfo> schemeMetaInfoAccessHandler() {
		return schemeMetaInfoAccessHandler != null ? schemeMetaInfoAccessHandler
				: (schemeMetaInfoAccessHandler = new SchemeMetaInfoAccessHandler());
	}

	public static be.uantwerpen.sniper.dao.WorksheetAccessHandler<Void> worksheetAccessHandler() {
		return worksheetAccessHandler != null ? worksheetAccessHandler
				: (worksheetAccessHandler = new WorksheetAccessHandler());
	}

	public static be.uantwerpen.sniper.dao.WorksheetAccessHandler<PropositionalContext> worksheetPropositionalContextAccessHandler() {
		return worksheetPropositionalContextAccessHandler != null ? worksheetPropositionalContextAccessHandler
				: (worksheetPropositionalContextAccessHandler = new WorksheetPropositionalContextAccessHandler());
	}

	public static be.uantwerpen.sniper.dao.WorksheetAccessHandler<WorksheetMetaInfo> worksheetMetaInfoAccessHandler() {
		return worksheetMetaInfoAccessHandler != null ? worksheetMetaInfoAccessHandler
				: (worksheetMetaInfoAccessHandler = new WorksheetMetaInfoAccessHandler());
	}

	private static class DataTableAccessHandler
			implements be.uantwerpen.sniper.dao.DataTableAccessHandler<PropositionalContext> {

		@Override
		public Optional<PropositionalContext> load(DataTable dataTable) {
			return Optional.empty();
		}

		@Override
		public boolean save(SaveDataParameter<DataTable> parameter, DataTable dataTable) {
			return saveDataHandler(parameter.getClass()).save(parameter);
		}

		@Override
		public boolean remove(DataTable dataTable) {
			return fileBasedPersistentStorageHandler().delete(new RawDataTableIdentifier(dataTable));
		}

		@Override
		public Optional<List<String>> rows(DataTable dataTable, int pageSize, int page) {
			Optional<File> file = fileBasedPersistentStorageHandler().get(new RawDataTableIdentifier(dataTable));

			int offSet = pageSize * page;

			try {
				BufferedReader reader = new BufferedReader(new FileReader(file.get()));

				List<String> rows = newArrayList();

				String line;

				int i = 0;

				while ((line = reader.readLine()) != null) {
					if (i >= offSet) {
						rows.add(line);
					}

					i++;

					if (i == offSet + pageSize) {
						break;
					}
				}

				reader.close();

				return Optional.of(rows);
			} catch (IOException e) {
				e.printStackTrace();

				return Optional.empty();
			}
		}

		@Override
		public Optional<Integer> rowCount(DataTable dataTable) {
			Optional<File> file = fileBasedPersistentStorageHandler().get(new RawDataTableIdentifier(dataTable));

			try {
				BufferedReader reader = new BufferedReader(new FileReader(file.get()));

				int i = 0;

				while (reader.readLine() != null) {
					i++;
				}

				reader.close();

				return Optional.of(i);
			} catch (IOException e) {
				e.printStackTrace();

				return Optional.empty();
			}
		}

	}

	private static class SchemeDataTableAccessHandler
			implements be.uantwerpen.sniper.dao.SchemeDataTableAccessHandler<PropositionalContext> {

		@Override
		public Optional<PropositionalContext> load(Scheme scheme) {
			Optional<SchemeMetaInfo> schemeMetaInfoOptional = schemeMetaInfoAccessHandler().load(scheme);

			if (!schemeMetaInfoOptional.isPresent()) {
				return Optional.empty();
			}

			SchemeMetaInfo schemeMetaInfo = schemeMetaInfoOptional.get();

			DataFormatType dataFormatType = null;

			if (SchemeMetaInfos.DataTableSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				dataFormatType = DataFormatType.DATATABLE;
			} else if (SchemeMetaInfos.TransactionsSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				dataFormatType = DataFormatType.TRANSACTIONS;
			} else if (SchemeMetaInfos.TextSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				dataFormatType = DataFormatType.TEXTFILE;
			}

			return propositionalContextLoader(dataFormatType)
					.load(new ProcessedDataTableIdentifier(scheme.getDataTable(), scheme), schemeMetaInfo, null);
		}

		@Override
		public boolean save(SaveDataParameter<Scheme> parameter, Scheme scheme) {
			return saveDataHandler(parameter.getClass()).save(parameter);
		}

		@Override
		public boolean remove(Scheme scheme) {
			return fileBasedPersistentStorageHandler()
					.delete(new ProcessedDataTableIdentifier(scheme.getDataTable(), scheme));
		}

		@Override
		public Optional<List<List<String>>> rows(Scheme scheme, String schemeType, int pageSize, int page) {
			Optional<File> file = fileBasedPersistentStorageHandler()
					.get(new ProcessedDataTableIdentifier(scheme.getDataTable(), scheme));

			int offSet = pageSize * page;

			try {
				BufferedReader reader = new BufferedReader(new FileReader(file.get()));

				List<List<String>> rows = newArrayList();

				String line;

				int i = 0;

				boolean read = !schemeType.equals("dataTable");

				while ((line = reader.readLine()) != null) {
					if (!read) {
						read = schemeType.equals("dataTable") && line.equals("@Data");
						continue;
					}

					if (i >= offSet) {
						rows.add(Lists.newArrayList(line.split(",")));
					}

					if (read) {
						i++;
					}

					if (i == offSet + pageSize) {
						break;
					}
				}

				reader.close();

				return Optional.of(rows);
			} catch (IOException e) {
				e.printStackTrace();

				return Optional.empty();
			}
		}

	}

	private static class SchemeMetaInfoAccessHandler
			implements be.uantwerpen.sniper.dao.SchemeAccessHandler<SchemeMetaInfo> {

		@Override
		public Optional<SchemeMetaInfo> load(Scheme scheme) {
			Optional<File> file = fileBasedPersistentStorageHandler()
					.get(new SchemeMetaInformationIdentifier(scheme.getDataTable(), scheme));

			if (!file.isPresent()) {
				return Optional.empty();
			}

			try {
				return Optional.of(new ObjectMapper().readValue(file.get(), SchemeMetaInfo.class));
			} catch (IOException e) {
				e.printStackTrace();

				return Optional.empty();
			}
		}

		@Override
		public boolean save(SaveDataParameter<Scheme> parameter, Scheme scheme) {
			return saveDataHandler(parameter.getClass()).save(parameter);
		}

		@Override
		public boolean remove(Scheme scheme) {
			return fileBasedPersistentStorageHandler()
					.delete(new SchemeMetaInformationIdentifier(scheme.getDataTable(), scheme));
		}

	}

	private static class WorksheetAccessHandler implements be.uantwerpen.sniper.dao.WorksheetAccessHandler<Void> {

		@Override
		public Optional<Void> load(Worksheet worksheet) {
			return Optional.empty();
		}

		@Override
		public boolean save(SaveDataParameter<Worksheet> parameter, Worksheet worskheet) {
			return false;
		}

		@Override
		public boolean remove(Worksheet worksheet) {
			return fileBasedPersistentStorageHandler().delete(new WorksheetIdentifier(worksheet));
		}

	}

	private static class WorksheetPropositionalContextAccessHandler
			implements be.uantwerpen.sniper.dao.WorksheetAccessHandler<PropositionalContext> {

		@Override
		public Optional<PropositionalContext> load(Worksheet worksheet) {
			Scheme scheme = worksheet.getScheme();

			Optional<SchemeMetaInfo> schemeMetaInfoOptional = schemeMetaInfoAccessHandler().load(scheme);

			if (!schemeMetaInfoOptional.isPresent()) {
				return Optional.empty();
			}

			SchemeMetaInfo schemeMetaInfo = schemeMetaInfoOptional.get();

			DataFormatType dataFormatType = null;

			WorksheetMetaInfo worksheetMetaInfo = null;

			if (SchemeMetaInfos.DataTableSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {

				Optional<WorksheetMetaInfo> worksheetMetaInfoOptional = worksheetMetaInfoAccessHandler()
						.load(worksheet);

				if (!schemeMetaInfoOptional.isPresent()) {
					return Optional.empty();
				}

				worksheetMetaInfo = worksheetMetaInfoOptional.get();

				if (WorksheetMetaInfos.DataTableWorksheetMetaInfo.class
						.isAssignableFrom(worksheetMetaInfo.getClass())) {
					dataFormatType = DataFormatType.DATATABLE;
				} else if (WorksheetMetaInfos.SequentialDataTableWorksheetMetaInfo.class
						.isAssignableFrom(worksheetMetaInfo.getClass())) {
					dataFormatType = DataFormatType.SEQUENCEDATATABLE;
				} else if (WorksheetMetaInfos.SequenceWorksheetMetaInfo.class
						.isAssignableFrom(worksheetMetaInfo.getClass())) {
					dataFormatType = DataFormatType.SEQUENCE;
				}

			} else if (SchemeMetaInfos.TransactionsSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				dataFormatType = DataFormatType.TRANSACTIONS;
			} else if (SchemeMetaInfos.TextSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				dataFormatType = DataFormatType.TEXTFILE;
			}

			return propositionalContextLoader(dataFormatType).load(
					new ProcessedDataTableIdentifier(scheme.getDataTable(), scheme), schemeMetaInfo, worksheetMetaInfo);
		}

		@Override
		public boolean save(SaveDataParameter<Worksheet> parameter, Worksheet worskheet) {
			return saveDataHandler(parameter.getClass()).save(parameter);
		}

		@Override
		public boolean remove(Worksheet worskheet) {
			return false;
		}

	}

	private static class WorksheetMetaInfoAccessHandler
			implements be.uantwerpen.sniper.dao.WorksheetAccessHandler<WorksheetMetaInfo> {

		@Override
		public Optional<WorksheetMetaInfo> load(Worksheet worksheet) {
			Optional<File> file = fileBasedPersistentStorageHandler().get(new WorksheetMetaInfoIdentifier(worksheet));

			if (!file.isPresent()) {
				return Optional.empty();
			}

			try {
				return Optional.of(new ObjectMapper().readValue(file.get(), WorksheetMetaInfo.class));
			} catch (IOException e) {
				e.printStackTrace();

				return Optional.empty();
			}
		}

		@Override
		public boolean save(SaveDataParameter<Worksheet> parameter, Worksheet worksheet) {
			return saveDataHandler(parameter.getClass()).save(parameter);
		}

		@Override
		public boolean remove(Worksheet worksheet) {
			return fileBasedPersistentStorageHandler().delete(new WorksheetMetaInfoIdentifier(worksheet));
		}

	}

}
