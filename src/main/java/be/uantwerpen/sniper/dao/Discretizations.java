/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.common.DiscretizationType;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Discretizations {

	public static class TypeBasedDiscretization implements Discretization {

		@JsonProperty("discretization")
		private DiscretizationType discretization;

		@JsonProperty("value")
		private int value;

		public TypeBasedDiscretization(@JsonProperty("discretization") DiscretizationType discretization,
				@JsonProperty("value") int value) {
			this.discretization = discretization;
			this.value = value;
		}

		public DiscretizationType discretization() {
			return this.discretization;
		}

		public int value() {
			return this.value;
		}

	}

	public static class DateBasedDiscretization implements Discretization {

		@JsonProperty("values")
		private List<String> values;

		public DateBasedDiscretization(@JsonProperty("values") List<String> values) {
			this.values = values;
		}

		public List<String> values() {
			return this.values;
		}

	}

	public static class FixedDiscretization implements Discretization {

		@JsonProperty("borders")
		private List<Double> borders;

		public FixedDiscretization(@JsonProperty("borders") List<Double> borders) {
			this.borders = ImmutableList.copyOf(borders);
		}

		public List<Double> borders() {
			return this.borders;
		}

	}

}
