/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import static be.uantwerpen.sniper.dao.PersistentStorageHandlers.fileBasedPersistentStorageHandler;
import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.WorksheetMetaInfoIdentifier;
import be.uantwerpen.sniper.dao.SaveDataParameters.SaveWorksheetMetaInfoParameter;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SaveDataHandlersWorksheet {

	@SaveDataHandlerAnn(saveDataParameterClass = SaveWorksheetMetaInfoParameter.class)
	public static SaveDataHandler<SaveWorksheetMetaInfoParameter> saveWorksheetMetaInfoHandler() {
		return new SaveDataHandlersWorksheet.SaveWorksheetMetaInfoHandler();
	}

	private static class SaveWorksheetMetaInfoHandler implements SaveDataHandler<SaveWorksheetMetaInfoParameter> {

		@Override
		public boolean save(SaveWorksheetMetaInfoParameter parameter) {
			List<String> data;
			try {
				data = newArrayList(new ObjectMapper().writeValueAsString(parameter.worksheetMetaInfo()));
			} catch (JsonProcessingException e) {
				e.printStackTrace();

				return false;
			}

			return fileBasedPersistentStorageHandler().store(new WorksheetMetaInfoIdentifier(parameter.worksheet()),
					data.iterator());
		}

	}

	// Suppress default constructor for non-instantiability
	private SaveDataHandlersWorksheet() {
		throw new AssertionError();
	}

}
