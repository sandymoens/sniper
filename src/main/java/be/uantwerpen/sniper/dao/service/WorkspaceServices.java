/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import be.uantwerpen.sniper.dao.entity.Workspace;
import be.uantwerpen.sniper.dao.repository.WorkspaceRepository;
import be.uantwerpen.sniper.dao.service.Services.DefaultServiceStringId;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class WorkspaceServices {

	@Service
	public static class DefaultWorkspaceService extends DefaultServiceStringId<Workspace> implements WorkspaceService {

		private WorkspaceRepository workspaceRespository;

		@Autowired
		public DefaultWorkspaceService(WorkspaceRepository workspaceRespository) {
			super(workspaceRespository);

			this.workspaceRespository = workspaceRespository;
		}

		@Override
		public Workspace newObject(String id) {
			return new Workspace(id);
		}

		@Override
		public List<Workspace> allInjected() {
			return this.workspaceRespository.allInjected();
		}

		@Override
		public Optional<Workspace> getInjected(String id) {
			return this.workspaceRespository.getInjected(id);
		}

	}

	// Suppress default constructor for non-instantiability
	private WorkspaceServices() {
		throw new AssertionError();
	}

}
