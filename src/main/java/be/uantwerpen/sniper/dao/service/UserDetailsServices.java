/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import be.uantwerpen.sniper.dao.entity.Role;
import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.repository.UserRepository;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class UserDetailsServices {

	@Service
	public static class UserDetailsServiceImpl implements UserDetailsService {

		@Autowired
		private UserRepository userRepository;

		@Override
		@Transactional(readOnly = true)
		public UserDetails loadUserByUsername(String username) {
			Optional<User> user = this.userRepository.getByUsernameInjected(username);
			if (!user.isPresent()) {
				throw new UsernameNotFoundException(username);
			}

			Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
			for (Role role : user.get().getRoles()) {
				grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
			}

			return new org.springframework.security.core.userdetails.User(user.get().getUsername(),
					user.get().getPassword(), grantedAuthorities);
		}

	}

	// Suppress default constructor for non-instantiability
	private UserDetailsServices() {
		throw new AssertionError();
	}

}
