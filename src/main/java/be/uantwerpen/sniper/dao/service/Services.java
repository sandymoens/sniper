/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.transaction.annotation.Transactional;

import be.uantwerpen.sniper.dao.entity.RepositoryEntity;
import be.uantwerpen.sniper.dao.repository.Repository;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class Services {

	public static abstract class DefaultServiceStringId<T extends RepositoryEntity<String>>
			implements Service<String, T> {

		private Repository<String, T> repository;

		public DefaultServiceStringId(Repository<String, T> repository) {
			this.repository = repository;
		}

		@Override
		public T newObject() {
			String id = UUID.randomUUID().toString();

			while (this.repository.contains(id)) {
				id = UUID.randomUUID().toString();
			}

			return newObject(id);
		}

		@Override
		public List<T> all() {
			return this.repository.all();
		}

		@Override
		public List<T> allInjected() {
			return this.repository.allInjected();
		}

		@Override
		public Optional<T> get(String id) {
			return this.repository.get(id);
		}

		@Override
		public Optional<T> getInjected(String id) {
			return this.repository.getInjected(id);
		}

		@Override
		public List<T> getInjected(List<String> ids) {
			return this.repository.getInjected(ids);
		}

		@Override
		@Transactional
		public boolean add(T object) {
			return this.repository.add(object);
		}

		@Override
		@Transactional
		public boolean remove(String id) {
			return this.repository.remove(id);
		}

	}

	public static abstract class DefaultServiceIntegerId<T extends RepositoryEntity<Integer>>
			implements Service<Integer, T> {

		private Repository<Integer, T> repository;

		public DefaultServiceIntegerId(Repository<Integer, T> repository) {
			this.repository = repository;
		}

		@Override
		public T newObject() {
			int id = 0;

			while (this.repository.contains(id)) {
				id++;
			}

			return newObject(id);
		}

		@Override
		public List<T> all() {
			return this.repository.all();
		}

		@Override
		public List<T> allInjected() {
			return this.repository.allInjected();
		}

		@Override
		public Optional<T> get(Integer id) {
			return this.repository.get(id);
		}

		@Override
		public Optional<T> getInjected(Integer id) {
			return this.repository.getInjected(id);
		}

		@Override
		public List<T> getInjected(List<Integer> ids) {
			return this.repository.getInjected(ids);
		}

		@Override
		@Transactional
		public boolean add(T object) {
			return this.repository.add(object);
		}

		@Override
		@Transactional
		public boolean remove(Integer id) {
			return this.repository.remove(id);
		}

	}

	// Suppress default constructor for non-instantiability
	private Services() {
		throw new AssertionError();
	}

}
