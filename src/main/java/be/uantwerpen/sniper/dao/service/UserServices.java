/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;

import be.uantwerpen.sniper.dao.entity.Role;
import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.repository.RoleRepository;
import be.uantwerpen.sniper.dao.repository.UserRepository;
import be.uantwerpen.sniper.dao.service.Services.DefaultServiceIntegerId;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class UserServices {

	@Service
	public static class DefaultUserService extends DefaultServiceIntegerId<User> implements UserService {

		private UserRepository userRepository;

		@Autowired
		private RoleRepository roleRepository;

		@Autowired
		public DefaultUserService(UserRepository userRepository) {
			super(userRepository);

			this.userRepository = userRepository;
		}

		private Optional<User> addNew(User user, String roleName) {
			Optional<Role> role = this.roleRepository.findByName(roleName);

			if (!role.isPresent()) {
				return Optional.empty();
			}

			User newUser = new User(user.getUsername(), user.getPassword(), user.getPasswordConfirm(),
					Sets.newHashSet(role.get()), false);

			this.userRepository.add(newUser);

			return this.userRepository.findByUsername(newUser.getUsername());
		}

		@Override
		public Optional<User> addNewUser(User user) {
			return addNew(user, "ROLE_USER");
		}

		@Override
		public Optional<User> addNewAdmin(User user) {
			return addNew(user, "ROLE_ADMIN");
		}

		@Override
		public Optional<User> updateUser(User user) {
			Optional<User> updateUser = findByUsername(user.getUsername());

			if (!updateUser.isPresent()) {
				return Optional.empty();
			}

			updateUser.get().setPassword(user.getPassword());
			updateUser.get().setPasswordConfirm(user.getPasswordConfirm());
			updateUser.get().setRoles(user.getRoles());
			updateUser.get().setDataTables(user.getDataTables());
			updateUser.get().setSchemes(user.getSchemes());
			updateUser.get().setWorkspaces(user.getWorkspaces());
			updateUser.get().setEnabled(user.getEnabled());

			if (this.userRepository.update(updateUser.get())) {
				return this.userRepository.findByUsername(user.getUsername());
			}

			return Optional.empty();

		}

		@Override
		public User newObject(Integer id) {
			return new User(id);
		}

		@Override
		public Optional<User> findByUsername(String username) {
			return this.userRepository.findByUsername(username);
		}

		@Override
		public Optional<User> getByUsernameInjected(String username) {
			return this.userRepository.getByUsernameInjected(username);
		}

	}

}
