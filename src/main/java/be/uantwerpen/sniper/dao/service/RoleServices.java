/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2019 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import be.uantwerpen.sniper.dao.entity.Role;
import be.uantwerpen.sniper.dao.repository.RoleRepository;
import be.uantwerpen.sniper.dao.service.Services.DefaultServiceIntegerId;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class RoleServices {

	@Service
	public static class DefaultRoleService extends DefaultServiceIntegerId<Role> implements RoleService {

		private RoleRepository roleRepository;

		@Autowired
		public DefaultRoleService(RoleRepository roleRepository) {
			super(roleRepository);

			this.roleRepository = roleRepository;
		}

		@Override
		public Role newObject() {
			return new Role();
		}

		@Override
		public Role newObject(Integer id) {
			return new Role(id);
		}

		@Override
		public Optional<Role> findRoleByName(String name) {
			return this.roleRepository.findByName(name);
		}

	}

}
