/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.service;

import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;

import be.uantwerpen.sniper.dao.entity.DataTable;
import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.dao.entity.User;
import be.uantwerpen.sniper.dao.repository.DataTableRepository;
import be.uantwerpen.sniper.dao.repository.UserRepository;
import be.uantwerpen.sniper.dao.service.Services.DefaultServiceStringId;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class DataTableServices {

	@Service
	public static class DefaultDataTableService extends DefaultServiceStringId<DataTable> implements DataTableService {

		private DataTableRepository dataTableRespository;

		@Autowired
		private UserRepository userRespository;

		@Autowired
		public DefaultDataTableService(DataTableRepository dataTableRespository) {
			super(dataTableRespository);

			this.dataTableRespository = dataTableRespository;
		}

		@Override
		public DataTable newObject(String id) {
			return new DataTable(id);
		}

		private DataTable filterSchemes(DataTable dataTable, Set<String> schemeIds) {
			Set<Scheme> schemes = Sets.newHashSet();

			for (Scheme scheme : dataTable.getSchemes()) {
				if (schemeIds.contains(scheme.getId())) {
					schemes.add(scheme);
				}
			}

			dataTable.setSchemes(schemes);

			return dataTable;
		}

		@Override
		public List<DataTable> getInjected(List<String> ids, int userId) {
			User user = this.userRespository.getInjected(userId).get();
			Set<String> schemeIds = user.getSchemes().stream().map(s -> s.getId()).collect(toSet());

			List<DataTable> dataTables = this.dataTableRespository.getInjected(ids);

			return dataTables.stream().map(d -> filterSchemes(d, schemeIds)).collect(Collectors.toList());
		}

	}

	// Suppress default constructor for non-instantiability
	private DataTableServices() {
		throw new AssertionError();
	}

}
