/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.dao.repository.SchemeRepository;
import be.uantwerpen.sniper.dao.service.Services.DefaultServiceStringId;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SchemeServices {

	@Service
	public static class DefaultSchemeService extends DefaultServiceStringId<Scheme> implements SchemeService {

		@Autowired
		public DefaultSchemeService(SchemeRepository schemeRespository) {
			super(schemeRespository);
		}

		@Override
		public Scheme newObject(String id) {
			return new Scheme(id);
		}

	}

	// Suppress default constructor for non-instantiability
	private SchemeServices() {
		throw new AssertionError();
	}

}
