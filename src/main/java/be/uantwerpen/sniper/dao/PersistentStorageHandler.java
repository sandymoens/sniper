/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import java.util.Iterator;
import java.util.Optional;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public interface PersistentStorageHandler<T> {

	/**
	 * Stores the data to the persistent storage medium. The data is identified by
	 * the given identifier. This method will return true if the data is stored
	 * properly. If the give identifier already exists, this method returns false.
	 * 
	 * @param identifier
	 *            the identifier for the data on the persistent storage medium
	 * @param data
	 *            the data that needs to be stored
	 * @return true if the data is stored correctly, else false
	 */
	public boolean store(PersistentStorageIdentifier identifier, Iterator<String> data);

	/**
	 * Returns a representation of the data identified by the given identifier.
	 * 
	 * @param identifier
	 *            the identifier of the data to retrieve
	 * @return an optional containing a representation of the data, or an empty
	 *         optional if the identifier is not found
	 */
	public Optional<T> get(PersistentStorageIdentifier identifier);

	/**
	 * Checks if the given identifier exists on the persistent storage medium.
	 * 
	 * @param identifier
	 *            the identifier of the data to find
	 * @return true if the identifier is found, else false
	 */
	public boolean exists(PersistentStorageIdentifier identifier);

	/**
	 * Removed the data identified by the given identifier. This method return true
	 * if a subsequent call to exists returns false, even when the identifier was
	 * not present prior to deletion.
	 * 
	 * @param identifier
	 *            the identifier of the data to remove
	 * @return true if the data has been deleted successfully or if the identifier
	 *         did not exist
	 */
	public boolean delete(PersistentStorageIdentifier identifier);

}
