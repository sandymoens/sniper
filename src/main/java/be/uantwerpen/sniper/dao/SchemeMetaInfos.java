/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.common.AttributeType;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SchemeMetaInfos {

	public static class SchemeAttribute {

		@JsonProperty("caption")
		private final String caption;

		@JsonProperty("type")
		private final AttributeType type;

		@JsonProperty("active")
		private final boolean active;

		@JsonProperty("icon")
		private final String icon;

		public SchemeAttribute(@JsonProperty("caption") String caption, @JsonProperty("type") AttributeType type,
				@JsonProperty("active") boolean active, @JsonProperty("icon") String icon) {
			this.caption = caption;
			this.type = type;
			this.active = active;
			this.icon = icon;
		}

		public String caption() {
			return this.caption;
		}

		public AttributeType type() {
			return this.type;
		}

		public boolean active() {
			return this.active;
		}

		public String icon() {
			return this.icon;
		}

	}

	@JsonTypeName("dataTable")
	public static class DataTableSchemeMetaInfo implements SchemeMetaInfo {

		private static final String SCHEME_TYPE = "dataTable";

		@JsonProperty("caption")
		private final String caption;

		@JsonProperty("description")
		private final String description;

		@JsonProperty("delimiter")
		private final String delimiter;

		@JsonProperty("hasHeaders")
		private final boolean hasHeaders;

		@JsonProperty("dateTimeFormat")
		private final String dateTimeFormat;

		@JsonProperty("attributes")
		private final List<SchemeAttribute> attributes;

		public DataTableSchemeMetaInfo(@JsonProperty("caption") String caption,
				@JsonProperty("description") String description, @JsonProperty("delimiter") String delimiter,
				@JsonProperty("hasHeaders") boolean hasHeaders, @JsonProperty("dateTimeFormat") String dateTimeFormat,
				@JsonProperty("attributes") List<SchemeAttribute> attributes) {
			this.caption = caption;
			this.description = description;
			this.delimiter = delimiter;
			this.hasHeaders = hasHeaders;
			this.dateTimeFormat = dateTimeFormat;
			this.attributes = ImmutableList.copyOf(attributes);
		}

		@Override
		public String schemeType() {
			return SCHEME_TYPE;
		}

		public String caption() {
			return this.caption;
		}

		public String description() {
			return this.description;
		}

		public String delimiter() {
			return this.delimiter;
		}

		public boolean hasHeaders() {
			return this.hasHeaders;
		}

		public String dateTimeFormat() {
			return this.dateTimeFormat;
		}

		public List<SchemeAttribute> attributes() {
			return this.attributes;
		}

	}

	@JsonTypeName("transactions")
	public static class TransactionsSchemeMetaInfo implements SchemeMetaInfo {

		private static final String SCHEME_TYPE = "transactions";

		@JsonProperty("caption")
		private final String caption;

		@JsonProperty("description")
		private final String description;

		@JsonProperty("delimiter")
		private final String delimiter;

		public TransactionsSchemeMetaInfo(@JsonProperty("caption") String caption,
				@JsonProperty("description") String description, @JsonProperty("delimiter") String delimiter) {
			this.caption = caption;
			this.description = description;
			this.delimiter = delimiter;
		}

		@Override
		public String schemeType() {
			return SCHEME_TYPE;
		}

		public String caption() {
			return this.caption;
		}

		public String description() {
			return this.description;
		}

		public String delimiter() {
			return this.delimiter;
		}

	}

	public static enum DFFilterType {
		NONE, TOPX, MINIMUMDF;
	}

	public static abstract class TextSchemeMetaInfo implements SchemeMetaInfo {

		@JsonProperty("caption")
		private final String caption;

		@JsonProperty("description")
		private final String description;

		@JsonProperty("recognizeSentences")
		private boolean recognizeSentences;

		@JsonProperty("alphabeticalOnly")
		private boolean alphabeticalOnly;

		@JsonProperty("filterPOS")
		private boolean filterPOS;

		@JsonProperty("stemTokens")
		private boolean stemTokens;

		public TextSchemeMetaInfo(@JsonProperty("caption") String caption,
				@JsonProperty("description") String description,
				@JsonProperty("recognizeSentences") boolean recognizeSentences,
				@JsonProperty("alphabeticalOnly") boolean alphabeticalOnly,
				@JsonProperty("filterPOS") boolean filterPOS, @JsonProperty("stemTokens") boolean stemTokens) {
			this.caption = caption;
			this.description = description;
			this.recognizeSentences = recognizeSentences;
			this.alphabeticalOnly = alphabeticalOnly;
			this.filterPOS = filterPOS;
			this.stemTokens = stemTokens;
		}

		public String caption() {
			return this.caption;
		}

		public String description() {
			return this.description;
		}

		public boolean recognizeSentences() {
			return this.recognizeSentences;
		}

		public boolean alphabeticalOnly() {
			return this.alphabeticalOnly;
		}

		public boolean filterPOS() {
			return this.filterPOS;
		}

		public boolean stemTokens() {
			return this.stemTokens;
		}

	}

	@JsonTypeName("textAsTransactions")
	public static class TextAsTransactionsSchemeMetaInfo extends TextSchemeMetaInfo {

		private static final String SCHEME_TYPE = "textAsTransactions";

		@JsonProperty("dFFilterType")
		private DFFilterType dFFilterType;

		@JsonProperty("dFFilterValue")
		private int dFFilterValue;

		public TextAsTransactionsSchemeMetaInfo(@JsonProperty("caption") String caption,
				@JsonProperty("description") String description,
				@JsonProperty("recognizeSentences") boolean recognizeSentences,
				@JsonProperty("alphabeticalOnly") boolean alphabeticalOnly,
				@JsonProperty("filterPOS") boolean filterPOS, @JsonProperty("stemTokens") boolean stemTokens,
				@JsonProperty("dFFilterType") DFFilterType dFFilterType,
				@JsonProperty("dFFilterValue") int DFFilterValue) {
			super(caption, description, recognizeSentences, alphabeticalOnly, filterPOS, stemTokens);
			this.dFFilterType = dFFilterType;
			this.dFFilterValue = DFFilterValue;
		}

		@Override
		public String schemeType() {
			return SCHEME_TYPE;
		}

		public DFFilterType dFFilterType() {
			return this.dFFilterType;
		}

		public int dFFilterValue() {
			return this.dFFilterValue;
		}

	}

	@JsonTypeName("textAsBowTfIdf")
	public static class TextAsBowTfIdfSchemeMetaInfo extends TextSchemeMetaInfo {

		private static final String SCHEME_TYPE = "textAsBowTfIdf";
		@JsonProperty("numberOfBins")
		private int numberOfBins;

		public TextAsBowTfIdfSchemeMetaInfo(@JsonProperty("caption") String caption,
				@JsonProperty("description") String description,
				@JsonProperty("recognizeSentences") boolean recognizeSentences,
				@JsonProperty("alphabeticalOnly") boolean alphabeticalOnly,
				@JsonProperty("filterPOS") boolean filterPOS, @JsonProperty("stemTokens") boolean stemTokens,
				@JsonProperty("numberOfBins") int numberOfBins) {
			super(caption, description, recognizeSentences, alphabeticalOnly, filterPOS, stemTokens);
			this.numberOfBins = numberOfBins;
		}

		@Override
		public String schemeType() {
			return SCHEME_TYPE;
		}

		public int numberOfBins() {
			return this.numberOfBins;
		}

	}

}
