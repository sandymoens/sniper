/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import static be.uantwerpen.sniper.dao.PersistentStorageHandlers.fileBasedPersistentStorageHandler;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;

import be.uantwerpen.sniper.common.AttributeType;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.ProcessedDataTableIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.RawDataTableIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.SchemeMetaInformationIdentifier;
import be.uantwerpen.sniper.dao.SaveDataParameters.SaveDataSchemeDataTableParameter;
import be.uantwerpen.sniper.dao.SaveDataParameters.SaveDataSchemeMetaInfoParameter;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.DataTableSchemeMetaInfo;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.SchemeAttribute;
import be.uantwerpen.sniper.dao.SchemeMetaInfos.TransactionsSchemeMetaInfo;
import be.uantwerpen.sniper.dao.entity.DataTable;
import be.uantwerpen.sniper.dao.entity.Scheme;

/**
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class SaveDataHandlersScheme {

	@SaveDataHandlerAnn(saveDataParameterClass = SaveDataSchemeMetaInfoParameter.class)
	public static SaveDataHandler<SaveDataSchemeMetaInfoParameter> saveDataSchemeMetaInfoHandler() {
		return new SaveDataHandlersScheme.SaveDataSchemeMetaInfoHandler();
	}

	@SaveDataHandlerAnn(saveDataParameterClass = SaveDataSchemeDataTableParameter.class)
	public static SaveDataHandler<SaveDataSchemeDataTableParameter> saveDataSchemeDataTableHandler() {
		return new SaveDataHandlersScheme.SaveDataSchemeDataTableHandler();
	}

	private static class SaveDataSchemeMetaInfoHandler implements SaveDataHandler<SaveDataSchemeMetaInfoParameter> {

		@Override
		public boolean save(SaveDataSchemeMetaInfoParameter parameter) {
			List<String> data;
			try {
				data = newArrayList(new ObjectMapper().writeValueAsString(parameter.schemeMetaInfo()));
			} catch (JsonProcessingException e) {
				e.printStackTrace();

				return false;
			}

			return fileBasedPersistentStorageHandler().store(
					new SchemeMetaInformationIdentifier(parameter.dataTable(), parameter.scheme()), data.iterator());
		}

	}

	private static class SaveDataSchemeDataTableHandler implements SaveDataHandler<SaveDataSchemeDataTableParameter> {

		private static class DataIterator implements Iterator<String> {

			private Iterator<String> header;
			private BufferedReader reader;
			private Set<Integer> columns;
			private String delimiter;
			private int stage;
			private String next;
			private StringBuilder builder;

			public DataIterator(List<String> header, BufferedReader reader, Set<Integer> columns, String delimiter) {
				this.header = header.iterator();
				this.reader = reader;
				this.columns = columns;
				this.delimiter = delimiter;
				this.stage = 0;
				this.builder = new StringBuilder();

				try {
					this.next = reader.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			@Override
			public boolean hasNext() {
				if (this.stage == 0 && this.header.hasNext()) {
					return true;
				}

				if (!this.header.hasNext()) {
					this.stage = 1;
				}

				return this.next != null;
			}

			@Override
			public String next() {
				if (this.stage == 0) {
					return this.header.next();
				}

				String toReturn = this.next;

				try {
					this.next = this.reader.readLine();

				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}

				if (this.delimiter != null) {
					this.builder.setLength(0);

					int i = 0;
					int added = 0;

					for (String str : toReturn.split(this.delimiter)) {
						if (this.columns == null || this.columns.contains(i)) {
							if (str.isEmpty()) {
								this.builder.append("?");
							} else {
								this.builder.append(str.replace(",", "."));
							}
							this.builder.append(",");
							added++;
						}
						i++;
					}

					if (this.columns != null) {
						for (; added < this.columns.size(); added++) {
							this.builder.append("?,");
						}
					}

					this.builder.setLength(this.builder.length() - 1);

					return this.builder.toString();
				}

				return toReturn;
			}

		}

		@Override
		public boolean save(SaveDataSchemeDataTableParameter parameter) {
			Optional<File> file = fileBasedPersistentStorageHandler()
					.get(new RawDataTableIdentifier(parameter.dataTable()));

			if (!file.isPresent()) {
				return false;
			}

			List<String> data = getHeader(parameter);
			try {
				BufferedReader reader = new BufferedReader(new FileReader(file.get()));

				if (hasHeaders(parameter.schemeMetaInfo())) {
					reader.readLine();
				}

				DataIterator dataIterator = new DataIterator(data, reader, getColumns(parameter.schemeMetaInfo()),
						getDelimiter(parameter.schemeMetaInfo()));

				boolean success = fileBasedPersistentStorageHandler().store(
						new ProcessedDataTableIdentifier(parameter.dataTable(), parameter.scheme()), dataIterator);

				reader.close();

				return success;
			} catch (IOException e) {
				e.printStackTrace();
			}

			return false;
		}

		private boolean hasHeaders(SchemeMetaInfo schemeMetaInfo) {
			if (DataTableSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				return ((DataTableSchemeMetaInfo) schemeMetaInfo).hasHeaders();
			}

			return false;
		}

		private Set<Integer> getColumns(SchemeMetaInfo schemeMetaInfo) {
			if (DataTableSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				Set<Integer> columns = newHashSet();

				int i = 0;
				for (SchemeAttribute attribute : ((DataTableSchemeMetaInfo) schemeMetaInfo).attributes()) {
					if (attribute.active()) {
						columns.add(i);
					}
					i++;
				}

				return columns;
			}

			return null;
		}

		private String getDelimiter(SchemeMetaInfo schemeMetaInfo) {
			if (DataTableSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				return ((DataTableSchemeMetaInfo) schemeMetaInfo).delimiter();
			} else if (TransactionsSchemeMetaInfo.class.isAssignableFrom(schemeMetaInfo.getClass())) {
				return ((TransactionsSchemeMetaInfo) schemeMetaInfo).delimiter();
			}

			return null;
		}

		private List<String> getHeader(SaveDataSchemeDataTableParameter parameter) {
			if (DataTableSchemeMetaInfo.class.isAssignableFrom(parameter.schemeMetaInfo().getClass())) {
				return getHeader(parameter.dataTable(), parameter.scheme(),
						(DataTableSchemeMetaInfo) parameter.schemeMetaInfo());
			}

			return ImmutableList.of();
		}

		private List<String> getHeader(DataTable dataTable, Scheme scheme, DataTableSchemeMetaInfo metaInfo) {
			List<String> header = newArrayList();

			header.add("@Relation " + dataTable.caption() + "_" + scheme.caption());
			header.add("");

			for (SchemeAttribute attribute : metaInfo.attributes()) {
				String dateTimeFormat = metaInfo.dateTimeFormat();
				dateTimeFormat = dateTimeFormat.replaceAll("Y", "y");
				dateTimeFormat = dateTimeFormat.replaceAll("D", "d");

				if (attribute.active()) {
					if (attribute.type().equals(AttributeType.METRIC)) {
						header.add("@Attribute " + attribute.caption() + " numeric");
					} else if (attribute.type().equals(AttributeType.DATE)) {
						header.add("@Attribute " + attribute.caption() + " " + attribute.type().toString().toLowerCase()
								+ " \"" + dateTimeFormat + "\"");
					} else {
						header.add(
								"@Attribute " + attribute.caption() + " " + attribute.type().toString().toLowerCase());
					}
				}
			}

			header.add("");
			header.add("@Data");

			return header;
		}

	}

	// Suppress default constructor for non-instantiability
	private SaveDataHandlersScheme() {
		throw new AssertionError();
	}

}
