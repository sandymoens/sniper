/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.dao;

import be.uantwerpen.sniper.dao.entity.DataTable;
import be.uantwerpen.sniper.dao.entity.Scheme;
import be.uantwerpen.sniper.dao.entity.Worksheet;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class PersistentStorageIdentifiers {

	public static class RawDataTableIdentifier implements PersistentStorageIdentifier {

		private DataTable dataTable;

		public RawDataTableIdentifier(DataTable dataTable) {
			this.dataTable = dataTable;
		}

		public String dataTableId() {
			return this.dataTable.getId();
		}

		public String dataTableCaption() {
			return this.dataTable.caption();
		}

	}

	public static class ProcessedDataTableIdentifier implements PersistentStorageIdentifier {

		private DataTable dataTable;
		private Scheme scheme;

		public ProcessedDataTableIdentifier(DataTable dataTable, Scheme scheme) {
			this.dataTable = dataTable;
			this.scheme = scheme;
		}

		public String dataTableId() {
			return this.dataTable.getId();
		}

		public String schemeId() {
			return this.scheme.getId();
		}

		public String dataTableCaption() {
			return this.dataTable.caption();
		}

	}

	public static class SchemeMetaInformationIdentifier implements PersistentStorageIdentifier {

		private DataTable dataTable;
		private Scheme scheme;

		public SchemeMetaInformationIdentifier(DataTable dataTable, Scheme scheme) {
			this.dataTable = dataTable;
			this.scheme = scheme;
		}

		public String dataTableId() {
			return this.dataTable.getId();
		}

		public String schemeId() {
			return this.scheme.getId();
		}

	}

	public static class WorksheetIdentifier implements PersistentStorageIdentifier {

		private Worksheet worksheet;

		public WorksheetIdentifier(Worksheet worksheet) {
			this.worksheet = worksheet;
		}

		public String worksheetId() {
			return this.worksheet.getId();
		}

	}

	public static class WorksheetMetaInfoIdentifier implements PersistentStorageIdentifier {

		private Worksheet worksheet;

		public WorksheetMetaInfoIdentifier(Worksheet worksheet) {
			this.worksheet = worksheet;
		}

		public String worksheetId() {
			return this.worksheet.getId();
		}

	}

}
