/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import be.uantwerpen.sniper.dao.entity.Worksheet;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class ActiveWorksheetControllers {

	private static Logger LOGGER = Logger.getLogger(ActiveWorksheetControllers.class);

	private static ActiveWorksheetController defaultActiveWorksheetController;

	private static LoadingCache<String, ActiveWorksheet> cache = CacheBuilder.newBuilder()
			.build(new CacheLoader<String, ActiveWorksheet>() {

				@Override
				public ActiveWorksheet load(String id) throws Exception {
					return null;
				}

			});

	public static ActiveWorksheetController activeWorksheetController() {
		return defaultActiveWorksheetController != null ? defaultActiveWorksheetController
				: (defaultActiveWorksheetController = new DefaultActiveWorksheetController());
	}

	private static class DefaultActiveWorksheetController implements ActiveWorksheetController {

		@Override
		public Optional<ActiveWorksheet> activeWorksheet(Worksheet worksheet) {
			ActiveWorksheet activeWorksheet;
			try {
				activeWorksheet = cache.get(worksheet.getId(), new WorksheetLoadActions.WorksheetLoadAction(worksheet));
			} catch (ExecutionException e) {
				e.printStackTrace();
				LOGGER.error("Unable to load worksheet with id '" + worksheet.getId() + "'.");
				return Optional.empty();
			}

			if (activeWorksheet != null) {
				return Optional.of(activeWorksheet);
			}

			return Optional.empty();
		}

	}

	// Suppress default constructor for non-instantiability
	private ActiveWorksheetControllers() {
		throw new AssertionError();
	}

}
