/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.command;

import static be.uantwerpen.sniper.utils.MeasureRegister.measureRegister;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import be.uantwerpen.sniper.ActiveWorksheet;
import be.uantwerpen.sniper.action.Action;
import be.uantwerpen.sniper.model.bricks.Brick;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class ActionBasedUndoCommands {

	public static ActionBasedUndoCommand actionBasedUndoCommand(ActiveWorksheet activeWorksheet, Action action,
			List<Brick<Pattern<?>, ?>> previousBricks, List<Brick<Pattern<?>, ?>> newBricks) {
		return new ListBasedUndoCommand(activeWorksheet, action, previousBricks, newBricks);
	}

	private static class ListBasedUndoCommand implements ActionBasedUndoCommand {

		private ActiveWorksheet activeWorksheet;
		private Action action;
		private List<Brick<Pattern<?>, ?>> previousBricks;
		private List<Brick<Pattern<?>, ?>> newBricks;

		public ListBasedUndoCommand(ActiveWorksheet activeWorksheet, Action action,
				List<Brick<Pattern<?>, ?>> previousBricks, List<Brick<Pattern<?>, ?>> newBricks) {
			this.activeWorksheet = activeWorksheet;
			this.action = action;
			this.previousBricks = previousBricks;
			this.newBricks = newBricks;
		}

		private void setBricks(List<Brick<Pattern<?>, ?>> bricks) {
			List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> procedures = newArrayList();

			for (Measure measure : this.activeWorksheet.measureModel().enabled()) {
				procedures.addAll(measureRegister().getMeasurementProcedures(measure));
			}

			List<Object> bricksToAdd = bricks.stream().map(brick -> brick.content()).collect(toList());

			this.activeWorksheet.brickModel().clear();
			this.activeWorksheet.brickModel().addAll(0, bricksToAdd);

			// try {
			// this.activeWorksheet.realKDWorkspace().overwrite(new
			// NamedPatternCollection("PatternCollection",
			// "PatternCollection", "Collection of patterns",
			// this.activeWorksheet.brickModel().patterns()));
			// } catch (OutOfMemoryError e) {
			// e.printStackTrace();
			// }
		}

		@Override
		public void undo() {
			this.activeWorksheet.patternController().measureComputerDaemon().pause();
			setBricks(this.previousBricks);
			this.activeWorksheet.patternController().measureComputerDaemon().restart();
		}

		@Override
		public void redo() {
			this.activeWorksheet.patternController().measureComputerDaemon().pause();
			setBricks(this.newBricks);
			this.activeWorksheet.patternController().measureComputerDaemon().restart();
		}

		@Override
		public Optional<Object> execute() {
			return Optional.empty();
		}

		@Override
		public Action action() {
			return this.action;
		}

	}

	// Suppress default constructor for non-instantiability
	private ActionBasedUndoCommands() {
		throw new AssertionError();
	}

}
