/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2018 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.sniper.command;

import java.util.Stack;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class UndoStacks {

	public static UndoStack<ActionBasedUndoCommand> newActionCommandBasedUndoStack(int maxSize) {
		return new ActionCommandBasedUndoStack(maxSize);
	}

	private static class ActionCommandBasedUndoStack implements UndoStack<ActionBasedUndoCommand> {

		private final Stack<ActionBasedUndoCommand> undoStack;
		private final Stack<ActionBasedUndoCommand> redoStack;

		private int maxSize;

		public ActionCommandBasedUndoStack(int maxSize) {
			this.undoStack = new Stack<>();
			this.redoStack = new Stack<>();

			this.maxSize = maxSize;
		}

		@Override
		public synchronized void push(ActionBasedUndoCommand command) {
			this.undoStack.push(command);

			while (this.undoStack.size() > this.maxSize) {
				this.undoStack.remove(0);
			}

			command.redo();

			this.redoStack.clear();
		}

		@Override
		public boolean canUndo() {
			return !this.undoStack.isEmpty();
		}

		@Override
		public boolean canRedo() {
			return !this.redoStack.isEmpty();
		}

		@Override
		public synchronized void undo() {
			if (this.undoStack.isEmpty()) {
				throw new RuntimeException("Can not undo because the stack is empty");
			}

			ActionBasedUndoCommand undoCommand = this.undoStack.pop();

			undoCommand.undo();

			this.redoStack.push(undoCommand);
		}

		@Override
		public synchronized void redo() {
			if (this.redoStack.isEmpty()) {
				throw new RuntimeException("Can not redo because the stack is empty");
			}

			ActionBasedUndoCommand undoCommand = this.redoStack.pop();

			undoCommand.redo();

			this.undoStack.push(undoCommand);
		}

		@Override
		public ActionBasedUndoCommand peek() {
			return this.undoStack.peek();
		}

	}

	// Suppress default constructor for non-instantiability
	private UndoStacks() {
		throw new AssertionError();
	}

}
