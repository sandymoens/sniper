/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.io.filesupport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class DataProviders {

	public static DataProvider newFlatFileHandlerNoDelimiter(InputStreamSource streamSource) {
		return new FlatFile(streamSource, null);
	}

	public static DataProvider newFlatFileHandlerWithDelimiter(InputStreamSource streamSource, String delimiter) {
		return new FlatFile(streamSource, delimiter);
	}

	public static DataProvider newSQLDumpFileHandlerWithDelimiter(InputStreamSource streamSource) {
		throw new NotYetImplementedException();
		// return new SQLDumpFile(streamSource);
	}

	public static DataProvider newExcelFileHandler(InputStreamSource streamSource) {
		return new ExcelFile(streamSource);
	}

	public static DataProvider newSQLTableRowProvider(String hostName, String database, String table, String userName,
			String password) throws ClassNotFoundException, SQLException {
		return new SQLTableRowProvider(hostName, database, table, userName, password);
	}

	/**
	 * Implements a data provider that can be used for flat files on the file
	 * system. It can accept a delimiter that is used to split lines into columns.
	 * It also skips over empty lines and lines start with a "%"-comment symbol.
	 *
	 * @author Sandy Moens
	 * @since 0.0.2
	 * @version 0.2.0
	 */
	private static class FlatFile implements DataProvider {

		private InputStreamSource streamSource;
		private BufferedReader reader;

		private FlatFile(InputStreamSource streamSource, String delimiter) {
			this.streamSource = streamSource;
		}

		@Override
		public void close() {
			try {
				this.reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public Iterator<String> iterator() {
			try {
				this.reader = new BufferedReader(new InputStreamReader(this.streamSource.getInputStream()));
				return this.reader.lines().iterator();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	/**
	 * Implements a data provider that can be used for sql dump files on the file
	 * system. The data lines should be on separate lines in the file. This class
	 * can handle two types of formatting:
	 * 
	 * 1) <code>
	 * INSERT INTO `testDataTable` (`id`, `name`, `f1`, `f2`)
	 * VALUES (1,'Jan',6,'e'),
	 * (2,'Jop',5,'r'),
	 * UNLOCK TABLES;
	 * </code>
	 * 
	 * 2) <code>
	 * INSERT INTO `testDataTable` VALUES (1,'Jan',6,'e');
	 * INSERT INTO `testDataTable` VALUES (2,'Jop',5,'r');
	 * UNLOCK TABLES;
	 * </code>
	 * 
	 * It stops reading as soon as "UNLOCK_TABLES;" has been read and also skips the
	 * empty lines.
	 *
	 * @author Sandy Moens
	 * @since 0.0.2
	 * @version 0.0.2
	 */
	// private static class SQLDumpFile implements DataProvider {
	//
	// private final class IteratorImplementation implements Iterator<String[]> {
	//
	// private final Iterator<String> iterator;
	// private String next = null;
	// private Pattern pattern;
	//
	// private IteratorImplementation(Iterator<String> iterator) {
	// this.pattern = Pattern.compile("^INSERT INTO.*.VALUES ");
	// this.iterator = iterator;
	// while ((this.next = iterator.next()) != null) {
	// if (this.next.equals("VALUES")) {
	// this.next = iterator.next();
	// this.pattern = null;
	// break;
	// } else if (this.pattern.matcher(this.next).find()) {
	// break;
	// }
	// }
	// }
	//
	// @Override
	// public boolean hasNext() {
	// return this.next != null;
	// }
	//
	// @Override
	// public String[] next() {
	// String toReturn = this.next;
	// while (true) {
	// try {
	// this.next = this.iterator.next();
	// if (this.next.startsWith("UNLOCK TABLES;")) {
	// this.next = null;
	// break;
	// }
	// if (!this.next.isEmpty() && !this.next.startsWith("--") &&
	// !this.next.startsWith("#")
	// && !this.next.startsWith("/*")) {
	// break;
	// }
	// } catch (Exception e) {
	// this.next = null;
	// break;
	// }
	// }
	//
	// if (this.pattern != null) {
	// toReturn = toReturn.replaceAll("^INSERT INTO.*.VALUES ", "");
	// }
	//
	// toReturn = toReturn.replaceAll("(^(\t)?\\()|(\\)(;|,)$)", "");
	// String[] values = toReturn.split(",");
	// for (int i = 0, end = values.length; i < end; i++) {
	// values[i] = values[i].replaceAll("(^')|('$)", "");
	// }
	// return values;
	// }
	// }
	//
	// private InputStreamSource streamSource;
	//
	// private SQLDumpFile(InputStreamSource streamSource) {
	// this.streamSource = streamSource;
	// }
	//
	// @Override
	// public Iterator<String> iterator() {
	// try {
	// BufferedReader reader = new BufferedReader(new
	// InputStreamReader(this.streamSource.getInputStream()));
	// return reader.lines().iterator();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// return null;
	// }
	// }

	/**
	 * Implements a data provider for excel files in .xlsx format on the file
	 * system. Each iteration a new row is returned with and the columns are
	 * provided as separate Strings.
	 *
	 * @author Sandy Moens
	 * @since 0.0.2
	 * @version 0.0.2
	 */
	private static class ExcelFile implements DataProvider {

		private InputStreamSource streamSource;
		private InputStream inputStream;
		private XSSFWorkbook wb;

		private ExcelFile(InputStreamSource streamSource) {
			this.streamSource = streamSource;
		}

		@Override
		public void close() {
			try {
				this.inputStream.close();
				this.wb.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public Iterator<String> iterator() {
			try {
				this.inputStream = this.streamSource.getInputStream();
				this.wb = new XSSFWorkbook(this.inputStream);
				XSSFSheet ws = this.wb.getSheetAt(0);

				final Iterator<Row> iterator = ws.iterator();

				return new Iterator<String>() {

					private Iterator<Row> localIterator = iterator;
					private StringBuilder builder = new StringBuilder();

					@Override
					public boolean hasNext() {
						return this.localIterator.hasNext();
					}

					@Override
					public String next() {
						this.builder.setLength(0);

						Row row = this.localIterator.next();

						Iterator<Cell> cellIterator = row.cellIterator();

						while (cellIterator.hasNext()) {
							this.builder.append(cellIterator.next());
							this.builder.append(";");
						}

						this.builder.setLength(this.builder.length() - 1);

						return this.builder.toString();
					}
				};
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	private static class SQLTableRowProvider implements DataProvider {

		private Connection con;
		private String table;
		private Statement statement;
		private String[] names;
		private String[] values;
		private String valuesString;

		public SQLTableRowProvider(String hostName, String database, String table, String userName, String password)
				throws ClassNotFoundException, SQLException {
			Class.forName("com.mysql.jdbc.Driver");
			String host = "jdbc:mysql://" + hostName + ":3307/" + database;
			this.con = DriverManager.getConnection(host, userName, password);
			this.table = table;
			this.statement = this.con.createStatement();

			ResultSet rs = this.statement.executeQuery("SELECT * FROM " + this.table);
			ResultSetMetaData md = rs.getMetaData();
			int colCount = md.getColumnCount();

			this.names = new String[colCount];
			this.values = new String[colCount];

			StringBuilder builder = new StringBuilder();

			for (int i = 0; i < colCount; i++) {
				this.names[i] = md.getColumnName(i + 1);
				this.values[i] = md.getColumnName(i + 1);
				builder.append(md.getColumnName(i + 1));
				builder.append(";");
			}

			builder.setLength(builder.length() - 1);

			this.valuesString = builder.toString();
		}

		@Override
		public void close() {
			try {
				this.con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public Iterator<String> iterator() {
			try {
				ResultSet result = this.statement.executeQuery("SELECT * FROM " + this.table);
				return new Iterator<String>() {
					String next = SQLTableRowProvider.this.valuesString;
					boolean hasNext = true;

					StringBuilder builder = new StringBuilder();

					@Override
					public boolean hasNext() {
						return this.hasNext;
					}

					@Override
					public String next() {
						try {
							String toReturn = this.next;
							if (this.hasNext = result.next()) {
								this.builder.setLength(0);

								for (int i = 0, end = SQLTableRowProvider.this.names.length; i < end; i++) {
									this.builder.append(result.getString(SQLTableRowProvider.this.names[i]));
									this.builder.append(";");
								}

								this.builder.setLength(this.builder.length() - 1);
								this.next = this.builder.toString();
							} else {
								this.next = null;
							}
							return toReturn;
						} catch (SQLException e) {
							e.printStackTrace();
							this.hasNext = false;
							this.next = null;
						}
						return null;
					}

				};
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}

	}

	// Suppress default constructor for non-instantiability
	private DataProviders() {
		throw new AssertionError();
	}

	public static void main(String[] args) throws IOException {
		{
			File file = new File("testData/testDataTable.dat");
			FileInputStream input = new FileInputStream(file);
			MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain",
					IOUtils.toByteArray(input));

			DataProvider h = DataProviders.newFlatFileHandlerWithDelimiter(multipartFile, " ");
			Iterator<String> iterator = h.iterator();
			while (iterator.hasNext()) {
				System.out.println(iterator.next());
			}
		}

		{
			File file = new File("testData/testDataTable.csv");
			FileInputStream input = new FileInputStream(file);
			MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain",
					IOUtils.toByteArray(input));

			DataProvider h = DataProviders.newFlatFileHandlerWithDelimiter(multipartFile, ";");
			Iterator<String> iterator = h.iterator();
			while (iterator.hasNext()) {
				System.out.println(iterator.next());
			}
		}

		{
			File file = new File("testData/testDataTable.xlsx");
			FileInputStream input = new FileInputStream(file);
			MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain",
					IOUtils.toByteArray(input));

			DataProvider h = DataProviders.newExcelFileHandler(multipartFile);
			Iterator<String> iterator = h.iterator();
			while (iterator.hasNext()) {
				System.out.println(iterator.next());
			}
		}

		// {
		// File file = new File("testData/testDataTable_mysqldump.sql");
		// FileInputStream input = new FileInputStream(file);
		// MultipartFile multipartFile = new MockMultipartFile("file", file.getName(),
		// "text/plain",
		// IOUtils.toByteArray(input));
		//
		// DataProvider h =
		// DataProviders.newSQLDumpFileHandlerWithDelimiter(multipartFile);
		// Iterator<String> iterator = h.iterator();
		// while (iterator.hasNext()) {
		// System.out.println(iterator.next());
		// }
		// }

		// {
		// File file = new File("testData/testDataTable_sequelPro.sql");
		// FileInputStream input = new FileInputStream(file);
		// MultipartFile multipartFile = new MockMultipartFile("file", file.getName(),
		// "text/plain",
		// IOUtils.toByteArray(input));
		//
		// DataProvider h =
		// DataProviders.newSQLDumpFileHandlerWithDelimiter(multipartFile);
		// Iterator<String> iterator = h.iterator();
		// while (iterator.hasNext()) {
		// System.out.println(iterator.next());
		// }
		// }
	}

}
