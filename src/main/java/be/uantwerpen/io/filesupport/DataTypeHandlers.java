/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.io.filesupport;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.commons.io.IOUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import be.uantwerpen.sniper.dao.PersistentStorageHandler;
import be.uantwerpen.sniper.dao.PersistentStorageHandlers;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifier;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifiers.RawDataTableIdentifier;
import be.uantwerpen.sniper.dao.entity.DataTable;

/**
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class DataTypeHandlers {

	public static DataTypeHandler<File> newTransactionFileHandler() {
		return new TransactionFileHandler();
	}

	public static DataTypeHandler<File> newDataTableTypeHandler() {
		return DataTypeHandlers.newDataTableTypeHandler(false);
	}

	public static DataTypeHandler<File> newDataTableTypeHandler(boolean colNames) {
		return new DataTableTypeHandler(colNames);
	}

	public static DataTypeHandler<File> newArffTypeHandler() {
		return new ArffTypeHandler();
	}

	public static DataTypeHandler<File> newTextTypeFileHandler() {
		return new TextTypeFileHandler();
	}

	/**
	 * Implements a handler for transactional (fimi) data files. I.e., each line is
	 * a transaction and contains a number of propositions (also items) it supports.
	 *
	 * @author Sandy Moens
	 * @since 0.0.2
	 * @version 0.2.0
	 */
	private static class TransactionFileHandler implements DataTypeHandler<File> {

		private TransactionFileHandler() {
		}

		@Override
		public void handleDataFile(PersistentStorageIdentifier identifier, DataProvider dataProvider,
				PersistentStorageHandler<File> persistentStorageHandler) throws Exception {
			persistentStorageHandler.store(identifier, dataProvider.iterator());
		}

	}

	/**
	 * Implements a handler for data table files. Essentially a data table is some
	 * raw data in column format, where each row has an entry for each column.
	 * 
	 * This class tries to deduce the type of a column either as numeric or as
	 * categoric value.
	 *
	 * @author Sandy Moens
	 * @since 0.0.2
	 * @version 0.0.2
	 */
	private static class DataTableTypeHandler implements DataTypeHandler<File> {

		// private boolean colNames;

		private DataTableTypeHandler(boolean colNames) {
			// this.colNames = colNames;
		}

		@Override
		public void handleDataFile(PersistentStorageIdentifier identifier, DataProvider dataProvider,
				PersistentStorageHandler<File> persistentStorageHandler) throws Exception {
			persistentStorageHandler.store(identifier, dataProvider.iterator());
			// persistentStorageHandler.store(dbName, "attributes.txt",
			// getAttributeInformationData(dataProvider));
		}

		// private Iterator<String> getDataIterator(DataProvider dataProvider) {
		// Iterator<String> iterator = dataProvider.iterator();
		// // remove the first line, containing the data table header
		// if (this.colNames) {
		// iterator.next();
		// }
		// return iterator;
		// }

		// private Iterator<String> getAttributeInformationData(DataProvider
		// dataProvider) {
		// // String attributeNames = "";
		// // String attributeTypes = "";
		// //
		// // Iterator<String> data = dataProvider.iterator();
		// //
		// // if (this.colNames) {
		// // attributeNames = data.next();
		// // } else {
		// // int length = data.next().length;
		// // attributeNames = new String[length];
		// // for (int i = 0, end = length; i < end; i++) {
		// // attributeNames[i] = "Col_" + i;
		// // }
		// // data = dataProvider.iterator();
		// // }
		// //
		// // attributeTypes = new String[attributeNames.length];
		// // for (int i = 0; i < attributeTypes.length; i++) {
		// // attributeTypes[i] = "numeric";
		// // }
		// // while (data.hasNext()) {
		// // String[] next = data.next();
		// // for (int i = 0, iEnd = next.length; i < iEnd; i++) {
		// // if (attributeTypes[i] == "categoric") {
		// // continue;
		// // }
		// // try {
		// // Double.parseDouble(next[i]);
		// // } catch (NumberFormatException e) {
		// // attributeTypes[i] = "categoric";
		// // }
		// // }
		// // }
		// //
		// // List<String[]> attributes =
		// newArrayListWithCapacity(attributeNames.length);
		// // for (int i = 0, iEnd = attributeNames.length; i < iEnd; i++) {
		// // attributes.add(new String[] { attributeNames[i], attributeTypes[i],
		// // attributeNames[i] });
		// // }
		// // return attributes.iterator();
		//
		// return new ArrayList<String>().iterator();
		// }

	}

	/**
	 * Implements a handler for arff data files. A typical data files contains 1) a
	 * header with information about the name of the relation and the attributes and
	 * types, and 2) a data part that is delimiter by the keyword "@data"
	 *
	 * @author Sandy Moens
	 * @since 0.0.2
	 * @version 0.0.2
	 */
	private static class ArffTypeHandler implements DataTypeHandler<File> {

		private ArffTypeHandler() {
		}

		@Override
		public void handleDataFile(PersistentStorageIdentifier identifier, DataProvider dataProvider,
				PersistentStorageHandler<File> persistentStorageHandler) throws Exception {
			persistentStorageHandler.store(identifier, getDataIterator(dataProvider));
			// persistentStorageHandler.store(dbName, "attributes.txt",
			// getAttributeInformationData(dataProvider));
		}

		private Iterator<String> getDataIterator(DataProvider dataProvider) {
			Iterator<String> iterator = dataProvider.iterator();
			while (!iterator.next().toLowerCase().startsWith("@data")) {
				// strip the header part until the data is available
			}
			return iterator;
		}

		// private Iterator<String> getAttributeInformationData(DataProvider
		// dataProvider) {
		// // Iterator<String> iterator = dataProvider.iterator();
		// //
		// // List<String> attributes = newArrayList();
		// //
		// // String line = null;
		// // while (!(line = iterator.next()).toLowerCase().startsWith("@data")) {
		// // if (line.toLowerCase().startsWith("@attribute")) {
		// // String type = line.toLowerCase().contains("real") ? "numeric" :
		// "categoric";
		// // String[] split = line.split(" |\t");
		// // attributes.add(new String[] { split[1], type, split[1] });
		// // }
		// // }
		// // return attributes.iterator();
		//
		// return new ArrayList<String>().iterator();
		// }

	}

	private static class TextTypeFileHandler implements DataTypeHandler<File> {

		private TextTypeFileHandler() {
		}

		@Override
		public void handleDataFile(PersistentStorageIdentifier identifier, DataProvider dataProvider,
				PersistentStorageHandler<File> persistentStorageHandler) throws Exception {
			persistentStorageHandler.store(identifier, dataProvider.iterator());
		}
	}

	// Suppress default constructor for non-instantiability
	private DataTypeHandlers() {
		throw new AssertionError();
	}

	public static void main(String[] args) throws Exception {
		PersistentStorageHandler<File> psh = PersistentStorageHandlers.fileBasedPersistentStorageHandler();

		{
			PersistentStorageIdentifier identifier = new RawDataTableIdentifier(
					new DataTable("testDataTable", "testDataTable", "Description"));

			psh.delete(identifier);

			File file = new File("testData/testDataTable.csv");
			FileInputStream input = new FileInputStream(file);
			MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain",
					IOUtils.toByteArray(input));

			DataProvider h = DataProviders.newFlatFileHandlerWithDelimiter(multipartFile, ";");
			DataTypeHandler<File> dth = DataTypeHandlers.newDataTableTypeHandler(true);

			dth.handleDataFile(identifier, h, psh);
		}

		{
			PersistentStorageIdentifier identifier = new RawDataTableIdentifier(
					new DataTable("testTransactions", "testTransactions", "Description"));

			psh.delete(identifier);

			File file = new File("testData/testTransactions.dat");
			FileInputStream input = new FileInputStream(file);
			MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain",
					IOUtils.toByteArray(input));

			DataProvider h = DataProviders.newFlatFileHandlerWithDelimiter(multipartFile, " ");
			DataTypeHandler<File> dth = DataTypeHandlers.newTransactionFileHandler();

			dth.handleDataFile(identifier, h, psh);
		}

		{
			PersistentStorageIdentifier identifier = new RawDataTableIdentifier(
					new DataTable("iris", "iris", "Description"));

			psh.delete(identifier);

			File file = new File("testData/iris.arff");
			FileInputStream input = new FileInputStream(file);
			MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain",
					IOUtils.toByteArray(input));

			DataProvider h = DataProviders.newFlatFileHandlerWithDelimiter(multipartFile, ",");
			DataTypeHandler<File> dth = DataTypeHandlers.newArffTypeHandler();

			dth.handleDataFile(identifier, h, psh);
		}

		// {
		// File file = new File("testData/testDataTable_mysqldump.sql");
		// FileInputStream input = new FileInputStream(file);
		// MultipartFile multipartFile = new MockMultipartFile("file", file.getName(),
		// "text/plain",
		// IOUtils.toByteArray(input));
		//
		// DataProvider h =
		// DataProviders.newSQLDumpFileHandlerWithDelimiter(multipartFile);
		// DataTypeHandler dth = DataTypeHandlers.newDataTableTypeHandler();
		// dth.handleDataFile("testDataTable_mysqldump", h, psh);
		// }

		// {
		// File file = new File("testData/testDataTable_sequelPro.sql");
		// FileInputStream input = new FileInputStream(file);
		// MultipartFile multipartFile = new MockMultipartFile("file", file.getName(),
		// "text/plain",
		// IOUtils.toByteArray(input));
		//
		// DataProvider h =
		// DataProviders.newSQLDumpFileHandlerWithDelimiter(multipartFile);
		// DataTypeHandler dth = DataTypeHandlers.newDataTableTypeHandler();
		// dth.handleDataFile("testDataTable_sequelPro", h, psh);
		// }

		{
			PersistentStorageIdentifier identifier = new RawDataTableIdentifier(
					new DataTable("testTextTypeData", "testTextTypeData", "Description"));

			psh.delete(identifier);

			File file = new File("testData/testTextTypeData.txt");
			FileInputStream input = new FileInputStream(file);
			MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain",
					IOUtils.toByteArray(input));

			DataProvider h = DataProviders.newFlatFileHandlerNoDelimiter(multipartFile);
			DataTypeHandler<File> dth = DataTypeHandlers.newTextTypeFileHandler();

			dth.handleDataFile(identifier, h, psh);
		}
	}

}
