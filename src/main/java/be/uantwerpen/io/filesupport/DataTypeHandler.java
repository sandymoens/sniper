/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015-2017 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.uantwerpen.io.filesupport;

import be.uantwerpen.sniper.dao.PersistentStorageHandler;
import be.uantwerpen.sniper.dao.PersistentStorageIdentifier;

/**
 * A interface that knows how to handle a specific type of data coming from a
 * generic data source. The content of the parsed file are written to a
 * persistent storage medium.
 *
 * @author Sandy Moens
 * @since 0.0.2
 * @version 0.2.0
 */
public interface DataTypeHandler<T> {

	/**
	 * Provides a handler for new data with a given name. The parsed content is
	 * written to the provided persistent storage medium handler.
	 * 
	 * @param identifier
	 *            the identifier for the data
	 * @param dataProvider
	 *            an object that provides iterator access to some data
	 * @param persistentStorageHandler
	 *            the handler that is used to write parsed data to in the form
	 * @throws Exception
	 */
	public void handleDataFile(PersistentStorageIdentifier identifier, DataProvider dataProvider,
			PersistentStorageHandler<T> persistentStorageHandler) throws Exception;

}
