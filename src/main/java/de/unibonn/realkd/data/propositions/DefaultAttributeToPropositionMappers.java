/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unibonn.realkd.data.propositions;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.2.0
 * @version 0.2.0
 */
public class DefaultAttributeToPropositionMappers {

	public static PropositionalizationRule newSpecificThenGlobalMapper(List<PropositionalizationRule> globalMappers,
			Map<String, List<PropositionalizationRule>> specificMappers) {
		return new SpecificThenGlobalMapper(globalMappers, specificMappers);
	}

	private static class SpecificThenGlobalMapper implements PropositionalizationRule {

		private List<PropositionalizationRule> globalMappers;

		private Map<String, List<PropositionalizationRule>> specificMappers;

		private SpecificThenGlobalMapper(List<PropositionalizationRule> globalMappers,
				Map<String, List<PropositionalizationRule>> specificMappers) {
			this.globalMappers = globalMappers;
			this.specificMappers = specificMappers;
		}

		private static <T> List<AttributeBasedProposition<?>> constructPropositions(DataTable table,
				Attribute<T> attribute, List<PropositionalizationRule> mappers) {
			List<AttributeBasedProposition<?>> propositions = Lists.newArrayList();

			for (PropositionalizationRule mapper : mappers) {
				propositions.addAll(mapper.apply(table, attribute));
			}

			return propositions;
		}

		@Override
		public <T> List<AttributeBasedProposition<?>> apply(DataTable table, Attribute<T> attribute) {
			List<PropositionalizationRule> specificMappers = this.specificMappers.get(attribute.caption());

			if (specificMappers != null) {
				return constructPropositions(table, attribute, specificMappers);
			}

			return constructPropositions(table, attribute, this.globalMappers);
		}

	}

	private DefaultAttributeToPropositionMappers() {
		;
	}
}
