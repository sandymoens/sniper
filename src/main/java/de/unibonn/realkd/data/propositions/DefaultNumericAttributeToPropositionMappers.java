/*
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * 
 * Version 3, 19 November 2007 
 *
 * Copyright (C) 2015 University of Antwerp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unibonn.realkd.data.propositions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

import de.unibonn.realkd.data.constraints.Constraint;
import de.unibonn.realkd.data.constraints.Constraints;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;

/**
 *
 *
 * @author Sandy Moens
 * @since 0.3.0
 * @version 0.3.0
 */
public class DefaultNumericAttributeToPropositionMappers {

	public static PropositionalizationRule newEquiDistantMapper(int numberOfBins) {
		return new EquiDistantMapper(numberOfBins);
	}

	public static PropositionalizationRule newEquiFrequentMapper(int numberOfBins) {
		return new EquiFrequentMapper(numberOfBins);
	}

	public static PropositionalizationRule newFixedBinMapper(SortedSet<Double> bins) {
		return new FixedBinMapper(bins);
	}

	private static Constraint<Double> newIntervalConstraint(double lower, boolean lowerInclusive, double upper,
			boolean upperInclusive) {
		Constraint<Double> gt = lowerInclusive ? Constraints.greaterOrEquals(lower) : Constraints.greaterThan(lower);
		Constraint<Double> lt = upperInclusive ? Constraints.lessOrEquals(upper) : Constraints.lessThan(upper);

		String name = String.format("%s%.2f", lowerInclusive ? "[" : "(", lower) + ";"
				+ String.format("%.2f%s", upper, upperInclusive ? "]" : ")");

		String name1 = String.format("%s%.2f", lowerInclusive ? ">=" : ">", lower);
		String name2 = String.format("%s%.2f", upperInclusive ? "<=" : "<", upper);

		return Constraints.namedConstraint(Constraints.and(Constraints.namedConstraint(gt, name1, name1),
				Constraints.namedConstraint(lt, name2, name2)), name, name);
	}

	private static class EquiDistantMapper implements PropositionalizationRule {

		private int numberOfBins;

		private EquiDistantMapper(int numberOfBins) {
			this.numberOfBins = numberOfBins;
		}

		@Override
		public <T> List<AttributeBasedProposition<?>> apply(DataTable table, Attribute<T> attribute) {
			List<AttributeBasedProposition<?>> result = new ArrayList<>();
			if (attribute instanceof MetricAttribute) {
				MetricAttribute metricAttribute = (MetricAttribute) attribute;
				double min = metricAttribute.min();
				double max = metricAttribute.max();
				double diff = (max - min) / this.numberOfBins;

				for (int i = 0; i < this.numberOfBins; i++) {
					result.add(Propositions.proposition(table, metricAttribute,
							newIntervalConstraint((min + (i * diff)), i == 0, min + ((i + 1) * diff), true)));
				}
			}
			return result;
		}

	}

	private static class EquiFrequentMapper implements PropositionalizationRule {

		private int numberOfBins;

		private EquiFrequentMapper(int numberOfBins) {
			this.numberOfBins = numberOfBins;
		}

		@Override
		public <T> List<AttributeBasedProposition<?>> apply(DataTable table, Attribute<T> attribute) {
			List<AttributeBasedProposition<?>> result = new ArrayList<>();
			if (attribute instanceof MetricAttribute) {
				MetricAttribute metricAttribute = (MetricAttribute) attribute;
				int numberOfNonMissingValues = metricAttribute.numberOfNonMissingValues();
				int numberOfValuesPerBin = (int) Math.ceil(1. * numberOfNonMissingValues / this.numberOfBins);
				List<Double> values = metricAttribute.nonMissingValuesInOrder();

				values.sort(Comparator.naturalOrder());

				double lower = values.get(0);
				double upper = values.get(0);

				int s = 0;
				int bin = 0;
				for (Double v : values) {
					upper = v;

					s++;

					// add all but last bins once they are full
					if (s == numberOfValuesPerBin && bin != this.numberOfBins) {
						result.add(Propositions.proposition(table, metricAttribute,
								newIntervalConstraint(lower, bin == 0, upper, true)));
						lower = upper;
						bin++;
						s = 0;
					}
				}

				// add last bin
				result.add(Propositions.proposition(table, metricAttribute,
						newIntervalConstraint(lower, false, upper, true)));
			}
			return result;
		}

	}

	private static class FixedBinMapper implements PropositionalizationRule {

		private SortedSet<Double> bins;

		private FixedBinMapper(SortedSet<Double> bins) {
			this.bins = bins;
		}

		@Override
		public <T> List<AttributeBasedProposition<?>> apply(DataTable table, Attribute<T> attribute) {
			List<AttributeBasedProposition<?>> result = new ArrayList<>();
			if (attribute instanceof MetricAttribute) {
				MetricAttribute metricAttribute = (MetricAttribute) attribute;

				Iterator<Double> binsIt = this.bins.iterator();

				double prev = binsIt.next();
				boolean first = true;

				while (binsIt.hasNext()) {
					if (first) {
						result.add(Propositions.proposition(table, metricAttribute,
								newIntervalConstraint(prev, true, prev = binsIt.next(), true)));
						first = false;
					} else {
						result.add(Propositions.proposition(table, metricAttribute,
								newIntervalConstraint(prev, false, prev = binsIt.next(), true)));
					}
				}
			}
			return result;
		}

	}

	private DefaultNumericAttributeToPropositionMappers() {
		;
	}
}
