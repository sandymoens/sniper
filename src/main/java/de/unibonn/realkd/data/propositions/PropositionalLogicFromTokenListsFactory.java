package de.unibonn.realkd.data.propositions;
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static de.unibonn.realkd.common.IndexSets.copyOf;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import com.google.common.collect.Lists;

import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.data.Populations;

/**
 * Creates a propositional logic directly from a transactional data file.
 * 
 * @author Sandy Moens
 * 
 * @since 0.2.1
 * 
 * @version 0.2.1
 *
 */
public class PropositionalLogicFromTokenListsFactory {

	private static final Logger LOGGER = Logger.getLogger(PropositionalLogicFromTokenListsFactory.class.getName());

	public static DefaultPropositionalContext build(String name, List<List<String>> tokenLists) {
		LOGGER.fine("Compiling proposition list from lists of tokens");

		Map<String, Integer> indexMap = newHashMap();
		List<List<Integer>> supportSets = newArrayList();
		List<String> objectNames = newArrayList();
		List<String> population = newArrayList();

		int tid = 0;
		for (List<String> tokenList : tokenLists) {
			for (String token : tokenList) {
				Integer id = indexMap.get(token);
				if (id == null) {
					objectNames.add(token);
					id = supportSets.size();
					indexMap.put(token, id);
					supportSets.add(Lists.newArrayList());
				}
				supportSets.get(id).add(tid);
			}
			population.add(String.valueOf(tid));
			tid++;
		}

		List<Proposition> propositions = IntStream.range(0, objectNames.size()).mapToObj(i -> {
			return Propositions.proposition(i, objectNames.get(i), copyOf(supportSets.get(i)));
		}).collect(toList());

		LOGGER.info("Done compiling proposition list (" + propositions.size() + " propositions added)");

		return new DefaultPropositionalContext(name, "",
				Populations.population(Identifier.id("population_of_" + name), "Population of " + name, "", population),
				propositions);
	}

}
