package de.unibonn.realkd.data.propositions;
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import de.unibonn.realkd.data.constraints.Constraint;
import de.unibonn.realkd.data.constraints.Constraints;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.DefaultTableBasedPropositionalLogic;
import de.unibonn.realkd.data.propositions.Propositions;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.DefaultCategoricAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attributegroups.AttributeGroup;
import de.unibonn.realkd.data.table.attributegroups.OrderedAttributeSequence;

/**
 * Creates a table-based propositional logic by applying a set of proposition
 * factories to each attribute and attribute group present in the input
 * datatable.
 * 
 * @author Sandy Moens
 * 
 * @since 0.2.1
 * 
 * @version 0.2.1
 *
 */
public class PropositionalLogicFromTableManualBinningFactory {

	private static final Logger LOGGER = Logger
			.getLogger(PropositionalLogicFromTableManualBinningFactory.class.getName());

	public static int NUMBER_OF_BINS = 5;

	private enum PropositionFactory {

		CATEGORIC_EQUALiTY {
			@Override
			public void constructPropositions(DataTable table, Attribute<?> attribute,
					List<AttributeBasedProposition<?>> result) {
				if (attribute instanceof DefaultCategoricAttribute) {
					for (String category : ((DefaultCategoricAttribute) attribute).categories()) {
						result.add(Propositions.proposition(table, (DefaultCategoricAttribute) attribute, Constraints.equalTo(category)));
					}
				}
			}
		},

		BINS {
			@Override
			public void constructPropositions(DataTable table, Attribute<?> attribute,
					List<AttributeBasedProposition<?>> result) {
				if (attribute instanceof MetricAttribute) {
					MetricAttribute defaultMetricAttribute = (MetricAttribute) attribute;
					double min = defaultMetricAttribute.min();
					double max = defaultMetricAttribute.max();
					double diff = (max - min) / NUMBER_OF_BINS;

					for (int i = 0; i < NUMBER_OF_BINS; i++) {
						Constraint<Double> greaterThan = Constraints.greaterThan(min + (i * diff));
						Constraint<Double> lessThan = Constraints.lessThan(min + ((i + 1) * diff) + 0.00001);
						String name1 = String.format("%s%.2f", ">", (min + (i * diff))).toString();
						String name2 = String.format("%s%.2f", "<=", min + ((i + 1) * diff)).toString();
						result.add(Propositions.proposition(table, defaultMetricAttribute, Constraints.and(Constraints.namedConstraint(greaterThan, name1, name1),
								Constraints.namedConstraint(lessThan, name2, name2))));
					}
				}
			}
		};

		public abstract void constructPropositions(DataTable table, Attribute<?> attribute,
				List<AttributeBasedProposition<?>> result);

	}

	private enum PropositionFromGroupFactory {

		CONSECUTIVE_CHANGE_ATTRIBUTES {
			@Override
			public void constructPropositions(DataTable table, AttributeGroup attributeGroup,
					List<AttributeBasedProposition<?>> result) {

				if (attributeGroup instanceof OrderedAttributeSequence) {
					OrderedAttributeSequence<?> sequenceGroup = (OrderedAttributeSequence<?>) attributeGroup;
					for (int i = 0; i < sequenceGroup.elements().size() - 1; i++) {
						Attribute<?> attribute = sequenceGroup.getChangeAttribute(i, i + 1);
						for (PropositionFactory attributeFactory : PropositionFactory.values()) {
							attributeFactory.constructPropositions(table, attribute, result);
						}
					}
				}

			}
		};

		public abstract void constructPropositions(DataTable table, AttributeGroup attributeGroup,
				List<AttributeBasedProposition<?>> result);

	}

	public static DefaultTableBasedPropositionalLogic build(DataTable dataTable) {
		LOGGER.fine("Compiling proposition list");
		List<AttributeBasedProposition<?>> propositions = new ArrayList<>();
		for (Attribute<?> attribute : dataTable.attributes()) {
			for (PropositionalLogicFromTableManualBinningFactory.PropositionFactory propFactory : PropositionalLogicFromTableManualBinningFactory.PropositionFactory
					.values()) {
				propFactory.constructPropositions(dataTable, attribute, propositions);
			}
		}

		for (AttributeGroup attributeGroup : dataTable.attributeGroups()) {
			for (PropositionalLogicFromTableManualBinningFactory.PropositionFromGroupFactory propFactory : PropositionalLogicFromTableManualBinningFactory.PropositionFromGroupFactory
					.values()) {
				propFactory.constructPropositions(dataTable, attributeGroup, propositions);
			}
		}

		propositions.forEach(p -> {
			propositions.subList(0, propositions.indexOf(p)).forEach(q -> {
				if (p.implies(q)) {
					LOGGER.fine("'" + p + "' implies already present proposition '" + q + "'");
				}
				if (q.implies(p)) {
					LOGGER.fine("'" + p + "' is implied by already present proposition '" + q + "'");
				}
			});
		});

		LOGGER.info("Done compiling proposition list (" + propositions.size() + " propositions added)");

		return new DefaultTableBasedPropositionalLogic(dataTable, propositions);
	}

}
