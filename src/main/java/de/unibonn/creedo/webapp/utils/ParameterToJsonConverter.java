package de.unibonn.creedo.webapp.utils;
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import de.unibonn.creedo.common.parameters.AbstractTransferableParameterState;
import de.unibonn.creedo.common.parameters.ParameterStateWithRangeInfo;
import de.unibonn.creedo.common.parameters.PlainParameterState;
import de.unibonn.creedo.common.parameters.TransferableParameterState;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

/**
 * Utility class that produces a utility object used for the transfer of
 * parameter information from server to client. The resulting data bean can
 * easily be converted to JSON by the framework.
 * 
 * To enable a new ui control element of a parameter type, make sure changing
 * *all* the following places accordingly: 1. ParameterType,
 * ParameterToJsonConverter.convert() in this class. 2.
 * static/repositoryAdministratioPage.jsp for enabling new ui element in
 * repository admin page. 3. ... 4. client/js/parameterHandler.js method
 * renderParameters() for enabling new ui element in custom mining dashboard. 5.
 * client/js/customDashboard.js method initializeParameters() to enable the
 * change listener in custom mining dashboard. 6. client/js/mineButtonREF.js
 * method updateParameterArea() due the code legacy.
 * 
 * @author Björn Jacobs
 * @author Sandy Moens
 * @since
 * @version 0.3.0
 */
public class ParameterToJsonConverter {

	private static final Logger LOGGER = Logger.getLogger(ParameterToJsonConverter.class.getName());

	public static <T> AbstractTransferableParameterState convert(Parameter<T> parameter, int depth) {

		if (parameter instanceof RangeEnumerableParameter) {

			// Handle range info
			RangeEnumerableParameter<T> rangeParam = (RangeEnumerableParameter<T>) parameter;
			// if (rangeParam.isContextValid()) {

			List<T> range;
			// if (rangeParam.getRange() == null) {
			if (!rangeParam.isContextValid()) {
				/*
				 * following old line was broken; setting range to null will cause null pointer
				 * exception below. Now set to empty list instead
				 */
				// range = null;
				range = new ArrayList<>();
			} else {
				range = new ArrayList<>(rangeParam.getRange());
			}

			if (range.size() == 0) {
				LOGGER.finer("Concrete range is empty. No option was applicable.");
			} else {
				// Otherwise set first applicable option as initial
				// value if not set already
				if (!rangeParam.isValid()) {
					rangeParam.set(range.get(0));
				}
			}
			List<String> rangeStringList = new ArrayList<>();
			for (Object obj : range) {
				rangeStringList.add(obj.toString());
			}

			AbstractTransferableParameterState container = new ParameterStateWithRangeInfo(depth,
					ParameterType.RANGE_ENUMERABLE.getName(), parameter.getName(), parameter.getDescription(),
					parameter.isContextValid(), generateDependsOnNotice(parameter.getDependsOnParameters()),
					parameter.get().map(v -> v.toString()), parameter.isValid(), parameter.getValueCorrectionHint(),
					rangeStringList, parameter.hidden());

			// container.setRange(rangeStringList);
			// container.setValues(rangeParam.get().map(
			// v -> ImmutableList.of(v.toString())));

			return container;

		} else if (parameter instanceof SubCollectionParameter) {
			TransferableParameterState container = new TransferableParameterState(depth,
					((parameter.getType() == Set.class) ? ParameterType.SUBSET.getName()
							: ParameterType.SUBCOLLECTION.getName()),
					parameter.getName(), parameter.getDescription(), parameter.isContextValid(),
					generateDependsOnNotice(parameter.getDependsOnParameters()), parameter.isValid(),
					parameter.getValueCorrectionHint(), parameter.hidden());

			SubCollectionParameter<?, ?> rangeParam = (SubCollectionParameter<?, ?>) parameter;

			// Handle range info
			List<String> range = new ArrayList<>();
			for (Object x : (Collection<?>) rangeParam.getCollection()) {
				range.add(x.toString());
			}

			Function<Collection<?>, List<String>> toStringList = l -> l.stream().map(e -> e.toString())
					.collect(Collectors.toList());
			Optional<List<String>> value = rangeParam.get().map(toStringList);
			container.setValues(value);
			container.setRange(range);
			return container;

		} else {
			String type = parameter.getType().equals(Integer.class) ? "Integer"
					: (parameter.getType().equals(Double.class) ? "Double" : "Text");

			return new PlainParameterState(depth, type, parameter.getName(), parameter.getDescription(),
					parameter.isContextValid(), generateDependsOnNotice(parameter.getDependsOnParameters()),
					parameter.get().map(v -> v.toString()), parameter.isValid(), parameter.getValueCorrectionHint(),
					parameter.hidden());
		}
	}

	private static String generateDependsOnNotice(List<Parameter<?>> dependsOnParameters) {
		if (dependsOnParameters == null || dependsOnParameters.size() == 0) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		sb.append("Depends on valid values for: ");
		Iterator<Parameter<?>> it = dependsOnParameters.iterator();
		while (it.hasNext()) {
			Parameter<?> param = it.next();
			sb.append(param.getName());

			if (it.hasNext()) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}

	// public static List<JsonTransferParameterContainer> convertFlat(
	// List<Parameter<?>> parameters) {
	// List<JsonTransferParameterContainer> result = new ArrayList<>();
	//
	// if (parameters != null) {
	// for (Parameter<?> parameter : parameters) {
	// result.add(convert(parameter, 0));
	// }
	// }
	// return result;
	// }

	public static List<AbstractTransferableParameterState> convertRecursively(List<Parameter<?>> parameters) {
		List<AbstractTransferableParameterState> result = new ArrayList<>();

		recurse(parameters, result, 0);

		return result;
	}

	private static List<AbstractTransferableParameterState> recurse(List<Parameter<?>> parameters,
			List<AbstractTransferableParameterState> result, int depth) {

		if (parameters != null) {
			for (Parameter<?> parameter : parameters) {
				result.add(convert(parameter, depth));

				if (parameter.current() instanceof ParameterContainer) {
					ParameterContainer container = (ParameterContainer) parameter.current();
					List<Parameter<?>> containerParams = container.getTopLevelParameters();

					recurse(containerParams, result, depth + 1);
				}
			}
		}
		return result;
	}

	private enum ParameterType {
		TEXT {
			@Override
			public String getName() {
				return "Text";
			}
		},
		RANGE_ENUMERABLE {
			@Override
			public String getName() {
				return "RangeEnumerable";
			}
		},
		SUBSET {
			@Override
			public String getName() {
				return "Subset";
			}
		},
		SUBLIST {
			@Override
			public String getName() {
				return "Sublist";
			}
		},
		SUBCOLLECTION {
			@Override
			public String getName() {
				return "Subcollection";
			}
		};
		public abstract String getName();
	}

}
