package de.unibonn.creedo.common.parameters;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class AbstractTransferableParameterState {

	@JsonProperty("depth")
	private final int depth;

	@JsonProperty("type")
	private final String type;

	@JsonProperty("name")
	private final String name;

	@JsonProperty("description")
	private final String description;

	@JsonProperty("solutionHint")
	private final String solutionHint;

	@JsonProperty("dependsOnNotice")
	private final String dependsOnNotice;

	@JsonProperty("valid")
	private final boolean valid;

	@JsonProperty("active")
	private final boolean active;

	@JsonProperty("hidden")
	private final boolean hidden;

	public AbstractTransferableParameterState(int depth, String type, String name, String description, boolean isActive,
			String dependsOnNotice, boolean isValid, String solutionHint, boolean hidden) {
		this.depth = depth;
		this.type = type;
		this.name = name;
		this.description = description;
		this.active = isActive;
		this.dependsOnNotice = dependsOnNotice;
		this.valid = isValid;
		this.solutionHint = solutionHint;
		this.hidden = hidden;
	}

	protected String name() {
		return this.name;
	}

	protected boolean valid() {
		return this.valid;
	}

	protected boolean active() {
		return this.active;
	}

}