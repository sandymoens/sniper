package de.unibonn.creedo.common.parameters;

import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ParameterStateWithRangeInfo extends PlainParameterState {

	@JsonProperty("range")
	private final List<String> range;

	public ParameterStateWithRangeInfo(int depth, String type, String name, String description, boolean isActive,
			String dependsOnNotice, Optional<String> value, boolean isValid, String solutionHint, List<String> range,
			boolean hidden) {
		super(depth, type, name, description, isActive, dependsOnNotice, value, isValid, solutionHint, hidden);
		this.range = range;
	}

}
