package de.unibonn.creedo.common.parameters;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlainParameterState extends AbstractTransferableParameterState {

	@JsonProperty("value")
	private final String value;

	public PlainParameterState(int depth, String type, String name, String description, boolean isActive,
			String dependsOnNotice, Optional<String> value, boolean isValid, String solutionHint, boolean hidden) {
		super(depth, type, name, description, isActive, dependsOnNotice, isValid, solutionHint, hidden);
		this.value = value.orElse(null);
	}

}
