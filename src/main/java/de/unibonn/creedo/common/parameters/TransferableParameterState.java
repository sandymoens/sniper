package de.unibonn.creedo.common.parameters;

import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Björn Jacobs
 * @author Sandy Moens
 * @since
 * @version 0.3.0
 */
public class TransferableParameterState extends AbstractTransferableParameterState {

	@JsonProperty("range")
	private List<String> range;

	@JsonProperty("values")
	private List<String> values;

	public TransferableParameterState(int depth, String type, String name, String description, boolean isActive,
			String dependsOnNotice, boolean isValid, String solutionHint, boolean hidden) {
		super(depth, type, name, description, isActive, dependsOnNotice, isValid, solutionHint, hidden);
	}

	public void setRange(List<String> range) {
		this.range = range;
	}

	public void setValues(Optional<List<String>> values) {
		this.values = values.orElse(null);
	}

	@Override
	public String toString() {
		return name() + " = " + this.values + ", active: " + active() + ", valid: " + valid();
	}

}
